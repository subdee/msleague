
var Chat = {
    
    refreshInterval : 10,
    
    le : new MSL.LoopedEvent(function() {
        $.ajax({
            url: MSL.Url('post', 'chat'),
            type: "POST",
            success: function(data)
            {
                if(data.redirect) MSL.Redirect(data.redirect);
                
                $(".chatbox").append(data.html);
                $(".chat").attr({
                    scrollTop: $(".chat").attr("scrollHeight")
                });
                Chat.Enable();
            },
            beforeSend: function() {
                Chat.Disable();
            }
        });
    }
    ),
    
    Enable : function() {
        this.le.SetInterval( this.refreshInterval * 1000 );
        this.le.Start();
    },
    Disable : function() {
        this.le.Stop();
    },
    RestartTimer : function() {
        this.le.Reset();
    }
}

$(document).ready(function(){

    Chat.Enable();
    
    $(".chatForm").submit(function(){
        var shout = $(".chatForm :text").val();
        if(shout == '') return false;
        
        $.ajax({
            url: MSL.Url('post', 'chat'),
            type: "POST",
            data: {
                'shout' : shout
            },
            success: function(data)
            {
                if (data.error){
                    $(".chatError").html(data.error);
                } else {
                    if($('#shoutCount').val() == 0) {
                        $(".chatbox").html(data.html);
                    } else {
                        $(".chatbox").append(data.html);
                    }
                    $('#shoutCount').val( parseInt($('#shoutCount').val()) + 1 );
                    
                    $(".chat table tbody").children(":last-child").show("highlight", {}, 2000);
                    $(".chatError").html("");
                    $(".chat").prop({
                        scrollTop: $(".chat").prop("scrollHeight")
                    });
                }
                $(".chatForm :submit").attr("disabled",false);
                $(".chatForm :text").attr('disabled', false).val("").focus();                
                $(".chatForm form img").hide();
                Chat.Enable();
            },
            beforeSend: function(){
                $(".chatForm form img").show();
                $(".chatForm :text").attr('disabled', true);
                $(".chatForm :submit").attr("disabled",true);
                Chat.Disable();
            },
            error: function(){
                Chat.Disable();
            }
        });
        return false;
    })
    
    $(".chat").prop({
        scrollTop: $(".chat").prop("scrollHeight")
    });
})