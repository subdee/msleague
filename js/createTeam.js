var data = {
    'gk'  : ['Goalkeeper', 'Goalkeeper'],
    'ld'  : ['Defender', 'Left Defender'],
    'cld' : ['Defender', 'Center Left Defender'],
    'crd' : ['Defender', 'Center Right Defender'],
    'rd'  : ['Defender', 'Right Defender'],
    'lm'  : ['Midfielder', 'Left Midfielder'],
    'clm' : ['Midfielder', 'Center Left Midfielder'],
    'crm' : ['Midfielder', 'Center Right Midfielder'],
    'rm'  : ['Midfielder', 'Right Midfielder'],
    'ls'  : ['Striker', 'Left Striker'],
    'rs'  : ['Striker', 'Right Striker'],
    'sg'  : ['Goalkeeper', 'Substitute Goalkeeper'],
    'sd'  : ['Defender', 'Substitute Defender'],
    'sm'  : ['Midfielder', 'Substitute Midfielder'],
    'ss'  : ['Striker', 'Substitute Striker']
};

$(document).ready(function()
{
    $('.pitchplayer').click(function(){
        var id = $(this).attr('id');
        $.ajax({
            url: "viewPlayers",
            type: "GET",
            data: "position=" + data[id][0] + "&pitchPosition=" + data[id][1],
            success: viewPlayersCb
        });
        return false;
    });

});

var Datatable =
{
    dttbl : null,

    filter : function( val, col, exactMatch )
    {
        if(exactMatch)
            val = (val == "" ? "" : "^" + val + "$");
        this.dttbl.fnFilter( val, col, true );
    }
};

function viewPlayersCb( data )
{
    $("#popupList").dialog("open");
    $("#playerList").html(data);
    Datatable.dttbl = $("#playerList table").dataTable({
        "bPaginate": false,
        "bLengthChange": false,
        "iDisplayLength": 15,
        "sScrollY": "300px",
        "bInfo": false,
        "aaSorting": [],
        'fnInitComplete' : function() {
            $('.dataTables_filter').remove();

            $('.filters .name input')
            .keyup(function(){
                Datatable.filter( $(this).val(), 0, false );
            })
            .focus(function(){
                $(this).val('');
                $(this).unbind('focus');
            });
            $('.filters .team select').change(function(){
                Datatable.filter($(this).val(), 1, true);
            });

        },
        "aoColumnDefs": [{
            "fnRender": function(obj) {
                return CurrencyFormatted(obj.aData[2]);
            },
            "bUseRendered": false,
            "aTargets": [2]
        }],
        "oLanguage": {
            "sProcessing":   "Επεξεργασία...",
            "sLengthMenu":   "Δείξε _MENU_ εγγραφές",
            "sZeroRecords":  "Δεν βρέθηκαν εγγραφές που να ταιριάζουν",
            "sInfo":         "Δείχνοντας _START_ εως _END_ από _TOTAL_ εγγραφές",
            "sInfoEmpty":    "Δείχνοντας 0 εως 0 από 0 εγγραφές",
            "sInfoFiltered": "(φιλτραρισμένες από _MAX_ συνολικά εγγραφές)",
            "sInfoPostFix":  "",
            "sSearch":       "Αναζήτηση:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "Πρώτη",
                "sPrevious": "Προηγούμενη",
                "sNext":     "Επόμενη",
                "sLast":     "Τελευταία"
            }
        }
    });
}