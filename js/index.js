$(document).ready(function()
{
    $('#loopedslider').loopedSlider({
        containerClick: false,
        addPagination: false,
        autoStart: 5000
    });

    if($('.items').size()>1)
    {
        $('.items').dataTable({
            "bPaginate": false,
            "bLengthChange": false,
            "oLanguage": {
                "sUrl": "i18n/el_GR.txt"
            }
        });
    }
});