
$(document).ready(function()
{
    $.ajaxSetup({
        //        timeout : 500
        });

    // has to do with the main navigation menu
    $('.menucontainer a img')
    .mouseover(function(){

        if($(this).attr('src').indexOf('_over') == -1) {
            img = $(this).attr('src').replace('.gif', '_over.gif');
            $(this).attr('src', img);
        }
        // first show the label and get the width.
        label = $('#lblcontainer');
        label.html( $(this).attr('alt') ).show();

        // figure out the offset required to center the label below the icon.
        offset = (label.outerWidth()-$(this).width()) * 0.5;
        label.css({
            left : $(this).position().left - offset,
            top  : $(this).position().top + $(this).height() + 5
        });
    })
    .mouseout(function(){
        if(!$(this).parent().hasClass('selected')) {
            img = $(this).attr('src').replace('_over.gif', '.gif');
            $(this).attr('src', img);
        }
        $('#lblcontainer').hide();
    });

    // has to to with all the gridviews
    $(".grid-view :text").focus(function(){
        $(this).val('');
    })

    //    $('.notice').centerText();
    globalAjaxCursorChange();

})

/**
 * Global (safe) ajax error handling
 */
$(document).ajaxError(function(event, jqXHR, ajaxSettings, thrownError)
{
    //    console.log(event);
    //    console.log(jqXHR);
    //    console.log(ajaxSettings);
    //    console.log(thrownError);

    // Override with custom call error handler
    if (ajaxSettings && ajaxSettings.error)
    {
        return ajaxSettings.error(event, jqXHR, ajaxSettings, thrownError);
    }

    // Handle global errors
    var errorText = thrownError + '(' + jqXHR.status + ')';
    if(jqXHR.readyState==4) errorText += ': ' + jqXHR.responseText;
    console.error(errorText);
});

/**
 * Checks whether a variable is numeric
 */
function IsNumeric(input)
{
    return (input - 0) == input && input.length > 0;
}

/**
 * Returns a currency variable formatted.
 * Note: This not safe because it is not localisable.
 *       It must take into account the application's locale.
 */
function CurrencyFormatted(amount)
{
    var i = parseFloat(amount);
    if(isNaN(i)) {
        i = 0.00;
    }
    var minus = '';
    if(i < 0) {
        minus = '-';
    }
    i = Math.abs(i);
    i = parseInt((i + .005) * 100);
    i = i / 100;
    s = new String(i);
    if(s.indexOf('.') < 0) {
        s += '.00';
    }
    if(s.indexOf('.') == (s.length - 2)) {
        s += '0';
    }
    s = minus + s;

    s = s.replace(',', '.');
    decs1 = s.substr(s.lastIndexOf('.'), 3);
    decs2 = decs1.replace('.', ',');
    s = s.replace(decs1, decs2);

    return s;
}

/**
 * Reads the url (get) variables
 */
$.extend({
    getHrefVars: function(href){
        var vars = [], hash;
        var hashes = href.slice(href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    getHrefVar: function(href, name){
        return $.getHrefVars(href)[name];
    },
    getUrlVars: function(){
        return $.getHrefVars(window.location.href);
    },
    getUrlVar: function(name){
        return $.getUrlVars()[name];
    }
});

/**
 * Centers text inside a paragraph.
 * Used for Notice widget
 */
(function($){
    $.fn.centerText = function()
    {
        return this.each(function(){
            obj = $(this).children('p');
            reset = false;
            if(!$(this).is(':visible'))
            {
                $(this).show();
                reset = true;
            }
            if(obj.outerHeight() < $(this).height())
            {
                obj.offset({
                    top: obj.position().top + ( $(this).height() - obj.outerHeight() ) * 0.5
                });
            }
            if(reset) $(this).hide();
        });
    };
})(jQuery);

(function($){
    $.fn.collapsable = function(options)
    {
        return this.each(function(index){

            if(typeof options == 'undefined') {
                options = {}
            }
            if(options.state != 'open') options.state = 'collapsed';

            id = 'collapsable_fieldset_wrapper_tohide_'+ index;
            $(this).wrapInner('<div id="'+id+'" />').find('legend').detach().prependTo($(this)).wrapInner('<a/>');
            $(this).find('legend a').
            css({
                'cursor':'pointer'
            }).
            click(function(){
                $('#collapsable_fieldset_wrapper_tohide_'+ index).toggle('blind');
                return false;
            });

            if(options.state == 'collapsed')
            {
                $(this).children('#'+id).hide();
            }
        });
    };
})(jQuery);

/**
 * Used in parallel with the Notice widget.
 * Helps enabling a Notice message with an ajax response.
 */
(function($){
    $.fn.notice = function(noticeData)
    {
        return this.each(function(){
            if(noticeData)
            {
                $(this).removeClass();
                $(this).addClass('notice');
                // Type
                if(!noticeData.type) {
                    noticeData.type = 'nosign';
                }
                $(this).addClass(noticeData.type);

                // Size
                if(!noticeData.size) {
                    noticeData.size = '';
                }

                // Here we access class as assoc array instead of object because class is a reserved keyword
                // and IE fails to parse this part correctly.
                $(this).addClass(noticeData['class']);

                // Content
                if(noticeData.message) {
                    $(this).children(':first').html(noticeData.message);
                }
            }
        });
    };
})(jQuery);

/**
 * Disables all actions as paste, autocomplete etc for an input field
 */
(function($){
    $.fn.InputAutocompleteOff = function()
    {
        return this.each(function(){
            $(this)
            .attr('autocomplete', 'off')
            .attr('onCopy', 'return false')
            .attr('onDrag', 'return false')
            .attr('onDrop', 'return false')
            .attr('onPaste', 'return false');
        });
    };
})(jQuery);

/**
 * Border animation
 */
(function($){
    $.fn.animateBorderColor = function(to, duration)
    {
        return this.each(function(){
            $(this).animate({
                'border-color': to
            }, duration, function(){
                $(this).animateBorderColor(to, duration);
            })
        });
    };
})(jQuery);

/**
 *
 */
(function($){
    $.fn.triggerCheckbox = function(check){
        return this.each(function(){
            var checkbox = $(this);
            checkbox.attr('checked', check);
            checkbox.triggerHandler('click');
            checkbox.triggerHandler('change');
        });
    };
})(jQuery);

/**
 *
 */
(function($){
    $.fn.toggleCheckbox = function(){
        return this.each(function(){
            var checkbox = $(this);
            checkbox.attr('checked', !checkbox.is(':checked'));
            checkbox.triggerHandler('click');
            checkbox.triggerHandler('change');
        });
    };
})(jQuery);

/**
 * Changes the cursor when waiting for an ajax response.
 */
function globalAjaxCursorChange()
{
    $("html").bind("ajaxStart", function(){
        $(this).addClass('busy');
    }).bind("ajaxStop", function(){
        $(this).removeClass('busy');
    });
}

/**
 * Help functions
 */

MSL.HasValue = function(param) {
    return typeof(param) != 'undefined';
}

MSL.DefaultValue = function(param, defVal) {
    return MSL.HasValue(param) ? param : defVal;
}

MSL.Redirect = function( loc ) {
    window.location.replace(loc);
}

MSL.Goto = function( loc ) {
    window.location.href = loc;
}

MSL.Refresh = function() {
    window.location.reload(true);
}

MSL.Url = function(action, controller, params) {
    var url = '';
    if(MSL.HasValue(controller)) {
        url = MSL.baseUrl + '/' + controller + '/' + action;
    } else {
        url = MSL.controllerUrl + '/' + action;
    }
    if(MSL.HasValue(params)) {
        url += '?';
        for(var p in params) {
            url += p + '=' + params[p] + '&';
        }
        url = url.substr(0, url.length -1);
    }
    return url;
}

//MSL.Image.Html = function(image) {
//    return '<img src="' + MSL.Image.Url(image) + '" />';
//}

MSL.Image = function(image) {
    return MSL.baseUrl + '/images/' + image;
}

MSL.ThemeImage = function(image) {
    if(MSL.themeUrl === undefined) {
        return MSL.baseUrl + '/images/' + image;
    }
    return MSL.themeUrl + '/images/' + image;
}

MSL.ModuleImage = function(module, image) {
    return MSL.modules[module].assetUrl + '/images/' + image;
}

MSL.LoopedEvent = function( func, interval ) {

    var _interval = interval;
    var _timerId = null;

    this.SetInterval = function(i) {
        _interval = i;
    }
    this.Start = function(startImmediately) {
        startImmediately = MSL.DefaultValue(startImmediately, true);

        if(startImmediately) {
            _timerId = setInterval(func, _interval);
        } else {
            setTimeout(function(){
                _timerId = setInterval(func, _interval);
            }, _interval);
        }
    }
    this.Stop = function() {
        if(_timerId)
            clearInterval(_timerId);
    }
    this.Reset = function() {
        this.Stop();
        this.Start();
    }
}