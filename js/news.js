var News = {    
    le : new MSL.LoopedEvent(function() {
        GetNews();
    }),

    Enable : function(){
        this.le.SetInterval( 3 * 1000 );
        this.le.Start();
    },
    Disable : function(){
        this.le.Stop();
    }
}

GetNews = function(){
    $.ajax({
        url: MSL.Url('indexNews', 'site'),
        type: "POST",
        success: function(data) {
            $(".widgetbox.news .news-container").html(data);
            News.Disable();
        },
        beforeSend: function() {
            $(".loading-image").show();
        },
        error: function() {
            News.Disable();
        }
    })
//    $.ajax({
//        url: MSL.Url('indexTeamNews', 'site'),
//        type: "POST",
//        success: function(data) {
//            $(".widgetbox.news .team-news-container").html(data);
//            News.Disable();
//        },
//        beforeSend: function() {
//            $(".loading-image").show();
//        },
//        error: function() {
//            News.Disable();
//        }
//    })
}

$(document).ready(function(){      
    GetNews();
    News.Enable();
})      