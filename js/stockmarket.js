
var Datatable =
{
    dttbl : null,

    filter : function( val, col, exactMatch )
    {
        if(exactMatch)
            val = (val == "" ? "" : "^" + val + "$");
        Datatable.dttbl.fnFilter( val, col, true );
        updateRowCounter();
    }
};

function updateRowCounter() {
    $('#player-count').html($('#playerList tbody tr').length);
}

$(document).ready(function()
{
    if(MSL.actionId != 'buy' &&
        MSL.actionId != 'sell' &&
        MSL.actionId != 'replace') return;

    // Initialise standard dialogs
    $('#popupWait').dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        draggable: false,
        closeOnEscape: false,
        width: 400,
        create: function() {
            $(this).prev().find('.ui-dialog-titlebar-close').hide();
        },
        open : function() {
            $('.stocksPopupStandard').dialog('close');
            // Hack!!!!
            $('#popupWaitNotice p').css({
                top : ''
            });
            $('#popupWaitNotice').show();
        }
    });

    $('#popupTransactionCompleted').dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        draggable: false,
        closeOnEscape: false,
        width: 400,
        create: function() {
            $(this).prev().find('.ui-dialog-titlebar-close').hide();
        }
    });

    // Init data table player list
    initPlayerList(MSL.actionId);
});

var TableOptions = {
    buy : {
        columnDef : [
        {
            "bVisible": false,
            "aTargets": [6, 7,8]
        },
        {
            "fnRender": function(obj) {
                return CurrencyFormatted(obj.aData[5]);
            },
            "bUseRendered": false,
            "aTargets": [5]
        },
        {
            "bSortable": false,
            "aTargets": [ 0 ]
        }
        ],
        setup : function(obj) {}
    },
    sell : {
        columnDef : [
        {
            "bVisible": false,
            "aTargets": [6, 7,8]
        },
        {
            "fnRender": function(obj) {
                return CurrencyFormatted(obj.aData[5]);
            },
            "bUseRendered": false,
            "aTargets": [5]
        },
        {
            "bSortable": false,
            "aTargets": [ 0 ]
        }
        ],
        setup : function(obj) {}
    },
    replace : {
        columnDef : [
        {
            "bVisible": false,
            "aTargets": [6,7,8]
        },
        {
            "fnRender": function(obj) {
                return CurrencyFormatted(obj.aData[5]);
            },
            "bUseRendered": false,
            "aTargets": [5]
        },
        {
            "bSortable": false,
            "aTargets": [ 0 ]
        }
        ],
        setup : function(obj) {}
    }
};

function initPlayerList(action) {

    // setup url vars for player list search filters
    var filters = {
        name:$.getUrlVar('fname'),
        pos:$.getUrlVar('fpos'),
        team:$.getUrlVar('fteam')
    };
    for(f in filters) {
        if(filters[f])
            filters[f] = decodeURIComponent(filters[f]);
    }

    Datatable.dttbl = $("#playerList table.player-list").dataTable({
        // pagination options
        "bPaginate": false,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 15,
        "bLengthChange": false,
        "aaSorting": [],
        // scroll bar options
        "sScrollY": "400px",

        "bInfo": false,
        'fnInitComplete' : function()
        {
            $(document).trigger('onStockmarketDatatableReady');

            // If filters have values, enable them now
            if(filters.name) $('.filters .name :text').val(filters.name);
            if(filters.pos) $('.filters .position select').val(filters.name);
            if(filters.team) $('.filters .team select').val(filters.name);

            $('.dataTables_filter').remove();

            $('#own-team').click(function(){
                Datatable.filter( ($(this).is(':checked') ? '1' : ''), 7, true );
            });
            $('#hide-inactive').click(function(){
                Datatable.filter( ($(this).is(':checked') ? '1' : ''), 6, true );
            });
            $('#watchlist').click(function(){
                Datatable.filter( ($(this).is(':checked') ? '1' : ''), 8, true );
            });
            $('#show-players-not-playing').click(function(){
                Datatable.filter(($(this).is(':checked') ? $(this).val() : ''), 3, true );
            });

            // Player name filter
            $(document).bind('onStockmarketNameFilter', function(event, value){
                Datatable.filter(value, 1, false);
            });
            // Player position
            $(document).bind('onStockmarketPositionFilter', function(event, value){
                Datatable.filter(value, 2, true);
            });

            // Player's team
            $(document).bind('onStockmarketMatchFilter', function(event, value){
                value = (value == "" ? "" : '(' + value + ')');
                Datatable.filter(value, 3, true);
            });

            $(document).bind('onStockmarketClearFilters', function(){
                // Clear text boxes
                $('.filters :text').val('').keyup();

                // Clear drop down lists
                $('.filters select').val('').change();
            });

            updateRowCounter();
        },
        "aoColumnDefs": TableOptions[action].columnDef,
        "oLanguage": {
            "sProcessing":   "Επεξεργασία...",
            "sLengthMenu":   "Δείξε _MENU_ εγγραφές",
            "sZeroRecords":  "Δεν βρέθηκαν εγγραφές που να ταιριάζουν",
            "sInfo":         "Δείχνοντας _START_ εως _END_ από _TOTAL_ εγγραφές",
            "sInfoEmpty":    "Δείχνοντας 0 εως 0 από 0 εγγραφές",
            "sInfoFiltered": "(φιλτραρισμένες από _MAX_ συνολικά εγγραφές)",
            "sInfoPostFix":  "",
            "sSearch":       "Αναζήτηση:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "Πρώτη",
                "sPrevious": "Προηγούμενη",
                "sNext":     "Επόμενη",
                "sLast":     "Τελευταία"
            }
        }
    });

    $(document).trigger('onStockmarketDatatableReadyEnd');

    // Trigger filters if they exist
    if(filters.name) $('.filters .name :text').trigger('keyup');
    if(filters.pos) $('.filters .position select').val(filters.pos).change();
    if(filters.team) $('.filters .team select').val(filters.team).change();

    // setup url var for clicked player
    var player = $.getUrlVar('player');
    if(IsNumeric(player)) showPopup(player, action);
}

/**
 * Slider class
 */
function Slider( name, maxValue, onChange ) {

    this.name = name;
    this.defaultValue = 0;
    this.maxValue = maxValue;

    var self = this;

    // debug
    if($(this.name).size() == 0) {
        alert('Slider: No html object with name/id "' + name + '"');
        return;
    }

    /**
     * Initial html:
     * <div class='slider' />
     *
     * Final html:
     * <div class='slider'>
     *  <div class='plus' />
     *  <div class='slider-widget' />
     *  <div class='minus' />
     * </div>
     */

    // Add the necessary html
    $(this.name)
    .append('<div class="minus" />')
    .append('<div class="slider-widget" />')
    .append('<div class="plus" />')
    ;

    // private jquery/object
    var slider = $(this.name + ' .slider-widget');

    // Initialise slider
    slider.slider({
        range: "min",
        min: 0,
        max: maxValue,
        value: this.defaultValue,
        slide: function( event, ui ) {
            onChange(ui.value);
        },
        change: function( event, ui ) {
            onChange(ui.value);
        }
    });

    // Bind events for plus/minus one
    $(this.name)
    .children('.plus').click(function(){
        self.plus();
    });
    $(this.name).children('.minus').click(function(){
        self.minus();
    });

    // Gets/Sets the value of the slider
    this.value = function( val ) {
        if(!MSL.HasValue(val)) {
            return slider.slider('value');
        }
        slider.slider('value', val);
        return val;
    }

    // Destroys the slider. Only the initial div remains
    this.destroy = function() {
        $(this.name).empty();
    }

    // Moves slider up by step [default: 1]
    this.plus = function( step ) {
        step = MSL.DefaultValue(step, 1);
        slider.slider('value', this.value() + 1);
    }

    // Moves slider down by step [default: 1]
    this.minus = function( step ) {
        step = MSL.DefaultValue(step, 1);
        slider.slider('value', this.value() - 1);
    }

    this.reset = function() {
        slider.slider('value', this.defaultValue);
    }
}

/**
 * Dialog class
 * Handles all popups used to complete the stockmarket transactions.
 */
function Dialog(name) {

    this.name = name;
    this.selector = '';
    this.playerId = 0;
    this.action = '';
    this.isBuy = true;
    this.player = null;
    this.tip = null;
    var isLoaded = false;
    var isLoading = false;

    this.onCreate = null;
    this.onOpen = null;
    this.onClose = null;
    this.onSubmit = null;
    this.onSliderChange = null;
    this.onLoad = null;

    this.slider = null;

    var dialog = this;

    // Slider getter/initiliser
    this.createSlider = function() {
        if(this.slider) {
            this.slider.destroy();
            delete this.slider;
            this.slider = null;
        }

        // Initialise dialog's slider
        this.slider = new Slider(
            this.selector + ' .slider',
            this.isBuy ? this.player.maxSharesToTransact : this.player.shareCount,
            function(sliderValue){
                setShareCount(sliderValue);
                setTotalPrice(dialog.player.shareValue, sliderValue);
                dialog.onSliderChange(sliderValue);
                $(document).trigger('onStockmarketSliderChange', [sliderValue]);
            });
        setShareCount(0);
        setTotalPrice(this.player.shareValue, 0);
    }

    function setShareCount(shareCount) {
        $(dialog.selector + ' .stock-count').html(shareCount);
    }

    function setTotalPrice(shareValue, shareCount) {
        var price = shareValue * shareCount;

        if(dialog.isBuy) {
            price *= MSL.config.commissionPerc;
        }
        $(dialog.selector + ' .purchase-price').html('€' + CurrencyFormatted(price));

    }

    function child(selector) {
        return $(this.selector + ' ' + selector);
    }

    this.loadPlayer = function(playerToLoad) {
        var selectBox = null;
        $.ajax({
            url: "getPlayer",
            type: "POST",
            data: {
                'player_id' : playerToLoad,
                'player_sell_id' : this.playerId
            },
            success: function(response) {
                if(response.redirect) MSL.Redirect(response.redirect);

                $(dialog.selector + ' form input[name="player_buy_id"]').val(playerToLoad);

                $(dialog.selector + ' .player-info-lesser').html(response.html);

                // setup custom select box click
                $(dialog.selector + ' .with-custom-select-box:visible').append(selectBox);
                $(dialog.selector + ' .max-stocks').html(response.params.maxSharesToTransact);

                dialog.setPlayer({
                    id     : playerToLoad,
                    player : response.params
                }, true);

                // fill the warning label with a message
                dialog.tip.setText(response.params.warningTip);
            },
            beforeSend: function() {
                selectBox = $(dialog.selector + ' .custom-select-box').detach();

                // show slider and transaction info (once)
                $(dialog.selector + ' .slider-info').show();
                $(dialog.selector + ' .transaction-info').show();

                // hide initial select box (once)
                $(dialog.selector + ' .initial-select-box').hide();
            }
        });
    }
    this.setPlayer = function(params, isReplace) {
        // init tip
        if(isReplace) {
            tooltip = '.warn-tooltip-replace-' + this.playerId;
        } else {
            tooltip = '.warn-tooltip-' + this.playerId;
        }
        this.tip = new Tip(tooltip);
        // init player
        this.player = new Player(params.id, params.player);
        // init slider
        this.createSlider();

        this.tip.init($(this.selector + ' .plus').offset());
        this.slider.reset();
    }

    this.close = function() {
        $(this.selector).dialog('close');
    }
    this.open = function(params) {
        if(isLoading) return;
        if(isLoaded) {
            $(this.selector).dialog('open');
            return;
        }
        load(params);
    }

    this.destroy = function(){
        isLoading = false;
        isLoaded = false;
        $(dialog.selector).dialog('destroy');
    }

    function load(params) {
        isLoading = true;

        dialog.playerId = params.playerId;
        dialog.action = params.action;
        dialog.selector = '#' + dialog.name + '-' + dialog.playerId;
        dialog.isBuy = params.action != 'sell';

        dialog.onClose = params.close;
        dialog.onOpen = params.open;
        dialog.onCreate = params.create;
        dialog.onSubmit = params.submit;
        dialog.onSliderChange = params.sliderChange;
        dialog.onLoad = params.load;

        $.ajax({
            url: dialog.name,
            type: "POST",
            data : params.ajaxData,
            success: function(response)
            {
                if(response.redirect) MSL.Redirect(response.redirect);

                $('#popupContainer').append(response.html);

                dialog.onLoad(response);

                // init player transaction dialog
                $(dialog.selector).dialog({
                    autoOpen: true,
                    modal: true,
                    resizable: false,
                    draggable: false,
                    closeOnEscape: params.closeOnEscape,
                    width: params.width,
                    create: function() {
                        dialog.onCreate();

                        $(dialog.selector + ' .transaction-info img[title]').tooltip({
                            position: "center right",
                        });
                    },
                    open : function() {
                        dialog.onOpen();
                        $(document).trigger('onStockmarketDialogOpen', [dialog]);
                    },
                    close: function() {
                        if(dialog.tip) dialog.tip.hide(true);
                        dialog.onClose();
                        $(document).trigger('onStockmarketDialogClose', [dialog]);

                    }
                });

                // init form
                $(dialog.selector + ' form').submit(function(){

                    if(!dialog.onSubmit()) {
                        return false;
                    }

                    $(this).find('input[name="shares"]').val( dialog.slider.value() );
                    Transation($(this).serialize());
                    return false;
                });

                isLoaded = true;
                isLoading = false;
            }
        });
    }
} // class Dialog


DialogPool =  {

    pool : new Array(),
    get  : function(name, playerId) {
        var selector = '#' + name + '-' + playerId;

        for(var dlg in this.pool) {
            if(this.pool[dlg].selector == selector) {
                return this.pool[dlg];
            }
        }

        // append new dialog
        this.pool[this.pool.length] = new Dialog(name);
        return this.pool[this.pool.length-1];
    },
    destroy : function() {
        for(var dlg in this.pool) {
            this.pool[dlg].destroy();
        }
        $('.stocksPopupStandard,.stocksPopupReplace').remove()
    }
}

/**
 *
 */
function Player(id, params) {

    this.id = id;
    // The price of one share of this player
    this.shareValue = params.shareValue;
    // The number of shares the manager currently owns of this player
    this.shareCount = params.shareCount;
    // Maximum number of shares i can purchase/sell
    this.maxSharesToTransact = params.maxSharesToTransact;
    // In case this player is to be sold completely, can it be replaced
    this.canBeReplaced = params.canBeReplaced;
}

/**
 *
 */
function Tip(name) {

    this.name = name;

    var tip = $(this.name);
    var isAnimDone = true;

    this.init = function(relativeObjectOffset) {
        tip.css({
            left: relativeObjectOffset.left + 35,
            top : relativeObjectOffset.top  - 20,
            'z-index' : $('.ui-dialog:visible').css('z-index') // index of the currently visible dialog
        });
    }
    this.show = function() {
        if(!tip.is(':visible')) {
            tip.stop(true, true).show('slide');
        }
    }
    this.hide = function( noEffect ) {
        if(tip.is(':visible') && isAnimDone) {
            isAnimDone = false;
            tip.stop(true, true);

            if(!MSL.DefaultValue(noEffect, false)) {
                tip.hide('slide', {}, 'fast', function(){
                    isAnimDone = true;
                });
            } else {
                tip.hide();
                isAnimDone = true;
            }
        }
    }
    this.setText = function(text) {
        tip.html(text);
    }
}

/**
 * Transaction class
 */
function Transation( data ) {
    $(document).trigger('onStockmarketBeforeTransaction');
    $.ajax({
        url: "transact",
        type: "POST",
        data : data,
        success: function(response) {
            if(response.redirect)
            {
                MSL.Redirect(response.redirect);
                return;
            }

            if(response.success) $('.transaction-result ul li:hidden').show();
            $('#transactionResultMessage').html(response.html);

            $('#popupTransactionCompleted').dialog({
                open : function() {
                    $('#popupWait').dialog('close');
                }
            })
            .dialog('open');
        },
        beforeSend: function() {
            $('#popupWait').dialog('open');
        }
    });
}

/**
 *
 */
//function getPlayer(playerId, playerToLoad)
//{
//    var dialog = DialogPool.get('stocksPopupReplace', playerId);
//    dialog.loadPlayer(playerToLoad)
//}

/**
 *
 */
function showPopup(playerId, action)
{
    var dialog = DialogPool.get('stocksPopup', playerId);
    dialog.open({
        playerId : playerId,
        ajaxData : {
            player_id : playerId,
            action : action
        },
        action : action,
        width : 400,
        closeOnEscape : false,
        create : function() {},
        open : function() {
            dialog.slider.reset();
            dialog.tip.init($(dialog.selector + ' .plus').offset());
        },
        submit : function() {
            if(!dialog.isBuy && dialog.slider.value() == dialog.player.shareCount && dialog.player.canBeReplaced) {
                MSL.Redirect(MSL.Url('replace', 'stockmarket', {
                    'id' : dialog.playerId
                }));
                return false;
            }
            return true;
        },
        sliderChange : function(value) {
            var buttonEnabled = true;
            if(value == 0 || (!dialog.isBuy && !dialog.player.canBeReplaced && value == dialog.player.shareCount && dialog.player.maxSharesToTransact < dialog.player.shareCount)) {
                buttonEnabled = false;
            }
            $(dialog.selector + ' form input:submit').attr('disabled', !buttonEnabled);

            if(((dialog.isBuy && value == dialog.player.maxSharesToTransact) || (!dialog.isBuy && value == dialog.player.shareCount)) && !(!dialog.isBuy && value == dialog.player.maxSharesToTransact)) {
                dialog.tip.show();
            } else {
                dialog.tip.hide();
            }
        },
        load : function(response) {
            dialog.setPlayer({
                id     : dialog.playerId,
                player : response.params
            }, action == 'replace' ? true : false);
        },
        close : function() {}
    });

} // function showPopup()