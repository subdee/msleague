
var clickedForSub = false;
var positions = ['goalkeeper', 'defender', 'midfielder', 'striker'];
var subclassObjects = null;

function cancelSub()
{
    if(!clickedForSub) return;
    $('.pitchplayer')
    .removeClass('disabled selected active')
    .addClass('clickable')
    .unbind('click')
    .click(playerClick)
    ;

    subclassObjects.unbind('click');
    clickedForSub = false;
    $(document).trigger('pitchPlayerCanceled');
}

function subUrl(player_in, player_out)
{
    return MSL.Url('makeSubstitution') + '?in=' + player_in + '&out=' + player_out;
}

function playerClick()
{
    $("#leaderList").dialog("close");

    if(clickedForSub)
    {
        if(!$(this).hasClass('selected')) cancelSub();
    }
    else
    {
        $(document).trigger('pitchPlayerClicked', [$(this)]);
        
        // mark as selected!
        $(this).addClass('selected');

        // find position
        for(var pos_i in positions)
        {
            if(!$(this).hasClass(positions[pos_i])) continue;

            // see if it is substitute or starter
            var isStarter = $(this).hasClass('pitchmain');
            var id = $(this).children('input[name="player_id"]').val();

            {
                var selector = '';
                if(isStarter) selector = '.pitchsub.'+ positions[pos_i];
                else selector = '.pitchmain.'+ positions[pos_i];

                // setup all players that should be available for substitution
                $(selector).addClass('active')
                .unbind('click')
                .click(function(){
                    var other = $(this).children('input[name="player_id"]').val();
                    if(isStarter) {
                        MSL.Goto(subUrl(other, id));
                    } else {
                        MSL.Goto(subUrl(id, other));
                    }
                    return false;
                });
            }
            
            // setup all players that should be inactive
            $('.pitchplayer:not(.selected,.active)')
            .addClass('disabled')
            .removeClass('clickable')
            .unbind('mouseenter mouseleave click')
            .click(cancelSub);

            // setup player just clicked
            $(this).removeClass('clickable')
            .unbind('mouseenter mouseleave click')
            .click(cancelSub);

            subclassObjects.click(cancelSub);
            
            break;
        } // end for

        clickedForSub = true;
    }
    return false;
}

$(document).ready(function()
{
    subclassObjects = $('.pitch-layout .pitch:not(.pitchplayer)');
    $('.pitchplayer').addClass('clickable').click(playerClick);

    $(".leader a").click(function(e) {
        var x = $(this).position().left- 50;
        var y = $(this).position().top - $(document).scrollTop();
        cancelSub();
        $("#leaderList").dialog("option", {
            position: [x, y]
            });
        $("#leaderList").dialog("open");
        return false;
    });

});

