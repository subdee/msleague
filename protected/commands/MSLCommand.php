<?php

class MSLCommand extends CConsoleCommand {

    private $hourNow = null;

    public function actionIndex() {
        if (Gameconfiguration::model()->isSeasonOpen) {
            $startTime = Utils::getUserTime();
            $scriptConfig = ScriptConfiguration::model()->find();

            $this->actionRemoveExpiredCodes();

            if ($this->isTimeToRun($scriptConfig))
                $this->actionPrices();
            if ($this->isTimeToRun($scriptConfig, true))
                $this->actionCalculateActive();
            if ($this->isTimeToRun($scriptConfig, false, true))
                $this->actionSendReminders();
            
            $this->actionSaveTeams();
            
            _log('Script run succesfully.');
        }
    }

    public function actionRemoveExpiredCodes() {
        $t = _transaction();

        try {
            Manager::model()->updateAll(
                array(
                'email_code' => null
                ), 'id IN (SELECT manager_id FROM manager_password_reset WHERE timeout < now() - interval 1 day)'
            );

            ManagerPasswordReset::model()->deleteAll('timeout < now() - interval 1 day');

            $t->commit();
//            _log('Removed expired codes');
        } catch (CDbException $e) {
            $t->rollback();
            _log($e->getMessage());
        }
    }

    public function actionPrices() {
        $gc = Gameconfiguration::model()->find();
        $gc->is_enabled = false;
        if ($gc->update()) {
            if ($this->actionPlayerValues()) {
                if ($this->actionPortfolios()) {
                    $gc->is_enabled = true;
                    if (!$gc->update())
                        _log('Could not reopen stockmarket. Please do so manually.');
                    return true;
                }
            }
        }
        _log('Prices calculation has not occurred because I was unable to close the stockmarket.');
        return false;
    }

    private function actionPlayerValues() {
        $t = _transaction();
        try {
            //Get formula limits
            $config = Gameconfiguration::model()->find();

            //Get formula coefficient
            $coefficient = ScriptConfiguration::model()->get('formula_coefficient');

            //Initialise counter
            $count = 0;

            //Calculate and retrieve bought shares per player
            $db = _app()->db;
            $comm = $db->createCommand(<<<SQL
                select p.id as id, p.current_value as value, ph.bought as bought_old, (gc.total_shares_per_player - p.bank_shares) as bought_now,p.delist_date as delisted, total_points as points
                    from player p left join player_daily_history ph on ph.player_id = p.id, gameconfiguration gc
                    where ph.date = (select max(date) from player_daily_history) and list_date is not null and list_date < now()
                    union
                    select p.id as id, p.current_value as value, 0 as bought_old, (gc.total_shares_per_player - p.bank_shares) as bought_now,p.delist_date as delisted, total_points as points
                    from player p, gameconfiguration gc
                    where list_date is not null and list_date < now() and p.id not in (select player_id from player_daily_history)
SQL
            );
            $bought = $comm->queryAll();

            //Get totals
            $comm2 = $db->createCommand(<<<SQL
                select count(distinct(p.id)) as no_of_players,sum(ph.bought*current_value) as sum_old, sum((gc.total_shares_per_player - p.bank_shares)*current_value) as sum_new
                    from player p left join player_daily_history ph on ph.player_id = p.id, gameconfiguration gc
                    where ph.date = (select max(date) from player_daily_history)
SQL
            );
            $totals = $comm2->queryRow();

            foreach ($bought as $player) {
                //If a player is delisted, keep his old value
                if ($player['delisted'] || strtotime($player['delisted'] > time()))
                    $value = $player['value'];
                else {
                    ++$count;
                    //Calculate price change using formula
                    $chng = (($config->limit_up)
                        /
                        (1 / ($totals['no_of_players'] + 1)) * $coefficient)
                        *
                        ((
                        ($player['bought_now'] * $player['value'])
                        /
                        (1 + $totals['sum_new'])
                        )
                        -
                        (
                        ($player['bought_old'] * $player['value'])
                        /
                        (1 + $totals['sum_old'])
                        ));

                    //If change is more than limit up, reduce to limit up
                    if ($chng > $config->limit_up)
                        $chng = $config->limit_up;
                    if ($chng < -$config->limit_up)
                        $chng = -$config->limit_up;

                    //Apply change to value
                    $value = $player['value'] * (1 + $chng);

                    //If value is higher than max value or lower than min value from config, change value to min/max
                    if ($value < $config->overall_min_price_per_share)
                        $value = $config->overall_min_price_per_share;
                    if ($value > $config->overall_max_price_per_share)
                        $value = $config->overall_max_price_per_share;

                    //If value is positive, round down to nearest hundredth else round up
                    if ($value > $player['value'])
                        $value = floor($value * 100) / 100;
                    elseif ($value < $player['value'])
                        $value = ceil($value * 100) / 100;
                }

                //Write a history record for current price and new bought
                $playerHistory = new PlayerDailyHistory;
                $playerHistory->player_id = $player['id'];
                $playerHistory->value = $player['value'];
                $playerHistory->bought = $player['bought_now'];
                $playerHistory->date = Utils::getUserTime('Y-m-d H:i');
                $playerHistory->save();

                //Update current value for all players with new value, VMI
                Player::model()->updateByPk($player['id'], array(
                    'current_value' => $value,
                    'vmi_season' => round(($player['points'] / $value) * 100, 2)
                ));
            }

            $t->commit();

            _log("Updated $count player values. Time: " . round(Yii::getLogger()->getExecutionTime(), 2) . " s");

            return true;
        } catch (CDbException $e) {
            _log($e->getMessage);
            $t->rollback();
        }
        return false;
    }

    private function actionPortfolios() {
        $t = _transaction();

        try {
            //Initialise counter
            $count = 0;

            //Get all managers
            $managers = Manager::model()->findAll(array(
                'condition' => 'role = :role',
                'params' => array(':role' => Role::MANAGER)
                ));

            foreach ($managers as $manager) {
                if ($manager->managerTeam) {
                    ++$count;
                    $value = 0;

                    //Calculate new total portfolio share value by iterating through players
                    foreach ($manager->managerTeam->managerTeamPlayers as $managerPlayer)
                        $value += $managerPlayer->player->current_value * $managerPlayer->shares;

                    //Write history record with new value
                    $managerHistory = new ManagerDailyHistory;
                    $managerHistory->manager_id = $manager->id;
                    $managerHistory->portfolio_value = $value + $manager->portfolio_cash;
                    $managerHistory->date = Utils::getUserTime('Y-m-d H:i');
                    if (!$managerHistory->save())
                        throw new CValidateException($managerHistory);

                    //Update current value for manager
                    $manager->portfolio_share_value = $value;
                    if (!$manager->update())
                        throw new CValidateException($manager);
                }
            }

            $t->commit();

            _log("Updated $count manager portfolio values. Time: " . round(Yii::getLogger()->getExecutionTime(), 2) . " s");

            return true;
        } catch (CDbException $e) {
            _log($e->getMessage());
            $t->rollback();
        } catch (CValidateException $e) {
            _log($e->getMessage());
            $t->rollback();
        }
        return false;
    }

    public function actionCalculateActive() {
        $t = _transaction();

        try {
            $activeConfig = explode(',', ScriptConfiguration::model()->get('active_options'));
            $burnedConfig = explode(',', ScriptConfiguration::model()->get('burned_options'));
            $stats = Statistics::model()->find('month(convert_tz(date,"UTC",:tz)) = month(:now)', array(':tz' => _app()->localtime->timezone, ':now' => Utils::getUserTime()));
            $days = date('j');

            $criteria = new CDbCriteria;
            $criteria->select = 't.id';
            $criteria->condition = 'convert_tz(t.last_login,"UTC",:tz) BETWEEN DATE_SUB(:now,INTERVAL :activeInterval DAY) AND :now
                AND ((select count(tr.id) from transaction tr where t.id = tr.manager_id and convert_tz(tr.date_completed,"UTC",:tz) BETWEEN DATE_SUB(:now,INTERVAL :activeInterval DAY) AND :now) >= :activeTransactions)';
            $criteria->params = array(
                ':tz' => _app()->localtime->timezone,
                ':now' => Utils::getUserTime(),
                ':activeInterval' => $activeConfig[0],
                ':activeTransactions' => $activeConfig[1]
            );
            $active = Manager::model()->count($criteria);

            //TODO Not sure if needed
            unset($criteria);

            $criteria = new CDbCriteria;
            $criteria->select = 't.id';
            $criteria->join = 'left join manager_team mt on mt.id = t.manager_team_id';
            $criteria->condition = <<<SQL
                (
                    select (count(tr.id)/(datediff(now(),mt.created_on)+1)) from transaction tr left join (manager m2 left join manager_team mt on mt.id = m2.manager_team_id) on m2.id = tr.manager_id where t.id = m2.id
                ) >= :burnedTransactions
SQL;
            $criteria->params[':burnedTransactions'] = $burnedConfig[0];
            Utils::triggerEvent('onCalculateBurned', $this, array('criteria' => $criteria, 'burnedComments' => $burnedConfig[1]));
            $burned = Manager::model()->count($criteria);

            if ($stats) {
                $active = $stats->active + (($active - $stats->active) / $days);
                $burned = $stats->top + (($burned - $stats->top) / $days);
            } else {
                $stats = new Stats;
            }
            $stats->active = (int) $active;
            $stats->top = (int) $burned;
            $stats->date = Utils::getUserTime();
            if (!$stats->save())
                throw new CValidateException($stats);

            $t->commit();

            _log("Calculated active and burned averages. Time: " . round(Yii::getLogger()->getExecutionTime(), 2) . " s");

            return true;
        } catch (CDbException $e) {
            $t->rollback();
            _log($e->getMessage());
        } catch (CValidateException $e) {
            $t->rollback();
            _log($e->getMessage());
        }
        return false;
    }

    public function actionSendReminders() {
        $newsletters = Newsletter::model()->findAll('is_auto AND is_active');
        $footer = Gameconfiguration::model()->get('newsletter_footer');
        $noReply = Gameconfiguration::model()->get('noreply_email');

        foreach ($newsletters as $newsletter) {
            $count = 0;
            $errors = 0;

            $managers = Manager::model()->findAllBySql($newsletter->managerGroup->query);
            foreach ($managers as $manager) {
                $content = Utils::replaceVars($newsletter->content) . $footer;
                if (Utils::sendEmail($manager->email, $newsletter->subject, $content, $noReply))
                    ++$errors;
                ++$count;
            }

            if ($errors)
                _log("Error sending newsletter with id {$newsletter->id} to $count managers.");
            else
                _log("Sent newsletter with id {$newsletter->id} to $count managers.");

            $newsletter->last_sent = Utils::getUserTime();
            $newsletter->update();
        }

        return true;
    }

    public function actionSaveTeams() {
        $isOpen = Game::model()->count(
            'date_played = (select min(date_played) from game where date(convert_tz(date_played,"UTC",:tz)) = date(:now)) AND :now between convert_tz(date_played,"UTC",:tz) - interval 15 minute and convert_tz(date_played,"UTC",:tz)', array(
            ':tz' => _app()->localtime->timezone,
            ':now' => Utils::getUserTime()
            )
        );
        
        if ($isOpen) {
            $t = _transaction();

            try {
                if ($existing = ManagerTeamPlayerHistory::model()->findAll('date(convert_tz(date,"UTC",:tz)) = date(:now)',array(':tz' => _app()->localtime->timezone, ':now' => Utils::getUserTime()))) {
                    foreach ($existing as $old)
                        $old->delete();
                }
                $managerTeams = ManagerTeam::model()->with('manager')->findAll('manager.role = :role', array(':role' => Role::MANAGER));
                foreach ($managerTeams as $managerTeam) {
                    if ($managerTeam->managerTeamPlayers) {
                        foreach ($managerTeam->managerTeamPlayers as $player) {
                            $teamHistory = new ManagerTeamPlayerHistory;
                            $teamHistory->attributes = $player->attributes;
                            $teamHistory->date = Utils::getUserTime();
                            $teamHistory->manager_id = $managerTeam->manager->id;
                            $teamHistory->save();
                        }
                    }
                }

                $t->commit();

                _log("Saved manager teams. Time: " . round(Yii::getLogger()->getExecutionTime(), 2) . " s");

                return true;
            } catch (CDbException $e) {
                $t->rollback();
                _log($e->getMessage());
            } catch (CValidateException $e) {
                $t->rollback();
                _log($e->getMessage());
            }

            return false;
        }
//        _log("Not time to save manager teams. Time: " . round(Yii::getLogger()->getExecutionTime(), 2) . " s");
        return true;
    }

    private function isTimeToRun($scriptConfig, $daily = false, $newsletters = false) {
        if ($this->hourNow == null)
            $this->hourNow = (string) Utils::getUserTime('G:i');

        if ($daily) {
            return $this->hourNow == $scriptConfig->daily_script_time;
        } elseif ($newsletters) {
            return $this->hourNow == $scriptConfig->newsletter_time;
        } else {
            $hours = array();
            for ($i = 0; $i < 24; $i += $scriptConfig->main_script_interval) {
                $hours[] = $i . ':00';
            }

//            $this->hourNow = '11:00';
            return in_array($this->hourNow, $hours);
        }
    }

    public function getHelp() {
        return <<<EOD
USAGE
  yiic msl [action]

DESCRIPTION
  This command will run the necessary scripts for the 
  MSLeague application, in order to do necessary
  automated actions. It also provides a way to
  run each tool manually by calling the corresponding
  action.

  It is recommended that you execute this command under
  the application's protected directory.

PARAMETERS
 * action: optional, you may call a specific action to run from
     the following available actions:
     - prices
     - removeExpiredCodes
     - sendReminders
     - calculateActive
     If you don't define an action, the script will call all
     actions as needed.

EOD;
    }

}

?>
