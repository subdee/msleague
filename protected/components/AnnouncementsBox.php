<?php

class AnnouncementsBox extends WidgetBox {

    public function init() {
        $this->view = 'announcementsBox';
        $this->name = 'announcements';
        $this->title = _t('Announcements');
        $this->more = array(
            'url' => _url('site/announcements'),
            'label' => _t('Announcements'),
        );
        $this->params = array(
            'announcements' => Announcement::model()->getLatest(),
        );
        
        parent::init();
    }

}

?>