<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/main';

    /**
     *
     */
    public function init() {
//        Yii::beginProfile('Controller');
        parent::init();

        // For testing purposes a mandatory reload of the manager can occur.
        if (_app()->params['managerAutoRefresh'] && Utils::isLoggedIn() && !Utils::isAdminUser()) {
            _manager()->refresh();
        }

        if (!Utils::isAdminUser() && Utils::isLoggedIn()) {
            _manager()->last_activity = _app()->localtime->getUTCNow('Y-m-d H:i:s');
            _manager()->saveAttributes(array('last_activity'));
        }
        
        if (Utils::isAdminUser() && Utils::isLoggedIn() && !$this->module) {
            _app()->session->clear();
            $this->redirect(_url('site/index'));
        } elseif ($this->module && $this->module->id == 'admin' && !Utils::isAdminUser() && Utils::isLoggedIn()) {
            _app()->session->clear();
            $this->redirect(AdminUtils::aUrl('site/index'));
        }

        _cs()->registerCoreScript('jquery');
        _cs()->registerCoreScript('jquery.ui');

        // Mimic browser tooltips with our own.
        Utils::registerJsFile('lib/jquery.tools.tooltip.min');
        _cs()->registerScript('mimic-tooltip', '$(".mimic-tooltip[title]").tooltip()');

        _cs()->registerScriptFile(_bu('js/main.js'));

        Yii::$classMap = array_merge(Yii::$classMap, array(
            'CaptchaExtendedAction' => Yii::getPathOfAlias('ext.captchaExtended') . DIRECTORY_SEPARATOR . 'CaptchaExtendedAction.php',
            'CaptchaExtendedValidator' => Yii::getPathOfAlias('ext.captchaExtended') . DIRECTORY_SEPARATOR . 'CaptchaExtendedValidator.php'
        ));

//        Yii::endProfile('Controller');
    }

    /**
     *
     * @param type $action
     * @return type 
     */
    protected function beforeAction($action) {
//        Yii::beginProfile('anyController');

        parent::beforeAction($action);
        Utils::triggerEvent('onVisitPage', $this);

        if (Utils::isLoggedIn() && !Utils::isAdminUser() && !_manager()->accepted_terms && ($action->id != 'termsOfUse' && $action->id != 'logout' && $action->id != 'acceptTerms' && $action->id != 'error')) {
            _user()->returnUrl = _url($action->controller->id . '/' . $action->id);
            $this->redirect(_url('site/termsOfUse'));
        }

        return true;
    }
    
    protected function afterAction($action) {
        parent::afterAction($action);
//        Yii::endProfile('anyController');
    }

    /**
     *
     */
    public function preloadModules() {
        foreach (_app()->getModules() as $name => $module) {
            if (isset($module['autoinit']) && $module['autoinit'] === true) {
                _app()->getModule($name);
            }
        }
    }

}
