<?php

class DebugWindow extends CWidget {

    private $data = null;

    public function init() {
        if (Debug::isDebug() && isset($_SESSION['msl_DebugWindowData'])) {
            $this->data = $_SESSION['msl_DebugWindowData'];
            unset($_SESSION['msl_DebugWindowData']);
        }
    }

    public function run() {
        if ($this->data) {
            $this->render('debugWindow', array('data' => $this->data));
        }
    }

    /**
     *
     * @staticvar int $indent
     * @param type $data
     * @return type 
     */
    protected function debugDialogTree($object) {
        static $indent = 0;

        $output = CHtml::openTag('ul');

        if (is_object($object)) {
            $object = (array) $object;
        }

        foreach ($object as $key => $value) {

            $type = ''; // '(' . gettype($value) . ')';
            $name = '';
            $count = '';
            if (is_object($value)) {
                $type = 'Object';
                $name = get_class($value);
            } elseif (is_array($value)) {
                $type = 'Array';
                $count = '(' . count($value) . ')';
            } elseif (is_bool($value)) {
                $value = $value ? CHtml::tag('span', array('class' => 'true'), 'true') : CHtml::tag('span', array('class' => 'false'), 'false');
            } elseif (is_null($value)) {
                $value = CHtml::tag('span', array('class' => 'null'), 'null');
            } elseif (is_string($value)) {
                $value = '"' . $value . '"';
            }

            if ($key{0} == "\0") {
                $keyParts = explode("\0", $key);
                $key = $keyParts[2];
            }

            if (is_object($value) || is_array($value)) {
                $output .= CHtml::tag('li', array(
                        'class' => 'collapsable',
                        'style' => "padding-left:{$indent}px"
                        ), "[$key] $name $type $count"
                );
                $indent += 40;
                $output .= $this->debugDialogTree($value);
                $indent -= 40;
            } else {
                $output .= CHtml::tag('li', array('style' => "padding-left:{$indent}px"), "[$key] = $type $value");
            }
        }
        $output .= CHtml::closeTag('ul');

        return $output;
    }
}

?>