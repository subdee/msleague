<?php

class EventSystem extends CApplicationComponent {

    /**
     * Navigation
     */
    public function onVisitPage($event) {
        $this->raiseEvent('onVisitPage', $event);
    }

    /**
     * Manager activity
     */
    public function onAssignLeader($event) {
        $this->raiseEvent('onAssignLeader', $event);
    }

    public function onSwapPlayers($event) {
        $this->raiseEvent('onSwapPlayers', $event);
    }

    public function onChangeFormation($event) {
        $this->raiseEvent('onChangeFormation', $event);
    }

    public function onBeforeProcessTeam($event) {
        $this->raiseEvent('onBeforeProcessTeam', $event);
    }

    public function onAfterProcessTeam($event) {
        $this->raiseEvent('onAfterProcessTeam', $event);
    }

    public function onAfterCreateTeamName($event) {
        $this->raiseEvent('onAfterCreateTeamName', $event);
    }

    public function onAfterReportManager($event) {
        $this->raiseEvent('onAfterReportManager', $event);
    }

    public function onManagerDelete($event) {
        $this->raiseEvent('onManagerDelete', $event);
    }

    public function onManagerBeforeFind($event) {
        $this->raiseEvent('onManagerBeforeFind', $event);
    }

    public function onManagerAfterFind($event) {
        $this->raiseEvent('onManagerAfterFind', $event);
    }

    /**
     * Stockmarket
     */
    public function onStockmarketDialogBuy($event) {
        $this->raiseEvent('onStockmarketDialogBuy', $event);
    }

    public function onStockmarketDialogSell($event) {
        $this->raiseEvent('onStockmarketDialogBuy', $event);
    }

    public function onStockmarketDialogReplace($event) {
        $this->raiseEvent('onStockmarketDialogBuy', $event);
    }

    public function onStockmarketTransaction($event) {
        $this->raiseEvent('onStockmarketTransaction', $event);
    }

    public function onStockmarketTransactionBuy($event) {
        $this->raiseEvent('onStockmarketTransaction', $event);
    }

    public function onStockmarketTransactionSell($event) {
        $this->raiseEvent('onStockmarketTransaction', $event);
    }

    public function onStockmarketTransactionReplace($event) {
        $this->raiseEvent('onStockmarketTransaction', $event);
    }

    /**
     * Match
     */
    public function onBeforeMatchDelete($event) {
        $this->raiseEvent('onBeforeMatchDelete', $event);
    }

    /**
     * Layout
     */

    /**
     * Right column
     */
    public function onLayoutRightColumnBegin($event) {
        $this->raiseEvent('onLayoutRightColumnBegin', $event);
    }

    public function onLayoutRightColumnEnd($event) {
        $this->raiseEvent('onLayoutRightColumnEnd', $event);
    }

    /**
     * Manager box
     */
    public function onManagerBoxMenu1($event) {
        $this->raiseEvent('onManagerBoxMenu1', $event);
    }
    
    public function onManagerBoxMenuTop($event) {
        $this->raiseEvent('onManagerBoxMenuTop', $event);
    }

    /**
     * Top menu
     */
    public function onTopMenuEnd($event) {
        $this->raiseEvent('onTopMenuEnd', $event);
    }

    /**
     * Frontpage widgets
     */
    public function onFrontpageBoxLeft1($event) {
        $this->raiseEvent('onFrontpageBoxLeft1', $event);
    }

    public function onFrontpageBoxLeft2($event) {
        $this->raiseEvent('onFrontpageBoxLeft2', $event);
    }

    public function onFrontpageBoxLeft3($event) {
        $this->raiseEvent('onFrontpageBoxLeft3', $event);
    }

    public function onFrontpageBoxLeft4($event) {
        $this->raiseEvent('onFrontpageBoxLeft4', $event);
    }

    public function onFrontpageBoxRight1($event) {
        $this->raiseEvent('onFrontpageBoxRight1', $event);
    }

    public function onFrontpageBoxRight2($event) {
        $this->raiseEvent('onFrontpageBoxRight2', $event);
    }

    public function onFrontpageBoxRight3($event) {
        $this->raiseEvent('onFrontpageBoxRight3', $event);
    }

    public function onFrontpageBoxRight4($event) {
        $this->raiseEvent('onFrontpageBoxRight4', $event);
    }

    /**
     * League match page
     */
    public function onPageLeagueMatchBottom($event) {
        $this->raiseEvent('onPageLeagueMatchBottom', $event);
    }

    /**
     * Match list view
     */
    public function onMatchListViewColumn3($event) {
        $this->raiseEvent('onMatchListViewColumn3', $event);
    }

    /**
     * Profile page
     */
    public function onProfileLeftColumnLinksTop($event) {
        $this->raiseEvent('onProfileLeftColumnLinksTop', $event);
    }

    public function onProfileLeftColumnLinksBottom($event) {
        $this->raiseEvent('onProfileLeftColumnLinksBottom', $event);
    }

    public function onProfileInfoSection($event) {
        $this->raiseEvent('onProfileInfoSection', $event);
    }

    /**
     * Admin
     */
    public function onAdminCommentIcon($event) {
        $this->raiseEvent('onAdminCommentIcon', $event);
    }
    
    /**
     * Scripts
     */
    public function onCalculateBurned($event) {
        $this->raiseEvent('onCalculateBurned', $event);
    }
}

?>
