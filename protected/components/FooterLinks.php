<?php

class FooterLinks extends CWidget {

    private $_startTime;

    public function init() {
        $this->_startTime = microtime(true);
    }

    public function getStartTime() {
        return $this->_startTime;
    }

    public function run() {
        $this->render('footerLinks', array('links' => array(
                array(_url('site/termsOfUse'), _t('Terms Of Use')),
//                array(_url('site/privacyPolicy'), _t('Privacy Policy')),
//                array(_url('site/about'), _t('About ' . _gameName())),
                array(_url('site/contact'), _t('Contact')),
            )));
    }

}

?>