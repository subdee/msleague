<?php

class InformationBar extends CWidget {

    public function run() {

        $latestUpdates = ManagerDailyHistory::model()->getLatestEvents(_user()->id, 4);

        $portfolioChanges = array();
        foreach ($latestUpdates as $lu) {
            
            if ($lu->chng > 0) {
                $class = 'pos';
                $image = Utils::imageUrl('grarr.gif');
            } elseif ($lu->chng < 0) {
                $class = 'neg';
                $image = Utils::imageUrl('redarr.gif');
            } else {
                $class = 'posgray';
                $image = Utils::imageUrl('graydot.gif');
            }
            $portfolioChanges[] = array(
                'time' => _app()->dateFormatter->format('HH:mm', $lu->date),
                'date' => _app()->dateFormatter->format('EEE,d MMM', $lu->date),
                'type' => $class,
                'change' => Utils::percent($lu->chng, true),
                'image' => $image,
            );
        }

        $this->render('informationBar', array(
            'portfolioChanges' => $portfolioChanges,
            'counts' => _team()->numOfPlayersPerPosition(),
            'transactions' => _manager()->getAvailableTransactions(),
            'transactionLowLimit' => 3,
        ));
    }

}

?>
