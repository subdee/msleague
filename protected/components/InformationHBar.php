<?php

class InformationHBar extends CWidget {

    public function run() {

        $latestUpdates = ManagerDailyHistory::model()->getLatestEvents(_user()->id, 4);

        $portfolioChanges = array();
        foreach ($latestUpdates as $lu) {
            $portfolioChanges[] = array(
                'time' => _app()->dateFormatter->format('HH:mm', $lu['date']),
                'date' => _app()->dateFormatter->format('EEE,d MMM', $lu['date']),
                'change' => $lu['chng'],
            );
        }

        _cs()->registerScriptFile(_bu('js/lib/jquery.tools.tooltip.min.js'));

        $this->render('informationHBar', array(
            'portfolioChanges' => $portfolioChanges,
            'players' => _team()->numOfPlayers(),
            'playerCounts' => _team()->numOfPlayersPerPosition(),
            'transactions' => _manager()->getAvailableTransactions(),
            'transactionLowLimit' => 3,
        ));
    }

}

?>
