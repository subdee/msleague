<?php

class LoginBox extends WidgetBox {

    public function init() {
        $this->view = 'loginBox';
        $this->place = 'sidebar';
        $this->title = _t('Login');
        $this->params = array(
            'model' => new LoginForm,
        );

        parent::init();
    }

}

?>