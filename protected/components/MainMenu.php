<?php

/**
 * Specificaitons:
 *
 * Must support:
 * label (optional [value callback]) default=none
 * url (optional) default=none
 * realUrl (optional) default=_url(this->url)
 * image (optional) default=none
 * visible (optional condition [boolean|expression]) default=true
 * enabled (optional condition [boolean|expression]) default=true
 * active (optional condition [boolean|expression]) NOTE: maybe this must be automatic. default=false. if active then force visible true
 * showItems (optional condition [boolean|expression]) default=true
 * subitems
 */
class MainMenu extends CWidget {

    /**
     * @var array The menu items to actually render.
     */
    private $_menu;

    /**
     * @var int The level of the menu to show.
     */
    public $showLevel = -1;

    /**
     * @var string
     */
    public $cssClassActive = 'selected';

    /**
     * @var string
     */
    public $cssClassDisabled = 'disabled';

    /**
     * @var string
     */
    public $cssClassLiExpression = null;

    /**
     * @var string
     */
    public $cssClassUl = 'menu';

    /**
     *
     */
    public function init() {
        $this->_menu = array(
            array(
                'label' => _t('Stockmarket'),
                'url' => 'stockmarket/index',
                'image' => Utils::imageUrl('mainMenu/stockmarket.png'),
                'visible' => Utils::isLoggedIn() && _manager()->hasRole(Role::MANAGER | Role::RESET),
                'items' => array(
                    array(
                        'label' => _t('Buy shares'),
                        'url' => 'stockmarket/buy',
                        'enabled' => Utils::isLoggedIn() && Gameconfiguration::model()->isOpen() && _manager()->hasRole(Role::MANAGER),
                    ),
                    array(
                        'label' => _t('Sell shares'),
                        'url' => 'stockmarket/sell',
                        'enabled' => Utils::isLoggedIn() && Gameconfiguration::model()->isOpen() && _manager()->hasRole(Role::MANAGER),
                    ),
                    array(
                        'label' => _t('Replace'),
                        'active' => _app()->controller->route == 'stockmarket/replace',
                    ),
            )),
            array(
                'label' => _t('Players'),
                'url' => 'players/index',
                'image' => Utils::imageUrl('mainMenu/players.png'),
                'items' => array(
                    array(
                        'label' => _t('Stocks'),
                        'url' => 'players/index',
                    ),
                    array(
                        'label' => _t('Statistics'),
                        'url' => 'players/stats',
                    ),
                    array(
                        'label' => _t('Watchlist'),
                        'url' => 'managers/watchList',
                        'visible' => Utils::isLoggedIn() && _manager()->hasRole(Role::MANAGER | Role::RESET),
                    ),
                    array(
                        'label' => _t('Player profile'),
                        'active' => _app()->controller->route == 'players/view',
                    ),
            )),
            array(
                'label' => _t('Standings'),
                'url' => 'standings/index',
                'image' => Utils::imageUrl('mainMenu/standings.png'),
            ),
            array(
                'label' => _t('News'),
                'url' => 'site/news',
                'image' => Utils::imageUrl('mainMenu/news.png'),
            ),
            array(
                'label' => _t('Matches'),
                'url' => 'match/index',
                'image' => Utils::imageUrl('mainMenu/league.png'),
                'items' => array(
                    array(
                        'label' => _t('Matchdays'),
                        'url' => 'match/index',
                    ),
                    array(
                        'label' => _t('By team'),
                        'url' => 'match/byteam',
                    ),
                    array(
                        'label' => _t('Match'),
                        'active' => _app()->controller->route == 'match/match'
                    ),
                ),
            ),
            array(
                'label' => _t('Game Guide'),
                'url' => 'instructions/index',
                'image' => Utils::imageUrl('mainMenu/instructions.png'),
                'items' => array(
                    array(
                        'label' => _t('Instructions'),
                        'url' => 'instructions/index',
                    ),
                    array(
                        'label' => _t('FAQ'),
                        'url' => 'instructions/faq',
                    ),
                    array(
                        'label' => _t('Screenshots'),
                        'url' => 'instructions/screenshots',
                    ),
                ),
            ),
            array(
                'label' => _t('Announcements'),
                'url' => 'site/announcements',
                'image' => Utils::imageUrl('mainMenu/announcements.png'),
            ),
            array(
                'label' => _t('Prizes'),
                'url' => 'site/awards',
                'image' => Utils::imageUrl('mainMenu/prizes.png'),
            ),
            array(
                'visible' => false,
                'items' => array(
                    array(
                        'label' => _t('Preferences'),
                        'url' => 'settings/index',
                    ),
                    array(
                        'label' => _t('Change email'),
                        'url' => 'settings/email',
                    ),
                    array(
                        'label' => _t('Change password'),
                        'url' => 'settings/password',
                    ),
                    array(
                        'label' => _t('Blocked users'),
                        'url' => 'settings/blockedUsers',
                        'visible' => Utils::isLoggedIn() && _manager()->hasRole(Role::MANAGER | Role::RESET),
                    ),
                    array(
                        'label' => _t('Delete account'),
                        'url' => 'settings/deleteAccount',
                        'visible' => _isModuleOn("deleteAccount"),
                    ),
                ),
            ),
        );

        $this->_activateActions($this->_menu, _app()->controller->route);
    }

    /**
     *
     */
    public function run() {
        $this->render('mainMenu');
    }

    /**
     * @return array The menu items.
     */
    public function getItems() {
        return $this->_menu;
    }

    /**
     * @param array $item The menu item to push at the end of the list.
     */
    public function pushItem($item) {
        $this->_menu[] = $item;
    }

    /**
     * @param int $level The menu level to render
     */
    public function renderLevel($level) {
        $items = $this->_getLevel($this->_menu, $level);

        if ($items !== null) {
            $this->render('_menuLevel', array(
                'level' => $level,
                'items' => $items,
            ));
        }
    }

    /**
     *
     * @param type $items
     * @param type $level
     * @param type $current
     */
    private function _getLevel($items, $level, $current = 0) {

        if ($level == $current)
            return $items;

        // For level greater than 1, we must render the submenu of the active parent item.
        foreach ($items as $item) {

            // Is item active?
            if (!$this->is($item, 'active'))
                continue;

            // Has item children?
            if (!isset($item['items']))
                continue;

            return $this->_getLevel($item['items'], $level, $current + 1);
        }

        return null;
    }

    /**
     * Sets as active items that either have at least one active child or their url matches exactly.
     * @param string $route
     * @param int $level
     * @return boolean True if at least one action was activated
     */
    private function _activateActions(&$items, $route, $level = 0) {

        $foundActiveAction = false;
        foreach ($items as &$item) {

            // Default enabled
            if (!isset($item['enabled']))
                $item['enabled'] = true;
            else {
                if (is_string($item['enabled']))
                    $item['enabled'] = eval("return {$item['enabled']};");
            }

            // Default show children
            if (!isset($item['showItems']))
                $item['showItems'] = true;
            else {
                if (is_string($item['showItems']))
                    $item['showItems'] = eval("return {$item['showItems']};");
            }

            // Default active
            if (!isset($item['active']))
                $item['active'] = false;
            else {
                if (is_string($item['active']))
                    $item['active'] = eval("return {$item['active']};");

                if (!isset($item['visible'])) {
                    $item['visible'] = $item['active'];
                }
            }

            // Set visible
            if (!isset($item['visible']))
                $item['visible'] = true;
            else {
                if (is_string($item['visible']))
                    $item['visible'] = eval("return {$item['visible']};");
            }

            // Set item class
            $item['cssClassLi'] = '';
            if ($this->cssClassLiExpression !== null)
                $item['cssClassLi'] = eval("return {$this->cssClassLiExpression};");

            if (!$item['active']) {

                if (!$item['enabled']) {
                    $item['cssClassLi'] .= ' ' . $this->cssClassDisabled;
                    continue;
                }

                // If item has not url, cannot be active.
                if (!isset($item['url']))
                    continue;

                // Visit item's children.
                $foundActiveChild = false;
                if (isset($item['items'])) {
                    $foundActiveChild = $this->_activateActions($item['items'], $route, $level + 1);
                }

                // Activate
                if ($foundActiveChild) {
                    $item['active'] = true;
                } else {
                    if ($item['url'] == $route) {
                        $item['active'] = true;
                        $foundActiveAction = true;
                    }
                }

                // Generate real url for anchor
                $item['realUrl'] = _url($item['url']);
            } else {
                $foundActiveAction = true;
            }

            if ($item['active']) {
                $item['cssClassLi'] .= ' ' . $this->cssClassActive;
            }
        }

        return $foundActiveAction;
    }

    /**
     *
     * @param array $item
     * @param string $attribute
     * @return boolean
     */
    public function is($item, $attribute) {
        return isset($item[$attribute]) && $item[$attribute] === true;
    }

}
?>