<?php

class ManagerBox extends WidgetBox {

    public function init() {
        _manager()->refreshAttributes(array('total_points'));

        $this->view = 'managerBox';
        $this->name = 'manager';
        $this->place = 'sidebar';
        $this->title = _manager()->hasRole(Role::MANAGER | Role::RESET) ? _manager()->profileLink() : _manager()->username;
        $this->titleImage = Utils::imageUrl('managerBox/title.png');
        $this->params = array(
            'points' => _manager()->total_points,
        );

        parent::init();
    }
}

?>