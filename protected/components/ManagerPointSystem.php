<?php

/**
 * This is a general class with no agenda. Its purpose is to serve other systems 
 * that want to take advantage of the point/achievement system as well as keep 
 * track of manager's advancement.
 */
class ManagerPointSystem extends CApplicationComponent {

    /**
     *
     * @param ManagerTask $task
     */
    public function connect(ManagerTask $task) {
        Utils::registerCallback($task->desc->event, $task, 'onEventNotify');
    }

    /**
     *
     * @param array $data 
     */
    public function addTaskDescriptions($descriptions) {
        foreach ($descriptions as $data) {
            if (isset($data['class'])) {
                $data = array_merge($data, call_user_func($data['class'] . '::description'));
            }

            $desc = new ManagerTaskDesc;
            $desc->name = $data['name'];
            $desc->points = $data['points'];
            $desc->event = $data['event'];
            if (isset($data['class']))
                $desc->class = $data['class'];
            if (isset($data['meta']))
                $desc->meta = $data['meta'];
            if (!$desc->save())
                throw new CValidateException($desc);
        }
    }

    /**
     *
     * @param int $manager_id
     * @param array $tasks 
     */
    public function assignTasks($manager_id, $context, $tasks) {
        $rank = 1;
        foreach ($tasks as $data) {
            if (isset($data['class'])) {
                $data = array_merge($data, call_user_func($data['class'] . '::setup'));
            }

            $desc = ManagerTaskDesc::model()->findByAttributes(array('name' => $data['name']));

            $task = new ManagerTask;
            $task->task_id = $desc->id;
            $task->manager_id = $manager_id;
            if (isset($data['rank']))
                $task->rank = $data['rank'];
            else {
                $task->rank = $rank;
                $rank++;
            }
            if (isset($data['meta']))
                $task->meta = $data['meta'];
            $task->context = $context;
            if (!$task->save())
                throw new CValidateException($task);
        }
    }

}

?>
