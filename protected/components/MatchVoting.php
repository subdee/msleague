<?php

class MatchVoting extends CWidget {

    public $game;
    public $hasVoted = false;
    public $hasMatchStarted = false;
    public $votes;
    public $totalVotes;

    public function init() {
        if (!($this->game instanceof Game))
            $this->game = Game::model()->findByPk($this->game);
        
        if (ManagerMatchVote::model()->exists('manager_id = :id and game_id = :gid',array(
            ':id' => _manager()->id,
            ':gid' => $this->game->id
        )))
            $this->hasVoted = true;
        
        if (Game::hasStarted($this->game))
            $this->hasMatchStarted = true;
        
        if (_manager()->isBanned() || $this->hasMatchStarted || $this->hasVoted)
            $this->votes = ManagerMatchVote::model()->getVotes($this->game->id);
    }

    public function run() {
        $this->render('matchVoting');
    }

}

?>
