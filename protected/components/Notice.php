<?php
class Notice extends CWidget
{
    // component id if any
    public $id = '';

    // default message to display. this param should always be set.
    public $message = '';
    
    // [nosign], info, question, warning, error, failed, success, loader
    public $type = 'nosign';

    // [false], true
    public $closable = false;

    // conditional notice
    public $condition = true;

    // session name to use if any
    public $session = '';

    // render hidden for client side use
    public $ajax = false;
    
    // extra user classes
    public $class = '';

    private $genericClass = '';
    
	public function init()
    {
        if(!empty($this->session) && isset($_SESSION[$this->session]))
        {
            foreach($_SESSION[$this->session] as $key => $value)
            {
                $this->$key = $value;
            }
        }
        
        $this->genericClass = 'notice ';
        $this->genericClass .= $this->type;
        if( $this->closable ) $this->genericClass .= ' closable';
        $this->genericClass .= ' '.$this->class;
    }

    public function run()
    {
        if(!$this->condition)
        {
            return;
        }
        
        if(!empty($this->session))
        {
            if(isset($_SESSION[$this->session]))
            {
                unset($_SESSION[$this->session]);
            }
            elseif($this->ajax)
            {
                $this->genericClass .= ' hidden';
            }
            else
            {
                return;
            }
        }
        elseif($this->ajax)
        {
            $this->genericClass .= ' hidden';
        }
        
        $this->render('notice', array('class' => $this->genericClass));
    }
}
?>
