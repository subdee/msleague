<?php

class NotificationDialog extends CWidget {

    public $obligatory = false;
    public $title = null;
    public $content = null;
    
    public function init() {
        
    }

    public function run() {
        $this->render('notificationDialog');
    }

}
?>