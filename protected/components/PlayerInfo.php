<?php

class PlayerInfo extends CWidget {

    public $player;
    public $change;
    public $ratingCount;
    public $showExtraDetails = false;
    public $st = array();
    public $bought;
    public $nextMatch;
    public $loSt;

    public function init() {
        if ($this->player) {
            $playerLastDaily = PlayerDailyHistory::model()->find('player_id = :plId order by date desc', array(':plId' => $this->player->id));

            if ($playerLastDaily) {
                $this->change = (($this->player->current_value - $playerLastDaily->value) / $playerLastDaily->value);
            } else {
                $this->change = 0;
            }

            $this->ratingCount = ManagerPlayerRating::model()->count('player_id = :plId', array(':plId' => $this->player->id));

            $this->bought = $playerLastDaily->bought;

            $criteria = new CDbCriteria;
            $criteria->select = <<<SQL
            p.team_id as player_team_id,
            t.*
SQL;

            $criteria->join = <<<SQL
            left join player p 
                   on (p.team_id = t.team_home or p.team_id = t.team_away)
SQL;

            $criteria->addCondition('p.id = :id');
            $criteria->addCondition('t.date_played > now()');

            $criteria->params[':id'] = $this->player->id;
            $criteria->order = 't.date_played asc';
            $criteria->limit = 1;
            $this->nextMatch = Game::model()->find($criteria);

            //Get the stock values of a player and create a line chart
            $stocks = PlayerDailyHistory::model()->getStocks($this->player->id);
            if (!$stocks) {
                $stocks = Player::model()->findByPk($this->player->id);
                $this->loSt = $stocks->current_value;
                $this->st['date'][0] = date('d-M');
                $this->st['value'][0] = (float) $stocks->current_value;
            } else {
                $i = 0;
                $this->loSt = 100000;
                foreach ($stocks as $stock) {
                    if ($stock->value < $this->loSt)
                        $this->loSt = $stock->value;
                    $this->st['date'][$i] = _app()->dateFormatter->format('d/M', $stock->date);
                    $this->st['value'][$i] = (float) $stock->value;
                    $i++;
                }
                if ($this->player->current_value < $this->loSt)
                    $this->loSt = $this->player->current_value;
                $this->st['date'][$i] = _app()->dateFormatter->format('d/M', date('d-M'));
                $this->st['value'][$i] = (float) $this->player->current_value;
            }
        }
    }

    public function run() {
        $this->render('playerInfo');
    }

}

?>