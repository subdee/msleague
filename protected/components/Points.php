<?php

class Points extends CWidget {

    public $manager_id = 0;
    public $date = 0;
    private $profile = null;
    private $basicPlayers;
    private $subPlayers;

    public function init() {

        // Setup credentials
        if (!$this->manager_id && isset($_GET['id'])) {
            $this->manager_id = $_GET['id'];
            if (!Manager::model()->exists('id = :id', array(':id' => $this->manager_id))) {
                _home();
            }
        }

        //get all dates from manager's point history


        if (isset($_GET['date'])) {
            $this->date = $_GET['date'];
            $this->profile = ManagerPointHistory::model()->find(array(
                    'condition' => 'manager_id = :id and date(convert_tz(date,"UTC",:tz)) = date(:date)',
                    'order' => 'date desc',
                    'params' => array(':id' => $this->manager_id,':date'=>$_GET['date'],':tz'=>_timezone()),
                    'select' => 'date as dateOnly, total_points, manager_id'
                ));
        } else {
            $this->profile = ManagerPointHistory::model()->find(array(
                    'condition' => 'manager_id = :id',
                    'order' => 'date desc',
                    'params' => array(':id' => $this->manager_id),
                    'select' => 'date as dateOnly, total_points, manager_id'
                ));
            if ($this->profile != null)
                $this->date = $this->profile->dateOnly;
            else
                $this->date = _app()->localtime->getGameNow('Y-m-d H:i:s');
        }

        // setup main players
        $this->basicPlayers = new ManagerTeamPlayerHistory('search');
        $this->basicPlayers->date = $this->date;
        $this->basicPlayers->manager_id = $this->manager_id;
        $s = Status::model()->findAll(array('condition' => 'status in (\'Starter\',\'Leader\')', 'select' => 'id'));
        foreach ($s as $stat)
            $status[] = $stat->id;
        $this->basicPlayers->stati = $status;

        // setup bench
        $this->subPlayers = new ManagerTeamPlayerHistory('search');
        $this->subPlayers->date = $this->date;
        $this->subPlayers->manager_id = $this->manager_id;
        $this->subPlayers->status_id = Status::model()->find('status = \'Substitute\'')->id;
    }

    public function profile() {
        return $this->profile;
    }

    public function basicPlayers() {
        return $this->basicPlayers;
    }

    public function subPlayers() {
        return $this->subPlayers;
    }

    public function getDate() {
        return $this->date;
    }

    public function run() {
        $this->render('points', array(
            'dateList' => ManagerPointHistory::model()->findAll(array(
                'condition' => 'manager_id = :id',
                'order' => 'date desc',
                'select' => 'date as dateOnly',
                'params' => array(':id' => $this->manager_id)
            ))
        ));
    }

}

?>
