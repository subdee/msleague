<?php

class StockmarketFilters extends CWidget {

    public $matches;
    public $matchRegex = '';

    public function init() {
        $this->matches = Game::model()->getNextMatchday();

        if(_action_id() == 'sell') {
            $this->matchRegex = '^((?!';
            // Regex to exclude players not belonging to any of the teams playing next.
            foreach($this->matches as $m) {
                $this->matchRegex .= $m->teamHome0->shortname5 . '$|' . $m->teamAway0->shortname5 . '$|';
            }
            $this->matchRegex = mb_substr($this->matchRegex, 0, -1);
            $this->matchRegex .= ').)*$';
        }
    }

    public function run() {
        $this->render('stockmarketFilters');

        $s = 'PAO,AEK,PAOK';
    }

}

?>