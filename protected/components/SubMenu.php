<?php

class SubMenu extends CWidget {

    public $menuItems = array();

    public function init() {

        if (!method_exists(_controller(), 'subMenuItems'))
            return;

        $menu = _controller()->subMenuItems();
        foreach ($menu as $m) {

            $success = true;
            // check any restrictions we may have
            if (isset($m['condition'])) {

                if (is_string($m['condition'])) {
                    // Eval expression
                    $success = eval("return {$m['condition']};");
                } elseif (is_bool($m['condition'])) {
                    // Boolean condition
                    $success = $m['condition'];
                } elseif (is_array($m['condition'])) {
                    // Model method call
                    $success = call_user_func("{$m['condition'][0]}::model")->{$m['condition'][1]}();
                }
                else
                    throw new CException('Invalid type of condition for submenu');
            }
            if (isset($m['expression'])) {
                $success = eval("return {$m['expression']};");
            }
            if (!$success) {
                // If condition failed, by default we hide the option.
                // To handle it differently add your handling code below.
                if (isset($m['onfail'])) {
                    switch ($m['onfail']) {
                        case 'disable':
                            $this->menuItems[] = array(
                                'title' => $m[1],
                                'class' => 'disabled',
                            );
                            break;
                    }
                }
                continue;
            }

            // url should take care of module/controller/view
            $url = '';
            if (_module())
                $url = _module_id() . '/';
            if(isset($m['controller']))
                $url .= $m[0];
            else
                $url .= _controller_id() . '/' . $m[0];

            // there may be more than one action at the same menu item
            $class = _app()->controller->action->id == $m[0] ? 'selected' : '';
            if (!$class && isset($m[2])) {
                foreach ($m[2] as $x) {
                    if (_app()->controller->action->id == $x) {
                        $class = 'selected';
                        break;
                    }
                }
            }

            $item = array(
                'title' => $m[1],
                'class' => $class,
            );

            if ($class != 'selected')
                $item['url'] = $url;

            $this->menuItems[] = $item;
        }
    }

    public function run() {
        if (count($this->menuItems) > 0) {
            $this->render('subMenu', array('menu' => $this->menuItems));
        }
    }

}

?>
