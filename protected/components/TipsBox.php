<?php

class TipsBox extends WidgetBox {

    public $tips = array();
    private $_cat1 = array();
    private $_cat2 = array();
    private $_cat3 = array();

    public function init() {
        $dependency = new CDbCacheDependency('SELECT MAX(last_updated) FROM tips');
        $tips = Tips::model()->cache(2592000, $dependency)->findAll();

        if ($tips != null) {

            $t1 = $t2 = $t3 = false;

            foreach ($tips as $tip) {
                switch ($tip->tip_group) {
                    case 1:
                        $this->_cat1[] = $tip->tip;
                        break;
                    case 2:
                        $this->_cat2[] = $tip->tip;
                        break;
                    case 3:
                        $this->_cat3[] = $tip->tip;
                        break;
                    default:
                        break;
                }
            }

            if (!empty($this->_cat1))
                $t1 = $this->_cat1[rand(0, count($this->_cat1) - 1)];
            if (!empty($this->_cat2))
                $t2 = $this->_cat2[rand(0, count($this->_cat2) - 1)];
            if (!empty($this->_cat3))
                $t3 = $this->_cat3[rand(0, count($this->_cat3) - 1)];

            $this->tips = array($t1, $t2, $t3);
        }

        $this->place = 'sidebar';
        $this->view = 'tipsBox';
        $this->title = _t('Tips');
        $this->name = 'tips';

        parent::init();
    }

}

?>