<?php

class TopMenu extends CWidget {

    /**
     * @var array The pool of menu items.
     */
    private $_pool;

    /**
     * @var array The menu items to actually render.
     */
    private $_menu;

    /**
     *
     */
    public function init() {

        // ARG!! make menu item pool OLO MALAKIES KANEIS
        $this->_pool = array(
            array('url' => 'site/index', 'img' => 'mb1', 'title' => _t('Home page')),
            array('url' => 'stockmarket/index', 'img' => 'stockmarket', 'title' => _t('Stockmarket')),
            array('url' => 'players/index', 'img' => 'players', 'title' => _t('Players')),
            array('url' => 'standings/index', 'img' => 'standings', 'title' => _t('Standings')),
            array('url' => 'site/news', 'img' => 'news', 'title' => _t('News')),
            array('url' => 'match/index', 'img' => 'league', 'title' => _t('Matches')),
            array('url' => 'instructions/index', 'img' => 'instructions', 'title' => _t('Game Guide')),
            array('url' => 'site/announcements', 'img' => 'announcements', 'title' => _t('Announcements')),
            array('url' => 'site/awards', 'img' => 'prizes', 'title' => _t('Prizes')),
        );

        if (_user()->isGuest || _manager()->hasRole(Role::TEMPORARY)) {
            $this->_menu = array(
                $this->_pool[6],
                $this->_pool[8],
                $this->_pool[3],
                $this->_pool[2],
                $this->_pool[5],
                $this->_pool[4],
            );
        } else if (_manager()->hasRole(Role::RESET)) {
            $this->_menu = array(
                $this->_pool[1],
                $this->_pool[6],
                $this->_pool[8],
                $this->_pool[3],
                $this->_pool[2],
                $this->_pool[5],
                $this->_pool[4],
            );
        } else {
            $this->_menu = array(
                $this->_pool[1],
                $this->_pool[2],
                $this->_pool[3],
                $this->_pool[4],
                $this->_pool[5],
                $this->_pool[7],
                $this->_pool[8],
            );
        }

        Utils::triggerEvent('onTopMenuEnd', $this);

        $subMenu = null;
        if (method_exists(_controller(), 'subMenuItems')) {
            $subMenu = _controller()->subMenuItems();
        }

        foreach ($this->_menu as &$m) {
            $selected = false;
            if (isset($m['url'])) {

                $route = str_replace('default/', '', _controller()->route);
                if ($route == $m['url']) {
                    $selected = true;
                } else {
                    $url = explode('/', $m['url']);
                    if (_controller_id() == $url[0]) {
                        if ($subMenu) {
                            foreach ($subMenu as $sm) {
                                if ($sm[0] == _action_id()) {
                                    $selected = true;
                                }
                            }
                        }
                    }
                }
            }

            $m['class'] = '';
            if ($selected) {
                $m['class'] = 'selected';
            }
        }
    }

    /**
     *
     */
    public function run() {
        $this->render('topMenu');
    }

    /**
     * @return array The menu items.
     */
    public function getItems() {
        return $this->_menu;
    }

    /**
     * @param array $item The menu item to push at the end of the list.
     */
    public function pushItem($item) {
        $this->_menu[] = $item;
    }

}

?>