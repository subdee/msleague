<?php

class TopNav extends CWidget {

    private $_noManagers;
    private $_isOpen;
    private $_showSocial = true;
    private $_showInfo = true;

    public function init() {
        if (!_user()->isGuest && !_manager()->hasRole(Role::TEMPORARY)) {
            $cr = new CDbCriteria;
            $cr->addInCondition('role', array(Role::MANAGER, Role::RESET));
            $this->_noManagers = Manager::model()->count($cr);
            $this->_isOpen = Gameconfiguration::model()->isOpen();
        } else {
            $this->_showInfo = false;
        }
        if (Debug::isDebug()) {
            $this->_showSocial = false;
        }
    }

    public function getNoOfManagers() {
        return $this->_noManagers;
    }

    public function getIsOpen() {
        return $this->_isOpen;
    }

    public function getShowSocial() {
        return $this->_showSocial;
    }

    public function getShowInfo() {
        return $this->_showInfo;
    }

    public function run() {
        $this->render('topNav');
    }

}

?>
