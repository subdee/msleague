<?php

/**
 * 
 */
class UrlManager extends CUrlManager
{
    /**
     * 
     */
    public function init()
    {
        $this->addCustomRules();
        parent::init();
    }

    /**
     *
     */
    private function addCustomRules()
    {
        $arrModule = Yii::app()->modules;

        $new = array();
        foreach ($arrModule as $strModule => $arrItem)
        {
            if($strModule == 'gii') continue;
            $new["$strModule/<action:\w+>"] = "$strModule/default/<action>";
        }
        $this->addRules($new);
    }
}

?>
