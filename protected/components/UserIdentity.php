<?php
/**
* UserIdentity represents the data needed to identity a user.
* It contains the authentication method that checks if the provided
* data can identify the user.
*/
class UserIdentity extends CUserIdentity
{
    private $_id;
    private $_username;
    public $admin;
    public $user;

    public function __construct($username, $password, $admin=false) {
        parent::__construct($username, $password);
        $this->admin = $admin;
    }

    /**
    * Authenticates a user using the User data model.
    * @return boolean whether authentication succeeds.
    */
    public function authenticate()
    {
        if ($this->admin)
            $user = User::model()->findByAttributes(array('username' => $this->username));
        else
            $user = Manager::model()->findByAttributes(array('username'=>$this->username));
        if(!$user || $user->role == Role::INACTIVE)
        {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        }
        else
        {
            if($user->password !== $user->encrypt($this->password))
            {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
            }
            else
            {
                $this->_id = $user->id;
                if ($this->admin) {
                    $this->setUser($user);
                    _app()->localtime->TimeZone = _timezone();
                } else {
                    if(!$user->last_login)
                    {
                        $lastLogin = _app()->localtime->getLocalNow('Y-m-d H:i:s');
                    }
                    else
                    {
                        $lastLogin = strtotime($user->last_login);
                    }
                    $user->last_login = _app()->localtime->getLocalNow('Y-m-d H:i:s');
                    $user->update();

                    $this->setState('manager', $user);
                }

                $this->errorCode=self::ERROR_NONE;
            }
        }

        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(CActiveRecord $user)
    {
        $this->user=$user->attributes;
    }
}