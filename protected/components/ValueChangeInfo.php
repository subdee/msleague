<?php

class ValueChangeInfo extends CWidget {

    /**
     *
     * @var type 
     */
    public $change;

    /**
     *
     * @var type 
     */
    public $type = '';

    /**
     *
     * @var type 
     */
    public $template = '{change}';
    
    /**
     *
     * @var type 
     */
    private $percent;

    /**
     *
     * @var type 
     */
    private $class = 'value-change-info';

    public function init() {
        $this->percent = Utils::percent($this->change,true);
        $chng = $this->change * 100;
        if ($chng > 0) {
            $this->class .= ' positive ' . $this->type;
        } elseif ($chng < 0) {
            $this->class .= ' negative ' . $this->type;
        } else {
            $this->class .= ' neutral ' . $this->type;
        }
        
        $this->template = str_replace('{change}', $this->percent, $this->template);
    }

    public function run() {
        $this->render('valueChangeInfo', array(
            'percent' => $this->percent,
            'class' => $this->class
        ));
    }

}

?>