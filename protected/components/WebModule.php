<?php

abstract class WebModule extends CWebModule {

    /**
     * @var bool Whether to auto initialise the module or not.
     */
    public $autoinit = false;

    /**
     * @var array Other module that this module depends on.
     */
    public $dependencies = array();

    /**
     * @var array Manager model behaviors to attach.
     */
    public $managerBehaviors = array();

    /**
     * @var array Manager model relations to attach.
     */
    public $managerRelations = array();

    /**
     *
     */
    public function init() {
        $this->checkDependencies();

        // Attach events
        Utils::registerCallback('onManagerBeforeFind', $this, 'attachManagerAttributes');
    }

    /**
     * Preloads child modules.
     */
    protected function preloadModules() {
        foreach ($this->getModules() as $name => $module) {
            if (isset($module['autoinit']) && $module['autoinit'] === true) {
                $this->getModule($name);
            }
        }
    }

    /**
     * Checks for dependant modules.
     */
    private function checkDependencies() {
        foreach ($this->dependencies as $dep) {
            if (!_isModuleOn($dep))
                throw new CException("Dependency failed for module '{$this->name}': missing module '{$dep}'");
        }
    }

    /**
     * @param CEvent $e
     */
    protected function attachManagerAttributes(CEvent $e) {
        
        // Attach behaviors to manager models
        foreach ($this->managerBehaviors as $class) {
            $e->sender->attachBehavior($class, new $class);
        }

        // Attach relations to manager models
        foreach ($this->managerRelations as $relation => $config) {
            $e->sender->getMetadata()->addRelation($relation, $config);
        }
    }

}

?>
