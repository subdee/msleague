<?php

class WebUser extends CWebUser {

    public function loginRequired() {
        $app = Yii::app();
        $request = $app->getRequest();
        $this->setReturnUrl($request->getUrl());
        if (($url = $this->loginUrl) !== null) {
            
            if (is_array($url)) {
                $url = $url[0];
            }
            if ($request->isAjaxRequest) {
                Utils::jsonReturn(array('redirect' => _url($url)));
            } else {
                $request->redirect(_url($url));
            }
        }
    }
    
    public function __get($name)
    {
        if ($this->hasState('__userInfo')) {
            $user=$this->getState('__userInfo',array());
            if (isset($user[$name])) {
                return $user[$name];
            }
        }
 
        return parent::__get($name);
    }
 
    public function login($identity, $duration) {
        $this->setState('__userInfo', $identity->getUser());
        parent::login($identity, $duration);
    }

}

?>
