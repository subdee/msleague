<?php

class WidgetBox extends CWidget {

    /**
     * @var string The place to go
     */
    public $place = 'main'; // or sidebar

    /**
     * @var string The view to render inside the box
     */
    public $view = null;

    /**
     * @var string The content from database to render inside the box
     */
    public $dynamic = null;

    /**
     * @var string The static content to render inside the box
     */
    public $static = null;

    /**
     * @var string The title of the box
     */
    public $title = null;

    /**
     * @var string A name for the box to use for css and javascript. Optional.
     */
    public $name = '';

    /**
     * @var string Extra css classes. Optional.
     */
    public $class = '';

    /**
     * @var array The parameters to pass in the view
     */
    public $params = array();

    /**
     * @var array If not empty, a link with follow the box's content
     */
    public $more = array();

    /**
     * @var array Option to cache the content.
     */
    public $cache = null;
    public $titleImage = null;
    public $widgetView = 'application.components.views.widgetBox';

    public function init() {

        if (Yii::app()->theme) {
            $this->widgetView = Utils::themeViewsDirectory('WidgetBox.widgetBox');
        }

        $this->class .= " {$this->place} {$this->name}";
        if ($this->titleImage)
            $this->class .= ' title-image';
    }

    final public function run() {
        $this->render($this->widgetView);
    }

    public function getClass() {
        return $this->class;
    }

}

?>
