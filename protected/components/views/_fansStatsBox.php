<?php

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $teams->search(),
//                    'hideHeader'=>true,
    'enablePagination' => false,
    'enableSorting' => false,
    'template' => '{items}',
    'columns' => array(
        array(
            'type' => 'image',
            'value' => '$data->small_jersey_url()'
        ),
        'name',
        array(
            'name' => 'fans',
            'htmlOptions' => array('style' => 'text-align:right'),
        ),
    ),
));
?>