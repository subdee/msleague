<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $games->search(),
    'hideHeader' => true,
    'enablePagination' => false,
    'enableSorting' => false,
    'template' => '{items}',
    'htmlOptions' => array('style' => 'cursor: pointer;'),
    'selectableRows' => 1,
    'selectionChanged' => 'function(id){ MSL.Goto("' . _url("match/match?id=") . '" + $.fn.yiiGridView.getSelection(id)); }',
    'columns' => array(
        array(
            'name' => 'date_played',
            'value' => 'Utils::date($data->date_played)',
            'htmlOptions' => array('class' => 'dateres'),
        ),
        array(
            'name' => 'teamHome0.name',
            'htmlOptions' => array('class' => 'tright teamres'),
        ),
        array(
            'type' => 'raw',
            'name' => 'score',
            'value' => '$data->is_cancelled ? "'._t("canc.").'" : "<strong>{$data->score_home} - {$data->score_away}</strong>"',
            'htmlOptions' => array('class' => 'tcenter scoreres'),
            'cssClassExpression' => '$data->is_cancelled ? "red" : ""'
        ),
        array(
            'name' => 'teamAway0.name',
            'htmlOptions' => array('class' => 'teamres'),
        ),
    ),
));
?>
<div class="footer"><?php echo _t('All matches are displayed in {timezone} timezone.', array('{timezone}' => _getUserTime()->format('T'))); ?></div>
