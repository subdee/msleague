<?php

echo CHtml::openTag('ul', array('class' => $this->cssClassUl . ' level-' . $level));

foreach ($items as $item) {
    if (!$item['visible'])
        continue;

    echo CHtml::openTag('li', array('class' => $item['cssClassLi'])); {

        $content = '';
        if (isset($item['image'])) {
            $content .= CHtml::image($item['image']);
        }
        if (isset($item['label'])) {
            $content .= CHtml::tag('span', array(), $item['label']);
        }

        if (isset($item['realUrl'])) {
            echo CHtml::link($content, $item['realUrl']);
        } else {
            echo CHtml::tag('span', array(), $content);
        }
    } // end li
    echo CHtml::closeTag('li');
} // end foreach

echo CHtml::closeTag('ul');
?>
