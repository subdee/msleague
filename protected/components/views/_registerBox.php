<?php
echo _t('Join us, create your team and be the top manager of {gameName}.', array(
    '{gameName}' => _gameName()
));
?>
<a href="<?php echo _url('user/register'); ?>">
    <?php echo _t('Register'); ?>
</a>