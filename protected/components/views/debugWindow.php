<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'msl-debug-dialog',
    // additional javascript options for the dialog plugin
    'options' => array(
        'title' => 'MSLeague Debug Window',
        'position' => array('left', 'top'),
        'closeOnEscape' => false,
        'min-width' => 200,
    ),
));

$staticTabs = array();
foreach ($data as $type => $tab) {
    $key = $type . ' (' . count($tab) . ')';
    $staticTabs[$key] = $this->debugDialogTree($tab);
}

$this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs' => $staticTabs,
    // additional javascript options for the tabs plugin
    'options' => array(
        'collapsible' => true,
    ),
));
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>
<script type="text/javascript">
    $(window).load(function(){
        // add minimize/maximize button
        var closeButton = $('#ui-dialog-title-msl-debug-dialog').next();
        
        closeButton.before('<a href="#" id="msl-debug-dialog-minimize" class="ui-dialog-titlebar-close ui-dialog-titlebar-minmax ui-corner-all" role="button"><span class="ui-icon ui-icon-carat-1-s">minimize</span></a>');
        closeButton.before('<a href="#" id="msl-debug-dialog-maximize" class="ui-dialog-titlebar-close ui-dialog-titlebar-minmax ui-corner-all ui-helper-hidden" role="button"><span class="ui-icon ui-icon-carat-1-n">maximize</span></a>');
        
        var dialogPrevState = {pos : null, dim : {height: 0}};
        
        var minmaxButtons = $('.ui-dialog-titlebar-minmax');
        minmaxButtons.hover(function(){
            $(this).addClass('ui-state-hover');
        }, function(){
            $(this).removeClass('ui-state-hover');
        })
        .click(function(){
            minmaxButtons.toggleClass('ui-helper-hidden');
            var dialogContent = $('#msl-debug-dialog');
            var dialog = dialogContent.parent();
            dialogContent.toggle();
            if(dialogContent.is(':visible')) {
                dialogContent.dialog({ draggable: true });
                dialog.css({
                    'position' : 'absolute', 
                    'top' : dialogPrevState.pos.top, 
                    'bottom' : '', 
                    'left' : dialogPrevState.pos.left,
                    'height' : dialogPrevState.dim.height
                });
            } else {
                dialogPrevState.pos = dialog.position();
                dialogPrevState.dim.height = dialog.height();
                dialogContent.dialog({ draggable: false });
                dialog.css({
                    'position' : 'fixed', 
                    'top': '', 
                    'bottom' : 0, 
                    'left' : 0,
                    'height' : 28
                });
            }
        })
        ;
        
        $('li.collapsable + ul').hide();
        $('li.collapsable').live('click', function(){
            $(this).next().toggle();
        });
    });
</script>
<style>
    #msl-debug-dialog {
        padding: 0;
    }
    #msl-debug-dialog .ui-tabs {
        border: none;
    }
    #msl-debug-dialog .ui-tabs-panel li {
        padding: 5px 0;
    }
    #msl-debug-dialog .ui-tabs-panel li.collapsable {
        cursor: pointer;
        font-weight: bold;
    }
    #msl-debug-dialog .ui-tabs-panel .null {
        color: blue;
    }
    #msl-debug-dialog .ui-tabs-panel .false {
        color: red;
    }
    #msl-debug-dialog .ui-tabs-panel .true {
        color: green;
    }
    #msl-debug-dialog .ui-tabs-nav {
        background: none;
        border: none;
        border-bottom: 1px solid black;
    }
    #msl-debug-dialog .ui-tabs-nav li a {
        padding: 5px;
    }
    .ui-dialog .ui-dialog-titlebar-minmax {
        right: 2em;
    }    
</style>