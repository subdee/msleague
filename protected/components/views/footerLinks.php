<ul>
<?php
    $i=0;
    $count=count($links);
    foreach($links as $link)
    {
        echo CHtml::openTag('li');
        echo CHtml::link($link[1], $link[0]);
        if($i < $count-1) echo '|';
        echo CHtml::closeTag('li');
        $i++;
    }
?>
</ul>
