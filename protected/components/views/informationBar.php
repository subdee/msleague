<ul class="information-bar">
    <li class="stockinfo">
        <span class="image"></span>
        <span class="content">
            <div class="label"><?php echo _t('Shares'); ?></div>
            <div class="value"><?php echo _currency(_manager()->portfolio_share_value); ?></div>
        </span>
    </li>
    <li class="cash">
        <span class="image"></span>
        <span class="content">
            <div class="label"><?php echo _t('Cash'); ?></div>
            <div class="value"><?php echo _currency(_manager()->portfolio_cash); ?></div>
        </span>
    </li>
    <li class="portfolio">
        <span class="image"></span>
        <span class="content">
            <div class="label"><?php echo _t('Portfolio'); ?></div>
            <div class="value"><?php echo _currency(_manager()->portfolio_cash + _manager()->portfolio_share_value); ?></div>
        </span>
    </li>
    <li class="lastupdate">
        <div>
            <span class="image"></span>
            <span class="content">
                <div class="label"><?php echo _t('Portfolio change'); ?></div>
                <div class="value nobold">
                    <?php if (!empty($portfolioChanges)) : ?>
                        <?php echo $portfolioChanges[0]['time']; ?>
                        <span class="<?php echo $portfolioChanges[0]['type']; ?> bold">
                            <?php echo $portfolioChanges[0]['change']; ?>
                            <img src="<?php echo $portfolioChanges[0]['image']; ?>" />
                        </span>
                        <?php unset($portfolioChanges[0]); ?>
                    <?php else : ?>
                        <?php echo _t('No updates yet!'); ?>
                    <?php endif; ?>
                </div>
            </span>
        </div>
        <?php if (count($portfolioChanges) > 0) : ?>
            <div class="details hidden">
                <?php foreach ($portfolioChanges as $pc) : ?>
                    <div>                
                        <?php echo $pc['date']; ?>,
                        <?php echo $pc['time']; ?>
                        <span class="<?php echo $pc['type']; ?> bold">
                            <?php echo $pc['change']; ?>
                            <img src="<?php echo $pc['image']; ?>" />
                        </span>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="toggle">
                <img class="close hidden" src="<?php echo Utils::imageUrl('st_up.png'); ?>" />
                <img class="open" src="<?php echo Utils::imageUrl('st_down.png'); ?>" />
            </div>
        <?php endif; ?>
    </li>
    <li class="players">
        <div>
            <span class="image"></span>
            <span class="content">
                <div class="label"><?php echo _t('Players'); ?></div>
                <div class="value"><?php echo _team()->numOfPlayers(); ?></div>
            </span>
        </div>
        <div class="details hidden">
            <div>
                <?php echo _t('Goalkeepers'); ?>:
                <span class="bold"><?php echo $counts['GK']; ?></span>
            </div>
            <div>
                <?php echo _t('Defenders'); ?>:
                <span class="bold"><?php echo $counts['DE']; ?></span>
            </div>
            <div>
                <?php echo _t('Midfielders'); ?>:
                <span class="bold"><?php echo $counts['MF']; ?></span>
            </div>
            <div>
                <?php echo _t('Strikers'); ?>:
                <span class="bold"><?php echo $counts['ST']; ?></span>
            </div>
        </div>
        <div class="toggle">
            <img class="close hidden" src="<?php echo Utils::imageUrl('st_up.png'); ?>" />
            <img class="open" src="<?php echo Utils::imageUrl('st_down.png'); ?>" />
        </div>
    </li>
    <li class="transactions lastitem">
        <span class="image"></span>
        <span class="content">
            <div class="label"><?php echo _t('Transactions'); ?></div>
            <div class="value">
                <?php echo $transactions; ?>
            </div>
        </span>
        <?php if ($transactions > 0 && $transactions <= $transactionLowLimit): ?>
            <img src="<?php echo Utils::imageUrl('info-warn.png'); ?>" title="<?php echo _t('You are running out of transactions.'); ?>" F/>
        <?php endif; ?>
    </li>

</ul>
<script type="text/javascript">
    $(document).ready(function(){
        
        $('.information-bar .toggle').click(function(){
            var details = $(this).parent().find('.details');
            if($(details).is(':visible')) {
                $(this).children('.close').hide();
                $(this).children('.open').show();
                details.hide('blind');
            } else {
                $(this).children('.open').hide();
                $(this).children('.close').show();
                details.show('blind');
            }
        });
        
        $('.information-bar .transactions > img')
        .effect("pulsate", { times: 5 }, 2000)
        .tooltip({
            position: "top center",
            offset: [-5, 0],
            tipClass : 'standard-tooltip low-transactions-tooltip'
        })
        .mouseover(function(){
            $(this)
            .stop(true, true)
            .css({opacity : 1});
        });
    })
</script>