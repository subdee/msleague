<div class="information-h-bar">
    <ul>
        <li class="cash">
            <div class="image"></div>
            <span class="label"><?php echo _currency(_manager()->portfolio_cash); ?></span>
            <div class="details standard-tooltip browser-tooltip hidden">
                <div class="bold header"><?php echo _t('Cash'); ?></div>
            </div>
        </li>
        <li class="portfolio">
            <div class="image"></div>
            <span class="label"><?php echo _currency(_manager()->portfolio_cash + _manager()->portfolio_share_value); ?></span>
            <div class="details standard-tooltip browser-tooltip hidden">
                <div class="bold header"><?php echo _t('Budget'); ?></div>
            </div>
        </li>
        <li class="lastupdate">
            <div class="image"></div>
            <span class="label">
                <?php
                if (!empty($portfolioChanges)) {
                    echo $portfolioChanges[0]['time'];
                    $this->widget("ValueChangeInfo", array("change" => $portfolioChanges[0]['change']));
                    unset($portfolioChanges[0]);
                } else {
                    echo _t('No updates yet!');
                }
                ?>
            </span>
            <div class="details standard-tooltip browser-tooltip hidden">
                <div class="bold header"><?php echo _t('Budget change'); ?></div>
                <?php
                if (count($portfolioChanges) > 0) {

                    foreach (array_reverse($portfolioChanges) as $pc) {
                        echo CHtml::openTag('div');
                        echo $pc['date'];
                        echo ', ';
                        echo $pc['time'];
                        $this->widget("ValueChangeInfo", array('change' => $pc['change']));
                        echo CHtml::closeTag('div');
                    }
                }
                ?>
            </div>
        </li>
        <li class="players">
            <div class="image"></div>
            <span class="label"><?php echo $players; ?></span>
            <div class="details standard-tooltip browser-tooltip hidden">
                <div class="bold header"><?php echo _t('Players'); ?></div>
                <div>
                    <?php echo _t('Goalkeepers'); ?>:
                    <span class="bold"><?php echo $playerCounts['GK']; ?></span>
                </div>
                <div>
                    <?php echo _t('Defenders'); ?>:
                    <span class="bold"><?php echo $playerCounts['DE']; ?></span>
                </div>
                <div>
                    <?php echo _t('Midfielders'); ?>:
                    <span class="bold"><?php echo $playerCounts['MF']; ?></span>
                </div>
                <div>
                    <?php echo _t('Strikers'); ?>:
                    <span class="bold"><?php echo $playerCounts['ST']; ?></span>
                </div>
            </div>
        </li>
        <li class="transactions">
            <div class="image"></div>
            <span class="label">
                <span><?php echo $transactions; ?></span>
                <?php if ($transactions > 0 && $transactions <= $transactionLowLimit): ?>
                    <img class="warning" src="<?php echo Utils::imageUrl('informationBar/info-warn.png'); ?>" />
                <?php endif; ?>
            </span>
            <div class="details standard-tooltip browser-tooltip hidden">
                <div class="bold header"><?php echo _t('Transactions'); ?></div>
                <div class="warning">
                    <?php if ($transactions > 0 && $transactions <= $transactionLowLimit): ?>
                        <?php echo _t('You are running out of transactions.'); ?>
                    <?php endif; ?>
                </div>
            </div>
        </li>
    </ul>
</div>
<script type="text/javascript">
    $('.information-h-bar .label').tooltip({
        position: "top center",
        offset: [-5, 0],
        effect: "fade"
    });

    $('.information-h-bar .transactions img.warning').effect("pulsate", { times: 5 }, 2000);
    $('.information-h-bar .transactions .label').mouseover(function(){
        $('img.warning', this)
        .stop(true, true)
        .css({opacity : 1});
    });
</script>