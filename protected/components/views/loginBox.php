<?php
$form = $this->beginWidget('CActiveForm', array(
        'action' => _url('user/login'),
        'htmlOptions' => array(
            'onSubmit' => '$(this).find(":submit").attr("disabled", true);',
            'class' => 'loginbox',
        ),
        'focus' => array($model, 'username')
    ));
?>

<div class="row">
    <?php echo $form->labelEx($model, 'username'); ?>
    <?php echo $form->textField($model, 'username'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'password'); ?>
    <?php echo $form->passwordField($model, 'password'); ?>
</div>

<div class="row buttons">
    <?php echo CHtml::submitButton(_t('Login')); ?>
</div>
<div class="row forgot-pass">
    <a href="<?php echo _url('passwordReset/index'); ?>"><?php echo _t('Forgot password?'); ?></a>
</div>
<?php $this->endWidget(); ?>