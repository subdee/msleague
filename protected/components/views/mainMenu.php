<?php

if ($this->showLevel >= 0) {
    $this->renderLevel($this->showLevel);
} else {
    $this->widget('application.extensions.mbmenu.MbMenu', array(
        'items' => $this->getItems(),
    ));
}
?>