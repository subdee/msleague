<ul>
    <?php if (_manager()->hasRole(Role::TEMPORARY | Role::RESET)) : ?>
        <?php if (Gameconfiguration::model()->get('is_season_open') && _controller()->route != 'createTeam/index') : ?>
            <li class="create-team">
                <?php echo _l(CHtml::tag('span', array(), _t('Create your team')), _url('createTeam/index')); ?>
            </li>
        <?php endif; ?>
    <?php endif; ?>
    <?php if (_manager()->hasRole(Role::MANAGER)) : ?>
        <?php Utils::triggerEvent('onManagerBoxMenuTop', $this); ?>
        <li class="first">
            <?php echo CHtml::image(Utils::imageUrl('managerBox/points.png')); ?>
            <?php echo _l(_t('{points} points', array('{points}' => _app()->numberFormatter->format('######.##', $points))), _url('managers/points')); ?>
        </li>
        <li>
            <?php echo CHtml::image(Utils::imageUrl('managerBox/portfolio.png')); ?>
            <?php echo _l('€' . Utils::number(Manager::getPortfolioValue()), _url('managers/portfolio')); ?>
        </li>
        <li>
            <?php echo CHtml::image(Utils::imageUrl('managerBox/team.png')); ?>
            <?php echo _l(_t('My team'), _url('pitch/index')); ?>
        </li>
    <?php endif; ?>
    <?php Utils::triggerEvent('onManagerBoxMenu1', $this); ?>
    <li class="last">
        <?php echo CHtml::image(Utils::imageUrl('managerBox/settings.png')); ?>
        <?php echo _l(_t('Settings'), _url('settings/index')); ?>
    </li>
</ul>
<?php
echo CHtml::form(_url('user/logout'));
echo CHtml::submitButton(_t('Log out'));
echo CHtml::endForm();
?>
