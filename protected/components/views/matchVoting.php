<div id="voting-container">
    <?php if ($this->hasVoted || $this->hasMatchStarted || _manager()->isBanned()) : ?>
        <div class="matchVoting">
            <div class="question">
                <?php echo _t('Which team will win?'); ?>
            </div>
            <div class="votes-text"><?php echo $this->votes->totalVotes.' '._t('votes'); ?></div>
            <div class="vote-content">
                <div class="box">
                    <div class="box-perc"><?php echo Utils::percent($this->votes->homePerc, true, false, false); ?></div>
                    <div class="bar">
                        <div class="barfill" style="width: <?php echo $this->votes->homePerc * 100; ?>%;"></div>
                    </div>
                    <div class="box-text"><?php echo $this->game->teamHome0->shortname15; ?></div>
                </div>
                <div class="box">
                    <div class="box-perc"><?php echo Utils::percent($this->votes->drawPerc, true, false, false); ?></div>
                    <div class="bar">
                        <div class="barfill" style="width: <?php echo $this->votes->drawPerc * 100; ?>%;"></div>
                    </div>
                    <div class="box-text"><?php echo _t('DRAW'); ?></div>
                </div>
                <div class="box">
                    <div class="box-perc"><?php echo Utils::percent($this->votes->awayPerc, true, false, false); ?></div>
                    <div class="bar">
                        <div class="barfill" style="width: <?php echo $this->votes->awayPerc * 100; ?>%;"></div>
                    </div>
                    <div class="box-text"><?php echo $this->game->teamAway0->shortname15; ?></div>
                </div>
            </div>
        </div>
    <?php else : ?>
        <div class="matchVoting">
            <div class="question">
                <?php echo _t('Which team will win?'); ?>
            </div>
            <div class="vote-content">
                <div class="box">
                    <a href="#"><div class="box-image" id="<?php echo ManagerMatchVote::HOME; ?>"></div></a>
                    <div class="box-text"><?php echo $this->game->teamHome0->shortname15; ?></div>
                </div>
                <div class="box">
                    <a href="#"><div class="box-image" id="<?php echo ManagerMatchVote::DRAW; ?>"></div></a>
                    <div class="box-text"><?php echo _t('DRAW'); ?></div>
                </div>
                <div class="box">
                    <a href="#"><div class="box-image" id="<?php echo ManagerMatchVote::AWAY; ?>"></div></a>
                    <div class="box-text"><?php echo $this->game->teamAway0->shortname15; ?></div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $(".box .box-image").click(function(){
            var vote = $(this).attr("id");
            $.ajax({
                url: "<?php echo _url('match/vote'); ?>",
                type: "POST",
                data: {
                    'vote' : vote,
                    'game' : <?php echo $this->game->id; ?>
                },
                success: function(data){
                    $("#voting-container").fadeOut(400,function(){$("#voting-container").html(data)}).fadeIn(400);
                }
            });
            return false;
        })
    })
</script>