<div class="<?php echo $class; ?>" id="<?php echo $this->id; ?>">
    <?php
    if ($this->closable)
        echo CHtml::image(Utils::imageUrl('notice/close.png'), '', array(
            'title' => _t('Close'),
            'onclick' => "\$(this).parent().hide('fade', function(){\$(document).trigger('onWidgetNoticeClose');});"
        ));
    echo CHtml::tag('div', array(), $this->message);
    ?>
</div>