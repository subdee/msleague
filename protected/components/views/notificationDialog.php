<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'notification-dialog',
    'options' => array(
        'title' => $this->title,
        'resizable' => false,
        'draggable' => false,
        'autoOpen' => true,
        'width' => '50%',
        'position' => array('center', 'middle'),
        'modal' => true,
        'closeOnEscape' => !$this->obligatory,
        'open' => 'js:function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }'
    ),
));
?>

<?php echo $this->content; ?>

<?php $this->endWidget(); ?>