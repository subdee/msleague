<div id="left-profile-column">
    <?php $chartClass = 'chart' . $this->player->id; ?>
    <div id="<?php echo $chartClass; ?>" class="chart">
        <!--Enter stock value chart-->
        <?php
//            var_dump($st); die();
        $this->Widget('ext.highcharts.HighchartsWidget', array(
            'options' => array(
                'credits' => array(
                    'enabled' => false
                ),
                'chart' => array(
                    'defaultSeriesType' => 'area',
                    'renderTo' => $chartClass,
                    'backgroundColor' => '#F0F0F0'
                ),
                'title' => array('text' => ''),
                'xAxis' => array(
                    'categories' => $this->st['date'],
                    'tickInterval' => (int) (count($this->st['date']) / 3),
                ),
                'yAxis' => array(
                    'title' => array('text' => ''),
                    'min' => $this->loSt - ($this->loSt * 0.05),
                    'labels' => array(
                            'formatter' => 'js:function(){
                            return "€"+this.value;
                                }',
                        ),
                ),
                'plotOptions' => array(
                    'area' => array(
                        'marker' => array(
                            'enabled' => false,
                            'symbol' => 'circle',
                            'radius' => 2,
                            'states' => array(
                                'hover' => array(
                                    'enabled' => true
                                )
                            )
                        ),
                        'lineWidth' => 1.5,
                        'shadow' => false,
                        'fillOpacity' => '0.35',
                    )
                ),
                'tooltip' => array(
                    'formatter' => 'js:function(){
                            return "<strong>"+this.x+":</strong> "+this.y;
                           }'
                ),
                'legend' => array(
                    'enabled' => false,
                ),
                'series' => array(
                    array('name' => _t('Share price'), 'data' => $this->st['value'])
                ),
                'theme' => 'default',
                'exporting' => array(
                    'enabled' => false
                )
            )
        ));
        ?>
    </div>
</div>
<div class="profile-usermenu player-view">
    <div class="meta">
        <ul>
            <li class="name">
                <?php if ($this->showExtraDetails) : ?>
                    <?php echo CHtml::link($this->player->name, _url('players/view', array('plId' => $this->player->id))); ?>
                <?php else : ?>
                    <?php echo $this->player->name; ?>
                <? endif; ?>
            </li>
        </ul>
    </div>
    <div class="status">
        <?php echo PlayerListDisplay::sInjuredHtml($this->player); ?>
        <?php echo PlayerListDisplay::sSuspendedHtml($this->player); ?>
    </div>
    <div class="watchlist-button">
        <?php if (_manager()->isBanned()) : ?>
            <?php echo CHtml::tag('div', array('class' => 'no-watchlist', 'title' => _t('You cannot add or remove players to your watchlist because you are banned.')), CHtml::image(Utils::imageUrl('iconset/grayadd.png')) . _t('Remove from watchlist')); ?>
        <?php else : ?>
            <?php if (Watchlist::model()->exists('manager_id = :id and player_id = :pid', array(':id' => _manager()->id, ':pid' => $this->player->id))) : ?>
                <?php echo CHtml::link(CHtml::tag('div', array('class' => 'remove-watchlist'), CHtml::image(Utils::imageUrl('iconset/remove.png')) . _t('Remove from watchlist')), _url('managers/removeWatchlist', array('id' => $this->player->id)), array('class' => 'remove-link-' . $this->player->id, 'id' => $this->player->id)); ?>
            <?php else : ?>
                <?php if ($canAdd) : ?>
                    <?php echo CHtml::link(CHtml::tag('div', array('class' => 'add-watchlist'), CHtml::image(Utils::imageUrl('iconset/add.png')) . _t('Add to watchlist')), _url('managers/addWatchlist', array('id' => $player->id))); ?>
                <?php else : ?>
                    <?php echo CHtml::link(CHtml::tag('div', array('class' => 'add-watchlist'), CHtml::image(Utils::imageUrl('iconset/add.png')) . _t('Add to watchlist')), _url('managers/addWatchlist', array('id' => $this->player->id))); ?>
                <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>
    </div>
    <div class="rating">
        <?php
        $this->widget('CStarRating', array(
            'name' => 'rating' . $this->player->id,
            'value' => $this->player->average_rating,
            'minRating' => 1,
            'maxRating' => 5,
            'resetValue' => 0,
            'allowEmpty' => false,
            'resetText' => _t('Reset vote'),
            'callback' => 'function(){
                        $.ajax({
                            type: "GET",
                            url: "' . _url('players/rating') . '",
                            data: "id=' . $this->player->id . '&rating="+$(this).val(),
                                success : function(data){
                                        $("#rating' . $this->player->id . ' > input").rating("select",data.rating - 1,false);
                                        $(".rateCount' . $this->player->id . '").html(data.votes);
                                }
                        })
                    }'
        ));
        ?>
        <div class="rateCount<?php echo $this->player->id; ?>">(<?php echo $this->ratingCount . ' ' . _t('votes'); ?>)</div>
    </div>
</div>
<div class="player-profile-info game-info">
    <?php echo $this->player->large_jersey_html(); ?>
    <table>
        <tr>
            <td class="first" ><?php echo _t('Position'); ?>:</td>
            <td><?php echo mb_ucfirst($this->player->basicPosition->getTranslatedName()); ?></td>
        </tr>
        <tr>
            <td class="first" ><?php echo _t('Team'); ?>:</td>
            <td>
                <?php
                if ($this->player->team != null) {
                    echo $this->player->team->name;
                } else {
                    echo '-';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td class="first" ><?php echo _t('Sell Price'); ?>:</td>
            <td>
                <?php echo _currency($this->player->current_value); ?>&nbsp;&nbsp;
                <?php
                $this->widget('ValueChangeInfo', array(
                    'change' => $this->change,
                    'type' => 'big',
                ));
                ?>
            </td>
        </tr>
        <tr>
            <td class="first" ><?php echo _t('Points'); ?>:</td>
            <td><?php echo $this->player->total_points; ?></td>
        </tr>
        <tr>
            <td class="first" ><?php echo _t('Owned'); ?>:</td>
            <td><?php echo $this->bought . ' ' . _t('shares'); ?></td>
        </tr>
        <tr>
            <td class="first" ><?php echo _t('Next match'); ?>:</td>
            <td class="font-85">
                <?php if ($this->nextMatch) : ?>
                    <?php echo $this->nextMatch->teamHome0->shortname5 . ' - ' . $this->nextMatch->teamAway0->shortname5; ?>
                    (<?php echo Utils::date($this->nextMatch->date_played); ?>)
                <?php endif; ?>
            </td>
        </tr>
    </table>
</div>