<?php if ($this->profile()) : ?>
    <div class="points-header">
        <div class="managerpoints">
            <span><?php echo _t('points'); ?></span>
            <strong><?php echo _app()->numberFormatter->format('######.##', $this->profile()->total_points); ?></strong>
        </div>
        <div class="round">
            <?php
            echo CHtml::form(_url(_controller_id() . '/points'), 'get');
            echo CHtml::tag('span', array(), _t('Matchday:'));
            echo CHtml::dropDownList('date', $this->getDate(), CHtml::listData($dateList, 'dateOnly', 'dateNoTime'), array('submit' => ''));
            echo CHtml::hiddenField('id', $this->profile()->manager_id);
            echo CHtml::endForm();
            ?>
        </div>
    </div>
<?php endif; ?>
<?php
$playerAttrLabels = Player::model()->attributeLabels();
$tooltipLabels = Player::model()->tooltipLabels();

function _getAttribLink($attr, $playerAttrLabels, $tooltipLabels) {
    return _l($playerAttrLabels[$attr] . CHtml::tag('span', array('class' => 'classic'), $tooltipLabels[$attr]), '#');
}

$cols = array(
    array(
        'header' => _t('Pos'),
        'value' => 'BasicPosition::model()->translated($data->pos)',
        'htmlOptions' => array('class' => 'first-column'),
    ),
    array(
        'type' => 'raw',
        'value' => '$data->status->status == "Leader" ?  CHtml::image(Utils::imageUrl("pitch/star.png"), "", array("title" => "' . _t('Captain receives double points') . '")) : ""',
        'htmlOptions' => array('class' => 'player-status'),
    ),
    array(
        'type' => 'raw',
        'header' => _t('Name'),
        'value' => '$data->player->link()',
        'htmlOptions' => array('class' => 'player'),
    ),
    array(
        'type' => 'raw',
        'header' => _t('Match'),
        'value' => 'GamePlayer::model()->getPlayersTeam($data->player->id, $data->gameDate)',
        'htmlOptions' => array('class' => 'match'),
    ),
    array(
        'header' => _getAttribLink('min', $playerAttrLabels, $tooltipLabels),
        'value' => '$data->min == 0 ? "-" : $data->min."\'"',
        'headerHtmlOptions' => array('class' => 'tooltip')
    ),
    array(
        'header' => _getAttribLink('gs', $playerAttrLabels, $tooltipLabels),
        'value' => '$data->gs == 0 ? "-" : $data->gs',
        'headerHtmlOptions' => array('class' => 'tooltip')
    ),
    array(
        'header' => _getAttribLink('ass', $playerAttrLabels, $tooltipLabels),
        'value' => '$data->ass == 0 ? "-" : $data->ass',
        'headerHtmlOptions' => array('class' => 'tooltip')
    ),
    array(
        'header' => _getAttribLink('ng', $playerAttrLabels, $tooltipLabels),
        'value' => '$data->ng == 0 ? "-" : $data->ng',
        'headerHtmlOptions' => array('class' => 'tooltip')
    ),
    array(
        'header' => _getAttribLink('ga', $playerAttrLabels, $tooltipLabels),
        'value' => '$data->ga == 0 ? "-" : $data->ga',
        'headerHtmlOptions' => array('class' => 'tooltip')
    ),
    array(
        'header' => _getAttribLink('cs', $playerAttrLabels, $tooltipLabels),
        'value' => '$data->cs == 0 ? "-" : $data->cs',
        'headerHtmlOptions' => array('class' => 'tooltip')
    ),
    array(
        'header' => _getAttribLink('ps', $playerAttrLabels, $tooltipLabels),
        'value' => '$data->ps == 0 ? "-" : $data->ps',
        'headerHtmlOptions' => array('class' => 'tooltip')
    ),
    array(
        'header' => _getAttribLink('rc', $playerAttrLabels, $tooltipLabels),
        'value' => '$data->rc == 0 ? "-" : $data->rc',
        'headerHtmlOptions' => array('class' => 'tooltip')
    ),
    array(
        'header' => _getAttribLink('og', $playerAttrLabels, $tooltipLabels),
        'value' => '$data->og == 0 ? "-" : $data->og',
        'headerHtmlOptions' => array('class' => 'tooltip')
    ),
    array(
        'header' => _getAttribLink('mp', $playerAttrLabels, $tooltipLabels),
        'value' => '$data->mp == 0 ? "-" : $data->mp',
        'headerHtmlOptions' => array('class' => 'tooltip')
    ),
    array(
        'header' => _getAttribLink('pointsPerShare', $playerAttrLabels, $tooltipLabels),
        'value' => '$data->points*$data->co',
        'htmlOptions' => array('class' => 'bold'),
        'headerHtmlOptions' => array('class' => 'tooltip')
    ),
    array(
        'header' => _getAttribLink('noOfShares', $playerAttrLabels, $tooltipLabels),
        'value' => '$data->shares',
        'headerHtmlOptions' => array('class' => 'tooltip')
    ),
    array(
        'header' => _l(_t('Points'), '#'),
        'value' => '$data->points*$data->shares*$data->co',
        'htmlOptions' => array('class' => 'bold'),
    ),
);
?>

<div class="points-table-header"><?php echo _t('Starters'); ?></div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $this->basicPlayers()->search(),
    'template' => '{items}',
    'htmlOptions' => array('class' => 'grid-view points'),
    'columns' => $cols,
    'emptyText' => _t('There is no point history yet.'),
));
?>

<div class="points-table-header"><?php echo _t('Substitutes'); ?></div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $this->subPlayers()->search(),
    'template' => '{items}',
    'htmlOptions' => array('class' => 'grid-view points'),
    'columns' => $cols,
    'emptyText' => _t('There is no point history yet.'),
));
?>
