<?php
echo _t('Join us, create your team and be the top manager of {gameName}.', array(
    '{gameName}' => _gameName()
));
?>
<a href="<?php echo _url('user/register'); ?>">
    <div class="registerImage"><div class="text"><?php echo _t('Register'); ?></div></div>
</a>