<ul class="stockmarket-filters">
    <li class="first">
        <h3><?php echo _t('Filters'); ?></h3>
        <div class="subheader"><?php echo _t('Use the filters to ease the search'); ?></div>
    </li>
    <?php if (_action_id() == 'buy' || _action_id() == 'replace') : ?>
        <?php if (_action_id() == 'buy') : ?>
            <li>
                <?php echo CHtml::checkBox('own-team'); ?>
                <?php echo CHtml::label(_t('My team'), 'own-team'); ?>
            </li>
        <?php endif; ?>
        <li>
            <?php echo CHtml::checkBox('watchlist'); ?>
            <?php echo CHtml::label(_t('My watchlist'), 'watchlist'); ?>
        </li>
        <li>
            <?php echo CHtml::checkBox('hide-inactive'); ?>
            <?php echo CHtml::label(_t('Active only'), 'hide-inactive'); ?>
        </li>
        <li>
            <?php echo CHtml::checkBox('next-matches'); ?>
            <?php echo CHtml::label(_t('Next matches'), 'next-matches'); ?>
        </li>
        <?php if ($this->matches) : ?>
            <li class="matches">
                <?php foreach ($this->matches as $match) : ?>
                    <div>
                        <?php
                        echo CHtml::checkBox('match-' . $match->id, false, array(
                            // The value here is sent to datatables team filter as a regural expression.
                            'value' => $match->teamHome0->shortname5 . '|' . $match->teamAway0->shortname5
                        ));
                        ?>
                        <?php echo CHtml::image($match->teamHome0->small_jersey_url()); ?>
                        <label for="<?php echo "match-{$match->id}"; ?>">
                            <?php echo $match->teamHome0->shortname5 . ' - ' . $match->teamAway0->shortname5; ?>
                        </label>
                        <div class="details standard-tooltip browser-tooltip hidden">
                            <div><strong><?php echo $match->teamHome0->name . ' - ' . $match->teamAway0->name; ?></strong></div>
                            <div><?php echo Utils::date($match->date_played); ?></div>
                        </div>
                        <?php echo CHtml::image($match->teamAway0->small_jersey_url()); ?>
                    </div>
                <?php endforeach; ?>
            </li>
        <?php endif; ?>
    <?php endif; ?>
    <?php if (_action_id() == 'sell') : ?>
        <li>
            <?php echo CHtml::checkBox('show-players-not-playing', false, array('value' => $this->matchRegex)); ?>
            <?php echo CHtml::label(_t('Players not in next matches'), 'show-players-not-playing'); ?>
        </li>
    <?php endif; ?>
    <li class="clear lastitem">
        <a id="clear-filters" href="#"><?php echo _t('clear filters'); ?></a>
    </li>
</ul>
<script type="text/javascript">
    $(document).ready(function(){

        // Clear all filters
        $('#clear-filters').click(function(){
            // Clear checkboxes
            $('.stockmarket-filters :checkbox:checked:not(.matches :checkbox,#next-matches)').each(function(){
                $(this).triggerCheckbox(false);
            });
            $('#next-matches').triggerCheckbox(false);
            $(document).trigger('onStockmarketClearFilters');
            return false;
        });

        // Matches label tooltip
        $('.stockmarket-filters .matches label').tooltip({
            position: "center right",
            offset: [0, 35],
            effect: "fade"
        });

        // Global hack;
        var clearMatchFilter = true;

        // Global matches checkbox
        $('#next-matches').click(function(){
            if(clearMatchFilter) {
                $('.filters .team select').val('');
            }
            var selector = '.stockmarket-filters .matches :checkbox:checked';
            var elem = $(this);
            if(elem.is(':checked')) {
                selector = '.stockmarket-filters .matches :checkbox:not(checked)';
            }
            $(selector).each(function(){
                $(this).triggerCheckbox(elem.is(':checked'));
            })
            clearMatchFilter = true;
        });

        $('.stockmarket-filters .matches :checkbox').click(function(){
            if(clearMatchFilter)
                $('.filters .team select').val('');
            var val = '';
            $('.stockmarket-filters .matches :checkbox').each(function(){
                if($(this).is(':checked')) {
                    if(val != '') val += '|';
                    val += $(this).val();
                }
            });
            $(document).trigger('onStockmarketMatchFilter', [val]);
        });

        $('.filters .name :text')
        .keyup(function(){
            $(document).trigger('onStockmarketNameFilter', [$(this).val()]);
        })
        .focus(function(){
            $(this).val('');
            $(this).unbind('focus');
            $(this).keyup();
        });

        $('.filters .team select').change(function(){
            clearMatchFilter = false;
            $('#next-matches').triggerCheckbox(false);
            $(document).trigger('onStockmarketMatchFilter', [$(this).val()]);
        });

        $('.filters .position select').change(function(){
            $(document).trigger('onStockmarketPositionFilter', [$(this).val()]);
        });

        // Auto-hide inactive
        //$('#hide-inactive').triggerCheckbox(true);
    });

    $(document).bind('onStockmarketDatatableReadyEnd', function(){
        // Auto-select match checkboxes
        $('#next-matches').triggerCheckbox(true);
    });

</script>