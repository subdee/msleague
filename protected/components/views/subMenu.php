<div id="submenu">
    <ul>
        <?php $count = count($menu); ?>
        <?php $counter = 0; ?>
        <?php foreach ($menu as $m) : ?>
            <?php
            $after = '';
            if ($counter < $count - 1)
                $after = '|';

            if ($counter == 0)
                $m['class'] .= ' first';
            elseif ($counter == $count - 1)
                $m['class'] .= ' last';
            ?>
            <li class="<?php echo $m['class']; ?>">
                <?php if (isset($m['url'])) : ?>
                    <?php echo CHtml::link($m['title'], _url($m['url'])); ?>
                <?php else : ?>
                    <?php echo CHtml::tag('span', array(), $m['title']); ?>
                <?php endif; ?>
            </li>
            <?php $counter++; ?>
        <?php endforeach; ?>
    </ul>
</div>