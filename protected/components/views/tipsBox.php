<?php $count = count($this->tips); ?>
<?php $counter = 0; ?>
<ul>
    <?php foreach ($this->tips as $tip) : ?>
        <?php $class = ''; ?>
        <?php if (!$counter) : ?>
            <?php $class = 'first'; ?>
        <?php elseif (($counter+1) == $count) : ?>
            <?php $class = 'last'; ?>
        <?php endif; ?>
        <li class="<?php echo $class; ?>"><span><?php echo $tip; ?></span></li>
        <?php ++$counter; ?>
    <?php endforeach; ?>
</ul>