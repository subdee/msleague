<?php

$count = count($this->getItems());
$counter = 0;
echo CHtml::openTag('ul');

foreach ($this->getItems() as $m) {

    $class = '';
    if ($counter == 0)
        $class = ' first';
    elseif ($counter == $count - 1)
        $class = ' last';

    echo CHtml::openTag('li', array('class' => $m['class'] . $class));

    if (isset($m['exturl']))
        echo CHtml::openTag('a', array('href' => $m['exturl'], 'target' => '_blank'));
    else
        echo CHtml::openTag('a', array('href' => _url($m['url'])));

    if (is_array($m['img'])) {
        echo CHtml::image(Utils::moduleImageUrl($m['img'][0], $m['img'][1]));

    } else {
        echo CHtml::image(Utils::imageUrl("mainMenu/{$m['img']}.png"));
    }
    echo CHtml::tag('span', array(), $m['title']);

    echo CHtml::closeTag('a');
    echo CHtml::closeTag('li');
    $counter++;
}

echo CHtml::closeTag('ul');
?>