<?php if ($this->getShowSocial()) : ?>
<div class="social-links">
    <iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Ffantasy.onsports.gr&amp;send=false&amp;layout=button_count&amp;width=150&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font=verdana&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:150px; height:21px;" allowTransparency="true"></iframe>
    <g:plusone href="http://fantasy.onsports.gr/"></g:plusone>
    <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://fantasy.onsports.gr" data-text="Onsports Fantasy League" data-count="horizontal">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
</div>
<?php endif; ?>
<?php if ($this->getShowInfo()) : ?>
<div class="general_info">
    <?php echo Utils::date(_app()->localtime->getLocalNow('Y-m-d H:i:s')); ?>
    &nbsp;|
    <?php echo CHtml::link(_t('MANAGERS'), _url('standings/index')) . ': ' . $this->getNoOfManagers(); ?>
    <?php if (!_manager()->hasRole(Role::TEMPORARY)) : ?>
        |
        <?php $market = $this->getIsOpen() ? 'unlocked' : 'locked'; ?>
        <?php echo CHtml::link(_t('STOCKMARKET'), _url('stockmarket/index')) . ': ' . CHtml::image(Utils::imageUrl("generalInfo/$market.png")); ?>
    <?php endif; ?>
</div>
<?php endif; ?>