<?php
$showContent = true;
if (is_array($this->cache)) {
    $showContent = $this->beginCache($this->cache[0], $this->cache[1]);
}
?>
<?php if ($showContent) : ?>
    <div class="widgetbox <?php echo $this->getClass(); ?>">
        <?php if ($this->title) : ?>
            <div class="header">
                <?php
                if ($this->titleImage)
                    echo CHtml::image($this->titleImage);
                ?>
                <div><?php echo $this->title; ?></div>
            </div>
        <?php endif; ?>
        <div class="body">
            <?php
            if ($this->view)
                $this->render($this->view, $this->params);
            elseif ($this->dynamic)
                $this->controller->renderDynamic($this->dynamic);
            elseif ($this->static)
                echo $this->static;
            ?>
            <?php if (isset($this->more['label']) && isset($this->more['url'])) : ?>
                <span class="more">
                    <span class="element">&raquo;</span>
                    <?php echo CHtml::link($this->more['label'], $this->more['url']); ?>
                </span>
            <?php endif; ?>
        </div>
    </div>
    <?php
    if (is_array($this->cache))
        $this->endCache();
    ?>
<?php endif; ?>