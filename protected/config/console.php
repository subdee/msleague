<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'MSLeague Script',
    'language' => 'en',
    // preloading 'log' component
    'preload' => array(
        'log',
    ),
    'onBeginRequest' => array('Controller', 'preloadModules'),
    // autoloading model and component classes
    'import' => array(
        'application.commands.MSLCommand',
        'application.models.Utils',
        'application.models.Gameconfiguration',
        'application.models.ScriptConfiguration',
        'application.models.Manager',
        'application.models.ManagerPasswordReset',
        'application.models.Player',
        'application.models.PlayerDailyHistory',
        'application.models.ManagerTeam',
        'application.models.ManagerTeamPlayer',
        'application.models.ManagerTeamPlayerHistory',
        'application.models.ManagerDailyHistory',
        'application.models.Statistics',
        'application.models.Game',
        'application.models.Timezone',
        'application.models.Role',
        'application.modules/admin.models.Newsletter',
        'application.components.Controller',
        'application.components.WebModule',
        'application.components.LocalTime',
        'application.components.EventSystem',
    ),
    'modules' => array(
//        'sms' => array(
//            'layoutPath' => 'protected/views/layouts',
//            'soap_api_id' => 3306802,
//            'soap_user' => 'manolis',
//            'soap_password' => 'BFQVam02',
//            'soap_url' => 'http://api.clickatell.com/soap/webservice.php?wsdl',
//            'autoinit' => true,
//        ),
        'forum' => array(
//            'layoutPath' => 'protected/views/layouts',
//            'maxShowDiscussionsPerUser' => 5,
//            'maxLoadDiscussionsPerUser' => 20,
            'showInMenu' => false,
            'autoinit' => true,
        ),
    ),
    // application components
    'components' => array(
        // MSLeague
        'eventSystem' => array(
            'class' => 'EventSystem'
        ),
        // Yii
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=msleague',
            'emulatePrepare' => true,
            'username' => 'msleague',
            'password' => 'msleague!@#',
            'charset' => 'utf8',
            'initSQLs' => array("set time_zone='+00:00';"),
//            'enableProfiling' => true,
//            'enableParamLogging' => true
        ),
        'localtime' => array(
            'class' => 'LocalTime',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'info',
                    'categories' => 'script',
                    'logFile' => 'script.log'
                ),
            ),
        ),
    ),
    'params' => array(
        'script' => true
    ),
);
?>
