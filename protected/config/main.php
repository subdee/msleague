<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'MSLeague',
    'language' => 'el',
    'sourceLanguage' => 'en',
//    'theme' => 'onsports',
    // preloading 'log' component
    'preload' => array(
        'log',
    ),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.vendors.*',
    ),
    'onBeginRequest' => array('Controller', 'preloadModules'),
    'modules' => array(
        // uncomment the following to enable the Gii tool
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'msleague',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
        'admin' => array(
            'autoinit' => true,
        ),
        'personalMessage' => array(
            'autoinit' => true,
        ),
        'notification' => array(
            'autoinit' => true,
        ),
        'customLeague' => array(
            'autoinit' => true,
            'modules' => array(
                'customLeagueInvitation' => array(
                    'autoinit' => true,
                    'dependencies' => array('forum'),
                ),
                'customLeagueDiscussion' => array(
                    'autoinit' => true,
                    'dependencies' => array('forum'),
                ),
            ),
        ),
        'forum' => array(
            'layoutPath' => 'protected/views/layouts',
            'maxShowDiscussionsPerUser' => 5,
            'maxLoadDiscussionsPerUser' => 20,
            'showInMenu' => true,
            'autoinit' => true,
        ),
        'matchDiscussion' => array(
            'dependencies' => array('forum'),
            'autoinit' => true,
        ),
        'advisor' => array(
        ),
        'training' => array(
            'components' => array(
                'scheme' => array(
                    'class' => 'training.components.Scheme'
                ),
            ),
            'preload' => array(
                'scheme',
            ),
            'autoinit' => true,
        ),
        'payment' => array(
            'autoinit' => true,
        ),
        'credits' => array(
            'autoinit' => true,
            'dependencies' => array('payment'),
        ),
//        'sms' => array(
//            'layoutPath' => 'protected/views/layouts',
//            'soap_api_id' => 3306802,
//            'soap_user' => 'manolis',
//            'soap_password' => 'BFQVam02',
//            'soap_url' => 'http://api.clickatell.com/soap/webservice.php?wsdl',
//            'autoinit' => true,
//        ),
//        'payPal' => array(
//            'env' => 'sandbox',
//            'account' => array(
//                'username' => 'msl_1322577005_biz_api1.gmail.com',
//                'password' => '1322577049',
//                'signature' => 'APNHhi.-x6oXY285w.6NIpl2e-OtAoc2KstnVZ8XYDIFBdCL2AfwxdO- ',
//                'email' => 'masterstockleague@gmail.com',
//                'identityToken' => false,
//            ),
//            'components' => array(
//                'buttonManager' => array(
////                    'class' => 'payPal.components.PPDbButtonManager'
//                    'class' => 'payPal.components.PPPhpButtonManager',
//                ),
//            ),
//        ),
    ),
    // application components
    'components' => array(
        // MSLeague
        'managerPointSystem' => array(
            'class' => 'ManagerPointSystem'
        ),
        'eventSystem' => array(
            'class' => 'EventSystem'
        ),
//        // Yii
        'urlManager' => array(
            'class' => 'application.components.UrlManager',
            'urlFormat' => 'path',
            'rules' => array(
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
            'showScriptName' => false,
        ),
        'user' => array(
            'class' => 'WebUser',
            'allowAutoLogin' => false,
            'loginUrl' => array('user/login'),
        ),
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'connectionID' => 'db',
        ),
        'cache' => array(
            'class' => 'system.caching.CMemCache',
            'useMemcached' => true,
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=msleague',
            'emulatePrepare' => true,
            'username' => 'msleague',
            'password' => 'msleague!@#',
            'charset' => 'utf8',
            'initSQLs' => array("set time_zone='+00:00';"),
            'schemaCachingDuration'=>360000,
//            'enableProfiling' => true,
//            'enableParamLogging' => true
        ),
        'localtime' => array(
            'class' => 'LocalTime',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error',
                ),
//                array(
//                    'class' => 'CProfileLogRoute',
//                ),
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'info',
                    'categories' => 'actions',
                    'logFile' => 'user.log'
                ),
            ),
        ),
        'messages' => array(
            'class' => 'CGettextMessageSource',
            'catalog' => 'default',
            'cachingDuration' => '3600',
            'useMoFile' => false,
        ),
//        'request'=>array(
//            'enableCsrfValidation'=>true,
//        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        'debugStatus' => true,
        'country' => 'Greece',
        'managerAutoRefresh' => false,
        'features' => array(
            'deleteAccount' => true,
            'chat' => false,
        ),
        'termsOfUseUrl' => 'http://www.onsports.gr/terms-and-conditions',
        'profilePhotoDefaultDimensions' => array('width' => 160, 'height' => 135),
        'profileThumbnailDefaultDimensions' => array('width' => 75, 'height' => 63),
    ),
);
?>
