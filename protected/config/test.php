<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
		'components'=>array(
			'fixture'=>array(
				'class'=>'system.test.CDbFixtureManager',
			),
			'db'=>array(
                                'connectionString' => 'mysql:host=localhost;dbname=msleague_qa',
                                'emulatePrepare' => true,
                                'username' => 'root',
                                'password' => 'hal999!@#',
                                'charset' => 'utf8',
                        ),
		),
	)
);
