<?php

class ChatController extends Controller {

    public function actionPost() {

        if (!Utils::isAjax())
            return;

        if (isset($_POST['shout'])) {
            $chat = new Chat;
            $chat->manager_id = _user()->id;
            $chat->shout = strip_tags($_POST['shout']);
            $chat->game_id = null;
            $chat->date_entered = _app()->localtime->getLocalNow();

            if (!$chat->save()) {
                Utils::jsonReturn(array('error' => Debug::arObjectErrors($chat)));
            }
        }

        $shouts = Chat::model()->getLastShouts(100);

        Utils::jsonReturn(array(
            'html' => $this->renderPartial('_newChat', array(
                'shouts' => $shouts
                ), true)
        ));
    }

}

?>
