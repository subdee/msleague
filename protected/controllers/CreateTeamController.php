<?php

class CreateTeamController extends Controller {

    public function filters() {
        return array('accessControl',);
    }

    public function accessRules() {
        return array(
            array('deny',
                'users' => array('?')
            ),
            array('deny',
                'expression' => '!Gameconfiguration::model()->get("is_season_open")',
                'message' => _t('You cannot create your team while the season is off.')
            ),
            array('deny',
                'expression' => '_manager()->isBanned()',
                'message' => _t('You cannot create your team because you are banned.')
            ),
            array('allow',
                'expression' => '_manager()->hasRole(Role::TEMPORARY | Role::RESET)',
            ),
            array('deny',
                'users' => array('*')
            )
        );
    }

    public function actionIndex() {
        _setPageTitle(_t('Create your team'));

        if (!empty(_team()->name)) {
            _app()->session['step1'] = true;
            _app()->session['step2'] = true;
            _app()->session['showPitch'] = true;
        } else {
            _app()->session['finishedTemp'] = false;
        }

        $team = new TempTeam;
        if (!_app()->session['showPitch']) {
            if (isset($_POST['createTeam'])) {
                _app()->session['step2'] = true;
                _app()->session['showPitch'] = true;
            } elseif (isset($_POST['TempTeam']) && !_app()->session['step1']) {
                $team->attributes = $_POST['TempTeam'];
                if ($team->create()) {
                    _app()->session['finishedTemp'] = true;
                    _app()->session['step1'] = true;
                }
            } else {
                _app()->session['step1'] = false;
            }
        }

        if (_app()->session['finishedTemp'] === false && !_app()->session['showPitch']) {
            $this->resetTemp();
        }

        _cs()->registerCoreScript('yii');
        _cs()->registerScriptFile(_bu('js/lib/jquery.dataTables.min.js'));
        _cs()->registerScriptFile(_bu('js/createTeam.js'), CClientScript::POS_END);

        $positions = Position::model()->findAllBySql('select name,abbreviation from position');
        $players = array();
        foreach ($positions as $position) {
            $params = array();
            $mtp = _team()->getPlayerByPos($position->abbreviation);
            if ($mtp) {
                $players[$position->abbreviation][0] = new PitchPlayer(array(
                        'pitchPosition' => $position->name,
                        'id' => strtolower($position->abbreviation)
                        ),
                        $mtp->player
                );
            } else {
                $players[$position->abbreviation][0] = new PitchPlayer(array(
                        'id' => strtolower($position->abbreviation)
                    ));
            }
        }

        $this->render('index', array(
            'model' => $team,
            'players' => $players,
            'numOfPlayers' => _team()->numOfPlayers(),
        ));
    }

    public function actionViewPlayers($position, $pitchPosition) {
        if (_app()->request->isAjaxRequest) {
            _app()->session['pitchPosition'] = $pitchPosition;

// Get all players
            $players = Player::model()->findPlayers(array('byPosition' => $position, 'order' => 'current_value desc'));

// Get a display list to use
            $displayList = PlayerListDisplayBuy::create(_manager(), $players, array('maxStockLimit' => 1));

            $this->renderPartial('_viewPlayers', array(
                'displayList' => $displayList,
                'position' => $position,
                'teams' => Team::model()->findAll(),
            ));
            _app()->end();
        }
    }

    public function actionAddTempPlayer($player_id) {
        if (_team()->name)
            $this->addTempPlayer($player_id, _app()->session['pitchPosition']);

        $this->redirect('index');
    }

    public function actionDeleteTempPlayer($position) {
        $this->deleteTempPlayer($position);
        $this->redirect('index');
    }

    public function actionProcessTempTeam() {
        if (isset($_POST['save'])) {
            /* extra checks just for hacking */
            $numOfTeamPlayers = _team()->numOfPlayers();
            $minNumOfPlayers = Gameconfiguration::model()->get('min_number_of_portfolio_players');
            if ($numOfTeamPlayers != $minNumOfPlayers) {
                Debug::logToWindow("You have $numOfTeamPlayers players instead of $minNumOfPlayers");
                $this->redirect('index');
            }
            if (!_manager()->hasRole(Role::TEMPORARY | Role::RESET)) {
                Debug::logToWindow('Your role is ' . _manager()->role);
                $this->redirect('index');
            }
            if (!_team()->name) {
                Debug::logToWindow('Your team does not have a name');
                $this->redirect('index');
            }

            // Open transaction
            $transaction = _app()->db->beginTransaction();
            try {
                // Trigger init events
                Utils::triggerEvent('onBeforeProcessTeam', $this);

                // Process team
                $this->processTeam();

                // picks a leader
                _team()->autoLeader();

                // Trigger end events
                Utils::triggerEvent('onAfterProcessTeam', $this);

                // Commit changes and reset session
                $transaction->commit();
                $this->resetTemp();

                _manager()->refresh();

                // Redirect on success
                _redirect('site/index');
            } catch (CDBException $e) {
                $transaction->rollback();
                Utils::notice('notice', _t('An error occured while processing your team'), 'error');
                Debug::logToWindow($e->getMessage(), 'process team');
            } catch (CValidateException $e) {
                $transaction->rollback();
                Utils::notice('notice', _t('An error occured while processing your team'), 'error');
            }
        } else {
            // empty team
            $this->clearTeam();
        }

        // Refresh here as well in case of error, to revert the session to the old values.
        _manager()->refresh();

        $this->redirect('index');
    }

    public function actionCreateRandomTeam() {
        $this->createRandomTeam();
        $this->redirect('index');
    }

    /**
     *
     */
    private function createRandomTeam() {
        // Get all players
        $players = Player::model()->findPlayers(array('order' => 'current_value asc'));

        // Filter players
        $displayList = PlayerListDisplayBuy::create(_manager(), $players, array('maxStockLimit' => 1));

        // Get all positions
        $pitchPositions = Position::model()->findAll();

        // Positions already filled
        $filled = array();

        // Untouched positions
        $untouched = array('Center Defender', 'Center Midfielder', 'Center Striker');

        // foreach player
        foreach ($displayList as $dl) {
            // if the position is not filled out and he is available, choose him
            if (!$dl->isActive || $dl->isOwned)
                continue;

            foreach ($pitchPositions as $pp) {
                if (!in_array($pp->name, $untouched)) {
                    // is player right for this position?
                    if ($dl->player->basic_position_id == $pp->basic_position_id) {
                        // is position filled?
                        if (!in_array($pp->name, $filled)) {
                            // try adding him
                            if ($this->addTempPlayer($dl->player->id, $pp->name, false)) {
                                // mark position as filled
                                $filled[] = $pp->name;
                                $dl->isOwned = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     *
     * @param type $player_id
     */
    private function addTempPlayer($player_id, $position, $forceReplace=true) {

        $dbt = _app()->db->beginTransaction();
        try {

            // subtract old player from temp team
            $mtp = _team()->getPlayerByPos($position, false);
            if ($mtp && $forceReplace) {
                // update manager's portfolio
                _manager()->portfolio_cash += $mtp->player->current_value;
                _manager()->portfolio_share_value -= $mtp->player->current_value;
                // remove player
                $mtp->delete();
            } elseif ($mtp && !$forceReplace) {
                $dbt->rollback();
                return true;
            }

            // create new player
            $mtp = ManagerTeamPlayer::createNew($position);
            $mtp->manager_team_id = _team()->id;
            $mtp->player_id = $player_id;

            if ($mtp->player->bank_shares <= 0)
                throw new CException(_t('There are no shares of this player available at the moment.'));

            if ((_manager()->portfolio_cash - $mtp->player->current_value) < 0)
                throw new CException(_t('Not enough cash.'));

            $mtp->save();

            $mtp->player->bank_shares -= 1;
            $mtp->player->save(true, array('bank_shares'));

            // update manager's portfolio
            _manager()->portfolio_cash -= $mtp->player->current_value;
            _manager()->portfolio_share_value += $mtp->player->current_value;
            _manager()->saveAttributes(array('portfolio_cash', 'portfolio_share_value'));

            $dbt->commit();
            _manager()->refresh();

            return true;
        } catch (CDbException $e) {
            $dbt->rollback();
            Utils::notice('notice', $e->getMessage(), 'error');
            Debug::logToWindow($e->getMessage());
        } catch (CException $e) {
            $dbt->rollback();
            Debug::logToWindow(Debug::exceptionDetailString($e));
        }

        return false;
    }

    /**
     *
     * @param type $position
     */
    private function deleteTempPlayer($position) {
        $mtp = _team()->getPlayerByPos($position, false);
        if ($mtp) {
            $dbt = _app()->db->beginTransaction();
            try {
                // get player
                // remove player from team
                $mtp->delete();

                // update manager's portfolio
                _manager()->portfolio_cash += $mtp->player->current_value;
                _manager()->portfolio_share_value -= $mtp->player->current_value;
                _manager()->saveAttributes(array('portfolio_cash', 'portfolio_share_value'));

                $dbt->commit();
                _manager()->refresh();

                return true;
            } catch (CDbException $e) {
                $dbt->rollback();
                Utils::notice('notice', _t('An error occured white removing the player.'), 'error');
                Debug::logToWindow($e->getMessage());
            }
        }

        return false;
    }

    /**
     *
     */
    private function processTeam() {

        // update manager team's create date
        _team()->reset();
        if (!_team()->save())
            throw new CValidateException(_team());

        if (_manager()->hasRole(Role::TEMPORARY)) {
            // Give manager team a rank
            Rank::giveNewTeamRank(_manager()->id);
        }

        // update manager's role and portfolio value
        _manager()->refresh();
        _manager()->role = Role::MANAGER;
        _manager()->portfolio_share_value = 0;
        foreach (_team()->managerTeamPlayers as $player) {
            _manager()->portfolio_share_value += $player->player->current_value;
        }

        if (!_manager()->save(true, array('role', 'portfolio_share_value')))
            throw new CValidateException(_manager());

        //Add first historical record for manager
        $managerHistory = new ManagerDailyHistory;
        $managerHistory->manager_id = _manager()->id;
        $managerHistory->date = Utils::getUserTime();
        $managerHistory->portfolio_value = _manager()->portfolio_cash + _manager()->portfolio_share_value;
        if (!$managerHistory->save())
            throw new CValidateException($managerHistory);
    }

    /**
     *
     */
    private function clearTeam() {
        $dbt = _app()->db->beginTransaction();
        try {
            foreach (_team()->managerTeamPlayers as $mtp) {
                _manager()->portfolio_cash += $mtp->player->current_value;
                _manager()->portfolio_share_value -= $mtp->player->current_value;

                $mtp->delete();
            }
            _manager()->saveAttributes(array('portfolio_cash', 'portfolio_share_value'));

            $dbt->commit();
            _manager()->refresh();

            return true;
        } catch (CDbException $e) {
            $dbt->rollback();
            Utils::notice('notice', _t('An error occured while reseting your team.'), 'error');
            Debug::logToWindow($e->getMessage());
        }

        return false;
    }

    /**
     *
     */
    private function resetTemp() {
        _app()->session->remove('step1');
        _app()->session->remove('step2');
        _app()->session->remove('showPitch');
    }

}
