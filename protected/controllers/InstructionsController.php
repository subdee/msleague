<?php

class InstructionsController extends Controller {

    public function filters() {
        return array('accessControl');
    }

    public function accessRules() {
        return array(
            array(
                'allow',
                'users' => array('?')
            ),
            array(
                'allow',
                'users' => array('@')
            ),
            array(
                'deny',
                'users' => array('*')
            )
        );
    }

    public function subMenuItems() {
        return array(
            array('index', _t('Instructions')),
            array('faq', _t('FAQ')),
            array('screenshots', _t('Screenshots')),
        );
    }

    public function init() {
        parent::init();
    }

    public function actionIndex() {
        $this->render('instructions');
    }

    public function actionFaq() {
        _setPageTitle(_t('FAQ'));
        $this->render('faq');
    }

    public function actionScreenshots() {
        Yii::import('ext.jqPrettyPhoto');

        $options = array(
            'slideshow' => 5000,
            'autoplay_slideshow' => false,
            'show_title' => false
        );

        $this->render('screenshots', array(
            'options' => $options
        ));
    }

    public function instructions() {
        $instructions = StaticContent::model()->getText('Instructions');
        $instructions = Utils::replaceVars($instructions);

        $events = array(
            array('Minutes Played'),
            array('Minutes Played Less', 1, 1, 1, 1),
            array('Goals Scored'),
            array('Assists'),
            array('No Goals'),
            array('Goals Against'),
            array('Clean Sheet'),
            array('Red Cards'),
            array('Own Goals'),
            array('Penalties Saved'),
            array('Missed Penalties'),
        );

        $pos = BasicPosition::model()->findAll();
        $all = Event::model()->findAll();

        foreach ($all as $a) {
            $i[$a->name][$a->basicPosition->name] = $a->points_awarded;
        }

        $pointTable = $this->renderPartial('_pointTable', array(
            'pos' => $pos,
            'events' => $events,
            'i' => $i,
            ), true);

        $instructions = str_replace('{table}', $pointTable, $instructions);

        return $instructions;
    }

    public function faq() {
        $faq = StaticContent::model()->getText('FAQ');
        $faq = Utils::replaceVars($faq);

        return $faq;
    }

}