<?php

class ManagersController extends Controller {

    public function filters() {
        return array('accessControl',);
    }

    public function accessRules() {
        return array(
            array('deny',
                'users' => array('?'), // Deby guests
            ),
            array(
                'allow',
                'users' => array('@'),
                'actions' => array('acceptTerms'), // Allow all managers to accept the new ToU
            ),
            array('allow',
                'actions' => array('points', 'portfolio'),
                'expression' => array('Manager', 'isBanned'),
                'message' => 'You have been denied from accessing this functionality because you have been banned. Please contact admin for more info.'
            ),
            array('deny',
                'actions' => array('points', 'portfolio'),
                'expression' => '_manager()->hasRole(Role::RESET)',
            ),
            array('allow',
                'expression' => '_manager()->hasRole(Role::MANAGER | Role::RESET)',
            ),
            array('deny',
                'users' => array('*')
            )
        );
    }

    public function actionPoints() {
        _setPageTitle(_t("{name}'s points", array('{name}' => _user()->name)));
        $this->render('points');
    }

    public function actionPortfolio() {
        _setPageTitle(_t("{name}'s budget", array('{name}' => _user()->name)));

        $team = null;

        $portfolioPlayers = new ManagerTeamPlayer('search');

        Utils::registerJsFile('lib/jquery.tools.tooltip.min');
        $this->render('portfolio', array('portfolioPlayers' => $portfolioPlayers));
    }

    /**
     * Blocks a manager
     * @param int $id
     */
    public function actionBlock($id) {
        $ret = _manager()->blockManager($id);
        if ($ret > 0) {
            Utils::notice(
                'blockaction', _t('You will not receive any messages from this user from now on! View and manage your block-list by clicking {url}', array(
                    '{url}' => '<a href="' . _url('settings/blockedUsers', array('tab' => 2)) . '">' . _t('here') . '</a>',
                )), 'warning'
            );
        } elseif ($ret == Manager::BLOCK_ERR_SELF) {
            Utils::notice('blockaction', _t('You cannot block yourself.'), 'error');
        } elseif ($ret == Manager::BLOCK_ERR_ADMIN) {
            Utils::notice('blockaction', _t('You cannot block an Administrator.'), 'error');
        } else {
            Utils::notice('blockaction', _t('An error occured. Blocking of user failed.'), 'error');
        }
        _redirect('profile/index', array('id' => $id));
    }

    /**
     * Unblocks a manager
     * @param int $id
     */
    public function actionUnblock($id) {
        if (_manager()->unblockManager($id)) {
            Utils::notice('blockaction', _t('You have successfully unblocked this user! You can receive messages from him from now on.'), 'success');
        } else {
            Utils::notice('blockaction', _t('An error occured. Unblocking of user failed.'), 'error');
        }
        _redirect('profile/index', array('id' => $id));
    }

    public function actionInvite() {
        $canInvite = _manager()->canInvite();
        $form = null;
        if ($canInvite) {
            $form = new InviteFriendForm;
            if (isset($_POST['InviteFriendForm'])) {
                $form->attributes = $_POST['InviteFriendForm'];
                if ($form->validate()) {
                    $form->invite();
                }
            }
        }

        _setPageTitle(_t('Invite friends'));
        $this->render('invite', array('model' => $form, 'canInvite' => $canInvite, 'username' => _manager()->username));
    }

    public function actionAddWatchlist($id) {
        $count = Watchlist::model()->countByAttributes(array(
            'manager_id' => _manager()->id
            ));
        if ($count < Gameconfiguration::model()->get('max_watchlist_items')) {
            $watchlist = new Watchlist;
            $watchlist->manager_id = _manager()->id;
            $watchlist->player_id = $id;
            $watchlist->save();

            Utils::notice('watchlistMsg', _t('You have succesfully added {player} to your watchlist', array('{player}' => Player::model()->findByPk($id)->shortname)), 'success');
        } else {
            Utils::notice('watchlistMsg', _t('You cannot add any more players to your watchlist'), 'error');
        }

        $this->redirect('watchList');
    }

    public function actionRemoveWatchlist($id) {
        $item = Watchlist::model()->findByPk(array(
            'manager_id' => _manager()->id,
            'player_id' => $id
            ));
        if (!$item)
            throw new CHttpException('404');

        $player = Player::model()->findByPk($id);
        if (!$player)
            throw new CHttpException('404');

        if ($item->delete())
            Utils::notice('watchlistMsg', _t('You have succesfully removed {player} from your watchlist', array('{player}' => $player->shortname)), 'success');
        else
            Utils::notice('watchlistMsg', _t('{player} could not be removed from your watchlist', array('{player}' => $player->shortname)), 'error');

        if (_app()->request->isAjaxRequest) {
           $notice = $this->widget('Notice', array('session' => 'watchlistMsg'), true);
           $count = _t('You have {players} players on your watchlist.',array('{players}' => Watchlist::model()->count('manager_id = :id', array(':id' => _manager()->id))));
           Utils::jsonReturn(array(
               'notice' => $notice,
               'count' => $count,
           ));
        } else
            $this->redirect('watchList');
    }

    public function actionWatchlist() {
        $players = Watchlist::model()->findAll('manager_id = :id', array(':id' => _manager()->id));

        if ($players == null)
            Utils::notice('watchlistMsg', _t('You have no players in your watchlist'), 'info');

        $this->render('watchList', array(
            'players' => $players
        ));
    }

    public function actionAcceptTerms() {
        _manager()->accepted_terms = true;
        _manager()->saveAttributes(array('accepted_terms'));
        _return();
    }

}
