<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SuperleagueController
 *
 * @author subdee
 */
class MatchController extends Controller {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl'
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('deny',
                'actions' => array('match'),
                'users' => array('?'),
            ),
            array('allow',
                'users' => array('*'),
            ),
        );
    }

    public function subMenuItems() {
        return array(
            array('index', _t('Matchdays')),
            array('byteam', _t('By team')),
            array('match', _t('Match'), 'condition' => _app()->controller->route == "match/match"),
        );
    }

    public function actionIndex($date=0) {
        if (!$date) {
            $date = _app()->localtime->getGameNow('Y-m-d');
            $longdate = _app()->localtime->getGameNow('Y-m-d H:i:s');
        } else {
            $longdate = $date . ' ' . _app()->localtime->getGameNow('H:i:s');
        }

        $nextGame = Game::model()->find(array(
            'condition' => 'date(convert_tz(date_played,"UTC",:tz)) > date(:date)',
            'params' => array(
                ':date' => $longdate,
                ':tz' => _timezone()
            ),
            'select' => 'min(date_played) as dateplayed'
            ));

        $previousGame = Game::model()->find(array(
            'condition' => 'date(convert_tz(date_played,"UTC",:tz)) < date(:date)',
            'params' => array(
                ':date' => $longdate,
                ':tz' => _timezone()
            ),
            'select' => 'max(date_played) as dateplayed'
            ));

        if (Game::model()->count('date(convert_tz(date_played,"UTC",:tz)) = date(:date)', array(':date' => $date, ':tz' => _timezone())) == 0) {
            if ($nextGame->dateplayed != null) {
                $date = $nextGame->dateplayed;
            } elseif ($previousGame->dateplayed != null) {
                $date = $previousGame->dateplayed;
            }
            $longdate = $date . ' ' . _app()->localtime->getGameNow('H:i:s');
            $nextGame = Game::model()->find(array(
                'condition' => 'date(convert_tz(date_played,"UTC",:tz)) > date(:date)',
                'params' => array(
                    ':date' => $longdate,
                    ':tz' => _timezone()
                ),
                'select' => 'min(date_played) as dateplayed'
                ));

            $previousGame = Game::model()->find(array(
                'condition' => 'date(convert_tz(date_played,"UTC",:tz)) < date(:date)',
                'params' => array(
                    ':date' => $longdate,
                    ':tz' => _timezone()
                ),
                'select' => 'max(date_played) as dateplayed'
                ));
        }

        $matchesPerMonth = new Game('search');
        $matchesPerMonth->date_played = $date;

        _setPageTitle(_t('League fixtures & results'));
        $this->render('index', array(
            'matchesPerMonth' => $matchesPerMonth,
            'date' => $date,
            'nextGame' => $nextGame,
            'previousGame' => $previousGame
        ));
    }

    public function actionByTeam($team=0) {
        if (!Team::model()->exists('id = :id', array(':id' => $team))) {
            if (_is_logged_in() && !_manager()->hasRole(Role::TEMPORARY)) {
                $team = _manager()->profile->team->id;
            } else {
                $team = 0;
            }
        }

        $matchesPerTeam = new Game('searchByTeam');
        $matchesPerTeam->team_home = $team;
        $matchesPerTeam->team_away = $team;

        _setPageTitle(_t('League fixtures & results'));
        $this->render('byteam', array(
            'team' => $team,
            'teams' => Team::model()->findAllBySql('select id,name from team order by name asc'),
            'matchesPerTeam' => $matchesPerTeam,
        ));
    }

    public function actionMatch($id) {

        if (!$id)
            $this->redirect('index');

        $game = Game::model()->with('teamHome0', 'teamAway0')->findByPk($id);
        if (!$game)
            $this->redirect('index');

        $events = array(
            'home' => array(),
            'away' => array(),
            'playerHomeCount' => 0,
            'playerAwayCount' => 0,
        );

        $gameEvents = GamePlayer::model()->
            with(array('game', 'player'))->
            findAll(array(
            'condition' => 'game_id = :id and e.is_team_event = false',
            'params' => array('id' => $id),
            'join' => 'left join event e on e.id = t.event_id',
            'order' => 'player.basic_position_id asc, player.shortname asc'
            ));

        foreach ($gameEvents as $gameEvent) {
            if ($gameEvent->is_home_team) {
                $dummy = &$events['home'];
            } else {
                $dummy = &$events['away'];
            }

            $dummy[$gameEvent->player->shortname]['details'] = array(
                'positionAbbr' => $gameEvent->player->basicPosition->abbreviation,
                'isListed' => $gameEvent->player->list_date != null ? true : false,
                'id' => $gameEvent->player->id,
                'positionText' => $gameEvent->player->basicPosition->getTranslatedAbbr(),
            );

            $dummy[$gameEvent->player->shortname]['events'][$gameEvent->event->name] = $gameEvent->event_value;
        }

        $events['playerHomeCount'] = count($events['home']);
        $events['playerAwayCount'] = count($events['away']);

        $params = array(
            'events' => $events,
            'game' => $game,
        );

        _setPageTitle(_t('{home} - {away}', array(
                '{home}' => $game->teamHome0->name,
                '{away}' => $game->teamAway0->name
            )));
        $this->render('match', $params);
    }
    
    public function actionVote() {
        if (_app()->request->isAjaxRequest){
            if (isset($_POST['game']) && isset($_POST['vote'])){
                if (!ManagerMatchVote::model()->exists('manager_id = :id and game_id = :gid',array(':id' => _manager()->id, ':gid' => $_POST['game']))){
                    $vote = new ManagerMatchVote;
                    $vote->game_id = $_POST['game'];
                    $vote->manager_id = _manager()->id;
                    $vote->vote = $_POST['vote'];
                    if (!$vote->save())
                        error_log(_get_ar_last_error());
                }
            }
        }
        
        echo $this->widget('MatchVoting',array('game' => $_POST['game']),true);
        return;
    }

}

?>
