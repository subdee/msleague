<?php

class PasswordResetController extends Controller {

    public function filters() {
        return array('accessControl');
    }

    public function accessRules() {
        return array(
            array(
                'allow',
                'actions' => array('captcha'),
                'users' => array('*')),
            array(
                'deny',
                'users' => array('@')),
            array(
                'allow',
                'users' => array('*')),
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
        );
    }

    public function init() {
        parent::init();
        _setPageTitle(_t('Password reset'));
    }

    public function actionIndex() {
        $form = new UserPasswordResetForm('forgot');
        if (isset($_POST['UserPasswordResetForm'])) {
            $form->attributes = $_POST['UserPasswordResetForm'];
            if ($form->sendResetEmail()) {
                $this->redirect('forgotCompleted');
            }
        }

        $this->render('forgot', array(
            'model' => $form,
        ));
    }

    public function actionReset($code, $email) {
        _cs()->registerScriptFile(_bu('js/lib/jquery.tools.tooltip.min.js'));
        _cs()->registerScriptFile(_bu('js/lib/passwordStrengthIndicator.js'));

        // handle reset form page
        $manager = Manager::model()->findByAttributes(array('email' => $email));
        if (!$manager)
            throw new CHttpException(403, _t('You are not allowed to access this page.'));

        if ($manager->passwordReset === null)
            throw new CHttpException(403, _t('You are not allowed to access this page.'));

        if ($manager->email_code === null || $manager->email_code != $code)
            throw new CHttpException(403, _t('You are not allowed to access this page.'));

        $form = new UserPasswordResetForm('reset');
        if (isset($_POST['UserPasswordResetForm'])) {
            $form->attributes = $_POST['UserPasswordResetForm'];
            if ($form->resetPassword($manager)) {
                $this->redirect('resetCompleted');
            }
        }

        $this->render('reset', array(
            'model' => $form,
            'manager_id' => $manager->id,
        ));
    }

    /**
     *
     */
    public function actionResetCompleted() {
        $this->render('resetCompleted');
    }

    /**
     *
     */
    public function actionForgotCompleted() {
        $this->render('forgotCompleted');
    }

}