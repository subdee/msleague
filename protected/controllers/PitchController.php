<?php

class PitchController extends Controller {

    public function filters() {
        return array('accessControl',);
    }

    public function accessRules() {
        return array(
            array('deny',
                'users' => array('?')
            ),
            array('allow',
                'actions' => array('index'),
                'expression' => array('Manager', 'isBanned'),
                'message' => 'You have been denied from accessing this functionality because you have been banned. Please contact admin for more info.'
            ),
            array('allow',
                'expression' => '_manager()->hasRole(Role::MANAGER | Role::RESET)',
            ),
            array('deny',
                'users' => array('*')
            )
        );
    }

    public function actionIndex() {
        if (_manager()->hasRole(Role::RESET))
            $this->redirect(_url('createTeam/index'));

        $isOpen = Gameconfiguration::model()->isOpen();
        $isBanned = _manager()->isBanned();
        $nextMatch = Gameconfiguration::model()->getCloseTime();
        $manualClosed = Gameconfiguration::model()->get('is_enabled');

        if ($isOpen) {
            _cs()->registerScriptFile(_bu('js/subevents.js'), CClientScript::POS_HEAD);
        }

        $players = array();
        foreach (_team()->managerTeamPlayers as $mtp) {
            $players[$mtp->position->abbreviation][] = new PitchPlayer(array(
                    'shares' => $mtp->shares,
                    'isLeader' => $mtp->isLeader(),
                    'isSubstitute' => $mtp->isSubstitute(),
                    'id' => strtolower($mtp->position->abbreviation),
                    'clickable' => $isOpen && !$isBanned,
                    ),
                    $mtp->player
            );
        } // foreach

        _setPageTitle(_team()->name);

        $this->render('index', array(
            'nextMatch' => $nextMatch,
            'isOpen' => $isOpen,
            'isBanned' => $isBanned,
            'formations' => Formation::model()->findAll(),
            'players' => $players,
            'manualClosed' => $manualClosed
        ));
    }

    public function actionMakeSubstitution($in, $out) {

        // Get players
        $playerIn = _team()->getPlayerById($in);
        $playerOut = _team()->getPlayerById($out);
        if (!($playerIn && $playerOut))
            throw new CHttpException('404');

        try {
            $trans = _app()->db->beginTransaction();

            // Assign the leader
            _team()->swapPlayers($playerOut, $playerIn);

            $trans->commit();
            _manager()->refresh();

        } catch (CDbException $e) {
            $trans->rollback();
            Utils::notice('changeError', _t('Failed to make substitution.'), 'error');
            Debug::logToWindow($e->getMessage(), 'make substitution');
        }

        $this->redirect('index');
    }

    public function actionAssignLeader($id) {
        // Get player
        $player = _team()->getPlayerById($id);
        if (!$player)
            throw new CHttpException('404');

        try {
            $trans = _app()->db->beginTransaction();

            // Assign the leader
            _team()->assignLeader($player);

            $trans->commit();
            _manager()->refresh();

        } catch (CDbException $e) {
            $trans->rollback();
            Utils::notice('changeError', _t('Failed to assign leader.'), 'error');
            Debug::logToWindow($e->getMessage(), 'assign leader');
        }

        $this->redirect('index');
    }

    public function actionChangeFormation($formation) {
        if (!_team()->changeFormation($formation)) {
            Utils::notice('changeError', _t('Failed to change formation.'), 'error');
        }
        $this->redirect('index');
    }

}
