<?php

class PlayersController extends Controller {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function subMenuItems() {
        return array(
            array('index', _t('Stocks')),
            array('stats', _t('Statistics')),
            array('managers/watchList', _t('Watchlist'), 'controller' => true, 'condition' => Utils::isLoggedIn() && _manager()->hasRole(Role::MANAGER | Role::RESET)),
            array('view', _t('Player profile'), 'condition' => _controller()->route == 'players/view'),
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('deny',
                'actions' => array('pitch', 'view', 'events'),
                'users' => array('?')
            ),
            array('allow',
                'actions' => array('view'),
                'expression' => '_manager()->hasRole(Role::TEMPORARY)',
            ),
            array('allow',
                'actions' => array('index', 'stats'),
                'users' => array('?'),
            ),
            array('allow',
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionView($plId) {

        $player = Player::model()->findByPk($plId);
        if (!$player) {
            throw new CHttpException('403', _t('The player you requested was not found.'));
        }

        Utils::registerJsFile('lib/jquery.tools.tooltip.min');

        $st = array();
        $p = array();
        $canAdd = true;

        $lastMatches = new Game('searchMatches');
        $nextMatches = new Game('searchMatches');
        $playerLastDaily = PlayerDailyHistory::model()->find('player_id = :plId order by date desc', array(':plId' => $plId));

        //Get stock change
        if ($playerLastDaily) {
            $change = (($player->current_value - $playerLastDaily->value) / $playerLastDaily->value);
        } else {
            $change = 0;
        }

        $playerTotalStats = Player::model()->getTotalStatsPerPlayer($plId);

        //Get the stock values of a player and create a line chart
        $stocks = PlayerDailyHistory::model()->getStocks($plId);
        if (!$stocks) {
            $stocks = Player::model()->findByPk($player->id);
            $hiSt = $stocks->current_value;
            $loSt = $stocks->current_value;
            $st['date'][0] = date('d-M');
            $st['value'][0] = (float) $stocks->current_value;
        } else {
            $i = 0;
            $hiSt = 0;
            $loSt = 100000;
            foreach ($stocks as $stock) {
                if ($stock->value > $hiSt)
                    $hiSt = $stock->value;
                if ($stock->value < $loSt)
                    $loSt = $stock->value;
                $st['date'][$i] = _app()->dateFormatter->format('d/M', $stock->date);
                $st['value'][$i] = (float) $stock->value;
                $i++;
            }
            if ($player->current_value > $hiSt)
                $hiSt = $player->current_value;
            if ($player->current_value < $loSt)
                $loSt = $player->current_value;
            $st['date'][$i] = _app()->dateFormatter->format('d/M', date('d-M'));
            $st['value'][$i] = (float) $player->current_value;
        }

        $total_shares = Gameconfiguration::model()->find()->total_shares_per_player;
        $ratingCount = ManagerPlayerRating::model()->count('player_id = :plId', array(':plId' => $plId));
        $count = Watchlist::model()->count('manager_id = :id', array(':id' => _manager()->id));
        if ($count >= Gameconfiguration::model()->get('max_watchlist_items'))
            $canAdd = false;

        _setPageTitle($player->name);
        $this->render('view', array(
            'player' => $player,
            'change' => $change,
            'playerTotalStats' => $playerTotalStats,
            'st' => $st,
            'hiSt' => $hiSt,
            'loSt' => $loSt,
            'total_shares' => $total_shares,
            'lastMatches' => $lastMatches,
            'nextMatches' => $nextMatches,
            'ratingCount' => $ratingCount,
            'canAdd' => $canAdd
        ));
    }

    public function actionIndex() {

        $name = _t('Search player...');

        $players = new Player('search');

        if (isset($_GET['Player'])) {
            $players->attributes = $_GET['Player'];
            if ($_GET['Player']['shortname'] == _t('Search player...'))
                $players->shortname = null;
            $name = $_GET['Player']['shortname'];
        }

        _setPageTitle(_t("Players' stock information"));
        $this->render('index', array('players' => $players, 'name' => $name));
    }

    public function actionStats() {

        $name = _t('Search player...');

        $players = new Player('statSearch');

        if (isset($_GET['Player'])) {
            $players->attributes = $_GET['Player'];
            if ($_GET['Player']['shortname'] == _t('Search player...'))
                $players->shortname = null;
            $name = $_GET['Player']['shortname'];
        }

        _setPageTitle(_t('Player statistics'));
        $this->render('stats', array('players' => $players, 'name' => $name));
    }

    public function actionPitch() {
        $de = $mi = $st = 1;
        $pos = '';

        $td = array();
        $pl = Player::model()->getTop11();
        foreach ($pl as $top) {
            switch ($top->abbreviation) {
                case 'DE':
                    if ($de == 1)
                        $pos = 'LD';
                    elseif ($de == 2)
                        $pos = 'CLD';
                    elseif ($de == 3)
                        $pos = 'CRD';
                    elseif ($de == 4)
                        $pos = 'RD';
                    $de++;
                    break;
                case 'MF':
                    if ($mi == 1)
                        $pos = 'LM';
                    elseif ($mi == 2)
                        $pos = 'CLM';
                    elseif ($mi == 3)
                        $pos = 'CRM';
                    elseif ($mi == 4)
                        $pos = 'RM';
                    $mi++;
                    break;
                case 'ST':
                    if ($st == 1)
                        $pos = 'LS';
                    elseif ($st == 2)
                        $pos = 'RS';
                    $st++;
                    break;
                default:
                    $pos = $top->abbreviation;
                    break;
            }

            $td[$pos][0] = new PitchPlayer(array('plpoints' => $top->total_points, 'clickable' => false), $top);
        } // foreach

        _setPageTitle(_t('Top 11'));
        $this->render('pitch', array(
            'td' => $td,
        ));
    }

    public function actionEvents($player, $game) {
        if (Utils::isAjax()) {

            $criteria = new CDbCriteria;
            $criteria->addCondition('game_id = :game');
            $criteria->addCondition('player_id = :player');
            $criteria->addCondition('is_team_event = 0');
            $criteria->params = array(':game' => $game, ':player' => $player);
//            $criteria->order = 'is_team_event';

            $events = GamePlayer::model()->with('event')->findAll($criteria);

            $this->renderPartial('_playerEvents', array(
                'game' => $game,
                'events' => $events,
            ));
        }
        _app()->end();
    }

    public function actionRating($id, $rating) {
        $ratings = ManagerPlayerRating::model()->findByAttributes(array(
            'player_id' => $id,
            'manager_id' => _manager()->id
            ));

        if ($ratings == null) {
            $ratings = new ManagerPlayerRating;
            $ratings->player_id = $id;
            $ratings->manager_id = _manager()->id;
        }
        $ratings->rating = $rating;
        $ratings->save();

        $player = Player::model()->findByPk($id);
        $player->average_rating = round($ratings->getAverage($id));
        $player->save(false, array('average_rating'));

        $ratingCount = ManagerPlayerRating::model()->count('player_id = :id', array(':id' => $id));

        Utils::jsonReturn(array('rating' => $player->average_rating, 'votes' => '(' . $ratingCount . ' ' . _t('votes') . ')'));

        return;
    }

}
