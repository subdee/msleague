<?php

class ProfileController extends Controller {

    public $model;

    public function filters() {
        return array('accessControl',
        );
    }

    public function init() {
        parent::init();
    }

    public function accessRules() {
        return array(
            array('deny',
                'users' => array('?')
            ),
            array('deny',
                'actions' => array('updateProfile', 'uploadPhoto', 'removePhoto'),
                'expression' => array('Manager', 'isBanned'),
            ),
            array('allow',
                'actions' => array('index', 'players'),
                'expression' => '_manager()->hasRole(Role::TEMPORARY)',
            ),
            array('deny',
                'actions' => array('points', 'players'),
                'expression' => 'isset($_GET["id"]) ? Role::is(Manager::model()->findByPk($_GET["id"]),Role::RESET) : false',
            ),
            array('allow',
                'expression' => '_manager()->hasRole(Role::MANAGER | Role::RESET)',
            ),
            array('deny',
                'users' => array('*')
            )
        );
    }

    /**
     *
     */
    public function actionIndex($id=0) {

        if ($id > 0) {
            $manager = Manager::model()->findByPk($id);
        }
        if (!$id || !$manager) {
            // Refresh manager to make sure any changes occuring from admin (foto approval)
            // are seen from the user asap.
            _manager()->profile->refresh();

            $manager = _manager();
        }
        if (!$manager->hasRole(Role::MANAGER | Role::RESET)) {
            throw new CHttpException('404');
        }


        // shorthands
        $transactions = Transaction::model()->getManagerTransactions($manager->id);
        $trCount = count($transactions);

        $showPhotoAnyway = _manager()->id == $manager->id ? true : false;

        _setPageTitle(_t("{name}'s profile", array('{name}' => $manager->username)));

        $canEditProfile = _manager()->id == $manager->id && !_manager()->isBanned();
        if ($canEditProfile) {
            Utils::registerJsFile('lib/jquery.jeditable.mini');
            Utils::registerJsFile('lib/jquery.tools.tooltip.min');
        }

        $this->render('profile', array(
            'manager' => $manager,
            'profilePhoto' => $manager->profilePhoto(false, $showPhotoAnyway),
            'portfolioChange' => ManagerDailyHistory::model()->getLatestEvents($manager->id, 1),
            'transactions' => $transactions,
            'trCount' => $trCount,
            'trToShow' => $trCount < 10 ? $trCount : 10,
            'canEditProfile' => $canEditProfile,
        ));
    }

    /**
     *
     */
    public function actionRemovePhoto() {
        if (!Utils::isAjax())
            return;

        // remove old photo if exists
        if (_manager()->profile->photo) {
            if (@unlink(Utils::managerPhotosUploadDirectory(_manager()->profile->photo))) {
                _manager()->profile->photo = null;
                if (_manager()->profile->save(true, array('photo'))) {
                    Report::model()->deleteReport(ReportReason::PHOTO, _manager()->id);
                    Utils::jsonReturn(array(
                        'success' => true,
                        'imageUrl' => Utils::imageUrl('profile/empty_profile.png'),
                    ));
                } else {
                    $details = Debug::arObjectErrors(_manager()->profile);
                }
            } else {
                $details = 'Could not find photo.';
            }

            Utils::jsonReturn(array(
                'error' => _t('Error removing profile photo.'),
                'details' => Debug::isDebug() ? $details : '',
            ));
        }
    }

    /**
     *
     */
    public function actionUploadPhoto() {
        if (!Utils::isAjax())
            return;

        Yii::import("ext.EAjaxUpload.qqFileUploader");

        $folder = Utils::managerPhotosUploadDirectory(); // folder for uploaded files
        $thumbnailFolder = Utils::managerThumbnailsUploadDirectory();

        $allowedExtensions = array('jpg', 'jpeg', 'gif', 'png');
        $sizeLimit = 102400; // maximum file size in bytes
        $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

        // remove old photo if exists
        if (_manager()->profile->photo) {
            @unlink($folder . _manager()->profile->photo);
            @unlink($thumbnailFolder . _manager()->profile->photo);
            _manager()->profile->photo = null;
        }

        // upload new photo
        $result = $uploader->handleUpload($folder);
        if (isset($result['success'])) {
            // update manager's profile
            _manager()->profile->photo = _manager()->username . '.' . $uploader->getExtensionName();
            _manager()->profile->photo_approved = false;

            // resize new photo
            $defaultDims = _param('profilePhotoDefaultDimensions');
            $newPhoto = $folder . $uploader->getFileName();
            Utils::ImageResize($newPhoto, $defaultDims['width'], $defaultDims['height'], false, $newPhoto, false);

            // rename photo
            $renamedPhoto = $folder . _manager()->profile->photo;
            @rename($newPhoto, $renamedPhoto);

            // Resize for thumbnail
            $defaultDims = _param('profileThumbnailDefaultDimensions');
            $thumbnail = $thumbnailFolder . $uploader->getFileName();
            Utils::ImageResize($renamedPhoto, $defaultDims['width'], $defaultDims['height'], false, $thumbnail, false);

            // rename photo
            $renamedPhoto = $thumbnailFolder . _manager()->profile->photo;
            @rename($thumbnail, $renamedPhoto);

            $result['imageUrl'] = Utils::managerPhotoUrl(_manager()->profile->photo);
        }

        if (!_manager()->profile->save(true, array('photo', 'photo_approved'))) {
            Debug::logToWindow(_manager()->profile->gerErrors());

            $result['success'] = false;
            $result['error'] = _t('An error occurred while updating your profile photo. Please try again.');
        } else {
            Report::model()->deleteReport(ReportReason::PHOTO, _manager()->id);
        }

        $result = htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        echo $result; // it's array
    }

    /**
     *
     */
    public function actionPlayers($id) {
        $manager = Manager::model()->with('managerTeam', 'managerTeam.managerTeamPlayers')->findByPk($id);

        if (!$manager || !$manager->hasRole(Role::MANAGER))
            _home();

        $players = array();
        foreach ($manager->managerTeam->managerTeamPlayers as $mtp) {
            $players[$mtp->position->abbreviation][] = new PitchPlayer(array(
                    'shares' => $mtp->shares,
                    'isLeader' => $mtp->isLeader(),
                    'clickable' => false,
                    ),
                    $mtp->player
            );
        } // foreach

        _setPageTitle(_t("{name}'s team", array('{name}' => $manager->username)));
        $this->render('players', array(
            'manager' => $manager,
            'players' => $players,
        ));
    }

    /**
     *
     */
    public function actionPoints($id) {

        if ($id == _manager()->id)
            _redirect('managers/points');


        $manager = Manager::model()->findByPk($id);
        if (!$manager)
            _home();

        _setPageTitle(_t("{name}'s points", array('{name}' => $manager->username)));
        $this->render('points', array(
            'manager' => $manager,
        ));
    }

    /**
     *
     */
    public function actionDeleted() {
        Utils::notice('deleted', _t('This user has deleted his account'), 'info');
        $this->render('deleted');
    }

    /**
     *
     */
    public function actionUpdateProfile() {
        if (!Utils::isAjax())
            return;

        if (!isset($_POST['id']))
            return;

        $attribute = $_POST['id'];
        $oldValue = _manager()->profile->$attribute;

        if (_manager()->profile->$attribute != $_POST['value']) {

            if (Utils::isPhraseAllowed($_POST['value'])) {

                // Update only if value is different
                _manager()->profile->$attribute = $_POST['value'];
                if (!_manager()->profile->save(true, array($attribute))) {
                    Debug::logToWindow(_manager()->profile->getErrors());
                }

                // After refresh, the old values will return if the udpate failed.
                _manager()->profile->refresh();
            }
        }

        switch ($attribute) {
            case 'country_id':
                echo _manager()->profile->country->country;
                break;

            case 'gender':
                echo _manager()->profile->genderText();
                break;

            case 'birthdate':
                $age = _manager()->getAge();
                $dateFormatted = _app()->dateFormatter->format('dd/MM/y', _manager()->profile->birthdate);
                echo "$age ($dateFormatted)";
                break;

            case 'line' :
                Report::model()->deleteReport(ReportReason::STATUS, _manager()->id);

            default:
                echo _manager()->profile->$attribute;
                break;
        }

        _app()->end();
    }

    /**
     *
     */
    public function actionCountryList() {
        if (!Utils::isAjax())
            return;

        if (!isset($_GET['id']))
            return;

        $countries = Country::model()->findAll();

        $return = array('selected' => _manager()->profile->country_id);
        foreach ($countries as $country) {
            $return[$country->id] = $country->country;
        }

        Utils::jsonReturn($return);
    }

}
