<?php

class ReportController extends Controller {

    public function filters() {
        return array('accessControl');
    }

    public function accessRules() {
        return array(
            array('deny',
                'users' => array('?')
            ),
            array(
                'allow',
                'expression' => '_manager()->hasRole(Role::MANAGER | Role::RESET)',
            ),
            array(
                'deny', // deny all users
                'users' => array('*'))
        );
    }

    public function actionIndex() {

        $id = $_GET['id'];
        $returnUrl = $_GET['returnUrl'];
        $reasons = $_GET['reasons'];
        $params = isset($_GET['params']) ? $_GET['params'] : array();
        $type = $_GET['type'];

        // Check if manager to report exists.
        $reported = null;
        if ($id) {
            $reported = Manager::model()->exists('id = :id', array(':id' => $id));
        }
        if (!$reported || _manager()->id == $id)
            _home();

        // Check if manager can be reported.
        if (!_manager()->canReport()) {
            Utils::notice('report', _t('You cannot make any more reports today.'), 'warning');
            _app()->request->redirect($returnUrl);
        } elseif (!_manager()->canReportUser($id)) {
            Utils::notice('report', _t('You have already reported this user.'), 'warning');
            _app()->request->redirect($returnUrl);
        }

        // Create report form
        $form = new Report;
        $form->manager_id = $id;
        $form->reporter_id = _manager()->id;
        $form->date_created = _app()->localtime->getLocalNow();

        if (isset($_POST['Report'])) {
            $form->attributes = $_POST['Report'];
            $action = ReportAction::model()->find('action = :type',array(':type' => $_POST['type']));
            $form->report_action_id = $action ? $action->id : null;
            if ($form->validate()) {

                $db = _app()->db->beginTransaction();
                try {

                    if (!$form->save())
                        throw new CValidateException($this);

                    // Trigger related events
                    Utils::triggerEvent('onAfterReportManager', $this, array_merge($params, array('report_id' => $form->id)));

                    // Commit database changes
                    $db->commit();

                    // Push success message
                    Utils::notice('report', _t('Your report has been successfully submitted.'), 'success');

                    // Return to previous location
                    _app()->request->redirect($returnUrl);
                } catch (CDbException $e) {
                    Utils::notice('report', _t("Failed to report manager {manager}", array('{manager}' => $form->manager->username)), 'error');
                    Debug::logToWindow($e->getMessage(), 'report');
                    $db->rollback();
                } catch (CValidateException $e) {
                    $db->rollback();
                }
                Utils::notice('report', _t("Failed to report manager {manager}", array('{manager}' => $form->manager->username)), 'error');
            }
        }

        $title = _t('Report {user}', array('{user}' => $form->manager->username));
        _setPageTitle($title);
        $this->render('report', array(
            'reasons' => ReportReason::getReasons($type),
            'model' => $form,
            'title' => $title,
            'returnUrl' => $returnUrl,
            'type' => $type
        ));
    }

}

?>
