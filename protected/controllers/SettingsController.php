<?php

class SettingsController extends Controller {

    public function filters() {
        return array('accessControl');
    }

    public function accessRules() {
        return array(
            array('deny',
                'users' => array('?')
            ),
            array('deny',
                'actions' => array('deleteAccount'),
                'expression' => '!_isModuleOn("deleteAccount")'),
            array('deny',
                'actions' => array('blockedUsers'),
                'expression' => '_manager()->hasRole(Role::TEMPORARY)',
            ),
            array('allow',
                'expression' => '!_manager()->hasRole(Role::INACTIVE)',
            ),
            array('deny',
                'users' => array('*')
            )
        );
    }

    public function subMenuItems() {
        return array(
            array('index', _t('Preferences')),
            array('email', _t('Change email')),
            array('password', _t('Change password')),
            array('blockedUsers', _t('Blocked users'), 'expression' => '_manager()->hasRole(Role::MANAGER | Role::RESET)'),
            array('deleteAccount', _t('Delete account'), 'expression' => '_isModuleOn("deleteAccount")'),
        );
    }

    public function init() {
        parent::init();
    }

    /**
     *
     */
    public function actionIndex() {
        if (isset($_POST['Manager'])) {
            _manager()->attributes = $_POST['Manager'];
            if (_manager()->save(true, array(
                    'timezone_id',
                    'newsletter'
                ))) {
                _app()->localtime->TimeZone = _manager()->timezone->name;
                _manager()->refresh();
                Utils::notice('result', _t('Your preferences have been updated.'), 'success');
            } else {
                Utils::notice('result', _t('Your preferences failed to update.'), 'error');
            }
        }

        _setPageTitle(_t('Settings - Preferences'));
        $this->render('index', array(
            'model' => _manager(),
            'timezones' => Timezone::model()->findAll()
        ));
    }

    /**
     *
     */
    public function actionEmail() {
        $form = new UserSettingsFormEmail;
        if (isset($_POST['UserSettingsFormEmail'])) {
            $form->update();
        }

        _setPageTitle(_t('Settings - Change email'));
        $this->render('email', array('model' => $form));
    }

    /**
     *
     * @param type $uid
     * @param type $newEmail
     * @param type $code
     */
    public function actionActivateEmail($uid, $newEmail, $code) {
        $form = new UserSettingsFormEmail;
        $form->activate($uid, $newEmail, $code);
        $this->redirect('email');
    }

    /**
     *
     */
    public function actionPassword() {
        $form = new UserSettingsFormPass;
        if (isset($_POST['UserSettingsFormPass'])) {
            $form->update();
        }
        _cs()->registerScriptFile(_bu('js/lib/jquery.tools.tooltip.min.js'));
        _cs()->registerScriptFile(_bu('js/lib/passwordStrengthIndicator.js'));

        _setPageTitle(_t('Settings - Change password'));
        $this->render('password', array('model' => $form));
    }

    /**
     *
     */
    public function actionBlockedUsers($id=0) {

        if ($id) {
            if (_manager()->unblockManager($id)) {
                Utils::notice('result', _t('User unblocked.'), 'success');
            } else {
                Utils::notice('result', _t('Unblocking of user failed! Please try again.'), 'error');
            }
        }

        _setPageTitle(_t('Settings - Blocked users'));
        $this->render('blockedUsers', array(
            'blocked' => BlockedManager::model()->findAllByAttributes(array('manager_id' => _manager()->id))
        ));
    }

    /**
     *
     */
    public function actionDeleteAccount() {
        $form = new UserSettingsFormDelete;
        if (isset($_POST['UserSettingsFormDelete'])) {
            if ($form->update()) {
                _user()->logout();
                $this->redirect(_url('user/goodbye'));
            }
        }

        $reasons = ReportReason::getReasons('delete');

        _setPageTitle(_t('Settings - Delete account'));
        $this->render('deleteAccount', array(
            'model' => $form,
            'reasons' => $reasons,
        ));
    }

}
