<?php

class SiteController extends Controller {

    public function filters() {
        return array('accessControl');
    }

    public function accessRules() {
        return array(
            array(
                'deny',
                'actions' => array('forum'),
                'expression' => '!_isModuleOn("forum")'
            ),
            array(
                'deny',
                'actions' => array('indexNews', 'indexTeamNews'),
                'users' => array('?')
            ),
            array(
                'allow',
                'users' => array('*')
            ),
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        _setPageTitle();

        if (_user()->isGuest || _manager()->hasRole(Role::TEMPORARY)) {

            $link = StaticContent::model()->getText('VideoUrl');
            // register slideshow
            _cs()->registerCssFile(_bu('css/slideshow.css'), 'screen, projection');
            _cs()->registerScriptFile(_bu('js/lib/loopedslider.js'));
            _cs()->registerScriptFile(_bu('js/lib/jwplayer.js'));
            _cs()->registerScriptFile(_bu('js/index.js'));

            $this->render('indexGuest', array(
                'link' => $link == null ? false : $link
            ));
            return;
        }

        _cs()->registerScriptFile(_bu('js/news.js'));

        $date = _app()->localtime->getGameNow('Y-m-d');
        $longdate = _app()->localtime->getGameNow('Y-m-d H:i:s');
        $nextGame = Game::model()->find(array(
            'condition' => 'date(convert_tz(date_played,"UTC",:tz)) > date(:date)',
            'params' => array(
                ':date' => $longdate,
                ':tz' => _timezone()
            ),
            'select' => 'min(date_played) as dateplayed'
            ));

        $previousGame = Game::model()->find(array(
            'condition' => 'date(convert_tz(date_played,"UTC",:tz)) < date(:date)',
            'params' => array(
                ':date' => $longdate,
                ':tz' => _timezone()
            ),
            'select' => 'max(date_played) as dateplayed'
            ));
        if (Game::model()->count('date(convert_tz(date_played,"UTC",:tz)) = date(:date)', array(':date' => $date, ':tz' => _timezone())) == 0) {
            if ($nextGame->dateplayed != null) {
                $date = $nextGame->dateplayed;
            } elseif ($previousGame->dateplayed != null) {
                $date = $previousGame->dateplayed;
            }
        }
        $games = new Game('search');
        $games->date_played = $date;

        $teams = new Team('search');
        $announcements = Announcement::model()->getLatest();
        $isPitchOpen = Gameconfiguration::model()->isOpen();

        $howToPlay = StaticContent::model()->getText('HowToPlay');

        $this->render('index', array(
            'games' => $games,
            'teams' => $teams,
            'isPitchOpen' => $isPitchOpen,
            'announcements' => $announcements,
            'howToPlay' => $howToPlay
        ));
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionAnnouncements() {
//            $this->layout = 'submenu';
        $a = Announcement::model()->findAll(array('condition' => 'date_entered > DATE_SUB(CURRENT_DATE, INTERVAL 30 DAY)', 'order' => 'date_entered desc'));

        if (count($a) < 30) {
            $a = Announcement::model()->findAll(array('limit' => '30', 'order' => 'date_entered desc'));
        }

        _setPageTitle(_t('Game announcements'));
        $this->render('announcements', array('a' => $a));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        _setPageTitle(_t('Error'));
        if (Utils::isAjax()) {
            echo $error['message'];
            _app()->end();
        }
        $this->render('error');
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {

        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                if ($model->send()) {
                    Utils::notice('contact', _t('Thank you for contacting us. We will respond to you as soon as possible.'), 'success');
                } else {
                    Utils::notice('contact', _t('Failed to send email. Try again.') . '<br /><br />' . $model->body, 'error');
                }
            }
        }

        _setPageTitle(_t('Contact'));

        if (!_user()->isGuest)
            $model->email = _manager()->email;

        _cs()->registerScriptFile(_bu('js/lib/jquery.tools.tooltip.min.js'));

        $this->render('contact', array(
            'model' => $model,
        ));
    }

    /**
     * Displays the contact page
     */
    public function actionPartners() {
        $model = new PartnersForm;
        if (isset($_POST['PartnersForm'])) {
            $model->attributes = $_POST['PartnersForm'];
            if ($model->validate()) {
                if ($model->send()) {
                    Utils::notice('contact', _t('Thank you for contacting us. We will respond to you as soon as possible.'), 'success');
                } else {
                    Utils::notice('contact', _t('Failed to send email. Try again.'), 'error');
                }
            }
        }
        _setPageTitle(_t('Partners'));
        $this->render('partners', array('model' => $model));
    }

    public function actionNews() {
        $news = $this->getNews(15);

        _setPageTitle(_t('Latest news'));
        $this->render('news', array(
            'news' => $news,
        ));
    }

    public function actionAwards() {
        _setPageTitle(_t('Awards'));
        $staticContent = StaticContent::model()->getText('Awards');
        $staticContent = Utils::replaceVars($staticContent);

        $this->render('staticPage', array('staticContent' => $staticContent));
    }

    public function actionTermsOfUse() {
        $staticContent = StaticContent::model()->getText('TermsOfUse');
        $staticContent = Utils::replaceVars($staticContent);
        
        $renderDialog = false;
        if (Utils::isLoggedIn() && !_manager()->accepted_terms)
            $renderDialog = true;

        $this->render('staticPage', array('staticContent' => $staticContent, 'renderDialog' => $renderDialog));
    }

    public function actionPrivacyPolicy() {
        $staticContent = StaticContent::model()->getText('PrivacyPolicy');
        $staticContent = Utils::replaceVars($staticContent);

        $this->render('staticPage', array('staticContent' => $staticContent));
    }

    public function actionIndexNews() {
        if (Debug::isDebug())
            return;

        if (Utils::isAjax())
            $this->renderPartial('_news', array('news' => $this->getNews(4)));
    }

    public function actionIndexTeamNews() {
        if (Debug::isDebug())
            return;

        if (Utils::isAjax())
            $this->renderPartial('_news', array('news' => $this->getNews(3, _manager()->profile->team->xml_id)));
    }

    public function guestText() {
        return StaticContent::model()->getText('Guest');
    }

    public function videoUrl() {
        $link = StaticContent::model()->getText('VideoUrl');
        return $this->renderPartial('_frontpageVideoWidget', array('link' => $link), true);
    }

    /**
     *
     * @param <type> $no
     * @return <type>
     */
    public function getNews($no=16, $team = false) {
        return OnSports::getNews($no, $team);
    }

}