<?php

class StandingsController extends Controller {

    public function filters() {
        return array('accessControl',);
    }

    public function accessRules() {
        return array(
            array(
                'allow',
                'users' => array('?')
            ),
            array(
                'allow',
                'users' => array('@')
            ),
            array(
                'deny',
                'users' => array('*')
            )
        );
    }

    public function actionIndex() {
        $name = _t('Search...');

        $model = new Manager('search');
        $model->role = array(Role::MANAGER,Role::RESET);
        
        if (isset($_GET['Manager'])) {
            $model->attributes = $_GET['Manager'];
            $name = $_GET['Manager']['username'];
        }

        _setPageTitle(_t('Season standings'));
        $this->render('index', array(
            'model' => $model,
            'name' => $name
        ));
    }

}