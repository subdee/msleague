<?php

class StockmarketController extends Controller {

    public $model;

    public function filters() {
        return array('accessControl',);
    }

    public function accessRules() {
        return array(
            array('deny',
                'users' => array('?')
            ),
            array(
                'allow',
                'actions' => array('index'),
                'expression' => '_manager()->hasRole(Role::MANAGER | Role::RESET)',
            ),
            array(
                'allow',
                'expression' => '_manager()->hasRole(Role::MANAGER)',
            ),
            array(
                'deny',
                'users' => array('*')
            ),
        );
    }

    public function init() {
        parent::init();
        // register datatable
        Utils::registerJsFile('lib/jquery.dataTables.min');

        // register js/css files
        _cs()->registerCssFile(_cs()->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css');
        Utils::registerJsFile('lib/jquery.tools.tooltip.min');
        Utils::registerJsFile('lib/jquery.hoverIntent');
        Utils::registerJsFile('stockmarket');
    }

    public function subMenuItems() {
        return array(
            array('buy', _t('Buy shares'), 'expression' => 'Gameconfiguration::model()->isOpen() && _manager()->hasRole(Role::MANAGER)', 'onfail' => 'disable'),
            array('sell', _t('Sell shares'), 'expression' => 'Gameconfiguration::model()->isOpen() && _manager()->hasRole(Role::MANAGER)', 'onfail' => 'disable'),
            array('replace', _t('Replace'), 'expression' => '_controller()->route=="stockmarket/replace"'),
        );
    }

    public function actionIndex() {
        // save stockmarket state
        $isMarketOpen = Gameconfiguration::model()->isOpen();
        $closeTime = Gameconfiguration::model()->getCloseTime();
        $manualClosed = Gameconfiguration::model()->get('is_enabled');

        $params = array(
            'isMarketOpen' => $isMarketOpen,
            'isUserBanned' => _manager()->isBanned(),
            'closeTime' => $closeTime,
            'manualClosed' => $manualClosed
        );

        //register chat files
        if (_isModuleOn('chat')) {
            _cs()->registerScriptFile(_bu('js/chat.js'));
//            _cs()->registerCSSFile(_tbu('css/chat.css'));

            $params['shouts'] = Chat::model()->getShouts(100);
        }

        // set page title and render
        _setPageTitle(_t('Stockmarket'));
        $this->render('index', $params);
    }

    public function actionBuy() {
        if (!Gameconfiguration::model()->isOpen())
            $this->redirect('index');

        // Return all players
        $players = Player::model()->findPlayers(array('order' => 't.current_value desc'));
        // Get a display list to use
        $displayList = PlayerListDisplayBuy::create(_manager(), $players);

        _setPageTitle(_t('Stockmarket - Buy shares'));
        $this->render('playerList', array(
            'teams' => Team::model()->findAll(),
            'displayList' => $displayList,
            'extra' => array(
                'positions' => BasicPosition::model()->findAll(),
            )
        ));
    }

    public function actionSell() {
        if (!Gameconfiguration::model()->isOpen())
            $this->redirect('index');

        $displayList = PlayerListDisplaySell::create(_manager(), _team()->managerTeamPlayers);

        _setPageTitle(_t('Stockmarket - Sell shares'));
        $this->render('playerList', array(
            'teams' => Team::model()->findAll(),
            'displayList' => $displayList,
            'extra' => array(
                'positions' => BasicPosition::model()->findAll(),
            )
        ));
    }

    public function actionReplace($id) {
        if (!Gameconfiguration::model()->isOpen())
            $this->redirect('index');

        $playerToReplace = _team()->getPlayerById($id);
        if (!$playerToReplace)
            $this->redirect('index');

        $_SESSION['playerToReplace'] = $id;

        // Return all players
        $players = Player::model()->findPlayers(array(
            'order' => 'team.shortname5',
            'byPosition' => $playerToReplace->player->basicPosition->name,
            'excludeOwned' => true,
            ));

        // Get a display list to use
        $displayList = PlayerListDisplayReplace::create(_manager(), $players, array('playerToSell' => $playerToReplace, 'sharesToBuy' => 1));

        $message = _t('To complete the sale, select {position} to replace {name} (new cash balance {cash}).', array(
            '{position}' => $playerToReplace->player->basicPosition->getTranslatedName(array('accusative' => true)),
            '{name}' => CHtml::tag('strong', array(), $playerToReplace->player->shortname),
            '{cash}' => CHtml::tag('strong', array(), _currency(_manager()->portfolio_cash + $playerToReplace->shares * $playerToReplace->player->current_value)),
            ));

        _setPageTitle(_t('Stockmarket - Replace player'));
        $this->render('playerList', array(
            'teams' => Team::model()->findAll(),
            'displayList' => $displayList,
            'extra' => array(
                'playerToReplace' => $playerToReplace,
                'notice' => array(
                    'type' => 'replace',
                    'message' => $message
                ),
            ),
        ));
    }

    /**
     * Ajax action that shows the popup for buying stocks from a particular player.
     */
    public function actionStocksPopup() {
        if (!_app()->request->isAjaxRequest)
            $this->redirect('index');

        // Check if the stockmarket is open and the user is banned.
        if (!Gameconfiguration::model()->isOpen() || _manager()->isBanned()) {
            Utils::notice('notice', _t('A change in the system has prevented the action from being completed.'), 'warning');
            Utils::jsonReturn(array('redirect' => _url('stockmarket/index')));
        }

        // Get current manager
        $params = null;

        if ($_POST['action'] == 'buy') {

            $player = null;

            // Get current player
            $mtp = _team()->getPlayerById($_POST['player_id']);

            $currentStocks = 0;
            if ($mtp) {
                $player = $mtp->player;
                $currentStocks = $mtp->shares;
            } else {
                $player = Player::model()->findByPk($_POST['player_id']);
            }
            if (!$player) {
                error_log("Invalid player id passed in stocksPopup(buy): {$_POST['player_id']}");
                _app()->end();
            }

            $maxStocks = _manager()->getAvailableStocksToBuy($player);
            $params = array(
                'player' => $player,
                'maxStocks' => $maxStocks,
                'curStocks' => $currentStocks,
                'maxReason' => PMSBuy::get()->message(),
            );
            $jsParams = array(
                'shareValue' => $player->current_value,
                'maxSharesToTransact' => $maxStocks,
                'shareCount' => $currentStocks,
            );
            Utils::triggerEvent('onStockmarketDialogBuy', $this);
        } elseif ($_POST['action'] == 'sell') {

            // Get reference of player
            $player = _team()->getPlayerById($_POST['player_id']);
            if (!$player) {
                error_log("Invalid player id passed in stocksPopup(sell): {$_POST['player_id']}");
                _app()->end();
            }

            $posLimits = BasicPosition::model()->getPosLimits();
            $position = $player->position->basicPosition->abbreviation;
            $maxStocks = _manager()->getAvailableStocksToSell($player);
            $curStocks = $player->shares;
            $canReplace = $maxStocks < $curStocks && _manager()->getAvailableTransactions() > 1;

            $params = array(
                'player' => $player->player,
                'maxStocks' => $maxStocks,
                'curStocks' => $player->shares,
                'isDelisted' => $player->player->isDelisted(),
                'posLimits' => array($posLimits[$position], $player->position->basicPosition->name),
                'maxReason' => PMSSell::get()->message(),
                'canReplace' => $canReplace,
            );
            $jsParams = array(
                'shareValue' => $player->player->current_value,
                'maxSharesToTransact' => $maxStocks,
                'shareCount' => $curStocks,
                'canBeReplaced' => $canReplace
            );
            Utils::triggerEvent('onStockmarketDialogSell', $this);
        } elseif ($_POST['action'] == 'replace') {

            // Get current player
            $playerToSell = _team()->getPlayerById($_SESSION['playerToReplace']);
            if (!$playerToSell) {
                error_log("Invalid player to sell id passed in stocksPopup(replace): {$_POST['player_id']}");
                _app()->end();
            }

            $playerToBuy = _team()->getPlayerById($_POST['player_id']);

            if ($playerToBuy)
                $currentStocks = $playerToBuy->shares;
            else
                $currentStocks = 0;

            $player = Player::model()->findByPk($_POST['player_id']);
            if (!$player) {
                error_log("Invalid player id passed in stocksPopup(replace): {$_POST['player_id']}");
                _app()->end();
            }

            $maxStocks = _manager()->getAvailableStocksToBuy($player, $playerToSell);
            $params = array(
                'player' => $player,
                'maxStocks' => $maxStocks,
                'curStocks' => $currentStocks,
                'maxReason' => PMSBuy::get()->message(),
            );
            $jsParams = array(
                'shareValue' => $player->current_value,
                'maxSharesToTransact' => $maxStocks,
                'shareCount' => $currentStocks,
            );
            Utils::triggerEvent('onStockmarketDialogReplace', $this);
        } else {
            error_log("Invalid action passed in stocks popup: {$_POST['action']}");
            _app()->end();
        }

        $params['action'] = $_POST['action'];
        $jsParams['action'] = $_POST['action'];
        $params['buy'] = $_POST['action'] == 'buy';
        $params['sell'] = $_POST['action'] == 'sell';
        $params['replace'] = $_POST['action'] == 'replace';

        Utils::jsonReturn(array(
            'html' => $this->renderPartial('_popupShares', $params, true),
            'params' => $jsParams
        ));
    }

    /**
     * Performs a share purchase.
     */
    public function actionTransact() {
        if (!_app()->request->isAjaxRequest)
            $this->redirect('index');

        // Check if the stockmarket is open and the user is banned.
        if (!Gameconfiguration::model()->isOpen() || _manager()->isBanned()) {
            Utils::notice('notice', _t('A change in the system has prevented the transaction from being completed.'), 'warning');
            Utils::jsonReturn(array('redirect' => _url('stockmarket/index')));
        }

        _manager()->refresh();

        // How many transactions to show at the end of the purchase.
        $nTrasactionsToLoad = 1;
        // How many transactions the manager has to spend.
        $availableTransactions = Manager::model()->getAvailableTransactions();

        if ($_POST['action'] == 'buy') {
            // Get current player
            $player = Player::model()->findByPk($_POST['player_id']);
            if (!$player) {
                error_log("Invalid player id passed in transact/buy: {$_POST['player_id']}");
                Utils::jsonReturn(array('success' => false, 'html' => $this->widget('Notice', array('message' => _t('This player does not exist'), 'type' => 'error'), true)));
            }

            // Check again against player status....
            if (_manager()->canBuyPlayerStocks($player, $availableTransactions > 0, $_POST['shares']) != PMSBuy::get()->ok()) {
                Utils::jsonReturn(array('success' => false, 'html' => $this->widget('Notice', array('message' => PMSBuy::get()->message(), 'type' => 'error'), true)));
            }

            // Go ahead with the transaction.
            if (!_manager()->buy($player, (int) $_POST['shares'])) {
                Utils::jsonReturn(array('redirect' => _url('stockmarket/index')));
            }
        } elseif ($_POST['action'] == 'sell') {
            $mtp = _team()->getPlayerById($_POST['player_id']);
            if (!$mtp) {
                error_log("Invalid player id passed in transact/sell: {$_POST['player_id']}");
                Utils::jsonReturn(array('success' => false, 'html' => $this->widget('Notice', array('message' => _t('You do not own any shares of this player'), 'type' => 'error'), true)));
            }
            $player = $mtp->player;

            // Check again against player status....
            if (_manager()->canSellPlayerStocks($mtp, $availableTransactions, $_POST['shares']) != PMSSell::get()->ok()) {
                Utils::jsonReturn(array('success' => false, 'html' => $this->widget('Notice', array('message' => PMSSell::get()->message(), 'type' => 'error'), true)));
            }

            // Go ahead with the transaction.
            if (!_manager()->sell($mtp, (int) $_POST['shares'])) {
                Utils::jsonReturn(array('redirect' => _url('stockmarket/index')));
            }
        } elseif ($_POST['action'] == 'replace') {
            // Get own player
            $playerToSell = _team()->getPlayerById($_POST['player_sell_id']);
            if (!$playerToSell) {
                error_log("Invalid player id passed in transact/replace/sell: {$_POST['player_sell_id']}");
                Utils::jsonReturn(array('success' => false, 'html' => $this->widget('Notice', array('message' => _t('You do not own any shares of this player'), 'type' => 'error'), true)));
            }

            // Get other player
            $playerToBuy = Player::model()->findByPk($_POST['player_buy_id']);
            if (!$playerToBuy) {
                error_log("Invalid player id passed in transact/replace/buy: {$_POST['player_buy_id']}");
                Utils::jsonReturn(array('success' => false, 'html' => $this->widget('Notice', array('message' => _t('This player does not exist'), 'type' => 'error'), true)));
            }

            if (_manager()->canReplacePlayer($playerToSell, $playerToBuy, $availableTransactions > 1, $_POST['shares']) != PMSBuy::get()->ok()) {
                Utils::jsonReturn(array('success' => false, 'html' => $this->widget('Notice', array('message' => PMSBuy::get()->message(), 'type' => 'error'), true)));
            }

            if (!_manager()->replace($playerToSell, $playerToBuy, (int) $_POST['shares'])) {
                Utils::jsonReturn(array('redirect' => _url('stockmarket/index')));
            }

            $nTrasactionsToLoad = 2;

            unset($_SESSION['playerToReplace']);
        } else {
            error_log("Invalid action passed in transact: {$_POST['action']}");
            Utils::jsonReturn(array('redirect' => _url('stockmarket/index')));
        }

        // Load last transactions
        $transactions = Transaction::model()->getManagerTransactions(_manager()->id, $nTrasactionsToLoad);

        // Create message
        $widget = '';
        foreach ($transactions as $t) {
            $message = _t('You have successfully {type} {shares} shares of {player}.', array(
                '{type}' => $t->type_of_transaction == 'sell' ? _t('sold') : _t('bought'),
                '{shares}' => CHtml::tag('strong', array(), $t->shares),
                '{player}' => CHtml::tag('strong', array(), $t->player->shortname),
                '{price}' => CHtml::tag('strong', array(), _currency($t->shares * $t->price_share)),
                ));
            $widget .= $this->widget('Notice', array('message' => $message, 'type' => 'success'), true);
        }

        $message = _t('{portfolio_cash} cash and {transactions} transactions remaining.', array(
            '{portfolio_cash}' => CHtml::tag('strong', array(), _currency(_manager()->portfolio_cash)),
            '{transactions}' => CHtml::tag('strong', array(), _manager()->getAvailableTransactions()),
            ));
        $widget .= $this->widget('Notice', array('message' => $message, 'type' => 'info'), true);

        Utils::jsonReturn(array(
            'success' => true,
            'html' => $widget
        ));
    }

}