<?php

class UserController extends Controller {

    public function filters() {
        return array('accessControl');
    }

    public function accessRules() {
        return array(
            array(
                'deny',
                'actions' => array('logout'),
                'users' => array('?')
            ),
            array(
                'deny',
                'actions' => array('login'),
                'users' => array('@')
            ),
            array(
                'allow',
                'users' => array('*')
            ),
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
//            'captcha' => array(
//                'class' => 'CCaptchaAction',
//                'backColor' => 0xFFFFFF,
//            ),
            'captcha' => array(
                'class' => 'CaptchaExtendedAction',
            ),
        );
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $model = new LoginForm;

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                if (_manager()->hasRole(Role::TEMPORARY)) {
                    if (Gameconfiguration::model()->get('is_season_open'))
                        _redirect('createTeam/index');
                    else
                        _redirect('site/index');
                }else
                    _return();
            }
            else {
                // loggin needed?
                Utils::notice('login', _t('Failed to login. Make sure your username and password are correct.'), 'error');
                $model->password = false;
            }
        }

        _setPageTitle(_t('Login'));
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        _user()->logout();
        _home();
    }

    /**
     * Registers a new manager and redirects to the homepage OR THE
     * check your email for activation link type of thingy.
     */
    public function actionRegister() {
        $tooMany = false;
        $maxUsers = Gameconfiguration::model()->find()->max_number_of_registered_users;
        $users = Manager::model()->count();
        if ($users >= $maxUsers || !_isEnabled('is_registration_open'))
            $tooMany = true;
        $form = new UserRegistrationForm;
        $form->newsletter = true;

        $invitation_id = null;
        $inviter = null;

        if (isset($_GET['invitation']) && isset($_GET['code'])) {
            // get invitation by id
            $invitation = Invitation::model()->findByPk($_GET['invitation']);
            if ($invitation) {
                // match registration code
                $manager = Manager::model()->findByPk($invitation->manager_id);
                $registration_code = $manager->createActivationCode(array($invitation->id));
                $inviter = $manager->username;
                unset($manager);
                if ($registration_code == $_GET['code']) {
                    // coming from invitation
                    $invitation_id = $invitation->id;
                }
            }
        } else {
            if (isset($_POST['inviter']))
                $inviter = $_POST['inviter'];
            if (isset($_POST['invitation_id']))
                $invitation_id = $_POST['invitation_id'];
        }

        // collect user input data
        if (isset($_POST['UserRegistrationForm'])) {
            $form->attributes = $_POST['UserRegistrationForm'];
            $form->manager->attributes = $_POST['Manager'];
            $form->manager->profile->attributes = $_POST['Profile'];

            $validated = $form->manager->validate();
            $validated = $form->validate() && $validated;

            if ($validated) {
                if ($form->register($invitation_id)) {
                    // Registration is successful.
                    $this->actionActivationInstructions();
                    return;
                }
            }
            // reset fields
            $form->verifyCode = null;
            $form->manager->password = null;
            $form->manager->password_repeat = null;
        } else {
            // get default country and timezone to select
            $form->manager->profile->country_id = Country::model()->findByAttributes(array('country' => _app()->params['country']))->id;
            $form->manager->timezone_id = Timezone::model()->findByAttributes(array('name' => _timezone()))->id;
        }

        _cs()->registerScriptFile(_bu('js/lib/jquery.tools.tooltip.min.js'));
        _cs()->registerScriptFile(_bu('js/lib/passwordStrengthIndicator.js'));

        _setPageTitle(_t('Registration'));
        $this->render('register', array(
            'model' => $form,
            'inviter' => $inviter,
            'invitation_id' => $invitation_id,
            'toomany' => $tooMany,
            'countries' => Country::model()->findAll(),
            'timezones' => Timezone::model()->findAll()
        ));
    }

    /**
     *
     * @param type $uid
     * @param type $code
     */
    public function actionVerifyEmail($uid, $code) {
        // get manager by id
        $manager = Manager::model()->findByPk($uid);

        // only handle inactive players
        if (!$manager || !$manager->hasRole(Role::INACTIVE))
            _home();

        // Check if email is verified. Verify it.
        if ($manager->verifyEmail($code)) {

            // If verified, verify everything else

            if (_isModuleOn('sms')) {
                _redirect('sms/verify', array('manager_id' => $manager->id));
            }

            $this->actionActivate($uid);
        }
    }

    /**
     * Inactive user is sent here to activate his account and become temporary manager.
     */
    public function actionActivate($uid) {
        // get manager by id
        $manager = Manager::model()->findByPk($uid);

        // only handle inactive players
        if (!$manager || !$manager->hasRole(Role::INACTIVE))
            _home();

        // email must be verified
        if ($manager->email_code != null)
            _home();

        if (_isModuleOn('sms')) {
            if (!Sms::isVerified(Mobile::model()->findByAttributes(array('manager_id' => $manager->id))))
                _home();
        }

        // if manager is fully verified, activate him/her
        if ($manager->activate()) {
            $this->render('activationCompleted', array(
                'manager' => $manager->username,
            ));
        }
    }

    /**
     *
     * @return <type>
     */
    public function actionGoodbye() {
        _cs()->registerMetaTag('5;url=' . _app()->homeUrl, null, 'refresh');
        $this->render('goodbye');
    }

    /**
     *
     */
    public function actionActivationInstructions() {
        // Render welcome screen with instructions.
        _setPageTitle();
        $this->render('activationInstructions');
    }

    /**
     *
     */
    public function actionActivationCompleted($manager) {
        // Render welcome screen with instructions.
        _setPageTitle();
        $this->render('activationCompleted', array('manager' => $manager));
    }

    /**
     *
     */
    public function actionCheckUsernameAvailability() {
        if (!Utils::isAjax())
            throw new CHttpException('404');

        if (isset($_GET['username'])) {
            $manager = new Manager;
            $manager->username = $_GET['username'];
            echo CActiveForm::validate($manager, array('username'));
        }

        _app()->end();
    }

}