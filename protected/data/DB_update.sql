-- Remove role table
ALTER TABLE manager DROP FOREIGN KEY fk_managers_role;
ALTER TABLE manager CHANGE role_id role INT DEFAULT '1' NOT NULL;
ALTER TABLE user DROP FOREIGN KEY fk_users_role;
ALTER TABLE user CHANGE role_id role INT DEFAULT '1' NOT NULL;
DROP TABLE role;
UPDATE manager SET role = 4 WHERE role = 2;
UPDATE manager SET role = 2 WHERE role = 1;
UPDATE manager SET role = 1 WHERE role = 9;
UPDATE user SET role = 1 WHERE role = 20;
UPDATE user SET role = 2 WHERE role = 30;
UPDATE user SET role = 4 WHERE role = 99;
-- Drop tournament_id from ranks
ALTER TABLE rank DROP COLUMN tournament_id;
-- Drop id column from ranks and set manager_id column as PK
ALTER TABLE rank DROP COLUMN id;
ALTER TABLE rank ADD PRIMARY KEY (manager_id);
-- Adding notifications module
CREATE TABLE mod_notifications
(
    id INT AUTO_INCREMENT,
    creation_date TIMESTAMP NOT NULL,
    type INT(1) NOT NULL,
    recipient_id INT NOT NULL,
    message TEXT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_notification_recipient_manager FOREIGN KEY (recipient_id) REFERENCES manager (id) ON
DELETE
    CASCADE ON
UPDATE
    CASCADE
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- Adding leagues tables
CREATE TABLE mod_custom_leagues
(
    id INT AUTO_INCREMENT,
    date_created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    creator_id INT,
    name VARCHAR(50) NOT NULL,
    capacity INT NOT NULL,
    type INT(1) NOT NULL,
    is_system BOOL DEFAULT '0' NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_leagues_creator_manager FOREIGN KEY (creator_id) REFERENCES manager(id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE mod_custom_leagues MODIFY column date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

CREATE TABLE mod_custom_leagues_managers
(
    id INT AUTO_INCREMENT,
    join_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    league_id INT,
    manager_id INT,
    PRIMARY KEY (id),
    CONSTRAINT fk_leagues_league_league FOREIGN KEY (league_id) REFERENCES mod_custom_leagues(id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_leagues_manager_manager FOREIGN KEY (manager_id) REFERENCES manager(id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE mod_custom_leagues_managers MODIFY column join_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

CREATE TABLE mod_custom_leagues_config
(
    maxLeaguesPerManager INT(2) DEFAULT '10' NOT NULL,
    maxManagersPerLeague INT DEFAULT '0' NOT NULL
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;
insert into mod_custom_leagues_config values (10, 0);

CREATE TABLE mod_custom_leagues_invitations
(
    league_id INT NOT NULL,
    notification_id INT NOT NULL,
    PRIMARY KEY (league_id, notification_id),
    CONSTRAINT fk_custom_league_notifications_league FOREIGN KEY (league_id) REFERENCES mod_custom_leagues (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_custom_league_notifications_notification FOREIGN KEY (notification_id) REFERENCES mod_notifications (id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- Adding fan league table
CREATE TABLE mod_custom_leagues_fan
(
    league_id INT NOT NULL,
    team_id INT NOT NULL,
    PRIMARY KEY (league_id, team_id),
    CONSTRAINT fk_custom_league_fan_league FOREIGN KEY (league_id) REFERENCES mod_custom_leagues (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_custom_league_fan_team FOREIGN KEY (team_id) REFERENCES team (id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- Removing is_system attribute from custom league.
ALTER TABLE mod_custom_leagues DROP COLUMN is_system;
-- Droping manager foreign key for custom league.
ALTER TABLE mod_custom_leagues DROP FOREIGN KEY fk_leagues_creator_manager;
-- Adding league discussion table
CREATE TABLE mod_custom_leagues_discussions
(
    league_id INT NOT NULL,
    discussion_id INT NOT NULL,
    PRIMARY KEY (league_id),
    CONSTRAINT fk_custom_league_discussion_league FOREIGN KEY (league_id) REFERENCES mod_custom_leagues (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_custom_league_discussion_discussion FOREIGN KEY (discussion_id) REFERENCES mod_forum_discussions (id) ON DELETE CASCADE ON UPDATE CASCADE 
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;
--formula coefficient configurable
ALTER TABLE script_configuration ADD (formula_coefficient FLOAT DEFAULT '1' NOT NULL);
-- match discussions -- 
CREATE TABLE mod_match_discussions
(
    game_id INT NOT NULL,
    discussion_id INT NOT NULL,
    PRIMARY KEY (game_id, discussion_id),
    CONSTRAINT fk_match_discussions_match FOREIGN KEY (game_id) REFERENCES game (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_match_discussions_discussion FOREIGN KEY (discussion_id) REFERENCES mod_forum_discussions (id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;
DROP TABLE `mod_forum_discussions_context`;

-- forum category discussion connector --
CREATE TABLE mod_forum_category_discussions
    (
        category_id INT NOT NULL,
        discussion_id INT NOT NULL,
        title VARCHAR(64) NOT NULL,
        PRIMARY KEY (discussion_id),
        CONSTRAINT fk_categories_discussions_category FOREIGN KEY (category_id) REFERENCES mod_forum_categories (id) ON DELETE CASCADE ON UPDATE CASCADE,
        CONSTRAINT fk_categories_discussions_discussion FOREIGN KEY (discussion_id) REFERENCES mod_forum_discussions (id) ON DELETE CASCADE ON UPDATE CASCADE
    )
    ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- move category discussion connection data to new table
insert into mod_forum_category_discussions (discussion_id, category_id, title)
    select id, category_id, title 
    from mod_forum_discussions
    where owner_id is not null;

-- drop category foreign key from discussions
ALTER TABLE mod_forum_discussions DROP FOREIGN KEY fk_forum_discussion_category;
-- drop category_id column from discussions
ALTER TABLE mod_forum_discussions DROP COLUMN category_id;
-- drop title from discussion table
ALTER TABLE mod_forum_discussions DROP COLUMN title;

-- move discussion data (date_created, owner, description) to comments table (date_posted, commentor, content)
insert into mod_forum_comments (discussion_id, date_posted, commentor_id, content)
    select id, date_created, owner_id, description
    from mod_forum_discussions 
    where owner_id is not null;
-- drop discussion description, owner id and date created
ALTER TABLE mod_forum_discussions DROP FOREIGN KEY fk_forum_discussion_manager;
ALTER TABLE mod_forum_discussions DROP COLUMN description;
ALTER TABLE mod_forum_discussions DROP COLUMN owner_id;
ALTER TABLE mod_forum_discussions DROP COLUMN date_created;

-- drop unique name column from forum categories
ALTER TABLE mod_forum_categories DROP COLUMN name;

-- fix for discussions date updated
ALTER TABLE mod_forum_discussions MODIFY COLUMN date_updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

-- add direct connectio to category discussion metadata
ALTER TABLE mod_forum_category_discussions ADD (metadata_id INT NOT NULL);

-- copy the ids of the first comments as metadata
update mod_forum_category_discussions cd, 
 (
    select metadata_id, discussion_id
    from (
    select t.id as metadata_id, d.id as discussion_id, cd.title, t.date_posted from mod_forum_comments t
    join mod_forum_discussions d on t.discussion_id = d.id
    join mod_forum_category_discussions cd on cd.discussion_id = d.id
    order by date_posted asc
    ) foo
    group by discussion_id
) c 
set cd.metadata_id = c.metadata_id
where cd.discussion_id = c.discussion_id;

-- @leagues: add separate property for privacy -- 
ALTER TABLE mod_custom_leagues ADD (privacy SMALLINT(1) UNSIGNED NOT NULL);
update mod_custom_leagues set privacy=3 where type=2;
update mod_custom_leagues set privacy=1 where type=1;
update mod_custom_leagues set type=2;
update mod_custom_leagues set type=3 where creator_id=0;
update mod_custom_leagues set creator_id=null where creator_id=0;

-- @leagues: add property for whether joining a league is enabled --
ALTER TABLE mod_custom_leagues ADD (open BOOL DEFAULT '1' NOT NULL);

-- @leagues: add metadata property for leagues
ALTER TABLE mod_custom_leagues ADD (metadata VARCHAR(128) DEFAULT '[]');

--terms and conditions
ALTER TABLE manager ADD (accepted_terms TINYINT(1) DEFAULT '0' NOT NULL);

--visibility for discussions
ALTER TABLE mod_forum_discussions ADD (visible TINYINT(1) DEFAULT '1' NOT NULL);

-- @league config: added primary key.
ALTER TABLE mod_custom_leagues_config ADD (id INT);
UPDATE mod_custom_leagues_config SET id=1;
ALTER TABLE mod_custom_leagues_config ADD PRIMARY KEY (id);

-- @game config: added configurable commission fee percentage
ALTER TABLE gameconfiguration ADD (commission_perc INT DEFAULT '2' NOT NULL);

-- @league: default metadata value
update mod_custom_leagues set metadata='[]' where metadata is null;

delete from mod_message_reported;
delete from mod_message_messages;
drop table mod_message_reported;
drop table mod_message_messages;

-- @PersonalMessage module

-- -----------------------------------------------------
-- Table `msleague`.`mod_pm_threads`
-- -----------------------------------------------------
CREATE TABLE `mod_pm_threads` (
  `discussion_id` int(11) NOT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `deleted_by_sender` tinyint(1) NOT NULL,
  `deleted_by_recipient` tinyint(1) NOT NULL,
  `read_by_sender` tinyint(1) NOT NULL,
  `read_by_recipient` tinyint(1) NOT NULL,
  `system` tinyint(1) NOT NULL,
  PRIMARY KEY (`discussion_id`),
  KEY `fk_pm_threads_sender_manager` (`sender_id`),
  KEY `fk_pm_threads_recipient_manager` (`recipient_id`),
  CONSTRAINT `fk_pm_threads_discussion` FOREIGN KEY (`discussion_id`) REFERENCES `mod_forum_discussions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pm_threads_recipient_manager` FOREIGN KEY (`recipient_id`) REFERENCES `manager` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `fk_pm_threads_sender_manager` FOREIGN KEY (`sender_id`) REFERENCES `manager` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------
-- Table `msleague`.`mod_pm_threads_comments`
-- -----------------------------------------------------
CREATE TABLE `mod_pm_threads_comments` (
  `comment_id` int(11) NOT NULL,
  `deleted_by_sender` tinyint(1) NOT NULL,
  `deleted_by_recipient` tinyint(1) NOT NULL,
  PRIMARY KEY (`comment_id`),
  CONSTRAINT `fk_mod_pm_threads_comment_id` FOREIGN KEY (`comment_id`) REFERENCES `mod_forum_comments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- @Credits module
CREATE TABLE `mod_credits_manager` (
  `manager_id` int(11) NOT NULL,
  `credits` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`manager_id`),
  CONSTRAINT `mod_credits_manager_manager` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `mod_credits_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `credits` int(11) NOT NULL DEFAULT '0',
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `image` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `mod_payments_history` (
  `manager_id` int(11) NOT NULL,
  `transaction_no` varchar(50) NOT NULL,
  `payment_provider` varchar(10) NOT NULL DEFAULT 'default',
  `date_completed` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`manager_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE feature;
ALTER TABLE gameconfiguration ADD (is_registration_open TINYINT(1) DEFAULT '0' NOT NULL);
