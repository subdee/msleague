-- MySQL dump 10.13  Distrib 5.1.58, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: msleague
-- ------------------------------------------------------
-- Server version	5.1.58-1ubuntu1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

USE `msleague`;

--
-- Dumping data for table `basic_position`
--

LOCK TABLES `basic_position` WRITE;
/*!40000 ALTER TABLE `basic_position` DISABLE KEYS */;
INSERT INTO `basic_position` VALUES (1,'Goalkeeper','GK',2),(2,'Defender','DE',5),(3,'Midfielder','MF',5),(4,'Striker','ST',3);
/*!40000 ALTER TABLE `basic_position` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'Afghanistan','AF','AFG','00','0','93'),(2,'Aland Islands','AX','ALA','00','0','358'),(3,'Albania','AL','ALB','00','0','355'),(4,'Algeria','DZ','DZA','00','7','213'),(5,'American Samoa','AS','ASM','011','1','1'),(6,'Andorra','AD','AND','00','','376'),(7,'Angola','AO','AGO','00','0','244'),(8,'Anguilla','AI','AIA','011','1','1'),(9,'Antarctica','AQ','ATA','011','1','1'),(10,'Antigua and Barbuda','AG','ATG','011','1','1'),(11,'Argentina','AR','ARG','00','0','54'),(12,'Armenia','AM','ARM','00','8','374'),(13,'Aruba','AW','ABW','00','','297'),(14,'Australia','AU','AUS','0011','0','61'),(15,'Austria','AT','AUT','00','0','43'),(16,'Azerbaijan','AZ','AZE','810','8','994'),(17,'Bahamas','BS','BHS','011','1','1'),(18,'Bahrain','BH','BHR','00','','973'),(19,'Bangladesh','BD','BGD','00','0','880'),(20,'Barbados','BB','BRB','011','1','1'),(21,'Belarus','BY','BLR','810','8','375'),(22,'Belgium','BE','BEL','00','0','32'),(23,'Belize','BZ','BLZ','00','0','501'),(24,'Benin','BJ','BEN','00','','229'),(25,'Bermuda','BM','BMU','011','1','1'),(26,'Bhutan','BT','BTN','00','','975'),(27,'Bolivia','BO','BOL','0010','10','591'),(28,'Bosnia and Herzegovina','BA','BIH','00','0','387'),(29,'Botswana','BW','BWA','00','','267'),(30,'Bouvet Island','BV','BVT','011','1','1'),(31,'Brazil','BR','BRA','0014','014','55'),(32,'British Indian Ocean Territory','IO','IOT','011','1','1'),(33,'Brunei Darussalam','BN','BRN','00','0','673'),(34,'Bulgaria','BG','BGR','00','0','359'),(35,'Burkina Faso','BF','BFA','00','','226'),(36,'Burundi','BI','BDI','90','','257'),(37,'Cambodia','KH','KHM','00','0','855'),(38,'Cameroon','CM','CMR','00','','237'),(39,'Canada','CA','CAN','011','1','1'),(40,'Cape Verde','CV','CPV','00','','238'),(41,'Cayman Islands','KY','CYM','011','1','1'),(42,'Central African Republic','CF','CAF','19','','236'),(43,'Chad','TD','TCD','15','','235'),(44,'Chile','CL','CHL','1200','0','56'),(45,'China','CN','CHN','00','0','86'),(46,'Christmas Island','CX','CXR','011','0','618'),(47,'Cocos (Keeling) Islands','CC','CCK','011','0','61'),(48,'Colombia','CO','COL','009','5','57'),(49,'Comoros','KM','COM','00','','269'),(50,'Congo','CG','COG','00','','242'),(51,'Congo, The Democratic Republic of the','CD','COD','00','','243'),(52,'Cook Islands','CK','COK','00','0','682'),(53,'Costa Rica','CR','CRI','00','','506'),(54,'Cote D\'ivoire','CI','CIV','00','0','225'),(55,'Croatia','HR','HRV','00','0','385'),(56,'Cuba','CU','CUB','119','0','53'),(57,'Cyprus','CY','CYP','00','','357'),(58,'Czech Republic','CZ','CZE','00','','420'),(59,'Denmark','DK','DNK','00','','45'),(60,'Djibouti','DJ','DJI','00','','253'),(61,'Dominica','DM','DMA','011','1','1'),(62,'Dominican Republic','DO','DOM','011','1','1'),(63,'Ecuador','EC','ECU','00','0','593'),(64,'Egypt','EG','EGY','00','0','20'),(65,'El Salvador','SV','SLV','00','','503'),(66,'Equatorial Guinea','GQ','GNQ','00','','240'),(67,'Eritrea','ER','ERI','00','0','291'),(68,'Estonia','EE','EST','00','','372'),(69,'Ethiopia','ET','ETH','00','0','251'),(70,'Falkland Islands (Malvinas)','FK','FLK','00','','500'),(71,'Faroe Islands','FO','FRO','00','','298'),(72,'Fiji','FJ','FJI','00','','679'),(73,'Finland','FI','FIN','00','0','358'),(74,'France','FR','FRA','00','0','33'),(75,'French Guiana','GF','GUF','00','','594'),(76,'French Polynesia','PF','PYF','00','','689'),(77,'French Southern Territories','TF','ATF','011','1','1'),(78,'Gabon','GA','GAB','00','','241'),(79,'Gambia','GM','GMB','00','','220'),(80,'Georgia','GE','GEO','810','8','995'),(81,'Germany','DE','DEU','00','0','49'),(82,'Ghana','GH','GHA','00','','233'),(83,'Gibraltar','GI','GIB','00','','350'),(84,'Greece','GR','GRC','00','','30'),(85,'Greenland','GL','GRL','9','','299'),(86,'Grenada','GD','GRD','011','1','1'),(87,'Guadeloupe','GP','GLP','00','','590'),(88,'Guam','GU','GUM','011','1','1'),(89,'Guatemala','GT','GTM','00','','502'),(90,'Guernsey','GG','GGY','00','0','44'),(91,'Guinea','GN','GIN','00','','224'),(92,'Guinea-Bissau','GW','GNB','00','','245'),(93,'Guyana','GY','GUY','00','','592'),(94,'Haiti','HT','HTI','00','','509'),(97,'Honduras','HN','HND','00','','504'),(98,'Hong Kong','HK','HKG','001','','852'),(99,'Hungary','HU','HUN','00','6','36'),(100,'Iceland','IS','ISL','00','0','354'),(101,'India','IN','IND','00','0','91'),(102,'Indonesia','ID','IDN','001','0','62'),(103,'Iran, Islamic Republic of','IR','IRN','00','0','98'),(104,'Iraq','IQ','IRQ','00','','964'),(105,'Ireland','IE','IRL','00','0','353'),(106,'Isle of Man','IM','IMN','00','0','44'),(107,'Israel','IL','ISR','00','0','972'),(108,'Italy','IT','ITA','00','','39'),(109,'Jamaica','JM','JAM','011','1','1'),(110,'Japan','JP','JPN','010','0','81'),(111,'Jersey','JE','JEY','00','0','44'),(112,'Jordan','JO','JOR','00','0','962'),(113,'Kazakhstan','KZ','KAZ','810','8','7'),(114,'Kenya','KE','KEN','000','0','254'),(115,'Kiribati','KI','KIR','00','','686'),(116,'Korea, Democratic People\'s Republic of','KP','PRK','00','0','850'),(117,'Korea, Republic of','KR','KOR','001','0','82'),(118,'Kuwait','KW','KWT','00','','965'),(119,'Kyrgyzstan','KG','KGZ','00','0','996'),(120,'Lao People\'s Democratic Republic','LA','LAO','00','0','856'),(121,'Latvia','LV','LVA','00','8','371'),(122,'Lebanon','LB','LBN','00','0','961'),(123,'Lesotho','LS','LSO','00','','266'),(124,'Liberia','LR','LBR','00','22','231'),(125,'Libyan Arab Jamahiriya','LY','LBY','00','0','218'),(126,'Liechtenstein','LI','LIE','00','','423'),(127,'Lithuania','LT','LTU','00','8','370'),(128,'Luxembourg','LU','LUX','00','','352'),(129,'Macao','MO','MAC','00','0','853'),(130,'Macedonia, The Former Yugoslav Republic of','MK','MKD','00','0','389'),(131,'Madagascar','MG','MDG','00','','261'),(132,'Malawi','MW','MWI','00','','265'),(133,'Malaysia','MY','MYS','00','0','60'),(134,'Maldives','MV','MDV','00','','960'),(135,'Mali','ML','MLI','00','0','223'),(136,'Malta','MT','MLT','00','21','356'),(137,'Marshall Islands','MH','MHL','00','1','692'),(138,'Martinique','MQ','MTQ','00','','596'),(139,'Mauritania','MR','MRT','00','0','222'),(140,'Mauritius','MU','MUS','020','','230'),(141,'Mayotte','YT','MYT','00','','269'),(142,'Mexico','MX','MEX','00','1','52'),(143,'Micronesia, Federated States of','FM','FSM','011','1','691'),(144,'Moldova, Republic of','MD','MDA','00','0','373'),(145,'Monaco','MC','MCO','00','0','377'),(146,'Mongolia','MN','MNG','001','0','976'),(147,'Montserrat','MS','MSR','011','1','1'),(148,'Morocco','MA','MAR','00','0','212'),(149,'Mozambique','MZ','MOZ','00','0','258'),(150,'Myanmar','MM','MMR','00','','95'),(151,'Namibia','NA','NAM','00','0','264'),(152,'Nauru','NR','NRU','00','0','674'),(153,'Nepal','NP','NPL','00','0','977'),(154,'Netherlands','NL','NLD','00','0','31'),(155,'Netherlands Antilles','AN','ANT','00','0','599'),(156,'New Caledonia','NC','NCL','00','','687'),(157,'New Zealand','NZ','NZL','00','0','64'),(158,'Nicaragua','NI','NIC','00','','505'),(159,'Niger','NE','NER','00','0','227'),(160,'Nigeria','NG','NGA','009','0','234'),(161,'Niue','NU','NIU','00','','683'),(162,'Norfolk Island','NF','NFK','00','','672'),(163,'Northern Mariana Islands','MP','MNP','011','1','1'),(164,'Norway','NO','NOR','00','','47'),(165,'Oman','OM','OMN','00','','968'),(166,'Pakistan','PK','PAK','00','0','92'),(167,'Palau','PW','PLW','00','','680'),(168,'Palestinian Territory, Occupied','PS','PSE','00','0','970'),(169,'Panama','PA','PAN','00','','507'),(170,'Papua New Guinea','PG','PNG','5','','675'),(171,'Paraguay','PY','PRY','002','0','595'),(172,'Peru','PE','PER','00','0','51'),(173,'Philippines','PH','PHL','00','0','63'),(174,'Pitcairn','PN','PCN','00','0','872'),(175,'Poland','PL','POL','00','0','48'),(176,'Portugal','PT','PRT','00','','351'),(177,'Puerto Rico','PR','PRI','011','1','1'),(178,'Qatar','QA','QAT','00','','974'),(179,'Reunion','RE','REU','00','','262'),(180,'Romania','RO','ROU','00','0','40'),(181,'Russian Federation','RU','RUS','810','8','7'),(182,'Rwanda','RW','RWA','00','0','250'),(183,'Saint Helena','SH','SHN','00','','290'),(184,'Saint Kitts and Nevis','KN','KNA','011','1','1'),(185,'Saint Lucia','LC','LCA','011','1','1'),(186,'Saint Pierre and Miquelon','PM','SPM','00','0','508'),(187,'Saint Vincent and the Grenadines','VC','VCT','011','1','1'),(188,'Samoa','WS','WSM','00','','685'),(189,'San Marino','SM','SMR','00','','378'),(190,'Sao Tome and Principe','ST','STP','00','0','239'),(191,'Saudi Arabia','SA','SAU','00','0','966'),(192,'Senegal','SN','SEN','00','','221'),(193,'Serbia','RS','SRB','99','0','381'),(194,'Seychelles','SC','SYC','00','','248'),(195,'Sierra Leone','SL','SLE','00','0','232'),(196,'Singapore','SG','SGP','001','','65'),(197,'Slovakia','SK','SVK','00','0','421'),(198,'Slovenia','SI','SVN','00','0','386'),(199,'Solomon Islands','SB','SLB','00','','677'),(200,'Somalia','SO','SOM','00','','252'),(201,'South Africa','ZA','ZAF','9','0','27'),(202,'South Georgia and the South Sandwich Islands','GS','SGS','011','1','1'),(203,'Spain','ES','ESP','00','','34'),(204,'Sri Lanka','LK','LKA','00','0','94'),(205,'Sudan','SD','SDN','00','0','249'),(206,'Suriname','SR','SUR','00','0','597'),(207,'Svalbard and Jan Mayen','SJ','SJM','00','','47'),(208,'Swaziland','SZ','SWZ','00','','268'),(209,'Sweden','SE','SWE','00','0','46'),(210,'Switzerland','CH','CHE','00','0','41'),(211,'Syrian Arab Republic','SY','SYR','00','0','963'),(212,'Taiwan','TW','TWN','002','','886'),(213,'Tajikistan','TJ','TJK','810','8','992'),(214,'Tanzania, United Republic of','TZ','TZA','000','0','255'),(215,'Thailand','TH','THA','001','0','66'),(216,'Timor-Leste','TL','TLS','','','670'),(217,'Togo','TG','TGO','00','','228'),(218,'Tokelau','TK','TKL','00','','690'),(219,'Tonga','TO','TON','00','','676'),(220,'Trinidad and Tobago','TT','TTO','011','1','1'),(221,'Tunisia','TN','TUN','00','','216'),(222,'Turkey','TR','TUR','00','0','90'),(223,'Turkmenistan','TM','TKM','810','8','993'),(224,'Turks and Caicos Islands','TC','TCA','011','1','1'),(225,'Tuvalu','TV','TUV','00','','688'),(226,'Uganda','UG','UGA','000','0','256'),(227,'Ukraine','UA','UKR','00','8','380'),(228,'United Arab Emirates','AE','ARE','00','0','971'),(229,'United Kingdom','GB','GBR','00','0','44'),(230,'United States','US','USA','011','1','1'),(231,'United States Minor Outlying Islands','UM','UMI','011','1','1'),(232,'Uruguay','UY','URY','00','0','598'),(233,'Uzbekistan','UZ','UZB','810','8','998'),(234,'Vanuatu','VU','VUT','00','','678'),(235,'Vatican City State','VA','VAT','00','','39'),(236,'Venezuela','VE','VEN','00','0','58'),(237,'Viet Nam','VN','VNM','00','0','84'),(238,'Virgin Islands, British','VG','VGB','011','1','1'),(239,'Virgin Islands, US','VI','VIR','011','1','1'),(240,'Wallis and Futuna','WF','WLF','19','','681'),(241,'Western Sahara','EH','ESH','011','1','1'),(242,'Yemen','YE','YEM','00','0','967'),(243,'Zambia','ZM','ZMB','00','0','260'),(244,'Zimbabwe','ZW','ZWE','00','0','263'),(245,'Montenegro','ME','MNE','99','0','382');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (1,'Minutes Played',1,2,0),(3,'Goals Scored',1,15,0),(4,'Assists',1,4,0),(9,'Penalties Saved',1,5,0),(11,'Red Cards',1,-4,0),(12,'Own Goals',1,-5,0),(13,'Missed Penalties',1,-4,0),(14,'Minutes Played',2,2,0),(16,'Goals Scored',2,12,0),(17,'Assists',2,4,0),(22,'Penalties Saved',2,5,0),(24,'Red Cards',2,-4,0),(25,'Own Goals',2,-5,0),(26,'Missed Penalties',2,-4,0),(27,'Minutes Played',3,2,0),(29,'Goals Scored',3,10,0),(30,'Assists',3,4,0),(35,'Penalties Saved',3,5,0),(37,'Red Cards',3,-4,0),(38,'Own Goals',3,-5,0),(39,'Missed Penalties',3,-4,0),(40,'Minutes Played',4,2,0),(42,'Goals Scored',4,8,0),(43,'Assists',4,4,0),(48,'Penalties Saved',4,5,0),(50,'Red Cards',4,-4,0),(51,'Own Goals',4,-5,0),(52,'Missed Penalties',4,-4,0),(57,'No Goals',1,0,1),(58,'No Goals',2,0,1),(59,'No Goals',3,-1,1),(60,'No Goals',4,-1,1),(61,'Clean Sheet',1,3,1),(62,'Clean Sheet',2,2,1),(63,'Clean Sheet',3,0,1),(64,'Clean Sheet',4,0,1),(65,'Goals Against',1,-1,1),(66,'Goals Against',2,-1,1),(67,'Goals Against',3,0,1),(68,'Goals Against',4,0,1);
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `feature`
--

LOCK TABLES `feature` WRITE;
/*!40000 ALTER TABLE `feature` DISABLE KEYS */;
INSERT INTO `feature` VALUES ('Chat',0),('Registration',1);
/*!40000 ALTER TABLE `feature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `formation`
--

LOCK TABLES `formation` WRITE;
/*!40000 ALTER TABLE `formation` DISABLE KEYS */;
INSERT INTO `formation` VALUES (2,'433'),(1,'442'),(3,'532');
/*!40000 ALTER TABLE `formation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `gameconfiguration`
--

LOCK TABLES `gameconfiguration` WRITE;
/*!40000 ALTER TABLE `gameconfiguration` DISABLE KEYS */;
INSERT INTO `gameconfiguration` VALUES ('Game Name','support@email',30,1,1000,200000,1000,15,18,4,1,15,'1',3,5,100000000,20,30,0,353,'Newsletter footer',NULL,'http://localhost/','no-reply@email',10,2);
/*!40000 ALTER TABLE `gameconfiguration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `language`
--

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
INSERT INTO `language` VALUES (1,'el');
/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `manager_task_desc`
--

LOCK TABLES `manager_task_desc` WRITE;
/*!40000 ALTER TABLE `manager_task_desc` DISABLE KEYS */;
INSERT INTO `manager_task_desc` VALUES (1,'makeOneSubstitution',1,NULL,'onSwapPlayers',NULL),(2,'assignLeader',1,NULL,'onAssignLeader',NULL),(3,'changeFormation',1,NULL,'onChangeFormation',NULL),(4,'makeOneTransaction',1,NULL,'onStockmarketTransaction',NULL);
/*!40000 ALTER TABLE `manager_task_desc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `position`
--

LOCK TABLES `position` WRITE;
/*!40000 ALTER TABLE `position` DISABLE KEYS */;
INSERT INTO `position` VALUES (5,'Left Defender','LD',2),(6,'Right Defender','RD',2),(7,'Center Left Defender','CLD',2),(8,'Center Right Defender','CRD',2),(9,'Left Midfielder','LM',3),(10,'Right Midfielder','RM',3),(11,'Center Left Midfielder','CLM',3),(12,'Center Right Midfielder','CRM',3),(13,'Right Striker','RS',4),(14,'Left Striker','LS',4),(15,'Substitute Defender','SD',2),(16,'Substitute Midfielder','SM',3),(17,'Substitute Striker','SS',4),(18,'Substitute Goalkeeper','SG',1),(19,'Center Midfielder','CM',3),(20,'Center Striker','CS',4),(21,'Center Defender','CD',2),(38,'Goalkeeper','GK',1);
/*!40000 ALTER TABLE `position` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `report_action`
--

LOCK TABLES `report_action` WRITE;
/*!40000 ALTER TABLE `report_action` DISABLE KEYS */;
INSERT INTO `report_action` VALUES (1,'profile'),(2,'forum'),(3,'message'),(4,'delete'),(5,'league');
/*!40000 ALTER TABLE `report_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `script_configuration`
--

LOCK TABLES `script_configuration` WRITE;
/*!40000 ALTER TABLE `script_configuration` DISABLE KEYS */;
INSERT INTO `script_configuration` VALUES (6,'0:00',1,'2011-09-16 06:17:43','14:45','5,1','5,0',1);
/*!40000 ALTER TABLE `script_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `static_content`
--

LOCK TABLES `static_content` WRITE;
/*!40000 ALTER TABLE `static_content` DISABLE KEYS */;
INSERT INTO `static_content` VALUES (1,'Guest','Please enter some text',1,'2011-11-25 11:43:23'),(2,'Instructions','Please enter some text',1,'2011-11-25 11:43:23'),(3,'FAQ','Please enter some text',1,'2011-11-25 11:43:23'),(4,'HowToPlay','Please enter some text',1,'2011-11-25 11:43:23'),(5,'TermsOfUse','Please enter some text',1,'2011-11-25 11:43:23'),(6,'PrivacyPolicy','Please enter some text',1,'2011-11-25 11:43:23'),(7,'InvitationMailDefault','Please enter some text',1,'2011-11-25 11:43:23'),(8,'WelcomeMessage','Please enter some text',1,'2011-11-25 11:43:23'),(9,'VideoUrl','http://video-url/',1,'2011-11-25 11:43:23'),(10,'Awards','Please enter some text',1,'2011-11-25 11:43:23');
/*!40000 ALTER TABLE `static_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` VALUES (1,'Leader','2.00'),(2,'Starter','1.00'),(3,'Substitute','0.50');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `timezone`
--

LOCK TABLES `timezone` WRITE;
/*!40000 ALTER TABLE `timezone` DISABLE KEYS */;
INSERT INTO `timezone` VALUES (2,'Africa/Abidjan'),(3,'Africa/Accra'),(4,'Africa/Addis_Ababa'),(5,'Africa/Algiers'),(6,'Africa/Asmara'),(7,'Africa/Asmera'),(8,'Africa/Bamako'),(9,'Africa/Bangui'),(10,'Africa/Banjul'),(11,'Africa/Bissau'),(12,'Africa/Blantyre'),(13,'Africa/Brazzaville'),(14,'Africa/Bujumbura'),(15,'Africa/Cairo'),(16,'Africa/Casablanca'),(17,'Africa/Ceuta'),(18,'Africa/Conakry'),(19,'Africa/Dakar'),(20,'Africa/Dar_es_Salaam'),(21,'Africa/Djibouti'),(22,'Africa/Douala'),(23,'Africa/El_Aaiun'),(24,'Africa/Freetown'),(25,'Africa/Gaborone'),(26,'Africa/Harare'),(27,'Africa/Johannesburg'),(28,'Africa/Kampala'),(29,'Africa/Khartoum'),(30,'Africa/Kigali'),(31,'Africa/Kinshasa'),(32,'Africa/Lagos'),(33,'Africa/Libreville'),(34,'Africa/Lome'),(35,'Africa/Luanda'),(36,'Africa/Lubumbashi'),(37,'Africa/Lusaka'),(38,'Africa/Malabo'),(39,'Africa/Maputo'),(40,'Africa/Maseru'),(41,'Africa/Mbabane'),(42,'Africa/Mogadishu'),(43,'Africa/Monrovia'),(44,'Africa/Nairobi'),(45,'Africa/Ndjamena'),(46,'Africa/Niamey'),(47,'Africa/Nouakchott'),(48,'Africa/Ouagadougou'),(49,'Africa/Porto-Novo'),(50,'Africa/Sao_Tome'),(51,'Africa/Timbuktu'),(52,'Africa/Tripoli'),(53,'Africa/Tunis'),(54,'Africa/Windhoek'),(55,'America/Adak'),(56,'America/Anchorage'),(57,'America/Anguilla'),(58,'America/Antigua'),(59,'America/Araguaina'),(60,'America/Argentina/Buenos_Aires'),(61,'America/Argentina/Catamarca'),(62,'America/Argentina/ComodRivadavia'),(63,'America/Argentina/Cordoba'),(64,'America/Argentina/Jujuy'),(65,'America/Argentina/La_Rioja'),(66,'America/Argentina/Mendoza'),(67,'America/Argentina/Rio_Gallegos'),(68,'America/Argentina/Salta'),(69,'America/Argentina/San_Juan'),(70,'America/Argentina/San_Luis'),(71,'America/Argentina/Tucuman'),(72,'America/Argentina/Ushuaia'),(73,'America/Aruba'),(74,'America/Asuncion'),(75,'America/Atikokan'),(76,'America/Atka'),(77,'America/Bahia'),(78,'America/Bahia_Banderas'),(79,'America/Barbados'),(80,'America/Belem'),(81,'America/Belize'),(82,'America/Blanc-Sablon'),(83,'America/Boa_Vista'),(84,'America/Bogota'),(85,'America/Boise'),(86,'America/Buenos_Aires'),(87,'America/Cambridge_Bay'),(88,'America/Campo_Grande'),(89,'America/Cancun'),(90,'America/Caracas'),(91,'America/Catamarca'),(92,'America/Cayenne'),(93,'America/Cayman'),(94,'America/Chicago'),(95,'America/Chihuahua'),(96,'America/Coral_Harbour'),(97,'America/Cordoba'),(98,'America/Costa_Rica'),(99,'America/Cuiaba'),(100,'America/Curacao'),(101,'America/Danmarkshavn'),(102,'America/Dawson'),(103,'America/Dawson_Creek'),(104,'America/Denver'),(105,'America/Detroit'),(106,'America/Dominica'),(107,'America/Edmonton'),(108,'America/Eirunepe'),(109,'America/El_Salvador'),(110,'America/Ensenada'),(111,'America/Fortaleza'),(112,'America/Fort_Wayne'),(113,'America/Glace_Bay'),(114,'America/Godthab'),(115,'America/Goose_Bay'),(116,'America/Grand_Turk'),(117,'America/Grenada'),(118,'America/Guadeloupe'),(119,'America/Guatemala'),(120,'America/Guayaquil'),(121,'America/Guyana'),(122,'America/Halifax'),(123,'America/Havana'),(124,'America/Hermosillo'),(125,'America/Indiana/Indianapolis'),(126,'America/Indiana/Knox'),(127,'America/Indiana/Marengo'),(128,'America/Indiana/Petersburg'),(129,'America/Indianapolis'),(130,'America/Indiana/Tell_City'),(131,'America/Indiana/Vevay'),(132,'America/Indiana/Vincennes'),(133,'America/Indiana/Winamac'),(134,'America/Inuvik'),(135,'America/Iqaluit'),(136,'America/Jamaica'),(137,'America/Jujuy'),(138,'America/Juneau'),(139,'America/Kentucky/Louisville'),(140,'America/Kentucky/Monticello'),(141,'America/Knox_IN'),(142,'America/La_Paz'),(143,'America/Lima'),(144,'America/Los_Angeles'),(145,'America/Louisville'),(146,'America/Maceio'),(147,'America/Managua'),(148,'America/Manaus'),(149,'America/Marigot'),(150,'America/Martinique'),(151,'America/Matamoros'),(152,'America/Mazatlan'),(153,'America/Mendoza'),(154,'America/Menominee'),(155,'America/Merida'),(156,'America/Metlakatla'),(157,'America/Mexico_City'),(158,'America/Miquelon'),(159,'America/Moncton'),(160,'America/Monterrey'),(161,'America/Montevideo'),(162,'America/Montreal'),(163,'America/Montserrat'),(164,'America/Nassau'),(165,'America/New_York'),(166,'America/Nipigon'),(167,'America/Nome'),(168,'America/Noronha'),(169,'America/North_Dakota/Beulah'),(170,'America/North_Dakota/Center'),(171,'America/North_Dakota/New_Salem'),(172,'America/Ojinaga'),(173,'America/Panama'),(174,'America/Pangnirtung'),(175,'America/Paramaribo'),(176,'America/Phoenix'),(177,'America/Port-au-Prince'),(178,'America/Porto_Acre'),(179,'America/Port_of_Spain'),(180,'America/Porto_Velho'),(181,'America/Puerto_Rico'),(182,'America/Rainy_River'),(183,'America/Rankin_Inlet'),(184,'America/Recife'),(185,'America/Regina'),(186,'America/Resolute'),(187,'America/Rio_Branco'),(188,'America/Rosario'),(189,'America/Santa_Isabel'),(190,'America/Santarem'),(191,'America/Santiago'),(192,'America/Santo_Domingo'),(193,'America/Sao_Paulo'),(194,'America/Scoresbysund'),(195,'America/Shiprock'),(196,'America/Sitka'),(197,'America/St_Barthelemy'),(198,'America/St_Johns'),(199,'America/St_Kitts'),(200,'America/St_Lucia'),(201,'America/St_Thomas'),(202,'America/St_Vincent'),(203,'America/Swift_Current'),(204,'America/Tegucigalpa'),(205,'America/Thule'),(206,'America/Thunder_Bay'),(207,'America/Tijuana'),(208,'America/Toronto'),(209,'America/Tortola'),(210,'America/Vancouver'),(211,'America/Virgin'),(212,'America/Whitehorse'),(213,'America/Winnipeg'),(214,'America/Yakutat'),(215,'America/Yellowknife'),(216,'Antarctica/Casey'),(217,'Antarctica/Davis'),(218,'Antarctica/DumontDUrville'),(219,'Antarctica/Macquarie'),(220,'Antarctica/Mawson'),(221,'Antarctica/McMurdo'),(222,'Antarctica/Palmer'),(223,'Antarctica/Rothera'),(224,'Antarctica/South_Pole'),(225,'Antarctica/Syowa'),(226,'Antarctica/Vostok'),(227,'Arctic/Longyearbyen'),(228,'Asia/Aden'),(229,'Asia/Almaty'),(230,'Asia/Amman'),(231,'Asia/Anadyr'),(232,'Asia/Aqtau'),(233,'Asia/Aqtobe'),(234,'Asia/Ashgabat'),(235,'Asia/Ashkhabad'),(236,'Asia/Baghdad'),(237,'Asia/Bahrain'),(238,'Asia/Baku'),(239,'Asia/Bangkok'),(240,'Asia/Beirut'),(241,'Asia/Bishkek'),(242,'Asia/Brunei'),(243,'Asia/Calcutta'),(244,'Asia/Choibalsan'),(245,'Asia/Chongqing'),(246,'Asia/Chungking'),(247,'Asia/Colombo'),(248,'Asia/Dacca'),(249,'Asia/Damascus'),(250,'Asia/Dhaka'),(251,'Asia/Dili'),(252,'Asia/Dubai'),(253,'Asia/Dushanbe'),(254,'Asia/Gaza'),(255,'Asia/Harbin'),(256,'Asia/Ho_Chi_Minh'),(257,'Asia/Hong_Kong'),(258,'Asia/Hovd'),(259,'Asia/Irkutsk'),(260,'Asia/Istanbul'),(261,'Asia/Jakarta'),(262,'Asia/Jayapura'),(263,'Asia/Jerusalem'),(264,'Asia/Kabul'),(265,'Asia/Kamchatka'),(266,'Asia/Karachi'),(267,'Asia/Kashgar'),(268,'Asia/Kathmandu'),(269,'Asia/Katmandu'),(270,'Asia/Kolkata'),(271,'Asia/Krasnoyarsk'),(272,'Asia/Kuala_Lumpur'),(273,'Asia/Kuching'),(274,'Asia/Kuwait'),(275,'Asia/Macao'),(276,'Asia/Macau'),(277,'Asia/Magadan'),(278,'Asia/Makassar'),(279,'Asia/Manila'),(280,'Asia/Muscat'),(281,'Asia/Nicosia'),(282,'Asia/Novokuznetsk'),(283,'Asia/Novosibirsk'),(284,'Asia/Omsk'),(285,'Asia/Oral'),(286,'Asia/Phnom_Penh'),(287,'Asia/Pontianak'),(288,'Asia/Pyongyang'),(289,'Asia/Qatar'),(290,'Asia/Qyzylorda'),(291,'Asia/Rangoon'),(292,'Asia/Riyadh'),(293,'Asia/Saigon'),(294,'Asia/Sakhalin'),(295,'Asia/Samarkand'),(296,'Asia/Seoul'),(297,'Asia/Shanghai'),(298,'Asia/Singapore'),(299,'Asia/Taipei'),(300,'Asia/Tashkent'),(301,'Asia/Tbilisi'),(302,'Asia/Tehran'),(303,'Asia/Tel_Aviv'),(304,'Asia/Thimbu'),(305,'Asia/Thimphu'),(306,'Asia/Tokyo'),(307,'Asia/Ujung_Pandang'),(308,'Asia/Ulaanbaatar'),(309,'Asia/Ulan_Bator'),(310,'Asia/Urumqi'),(311,'Asia/Vientiane'),(312,'Asia/Vladivostok'),(313,'Asia/Yakutsk'),(314,'Asia/Yekaterinburg'),(315,'Asia/Yerevan'),(316,'Atlantic/Azores'),(317,'Atlantic/Bermuda'),(318,'Atlantic/Canary'),(319,'Atlantic/Cape_Verde'),(320,'Atlantic/Faeroe'),(321,'Atlantic/Faroe'),(322,'Atlantic/Jan_Mayen'),(323,'Atlantic/Madeira'),(324,'Atlantic/Reykjavik'),(325,'Atlantic/South_Georgia'),(326,'Atlantic/Stanley'),(327,'Atlantic/St_Helena'),(328,'Australia/ACT'),(329,'Australia/Adelaide'),(330,'Australia/Brisbane'),(331,'Australia/Broken_Hill'),(332,'Australia/Canberra'),(333,'Australia/Currie'),(334,'Australia/Darwin'),(335,'Australia/Eucla'),(336,'Australia/Hobart'),(337,'Australia/LHI'),(338,'Australia/Lindeman'),(339,'Australia/Lord_Howe'),(340,'Australia/Melbourne'),(341,'Australia/North'),(342,'Australia/NSW'),(343,'Australia/Perth'),(344,'Australia/Queensland'),(345,'Australia/South'),(346,'Australia/Sydney'),(347,'Australia/Tasmania'),(348,'Australia/Victoria'),(349,'Australia/West'),(350,'Australia/Yancowinna'),(351,'Europe/Amsterdam'),(352,'Europe/Andorra'),(353,'Europe/Athens'),(354,'Europe/Belfast'),(355,'Europe/Belgrade'),(356,'Europe/Berlin'),(357,'Europe/Bratislava'),(358,'Europe/Brussels'),(359,'Europe/Bucharest'),(360,'Europe/Budapest'),(361,'Europe/Chisinau'),(362,'Europe/Copenhagen'),(363,'Europe/Dublin'),(364,'Europe/Gibraltar'),(365,'Europe/Guernsey'),(366,'Europe/Helsinki'),(367,'Europe/Isle_of_Man'),(368,'Europe/Istanbul'),(369,'Europe/Jersey'),(370,'Europe/Kaliningrad'),(371,'Europe/Kiev'),(372,'Europe/Lisbon'),(373,'Europe/Ljubljana'),(374,'Europe/London'),(375,'Europe/Luxembourg'),(376,'Europe/Madrid'),(377,'Europe/Malta'),(378,'Europe/Mariehamn'),(379,'Europe/Minsk'),(380,'Europe/Monaco'),(381,'Europe/Moscow'),(382,'Europe/Nicosia'),(383,'Europe/Oslo'),(384,'Europe/Paris'),(385,'Europe/Podgorica'),(386,'Europe/Prague'),(387,'Europe/Riga'),(388,'Europe/Rome'),(389,'Europe/Samara'),(390,'Europe/San_Marino'),(391,'Europe/Sarajevo'),(392,'Europe/Simferopol'),(393,'Europe/Skopje'),(394,'Europe/Sofia'),(395,'Europe/Stockholm'),(396,'Europe/Tallinn'),(397,'Europe/Tirane'),(398,'Europe/Tiraspol'),(399,'Europe/Uzhgorod'),(400,'Europe/Vaduz'),(401,'Europe/Vatican'),(402,'Europe/Vienna'),(403,'Europe/Vilnius'),(404,'Europe/Volgograd'),(405,'Europe/Warsaw'),(406,'Europe/Zagreb'),(407,'Europe/Zaporozhye'),(408,'Europe/Zurich'),(409,'Indian/Antananarivo'),(410,'Indian/Chagos'),(411,'Indian/Christmas'),(412,'Indian/Cocos'),(413,'Indian/Comoro'),(414,'Indian/Kerguelen'),(415,'Indian/Mahe'),(416,'Indian/Maldives'),(417,'Indian/Mauritius'),(418,'Indian/Mayotte'),(419,'Indian/Reunion'),(420,'Pacific/Apia'),(421,'Pacific/Auckland'),(422,'Pacific/Chatham'),(423,'Pacific/Chuuk'),(424,'Pacific/Easter'),(425,'Pacific/Efate'),(426,'Pacific/Enderbury'),(427,'Pacific/Fakaofo'),(428,'Pacific/Fiji'),(429,'Pacific/Funafuti'),(430,'Pacific/Galapagos'),(431,'Pacific/Gambier'),(432,'Pacific/Guadalcanal'),(433,'Pacific/Guam'),(434,'Pacific/Honolulu'),(435,'Pacific/Johnston'),(436,'Pacific/Kiritimati'),(437,'Pacific/Kosrae'),(438,'Pacific/Kwajalein'),(439,'Pacific/Majuro'),(440,'Pacific/Marquesas'),(441,'Pacific/Midway'),(442,'Pacific/Nauru'),(443,'Pacific/Niue'),(444,'Pacific/Norfolk'),(445,'Pacific/Noumea'),(446,'Pacific/Pago_Pago'),(447,'Pacific/Palau'),(448,'Pacific/Pitcairn'),(449,'Pacific/Pohnpei'),(450,'Pacific/Ponape'),(451,'Pacific/Port_Moresby'),(452,'Pacific/Rarotonga'),(453,'Pacific/Saipan'),(454,'Pacific/Samoa'),(455,'Pacific/Tahiti'),(456,'Pacific/Tarawa'),(457,'Pacific/Tongatapu'),(458,'Pacific/Truk'),(459,'Pacific/Wake'),(460,'Pacific/Wallis'),(461,'Pacific/Yap');
/*!40000 ALTER TABLE `timezone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','e10adc3949ba59abbe56e057f20f883e',4);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

INSERT INTO `mod_custom_leagues_config` (`maxLeaguesPerManager`, `maxManagersPerLeague`, `id`) VALUES
(10, 2, 1);

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

