-- phpMyAdmin SQL Dump
-- version 3.4.5deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 23, 2011 at 01:59 PM
-- Server version: 5.1.58
-- PHP Version: 5.3.6-13ubuntu3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `msleague`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE IF NOT EXISTS `announcement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `date_entered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_announcements_user` (`user_id`),
  KEY `fk_announcement_user` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `ban`
--

CREATE TABLE IF NOT EXISTS `ban` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manager_id` int(11) NOT NULL,
  `report_reason_id` int(11) DEFAULT NULL,
  `duration` int(11) NOT NULL,
  `date_banned` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `fk_ban_manager` (`manager_id`),
  KEY `fk_ban_status_change_reason` (`report_reason_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

-- --------------------------------------------------------

--
-- Table structure for table `basic_position`
--

CREATE TABLE IF NOT EXISTS `basic_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `abbreviation` varchar(3) NOT NULL,
  `min_per_manager` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;


-- --------------------------------------------------------

--
-- Table structure for table `blocked_manager`
--

CREATE TABLE IF NOT EXISTS `blocked_manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manager_id` int(11) NOT NULL,
  `blocked_manager_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_blocked_manager_manager` (`manager_id`),
  KEY `fk_blocked_manager_blocked_manager` (`blocked_manager_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manager_id` int(11) NOT NULL,
  `game_id` int(11) DEFAULT NULL,
  `shout` varchar(160) NOT NULL,
  `date_entered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_chat_manager` (`manager_id`),
  KEY `fk_chat_game` (`game_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme`
--

CREATE TABLE IF NOT EXISTS `cms_theme` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `rule` text NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'image',
  `value` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(45) NOT NULL,
  `iso_2` varchar(4) DEFAULT NULL,
  `iso_3` varchar(4) DEFAULT NULL,
  `idd` varchar(4) DEFAULT NULL,
  `ndd` varchar(4) DEFAULT NULL,
  `dial` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=246 ;


-- --------------------------------------------------------

--
-- Table structure for table `deleted_manager`
--

CREATE TABLE IF NOT EXISTS `deleted_manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_reason_id` int(11) DEFAULT NULL,
  `details` blob,
  `date` timestamp NULL DEFAULT NULL,
  `username` varchar(16) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `team_name` varchar(45) DEFAULT NULL,
  `registration_date` timestamp NULL DEFAULT NULL,
  `total_points` decimal(7,1) DEFAULT NULL,
  `portfolio_cash` decimal(9,2) DEFAULT NULL,
  `portfolio_share_value` decimal(9,2) DEFAULT NULL,
  `gender` enum('M','F') DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `fullname` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_deleted_manager_status_change_reason` (`report_reason_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `basic_position_id` int(11) NOT NULL,
  `points_awarded` int(11) NOT NULL,
  `is_team_event` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_events_basic_position` (`basic_position_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=69 ;


-- --------------------------------------------------------

--
-- Table structure for table `formation`
--

CREATE TABLE IF NOT EXISTS `formation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `formation` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `formation_UNIQUE` (`formation`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE IF NOT EXISTS `game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_home` int(11) NOT NULL,
  `team_away` int(11) NOT NULL,
  `score_home` int(11) DEFAULT NULL,
  `score_away` int(11) DEFAULT NULL,
  `date_played` timestamp NULL DEFAULT NULL,
  `points_calculated` tinyint(1) NOT NULL DEFAULT '0',
  `xml_id` int(11) DEFAULT NULL,
  `is_cancelled` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_games_home` (`team_home`),
  KEY `fk_games_away` (`team_away`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1566 ;

-- --------------------------------------------------------

--
-- Table structure for table `gameconfiguration`
--

CREATE TABLE IF NOT EXISTS `gameconfiguration` (
  `game_name` varchar(30) DEFAULT NULL,
  `support_email` varchar(30) DEFAULT NULL,
  `max_transactions_per_day` int(11) NOT NULL,
  `overall_min_price_per_share` int(11) NOT NULL,
  `overall_max_price_per_share` int(11) NOT NULL,
  `total_shares_per_player` int(11) NOT NULL,
  `manager_initial_budget` int(11) NOT NULL,
  `min_number_of_portfolio_players` int(11) NOT NULL,
  `max_number_of_portfolio_players` int(11) NOT NULL,
  `max_players_same_team` int(11) NOT NULL,
  `is_enabled` tinyint(1) NOT NULL,
  `portfolio_perc_rule` int(11) NOT NULL,
  `id` varchar(45) NOT NULL,
  `limit_up` int(11) NOT NULL,
  `max_reports_per_day` int(2) NOT NULL,
  `max_number_of_registered_users` int(11) NOT NULL,
  `max_invites_per_day` int(1) NOT NULL,
  `matches_front_page` int(2) NOT NULL DEFAULT '10',
  `is_season_open` tinyint(1) NOT NULL DEFAULT '0',
  `timezone_id` int(11) NOT NULL DEFAULT '353',
  `newsletter_footer` varchar(500) DEFAULT NULL,
  `disallowed_usernames` text,
  `site_url` varchar(40) DEFAULT NULL,
  `noreply_email` varchar(40) NOT NULL DEFAULT 'no-reply@msleague.com',
  `max_watchlist_items` int(11) NOT NULL DEFAULT '10',
  `accepted_terms` tinyint(1) NOT NULL DEFAULT '0',
  `commission_perc` int(11) NOT NULL DEFAULT '2',
  `is_registration_open` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_timezone` (`timezone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;


-- --------------------------------------------------------

--
-- Table structure for table `game_player`
--

CREATE TABLE IF NOT EXISTS `game_player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `event_value` int(11) DEFAULT NULL,
  `entered` timestamp NULL DEFAULT NULL,
  `is_home_team` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_game_players_game` (`game_id`),
  KEY `fk_game_players_player` (`player_id`),
  KEY `fk_game_players_event` (`event_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10394 ;

-- --------------------------------------------------------

--
-- Table structure for table `invitation`
--

CREATE TABLE IF NOT EXISTS `invitation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manager_id` int(11) NOT NULL,
  `email` varchar(45) NOT NULL,
  `date_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_activated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `fk_invitation_manager` (`manager_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=73 ;

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `event` text NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `manager`
--

CREATE TABLE IF NOT EXISTS `manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` int(11) NOT NULL DEFAULT '1',
  `last_login` timestamp NULL DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `username` varchar(16) NOT NULL,
  `password` varchar(45) NOT NULL,
  `manager_team_id` int(11) DEFAULT NULL,
  `total_points` decimal(7,1) DEFAULT '0.0',
  `portfolio_share_value` decimal(9,2) DEFAULT '0.00',
  `portfolio_cash` decimal(9,2) DEFAULT '0.00',
  `registration_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `notes` text,
  `last_known_ip` varchar(45) DEFAULT NULL,
  `newsletter` tinyint(1) DEFAULT '1',
  `email_code` varchar(45) DEFAULT NULL,
  `activation_date` timestamp NULL DEFAULT NULL,
  `timezone_id` int(11) DEFAULT NULL,
  `last_activity` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `experience_points` int(5) NOT NULL DEFAULT '0',
  `fullname` varchar(50) NOT NULL,
  `accepted_terms` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_managers_role` (`role`),
  KEY `fk_managers_manager_team` (`manager_team_id`),
  KEY `fk_manager_timezone` (`timezone_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=161593 ;

-- --------------------------------------------------------

--
-- Table structure for table `manager_daily_history`
--

CREATE TABLE IF NOT EXISTS `manager_daily_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manager_id` int(11) NOT NULL,
  `portfolio_value` decimal(9,2) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_manager_daily_history_manager` (`manager_id`),
  KEY `manager_daily_history_date` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `manager_group`
--

CREATE TABLE IF NOT EXISTS `manager_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(26) NOT NULL,
  `query` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `manager_match_vote`
--

CREATE TABLE IF NOT EXISTS `manager_match_vote` (
  `manager_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `vote` int(1) DEFAULT NULL,
  PRIMARY KEY (`manager_id`,`game_id`),
  KEY `manager_match_vote_game` (`game_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `manager_password_reset`
--

CREATE TABLE IF NOT EXISTS `manager_password_reset` (
  `manager_id` int(11) NOT NULL,
  `timeout` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`manager_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `manager_player_rating`
--

CREATE TABLE IF NOT EXISTS `manager_player_rating` (
  `manager_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `rating` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`manager_id`,`player_id`),
  KEY `manager_player_rating_player` (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `manager_point_history`
--

CREATE TABLE IF NOT EXISTS `manager_point_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manager_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `total_points` decimal(9,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_manager_gameweek_history_manager` (`manager_id`),
  KEY `fk_manager_gameweek_history_gameweek` (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `manager_task`
--

CREATE TABLE IF NOT EXISTS `manager_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `manager_id` int(11) NOT NULL,
  `meta` text,
  `rank` int(2) NOT NULL DEFAULT '1',
  `context` varchar(32) NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_manager_actions_action_desc` (`task_id`),
  KEY `fk_manager_actions_manager` (`manager_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1689 ;

-- --------------------------------------------------------

--
-- Table structure for table `manager_task_desc`
--

CREATE TABLE IF NOT EXISTS `manager_task_desc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `points` int(2) NOT NULL,
  `meta` text,
  `event` varchar(32) NOT NULL,
  `class` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `manager_team`
--

CREATE TABLE IF NOT EXISTS `manager_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `formation_id` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `manager_team_name` (`name`),
  KEY `fk_manager_teams_formation` (`formation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=50579 ;

-- --------------------------------------------------------

--
-- Table structure for table `manager_team_player`
--

CREATE TABLE IF NOT EXISTS `manager_team_player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manager_team_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `shares` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_manager_team_players_manager_team` (`manager_team_id`),
  KEY `fk_manager_team_players_player` (`player_id`),
  KEY `fk_manager_team_players_position` (`position_id`),
  KEY `fk_manager_team_players_status` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `manager_team_player_history`
--

CREATE TABLE IF NOT EXISTS `manager_team_player_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `shares` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `manager_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_manager_team_player_history_player` (`player_id`),
  KEY `fk_manager_team_player_history_position` (`position_id`),
  KEY `fk_manager_team_player_history_status` (`status_id`),
  KEY `fk_manager_team_player_history_gameweek` (`date`),
  KEY `fk_manager_team_player_history_manager` (`manager_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mod_credits_manager`
--

CREATE TABLE IF NOT EXISTS `mod_credits_manager` (
  `manager_id` int(11) NOT NULL,
  `credits` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`manager_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mod_credits_package`
--

CREATE TABLE IF NOT EXISTS `mod_credits_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `credits` int(11) NOT NULL DEFAULT '0',
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `image` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mod_custom_leagues`
--

CREATE TABLE IF NOT EXISTS `mod_custom_leagues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `creator_id` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `capacity` int(11) NOT NULL,
  `type` int(1) NOT NULL,
  `privacy` smallint(1) unsigned NOT NULL,
  `open` tinyint(1) NOT NULL DEFAULT '1',
  `metadata` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_leagues_creator_manager` (`creator_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=218 ;

-- --------------------------------------------------------

--
-- Table structure for table `mod_custom_leagues_config`
--

CREATE TABLE IF NOT EXISTS `mod_custom_leagues_config` (
  `maxLeaguesPerManager` int(2) NOT NULL DEFAULT '10',
  `maxManagersPerLeague` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mod_custom_leagues_config`
--

-- --------------------------------------------------------

--
-- Table structure for table `mod_custom_leagues_discussions`
--

CREATE TABLE IF NOT EXISTS `mod_custom_leagues_discussions` (
  `league_id` int(11) NOT NULL,
  `discussion_id` int(11) NOT NULL,
  PRIMARY KEY (`league_id`),
  KEY `fk_custom_league_discussion_discussion` (`discussion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mod_custom_leagues_fan`
--

CREATE TABLE IF NOT EXISTS `mod_custom_leagues_fan` (
  `league_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY (`league_id`,`team_id`),
  KEY `fk_custom_league_fan_team` (`team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mod_custom_leagues_invitations`
--

CREATE TABLE IF NOT EXISTS `mod_custom_leagues_invitations` (
  `league_id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  PRIMARY KEY (`league_id`,`notification_id`),
  KEY `fk_custom_league_notifications_notification` (`notification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mod_custom_leagues_managers`
--

CREATE TABLE IF NOT EXISTS `mod_custom_leagues_managers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `join_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `league_id` int(11) DEFAULT NULL,
  `manager_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_leagues_league_league` (`league_id`),
  KEY `fk_leagues_manager_manager` (`manager_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=877 ;

-- --------------------------------------------------------

--
-- Table structure for table `mod_forum_categories`
--

CREATE TABLE IF NOT EXISTS `mod_forum_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank` smallint(1) NOT NULL,
  `image` varchar(64) DEFAULT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `mod_forum_categories_text`
--

CREATE TABLE IF NOT EXISTS `mod_forum_categories_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  `lang` varchar(6) NOT NULL DEFAULT 'en_US',
  PRIMARY KEY (`id`),
  KEY `fk_forum_category_category_text` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=44 ;

-- --------------------------------------------------------

--
-- Table structure for table `mod_forum_category_discussions`
--

CREATE TABLE IF NOT EXISTS `mod_forum_category_discussions` (
  `category_id` int(11) NOT NULL,
  `discussion_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `metadata_id` int(11) NOT NULL,
  PRIMARY KEY (`discussion_id`),
  KEY `fk_categories_discussions_category` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mod_forum_comments`
--

CREATE TABLE IF NOT EXISTS `mod_forum_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `discussion_id` int(11) NOT NULL,
  `commentor_id` int(11) DEFAULT NULL,
  `date_posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_forum_comment_discussion` (`discussion_id`),
  KEY `fk_forum_comment_commentor` (`commentor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=222 ;

-- --------------------------------------------------------

--
-- Table structure for table `mod_forum_comments_reported`
--

CREATE TABLE IF NOT EXISTS `mod_forum_comments_reported` (
  `comment_id` int(11) NOT NULL,
  `report_id` int(11) NOT NULL,
  PRIMARY KEY (`report_id`),
  KEY `fk_forum_comment_reported_report` (`report_id`),
  KEY `fk_mod_forum_comments_reported_comment` (`comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mod_forum_discussions`
--

CREATE TABLE IF NOT EXISTS `mod_forum_discussions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comments_open` tinyint(1) NOT NULL DEFAULT '1',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=584 ;

-- --------------------------------------------------------

--
-- Table structure for table `mod_match_discussions`
--

CREATE TABLE IF NOT EXISTS `mod_match_discussions` (
  `game_id` int(11) NOT NULL,
  `discussion_id` int(11) NOT NULL,
  PRIMARY KEY (`game_id`,`discussion_id`),
  KEY `fk_match_discussions_discussion` (`discussion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mod_notifications`
--

CREATE TABLE IF NOT EXISTS `mod_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` int(1) NOT NULL,
  `recipient_id` int(11) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notification_recipient_manager` (`recipient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=101 ;

-- --------------------------------------------------------

--
-- Table structure for table `mod_payments_history`
--

CREATE TABLE IF NOT EXISTS `mod_payments_history` (
  `manager_id` int(11) NOT NULL,
  `transaction_no` varchar(50) NOT NULL,
  `payment_provider` varchar(10) NOT NULL DEFAULT 'default',
  `date_completed` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`manager_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mod_pm_threads`
--

CREATE TABLE IF NOT EXISTS `mod_pm_threads` (
  `discussion_id` int(11) NOT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `deleted_by_sender` tinyint(1) NOT NULL,
  `deleted_by_recipient` tinyint(1) NOT NULL,
  `read_by_sender` tinyint(1) NOT NULL,
  `read_by_recipient` tinyint(1) NOT NULL,
  `system` tinyint(1) NOT NULL,
  PRIMARY KEY (`discussion_id`),
  KEY `fk_pm_threads_sender_manager` (`sender_id`),
  KEY `fk_pm_threads_recipient_manager` (`recipient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mod_sms_mobile`
--

CREATE TABLE IF NOT EXISTS `mod_sms_mobile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manager_id` int(11) NOT NULL,
  `mobile` varchar(16) DEFAULT NULL,
  `activation_code` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_mobile_manager` (`manager_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `newsletter`
--

CREATE TABLE IF NOT EXISTS `newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `is_auto` tinyint(1) NOT NULL DEFAULT '0',
  `manager_group_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `last_sent` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_newsletter_manager_group` (`manager_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `player`
--

CREATE TABLE IF NOT EXISTS `player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `shortname` varchar(17) NOT NULL,
  `basic_position_id` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `list_date` timestamp NULL DEFAULT NULL,
  `delist_date` timestamp NULL DEFAULT NULL,
  `bank_shares` int(11) DEFAULT NULL,
  `current_value` decimal(9,2) DEFAULT NULL,
  `initial_value` decimal(9,2) DEFAULT NULL,
  `vmi_season` decimal(7,2) DEFAULT NULL,
  `total_points` int(11) DEFAULT NULL,
  `is_injured` tinyint(1) DEFAULT NULL,
  `is_suspended` tinyint(1) DEFAULT NULL,
  `owned_perc` varchar(45) DEFAULT NULL,
  `jersey_no` int(3) DEFAULT NULL,
  `xml_id` int(5) DEFAULT NULL,
  `average_rating` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `xml_id_UNIQUE` (`xml_id`),
  KEY `fk_players_team` (`team_id`),
  KEY `fk_players_basic_position` (`basic_position_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2031 ;

-- --------------------------------------------------------

--
-- Table structure for table `player_daily_history`
--

CREATE TABLE IF NOT EXISTS `player_daily_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) NOT NULL,
  `value` decimal(9,2) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bought` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_player_history_player` (`player_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=58611 ;

-- --------------------------------------------------------

--
-- Table structure for table `player_point_history`
--

CREATE TABLE IF NOT EXISTS `player_point_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `player_id` int(11) NOT NULL,
  `points` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_player_gameweek_history_gameweek` (`date`),
  KEY `fk_player_gameweek_history_player` (`player_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16664 ;

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE IF NOT EXISTS `position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `abbreviation` varchar(3) DEFAULT NULL,
  `basic_position_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_position_basic_position` (`basic_position_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=39 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manager_id` int(11) NOT NULL,
  `gender` enum('M','F') DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `photo` varchar(45) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `line` varchar(45) DEFAULT NULL,
  `photo_approved` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix1` (`id`),
  UNIQUE KEY `ix2` (`manager_id`),
  KEY `fk_country_id` (`country_id`),
  KEY `fk_team_id` (`team_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=578 ;

-- --------------------------------------------------------

--
-- Table structure for table `rank`
--

CREATE TABLE IF NOT EXISTS `rank` (
  `rank` int(11) NOT NULL,
  `manager_id` int(11) NOT NULL,
  PRIMARY KEY (`manager_id`),
  KEY `fk_rank_manager` (`manager_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE IF NOT EXISTS `report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manager_id` int(11) DEFAULT NULL,
  `reporter_id` int(11) DEFAULT NULL,
  `report_reason_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `comment` text,
  `report_action_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_report_manager` (`manager_id`),
  KEY `fk_report_reporter` (`reporter_id`),
  KEY `fk_report_status_change_user` (`report_reason_id`),
  KEY `fk_report_action` (`report_action_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=83 ;

-- --------------------------------------------------------

--
-- Table structure for table `report_action`
--

CREATE TABLE IF NOT EXISTS `report_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `report_reason`
--

CREATE TABLE IF NOT EXISTS `report_reason` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reason` varchar(100) NOT NULL,
  `language_id` int(11) NOT NULL,
  `reason_type` int(11) NOT NULL DEFAULT '0',
  `display_order` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `report_reason_language` (`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

-- --------------------------------------------------------

--
-- Table structure for table `report_reason_action`
--

CREATE TABLE IF NOT EXISTS `report_reason_action` (
  `report_reason_id` int(11) NOT NULL,
  `report_action_id` int(11) NOT NULL,
  PRIMARY KEY (`report_reason_id`,`report_action_id`),
  KEY `report_reason_action_action` (`report_action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `script_configuration`
--

CREATE TABLE IF NOT EXISTS `script_configuration` (
  `main_script_interval` int(1) DEFAULT '6',
  `daily_script_time` varchar(5) DEFAULT '00:00',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `last_daily_run` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `newsletter_time` varchar(5) NOT NULL DEFAULT '0:00',
  `active_options` varchar(10) NOT NULL DEFAULT '5,10',
  `burned_options` varchar(10) NOT NULL DEFAULT '2,1',
  `formula_coefficient` float NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;


-- --------------------------------------------------------

--
-- Table structure for table `static_content`
--

CREATE TABLE IF NOT EXISTS `static_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section` varchar(50) NOT NULL,
  `content` text,
  `lang` int(11) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_static_content_language` (`lang`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;


-- --------------------------------------------------------

--
-- Table structure for table `statistics`
--

CREATE TABLE IF NOT EXISTS `statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL DEFAULT '0',
  `top` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) NOT NULL,
  `coefficient` decimal(4,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `status`
--



-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `shortname15` varchar(15) NOT NULL,
  `shortname5` varchar(5) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  `jersey_small` varchar(45) DEFAULT NULL,
  `jersey_large` varchar(45) DEFAULT NULL,
  `xml_id` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `xml_id_UNIQUE` (`xml_id`),
  KEY `fk_teams_country` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=64 ;

-- --------------------------------------------------------

--
-- Table structure for table `timezone`
--

CREATE TABLE IF NOT EXISTS `timezone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=462 ;

--
-- Dumping data for table `timezone`
--



-- --------------------------------------------------------

--
-- Table structure for table `tips`
--

CREATE TABLE IF NOT EXISTS `tips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tip_group` int(1) NOT NULL,
  `tip` varchar(150) NOT NULL,
  `lang` int(11) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_tips_language` (`lang`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manager_id` int(11) NOT NULL,
  `date_completed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `player_id` int(11) NOT NULL,
  `shares` int(11) NOT NULL,
  `price_share` decimal(9,2) NOT NULL,
  `type_of_transaction` enum('buy','sell') NOT NULL,
  `coefficient` decimal(3,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_transactions_manager` (`manager_id`),
  KEY `fk_transaction_player` (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `fk_users_role` (`role`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user`
--



-- --------------------------------------------------------

--
-- Table structure for table `watchlist`
--

CREATE TABLE IF NOT EXISTS `watchlist` (
  `manager_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  PRIMARY KEY (`manager_id`,`player_id`),
  KEY `watchlist_player` (`player_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Constraints for dumped tables
--

--
-- Constraints for table `announcement`
--
ALTER TABLE `announcement`
  ADD CONSTRAINT `fk_announcement_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ban`
--
ALTER TABLE `ban`
  ADD CONSTRAINT `fk_ban_manager` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ban_report_reason` FOREIGN KEY (`report_reason_id`) REFERENCES `report_reason` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `blocked_manager`
--
ALTER TABLE `blocked_manager`
  ADD CONSTRAINT `fk_blocked_manager_blocked_manager` FOREIGN KEY (`blocked_manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_blocked_manager_manager` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `fk_chat_game` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_chat_manager` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `deleted_manager`
--
ALTER TABLE `deleted_manager`
  ADD CONSTRAINT `deleted_manager_report_reason` FOREIGN KEY (`report_reason_id`) REFERENCES `report_reason` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `fk_events_basic_position` FOREIGN KEY (`basic_position_id`) REFERENCES `basic_position` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `game`
--
ALTER TABLE `game`
  ADD CONSTRAINT `fk_games_away` FOREIGN KEY (`team_away`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_games_home` FOREIGN KEY (`team_home`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `gameconfiguration`
--
ALTER TABLE `gameconfiguration`
  ADD CONSTRAINT `fk_timezone` FOREIGN KEY (`timezone_id`) REFERENCES `timezone` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `game_player`
--
ALTER TABLE `game_player`
  ADD CONSTRAINT `fk_game_players_event` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_game_players_game` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_game_players_player` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `manager`
--
ALTER TABLE `manager`
  ADD CONSTRAINT `fk_managers_manager_team` FOREIGN KEY (`manager_team_id`) REFERENCES `manager_team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_manager_timezone` FOREIGN KEY (`timezone_id`) REFERENCES `timezone` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `manager_daily_history`
--
ALTER TABLE `manager_daily_history`
  ADD CONSTRAINT `fk_manager_daily_history_manager` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `manager_match_vote`
--
ALTER TABLE `manager_match_vote`
  ADD CONSTRAINT `manager_match_vote_game` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `manager_match_vote_manager` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `manager_password_reset`
--
ALTER TABLE `manager_password_reset`
  ADD CONSTRAINT `fk_pass_reset_manager_id` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `manager_player_rating`
--
ALTER TABLE `manager_player_rating`
  ADD CONSTRAINT `manager_player_rating_manager` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `manager_player_rating_player` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `manager_point_history`
--
ALTER TABLE `manager_point_history`
  ADD CONSTRAINT `fk_manager_gameweek_history_manager` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `manager_task`
--
ALTER TABLE `manager_task`
  ADD CONSTRAINT `fk_manager_actions_action_desc` FOREIGN KEY (`task_id`) REFERENCES `manager_task_desc` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_manager_actions_manager` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `manager_team`
--
ALTER TABLE `manager_team`
  ADD CONSTRAINT `fk_manager_teams_formation` FOREIGN KEY (`formation_id`) REFERENCES `formation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `manager_team_player`
--
ALTER TABLE `manager_team_player`
  ADD CONSTRAINT `fk_manager_team_players_manager_team` FOREIGN KEY (`manager_team_id`) REFERENCES `manager_team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_manager_team_players_player` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_manager_team_players_position` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_manager_team_players_status` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `manager_team_player_history`
--
ALTER TABLE `manager_team_player_history`
  ADD CONSTRAINT `fk_manager_team_player_history_manager` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_manager_team_player_history_player` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_manager_team_player_history_position` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_manager_team_player_history_status` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mod_credits_manager`
--
ALTER TABLE `mod_credits_manager`
  ADD CONSTRAINT `mod_credits_manager_manager` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mod_custom_leagues_discussions`
--
ALTER TABLE `mod_custom_leagues_discussions`
  ADD CONSTRAINT `fk_custom_league_discussion_discussion` FOREIGN KEY (`discussion_id`) REFERENCES `mod_forum_discussions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_custom_league_discussion_league` FOREIGN KEY (`league_id`) REFERENCES `mod_custom_leagues` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mod_custom_leagues_fan`
--
ALTER TABLE `mod_custom_leagues_fan`
  ADD CONSTRAINT `fk_custom_league_fan_league` FOREIGN KEY (`league_id`) REFERENCES `mod_custom_leagues` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_custom_league_fan_team` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mod_custom_leagues_invitations`
--
ALTER TABLE `mod_custom_leagues_invitations`
  ADD CONSTRAINT `fk_custom_league_notifications_league` FOREIGN KEY (`league_id`) REFERENCES `mod_custom_leagues` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_custom_league_notifications_notification` FOREIGN KEY (`notification_id`) REFERENCES `mod_notifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mod_custom_leagues_managers`
--
ALTER TABLE `mod_custom_leagues_managers`
  ADD CONSTRAINT `fk_leagues_league_league` FOREIGN KEY (`league_id`) REFERENCES `mod_custom_leagues` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_leagues_manager_manager` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mod_forum_categories_text`
--
ALTER TABLE `mod_forum_categories_text`
  ADD CONSTRAINT `fk_forum_category_category_text` FOREIGN KEY (`category_id`) REFERENCES `mod_forum_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mod_forum_category_discussions`
--
ALTER TABLE `mod_forum_category_discussions`
  ADD CONSTRAINT `fk_categories_discussions_category` FOREIGN KEY (`category_id`) REFERENCES `mod_forum_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_categories_discussions_discussion` FOREIGN KEY (`discussion_id`) REFERENCES `mod_forum_discussions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mod_forum_comments`
--
ALTER TABLE `mod_forum_comments`
  ADD CONSTRAINT `fk_forum_comment_commentor` FOREIGN KEY (`commentor_id`) REFERENCES `manager` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_forum_comment_discussion` FOREIGN KEY (`discussion_id`) REFERENCES `mod_forum_discussions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mod_forum_comments_reported`
--
ALTER TABLE `mod_forum_comments_reported`
  ADD CONSTRAINT `fk_mod_forum_comments_reported_comment` FOREIGN KEY (`comment_id`) REFERENCES `mod_forum_comments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_mod_forum_comments_reported_report` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mod_match_discussions`
--
ALTER TABLE `mod_match_discussions`
  ADD CONSTRAINT `fk_match_discussions_discussion` FOREIGN KEY (`discussion_id`) REFERENCES `mod_forum_discussions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_match_discussions_match` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mod_notifications`
--
ALTER TABLE `mod_notifications`
  ADD CONSTRAINT `fk_notification_recipient_manager` FOREIGN KEY (`recipient_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mod_pm_threads`
--
ALTER TABLE `mod_pm_threads`
  ADD CONSTRAINT `fk_pm_threads_discussion` FOREIGN KEY (`discussion_id`) REFERENCES `mod_forum_discussions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pm_threads_recipient_manager` FOREIGN KEY (`recipient_id`) REFERENCES `manager` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_pm_threads_sender_manager` FOREIGN KEY (`sender_id`) REFERENCES `manager` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `mod_sms_mobile`
--
ALTER TABLE `mod_sms_mobile`
  ADD CONSTRAINT `fk_mobile_manager` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `newsletter`
--
ALTER TABLE `newsletter`
  ADD CONSTRAINT `fk_newsletter_manager_group` FOREIGN KEY (`manager_group_id`) REFERENCES `manager_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `player`
--
ALTER TABLE `player`
  ADD CONSTRAINT `fk_players_basic_position` FOREIGN KEY (`basic_position_id`) REFERENCES `basic_position` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_players_team` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `player_daily_history`
--
ALTER TABLE `player_daily_history`
  ADD CONSTRAINT `fk_player_history_player` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `player_point_history`
--
ALTER TABLE `player_point_history`
  ADD CONSTRAINT `fk_player_gameweek_history_player` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `position`
--
ALTER TABLE `position`
  ADD CONSTRAINT `fk_position_basic_position` FOREIGN KEY (`basic_position_id`) REFERENCES `basic_position` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_country_id` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_manager_id` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_team_id` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `rank`
--
ALTER TABLE `rank`
  ADD CONSTRAINT `fk_rank_manager` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `report`
--
ALTER TABLE `report`
  ADD CONSTRAINT `fk_report_action` FOREIGN KEY (`report_action_id`) REFERENCES `report_action` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_report_manager` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_report_reason` FOREIGN KEY (`report_reason_id`) REFERENCES `report_reason` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_report_reporter` FOREIGN KEY (`reporter_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `report_reason`
--
ALTER TABLE `report_reason`
  ADD CONSTRAINT `report_reason_language` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `report_reason_action`
--
ALTER TABLE `report_reason_action`
  ADD CONSTRAINT `report_reason_action_action` FOREIGN KEY (`report_action_id`) REFERENCES `report_action` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `report_reason_action_reason` FOREIGN KEY (`report_reason_id`) REFERENCES `report_reason` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `static_content`
--
ALTER TABLE `static_content`
  ADD CONSTRAINT `fk_static_content_language` FOREIGN KEY (`lang`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `team`
--
ALTER TABLE `team`
  ADD CONSTRAINT `fk_teams_country` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tips`
--
ALTER TABLE `tips`
  ADD CONSTRAINT `fk_tips_language` FOREIGN KEY (`lang`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `fk_transactions_manager` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_transaction_player` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `watchlist`
--
ALTER TABLE `watchlist`
  ADD CONSTRAINT `watchlist_manager` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `watchlist_player` FOREIGN KEY (`player_id`) REFERENCES `player` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
