��    
      l      �       �      �        #   !     E     `  5   s  #   �  A   �  5     �  E     '  %   =     c     �     �  ^   �  Q     0   j  >   �           	                
                  defaultBuy price defaultConversation deleted. defaultConversation with {manager} defaultMessages from {gn} defaultSell price defaultYou can adjust your team again on {openTime}. defaultYou have joined the league. defaultYou have successfully {type} {shares} shares of {player}. defaultYou have {players} players on your watchlist. Project-Id-Version: MSLeague
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-12-19 14:20-0000
PO-Revision-Date: 2011-12-20 13:29-0000
Last-Translator: subdee <subdee.studio@gmail.com>
Language-Team: dev <dev@msleague.com>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: _t
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: utf-8
X-Poedit-SearchPath-0: /var/www/msleague
 Τιμή αγοράς Η συζήτηση διεγράφη. Συζήτηση με {manager} Μηνύματα  από {gn} Τιμή πώλησης Μπορείς να κάνεις αλλαγές στην ομάδα σου στις {openTime} Η ομάδα σου συμμετέχει πλέον στο πρωτάθλημα.  {type} {shares} μετοχές του {player}. Έχεις {players} παίκτες στην Λίστα σου 