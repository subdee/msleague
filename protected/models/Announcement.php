<?php

/**
 * This is the model class for table "announcement".
 *
 * The followings are the available columns in table 'announcement':
 * @property integer $id
 * @property string $announcement
 * @property string $date_entered
 * @property integer $user_id
 *
 * The followings are the available model relations:
 * @property User $user
 */
class Announcement extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return Announcement the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'announcement';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('content, date_entered, user_id, title', 'required'),
            array('user_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, content, date_entered, user_id, title', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'content' => 'Announcement',
            'date_entered' => 'Date Entered',
            'user_id' => 'User',
            'title' => 'Title'
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->date_entered =
                Yii::app()->localtime->fromLocalDateTime(
                    $this->date_entered, 'Y-m-d H:i:s');
            return true;
        }
        else
            return false;
    }

    protected function afterFind() {
        $this->date_entered =
            Yii::app()->localtime->toLocalDateTime(
                $this->date_entered, 'Y-m-d H:i:s');

        return (parent::afterFind());
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('date_entered', $this->date_entered, true);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('title', $this->title, true);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }

    public function getLatest() {
        return $this->find('date_entered = (select max(date_entered) from announcement)');
    }

}