<?php

/**
 * This is the model class for table "ban".
 *
 * The followings are the available columns in table 'ban':
 * @property integer $id
 * @property integer $manager_id
 * @property string $reason
 * @property integer $duration
 *
 * The followings are the available model relations:
 * @property Manager $manager
 */
class Ban extends CActiveRecord {

    public $username;

    /**
     * Returns the static model of the specified AR class.
     * @return Ban the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'ban';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('manager_id,report_reason_id,duration', 'required'),
            array('manager_id', 'numerical', 'integerOnly' => true),
            array('duration', 'numerical', 'integerOnly' => true, 'min' => 1, 'max' => 365, 'tooSmall' => 'You cannot ban someone for less than a day!', 'tooBig' => 'You cannot ban someone for longer than a year'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, manager_id, report_reason_id, duration', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'manager' => array(self::BELONGS_TO, 'Manager', 'manager_id'),
            'reportReason' => array(self::BELONGS_TO, 'ReportReason', 'report_reason_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'manager_id' => 'Manager',
            'reason' => 'Reason',
            'duration' => 'Duration',
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->date_banned =
                Yii::app()->localtime->fromLocalDateTime(
                    $this->date_banned, 'Y-m-d H:i:s');
            return true;
        }
        else
            return false;
    }

    protected function afterFind() {
        $this->date_banned =
            Yii::app()->localtime->toLocalDateTime(
                $this->date_banned, 'Y-m-d H:i:s');

        return (parent::afterFind());
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('manager_id', $this->manager_id);
        $criteria->compare('report_reason_id', $this->report_reason_id, true);
        $criteria->compare('duration', $this->duration);

        $criteria->addCondition('DATE(NOW()) BETWEEN date(date_banned) AND date(DATE_ADD(date_banned, INTERVAL duration DAY))');
        $criteria->join = 'LEFT JOIN manager ON manager.id = t.manager_id';

        $sort = new CSort;
        $sort->attributes = array(
            'manager.username'
        );
        $sort->defaultOrder = 'manager.username asc';

        $data = new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 50
            ),
            'sort' => $sort
            ));

        return $data;
    }
    
    /**
     * @return string The time until which the manager is banned.
     */
    public function unbanTime() {
        return Utils::date(date('Y-m-d H:i:s', strtotime($this->date_banned) + 60 * 60 * 24 * $this->duration), array('time' => false));
    }

}