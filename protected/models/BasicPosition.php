<?php

/**
 * This is the model class for table "basic_position".
 *
 * The followings are the available columns in table 'basic_position':
 * @property integer $id
 * @property string $name
 * @property string $abbreviation
 * @property integer $min_per_manager
 *
 * The followings are the available model relations:
 * @property Event[] $events
 * @property Player[] $players
 * @property Position[] $positions
 */
class BasicPosition extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return BasicPosition the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'basic_position';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, abbreviation', 'required'),
            array('min_per_manager', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 45),
            array('abbreviation', 'length', 'max' => 3),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, abbreviation, min_per_manager', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'events' => array(self::HAS_MANY, 'Event', 'basic_position_id'),
            'players' => array(self::HAS_MANY, 'Player', 'basic_position_id'),
            'positions' => array(self::HAS_MANY, 'Position', 'basic_position_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => _t('Pos'),
            'abbreviation' => _t('Pos'),
            'min_per_manager' => 'Min Per Manager',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('abbreviation', $this->abbreviation, true);
        $criteria->compare('min_per_manager', $this->min_per_manager);

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
            ));
    }

    /**
     * Returns each position limits associated to position abbreviation
     * @return array
     */
    public function getPosLimits() {
        $positions = BasicPosition::model()->findAll();

        $limits = array();
        foreach ($positions as $p) {
            $limits[$p->abbreviation] = $p->min_per_manager;
        }
        return $limits;
    }

    /**
     * Returns the nationalised string for the name field.
     * @return string
     */
    public function getTranslatedName($params = array()) {
        return BasicPosition::translated($this->name, $params);
    }

    /**
     *
     * @param array $params
     * @return <type> 
     */
    public function getTranslatedAbbr() {
        return BasicPosition::translated($this->abbreviation);
    }

    /**
     * Returns the translated position in the correct tense.
     * @param string $position
     * @param array $params
     * @return string
     */
    public static function translated($position, $params = array()) {

        if (isset($params['plural']) && $params['plural']) {
            if (isset($params['ablative']) && $params['ablative']) {
                switch ($position) {
                    case 'Goalkeeper': return _t('of the goalkeepers');
                    case 'Defender': return _t('of the defenders');
                    case 'Midfielder': return _t('of the midfielders');
                    case 'Striker': return _t('of the strikers');
                }
            } elseif (isset($params['accusative']) && $params['accusative']) {
                switch ($position) {
                    case 'Goalkeeper': return _t('goalkeepers');
                    case 'Defender': return _t('defenders');
                    case 'Midfielder': return _t('midfielders');
                    case 'Striker': return _t('strikers');
                }
            } else {
                switch ($position) {
                    case 'Goalkeeper': return _t('goalkeepers');
                    case 'Defender': return _t('defenders');
                    case 'Midfielder': return _t('midfielders');
                    case 'Striker': return _t('strikers');
                }
            }
        } else {
            if (isset($params['ablative']) && $params['ablative']) {
                switch ($position) {
                    case 'Goalkeeper': return _t('of the goalkeeper');
                    case 'Defender': return _t('of the defender');
                    case 'Midfielder': return _t('of the midfielder');
                    case 'Striker': return _t('of the striker');
                }
            } elseif (isset($params['accusative']) && $params['accusative']) {
                switch ($position) {
                    case 'Goalkeeper': return _t('a goalkeeper');
                    case 'Defender': return _t('a defender');
                    case 'Midfielder': return _t('a midfielder');
                    case 'Striker': return _t('a striker');
                }
            }
        }

        switch ($position) {
            case 'Goalkeeper': return _t('goalkeeper');
            case 'Defender': return _t('defender');
            case 'Midfielder': return _t('midfielder');
            case 'Striker': return _t('striker');
            case 'GK': return _t('GK');
            case 'DE': return _t('DE');
            case 'MF': return _t('MF');
            case 'ST': return _t('ST');
        }
        return '';
    }

}