<?php

/**
 * This is the model class for table "blocked_manager".
 *
 * The followings are the available columns in table 'blocked_manager':
 * @property integer $id
 * @property integer $manager_id
 * @property string $blocked_manager_id
 *
 * The followings are the available model relations:
 * @property Manager $manager
 */
class BlockedManager extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return BlockedManager the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'blocked_manager';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('manager_id', 'numerical', 'integerOnly'=>true),
			array('blocked_manager_id', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, manager_id, blocked_manager_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'manager' => array(self::BELONGS_TO, 'Manager', 'manager_id'),
			'blocked' => array(self::BELONGS_TO, 'Manager', 'blocked_manager_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'manager_id' => 'Manager',
			'blocked_manager_id' => 'Blocked Manager',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('manager_id',$this->manager_id);
		$criteria->compare('blocked_manager_id',$this->blocked_manager_id,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}