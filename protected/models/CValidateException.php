<?php

class CValidateException extends CException {

    public function __construct($ar, $log=true, $code=0) {
        parent::__construct(Debug::arObjectErrors($ar), $code);
        if ($log) {
            Debug::logToWindow($ar->getErrors(), 'validation', false);
        }
    }

}

?>
