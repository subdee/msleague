<?php

/**
 * This is the model class for table "chatbox".
 *
 * The followings are the available columns in table 'chatbox':
 * @property integer $id
 * @property integer $manager_id
 * @property string $shout
 * @property string $date_entered
 *
 * The followings are the available model relations:
 * @property Manager $manager
 */
class Chat extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return Chatbox the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'chat';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('manager_id, shout', 'required'),
            array('manager_id', 'numerical', 'integerOnly' => true),
            array('shout', 'length', 'max' => 160, 'tooLong' => _t('Your message can contain up to {max} characters')),
            // The following rule is used by search().
// Please remove those attributes that should not be searched.
            array('id, manager_id, shout, date_entered', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'manager' => array(self::BELONGS_TO, 'Manager', 'manager_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'manager_id' => 'Manager',
            'shout' => 'Shout',
            'date_entered' => 'Date Entered',
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->date_entered =
                Yii::app()->localtime->fromLocalDateTime(
                    $this->date_entered, 'Y-m-d H:i:s');
            return true;
        }
        else
            return false;
    }

    protected function afterFind() {
        $this->date_entered =
            Yii::app()->localtime->toLocalDateTime(
                $this->date_entered, 'Y-m-d H:i:s');

        return (parent::afterFind());
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
// Warning: Please modify the following code to remove attributes that
// should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('manager_id', $this->manager_id);
        $criteria->compare('shout', $this->shout, true);
        $criteria->compare('date_entered', $this->date_entered, true);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }

    public function getLastShouts($num) {
        $criteria = new CDbCriteria;
        $criteria->condition = 'game_id IS NULL and date_entered > :date';
        $criteria->limit = $num;
        $criteria->order = 'date_entered';
        $criteria->params = array(':date' => _app()->session['lastChatUpdate']);
        $criteria->with = 'manager';

        $data = $this->findAll($criteria);

        _app()->session['lastChatUpdate'] = _app()->localtime->getUTCNow('Y-m-d H:i:s');

        return $data;
    }

    public function getShouts($num) {
        $dep = new CDbCacheDependency('SELECT COUNT(id) FROM chat');
        $criteria = new CDbCriteria;
        $criteria->condition = 'game_id IS NULL AND t.id IN (select * from (select t3.id from chat as t3 order by date_entered desc limit :num) as t2)';
        $criteria->params = array(':num' => $num);

        $data = $this->cache(10000, $dep)->findAll($criteria);

        _app()->session['lastChatUpdate'] = _app()->localtime->getUTCNow('Y-m-d H:i:s');

        return $data;
    }

}