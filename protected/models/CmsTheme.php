<?php

/**
 * This is the model class for table "cms_theme".
 *
 * The followings are the available columns in table 'cms_theme':
 * @property string $id
 * @property string $rule
 * @property string $type
 * @property string $value
 * @property string $name
 */
class CmsTheme extends CActiveRecord {

    public $type = 'image';
    public $image;
    public $imageRealPath;
    public $color;

    /**
     * Returns the static model of the specified AR class.
     * @return CmsTheme the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cms_theme';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('rule, type, name', 'required'),
            array('type, name', 'length', 'max' => 20),
            array('value', 'length', 'max' => 60),
            // The following rule is used by search().
// Please remove those attributes that should not be searched.
            array('color', 'safe'),
            array('id, rule, type, value, name', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'rule' => 'Rule',
            'type' => 'Type',
            'value' => 'Value',
            'name' => 'Name',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
// Warning: Please modify the following code to remove attributes that
// should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('rule', $this->rule, true);
//        $criteria->compare('type', $this->type, true);
        $criteria->compare('value', $this->value, true);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'sort' => false,
            ));
    }

    public function ruleTypes() {
        return array(
            'image' => 'image',
            'color' => 'color',
        );
    }

    public function afterFind() {
        parent::afterFind();
        if ($this->type == 'image') {
            $this->imageRealPath = _bp("../images/cms/{$this->value}");
        } elseif ($this->type == 'color') {
            $this->color = $this->value;
        }
    }

    public function afterSave() {
        parent::afterSave();
        // If it is not of type image, delete possible left overs
        if ($this->type != 'image') {
            $this->removeImage();
        }
    }

    public function afterDelete() {
        parent::afterDelete();
        if ($this->type == 'image') {
            $this->removeImage();
        }
    }

    public function removeImage() {
        if (file_exists($this->imageRealPath)) {
            unlink($this->imageRealPath);
        }
    }

    /**
     *
     * @return type 
     */
    public function edit() {
        if (isset($_POST['CmsTheme'])) {
            $this->attributes = $_POST['CmsTheme'];
            try {
                $this->name = str_replace(' ', '_', strtolower($this->name));

                $callback = 'editType' . $this->type;
                if ($this->$callback())
                    return true;
                else
                    throw new CException(print_r($this->getErrors(), true));
            } catch (CDbException $e) {
                _app()->user->setFlash('error', $e->getMessage());
            } catch (CException $e) {
                _app()->user->setFlash('error', $e->getMessage());
            }
        }
        return false;
    }

    /**
     *
     * @return type 
     */
    public function editTypeImage() {
        $this->image = CUploadedFile::getInstance($this, 'image');
        if ($this->image) {

            $this->removeImage();

            $ext = $this->image->getExtensionName();
            $this->value = "{$this->name}.{$ext}";
        }

        if ($this->save()) {
            if ($this->image) {
                $this->imageRealPath = _bp("../images/cms/{$this->value}");
                if (!$this->image->SaveAs($this->imageRealPath)) {
                    throw new CException('Failed to upload image');
                }
            }
            return true;
        }
        return false;
    }

    /**
     *
     * @return type 
     */
    public function editTypeColor() {
        $this->value = $this->color;
        if ($this->save()) {
            return true;
        }
        return false;
    }

    /**
     * 
     */
    public function display() {
        if ($this->type == 'image') {
            $value = $this->value != '' ? "url('../images/cms/{$this->value}');" : 'none;';
        } elseif ($this->type == 'color') {
            $value = $this->value != '' ? "#{$this->value};" : 'default;';
        } else
            $value = $this->value;

        return str_replace("{{value}}", $value, $this->rule);
    }

}

?>
