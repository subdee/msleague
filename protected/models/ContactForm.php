<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel {

    public $email;
    public $subject;
    public $body;
    public $verifyCode;

    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            // email, subject and body are required
            array('email, subject, body', 'required'),
            // email has to be a valid email address
            array('email', 'email'),
            // verifyCode needs to be entered correctly
            array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels() {
        return array(
            'email' => _t('Email'),
            'subject' => _t('Subject'),
            'body' => _t('Message'),
            'verifyCode' => _t('Verification Code'),
        );
    }

    public function send() {
        if (_is_logged_in()) {
            $body = _t("From user {user}:\n\n", array('{user}' => _manager()->username)) . $this->body;
        } else {
            $body = $this->body;
        }

        if (Utils::sendEmail(_support(), $this->subject, $body, $this->email)) {
            $this->email = null;
            $this->subject = null;
            $this->body = null;
            $this->verifyCode = null;
            return true;
        }

        $this->verifyCode = null;
        return false;
    }

}