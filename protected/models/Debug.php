<?php

class Debug {

    /**
     *
     * @param type $session
     * @param type $msg
     * @param type $type
     * @return type
     */
    public static function notice($session, $msg, $type='nosign') {
        if (!self::isDebug())
            return;
        Utils::notice($session, $msg, $type);
    }

    /**
     *
     * @param type $key
     * @param type $data
     * @param type $type
     * @param type $error_log
     * @return type
     */
    public static function logToWindowKey($key, $data, $type='debug', $error_log=true) {

        if ($error_log) {
            $log = $data;
            if (is_array($log) || is_object($log)) {
                $log = serialize($log);
            }

            error_log($log);
        }

        if (!self::isDebug() || Utils::isAjax())
            return;

        if ($key === null) {
            $_SESSION['msl_DebugWindowData'][$type][] = $data;
        } else {
            $_SESSION['msl_DebugWindowData'][$type][$key] = $data;
        }
    }

    /**
     *
     * @param type $data
     * @param type $type
     * @param type $error_log
     * @return type
     */
    public static function logToWindow($data, $type='debug', $error_log=true) {
        self::logToWindowKey(null, $data, $type, $error_log);
    }

    /**
     *
     * @param type $ar
     * @return string
     */
    public static function arObjectErrors($ar) {
        $errors = $ar->getErrors();
        $errorCount = count($errors);
        $counter = 0;
        $out = '';
        foreach ($errors as $msg) {
            $out .= $msg[0];
            if ($counter < $errorCount - 1) {
                $out .= "\n";
            }
            ++$counter;
        }
        return $out;
    }

    /**
     *
     * @param type $string
     */
    public static function setSessionError($string) {
        $_SESSION['msl_arLastError'] = $string;
    }

    /**
     *
     * @param type $obj
     */
    public static function setSessionObjectError($obj) {
        self::saveSessionError(self::arObjectErrors($obj));
    }

    /**
     *
     * @return type
     */
    public static function getSessionError() {
        return isset($_SESSION['msl_arLastError']) ? $_SESSION['msl_arLastError'] : '';
    }

    /**
     *
     * @param type $e
     * @param type $custom
     * @return type
     */
    public static function exceptionDetailString($e, $custom='') {
        return $e->getFile() . '(' . $e->getLine() . ") $custom:" . $e->getMessage();
    }

    /**
     *
     * @param type $ar
     */
    public static function logArObjectErrors($ar) {
        error_log(self::arObjectErrors($ar));
    }

    /**
     *
     * @param type $e
     * @param type $custom
     */
    public static function logExceptionDetailString($e, $custom='') {
        error_log(self::exceptionDetailString($e, $custom));
    }

    /**
     *
     * @return type
     */
    public static function isDebug() {
        return isset(_app()->params['debugStatus']) && _app()->params['debugStatus'] == true;
    }

}

?>