<?php

/**
 * This is the model class for table "deleted_manager".
 *
 * The followings are the available columns in table 'deleted_manager':
 * @property integer $id
 * @property integer $reason
 * @property string $details
 * @property string $date
 * @property string $username
 * @property string $email
 * @property string $team_name
 * @property string $registration_date
 * @property integer $total_points
 * @property string $portfolio_cash
 * @property string $portfolio_share_value
 * @property string $gender
 * @property string $birthdate
 * @property string $location
 * @property integer $country_id
 * @property string $fullname
 *
 * The followings are the available model relations:
 * @property ReportReason $ReportReason
 */
class DeletedManager extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return DeletedManager the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'deleted_manager';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('country_id', 'numerical', 'integerOnly' => true),
            array('username', 'length', 'max' => 16),
            array('fullname', 'length', 'max' => 50),
            array('email, team_name, location', 'length', 'max' => 45),
            array('portfolio_cash, portfolio_share_value', 'length', 'max' => 9),
            array('total_points', 'length', 'max' => 8),
            array('gender', 'length', 'max' => 1),
            array('details, date, registration_date, birthdate', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, reason, details, date, username, email, team_name, registration_date, total_points, portfolio_cash, portfolio_share_value, gender, birthdate, location, country_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'reportReason' => array(self::BELONGS_TO, 'ReportReason', 'report_reason_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'reportReason' => 'Reason',
            'details' => 'Details',
            'date' => 'Date',
            'username' => 'Username',
            'email' => 'Email',
            'team_name' => 'Team Name',
            'registration_date' => 'Registration Date',
            'total_points' => 'Total Points',
            'portfolio_cash' => 'Portfolio Cash',
            'portfolio_share_value' => 'Portfolio Share Value',
            'gender' => 'Gender',
            'birthdate' => 'Birthdate',
            'location' => 'Location',
            'country_id' => 'Country',
            'fullname' => 'Fullname',
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->date =
                Yii::app()->localtime->fromLocalDateTime(
                $this->date, 'Y-m-d H:i:s');
            return true;
        }
        else
            return false;
    }

    protected function afterFind() {
        $this->registration_date =
            Yii::app()->localtime->toLocalDateTime(
            $this->registration_date, 'Y-m-d H:i:s');
        $this->date =
            Yii::app()->localtime->toLocalDateTime(
            $this->date, 'Y-m-d H:i:s');

        if (Utils::isAdminUser())
            $this->username = CHtml::link($this->username, _url('admin/manager/view', array('id' => $this->id, 'deleted' => true)));

        return (parent::afterFind());
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        
        $criteria->compare('username', $this->username, true);

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 50
                ),
//                        'sort'=>$sort
            ));
    }

    public function delete($id) {
        $dbTransaction = _app()->db->beginTransaction();
        try {
            $manager = Manager::model()->findByPk($id);
            // Save manager data to deleted managers table
            $deleted = new DeletedManager;
            $deleted->report_reason_id = 19;
            $deleted->details = 'Deleted by ' . User::model()->findByPk(_user()->id)->username;
            $deleted->date = _app()->localtime->getLocalNow('Y-m-d H:i:s');
            $deleted->team_name = $manager->managerTeam->name;

            $mgrattrs = array('username', 'registration_date', 'portfolio_cash', 'portfolio_share_value', 'total_points', 'email');
            foreach ($mgrattrs as $ma) {
                $deleted->$ma = $manager->$ma;
            }
            $profattrbs = array('gender', 'birthdate', 'location', 'country_id');
            foreach ($profattrbs as $prof) {
                $deleted->$prof = $manager->profile->$prof;
            }

            if (!$deleted->save())
                throw new Exception(_get_errors_string($deleted));

            $team = $manager->managerTeam;
            if (!$team->delete())
                throw new Exception(_get_errors_string($team));

            $dbTransaction->commit();
            return true;
        } catch (CDBException $e) {
            $dbTransaction->rollback();
            return false;
            _user()->setFlash('failure', $e->getMessage());
        } catch (Exception $e) {
            $dbTransaction->rollback();
            _user()->setFlash('failure', $e->getMessage());
            return false;
        }
    }

    public function getCleanUsername() {
        return strip_tags($this->username);
    }

}