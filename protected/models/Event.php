<?php

/**
 * This is the model class for table "event".
 *
 * The followings are the available columns in table 'event':
 * @property integer $id
 * @property string $name
 * @property integer $basic_position_id
 * @property integer $points_awarded
 * @property boolean $is_team_event
 * 
 * The followings are the available model relations:
 * @property BasicPosition $basicPosition
 * @property GamePlayer[] $gamePlayers
 */
class Event extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return Event the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'event';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, basic_position_id, points_awarded', 'required'),
            array('basic_position_id, points_awarded', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 45),
            array('is_team_event', 'boolean'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, basic_position_id, points_awarded, is_team_event', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'basicPosition' => array(self::BELONGS_TO, 'BasicPosition', 'basic_position_id'),
            'gamePlayers' => array(self::HAS_MANY, 'GamePlayer', 'event_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'basic_position_id' => 'Basic Position',
            'points_awarded' => 'Points Awarded',
            'is_team_event' => 'Is Team Event',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('basic_position_id', $this->basic_position_id);
        $criteria->compare('points_awarded', $this->points_awarded);
        $criteria->compare('is_team_event', $this->is_team_event);

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
            ));
    }

    public static function translated($event) {
        switch ($event) {
            case 'Assists': return _t('For each assist made');
            case 'Clean Sheet': return _t('If his team didn\'t concede any goals (clean sheet)*');
            case 'Goals Against': return _t('For each goal his team conceded (goals against)*');
            case 'Goals Scored': return _t('For each goal scored');
            case 'Minutes Played': return _t('Played for >= 60 minutes');
            case 'Minutes Played Less': return _t('Played for < 60 minutes');
            case 'No Goals': return _t('If his team didn\'t score (no score) *');
            case 'Own Goals': return _t('For each own goal scored');
            case 'Penalties Saved': return _t('For each penalty saved');
            case 'Red Cards': return _t('If he received a red card');
            case 'Missed Penalties': return _t('For each penalty missed');
        }
        return '';
    }

    public static function image($event, $size, $showTitle=true) {

        switch ($event) {
            case 'Assists': $title = _t('Assist');
                break;
            case 'Goals Scored': $title = _t('Goal');
                break;
            case 'Own Goals': $title = _t('Own Goal');
                break;
            case 'Penalties Saved': $title = _t('Saved Penalty');
                break;
            case 'Red Cards': $title = _t('Red Card');
                break;
            case 'Missed Penalties': $title = _t('Missed Penalty');
                break;
            case 'Minutes Played': $title = _t('Minutes Played');
                break;
        }

        $event = strtolower($event);
        $event = str_replace(' ', '_', $event);

        return CHtml::image(Utils::imageUrl("events/{$event}{$size}.png"), $title, array(
                'title' => $showTitle ? $title : ''
            ));
    }
    
    public function getPoints($event,$position,$status='Starter'){
        $points = $this->find(array(
            'with' => 'basicPosition',
            'condition' => 't.name = :event AND basicPosition.name = :position',
            'params' => array(
                ':event' => $event,
                ':position' => $position
            )
        ));
        $coefficient = Status::model()->find('status = :status',array(':status'=>$status))->coefficient;
        
        if ($points != null)
            return $points->points_awarded*$coefficient;
        return false;
    }
    
    /**
     * Get all the event types that are not team events
     * @return array An array of Event objects
     */
    public function getEventTypes() {
        return $this->findAll(array(
            'select' => 'id,name',
            'group' => 'name',
            'order' => 'name asc',
            'condition' => 'name not in ("Clean Sheet","No Goals","Goals Against")'));
    }

    /**
     * Get the ID of an event depending on name and position
     * @param string $name
     * @param integer $pos
     * @return integer
     */
    public function getByName($name, $pos) {
//            return $this->find(array('condition'=>'name = :name and basic_position_id = :pos','params'=>array(':name'=>$name,':pos'=>$pos)));
        $conn = _app()->db;

        $comm = $conn->createCommand("
                select id from event where name = :name and basic_position_id = :pos
            ");

        $comm->bindParam(':name', $name);
        $comm->bindParam(':pos', $pos);

        $data = $comm->queryRow();

        return $data['id'];
    }

}