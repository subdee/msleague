<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Football
 *
 * @author subdee
 */
class Football implements Sport{

        private $formations;
        private $pointSystem;

        function __construct() {
        }

        public function changeFormation($formation,$id = null) {
        }

        public function calculateStandings($pointSystem){
                if (!is_string($pointSystem)){
                        return false;;
                }

                $points = explode(',',$pointSystem);
                $win = (int)$points[0];
                $draw = (int)$points[1];
                $loss = (int)$points[2];

                $team = array();
                $games = Game::model()->findAll();

                if (!$games){
                    $team = $this->setZerosToTeams();
                }else{
                    foreach($games as $game){
                            if ($game->score_home != null && $game->score_away != null){
                                    if (!isset($team[$game->teamHome0->shortname15]))
                                            $team[$game->teamHome0->shortname15] = array('p'=>0,'hw'=>0,'hgf'=>0,'agf'=>0,'hga'=>0,'aga'=>0,'hl'=>0,'ga'=>0,'hd'=>0,'g'=>0,'aw'=>0,'al'=>0,'ad'=>0);
                                    if (!isset($team[$game->teamAway0->shortname15]))
                                            $team[$game->teamAway0->shortname15] = array('p'=>0,'aw'=>0,'hgf'=>0,'agf'=>0,'hga'=>0,'aga'=>0,'al'=>0,'ga'=>0,'ad'=>0,'g'=>0,'hw'=>0,'hl'=>0,'hd'=>0);
                                    $team[$game->teamHome0->shortname15]['g']++ ;
                                    $team[$game->teamAway0->shortname15]['g']++ ;
                                    $team[$game->teamHome0->shortname15]['hgf'] += $game->score_home;
                                    $team[$game->teamHome0->shortname15]['hga'] += $game->score_away;
                                    $team[$game->teamAway0->shortname15]['agf'] += $game->score_away;
                                    $team[$game->teamAway0->shortname15]['aga'] += $game->score_home;
                                    if ($game->score_home > $game->score_away){
                                            $team[$game->teamHome0->shortname15]['p'] += $win;
                                            $team[$game->teamHome0->shortname15]['hw']++;
                                            $team[$game->teamAway0->shortname15]['p'] += $loss;
                                            $team[$game->teamAway0->shortname15]['al']++;
                                    } elseif ($game->score_home < $game->score_away){
                                            $team[$game->teamHome0->shortname15]['p'] += $loss;
                                            $team[$game->teamHome0->shortname15]['hl']++;
                                            $team[$game->teamAway0->shortname15]['p'] += $win;
                                            $team[$game->teamAway0->shortname15]['aw']++;
                                    } else {
                                            $team[$game->teamHome0->shortname15]['p'] += $draw;
                                            $team[$game->teamHome0->shortname15]['hd']++;
                                            $team[$game->teamAway0->shortname15]['p'] += $draw;
                                            $team[$game->teamAway0->shortname15]['ad']++;
                                    }
                            }else{
//                                $tempA = Football::setZerosToTeams($game->teamHome0->id);
//                                $tempH = Football::setZerosToTeams($game->teamAway0->id);
//                                $team = array_merge($team,$tempA);
//                                $team = array_merge($team,$tempH);
                            }
                    }
                }

                if ($team){
                    foreach ($team as $name=>$prop){
                            $p[$name] = $prop['p'];
                            $g[$name] = $name;
                    }
                    if (array_multisort($p, SORT_NUMERIC, SORT_DESC,
                                                            $g, SORT_STRING, SORT_ASC,
                                                                    $team))
                    {
                            $i = 1;
                            $prev = 0;
                            foreach ($team as $name=>$prop){
                                    if ($prop['p'] == $prev)
                                            $index = '-';
                                    else
                                            $index = $i;
                                    $list[$i]['index'] = $index;
                                    $list[$i]['name'] = $name;
                                    $list[$i]['props'] = $prop;
                                    $prev = $prop['p'];
                                    $i++;
                            }
                    }
                }else{
                    $list = null;
                }

                return $list;
        }

        public function calculatePoints($players,$gameweek) {
            $all = array();

            foreach ($players as $player) {
                $all[$player['id']]['name'] = $player['name'];
                $all[$player['id']]['team'] = $player['team'];
                $all[$player['id']]['position'] = $player['position'];
                $all[$player['id']]['status'] = $player['status'];
                $min = 0;
                
                $plStats = Manager::model()->getPlayerStatistics($player['id'],$gameweek['id']);
                foreach ($plStats as $plStat) {
                    if (!isset($all[$plStat['id']]['total'])) {
                        $all[$plStat['id']]['total'] = 0;
                    }
                    switch ($plStat['event']) {
                        case 'Minutes Played':
                            $min = $all[$plStat['id']]['min'] = $plStat['value'];
                            break;
                        case 'Goals Scored':
                            $all[$plStat['id']]['gs'] = $plStat['value'];
                            break;
                        case 'Assists':
                            $all[$plStat['id']]['as'] = $plStat['value'];
                            break;
                        case 'Penalties Saved':
                            $all[$plStat['id']]['ps'] = $plStat['value'];
                            break;
                        case 'Red Cards':
                            $all[$plStat['id']]['rc'] = $plStat['value'];
                            break;
                        case 'Own Goals':
                            $all[$plStat['id']]['og'] = $plStat['value'];
                            break;
                        case 'Missed Penalties':
                            $all[$plStat['id']]['mp'] = $plStat['value'];
                            break;
                        case 'No Goals':
                            if ($plStat['p_aw'] == 0)
                                $all[$plStat['id']]['ng'] = 0;
                            else
                                $all[$plStat['id']]['ng'] = $plStat['value'];
                            break;
                        case 'Clean Sheet':
                            if ($plStat['p_aw'] == 0)
                                $all[$plStat['id']]['ng'] = 0;
                            else
                                $all[$plStat['id']]['cs'] = $plStat['value'];
                            break;
                        case 'Goals Against':
                            if ($plStat['p_aw'] == 0)
                                $all[$plStat['id']]['ng'] = 0;
                            else
                                $all[$plStat['id']]['ga'] = $plStat['value'];
                            break;
                    }
                }

                if ($min == 0) {
                    $all[$player['id']]['min'] = $all[$player['id']]['gs'] = $all[$player['id']]['as'] =
                     $all[$player['id']]['ps'] = $all[$player['id']]['yc'] = $all[$player['id']]['rc'] =
                     $all[$player['id']]['og'] = $all[$player['id']]['mp'] = $all[$player['id']]['ng'] =
                     $all[$player['id']]['cs'] = $all[$player['id']]['ga'] = $all[$player['id']]['total'] = '-';
                }

                $all[$player['id']]['shares'] = $player['shares'];
                $plPts = PlayerGameweekHistory::model()->getInfo($player['id'],$gameweek['id']);
                if ($plPts != null)
                    $all[$player['id']]['total'] = $plPts->points*$player['co'];
            }

            return $all;
        }

        private function setZerosToTeams($teams = null){
            if ($teams == null)
                $teams = Team::model()->findAll();
            else
                $teams = Team::model()->findAll('id = :id',array(':id'=>$teams));
            
            $team = array();

            foreach ($teams as $t){
                $team[$t->shortname15] = array('p'=>0,'hw'=>0,'hgf'=>0,'agf'=>0,'hga'=>0,'aga'=>0,'hl'=>0,'ga'=>0,'hd'=>0,'g'=>0,'aw'=>0,'al'=>0,'ad'=>0);
            }

            return $team;
        }

        public function getFormations() {
                return $this->formations;
        }

        public function setFormations($formations) {
                $this->formations = $formations;
        }

        public function getPointSystem() {
                return $this->pointSystem;
        }

        public function setPointSystem($pointSystem) {
                $this->pointSystem = $pointSystem;
        }

}
?>
