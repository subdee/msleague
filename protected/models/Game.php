<?php

/**
 * This is the model class for table "game".
 *
 * The followings are the available columns in table 'game':
 * @property integer $id
 * @property integer $team_home
 * @property integer $team_away
 * @property integer $score_home
 * @property integer $score_away
 * @property integer $gameweek_id
 * @property string $date_played
 *
 * The followings are the available model relations:
 * @property Team $teamAway0
 * @property Gameweek $gameweek
 * @property Team $teamHome0
 * @property GamePlayer[] $gamePlayers
 */
class Game extends CActiveRecord {

    /**
     * Used in Player -> Game relationship. Total points earned at that game.
     * @var int
     */
    public $points;

    /**
     * Used in Player -> Game relationship. Whether a player was playing
     * with the home or away team at that game.
     * @var boolean
     */
    public $is_home_team;

    /**
     * Used in Player -> Game relationship.
     * @var int
     */
    public $player_id;

    /**
     * Used in Player -> Game relationship.
     * @var int
     */
    public $player_team_id;

    /**
     * The closing time for the day depending on game time
     * @var datetime
     */
    public $close;

    /**
     *  Month for searching games
     * @var int
     */
    public $dateplayed;

    /**
     * Returns the static model of the specified AR class.
     * @return Game the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'game';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('team_home, team_away', 'required'),
            array('team_home, team_away, score_home, score_away', 'numerical', 'integerOnly' => true),
            array('date_played', 'notPast'),
            array('date_played, id', 'safe'),
            array('team_home', 'sameTeam'),
            array('xml_id', 'unique'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, team_home, team_away, score_home, score_away, date_played', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'teamAway0' => array(self::BELONGS_TO, 'Team', 'team_away'),
            'teamHome0' => array(self::BELONGS_TO, 'Team', 'team_home'),
            'gamePlayers' => array(self::HAS_MANY, 'GamePlayer', 'game_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'team_home' => _t('Home'),
            'team_away' => _t('Away'),
            'score_home' => _t('Score Home'),
            'score_away' => _t('Score Away'),
            'date_played' => _t('Time'),
        );
    }

    /**
     * @return type
     */
    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->date_played = Utils::fromLocalDatetime($this->date_played);
            return true;
        }
        else
            return false;
    }

    /**
     * @return type
     */
    protected function afterFind() {
        parent::afterFind();
        $this->date_played = Utils::toLocalDatetime($this->date_played);
        //Different format
        $this->dateplayed = Yii::app()->localtime->toGameDateTime($this->dateplayed, 'Y-m-d');
    }

    /**
     *
     */
    protected function beforeDelete() {
        if (parent::beforeDelete()) {
            Utils::triggerEvent('onBeforeMatchDelete', $this);
            return true;
        }
        return false;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('score_home', $this->score_home);
        $criteria->compare('score_away', $this->score_away);
//        $criteria->compare('date_played', $this->date_played, true);

        $criteria->limit = Gameconfiguration::model()->get('matches_front_page');

        if ($this->scenario == 'searchByTeam') {
            $criteria->compare('team_home', $this->team_home);
            $criteria->compare('team_away', $this->team_away, false, 'OR');
        }

        if ($this->date_played != null) {
            $criteria->addCondition('date_played between convert_tz(:date,:tz,"UTC") and convert_tz(:date,:tz,"UTC") + interval 1 day');
            $criteria->params = array(':date' => $this->date_played, ':tz' => _timezone());
        }

        $criteria->order = 'date_played asc';

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'pagination' => false
            ));
    }

    public function searchIndexFuture() {
        $criteria = new CDbCriteria;

        $criteria->limit = Gameconfiguration::model()->get('matches_front_page');

        $criteria->addCondition('date_played = (select min(date_played) from game where date_played > now())');

        $criteria->order = 'date_played desc';

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
            ));
    }

    public function searchIndexPast() {
        $criteria = new CDbCriteria;

        $criteria->limit = Gameconfiguration::model()->get('matches_front_page');

        $criteria->addCondition('date_played = (select max(date_played) from game where date_played < now())');

        $criteria->order = 'date_played desc';

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
            ));
    }

    public function searchPastMatchesByPlayerId($playerId, $limit = 5) {
        $criteria = new CDbCriteria;
        $criteria->select = <<<SQL
            p.id as player_id,
            t.*,
            game_player.is_home_team
SQL;

        $criteria->join = <<<SQL
            left join (game_player
                left join player p
                    on p.id = game_player.player_id)
               on t.id = game_player.game_id

SQL;

        $criteria->addCondition('p.id = :id');
        $criteria->addCondition('game_player.player_id = :id');
        $criteria->addCondition('t.date_played < now()');

        $criteria->params[':id'] = $playerId;
        $criteria->order = 't.date_played desc';
        $criteria->group = 't.id';
        $criteria->limit = $limit;

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'pagination' => false
            ));
    }

    public function searchFutureMatchesByPlayerId($playerId, $limit = 5) {
        $criteria = new CDbCriteria;
        $criteria->select = <<<SQL
            p.team_id as player_team_id,
            t.*
SQL;

        $criteria->join = <<<SQL
            left join player p
                   on (p.team_id = t.team_home or p.team_id = t.team_away)
SQL;

        $criteria->addCondition('p.id = :id');
        $criteria->addCondition('t.date_played > now()');

        $criteria->params[':id'] = $playerId;
        $criteria->order = 't.date_played asc';
        $criteria->limit = $limit;

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'pagination' => false
            ));
    }

    /**
     * Get all games for current or next matchday
     * @return Game
     */
    public function getNextMatchday() {
        $games = Game::model()->findAll(array(
            'condition' => 'date(convert_tz(date_played,"UTC",:tz)) = date(:date)',
            'params' => array(':date' => _app()->localtime->getGameNow('Y-m-d'), ':tz' => _timezone()),
            'order' => 'date_played asc'
            ));
        if (count($games) == 0) {
            $games = Game::model()->findAll(array(
                'condition' => 'date(convert_tz(date_played,"UTC",:tz)) =
                        date(convert_tz((select min(date_played) from game where date(convert_tz(date_played,"UTC",:tz)) > date(:date)),"UTC","America/Argentina/Buenos_Aires"))',
                'params' => array(
                    ':date' => _app()->localtime->getGameNow('Y-m-d H:i:s'),
                    ':tz' => _timezone()
                ),
                'order' => 'date_played asc'
                ));
        }
        return $games;
    }

    public static function hasStarted(Game $game) {
        if (strtotime($game->date_played) > strtotime(_app()->localtime->getLocalNow('Y-m-d H:i:s')))
            return false;
        return true;
    }
    
    /**
     * A validator that checks whether the same team has been submitted in a form
     * @param string $attribute
     * @param array $params
     * @return boolean
     */
    public function sameTeam($attribute, $params) {
        if ($this->team_home == $this->team_away) {
            $this->addError('team_home', 'Cannot have same team on both home and away');
            return false;
        }
        return true;
    }

    public function notPast($attribute, $params) {
        if (strtotime($this->date_played)-(60*15) < strtotime(_app()->localtime->getLocalNow('Y-m-d H:i:s'))) {
            $this->addError('date_played', 'You cannot set a date/time in the past.<br />Time must be at least 15 minutes from now.');
            return false;
        }
        return true;
    }

}