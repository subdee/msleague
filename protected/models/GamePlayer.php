<?php

/**
 * This is the model class for table "game_player".
 *
 * The followings are the available columns in table 'game_player':
 * @property integer $id
 * @property integer $game_id
 * @property integer $player_id
 * @property integer $event_id
 * @property integer $event_value
 *
 * The followings are the available model relations:
 * @property Event $event
 * @property Game $game
 * @property Player $player
 */
class GamePlayer extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return GamePlayer the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'game_player';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('game_id, player_id, event_id', 'required'),
            array('game_id, player_id, event_id, event_value', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, game_id, player_id, event_id, event_value', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'event' => array(self::BELONGS_TO, 'Event', 'event_id'),
            'game' => array(self::BELONGS_TO, 'Game', 'game_id'),
            'player' => array(self::BELONGS_TO, 'Player', 'player_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'game_id' => 'Game',
            'player_id' => 'Player',
            'event_id' => 'Event',
            'event_value' => 'Event Value',
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->entered =
                Yii::app()->localtime->fromLocalDateTime(
                $this->entered, 'Y-m-d H:i:s');
            return true;
        }
        else
            return false;
    }

    protected function afterFind() {
        $this->entered =
            Yii::app()->localtime->toLocalDateTime(
            $this->entered, 'Y-m-d H:i:s');

        return (parent::afterFind());
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('game_id', $this->game_id);
        $criteria->compare('player_id', $this->player_id);
        $criteria->compare('event_id', $this->event_id);
        $criteria->compare('event_value', $this->event_value);

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
            ));
    }

    public function getPlayersTeam($plId, $date) {
        $pl = $this->model()->find(array(
            'condition' => 'player_id = :plId and date(convert_tz(game.date_played,"UTC",:tz)) = date(:date)',
            'params' => array(
                ':plId' => $plId,
                ':date' => $date,
                ':tz' => _timezone()
            ),
            'with' => 'game'
            ));

        if ($pl != null) {
            $game = Game::model()->find('id = :gwId', array(':gwId' => $pl->game->id));
            $home = $away = '';
            if ($pl->is_home_team) {
                $home = 'active';
            } else {
                $away = 'active';
            }

            $label = CHtml::tag('span', array('class' => $home), $game->teamHome0->shortname5);
            $label.= '-';
            $label.= CHtml::tag('span', array('class' => $away), $game->teamAway0->shortname5);
            return CHtml::link($label, _url('match/match', array('id' => $game->id)));
        }
        return '-';
    }

}