<?php

/**
 * This is the model class for table "gameconfiguration".
 *
 * The followings are the available columns in table 'gameconfiguration':
 * @property string $game_name
 * @property string $support_email
 * @property integer $max_transactions_per_day
 * @property integer $overall_min_price_per_share
 * @property integer $overall_max_price_per_share
 * @property integer $total_shares_per_player
 * @property integer $manager_initial_budget
 * @property integer $min_number_of_portfolio_players
 * @property integer $max_number_of_portfolio_players
 * @property integer $max_players_same_team
 * @property integer $is_enabled
 * @property integer $portfolio_perc_rule
 * @property string $id
 * @property integer $limit_up
 * @property integer $max_reports_per_day
 * @property integer $max_number_of_registered_users
 * @property integer $max_invites_per_day
 */
class Gameconfiguration extends CActiveRecord {

    private $config = null;

    /**
     * Returns the static model of the specified AR class.
     * @return Gameconfiguration the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'gameconfiguration';
    }

    protected function beforeSave() {
        if (parent::beforeSave()){
            $this->disallowed_usernames = preg_replace('/\s+/', '', $this->disallowed_usernames);
            $this->disallowed_usernames = rtrim($this->disallowed_usernames, ',');
            $this->disallowed_usernames = ltrim($this->disallowed_usernames, ',');

            return true;
        } else {
            return false;
        }
    }

    protected function afterFind() {
        $this->limit_up /= 100;

        return (parent::afterFind());
    }
    
    protected function beforeValidate() {
        $this->limit_up = (int) ($this->limit_up * 100);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('max_transactions_per_day, overall_min_price_per_share, overall_max_price_per_share, total_shares_per_player, manager_initial_budget, min_number_of_portfolio_players, max_number_of_portfolio_players, max_players_same_team, is_enabled, portfolio_perc_rule, id, limit_up, max_reports_per_day, max_number_of_registered_users, max_invites_per_day,timezone_id,game_name,support_email,matches_front_page', 'required'),
            array('max_transactions_per_day, overall_min_price_per_share, overall_max_price_per_share, total_shares_per_player, manager_initial_budget, min_number_of_portfolio_players, max_number_of_portfolio_players, max_players_same_team, is_enabled, portfolio_perc_rule, limit_up, max_reports_per_day, max_number_of_registered_users, max_invites_per_day,timezone_id', 'numerical', 'integerOnly' => true),
            array('newsletter_footer,disallowed_usernames','length','max'=>1000),
            array('game_name', 'length', 'max' => 30),
            array('support_email,noreply_email', 'length', 'max' => 30),
            array('id', 'length', 'max' => 45),
            array('site_url,noreply_email,max_watchlist_items, is_season_open, is_registration_open','safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('game_name, support_email, max_transactions_per_day, overall_min_price_per_share, overall_max_price_per_share, total_shares_per_player, manager_initial_budget, min_number_of_portfolio_players, max_number_of_portfolio_players, max_players_same_team, is_enabled, portfolio_perc_rule, id, limit_up, max_reports_per_day, max_number_of_registered_users, max_invites_per_day,timezone_id,newsletter_footer,disallowed_usernames,site_url, is_season_open, is_registration_open', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'timezone' => array(self::BELONGS_TO, 'Timezone', 'timezone_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'game_name' => 'Game Name',
            'support_email' => 'Support Email',
            'max_transactions_per_day' => 'Max Transactions Per Day',
            'overall_min_price_per_share' => 'Overall Min Price Per Share',
            'overall_max_price_per_share' => 'Overall Max Price Per Share',
            'total_shares_per_player' => 'Total Shares Per Player',
            'manager_initial_budget' => 'Manager Initial Budget',
            'min_number_of_portfolio_players' => 'Min Number Of Portfolio Players',
            'max_number_of_portfolio_players' => 'Max Number Of Portfolio Players',
            'max_players_same_team' => 'Max Players Same Team',
            'is_enabled' => 'Is Enabled',
            'portfolio_perc_rule' => 'Portfolio Perc Rule',
            'id' => 'ID',
            'limit_up' => 'Limit Up',
            'max_reports_per_day' => 'Max Reports Per Day',
            'max_number_of_registered_users' => 'Max Number Of Registered Users',
            'max_invites_per_day' => 'Max Invites Per Day',
            'timezone_id' => 'Game Timezone',
            'disallowed_usernames'=>'Disallowed Usernames (comma-separated)'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('game_name', $this->game_name, true);
        $criteria->compare('support_email', $this->support_email, true);
        $criteria->compare('max_transactions_per_day', $this->max_transactions_per_day);
        $criteria->compare('overall_min_price_per_share', $this->overall_min_price_per_share);
        $criteria->compare('overall_max_price_per_share', $this->overall_max_price_per_share);
        $criteria->compare('total_shares_per_player', $this->total_shares_per_player);
        $criteria->compare('manager_initial_budget', $this->manager_initial_budget);
        $criteria->compare('min_number_of_portfolio_players', $this->min_number_of_portfolio_players);
        $criteria->compare('max_number_of_portfolio_players', $this->max_number_of_portfolio_players);
        $criteria->compare('max_players_same_team', $this->max_players_same_team);
        $criteria->compare('is_enabled', $this->is_enabled);
        $criteria->compare('portfolio_perc_rule', $this->portfolio_perc_rule);
        $criteria->compare('id', $this->id, true);
        $criteria->compare('limit_up', $this->limit_up);
        $criteria->compare('max_reports_per_day', $this->max_reports_per_day);
        $criteria->compare('max_number_of_registered_users', $this->max_number_of_registered_users);
        $criteria->compare('max_invites_per_day', $this->max_invites_per_day);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }

    public function isOpen() {
        if ($this->get('is_enabled')){
            if(Game::model()->find('now() > date_played - interval 15 minute and date(convert_tz(date_played,"UTC",:tz)) = date(convert_tz(now(),"UTC",:tz))',array(':tz'=>$this->get('timezone')->name)) == null) {
                if($this->getCloseTime()) {
                    if(!_manager()->isBanned()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function getCloseTime() {
        $game = Game::model()->find(array(
                'condition' => 'date_played = (select min(date_played) from game where date_played >= now())',
                'select' => '*,date_played - interval 15 minute as close'
            ));

        return $game != null ? _app()->localtime->toLocalDateTime($game->close,'Y-m-d H:i:s') : 0;
    }

    public function get($key) {
        if (null == $this->config) {
            $this->config = $this->find();
        }
        return $this->config->$key;
    }

    public function getAll($keys) {
        if (null == $this->config) {
            $this->config = $this->find();
        }
        foreach ($keys as $key)
            $data[$key] = $this->config->$key;

        return $data;
    }

    public function getTimezone() {
        if (null == $this->config) {
            $this->config = $this->find();
        }
        return $this->config->timezone->name;
    }

    /**
     * @return float Returns the transaction commission fee factor.
     */
    public function getCommissionFactor() {
        return 1 + ($this->get('commission_perc') * 0.01);
    }

    public function getIsSeasonOpen() {
        return $this->get('is_season_open');
    }
    
    public function getConfig() {
        return $this->config;
    }

}
