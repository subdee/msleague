<?php

/**
 * This is the model class for table "invitation".
 *
 * The followings are the available columns in table 'invitation':
 * @property integer $id
 * @property integer $manager_id
 * @property string $email
 * @property string $date_sent
 * @property string $date_activated
 */
class Invitation extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return Invitation the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'invitation';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('manager_id, email, date_sent', 'required'),
            array('manager_id', 'numerical', 'integerOnly' => true),
            array('email', 'length', 'max' => 45),
            array('date_activated', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, manager_id, email, date_sent, date_activated', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'manager_id' => 'Manager',
            'email' => 'Email',
            'date_sent' => 'Date Sent',
            'date_activated' => 'Date Activated',
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->date_sent =
                Yii::app()->localtime->fromLocalDateTime(
                    $this->date_sent, 'Y-m-d H:i:s');
            $this->date_activated =
                Yii::app()->localtime->fromLocalDateTime(
                    $this->date_activated, 'Y-m-d H:i:s');
            return true;
        }
        else
            return false;
    }

    protected function afterFind() {
        $this->date_sent =
            Yii::app()->localtime->toLocalDateTime(
                $this->date_sent, 'Y-m-d H:i:s');
        $this->date_activated =
            Yii::app()->localtime->toLocalDateTime(
                $this->date_activated, 'Y-m-d H:i:s');

        return (parent::afterFind());
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('manager_id', $this->manager_id);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('date_sent', $this->date_sent, true);
        $criteria->compare('date_activated', $this->date_activated, true);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Creates a new invitation and returns the object on success.
     * @param string $email
     * @return Invitation
     */
    public static function createNew($email) {
        $invitation = new Invitation;
        $invitation->manager_id = _manager()->id;
        $invitation->email = $email;
        $invitation->date_sent = _app()->localtime->getLocalNow();
        if (!$invitation->save()) {
            Debug::setSessionObjectError($invitation);
            return null;
        }

        return $invitation;
    }

    /**
     *
     * @return bool
     */
    public function activate() {
        $this->date_activated = _app()->localtime->getLocalNow();
        if (!$this->save()) {
            Debug::setSessionObjectError($this);
            return false;
        }
        return true;
    }

}