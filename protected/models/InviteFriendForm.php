<?php

/**
 * InviteFriendForm class.
 */
class InviteFriendForm extends CFormModel {

    public $email;
    public $message;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            // required fields
            array('email', 'required', 'message' => _t("Your friend's email address is not valid.")),
            // emails need to be checked
            array('email', 'email', 'message' => _t("Your friend's email address is not valid.")),
            array('email', 'unique', 'className' => 'Manager'),
            array('email', 'unique', 'className' => 'Invitation',
                'criteria' => array(
                    'condition' => 'manager_id = :mid',
                    'params' => array(':mid' => _manager()->id)
            ), 'message' => _t("You have already invited a user with this email address.")),
            array('email, message', 'safe'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'email' => _t('Your friend\'s email'),
            'message' => _t('Your Message'),
        );
    }

    public function invite() {
        $invitation = Invitation::model()->createNew($this->email);
        if (!$invitation) {
            Utils::notice('invite', Debug::getSessionError(), 'error');
            return false;
        }

        $registration_code = _manager()->createActivationCode(array($invitation->id));
        $registration_link = _app()->createAbsoluteUrl('/user/register', array('invitation' => $invitation->id, 'code' => $registration_code));

        $tpl = _controller()->renderPartial('_invitationEmail', array(
                'username' => _manager()->username,
                'message' => $this->message,
                'registration_link' => $registration_link,
                ), true);
        
        $subject = _t('{username} invited you to join {gameName}',array(
            '{username}' => _manager()->username,
            '{gameName}' => _gameName()
        ));

        if (Utils::sendEmail($this->email, $subject, $tpl)) {
            Utils::notice('invite', _t("An invitation has been sent to {email}.", array('{email}' => $_POST['InviteFriendForm']['email'])), 'success');
        } else {
            Utils::notice('invite', _t('Failed to send invitation email. Please contact us at {support}', array('{support}' => _support())), 'error');
        }
        Debug::notice('failedEmail', 'DEBUG: Email content:<br /><br />' . strip_tags($tpl, '<a>'), 'warning');

        $this->email = null;
        $this->message = null;
        return true;
    }

}
