<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel {

    public $username;
    public $password;
//	public $rememberMe;
    private $_identity;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            // username and password are required
            array('username, password', 'required'),
            // rememberMe needs to be a boolean
//			array('rememberMe', 'boolean'),
            // password needs to be authenticated
//            array('password', 'authenticate'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'username' => _t('Username'),
            'password' => _t('Password'),
            'rememberMe' => _t('Remember Me'),
        );
    }

    /**
     * Authenticates the password.
     * This is the 'authenticate' validator as declared in rules().
     */
//    public function authenticate($attribute, $params) {
//        if (!$this->hasErrors()) {
//            $this->_identity = new UserIdentity($this->username, $this->password);
//            if (!$this->_identity->authenticate())
//                $this->addError($attribute, "Incorrect $attribute");
//        }
//    }

    public function login($admin = false) {
        if (!$this->_identity) {
            $this->_identity = new UserIdentity($this->username, $this->password, $admin);
            if (!$this->_identity->authenticate())
                $this->addError('password', "Incorrect username or password");
        }

        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $duration = 0;
            _user()->login($this->_identity, $duration);
            if (!$admin) {
                $ip = $this->getIpAddress();
                _manager()->last_known_ip = $ip ? $ip : '127.0.0.1';
                _manager()->update();
            }
            return true;
        }
        return false;
    }

    private function getIpAddress() {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
//                    _debugWindowData($ip);
                    if (filter_var($ip, FILTER_VALIDATE_IP) !== false && $ip != '127.0.0.1') {
                        return $ip;
                    }
                }
            }
        }
    }

}
