<?php

/**
 * This is the model class for table "manager".
 *
 * The followings are the available columns in table 'manager':
 * @property integer $id
 * @property integer $role
 * @property string $last_login
 * @property string $email
 * @property string $username
 * @property string $password
 * @property integer $manager_team_id
 * @property string $total_points
 * @property string $portfolio_share_value
 * @property string $portfolio_cash
 * @property string $registration_date
 * @property string $notes
 * @property string $last_known_ip
 * @property boolean $newsletter
 * @property integer $timezone_id
 * @property string $fullname
 * @property boolean $accepted_terms
 *
 * The followings are the available model relations:
 * @property ManagerTeam $managerTeam
 * @property ManagerDailyHistory[] $managerDailyHistories
 * @property ManagerGameweekHistory[] $managerGameweekHistories
 * @property Transaction[] $transactions
 * @property Ban $ban
 * @property Report[] $reports
 * @property Report[] $reporters
 * @property Profile $profile
 * @property Chat[] $chat
 * @property Timezone $timezone
 * @property Rank $rank
 * @property ManagerTask[] $tasks
 * @property ManagerPasswordReset $passwordReset
 */
class Manager extends CActiveRecord {

    /**
     *
     * @var string Used when registering/changing passwords
     */
    public $password_repeat;

    /**
     *
     * @var string Used when registering/changing emails
     */
    public $email_repeat;
    public $chng;
    public $value;

    /**
     * Returns the static model of the specified AR class.
     * @return Manager the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'manager';
    }

    /**
     * @return boolean
     */
    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->registration_date = Utils::fromLocalDatetime($this->registration_date);
            $this->activation_date = Utils::fromLocalDatetime($this->activation_date);
            $this->last_activity = Utils::fromLocalDatetime($this->last_activity);
            $this->last_login = Utils::fromLocalDatetime($this->last_login);
            $this->username = strip_tags($this->username);
            return true;
        }
        return false;
    }

    /**
     *
     */
    protected function beforeValidate() {
        if (parent::beforeValidate()) {
            $this->portfolio_cash = round(($this->portfolio_cash * 100), 2) * 0.01;
            $this->portfolio_share_value = round(($this->portfolio_share_value * 100), 2) * 0.01;

            $this->username = strip_tags($this->username);

            return true;
        }

        return false;
    }

    /**
     *
     */
    protected function afterFind() {
        parent::afterFind();
        $this->registration_date = Utils::toLocalDatetime($this->registration_date);
        $this->activation_date = Utils::toLocalDatetime($this->activation_date);
        $this->last_activity = Utils::toLocalDatetime($this->last_activity);
        $this->last_login = Utils::toLocalDatetime($this->last_login);
        if (!Utils::isScript() && Utils::isAdminUser())
            $this->username = CHtml::link($this->username, _url('admin/manager/view', array('id' => $this->id)));
    }

    /**
     *
     */
    protected function beforeFind() {
        parent::beforeFind();
//        Utils::triggerEvent('onManagerBeforeFind', $this);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('role, registration_date', 'required'),
            array('role, manager_team_id, timezone_id', 'numerical', 'integerOnly' => true),
            array('last_known_ip', 'length', 'max' => 45),
            array('total_points, portfolio_share_value, portfolio_cash', 'length', 'max' => 9),
            array('role, registration_date, last_login, notes, email_code', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, role, last_login, manager_team_id, total_points, portfolio_share_value, portfolio_cash, registration_date, notes, last_known_ip, timezone_id', 'safe', 'on' => 'search'),
            array('fullname', 'length', 'max' => 50),
            // Username
            array('username', 'required'),
            array('username', 'unique', 'className' => 'Manager', 'message' => _t('The username you have selected is not available')),
            array('username', 'match', 'pattern' => '/^[a-zA-Z0-9_]{5,16}$/', 'message' => _t('Your username must be bewteen 5 and 16 characters (letters, numbers and underscores).')),
            array('username', 'isAllowed'),
            array('username', 'safe', 'on' => 'search'),
            // Password
            array('password, password_repeat', 'required'),
//            array('password', 'match', 'pattern' => '/^(?=^.{6,}$)((?=.*[A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z]))^.*$/', 'message' => _t('Your password must be at least 6 characters long. Try')),
            array('password', 'length', 'min' => 6, 'max' => 100, 'tooShort' => _t('Your password must be at least {min} characters long. Make your password strong by adding numbers, symbols and capital letters.')),
            array('password_repeat', 'compare', 'compareAttribute' => 'password', 'on' => 'register'),
            // Email
            array('email, email_repeat', 'required'),
            array('email', 'length', 'max' => 45),
            array('email', 'email', 'checkMX' => true, 'message' => _t('This email is not valid.')),
            array('email', 'unique', 'className' => 'Manager', 'message' => _t('The email address is being used')),
            array('email_repeat', 'compare', 'compareAttribute' => 'email', 'on' => 'register'),
            array('email', 'safe', 'on' => 'search'),
            // Newsletter
            array('newsletter, accepted_terms', 'boolean'),
            array('newsletter, accepted_terms', 'safe', 'on' => 'register'),
            // Fullname
            array('fullname', 'required', 'on' => 'register'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'managerTeam' => array(self::BELONGS_TO, 'ManagerTeam', 'manager_team_id'),
            'managerDailyHistories' => array(self::HAS_MANY, 'ManagerDailyHistory', 'manager_id'),
            'managerPointHistories' => array(self::HAS_MANY, 'ManagerPointHistory', 'manager_id'),
            'transactions' => array(self::HAS_MANY, 'Transaction', 'manager_id'),
            'ban' => array(self::HAS_ONE, 'Ban', 'manager_id'),
            'reports' => array(self::HAS_MANY, 'Report', 'manager_id'),
            'reporters' => array(self::HAS_MANY, 'Report', 'reporter_id'),
            'rank' => array(self::HAS_ONE, 'Rank', 'manager_id'),
            'profile' => array(self::HAS_ONE, 'Profile', 'manager_id'),
            'chat' => array(self::HAS_MANY, 'Chat', 'manager_id'),
            'timezone' => array(self::BELONGS_TO, 'Timezone', 'timezone_id'),
            'tasks' => array(self::HAS_MANY, 'ManagerTask', 'manager_id', 'order' => 'rank asc'),
            'passwordReset' => array(self::HAS_ONE, 'ManagerPasswordReset', 'manager_id'),
            'watchlists' => array(self::HAS_MANY, 'Watchlist', 'manager_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'role' => 'Role',
            'last_login' => 'Last Login',
            'email' => _t('Email'),
            'email_repeat' => _t('Confirm Email'),
            'username' => _t('Username'),
            'password' => _t('Password'),
            'password_repeat' => _t('Confirm Password'),
            'manager_team_id' => 'Manager Team',
            'total_points' => _t('Points'),
            'portfolio_share_value' => 'Portfolio Value',
            'portfolio_cash' => 'Portfolio Cash',
            'registration_date' => 'Registration Date',
            'notes' => 'Notes',
            'last_known_ip' => 'Last Known Ip',
            'rank' => _t('Rank'),
            'timezone_id' => _t('Timezone'),
            'fullname' => _t('Fullname'),
            'newsletter' => _t('Newsletter'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->select = 't.id as id, t.username as username, t.total_points as total_points, rank.rank as rank, t.manager_team_id, t.role, manager_team.id as mt_id, manager_team.name';
        $criteria->join = 'left join rank on rank.manager_id = t.id left join manager_team on t.manager_team_id = manager_team.id';
        $criteria->order = 'rank.rank asc, created_on desc';

        $criteria->compare('role', $this->role);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('manager_team.name', $this->username, true, 'OR');

        $criteria->addInCondition('role', array(Role::MANAGER, Role::RESET));

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 50
                ),
                'sort' => false
            ));
    }

/********************************U S E D   B Y   A D M I N*************************************************/
    /**
     * Returns an ADP with all managers
     * @return CActiveDataProvider
     */
    public function searchManagers() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('username', $this->username, true);

        $criteria->join = 'LEFT JOIN manager_team mt ON mt.id = t.manager_team_id';

        $criteria->addInCondition('role', array(Role::MANAGER));

        $sort = new CSort;
        $sort->attributes = array(
            'username',
            'email',
            'registration_date',
            'managerTeam.name' => array(
                'asc' => 'mt.name asc',
                'desc' => 'mt.name desc',
            )
        );
        $sort->defaultOrder = 'username asc';

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 50
                ),
                'sort' => $sort
            ));
    }

    /**
     * Returns an ADP with all managers' portfolio attributes
     * @return CActiveDataProvider
     */
    public function searchPortfolios() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('username', $this->username, true);

        $criteria->select = '*,t.portfolio_share_value+t.portfolio_cash as value';

        $sort = new CSort;
        $sort->attributes = array(
            'username',
            'total_points',
            'value' => array(
                'asc' => 't.portfolio_share_value+t.portfolio_cash asc',
                'desc' => 't.portfolio_share_value+t.portfolio_cash desc',
            ),
        );
        $sort->defaultOrder = 't.portfolio_share_value+t.portfolio_cash desc';

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 50
                ),
                'sort' => $sort
            ));
    }

    /**
     * Returns an ADP with all inactive managers
     * @return CActiveDataProvider
     */
    public function searchInactive() {

        $criteria = new CDbCriteria;

        $criteria->compare('username', $this->username, true);

        $criteria->addInCondition('role', array(Role::INACTIVE));

        if (isset($_GET['Manager']['username']))
            $criteria->addSearchCondition('username', $_GET['Manager']['username']);

        $sort = new CSort;
        $sort->attributes = array(
            'username',
        );
        $sort->defaultOrder = 'username asc';

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 50
                ),
                'sort' => $sort
            ));
    }

    /**
     * Returns an ADP with all inactive managers
     * @return CActiveDataProvider
     */
    public function searchReset() {

        $criteria = new CDbCriteria;

        $criteria->compare('username', $this->username, true);

        $criteria->addInCondition('role', array(Role::RESET));

        if (isset($_GET['Manager']['username']))
            $criteria->addSearchCondition('username', $_GET['Manager']['username']);

        $sort = new CSort;
        $sort->attributes = array(
            'username',
            'registration_date',
        );
        $sort->defaultOrder = 'username asc';

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 50
                ),
                'sort' => $sort
            ));
    }

    /**
     * Returns an ADP with all temporary managers
     * @return CActiveDataProvider
     */
    public function searchTemp() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('username', $this->username, true);

        $criteria->addInCondition('role', array(Role::TEMPORARY));

        $sort = new CSort;
        $sort->attributes = array(
            'username',
            'email',
            'registration_date',
            'activation_date',
        );
        $sort->defaultOrder = 'username asc';

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 50
                ),
                'sort' => $sort
            ));
    }

    public function searchBanned() {

        $criteria = new CDbCriteria;

        $criteria->compare('username', $this->username, true);

        $criteria->addCondition('t.id in (select manager_id from ban where DATE(NOW()) BETWEEN date_banned AND DATE_ADD(date_banned, INTERVAL duration DAY))');

        $sort = new CSort;
        $sort->attributes = array(
            'username'
        );
        $sort->defaultOrder = 'username asc';

        $data = new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 50
                ),
                'sort' => $sort
            ));

        return $data;
    }

    public function searchOnline() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->addCondition('convert_tz(last_activity,"UTC",:tz) BETWEEN DATE_SUB(:now,INTERVAL 10 MINUTE) AND :now');
        $criteria->params = array(':tz' => _timezone(), ':now' => _app()->localtime->getLocalNow('Y-m-d H:i:s'));

        $sort = new CSort;
        $sort->attributes = array(
            'username',
        );
        $sort->defaultOrder = 'username asc';

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 50
                ),
                'sort' => $sort
            ));
    }

    /**
     * Returns all active managers
     * @return array of Manager objects
     */
    public function getActiveManagers() {
        return $this->findAll(array('condition' => 'role = :role', 'order' => 'username asc', 'params' => array(':role' => Role::MANAGER)));
    }

    public static function changeAttribute($manager_id, $attribute, $value, $action='delete', $relation = false) {
        $manager = Manager::model()->findByPk($manager_id);
        if ($action == 'delete') {
            if ($relation)
                $manager->$relation->$attribute = null;
            else
                $manager->$attribute = null;
        } else {
            if ($relation)
                $manager->$relation->$attribute = $value;
            else
                $manager->$attribute = $value;
        }
        if ($relation) {
            if ($manager->$relation->save(true, array($attribute)))
                return true;
            else
                _user()->setFlash('failure', Debug::arObjectErrors($manager->$relation));
        }else {
            if ($manager->save(true, array($attribute)))
                return true;
            else
                _user()->setFlash('failure', Debug::arObjectErrors($manager));
        }
        return false;
    }

    public function getCleanUsername() {
        return strip_tags($this->username);
    }

/***************************** ^ U S E D   B Y   A D M I N ^ *************************************************/

    /**
     *
     * @param type $manager_id
     * @return type
     */
    public function getAvailableTransactions($manager_id=0) {

        $id = $this->_getUserContextId($manager_id);

        $connection = _app()->db;
        $command = $connection->createCommand('
                    select
                        (select max_transactions_per_day from gameconfiguration) -
                        (select count(t.id)
                           from transaction t,
                                   gameconfiguration gc
                         where date(convert_tz(t.date_completed,"UTC",:tz)) = date(:now)
                             and t.manager_id = :id
                    ) as maxtrpw');

        $command->bindValue(':id', $id);
        $command->bindValue(':tz', _timezone());
        $command->bindValue(':now', _app()->localtime->getGameNow('Y-m-d H:i:s'));

        $dataReader = $command->queryRow();

        return $dataReader['maxtrpw'];
    }

    /**
     *
     * @param <type> $password
     */
    public function setPassword($password) {
        $this->password = $this->encrypt($password);
    }

    /**
     *
     * @param <type> $password
     * @return <type>
     */
    public function encrypt($password) {
        return md5($password);
    }

    /**
     *
     * @param Player $player
     * @param bool $canMakeTransactions
     * @param int $sharesToBuy
     * @return int
     */
    public function canBuyPlayerStocks(Player $player, $canMakeTransactions, $sharesToBuy=1, $maxStockLimit=0) {
        // 1. check if player is being delisted
        if ($player->isDelisted())
            return PMSBuy::get()->delisted();

        // 2. check if manager can make any more transactions
        if (!$canMakeTransactions)
            return PMSBuy::get()->noTransactionsLeft();

        // this is a transfer, if the player is not already owned by this manager
        $mtp = $this->managerTeam->getPlayerById($player->id);
        $isTransfer = $mtp == null ? true : false;

        // 4. load game configuration and number of players of this manager
        $maxNumberOfPortfolioPlayers = Gameconfiguration::model()->get('max_number_of_portfolio_players');
        $numOfPlayers = $this->managerTeam->numOfPlayers();
        if (( $numOfPlayers >= $maxNumberOfPortfolioPlayers ) && $isTransfer)
            return PMSBuy::get()->reachedPlayerLimit($maxNumberOfPortfolioPlayers);

        // 5. the manager has already 3 players from the same team
        if ($isTransfer) {
            $maxPlayersSameTeam = Gameconfiguration::model()->get('max_players_same_team');
            $mates = $this->numOfTeammates($player->team_id);
            if ($mates >= $maxPlayersSameTeam)
                return PMSBuy::get()->tooManyPlayersFromSameTeam($maxPlayersSameTeam);
        }

        // 6. check if the share value is greater than the manager's available cash
        $valueToBuy = $player->current_value * $sharesToBuy * GameConfiguration::model()->getCommissionFactor();
        if ($valueToBuy > $this->portfolio_cash)
            return PMSBuy::get()->notEnoughCapital();

        // 7. if player is already purchased, check if his current value exceeds the 20% of the portfolio value
        $sharesToCheck = $isTransfer ? $sharesToBuy : ($mtp->shares + $sharesToBuy);
        if ($this->getPlayerStockLimit($player) < $sharesToCheck)
            return PMSBuy::get()->portfolioPercTooHigh();

        // 8. check if player has any shares left in the bank
        if ($player->bank_shares == 0)
            return PMSBuy::get()->noBankShares();

        // 9. last check forcibly introduced for now.
        if ($maxStockLimit > 0 && !$isTransfer)
            return PMSBuy::get()->alreadyHavePlayer();

        return PMSBuy::get()->ok();
    }

    /**
     *
     * @param ManagerTeamPlayer $mtp
     * @param bool $transactions
     * @param int $stocksToSell Check against a specific number of shares if needed.
     * @return bool
     */
    public function canSellPlayerStocks(ManagerTeamPlayer $mtp, $transactions, $stocksToSell=0) {
        // 1. check if manager can make any more transactions
        if ($transactions <= 0)
            return PMSSell::get()->noTransactionsLeft();

        // 2. Check for player count limits.
        $bpos = $mtp->player->basicPosition;

        if ($this->isLastShare($mtp, $stocksToSell) && $this->isTeamShortOnPosition($bpos->abbreviation)) {
            if ($transactions == 1) {
                return PMSSell::get()->notEnoughTransactions();
            }
            return PMSSell::get()->replacePositionPrompt();
        }

        // 3. check number of players against team low limit
//        $minNumberOfPortfolioPlayers = Gameconfiguration::model()->get('min_number_of_portfolio_players');
//        $numOfPlayers = $this->managerTeam->numOfPlayers();
//        if ($this->isLastShare($mtp, $stocksToSell) && $numOfPlayers <= $minNumberOfPortfolioPlayers)
//            return PMSSell::get()->reachedPlayerLimit($minNumberOfPortfolioPlayers);

        return PMSSell::get()->ok();
    }

    /**
     *
     * @param ManagerTeamPlayer $playerToSell
     * @param Player $playerToBuy
     * @param type $canMakeTransactions
     * @param type $sharesToBuy
     * @return int
     */
    public function canReplacePlayer(ManagerTeamPlayer $playerToSell, Player $playerToBuy, $canMakeTransactions, $sharesToBuy) {
        // 1. check if player is being delisted
        if ($playerToBuy->isDelisted())
            return PMSBuy::get()->delisted();

        // 2. check if manager can make any more transactions
        if (!$canMakeTransactions)
            return PMSBuy::get()->notEnoughTransactions();

        $mtp = $this->managerTeam->getPlayerById($playerToBuy->id);
        $isTransfer = $mtp == null ? true : false;

        if (!$isTransfer)
            return PMSBuy::get()->alreadyHavePlayer();

        // 4. load game configuration and number of players of this manager
        $maxNumberOfPortfolioPlayers = Gameconfiguration::model()->get('max_number_of_portfolio_players');

        // Check against not current number of players but the number minus one (the player we are about to sell).
        // In practive this sould never be a problem.
        $numOfPlayers = $this->managerTeam->numOfPlayers();
        if ($numOfPlayers > $maxNumberOfPortfolioPlayers)
            return PMSBuy::get()->reachedPlayerLimit($maxNumberOfPortfolioPlayers);

        // 5. the manager has already 3 players from the same team
        $maxPlayersSameTeam = Gameconfiguration::model()->get('max_players_same_team');

        // Same as above, if we have 3 players of team A, we should not be able to get a player from this team.
        // Unless we are selling a player of team A as well. In that case we should still have to check for the
        // case we have more than 3 player of team A because of a real world transfer.
        $mates = $this->numOfTeammates($playerToBuy->team_id);
        if ($mates > $maxPlayersSameTeam ||
            ($mates == $maxPlayersSameTeam && $playerToSell->player->team_id != $playerToBuy->team_id))
            return PMSBuy::get()->tooManyPlayersFromSameTeam($maxPlayersSameTeam);

        // 6. check if the share value is greater than the manager's available cash
        // Available cash is manager cash plus the price of the selling player shares.
        $valueToBuy = $playerToBuy->current_value * $sharesToBuy * GameConfiguration::model()->getCommissionFactor();
        $availableCash = $this->portfolio_cash + $playerToSell->player->current_value * $playerToSell->shares;
        if ($valueToBuy > $availableCash)
            return PMSBuy::get()->notEnoughCapital();

        // 7. check if his current value exceeds the 20% of the portfolio value
        if ($this->getPlayerStockLimit($playerToBuy) < $sharesToBuy)
            return PMSBuy::get()->portfolioPercTooHigh();

        // 8. check if player has any shares left in the bank
        if ($playerToBuy->bank_shares == 0)
            return PMSBuy::get()->noBankShares();

        return PMSBuy::get()->ok();
    }

    /**
     * Returns true if the manager has at least one stock of this player
     * @param int $player_id
     * @return bool
     */
    public function hasPlayer($player_id) {
        foreach ($this->managerTeam->managerTeamPlayers as $mtp) {
            if ($mtp->player_id == $player_id) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the total number of shares the manager owns from all players.
     * @return int
     */
    public function totalNumOfShares() {
        $shares = 0;
        foreach ($this->managerTeam->managerTeamPlayers as $mtp) {
            $shares += $mtp->shares;
        }
        return $shares;
    }

    /**
     *
     * @param int $team_id
     * @return int
     */
    public function numOfTeammates($team_id) {
        $tm = 0;
        foreach ($this->managerTeam->managerTeamPlayers as $mtp) {
            if ($mtp->player->team_id == $team_id)
                $tm++;
        }
        return $tm;
    }

    /**
     * Returns the maximum number of stocks that the manager can buy from this player.
     * @param Player $player
     * @param ManagerTeamPlayer $playerToSell
     * @return type
     */
    public function getAvailableStocksToBuy(Player $player, ManagerTeamPlayer $playerToSell = null) {
        // if we are replacing players, add the value of the player we are selling
        // to our cash
        $portfolio_cash = $this->portfolio_cash;
        if ($playerToSell) {
            $portfolio_cash += $playerToSell->shares * $playerToSell->player->current_value;
        }

        $canAfford = (int) ($portfolio_cash / ($player->current_value * GameConfiguration::model()->getCommissionFactor()));

        $mtp = $this->managerTeam->getPlayerById($player->id);
        $playerLimit = $this->getPlayerStockLimit($player) - ($mtp ? $mtp->shares : 0);

        $min = $player->bank_shares;
        PMSBuy::get()->noBankSharesMax($min);
        if ($canAfford < $min) {
            $min = $canAfford;
            PMSBuy::get()->notEnoughCapitalMax($min);
        }
        if ($playerLimit < $min) {
            $min = $playerLimit;
            PMSBuy::get()->portfolioPercTooHighMax($min, Gameconfiguration::model()->get('portfolio_perc_rule'));
        }
        return $min;
    }

    /**
     * See if the manager owns enough shares of this player depending on
     * the player's position.
     * @param ManagerTeamPlayer $player
     * @return int
     */
    public function getAvailableStocksToSell(ManagerTeamPlayer $mtp) {

        $transactions = $this->getAvailableTransactions();

        $bpos = $mtp->player->basicPosition;
        if ($this->isTeamShortOnPosition($bpos->abbreviation)) {
            if ($transactions == 1) {
                PMSSell::get()->notEnoughTransactions();
                return $mtp->shares - 1;
            }

            PMSSell::get()->replacePositionPrompt();
            return $mtp->shares - 1;
        }

//        $minNumberOfPortfolioPlayers = Gameconfiguration::model()->get('min_number_of_portfolio_players');
//        if ($this->managerTeam->numOfPlayers() <= $minNumberOfPortfolioPlayers) {
//            PMSSell::get()->reachedPlayerLimit($minNumberOfPortfolioPlayers);
//            return $mtp->shares - 1;
//        }

        PMSSell::get()->canSellCompletely();
        return $mtp->shares;
    }

    /**
     *
     * @param ManagerTeamPlayer $mtp
     * @param int $stocksToSell
     * @return bool
     */
    public function isLastShare($mtp, $stocksToSell) {
        // check if the manager has more than one stock of this player.
        return $mtp->shares - ($stocksToSell > 0 ? $stocksToSell : 1) <= 0 ? true : false;
    }

    /**
     *
     * @param int $pos
     * @return bool
     */
    public function isTeamShortOnPosition($pos) {
        // get position limits
        $posLimits = BasicPosition::model()->getPosLimits();

        // get manger's player count per position
        $playerCounts = $this->managerTeam->numOfPlayersPerPosition();

        // check for limitations on player numbers per position.
        if ($playerCounts[$pos] <= $posLimits[$pos]) {
            return true;
        }

        return false;
    }

    /**
     * Returns the number of stocks that the manager can have from a particular player, based on
     * system limitations and manager attributes.
     * @param Player $player
     * @param int $shares
     * @return int
     */
    public function getPlayerStockLimit($player) {
        $perc = Gameconfiguration::model()->get('portfolio_perc_rule') / 100;
        return (int) (($perc * ($this->portfolio_cash + $this->portfolio_share_value)) / $player->current_value);
    }

    /**
     * Buys shares of a player.
     * If player is new, he is placed as substitute.
     * @param Player $player
     * @param int $shares
     * @return bool
     */
    public function buy(Player $player, $shares) {
        $dbTransaction = _app()->db->beginTransaction();
        try {
            $event = array(
                'action' => 'buy',
                'player_id' => $player->id,
                'shares' => $shares
            );

            $this->_buy($player, $shares);

            Utils::triggerEvent('onStockmarketTransaction', $this);
            Utils::triggerEvent('onStockmarketTransactionBuy', $this);

            $dbTransaction->commit();
            _manager()->refresh();
            return true;
        } catch (CDBException $e) {
            $dbTransaction->rollback();
            Utils::notice('notice', $e->getMessage(), 'error');
            Debug::logToWindow(Debug::exceptionDetailString($e));
        } catch (CException $e) {
            $dbTransaction->rollback();
            Debug::logToWindow(Debug::exceptionDetailString($e));
            return false;
        } catch (CValidateException $e) {
            $dbTransaction->rollback();
            Utils::notice('notice', $e->getMessage(), 'error');
            Debug::logToWindow(Debug::exceptionDetailString($e));
        }
        return false;
    }

    /**
     * Sells shares of a player.
     * If player is sold completely, he is first benched (fittest sub takes his place).
     * @param ManagerTeamPlayer $mtp
     * @param int $shares
     * @return bool
     */
    public function sell(ManagerTeamPlayer $mtp, $shares) {

        // TODO: examine if this check is necessary.
        $curShares = $mtp->shares;
        if ($shares > $curShares) {
            Debug::logToWindow("SELL: Trying to sell $shares while having $curShares.");
            return false;
        }

        $dbTransaction = _app()->db->beginTransaction();
        try {

            if ($curShares == $shares) {
                // if this is a complete sell
                // reorganise team by swapping the player sold first with a best fit from the bench
                $this->managerTeam->autoSubstitute($mtp);
            }

            $event = array(
                'action' => 'sell',
                'player_id' => $mtp->player->id,
                'shares' => $shares,
                'shares_before_transaction' => $mtp->shares
            );

            $this->_sell($mtp, $shares);

            Utils::triggerEvent('onStockmarketTransaction', $this);
            Utils::triggerEvent('onStockmarketTransactionSell', $this);

            $dbTransaction->commit();
            _manager()->refresh();

            return true;
        } catch (CDBException $e) {
            $dbTransaction->rollback();
            Utils::notice('notice', $e->getMessage(), 'error');
            Debug::logToWindow($e->getMessage());
        } catch (CException $e) {
            $dbTransaction->rollback();
            Debug::logToWindow(Debug::exceptionDetailString($e));
            return false;
        } catch (CValidateException $e) {
            $dbTransaction->rollback();
            Utils::notice('notice', $e->getMessage(), 'error');
        }

        return false;
    }

    /**
     * Performs a complete sell of one player and a buy of a new one.
     * New player takes the place of the old one, either in the field or bench.
     * @param ManagerTeamPlayer $playerToSell
     * @param Player $playerToBuy
     * @param type $shares
     * @return bool
     */
    public function replace(ManagerTeamPlayer $playerToSell, Player $playerToBuy, $shares) {

        $dbTransaction = _app()->db->beginTransaction();
        try {
            $event = array(
                'action' => 'replace',
                'player_to_sell_id' => $playerToSell->player->id,
                'player_to_buy_id' => $playerToBuy->id,
                'shares' => $shares,
            );

            $tempStatusId = $playerToSell->status_id;
            $tempPositionId = $playerToSell->position_id;

            $this->_sell($playerToSell, $playerToSell->shares);

            $newPlayer = $this->_buy($playerToBuy, $shares);

            // Put new player at the old one's position.
            $newPlayer->status_id = $tempStatusId;
            $newPlayer->position_id = $tempPositionId;
            $newPlayer->save();

            Utils::triggerEvent('onStockmarketTransaction', $this);
            Utils::triggerEvent('onStockmarketTransactionReplace', $this);

            $dbTransaction->commit();
            _manager()->refresh();

            return true;
        } catch (CDBException $e) {
            $dbTransaction->rollback();
            Utils::notice('notice', $e->getMessage(), 'error');
            Debug::logToWindow(Debug::exceptionDetailString($e));
        } catch (CException $e) {
            $dbTransaction->rollback();
            Debug::logToWindow(Debug::exceptionDetailString($e));
        } catch (CValidateException $e) {
            $dbTransaction->rollback();
            Utils::notice('notice', $e->getMessage(), 'error');
            Debug::logToWindow(Debug::exceptionDetailString($e));
        }
        _manager()->refresh();
        return false;
    }

    /**
     * Returns true if user has role given.
     * @param int $role
     * @return bool
     */
    public function hasRole($role) {
        return Role::is($this, $role);
    }

    /**
     * Returns true if user is banned.
     * @return bool
     */
    public function isBanned($user=0) {
        return Ban::model()->count('manager_id = :mid and DATE_ADD(date_banned, INTERVAL duration DAY) > DATE(NOW())', array(
                ':mid' => self::_getUserContextId($user),
            ));
    }

    /**
     *
     * @param type $user
     * @return type
     */
    private function _getUserContextId($user) {
        // Check for this context, is this an object?
        if (isset($this) && $this->id)
            return $this->id;

        // Check if the system has used this function (access rules) with webuser.
        if ($user && !is_object($user))
            return $user;

        // Check for static use with default param.
        if (_user()->id > 0)
            return _user()->id;

        return 0;
    }

    /**
     * Returns the number of transactions the manager has done till now.
     * @return int
     */
    public function getNumberOfTransactions() {
        return count($this->transactions);
    }

    /**
     *
     */
    public function team() {
        return $this->managerTeam;
    }

    /**
     *
     * @param <type> $extra
     * @return <type>
     */
    public function createActivationCode($extra=array()) {
        $string_to_encrypt = microtime() . $this->username . $this->email;
        foreach ($extra as $e)
            $string_to_encrypt .= $e;
        return md5($string_to_encrypt);
    }

    /**
     *
     * @param int $manager_id
     * @return bool
     */
    public function unblockManager($manager_id) {
        return (BlockedManager::model()->deleteAllByAttributes(array(
                'manager_id' => $this->id,
                'blocked_manager_id' => $manager_id
            )) > 0);
    }

    const BLOCK_ERR_SELF = -1;

    /**
     *
     * @param int $manager_id
     * @return bool
     */
    public function blockManager($manager_id) {
        if ($manager_id == $this->id)
            return self::BLOCK_ERR_SELF;

        $bm = new BlockedManager;
        $bm->manager_id = $this->id;
        $bm->blocked_manager_id = $manager_id;
        return $bm->save();
    }

    /**
     *
     * @param int $manager_id
     * @return bool
     */
    public function hasBlockedManager($manager_id) {
        return BlockedManager::model()->exists('manager_id = :mid AND blocked_manager_id = :bmid', array(
                ':mid' => $this->id,
                ':bmid' => $manager_id
            ));
    }

    /**
     *
     * @param int $manager_id
     * @return bool
     */
    public function isBlockedBy($manager_id) {
        $id = $this->id;
        return BlockedManager::model()->exists('manager_id = :mid AND blocked_manager_id = :bmid', array(
                ':mid' => $manager_id,
                ':bmid' => $this->id
            ));
    }

    /**
     *
     * @return <type>
     */
    public function canReport() {
        $nr = Report::model()->count('reporter_id = :id and date(date_created) = date(now())', array(':id' => $this->id));
        return $nr < Gameconfiguration::model()->get('max_reports_per_day');
    }

    /**
     * Returns false is this manager has already reported the other manager.
     * @return <type>
     */
    public function canReportUser($reported_id) {
        return!Report::model()->exists('manager_id = :mid and reporter_id = :id', array(':id' => $this->id, ':mid' => $reported_id));
    }

    /**
     *
     * @param array $params
     * @return Manager
     */
    public static function createNew($params) {

        $config = Gameconfiguration::model()->find();

        $dbt = _app()->db->beginTransaction();
        try {
            // create manager team
            $managerTeam = ManagerTeam::createNew();
            if (!$managerTeam->save())
                throw new CValidateException($managerTeam);

            // create manager
            $manager = new Manager;
            $manager->role = Role::INACTIVE;
            $manager->username = $params['username'];
            $manager->fullname = $params['fullname'];
            $manager->setPassword($params['password']);
            $manager->email = $params['email'];
            $manager->newsletter = (int) $params['newsletter'];
            $manager->timezone_id = (int) $params['timezone'];
            $manager->registration_date = _app()->localtime->getLocalNow();
            $manager->portfolio_cash = $config->manager_initial_budget;
            $manager->last_known_ip = Yii::app()->request->userHostAddress;
            $manager->total_points = 0;
            $manager->manager_team_id = $managerTeam->id;
            if (!$manager->save())
                throw new CValidateException($manager);

            $profile = new Profile;
            $profile->manager_id = $manager->id;
            $profile->gender = $params['gender'];
            $profile->country_id = (int) $params['country'];
            if (!$profile->save())
                throw new CValidateException($profile);

            $dbt->commit();
            return $manager;
        } catch (CDbException $e) {
            $dbt->rollback();
            Utils::notice('notice', $e->getMessage(), 'error');
            Debug::logToWindow(Debug::exceptionDetailString($e));
        } catch (CException $e) {
            $dbTransaction->rollback();
            Debug::logToWindow(Debug::exceptionDetailString($e));
            return false;
        } catch (CValidateException $e) {
            $dbt->rollback();
            Utils::notice('notice', $e->getMessage(), 'error');
            Debug::logToWindow(Debug::exceptionDetailString($e));
        }
        return null;
    }

    /**
     *
     * @return <type>
     */
    public function getUnbanDate() {
        $connection = Yii::app()->db;

        $sql = 'SELECT DATE_FORMAT(DATE_ADD(date_banned, INTERVAL duration DAY), "%d/%m/%Y") as unban FROM ban WHERE manager_id = :mid';
        $command = $connection->createCommand($sql);
        $command->bindValue(':mid', $this->id);
        return $command->queryScalar();
    }

    /**
     * Returns the current (logged in) manager.
     * For database updates and such use this method and update the instance returned. Later refresh the
     * session loaded version using _manager()->refresh(). Do not use the _manager()->save() to update
     * the manager because in case of failure there is no way to restore the loaded session data.
     * @return <type>
     */
//    public static function current() {
//        return Manager::model()->findByPk(_manager()->id);
//    }

    /**
     *
     * @return <type>
     */
    public function getAge() {
        if (!$this->profile->birthdate)
            return null;

        list($birthYear, $birthMonth, $birthDay) = explode("-", $this->profile->birthdate);

        $yearDiff = date("Y") - $birthYear;
        $monthDiff = date("m") - $birthMonth;
        $dayDiff = date("d") - $birthDay;

        if ($dayDiff < 0 && $monthDiff <= 0)
            $yearDiff--;

        return $yearDiff;
    }

    /**
     *
     * @return <type>
     */
    public function canInvite() {
        $invs = Invitation::model()->countByAttributes(array('manager_id' => _manager()->id), 'DATE(date_sent) = DATE(NOW())');
        return $invs < Gameconfiguration::model()->get('max_invites_per_day');
    }

    /**
     * Checks verification code with code in database
     * @param string $code_in
     * @return bool
     */
    public function verifyEmail($code_in) {
        // email code is null, email is already verified.
        if ($this->email_code == null)
            return true;

        if ($this->role == Role::INACTIVE && $code_in == $this->email_code) {
            $this->email_code = null;
            if ($this->save(true, array('email_code'))) {
                return true;
            }
            Debug::logToWindow($this->getErrors());
        }
        return false;
    }

    /**
     * Turns an inactive manager to temporary manager
     * @return type
     */
    public function activate() {
        if ($this->role == Role::INACTIVE) {
            $this->role = Role::TEMPORARY;
            $this->activation_date = _app()->localtime->getLocalNow();
            if (!$this->save(true, array('role', 'activation_date'))) {
                Debug::logToWindow($this->getErrors());
                return false;
            }
        }
        return true;
    }

    /**
     * Partial manager refresh
     * @param type $attributes
     */
    public function refreshAttributes($attributes) {
        $select = '';
        foreach ($attributes as $attr)
            $select = $attr . ',';
        $select = rtrim($select, ',');

        $id = $this->id;
        $connection = Yii::app()->db;
        $command = $connection->createCommand("select $select from manager where id = :mid");
        $command->bindParam(':mid', $id);
        $values = $command->queryRow();

        foreach ($values as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Performs a buy purchase with no database locking.
     * @param Player $player
     * @param int $shares
     * @return ManagerTeamPlayer
     */
    private function _buy(Player $player, $shares) {
        // At this point we can assume that all necessary checks have been completed.
        // update player's bank shares
        $player->bank_shares -= $shares;
        if (!$player->save(true, array('bank_shares')))
            throw new CValidateException($player);

        // update manager's portfolio
        $sharesValue = $shares * $player->current_value;
        $this->portfolio_cash -= $sharesValue * GameConfiguration::model()->getCommissionFactor();
        $this->portfolio_share_value += $sharesValue;
        if (!$this->save(true, array('portfolio_cash', 'portfolio_share_value')))
            throw new CValidateException($this);

        // update manager's team player list
        $isTransfer = true;

        $mtp = $this->managerTeam->getPlayerById($player->id);
        if (!$mtp) {
            $subAbbr = 'SG';
            switch ($player->basicPosition->abbreviation) {
                case 'DE': $subAbbr = 'SD';
                    break;
                case 'MF': $subAbbr = 'SM';
                    break;
                case 'ST': $subAbbr = 'SS';
                    break;
            }

            // create the new entry
            $mtp = new ManagerTeamPlayer;
            $mtp->manager_team_id = $this->manager_team_id;
            $mtp->player_id = $player->id;
            $mtp->shares = $shares;
            $mtp->position_id = Position::model()->id($subAbbr);
            $mtp->status_id = Status::model()->id('Substitute'); // substitue
        } else {
            $isTransfer = false;

            // update the existing entry
            $mtp->shares += $shares;
        }
        if (!$mtp->save())
            throw new CValidateException($mtp);

        // update transactions
        $transaction = new Transaction;
        $transaction->manager_id = $this->id;
        $transaction->date_completed = _app()->localtime->getLocalNow();
        $transaction->player_id = $player->id;
        $transaction->shares = $shares;
        $transaction->price_share = $player->current_value * Gameconfiguration::model()->getCommissionFactor();
        $transaction->type_of_transaction = 'buy';
        if (!$transaction->save())
            throw new CValidateException($transaction);

        return $mtp;
    }

    /**
     * Performs a sell purchase, with no checks of database locks.
     * @param ManagerTeamPlayer $mtp
     * @param int $shares
     */
    private function _sell(ManagerTeamPlayer $mtp, $shares) {
        // At this point we can assume that all necessary checks have been completed.

        $player = $mtp->player;

        // update manager's team player list
        if ($mtp->shares == $shares) {
            // remove the manager team player entry from the database table
            $mtp->delete();
        } else {
            // update the existing entry
            $mtp->shares -= $shares;
            if (!$mtp->save(true, array('shares')))
                throw new CValidateException($mtp);

            // update player's bank shares
            $player->bank_shares += $shares;
//            $shares += $player->bank_shares;
//            $player->bank_shares = $shares;
            if (!$player->save(true, array('bank_shares')))
                throw new CValidateException($player);
        }

        // update manager's portfolio
        $sharesValue = $shares * $player->current_value;
        $this->portfolio_cash += $sharesValue;
        $this->portfolio_share_value -= $sharesValue;
        if (!$this->save(true, array('portfolio_cash', 'portfolio_share_value')))
            throw new CValidateException($this);

        // update transactions
        $transaction = new Transaction;
        $transaction->manager_id = $this->id;
        $transaction->date_completed = _app()->localtime->getLocalNow();
        $transaction->player_id = $player->id;
        $transaction->shares = $shares;
        $transaction->price_share = $player->current_value;
        $transaction->type_of_transaction = 'sell';
        if (!$transaction->save())
            throw new CValidateException($transaction);
    }

    public function getPortfolioValue($id = null) {
        if ($id == null)
            return _user()->manager->portfolio_share_value + _user()->manager->portfolio_cash;
        return 0;
    }

    /**
     *
     * @param type $attribute
     * @param type $params
     */
    public function isAllowed($attribute, $params) {
        if (!Utils::isPhraseAllowed($this->username)) {
            $this->addError($attribute, _t('This username is not allowed.'));
        }
    }

    /**
     *
     * @return float Last portfolio change.
     */
    public function lastPortfolioChange() {
        $managerHistory = ManagerDailyHistory::model()->getLatestInfo($this->id);
        if ($managerHistory && count($managerHistory) > 1) {
            return (($managerHistory[0]->portfolio_value - $managerHistory[1]->portfolio_value) / $managerHistory[1]->portfolio_value);
        }
        return false;
    }

    /**
     * @param bool $thumbnail
     * @param bool $showUnapproved
     * @return string The manager's profile photo if approved, default image otherwise
     */
    public function profilePhoto($thumbnail = false, $showUnapproved = false) {
        return $this->profile->profilePhoto($thumbnail, $showUnapproved);
    }

    /**
     * @return float The rank in percent form
     */
    public function getRankByPercent() {
        $countManagers = Manager::model()->countByAttributes(array('role' => array(Role::MANAGER, Role::RESET)));
        return ceil(($this->rank->rank / $countManagers) * 100);
    }

    /**
     * @param string $attribute The link content.
     * @return string The link to the manager's profile page.
     */
    public function profileLink($attribute='username') {
        return CHtml::link($this->$attribute, _url('profile/index', array('id' => $this->id)));
    }

    /**
     * @return string The link to the manager's profile page.
     */
    public function teamLink() {
        if (Utils::isLoggedIn() && _manager()->id == $this->id) {
            return CHtml::tag('span', array(), CHtml::link($this->managerTeam->name, _url('pitch/index')));
        }
        if ($this->hasRole(Role::RESET)) {
            return CHtml::tag('span', array('title' => _t('This manager has not yet created his team')), $this->managerTeam->name);
        }
        return CHtml::tag('span', array(), CHtml::link($this->managerTeam->name, _url('profile/players', array('id' => $this->id))));
    }

    /**
     * @return string The role in text form.
     */
    public function roleToText() {
        return Role::toText($this->role);
    }

}

final class PMSBuy {

    protected $msg;

    public function message() {
        return $this->msg;
    }

    static private $instance = NULL;

    static public function get() {
        if (self::$instance == NULL) {
            $cn = __CLASS__;
            self::$instance = new $cn();
        }
        return self::$instance;
    }

    public function ok() {
        return 1;
    }

    public function noBankShares() {
        $this->msg = _t('No shares of this player available for sale at the moment.');
        return -1;
    }

    public function noBankSharesMax($p) {
        $this->msg = _t('Only {p} shares of this player are currently available.', array('{p}' => $p));
        return -1;
    }

    public function delisted() {
        $this->msg = _t('Player is delisted.');
        return -2;
    }

    public function notEnoughCapital() {
        $this->msg = _t('Not enough cash.');
        return -3;
    }

    public function notEnoughCapitalMax($p) {
        $this->msg = _t('You have enough cash for {p} shares.', array('{p}' => $p));
        return -3;
    }

    public function reachedPlayerLimit($p) {
        $this->msg = _t('Maximum {p} players allowed in your team.', array('{p}' => $p));
        return -4;
    }

    public function tooManyPlayersFromSameTeam($p) {
        $this->msg = _t('Maximum {p} players from this team allowed in yours.', array('{p}' => $p));
        return -5;
    }

    public function portfolioPercTooHigh() {
        $this->msg = _t('Budget limit for this player reached.');
        return -6;
    }

    public function portfolioPercTooHighMax($p1, $p2) {
        $this->msg = _t('Budget limit.');
        return -6;
    }

    public function noTransactionsLeft() {
        $this->msg = _t('No remaining transactions today.');
        return -8;
    }

    public function notEnoughTransactions() {
        $this->msg = _t('Not enough transactions remaining today.');
        return -8;
    }

    public function alreadyHavePlayer() {
        $this->msg = _t('You have already picked this player.');
        return -9;
    }

}

final class PMSSell {

    protected $msg;

    public function message() {
        return $this->msg;
    }

    static private $instance = NULL;

    static public function get() {
        if (self::$instance == NULL) {
            $cn = __CLASS__;
            self::$instance = new $cn();
        }
        return self::$instance;
    }

    public function ok() {
        return 1;
    }

    public function canSellCompletely() {
        $this->msg = _t('You can sell all shares of this player.');
        return 1;
    }

    //! Seems deprecated. Unused.
//    public function tooFewPlayersInPosition($p1, $p2) {
//        $this->msg = _t('You cannot sell all shares of this player as your team must consist of {p1} {p2}s.', array('{p1}' => $p1, '{p2}' => $p2));
//        return -1;
//    }

    public function reachedPlayerLimit($p) {
        $this->msg = _t('You cannot sell all shares of this player as your team must consist of {p} players.', array('{p}' => $p));
        return -2;
    }

    public function noTransactionsLeft() {
        $this->msg = _t('No remaining transactions for today.');
        return -3;
    }

    public function notEnoughTransactions() {
        $this->msg = _t('2 transactions are required to replace this player.');
        return -4;
    }

    public function replacePositionPrompt() {
        $this->msg = _t('Replace player');
        return -5;
    }

}

class StockmarketBuyException extends Exception {

    public function __construct($obj, $message) {
        parent::__construct('BUY: ' . $message . ' [' . print_r($obj->getErrors(), true) . ']');
    }

}
