<?php

/**
 * This is the model class for table "manager_daily_history".
 *
 * The followings are the available columns in table 'manager_daily_history':
 * @property integer $id
 * @property integer $manager_id
 * @property string $portfolio_value
 * @property string $date
 *
 * The followings are the available model relations:
 * @property Manager $manager
 */
class ManagerDailyHistory extends CActiveRecord {

    public $chng;

    /**
     * Returns the static model of the specified AR class.
     * @return ManagerDailyHistory the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'manager_daily_history';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('manager_id, portfolio_value, date', 'required'),
            array('manager_id', 'numerical', 'integerOnly' => true),
            array('portfolio_value', 'length', 'max' => 9),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, manager_id, portfolio_value, date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'manager' => array(self::BELONGS_TO, 'Manager', 'manager_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'manager_id' => 'Manager',
            'portfolio_value' => 'Portfolio Value',
            'date' => 'Date',
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->date =
                Yii::app()->localtime->fromLocalDateTime(
                $this->date, 'Y-m-d H:i:s');
            return true;
        }
        else
            return false;
    }

    protected function afterFind() {
        $this->date =
            Yii::app()->localtime->toLocalDateTime(
            $this->date, 'Y-m-d H:i:s');

        return (parent::afterFind());
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('manager_id', $this->manager_id);
        $criteria->compare('portfolio_value', $this->portfolio_value, true);
        $criteria->compare('date', $this->date, true);

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
            ));
    }

    public function getLatestInfo($id) {
        $criteria = new CDbCriteria;
        $criteria->select = 't.portfolio_value';
        $criteria->condition = 'manager_id=?';
        $criteria->params = array($id);
        $criteria->order = 'date desc';
        $criteria->limit = 2;
        return $this->findAll($criteria);
    }

    public function getLatestEvents($id, $limit) {
//        $criteria = new CDbCriteria;
//        $criteria->select = '(((m.portfolio_share_value + m.portfolio_cash)-t.portfolio_value)/portfolio_value) as chng,date';
//        $criteria->join = 'left join manager m on m.id = t.manager_id';
//        $criteria->condition = 't.manager_id = :id';
//        $criteria->params = array(':id'=>$id);
//        $criteria->order = 'date desc';
//        $criteria->limit = $limit;
//        return $this->findAll($criteria);
        $dependency = new CDbCacheDependency('SELECT count(id) FROM manager_daily_history');
        $history = $this->cache(86400, $dependency)->findAll(array(
                'condition' => 'manager_id = ?',
                'limit' => $limit + 1,
                'order' => 'date desc',
                'params' => array($id)
            ));

        $data = array();

        $i = 1;
        foreach ($history as $hist) {
            if ($i > 1) {
                $data[$i]['chng'] = round((($current - $hist->portfolio_value) / $hist->portfolio_value), 4);
                $data[$i]['date'] = $currentDate;
            }

            $current = $hist->portfolio_value;
            $currentDate = $hist->date;
            ++$i;
        }

        return $data;
    }

}
