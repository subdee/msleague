<?php

/**
 * This is the model class for table "manager_match_vote".
 *
 * The followings are the available columns in table 'manager_match_vote':
 * @property integer $manager_id
 * @property integer $game_id
 * @property integer $vote
 */
class ManagerMatchVote extends CActiveRecord {

    const DRAW = 0;
    const HOME = 1;
    const AWAY = 2;
    
    public $homePerc = 0;
    public $awayPerc = 0;
    public $drawPerc = 0;
    public $totalVotes = 0;

    /**
     * Returns the static model of the specified AR class.
     * @return ManagerMatchVote the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    
    public function primaryKey() {
        return array('manager_id','game_id');
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'manager_match_vote';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('manager_id, game_id', 'required'),
            array('manager_id, game_id, vote', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('manager_id, game_id, vote', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'manager' => array(self::BELONGS_TO, 'Manager', 'manager_id'),
            'game' => array(self::BELONGS_TO, 'Game', 'game_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'manager_id' => 'Manager',
            'game_id' => 'Match',
            'vote' => 'Vote',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('manager_id', $this->manager_id);
        $criteria->compare('game_id', $this->game_id);
        $criteria->compare('vote', $this->vote);

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
    }
    
    public function getVotes($gid) {
        $homeVotes = $this->model()->countByAttributes(array('vote' => self::HOME,'game_id' => $gid));
        $awayVotes = $this->model()->countByAttributes(array('vote' => self::AWAY,'game_id' => $gid));
        $drawVotes = $this->model()->countByAttributes(array('vote' => self::DRAW,'game_id' => $gid));
        $this->totalVotes = $homeVotes+$awayVotes+$drawVotes;
        
        if ($this->totalVotes){
            $this->homePerc = round(($homeVotes/($homeVotes+$awayVotes+$drawVotes))*100)/100;
            $this->awayPerc = round(($awayVotes/($homeVotes+$awayVotes+$drawVotes))*100)/100;
            $this->drawPerc = 1-($this->homePerc + $this->awayPerc);
        }
        
        return $this;
    }

}