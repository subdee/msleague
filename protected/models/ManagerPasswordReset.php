<?php

/**
 * This is the model class for table "manager_password_reset".
 *
 * The followings are the available columns in table 'manager_password_reset':
 * @property integer $manager_id
 * @property string $timeout
 *
 * The followings are the available model relations:
 * @property Manager $manager
 */
class ManagerPasswordReset extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return ManagerPasswordReset the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'manager_password_reset';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('manager_id, timeout', 'required'),
            array('manager_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('manager_id, timeout', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'manager' => array(self::BELONGS_TO, 'Manager', 'manager_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'manager_id' => 'Manager',
            'timeout' => 'Timeout',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('manager_id', $this->manager_id);
        $criteria->compare('timeout', $this->timeout, true);

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
            ));
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->timeout =
                Yii::app()->localtime->fromLocalDateTime(
                $this->timeout, 'Y-m-d H:i:s');
            return true;
        }
        return false;
    }

    protected function afterFind() {
        $this->timeout =
            Yii::app()->localtime->toLocalDateTime(
            $this->timeout, 'Y-m-d H:i:s');
        return (parent::afterFind());
    }

}

?>