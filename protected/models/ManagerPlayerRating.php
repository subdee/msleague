<?php

/**
 * This is the model class for table "role".
 *
 * The followings are the available columns in table 'role':
 * @property integer $id
 * @property string $role
 *
 * The followings are the available model relations:
 * @property Manager[] $managers
 * @property User[] $users
 */
class ManagerPlayerRating extends CActiveRecord {

    public $average;
    
    /**
     * Returns the static model of the specified AR class.
     * @return Role the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'manager_player_rating';
    }

    public function primaryKey() {
        return array('manager_id','player_id');
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('rating, manager_id, player_id', 'required'),
            array('rating, manager_id, player_id',  'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('rating, manager_id, player_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'managers' => array(self::HAS_ONE, 'Manager', 'manager_id'),
            'players' => array(self::HAS_ONE, 'Player', 'role_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'rating' => 'Rating',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('player_id', $this->player_id);
        $criteria->compare('manager_id', $this->manager_id);
        $criteria->compare('rating', $this->rating);

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
            ));
    }
    
    public function getAverage($plId){
        $avg = $this->find(array(
            'select' => 'sum(rating)/count(player_id) as average',
            'condition' => 'player_id = :plId',
            'params' => array(':plId' => $plId)
        ));
        
        if ($avg)
            return $avg->average;
        else
            return 0;
    }

}