<?php

/**
 * This is the model class for table "manager_point_history".
 *
 * The followings are the available columns in table 'manager_point_history':
 * @property integer $id
 * @property integer $manager_id
 * @property integer $gameweek_id
 * @property string $total_points
 *
 * The followings are the available model relations:
 * @property Gameweek $gameweek
 * @property Manager $manager
 */
class ManagerPointHistory extends CActiveRecord {

    public $dateOnly;
    
    /**
     * Returns the static model of the specified AR class.
     * @return ManagerGameweekHistory the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'manager_point_history';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('manager_id, date, total_points', 'required'),
            array('manager_id', 'numerical', 'integerOnly' => true),
            array('total_points', 'length', 'max' => 7),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, manager_id, date, total_points', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'manager' => array(self::BELONGS_TO, 'Manager', 'manager_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'manager_id' => 'Manager',
            'date' => 'Date',
            'total_points' => 'Total Points',
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->date =
                Yii::app()->localtime->fromLocalDateTime(
                    $this->date, 'Y-m-d H:i:s');
            return true;
        }
        else
            return false;
    }

    protected function afterFind() {
        $this->date =
            Yii::app()->localtime->toLocalDateTime(
                $this->date, 'Y-m-d H:i:s');
        $this->dateOnly =
            Yii::app()->localtime->toGameDateTime(
                $this->dateOnly, 'Y-m-d');

        return (parent::afterFind());
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('manager_id', $this->manager_id);
        $criteria->compare('date', $this->date);
        $criteria->compare('total_points', $this->total_points, true);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }

    public function getManagerTeamPoints($id, $month, $team) {
//        $dep = new CDbCacheDependency('select count(id) from manager_gameweek_history where gameweek_id = '.$gw.' and manager_id = '.$id);
        return ManagerPointHistory::model()->with('manager')->find('month(date) = :month and manager_id = :id', array(':month' => $month, ':id' => $id));
    }

    public function getDateNoTime() {
        return Utils::date($this->dateOnly, array('time' => false));
    }

}
