<?php

/**
 * This is the model class for table "manager_task".
 *
 * The followings are the available columns in table 'manager_task':
 * @property integer $id
 * @property integer $task_id
 * @property integer $manager_id
 * @property string $meta
 * @property boolean $completed
 * @property integer $rank
 * @property string $context
 *
 * The followings are the available model relations:
 * @property ManagerTaskDesc $desc
 * @property Manager $manager
 */
class ManagerTask extends CActiveRecord {

    private
    $_exists = false,
    $_context = null
    ;
    public
    $deleteOnCompletion = false

    ;

    /**
     * Returns the static model of the specified AR class.
     * @return ManagerTask the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'manager_task';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('task_id, manager_id', 'required'),
            array('task_id, manager_id, rank', 'numerical', 'integerOnly' => true),
            array('completed', 'boolean'),
            array('id, meta, completed, rank, context', 'safe'),
            array('task_id', 'checkUniqueTask'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, task_id, manager_id, meta, completed, rank, context', 'safe', 'on' => 'search'),
        );
    }

    /**
     *
     * @param type $attribute
     * @param type $params 
     */
    public function checkUniqueTask($attribute, $params) {
        $count = $this->count('task_id = :tid and manager_id = :mid', array(
            ':tid' => $this->task_id,
            ':mid' => $this->manager_id,
            ));
        if ($count >= 1) {
            $this->addError('task_id', _t('Duplicate task ({value}) assignment for the same manager not allowed', array('{value}' => $this->task_id)));
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'desc' => array(self::BELONGS_TO, 'ManagerTaskDesc', 'task_id'),
            'manager' => array(self::BELONGS_TO, 'Manager', 'manager_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'task_id' => 'Task',
            'manager_id' => 'Manager',
            'meta' => 'Meta',
            'completed' => 'Completed',
            'rank' => 'Rank',
            'context' => 'Context',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('task_id', $this->task_id);
        $criteria->compare('manager_id', $this->manager_id);
        $criteria->compare('meta', $this->meta, true);
//        $criteria->compare('completed', $this->completed);
        $criteria->compare('rank', $this->rank);
        $criteria->compare('context', $this->context);

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
            ));
    }

    protected function afterFind() {
        parent::afterFind();
        $this->meta = json_decode($this->meta);
    }

    protected function beforeSave() {
        $this->meta = json_encode($this->meta);
        return parent::beforeSave();
    }

    /**
     * This is called when the event is notifying listeners.
     * @param CEvent $event
     * @return bool Whether the task was completed
     */
    public function onEventNotify(CEvent $event) {

        if (!$this->completed) {

            // If task has meta data means it has conditions to check.
            if ($this->meta !== null) {
                $this->completed = $this->onTaskUpdate($event->params);
            } else {
                $this->completed = true;
            }

            $error = false;
            if ($this->completed) {
                // Update the task as completed.
                $this->saveAttributes(array('completed'));

                // update manager's points
                $this->manager->experience_points += $this->desc->points;
                $this->manager->saveAttributes(array('experience_points'));
            } else {
                // Update the task's metadata
                $this->save(false, array('meta'));
            }

            if (!$error) {
                // Call a callback method specific to the task named after the task name
                $method = 'on' . ucfirst($this->desc->name);
                
                $this->_context = Utils::completeObject($this->_context);
                
                if ($this->_context && method_exists($this->_context, $method))
                    call_user_func(array($this->_context, $method), $event->params, $this);

                // Call a general callback for all tasks
                if ($this->_context && method_exists($this->_context, 'onManagerTask'))
                    call_user_func(array($this->_context, 'onManagerTask'), $event->params, $this);
            }
        }

        // If task has already been completed just return.
        if ($this->completed) {

            if ($this->deleteOnCompletion) {
                $this->delete();
            } else {
                // Call a callback method specific to the task named after the task name
                $method = 'on' . ucfirst($this->desc->name) . 'Completed';

                $this->_context = Utils::completeObject($this->_context);

                if ($this->_context && method_exists($this->_context, $method))
                    call_user_func(array($this->_context, $method), $event->params, $this);

                // Call a general callback for all tasks
                if ($this->_context && method_exists($this->_context, 'onManagerTaskCompleted'))
                    call_user_func(array($this->_context, 'onManagerTaskCompleted'), $event->params, $this);
            }
            return true;
        }

        return $this->completed;
    }

    /**
     * This is called to update the state of the task. It returns true the task is complete.
     * You can override this to add conditions on task completion.
     * @return bool Whether the task met completion conditions. 
     */
    protected function onTaskUpdate(array $params) {
        return true;
    }

    public function setContext($value) {
        if ($this->_context === null) {
            $this->_context = $value;
        }
    }

    public function exists() {
        return $this->_exists;
    }

}

?>