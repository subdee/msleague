<?php

/**
 * This is the model class for table "manager_task_desc".
 *
 * The followings are the available columns in table 'manager_task_desc':
 * @property integer $id
 * @property string $name
 * @property integer $points
 * @property string $meta
 * @property string $event
 * @property string $class
 *
 * The followings are the available model relations:
 * @property Manager[] $managers
 */
class ManagerTaskDesc extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return ManagerTasksDesc the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'manager_task_desc';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, points, event', 'required'),
            array('points', 'numerical', 'integerOnly'=>true),
            array('name, event', 'length', 'max'=>32),
            array('meta, class', 'safe'),
            array('name', 'unique', 'className' => 'ManagerTaskDesc', 'message' => _t('A task named "{value}" already exists')),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, points, meta, event, class', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'tasks' => array(self::HAS_MANY, 'ManagerTask', 'task_id'),
            'managers' => array(self::HAS_MANY, 'Manager', 'manager_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'points' => 'Points',
            'meta' => 'Meta',
            'event' => 'Event',
            'class' => 'Class',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('points',$this->points);
        $criteria->compare('meta',$this->meta,true);
        $criteria->compare('event',$this->event,true);
        $criteria->compare('class', $this->class);

        return new CActiveDataProvider(get_class($this), array(
            'criteria'=>$criteria,
        ));
    }
    
    protected function afterFind() {
        parent::afterFind();
        $this->meta = json_decode($this->meta);
    }
    
    protected function beforeSave() {
        $this->meta = json_encode($this->meta);
        return parent::beforeSave();
    }
} 

?>