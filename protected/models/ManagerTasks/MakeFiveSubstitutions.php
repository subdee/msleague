<?php

/**
 * To create a conditional manager task you need to override:
 * 1. The constructor to as shown below
 * 2. the onActionUpdate callback
 */
class MakeFiveSubstitutions extends ManagerTask {

    /**
     * Called by the manager point system when installing the description.
     * @return array
     */
    public static function description() {
        return array(
            'name' => 'make_five_substitutions',
            'points' => 3,
            'event' => 'onSwapPlayers',
            'meta' => 5,
        );
    }

    /**
     * Called by the manager point system when assigning the task to a manager.
     * @return array
     */
    public static function setup() {
        return array(
            'name' => 'make_five_substitutions',
            'meta' => 0,
        );
    }

    protected function onTaskUpdate(array $eventParams) {
        $this->meta++;
        return ($this->meta >= $this->desc->meta);
    }

}

?>
