<?php

/**
 * This is the model class for table "manager_team".
 *
 * The followings are the available columns in table 'manager_team':
 * @property integer $id
 * @property string $name
 * @property integer $formation_id
 * @property string $created_on
 *
 * The followings are the available model relations:
 * @property Manager[] $managers
 * @property ManagerGameweekHistory[] $managerGameweekHistories
 * @property Formation $formation
 * @property ManagerTeamPlayer[] $managerTeamPlayers
 */
class ManagerTeam extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return ManagerTeam the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'manager_team';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('formation_id', 'required'),
            array('formation_id', 'numerical', 'integerOnly' => true),
            array('name', 'match', 'pattern' => '/^[a-zA-Z0-9_\s]{4,30}$/', 'message' => 'The team name must be bewteen 4 and 30 characters (letters, numbers, spaces and underscores).'),
            array('name', 'unique'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, formation_id, created_on', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'manager' => array(self::HAS_ONE, 'Manager', 'manager_team_id'),
            'formation' => array(self::BELONGS_TO, 'Formation', 'formation_id'),
            'managerTeamPlayers' => array(self::HAS_MANY, 'ManagerTeamPlayer', 'manager_team_id', 'order' => 'player.current_value desc', 'with' => 'player'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'formation_id' => 'Formation',
            'created_on' => 'Created On',
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->created_on =
                Yii::app()->localtime->fromLocalDateTime(
                $this->created_on, 'Y-m-d H:i:s');
            return true;
        }
        else
            return false;
    }

    protected function afterFind() {
        $this->created_on =
            Yii::app()->localtime->toLocalDateTime(
            $this->created_on, 'Y-m-d H:i:s');

        return (parent::afterFind());
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('formation_id', $this->formation_id);
        $criteria->compare('created_on', $this->created_on, true);

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
            ));
    }

    /**
     * Picks a player to be a leader. Assumes that there are no empty spots in the line-up
     * selects a best-fit player from the starters.
     */
    public function autoLeader() {
        $rhs = -1;

        $count = $this->numOfPlayers();

        // compare starting players with each other
        for ($lhs = 1; $lhs < $count; $lhs++) {
            if ($this->managerTeamPlayers[$lhs]->isSubstitute())
                continue;

            if ($rhs == -1 || $this->managerTeamPlayers[$lhs]->isFitterThan($this->managerTeamPlayers[$rhs])) {
                $rhs = $lhs;
            }
        }

        $this->assignLeader($this->managerTeamPlayers[$rhs]);
    }

    /**
     * Assigns a new leader to the first team. If old leader is unassigned, the method looks for him.
     * @param ManagerTeamPlayer $mtpNewLeader
     * @param ManagerTeamPlayer $mtpLeader
     */
    public function assignLeader(ManagerTeamPlayer $mtpNewLeader, ManagerTeamPlayer $mtpLeader=null) {

        // Check if the new leader is substitute
        if ($mtpNewLeader->isSubstitute())
            return;

        // Find current leader, if he is not provided
        if (!$mtpLeader) {
            foreach ($this->managerTeamPlayers as $mtp) {
                if ($mtp->isLeader()) {
                    $mtpLeader = $mtp;
                    break;
                }
            }
        }

        // Get all statii
        $statii = Status::model()->all();

        if ($mtpLeader) {
            $mtpLeader->status_id = $statii['Starter']['id'];
            $mtpLeader->saveAttributes(array('status_id'));
        }

        $mtpNewLeader->status_id = $statii['Leader']['id'];
        $mtpNewLeader->saveAttributes(array('status_id'));

        Utils::triggerEvent('onAssignLeader', $this, array(
            'old_leader' => $mtpLeader,
            'new_leader' => $mtpNewLeader,
        ));
    }

    /**
     * Substitutes the given player with the best fit from the bench.
     * @param ManagerTeamPlayer $managerTeamPlayer
     */
    public function autoSubstitute($managerTeamPlayer) {
        // Check if player is already subistitute
        if ($managerTeamPlayer->isSubstitute())
            return true;

        // Get fittest sub.
        $fittest = $this->getFittestSub($managerTeamPlayer);

        $this->swapPlayers($managerTeamPlayer, $fittest);
    }

    /**
     * @param ManagerTeamPlayer $mtp1 The player that we want to sit on the bench
     * @param ManagerTeamPlayer $mtp2 The player that we want to get on the field
     */
    public function swapPlayers(ManagerTeamPlayer $mtp1, ManagerTeamPlayer $mtp2) {
        // update position and status

        $tmpPos = $mtp1->position_id;
        $tmpStatus = $mtp1->status_id;

        $mtp1->position_id = $mtp2->position_id;
        $mtp1->status_id = $mtp2->status_id;
        $mtp1->saveAttributes(array('position_id', 'status_id'));

        $mtp2->position_id = $tmpPos;
        $mtp2->status_id = $tmpStatus;
        $mtp2->saveAttributes(array('position_id', 'status_id'));

        Utils::triggerEvent('onSwapPlayers', $this, array(
            'player_out' => $mtp1,
            'player_in' => $mtp2,
        ));
    }

    /**
     * Returns the number of portfolio players
     * @return int
     */
    public function numOfPlayers() {
        // TODO: examine if we can cache it
        return count($this->managerTeamPlayers);
    }

    /**
     * Returns the number of players per position.
     * @return array
     */
    public function numOfPlayersPerPosition() {
        $counts = array('GK' => 0, 'DE' => 0, 'MF' => 0, 'ST' => 0);
        foreach ($this->managerTeamPlayers as $mtp) {
            $counts[$mtp->position->basicPosition->abbreviation]++;
        }
        return $counts;
    }

    /**
     * Returns the number of players per position.
     * @return array
     */
    public function numOfSubPlayersPerPosition() {
        $counts = array('GK' => 0, 'DE' => 0, 'MF' => 0, 'ST' => 0);
        foreach ($this->managerTeamPlayers as $mtp) {
            if ($mtp->isSubstitute()) {
                $counts[$mtp->position->basicPosition->abbreviation]++;
            }
        }
        return $counts;
    }

    /**
     *
     * @param int $player_id
     * @return ManagerTeamPlayer
     */
    public function getPlayerById($player_id) {
        foreach ($this->managerTeamPlayers as $mtp) {
            if ($mtp->player_id == $player_id)
                return $mtp;
        }
        return null;
    }

    /**
     *
     * @param string $pos
     * @param bool $useAbbr
     * @return ManagerTeamPlayer
     */
    public function getPlayerByPos($pos, $useAbbr=true) {
        if ($useAbbr) {
            $key = 'abbreviation';
            $pos = strtoupper($pos);
        } else {
            $key = 'name';
        }

        foreach ($this->managerTeamPlayers as $mtp) {
            if ($mtp->position->$key == $pos)
                return $mtp;
        }
        return null;
    }

    /**
     * Returns the fittest substitute for a position.
     * If there is no available substitute returns null.
     * @param mixed $param
     * @return ManagerTeamPlayer
     */
    public function getFittestSub($param) {
        // go through the subs of a particular position and find the best fit

        if (is_string($param)) {
            $subPosId = Position::model()->id($param, true);
        } elseif (is_object($param) && ($param instanceof ManagerTeamPlayer)) {
            $subPosId = $param->getSubPosId();
        } else {
            Debug::logToWindow(__FUNCTION__ . ": passed parameter of invalid type [$param]");
            return null;
        }

        $rhs = -1;

        $count = $this->numOfPlayers();

        // compare all players with each other
        for ($lhs = 0; $lhs < $count; $lhs++) {
            if ($subPosId != $this->managerTeamPlayers[$lhs]->position_id)
                continue;

            if (-1 == $rhs || $this->managerTeamPlayers[$lhs]->isFitterThan($this->managerTeamPlayers[$rhs])) {
                $rhs = $lhs;
            }
        }

        if (-1 == $rhs) {
            return null;
        }

        return $this->managerTeamPlayers[$rhs];
    }

    /**
     *
     * @param string $pos
     * @param bool $excludeLeader
     * @param bool $excludeSubstitute
     * @return ManagerTeamPlayer
     */
    public function getLessFit($pos, $excludeLeader=false, $excludeSubstitute=true) {
        $rhs = -1;

        $count = $this->numOfPlayers();

        // compare all players with each other
        for ($lhs = 0; $lhs < $count; $lhs++) {
            if (!$this->managerTeamPlayers[$lhs]->is($pos))
                continue;
            if ($excludeLeader && $this->managerTeamPlayers[$lhs]->is('Leader'))
                continue;
            if ($excludeSubstitute && $this->managerTeamPlayers[$lhs]->is('Substitute'))
                continue;

            if (-1 == $rhs || $this->managerTeamPlayers[$rhs]->isFitterThan($this->managerTeamPlayers[$lhs])) {
                $rhs = $lhs;
            }
        }

        return $this->managerTeamPlayers[$rhs];
    }

    /**
     *
     * @param string $formation
     * @return bool
     */
    public function changeFormation($formation) {
        //Determine formation change
        $oldFormation = $this->formation->formation;
        $formChange = $oldFormation . '->' . $formation;

        //Begin transaction
        $dbTransaction = _app()->db->beginTransaction();
        try {
            switch ($formChange) {
                case '442->433':
                    // To make that formation change we need to bench a midfieder that is
                    // not a leader and is the less fit player and bring in the fittest striker.

                    $this->_removeMidfielder();
                    $this->_addStriker();
                    break;

                case '442->532':
                    // To make that formation change we need to bench a midfieder that is
                    // not a leader and is the less fit player and bring in the fittest defender.

                    $this->_removeMidfielder();
                    $this->_addDefender();
                    break;

                case '433->442':
                    // To make that formation change we need to bench a striker that is
                    // not a leader and is the less fit player and bring in the fittest midfielder.

                    $this->_removeStriker();
                    $this->_addMidfielder();
                    break;

                case '433->532':

                    $this->_removeStriker();
                    $this->_addDefender();
                    break;

                case '532->442':
                    $this->_removeDefender();
                    $this->_addMidfielder();
                    break;

                case '532->433':
                    $this->_removeDefender();
                    $this->_addStriker();
                    break;

                default: throw new Exception("Invalid formation $formChange");
            }

            $this->formation_id = Formation::model()->id($formation);
            $this->save();

            Utils::triggerEvent('onChangeFormation', $this, array(
                'old_formation' => $oldFormation,
                'new_formation' => $formation,
            ));

            //Commit changes
            $dbTransaction->commit();
            _manager()->refresh();

            return true;
        } catch (CDbException $e) {
            $dbTransaction->rollback();
            Debug::logToWindow(Debug::exceptionDetailString($e));
        } catch (CException $e) {
            $dbTransaction->rollback();
            Debug::logToWindow(Debug::exceptionDetailString($e));
            return false;
        } catch (Exception $e) {
            $dbTransaction->rollback();
            Debug::logToWindow(Debug::exceptionDetailString($e));
        }
        return false;
    }

    private function _removeMidfielder() {
        // --- Removing one midfielder
        // Get less fit midfielder that is not a leader.
        $toBench = $this->getLessFit('Midfielder', true);

        // Save hid current position and bench his ass.
        $toBenchPos = $toBench->position->abbreviation;

        $toBench->bench()->save();

        switch ($toBenchPos) {
            case 'CRM':
                $this->getPlayerByPos('CLM')->move('CM')->save();
                break;
            case 'CLM':
                $this->getPlayerByPos('CRM')->move('CM')->save();
                break;
            case 'LM':
                $this->getPlayerByPos('CLM')->move('LM')->save();
                $this->getPlayerByPos('CRM')->move('CM')->save();
                break;
            case 'RM':
                $this->getPlayerByPos('CLM')->move('CM')->save();
                $this->getPlayerByPos('CRM')->move('RM')->save();
                break;
        }
    }

    private function _addMidfielder() {
        // --- Adding a midfielder
        $this->getPlayerByPos('CM')->move('CLM')->save();
        $this->getFittestSub('Midfielder')->move('CRM')->save();
    }

    private function _removeStriker() {
        // --- Removing one striker
        $toBench = $this->getLessFit('Striker', true);

        // Save hid current position and bench his ass.
        $toBenchPos = $toBench->position->abbreviation;

        $toBench->bench()->save();

        // Lining up strikers.
        if ($toBenchPos != 'CS')
            $this->getPlayerByPos('CS')->move($toBenchPos)->save();
    }

    private function _addStriker() {
        // --- Adding a striker
        // Move fittest substitute striker as center striker.
        $this->getFittestSub('Striker')->move('CS')->save();
    }

    private function _removeDefender() {
        // --- Removing one defender
        $toBench = $this->getLessFit('Defender', true);

        // Save hid current position and bench his ass.
        $toBenchPos = $toBench->position->abbreviation;

        $toBench->bench()->save();

        switch ($toBenchPos) {
            case 'LD':
                $this->getPlayerByPos('CLD')->move('LD')->save();
                $this->getPlayerByPos('CD')->move('CLD')->save();
                break;
            case 'CLD':
            case 'CRD':
                $this->getPlayerByPos('CD')->move($toBenchPos)->save();
                break;
            case 'RD':
                $this->getPlayerByPos('CRD')->move('RD')->save();
                $this->getPlayerByPos('CD')->move('CRD')->save();
                break;
        }
    }

    private function _addDefender() {
        // --- Adding a defender
        $this->getFittestSub('Defender')->move('CD')->save();
    }

    /**
     *
     * @param <type> $data
     * @return ManagerTeam
     */
    public static function createNew() {
        $mt = new ManagerTeam;
        $mt->reset();
        return $mt;
    }

    /**
     *
     */
    public function reset() {
        $this->formation_id = Formation::model()->id('442');
        if (isset(_user()->manager) && _manager()->hasRole(Role::TEMPORARY))
            $this->created_on = _app()->localtime->getLocalNow();
    }

}
