<?php

/**
 * This is the model class for table "manager_team_player".
 *
 * The followings are the available columns in table 'manager_team_player':
 * @property integer $id
 * @property integer $manager_team_id
 * @property integer $player_id
 * @property integer $position_id
 * @property integer $shares
 * @property integer $status_id
 *
 * The followings are the available model relations:
 * @property ManagerTeam $managerTeam
 * @property Player $player
 * @property Position $position
 * @property Status $status
 */
class ManagerTeamPlayer extends CActiveRecord {

    public $chng;
    public $total_value;
    public $perc;

    /**
     * Returns the static model of the specified AR class.
     * @return ManagerTeamPlayer the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'manager_team_player';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('manager_team_id, player_id, position_id, shares, status_id', 'required'),
            array('manager_team_id, player_id, position_id, shares, status_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, manager_team_id, player_id, position_id, shares, status_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'managerTeam' => array(self::BELONGS_TO, 'ManagerTeam', 'manager_team_id'),
            'player' => array(self::BELONGS_TO, 'Player', 'player_id'),
            'position' => array(self::BELONGS_TO, 'Position', 'position_id'),
            'status' => array(self::BELONGS_TO, 'Status', 'status_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'manager_team_id' => 'Manager Team',
            'player_id' => 'Player',
            'position_id' => 'Position',
            'shares' => _t('Shares'),
            'status_id' => 'Status',
            'chng' => _t('Change'),
            'total_value' => _t('Total Price'),
            'perc' => _t('Owned'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->select = '*, (t.shares * player.current_value) as total_value, (player.current_value - ph.value)/ph.value as chng';
        $criteria->with = array('player' => array('join' => 'left join player_daily_history ph on ph.player_id = player.id'),'managerTeam' => array('with' => 'manager'));
        $criteria->condition = 'manager.id = :id and ph.date = (select max(date) from player_daily_history)';
        $criteria->params = array(':id' => _user()->id);

        $sort = new CSort;
        $sort->attributes = array(
            'player.basicPosition.abbreviation' => array(
                'asc' => 'player.basic_position_id asc',
                'desc' => 'player.basic_position_id desc'
            ),
            'player.shortname',
            'player.team.shortname5' => array(
                'asc' => 'player.team_id asc',
                'desc' => 'player.team_id desc',
            ),
            'shares',
            'player.current_value',
            'chng' => array(
                'asc' => '(p.current_value - ph.value)/ph.value asc',
                'desc' => '(p.current_value - ph.value)/ph.value desc',
            ),
            'total_value' => array(
                'asc' => '(t.shares * p.current_value) asc',
                'desc' => '(t.shares * p.current_value) desc',
            ),
            'perc' => array(
                'asc' => '(t.shares * p.current_value)/(m.portfolio_cash + m.portfolio_share_value) asc',
                'desc' => '(t.shares * p.current_value)/(m.portfolio_cash + m.portfolio_share_value) desc',
            ));
        $sort->defaultOrder = 'player.basic_position_id asc, player.shortname asc';

        $dep = new CDbCacheDependency('SELECT SUM(shares)+SUM(id) FROM manager_team_player');

        return new CActiveDataProvider(ManagerTeamPlayer::model()->cache(21600, $dep), array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 100
            ),
            'sort' => $sort
        ));
    }

    /**
     * Before deleting the manager team player entry, return player's bank shares.
     */
    public function beforeDelete() {
        if (parent::beforeDelete()) {
            $this->player->bank_shares += $this->shares;
            return $this->player->save(true, array('bank_shares'));
        }
        return false;
    }

    /**
     * Returns true if one player is fitter to be a leader or in the starters line-up than  another.
     * @param ManagerTeamPlayer $player
     * @return bool
     */
    public function isFitterThan($managerTeamPlayer) {
        // 1. number of shares owned by the manager
        if ($this->shares > $managerTeamPlayer->shares)
            return true;

        // 2. share value
        if ($this->shares == $managerTeamPlayer->shares) {
            if ($this->player->current_value > $managerTeamPlayer->player->current_value)
                return true;

            // 3. alphabetical
            if ($this->player->current_value == $managerTeamPlayer->player->current_value) {
                if (strcmp($this->player->name, $managerTeamPlayer->player->name) < 0)
                    return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isSubstitute() {
        return $this->status->isSubstitute();
    }

    /**
     * @return bool
     */
    public function isLeader() {
        return $this->status->isLeader();
    }

    /**
     * @return bool
     */
    public function isStarter() {
        return $this->status->isStarter();
    }

    public function is($p) {
        return ($this->position->name === $p ||
        $this->position->abbreviation === $p ||
        $this->position->basicPosition->name === $p ||
        $this->position->basicPosition->abbreviation === $p ||
        $this->status->status === $p);
    }

    /**
     * Returns the corresponding substitute id of his basic position.
     * @return int
     */
    public function getSubPosId() {
        // get player's basic position name
        $basicName = $this->position->basicPosition->name;
        // in order to get the sub position's id we are looking for
        return Position::model()->id($basicName, true);
    }

    /**
     * Sits player on the bench.
     * User is responsible for not messing up the lineup with actions like leaving an empty spot or having 2 Center Stikers.
     * @param boolean $leader
     * @return ManagerTeamPlayer
     */
    public function bench(&$leader = false) {
        // set correct position id
        $this->position_id = $this->getSubPosId();
        // set status
        $leader = $this->isLeader();
        $this->status_id = Status::model()->id('Substitute');

        return $this;
    }

    /**
     * Moves any player to specific starting position.
     * This method should not be used to move a player to a substitute position. Use bench() instead.
     * User is responsible for not messing up the lineup with actions like leaving an empty spot or having 2 Center Stikers.
     * @param string $pos
     * @return ManagerTeamPlayer
     */
    public function move($pos) {
        $this->position_id = Position::model()->id($pos);

        if ($this->isSubstitute()) {
            $this->status_id = Status::model()->id('Starter');
        }
        return $this;
    }

    /**
     *
     * @param <type> $data
     * @return ManagerTeamPlayer
     */
    public static function createNew($position) {
        $pitchPosition = Position::model()->findByAttributes(array('name' => $position));

        $mtp = new ManagerTeamPlayer;
        $mtp->shares = 1;
        $mtp->position_id = $pitchPosition->id;
        if (strpos($position, 'Substitute') === false) {
            $mtp->status_id = Status::model()->id('Starter');
        } else {
            $mtp->status_id = Status::model()->id('Substitute');
        }
        return $mtp;
    }

}
