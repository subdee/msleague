<?php

/**
 * This is the model class for table "manager_team_player_history".
 *
 * The followings are the available columns in table 'manager_team_player_history':
 * @property integer $id
 * @property integer $player_id
 * @property integer $position_id
 * @property integer $shares
 * @property integer $status_id
 * @property integer $gameweek_id
 * @property integer $manager_id
 *
 * The followings are the available model relations:
 * @property Gameweek $gameweek
 * @property Manager $manager
 * @property Player $player
 * @property Position $position
 * @property Status $status
 */
class ManagerTeamPlayerHistory extends CActiveRecord {

    public $name;
    public $team;
    public $points;
    public $co;
    public $gs;
    public $ng;
    public $ass;
    public $ga;
    public $cs;
    public $ps;
    public $rc;
    public $og;
    public $mp;
    public $min;
    public $pos;
    public $stati;
    public $gameDate;

    /**
     * Returns the static model of the specified AR class.
     * @return ManagerTeamPlayerHistory the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'manager_team_player_history';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('player_id, position_id, shares, status_id, date, manager_id', 'required'),
            array('player_id, position_id, shares, status_id, manager_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, player_id, position_id, shares, status_id, date, manager_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'manager' => array(self::BELONGS_TO, 'Manager', 'manager_id'),
            'player' => array(self::BELONGS_TO, 'Player', 'player_id'),
            'position' => array(self::BELONGS_TO, 'Position', 'position_id'),
            'status' => array(self::BELONGS_TO, 'Status', 'status_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'player_id' => 'Player',
            'position_id' => 'Position',
            'shares' => 'Shares',
            'status_id' => 'Status',
            'date' => 'Date',
            'manager_id' => 'Manager',
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->date =
                Yii::app()->localtime->fromLocalDateTime(
                $this->date, 'Y-m-d H:i:s');
            return true;
        }
        else
            return false;
    }

    protected function afterFind() {
        $this->date =
            Yii::app()->localtime->toLocalDateTime(
            $this->date, 'Y-m-d H:i:s');
        $this->gameDate =
            Yii::app()->localtime->toGameDateTime(
            $this->gameDate, 'Y-m-d H:i:s');

        return (parent::afterFind());
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {

        if ($this->date == null)
            $date = _app()->localtime->getLocalNow('Y-m-d H:i:s');
        else
            $date = '= DATE(:date)';

        $minS = '(SELECT IFNULL(sum(event_value),0) FROM game_player gp LEFT JOIN game g ON g.id = gp.game_id LEFT JOIN event e ON e.id = gp.event_id WHERE gp.player_id = t.player_id AND DATE(CONVERT_TZ(g.date_played,"UTC",:tz)) ' . $date . ' AND e.id = gp.event_id AND e.name = \'Minutes Played\')';
        $gsS = '(SELECT IFNULL(sum(event_value),0) FROM game_player gp LEFT JOIN game g ON g.id = gp.game_id LEFT JOIN event e ON e.id = gp.event_id WHERE gp.player_id = t.player_id AND DATE(CONVERT_TZ(g.date_played,"UTC",:tz)) ' . $date . ' AND e.id = gp.event_id AND e.name = \'Goals Scored\')';
        $assS = '(SELECT IFNULL(sum(event_value),0) FROM game_player gp LEFT JOIN game g ON g.id = gp.game_id LEFT JOIN event e ON e.id = gp.event_id WHERE gp.player_id = t.player_id AND DATE(CONVERT_TZ(g.date_played,"UTC",:tz)) ' . $date . ' AND e.id = gp.event_id AND e.name = \'Assists\')';
        $ngS = '(SELECT IFNULL(sum(event_value),0) FROM game_player gp LEFT JOIN game g ON g.id = gp.game_id LEFT JOIN event e ON e.id = gp.event_id WHERE gp.player_id = t.player_id AND DATE(CONVERT_TZ(g.date_played,"UTC",:tz)) ' . $date . ' AND e.id = gp.event_id AND e.name = \'No Goals\')';
        $gaS = '(SELECT IFNULL(sum(event_value),0) FROM game_player gp LEFT JOIN game g ON g.id = gp.game_id LEFT JOIN event e ON e.id = gp.event_id WHERE gp.player_id = t.player_id AND DATE(CONVERT_TZ(g.date_played,"UTC",:tz)) ' . $date . ' AND e.id = gp.event_id AND e.name = \'Goals Against\')';
        $csS = '(SELECT IFNULL(sum(event_value),0) FROM game_player gp LEFT JOIN game g ON g.id = gp.game_id LEFT JOIN event e ON e.id = gp.event_id WHERE gp.player_id = t.player_id AND DATE(CONVERT_TZ(g.date_played,"UTC",:tz)) ' . $date . ' AND e.id = gp.event_id AND e.name = \'Clean Sheet\')';
        $psS = '(SELECT IFNULL(sum(event_value),0) FROM game_player gp LEFT JOIN game g ON g.id = gp.game_id LEFT JOIN event e ON e.id = gp.event_id WHERE gp.player_id = t.player_id AND DATE(CONVERT_TZ(g.date_played,"UTC",:tz)) ' . $date . ' AND e.id = gp.event_id AND e.name = \'Penalties Saved\')';
        $rcS = '(SELECT IFNULL(sum(event_value),0) FROM game_player gp LEFT JOIN game g ON g.id = gp.game_id LEFT JOIN event e ON e.id = gp.event_id WHERE gp.player_id = t.player_id AND DATE(CONVERT_TZ(g.date_played,"UTC",:tz)) ' . $date . ' AND e.id = gp.event_id AND e.name = \'Red Cards\')';
        $ogS = '(SELECT IFNULL(sum(event_value),0) FROM game_player gp LEFT JOIN game g ON g.id = gp.game_id LEFT JOIN event e ON e.id = gp.event_id WHERE gp.player_id = t.player_id AND DATE(CONVERT_TZ(g.date_played,"UTC",:tz)) ' . $date . ' AND e.id = gp.event_id AND e.name = \'Own Goals\')';
        $mpS = '(SELECT IFNULL(sum(event_value),0) FROM game_player gp LEFT JOIN game g ON g.id = gp.game_id LEFT JOIN event e ON e.id = gp.event_id WHERE gp.player_id = t.player_id AND DATE(CONVERT_TZ(g.date_played,"UTC",:tz)) ' . $date . ' AND e.id = gp.event_id AND e.name = \'Missed Penalties\')';

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('player_id', $this->player_id);
        $criteria->compare('position_id', $this->position_id);
        $criteria->compare('shares', $this->shares);
        $criteria->compare('status_id', $this->status_id);
        $criteria->compare('status_id', $this->stati);
        $criteria->compare('manager_id', $this->manager_id);

        $condition = 'date(convert_tz(pl.date,"UTC",:tz)) ' . $date . ' AND date(convert_tz(t.date,"UTC",:tz)) ' . $date;
        $join = '
            LEFT JOIN
                (player_point_history pl LEFT JOIN
                    (player p LEFT JOIN basic_position pos ON pos.id = p.basic_position_id LEFT JOIN team te ON te.id = p.team_id)
                    ON pl.player_id = p.id)
                ON t.player_id = pl.player_id
            LEFT JOIN manager m ON m.id = t.manager_id
            LEFT JOIN status s ON s.id = t.status_id';

        $criteria->addCondition($condition);
        $criteria->join = $join;
//        $criteria->params = $params;

        $criteria->select = "*,pos.abbreviation as pos,p.shortname as name,pl.points as points,s.coefficient as co,t.date as gameDate,
            $minS AS min,
            $gsS AS gs,
            $assS AS ass,
            $ngS AS ng,
            $gaS AS ga,
            $csS AS cs,
            $psS AS ps,
            $rcS AS rc,
            $ogS AS og,
            $mpS AS mp";

        $criteria->params = array_merge($criteria->params, array(
            ':date' => $this->date,
            ':tz' => _timezone()
            ));

        $sort = new CSort;
        $sort->defaultOrder = 'pos.id asc';

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'pagination' => false,
                'sort' => $sort
            ));
    }

    /**
     * @return bool
     */
    public function isSubstitute() {
        return $this->status->isSubstitute();
    }

    /**
     * @return bool
     */
    public function isLeader() {
        return $this->status->isLeader();
    }

    /**
     * @return bool
     */
    public function isStarter() {
        return $this->status->isStarter();
    }

}
