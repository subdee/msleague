<?php

class OnSports implements NewsFeed {

    public function getNews($no, $team) {

        if ($team) {
            $tstr[$team] = _app()->cache->get('index_team_' . $team . '_news');
            if ($tstr[$team] === false) {

                $context = stream_context_create(array(
                    'http' => array(
                        'timeout' => 10
                    )
                    ));
                
                $tstr[$team] = file_get_contents('http://www.onsports.gr/soccer/AEK?format=feed', 0, $context);

                if (!empty($tstr[$team]))
                    _app()->cache->set('index__team_'.$team.'_news', $tstr[$team], 1800);
                else
                    return false;
            }

            $xml = new SimpleXMLElement($tstr[$team]);
        } else {
            $str = _app()->cache->get('index_news');
            if ($str === false) {

                $context = stream_context_create(array(
                    'http' => array(
                        'timeout' => 10
                    )
                    ));

                $str = file_get_contents('http://www.onsports.gr/Podosfairo/Super-League?format=feed', 0, $context);

                if (!empty($str))
                    _app()->cache->set('index_news', $str, 1800);
                else
                    return false;
            }

            $xml = new SimpleXMLElement($str);
        }

        $i = 0;
        foreach ($xml->children() as $child) {
            foreach ($child->children() as $item) {                
                if ($item->getName() == 'item') {
                    if ($i < $no) {
                        $news[] = $item;
                        ++$i;
                    }
                }
            }
        }

        $j = 0;
        foreach ($news as $new) {
//            var_dump($new->link);
            $items[$j]['link'] = $new->link;
            $items[$j]['title'] = $new->title;
            $items[$j]['date'] = date('Y-m-d H:i:s',strtotime($new->pubDate));
            $descr = strip_tags((string) $new->description, '<img>');
            if(strlen($descr) >= 400)
                $items[$j]['content'] = substr($descr, 0, strpos($descr, " ", 400)) . '...';
            else
                $items[$j]['content'] = $descr;
            ++$j;
        }

//        die();

        return $items;
    }

}

?>
