<?php

/**
 * PartnersForm class.
 * PartnersForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class PartnersForm extends CFormModel
{
	public $fullname;
	public $email;
	public $company;
	public $address;
	public $phone;
	public $topic;
	public $verifyCode;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// email, subject and body are required
			array('fullname, email, company, address, phone, topic', 'required'),
            array('phone', 'numerical'),
			// email has to be a valid email address
			array('email', 'email'),
			// verifyCode needs to be entered correctly
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'fullname'=>_t('Full name'),
			'email'=>_t('Email'),
			'company'=>_t('Company'),
			'address'=>_t('Address'),
			'phone'=>_t('Phone'),
			'topic'=>_t('Topic'),
			'verifyCode'=>_t('Verification Code'),
		);
	}

    public function send()
    {
        $tpl = _controller()->renderPartial('_partnersEmail', array(
            'fullname'=>$this->fullname,
            'company'=>$this->company,
            'address'=>$this->address,
            'phone'=>$this->phone,
            'topic'=>$this->topic,
            ), true);

        if(Utils::sendEmail('partners@msleague.com', "Proposal from {$this->fullname}", $tpl, $this->email, $this->fullname))
        {
            $this->fullname = null;
            $this->email = null;
            $this->company = null;
            $this->address = null;
            $this->phone = null;
            $this->topic = null;
            $this->verifyCode = null;
            return true;
        }
        $this->verifyCode = null;
        return false;
    }
}