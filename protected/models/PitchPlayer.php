<?php

class PitchPlayer
{
    // common
    public $class = 'clickable ';
    public $player;
    public $isEmpty;
    
    // extra parameters
    public $params;
    
    
    public function __construct(array $params, Player $player = null) 
    {
        if(isset($params['clickable']) && !$params['clickable'])
        {
            $this->class = '';
        }
        if(!$player)
        {
            $this->isEmpty = true;
            $this->class .= 'pitchplayer empty';
        }
        else
        {
            $this->player = $player;
            $this->class .= "pitchplayer nonempty pitchplayer-{$this->player->id} ";
            $this->class .= strtolower($this->player->basicPosition->name);

            if(isset($params['isSubstitute']) && $params['isSubstitute']) 
            {
                $this->class .= ' pitchsub ';
            }
            else 
            {
                $this->class .= ' pitchmain ';
            }
        }
        $this->params = $params;
    }
    
    public function view()
    {
        return _controller()->renderPartial('/pitch/_pitchPlayer', 
            array_merge(
                array(
                    'player' => $this->player,
                    'isEmpty' => $this->isEmpty,
                    'class' => $this->class,
                ),
                $this->params
            ), 
            true );
    }
}

?>
