<?php

/**
 * This is the model class for table "player".
 *
 * The followings are the available columns in table 'player':
 * @property integer $id
 * @property string $name
 * @property string $shortname
 * @property integer $basic_position_id
 * @property integer $team_id
 * @property string $list_date
 * @property string $delist_date
 * @property integer $bank_shares
 * @property string $current_value
 * @property string $initial_value
 * @property string $vmi_season
 * @property string $total_points
 * @property integer $is_injured
 * @property integer $is_suspended
 * @property string $owned_perc
 *
 * The followings are the available model relations:
 * @property GamePlayer[] $gamePlayers
 * @property ManagerTeamPlayer[] $managerTeamPlayers
 * @property ManagerTeamPlayerHistory[] $managerTeamPlayerHistories
 * @property BasicPosition $basicPosition
 * @property Team $team
 * @property PlayerDailyHistory[] $playerDailyHistories
 * @property PlayerGameweekHistory[] $playerGameweekHistories
 * @property Transaction[] $transactions
 */
class Player extends CActiveRecord {

    public $chng;
    public $pos;
    public $myTeam;
    public $plId;
    public $apps;
    public $low_apps;
    public $gs;
    public $ng;
    public $ass;
    public $ga;
    public $cs;
    public $ps;
    public $rc;
    public $og;
    public $mp;
    public $bought;
    public $abbreviation;

    /**
     * Returns the static model of the specified AR class.
     * @return Player the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'player';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, shortname', 'required'),
            array('name, shortname, xml_id', 'unique'),
            array('basic_position_id, team_id, bank_shares', 'numerical', 'integerOnly' => true),
            array('is_injured, is_suspended', 'boolean'),
            array('name', 'length', 'max' => 45),
            array('shortname', 'length', 'max' => 17),
            array('current_value, initial_value', 'length', 'max' => 9),
            array('vmi_season, total_points,owned_perc', 'length', 'max' => 7),
            array('name, shortname, list_date, delist_date, pos, myTeam', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, shortname, basic_position_id, team_id, list_date, delist_date, bank_shares, current_value, initial_value, vmi_season, total_points, is_injured, is_suspended, average_rating', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'gamePlayers' => array(self::HAS_MANY, 'GamePlayer', 'player_id'),
            'managerTeamPlayers' => array(self::HAS_MANY, 'ManagerTeamPlayer', 'player_id'),
            'managerTeamPlayerHistories' => array(self::HAS_MANY, 'ManagerTeamPlayerHistory', 'player_id'),
            'basicPosition' => array(self::BELONGS_TO, 'BasicPosition', 'basic_position_id'),
            'team' => array(self::BELONGS_TO, 'Team', 'team_id'),
            'playerDailyHistories' => array(self::HAS_MANY, 'PlayerDailyHistory', 'player_id'),
            'playerPointHistories' => array(self::HAS_MANY, 'PlayerPointHistory', 'player_id'),
            'transactions' => array(self::HAS_MANY, 'Transaction', 'player_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => _t('Name'),
            'shortname' => _t('Name'),
            'basic_position_id' => 'Basic Position',
            'team_id' => _t('Team'),
            'list_date' => _t('List Date'),
            'delist_date' => _t('Delist Date'),
            'bank_shares' => _t('Bank Shares'),
            'current_value' => _t('Price'),
            'initial_value' => _t('Initial Price'),
            'vmi_season' => _t('VMI'),
            'total_points' => _t('Points'),
            'is_injured' => _t('Injured'),
            'is_suspended' => _t('Suspended'),
            'owned_perc' => _t('% Owned'),
            'pos' => _t('Position'),
            'myTeam' => _t('Team'),
            'perc_owned' => _t('% Owned'),
            'chng' => _t('Change'),
            'apps' => _t('60\'+'),
            'low_apps' => _t('<60\''),
            'gs' => Event::image('Goals Scored', 16, false),
            'ass' => Event::image('Assists', 16, false),
            'ng' => _t('NS'),
            'ga' => _t('GA'),
            'cs' => _t('CS'),
            'ps' => Event::image('Penalties Saved', 16, false),
            'rc' => Event::image('Red Cards', 16, false),
            'og' => Event::image('Own Goals', 16, false),
            'mp' => Event::image('Missed Penalties', 16, false),
            'min' => Event::image('Minutes Played', 16, false),
            'pointsPerShare' => _t('P/Sh'),
            'noOfShares' => _t('Sh'),
        );
    }

    public function tooltipLabels() {
        return array(
            'apps' => _t('Appearances over 60 min'),
            'low_apps' => _t('Appearances under 60 min'),
            'gs' => _t('Goals'),
            'ass' => _t('Assists'),
            'ng' => _t('No Score'),
            'ga' => _t('Goals Against'),
            'cs' => _t('Clean Sheets'),
            'ps' => _t('Penalties Saved'),
            'rc' => _t('Red Cards'),
            'og' => _t('Own Goals'),
            'mp' => _t('Missed Penalties'),
            'min' => _t('Minutes Played'),
            'pointsPerShare' => _t('Points per Share'),
            'noOfShares' => _t('Number of Shares'),
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->list_date =
                Yii::app()->localtime->fromLocalDateTime(
                $this->list_date, 'Y-m-d H:i:s');
            $this->delist_date =
                Yii::app()->localtime->fromLocalDateTime(
                $this->delist_date, 'Y-m-d H:i:s');
            return true;
        }
        else
            return false;
    }

    protected function afterFind() {
        $this->list_date =
            Yii::app()->localtime->toLocalDateTime(
            $this->list_date, 'Y-m-d H:i:s');
        $this->delist_date =
            Yii::app()->localtime->toLocalDateTime(
            $this->delist_date, 'Y-m-d H:i:s');

        return (parent::afterFind());
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('shortname', $this->shortname, true);
        $criteria->compare('basic_position_id', $this->basic_position_id);
        $criteria->compare('team_id', $this->team_id);
        $criteria->compare('list_date', $this->list_date, true);
        $criteria->compare('delist_date', $this->delist_date, true);
        $criteria->compare('bank_shares', $this->bank_shares);
        $criteria->compare('current_value', $this->current_value, true);
        $criteria->compare('initial_value', $this->initial_value, true);
        $criteria->compare('vmi_season', $this->vmi_season, true);
        $criteria->compare('total_points', $this->total_points, true);
        $criteria->compare('is_injured', $this->is_injured);
        $criteria->compare('is_suspended', $this->is_suspended);

        $with = null;
        $condition = 'IF (player_daily_history.date is not null,player_daily_history.date = (select max(date) from player_daily_history where date < now()),1) and t.list_date is not null and t.list_date < now()';
        $join = 'LEFT JOIN player_daily_history ON player_daily_history.player_id = t.id  LEFT JOIN team te ON te.id = t.team_id';
        $params = array();

        if (isset($_GET['Player']['pos']) && $_GET['Player']['pos'] != null) {
//                    $with[] = 'basicPosition';
//                    $condition .= " and t.basic_position_id = :pos";
//                    $params[':pos'] = $_GET['Player']['pos'];
            $criteria->addSearchCondition('t.basic_position_id', $_GET['Player']['pos'], false);
        }

        if (isset($_GET['Player']['myTeam']) && $_GET['Player']['myTeam'] != null) {
//                    $with[] = 'team';
//                    $condition .= " and t.team_id = :team";
//                    $params[':team'] = $_GET['Player']['myTeam'];
            $criteria->addSearchCondition('t.team_id', $_GET['Player']['myTeam'], false);
        }
//                else{
//                    $join .= ' ';
//                }

        $join .= ', gameconfiguration gc';
        if ($with != null)
            $criteria->with = $with;
        $criteria->join = $join;
        $criteria->addCondition($condition);
//                if (!empty($params))
//                    $criteria->params = $params;
        $criteria->select = '*,t.id as plId,
                    player_daily_history.bought as bought,
                    IFNULL((t.current_value - player_daily_history.value)/player_daily_history.value,0) as chng';

        $sort = new CSort;
        $sort->attributes = array(
//                    'basicPosition.abbreviation',
            'shortname',
            'myTeam' => array(
                'asc' => 'te.shortname5 asc',
                'desc' => 'te.shortname5 desc',
            ),
            'total_points',
            'current_value',
            'vmi_season',
            'bought' => array(
                'asc' => 'player_daily_history.bought asc',
                'desc' => 'player_daily_history.bought desc'
            ),
            'chng' => array(
                'asc' => 'IFNULL((t.current_value - player_daily_history.value)/player_daily_history.value,0) asc',
                'desc' => 'IFNULL((t.current_value - player_daily_history.value)/player_daily_history.value,0) desc',
            ),
            'pos' => array(
                'asc' => 'basic_position_id asc',
                'desc' => 'basic_position_id desc',
            ));

        $sort->defaultOrder = array(
            'current_value' => true,
        );

        $dep = new CDbCacheDependency('SELECT SUM(current_value) FROM player');

        return new CActiveDataProvider(Player::model()->cache(21600, $dep), array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 20
                ),
                'sort' => $sort
            ));
    }

    public function statSearch() {

        $appsS = '(SELECT COUNT(gp.id) FROM game_player gp, event e WHERE gp.player_id = t.id AND e.id = gp.event_id AND e.name = \'Minutes Played\' AND gp.event_value >= 60)';
        $lowAppsS = '(SELECT COUNT(gp.id) FROM game_player gp, event e WHERE gp.player_id = t.id AND e.id = gp.event_id AND e.name = \'Minutes Played\' AND gp.event_value < 60)';
        $gsS = '(SELECT IFNULL(SUM(event_value),0) FROM game_player gp, event e WHERE gp.player_id = t.id AND e.id = gp.event_id AND e.name = \'Goals Scored\')';
        $assS = '(SELECT IFNULL(SUM(event_value),0) FROM game_player gp, event e WHERE gp.player_id = t.id AND e.id = gp.event_id AND e.name = \'Assists\')';
        $ngS = '(SELECT IFNULL(SUM(event_value),0) FROM game_player gp, event e WHERE gp.player_id = t.id AND e.id = gp.event_id AND e.name = \'No Goals\')';
        $gaS = '(SELECT IFNULL(SUM(event_value),0) FROM game_player gp, event e WHERE gp.player_id = t.id AND e.id = gp.event_id AND e.name = \'Goals Against\')';
        $csS = '(SELECT IFNULL(SUM(event_value),0) FROM game_player gp, event e WHERE gp.player_id = t.id AND e.id = gp.event_id AND e.name = \'Clean Sheet\')';
        $psS = '(SELECT IFNULL(SUM(event_value),0) FROM game_player gp, event e WHERE gp.player_id = t.id AND e.id = gp.event_id AND e.name = \'Penalties Saved\')';
        $rcS = '(SELECT IFNULL(SUM(event_value),0) FROM game_player gp, event e WHERE gp.player_id = t.id AND e.id = gp.event_id AND e.name = \'Red Cards\')';
        $ogS = '(SELECT IFNULL(SUM(event_value),0) FROM game_player gp, event e WHERE gp.player_id = t.id AND e.id = gp.event_id AND e.name = \'Own Goals\')';
        $mpS = '(SELECT IFNULL(SUM(event_value),0) FROM game_player gp, event e WHERE gp.player_id = t.id AND e.id = gp.event_id AND e.name = \'Missed Penalties\')';

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('shortname', $this->shortname, true);
        $criteria->compare('basic_position_id', $this->basic_position_id);
        $criteria->compare('team_id', $this->team_id);
        $criteria->compare('list_date', $this->list_date, true);
        $criteria->compare('delist_date', $this->delist_date, true);
        $criteria->compare('bank_shares', $this->bank_shares);
        $criteria->compare('current_value', $this->current_value, true);
        $criteria->compare('initial_value', $this->initial_value, true);
        $criteria->compare('vmi_season', $this->vmi_season, true);
        $criteria->compare('total_points', $this->total_points, true);
        $criteria->compare('is_injured', $this->is_injured);
        $criteria->compare('is_suspended', $this->is_suspended);

        $condition = 't.list_date is not null and t.list_date < now()';
        $join = 'LEFT JOIN basic_position pos ON pos.id = t.basic_position_id LEFT JOIN team te ON te.id = t.team_id';
        $params = array();

        if (isset($_GET['Player']['pos']) && $_GET['Player']['pos'] != null) {
//            $condition .= " and pos.id = :pos";
//            $params[':pos'] = $_GET['Player']['pos'];
            $criteria->addSearchCondition('pos.id', $_GET['Player']['pos'], false);
        }

        if (isset($_GET['Player']['myTeam']) && $_GET['Player']['myTeam'] != null) {
//            $condition .= " and te.id = :team";
//            $params[':team'] = $_GET['Player']['myTeam'];
            $criteria->addSearchCondition('te.id', $_GET['Player']['myTeam'], false);
        }

        $criteria->addCondition($condition);
        $criteria->join = $join;
        if (!empty($params))
            $criteria->params = $params;

        $criteria->select = "*,t.id as plId,
            $appsS AS apps,
            $lowAppsS AS low_apps,
            $gsS AS gs,
            $assS AS ass,
            $ngS AS ng,
            $gaS AS ga,
            $csS AS cs,
            $psS AS ps,
            $rcS AS rc,
            $ogS AS og,
            $mpS AS mp";

        $sort = new CSort;
        $sort->attributes = array(
            'shortname',
            'myTeam' => array(
                'asc' => 'te.name asc',
                'desc' => 'te.name desc',
            ),
            'total_points',
            'current_value',
            'pos' => array(
                'asc' => 'basic_position_id asc',
                'desc' => 'basic_position_id desc',
            ),
            'apps' => array(
                'asc' => "$appsS asc",
                'desc' => "$appsS desc",
            ),
            'low_apps' => array(
                'asc' => "$lowAppsS asc",
                'desc' => "$lowAppsS desc",
            ),
            'ng' => array(
                'asc' => "$ngS asc",
                'desc' => "$ngS desc",
            ),
            'gs' => array(
                'asc' => "$gsS asc",
                'desc' => "$gsS desc",
            ),
            'ass' => array(
                'asc' => "$assS asc",
                'desc' => "$assS desc",
            ),
            'ga' => array(
                'asc' => "$gaS asc",
                'desc' => "$gaS desc",
            ),
            'cs' => array(
                'asc' => "$csS asc",
                'desc' => "$csS desc",
            ),
            'ps' => array(
                'asc' => "$psS asc",
                'desc' => "$psS desc",
            ),
            'rc' => array(
                'asc' => "$rcS asc",
                'desc' => "$rcS desc",
            ),
            'og' => array(
                'asc' => "$ogS asc",
                'desc' => "$ogS desc",
            ),
            'mp' => array(
                'asc' => "$mpS asc",
                'desc' => "$mpS desc",
            ),
        );
        $sort->defaultOrder = 't.current_value desc';

        $dep = new CDbCacheDependency('SELECT COUNT(id) FROM game_player');

        return new CActiveDataProvider(Player::model()->cache(86400, $dep), array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 20
                ),
                'sort' => $sort
            ));
    }

    public function statusSearch() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('shortname', $this->shortname, true);
        $criteria->compare('basic_position_id', $this->basic_position_id);
        $criteria->compare('team_id', $this->team_id);
        $criteria->compare('is_injured', $this->is_injured);
        $criteria->compare('is_suspended', $this->is_suspended);

        $criteria->addCondition('list_date is not null and t.list_date < now()');

        $sort = new CSort;
        $sort->defaultOrder = 't.current_value desc';

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 20
                ),
                'sort' => $sort
            ));
    }
    
    public function searchAdmin() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

//        $criteria->compare('id', $this->id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('shortname', $this->shortname, true);
        $criteria->compare('basic_position_id', $this->basic_position_id);
        $criteria->compare('team_id', $this->team_id);
        $criteria->compare('list_date', $this->list_date, true);
        $criteria->compare('delist_date', $this->delist_date, true);
        $criteria->compare('bank_shares', $this->bank_shares);
        $criteria->compare('current_value', $this->current_value, true);
        $criteria->compare('initial_value', $this->initial_value, true);
        $criteria->compare('vmi_season', $this->vmi_season, true);
        $criteria->compare('total_points', $this->total_points, true);
        $criteria->compare('is_injured', $this->is_injured);
        $criteria->compare('is_suspended', $this->is_suspended);

        $criteria->join = ', gameconfiguration gc';
        $criteria->addCondition('t.list_date is not null');

        $sort = new CSort;
        $sort->attributes = array(
            't.name',
            'current_value',
            'vmi_season',
            'bank_shares',
            'list_date',
            'delist_date',
            'total_points'
        );
        $sort->defaultOrder = 'shortname asc';

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 50
            ),
            'sort' => $sort
        ));
    }
    
    public function searchIndex() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('shortname', $this->shortname, true);
        $criteria->compare('team_id', $this->team_id);
        $criteria->compare('basic_position_id', $this->basic_position_id);

        $criteria->join = 'LEFT JOIN basic_position bp ON bp.id = t.basic_position_id LEFT JOIN team te ON te.id = t.team_id';
        $criteria->addCondition('t.list_date is null or t.list_date > now()');

        $sort = new CSort;
        $sort->attributes = array(
            'name',
            'pos' => array(
                'asc' => 'bp.id asc',
                'desc' => 'bp.id desc',
            ),
            'myTeam' => array(
                'asc' => 'te.shortname5 asc',
                'desc' => 'te.shortname5 desc',
            ),
        );
        $sort->defaultOrder = 'name asc';

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 50
            ),
            'sort' => $sort
        ));
    }

    public function searchListed() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('shortname', $this->shortname, true);
        $criteria->compare('team_id', $this->team_id);
        $criteria->compare('basic_position_id', $this->basic_position_id);

        $criteria->join = 'LEFT JOIN basic_position bp ON bp.id = t.basic_position_id LEFT JOIN team te ON te.id = t.team_id';
        $criteria->addCondition('(list_date is not null and list_date < now()) and (delist_date is null or delist_date > now())');

        $sort = new CSort;
        $sort->attributes = array(
            'name',
            'list_date',
            'initial_value',
            'pos' => array(
                'asc' => 'bp.id asc',
                'desc' => 'bp.id desc',
            ),
            'myTeam' => array(
                'asc' => 'te.shortname5 asc',
                'desc' => 'te.shortname5 desc',
            ),
        );
        $sort->defaultOrder = 'name asc';

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 50
            ),
            'sort' => $sort
        ));
    }

    public function searchDelisted() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('shortname', $this->shortname, true);
        $criteria->compare('team_id', $this->team_id);
        $criteria->compare('basic_position_id', $this->basic_position_id);

        $criteria->join = 'LEFT JOIN basic_position bp ON bp.id = t.basic_position_id';
        $criteria->addCondition('delist_date is not null and delist_date < now()');

        $sort = new CSort;
        $sort->attributes = array(
            'name',
            'delist_date',
            'pos' => array(
                'asc' => 'bp.id asc',
                'desc' => 'bp.id desc',
            ),
        );
        $sort->defaultOrder = 'name asc';

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 50
            ),
            'sort' => $sort
        ));
    }

    /**
     * Custom find method with our own hard criteria.
     * \ includeDelisted [(false), true]
     * \ byPosition      [(false), 'GK', 'DE', 'MF', 'ST']
     * \ order           [(false), sql order clause]
     * @param array $params
     * @return type
     */
    public function findPlayers(array $params = array()) {

        $default = array(
            'includeDelisted' => false,
            'byPosition' => false,
            'excludeOwned' => false,
            'order' => false,
        );
        $params = array_merge($default, $params);

        $criteria = new CDbCriteria;
        $criteria->condition = 'list_date is not null and list_date < now()';
        $criteria->with[] = 'team';

        // handle options
        if (!$params['includeDelisted']) {
            $criteria->addCondition('delist_date is null or delist_date > now()');
        }
        if ($params['byPosition'] !== false) {
            $criteria->with[] = 'basicPosition';
            $criteria->addCondition('basicPosition.abbreviation = :pos or basicPosition.name = :pos');
            $criteria->params[':pos'] = $params['byPosition'];
        }
        if ($params['excludeOwned']) {
            $criteria->addCondition('t.id not in (select player_id from manager_team_player where manager_team_id = :mtid)');
            $criteria->params[':mtid'] = _team()->id;
        }
        if ($params['order'] !== false) {
            $criteria->order = $params['order'];
        }

        // finally return players
        return Player::model()->findAll($criteria);
    }

    public function getTotalStatsPerPlayer($id) {

        $pe = array('apps' => 0, 'low_apps' => 0, 'gs' => 0, 'ass' => 0, 'ng' => 0, 'ga' => 0, 'cs' => 0, 'ps' => 0, 'rc' => 0,
            'og' => 0, 'mp' => 0);
        $events = GamePlayer::model()->with('event')->findAll('player_id = :plId', array(':plId' => $id));

        //Get all events for player
        foreach ($events as $event) {
            switch ($event->event->name) {
                case 'Minutes Played':
                    if ($event->event_value < 60)
                        $pe['low_apps'] += 1;
                    else
                        $pe['apps'] += 1;
                    break;
                case 'Goals Scored':
                    $pe['gs'] += $event->event_value;
                    break;
                case 'Assists':
                    $pe['ass'] += $event->event_value;
                    break;
                case 'No Goals':
                    $pe['ng'] += 1;
                    break;
                case 'Goals Against':
                    $pe['ga'] += $event->event_value;
                    break;
                case 'Clean Sheet':
                    $pe['cs'] += 1;
                    break;
                case 'Penalties Saved':
                    $pe['ps'] += $event->event_value;
                    break;
                case 'Red Cards':
                    $pe['rc'] += $event->event_value;
                    break;
                case 'Own Goals':
                    $pe['og'] += $event->event_value;
                    break;
                case 'Missed Penalties':
                    $pe['mp'] += $event->event_value;
                    break;
            }
        }

        return $pe;
    }

    public function getValueChange($id, $value) {
        $ph = PlayerDailyHistory::model()->find('player_id = :id and date = (select max(date) from player_daily_history where date < date(now()))', array(':id' => $id));

        if ($ph != null)
            return (($value - $ph->value) / $ph->value);
        return 0;
    }

    /**
     * Returns true if player has been delisted
     * @return bool
     */
    public function isDelisted() {
        return Player::model()->exists('id = :pid and delist_date is not null and delist_date < now()', array(':pid' => $this->id));
    }

    public function getTop11() {
        //Sport specific code
        return Player::model()->with('team', 'basicPosition')->findAllBySql('select * from
                                                                (select * from top_gk
                                                                union
                                                                select * from top_de
                                                                union
                                                                select * from top_mf
                                                                union
                                                                select * from top_st)
                                                            as players;');
    }

    /**
     * @return string
     */
    public function small_jersey_url() {
        if ($this->team)
            return $this->team->small_jersey_url();
        return Team::noteam_small_jersey_url();
    }

    /**
     * @return string
     */
    public function large_jersey_url() {
        if ($this->team)
            return $this->team->large_jersey_url();
        return Team::noteam_large_jersey_url();
    }

    /**
     * @return string
     */
    public function small_jersey_html() {
        if ($this->team)
            return $this->team->small_jersey_html();
        return Team::noteam_small_jersey_html();
    }

    /**
     * @return string
     */
    public function large_jersey_html() {
        if ($this->team)
            return $this->team->large_jersey_html();
        return Team::noteam_large_jersey_html();
    }

    /**
     * @param string $attribute The link content.
     * @return string The link to the manager's profile page.
     */
    public function link($attribute='shortname', $plid='id') {
        return CHtml::link($this->$attribute, _url('players/view', array('plId' => $this->$plid)));
    }
    
    /**
     * Returns all unlisted players
     * @return array
     */
    public function getNewPlayers() {
        return $this->findAll(array('condition' => 'list_date is null and delist_date is null', 'order' => 'name asc'));
    }

    /**
     * Returns all uninjured players
     * @return array
     */
    public function getUninjuredPlayers() {
        return $this->findAll(array('condition' => 'is_injured is null or is_injured = 0', 'order' => 't.name asc'));
    }

    /**
     * Returns all unsuspended players
     * @return array
     */
    public function getUnsuspendedPlayers() {
        return $this->findAll(array('condition' => 'is_suspended is null or is_suspended = 0', 'order' => 'name asc'));
    }

    /**
     * Returns the name of the team along with the player in parenthesis
     * @return string
     */
    public function getConcatNameTeam() {
        $team = $this->team != null ? $this->team->name : '-';
        return $this->shortname . ' (' . $team . ')';
    }

}
