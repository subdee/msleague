<?php

/**
 * This is the model class for table "player_daily_history".
 *
 * The followings are the available columns in table 'player_daily_history':
 * @property integer $id
 * @property integer $player_id
 * @property string $value
 * @property string $date
 *
 * The followings are the available model relations:
 * @property Player $player
 */
class PlayerDailyHistory extends CActiveRecord {

    public $chng;

    /**
     * Returns the static model of the specified AR class.
     * @return PlayerDailyHistory the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'player_daily_history';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('player_id, value, date', 'required'),
            array('player_id', 'numerical', 'integerOnly' => true),
            array('value', 'length', 'max' => 9),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, player_id, value, date,player_name', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'player' => array(self::BELONGS_TO, 'Player', 'player_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'player_id' => _t('Player'),
            'value' => _t('Value'),
            'date' => _t('Date'),
            'chng' => _t('% Change'),
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->date =
                Yii::app()->localtime->fromLocalDateTime(
                    $this->date, 'Y-m-d H:i:s');
            return true;
        }
        else
            return false;
    }

    protected function afterFind() {
        $this->date =
            Yii::app()->localtime->toLocalDateTime(
                $this->date, 'Y-m-d H:i:s');

        return (parent::afterFind());
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('player_id', $this->player_id);
        $criteria->compare('value', $this->value, true);
        $criteria->compare('date', $this->date, true);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 20
            ),
            'sort' => $sort
        ));
    }

    public function getStocks($id) {
        return $this->findAllBySql('select b.* from (select a.* from player_daily_history a where player_id = :id order by a.date desc) as b order by b.date asc', array(':id' => $id));
    }

}