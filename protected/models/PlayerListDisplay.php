<?php

abstract class PlayerListDisplay {

    /**
     * 
     * @var Bool
     */
    public $isOwned = false;
    /**
     *
     * @var bool
     */
    public $isActive = true;
    /**
     *
     * @var bool
     */
    public $isWatched = false;
    /**
     *
     * @var string
     */
    public $inactivityReason = '';
    /**
     *
     * @var string
     */
    public $cssClass = '';
    /**
     *
     * @var Player
     */
    public $player;
    /**
     *
     * @var ManagerTeamPlayer
     */
    public $mtp = null;

    /**
     * Constructor
     * @param Player | ManagerTeamPlayer $player
     */
    public function __construct($player) {
        if ($player instanceof ManagerTeamPlayer) {
            $this->mtp = $player;
            $this->player = $player->player;
        } else {
            $this->player = $player;
            $this->mtp = _team()->getPlayerById($player->id);
        }
    }

    /**
     *
     * @return string
     */
    public function injuredHtml() {
        return self::sInjuredHtml($this->player);
    }

    /**
     *
     * @return string
     */
    public function suspendedHtml() {
        return self::sSuspendedHtml($this->player);
    }

    /**
     *
     * @return string
     */
    public static function sInjuredHtml(Player $player) {
        if ($player->is_injured) {
            return CHtml::image(Utils::imageUrl('playerStatus/injured16.png'), _t('Injured'), array('title' => _t('Injured')));
        }
        return '';
    }

    /**
     *
     * @return string
     */
    public static function sSuspendedHtml(Player $player) {
        if ($player->is_suspended) {
            return CHtml::image(Utils::imageUrl('playerStatus/suspended16.png'), _t('Suspended'), array('title' => _t('Suspended')));
        }
        return '';
    }

    /**
     * Returns a list of PlayerListDisplay objects to use in views
     * @param Manager $manager
     * @param array $players
     * @return array of PlayerListDisplay 
     */
    public static function create(Manager $manager, array $players, array $params = array(), $className = __CLASS__) {
        $transactions = $manager->getAvailableTransactions();

        $return = array();
        foreach ($players as $player) {
            $display = new $className($player);
            $display->setup($manager, $transactions, $params);
            $return[] = $display;
        }

        return $return;
    }

    /**
     * @param Manager $manager
     * @param int $transactions
     * @param array $params
     * @return array
     */
    abstract function setup(Manager $manager, $transactions, array $params = array());
}

?>