<?php

class PlayerListDisplayBuy extends PlayerListDisplay {
    
     public static function create(Manager $manager, array $players, array $params = array(), $className = __CLASS__) {
        return parent::create($manager, $players, $params, $className);
    }
    
    public function className() {
        return __CLASS__;
    }
    
    /**
     * @param Manager $manager
     * @param type $transactions
     * @param array $params 
     */
    public function setup(Manager $manager, $transactions, array $params = array()) {
        
        if ($manager->hasPlayer($this->player->id)) {
            $this->cssClass = 'owner';
            $this->isOwned = true;
        }
        
        if (Watchlist::model()->exists('manager_id = :id AND player_id = :pid',array(':id'=>_manager()->id,':pid'=>$this->player->id)))
            $this->isWatched = true ;
        
        $maxStockLimit = isset($params['maxStockLimit']) ? $params['maxStockLimit'] : 0;

        // for each player check if he can be baught from the current manager
        if ($manager->canBuyPlayerStocks($this->player, $transactions > 0, 1, $maxStockLimit) != PMSBuy::get()->ok()) {
            $this->cssClass .= ' inactive';
            $this->isActive = false;
            $this->inactivityReason = PMSBuy::get()->message();
        }
    }
}

?>