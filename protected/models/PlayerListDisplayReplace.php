<?php

class PlayerListDisplayReplace extends PlayerListDisplay {

    public static function create(Manager $manager, array $players, array $params = array(), $className = __CLASS__) {
        return parent::create($manager, $players, $params, $className);
    }

    /**
     * @param Manager $manager
     * @param type $transactions
     * @param array $params 
     */
    public function setup(Manager $manager, $transactions, array $params = array()) {
        // for each player check if he can be baught from the current manager
        if ($manager->canReplacePlayer($params['playerToSell'], $this->player, $transactions > 1, $params['sharesToBuy']) != PMSBuy::get()->ok()) {
            $this->cssClass .= ' inactive';
            $this->isActive = false;
            $this->inactivityReason = PMSBuy::get()->message();
        }

        if (Watchlist::model()->exists('manager_id = :id AND player_id = :pid', array(':id' => _manager()->id, ':pid' => $this->player->id))) {
            $this->isWatched = true;
        }
    }

}

?>