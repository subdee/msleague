<?php

class PlayerListDisplaySell extends PlayerListDisplay {
    
    public static function create(Manager $manager, array $players, array $params = array(), $className = __CLASS__) {
        return parent::create($manager, $players, $params, $className);
    }
    
    /**
     * @param Manager $manager
     * @param type $transactions
     * @param array $params 
     */
    public function setup(Manager $manager, $transactions, array $params = array()) {
        // for each player check if he can be baught from the current manager
        if ($manager->canSellPlayerStocks($this->mtp, $transactions) != PMSSell::get()->ok()) {
            if ($transactions < 2) {
                $this->cssClass .= ' inactive';
                $this->isActive = false;
                $this->inactivityReason = PMSSell::get()->message();
            }
        }
    }
}

?>