<?php

/**
 * This is the model class for table "player_point_history".
 *
 * The followings are the available columns in table 'player_point_history':
 * @property integer $id
 * @property integer $gameweek_id
 * @property integer $player_id
 * @property string $points
 *
 * The followings are the available model relations:
 * @property Gameweek $gameweek
 * @property Player $player
 */
class PlayerPointHistory extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return PlayerPointHistory the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'player_point_history';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('date, player_id', 'required'),
            array('player_id', 'numerical', 'integerOnly' => true),
            array('points', 'length', 'max' => 7),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, date, player_id, points', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'player' => array(self::BELONGS_TO, 'Player', 'player_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'date' => 'Date',
            'player_id' => 'Player',
            'points' => 'Points',
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->date =
                Yii::app()->localtime->fromLocalDateTime(
                    $this->date, 'Y-m-d H:i:s');
            return true;
        }
        else
            return false;
    }

    protected function afterFind() {
        $this->date =
            Yii::app()->localtime->toLocalDateTime(
                $this->date, 'Y-m-d H:i:s');

        return (parent::afterFind());
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('date', $this->date);
        $criteria->compare('player_id', $this->player_id);
        $criteria->compare('points', $this->points, true);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }

}