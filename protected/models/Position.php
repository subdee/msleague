<?php

/**
 * This is the model class for table "position".
 *
 * The followings are the available columns in table 'position':
 * @property integer $id
 * @property string $name
 * @property string $abbreviation
 * @property integer $basic_position_id
 *
 * The followings are the available model relations:
 * @property ManagerTeamPlayer[] $managerTeamPlayers
 * @property ManagerTeamPlayerHistory[] $managerTeamPlayerHistories
 * @property BasicPosition $basicPosition
 */
class Position extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Position the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'position';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('basic_position_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>45),
			array('abbreviation', 'length', 'max'=>3),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, abbreviation, basic_position_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'managerTeamPlayers' => array(self::HAS_MANY, 'ManagerTeamPlayer', 'position_id'),
			'managerTeamPlayerHistories' => array(self::HAS_MANY, 'ManagerTeamPlayerHistory', 'position_id'),
			'basicPosition' => array(self::BELONGS_TO, 'BasicPosition', 'basic_position_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'abbreviation' => 'Abbreviation',
			'basic_position_id' => 'Basic Position',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('abbreviation',$this->abbreviation,true);
		$criteria->compare('basic_position_id',$this->basic_position_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}

    /**
         * Static function that returns the id of a position when asked by name or abbreviation.
         * Note: Do not make use of the second parameter if quering by abbreviation!!
         * @param string $name
         * @param bool $isSubstitute
         * @return int
         */
    public function id( $name, $isSubstitute=false )
    {
        if( $isSubstitute ) $name = 'Substitute '. $name;

        return Position::model()->find('name=:name or abbreviation=:name', array(':name'=>$name))->id;
    }
}