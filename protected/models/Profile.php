<?php

/**
 * This is the model class for table "profile".
 *
 * The followings are the available columns in table 'profile':
 * @property integer $id
 * @property integer $manager_id
 * @property string $gender
 * @property string $birthdate
 * @property string $location
 * @property integer $country_id
 * @property string $photo
 * @property integer $team_id
 * @property string $line
 * @property integer $photo_approved
 *
 * The followings are the available model relations:
 * @property Manager $manager
 */
class Profile extends CActiveRecord {
    const GENDER_MALE = 'M';
    const GENDER_FEMALE = 'F';

    public $birthdateArray;

    /**
     * Returns the static model of the specified AR class.
     * @return Profile the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'profile';
    }

    public function afterFind() {

        $this->birthdateArray = array();

        $this->birthdateArray['year'] = Yii::app()->dateFormatter->format('y', $this->birthdate);
        $this->birthdateArray['month'] = Yii::app()->dateFormatter->format('M', $this->birthdate);
        $this->birthdateArray['day'] = Yii::app()->dateFormatter->format('d', $this->birthdate);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('manager_id', 'required'),
            array('manager_id, country_id, team_id', 'numerical', 'integerOnly' => true),
            array('gender', 'length', 'max' => 1),
            array('location, photo, line', 'length', 'max' => 45),
            array('birthdate, line, birthdateArray, file', 'safe'),
            array('birthdate', 'validatebirthdateArray', 'on' => 'search'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, manager_id, gender, birthdate, location, country_id, photo, team_id, line', 'safe', 'on' => 'search'),
            array('gender, country_id', 'required', 'on' => 'register'),
            array('country_id', 'safe', 'on' => 'register'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'manager' => array(self::BELONGS_TO, 'Manager', 'manager_id'),
            'country' => array(self::BELONGS_TO, 'Country', 'country_id'),
            'team' => array(self::BELONGS_TO, 'Team', 'team_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'manager_id' => 'Manager',
            'birthdate' => _t('Date of birth'),
            'location' => _t('Location'),
            'photo' => 'Photo',
            'team_id' => 'Team',
            'line' => _t('My catchphrase'),
            'photo_approved' => 'Photo Approved',
            'gender' => _t('Gender'),
            'country_id' => _t('Country'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('manager_id', $this->manager_id);
        $criteria->compare('gender', $this->gender, true);
        $criteria->compare('birthdate', $this->birthdate, true);
        $criteria->compare('location', $this->location, true);
        $criteria->compare('country_id', $this->country_id);
        $criteria->compare('photo', $this->photo, true);
        $criteria->compare('team_id', $this->team_id);
        $criteria->compare('line', $this->line, true);
        $criteria->compare('photo_approved', $this->photo_approved);

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
            ));
    }

    /**
     *
     * @param <type> $birthdate
     */
    public function setBirthdate($birthdate) {
        if (is_array($birthdate)) {
            $this->birthdate = $birthdate['year'] . '-' . $birthdate['month'] . '-' . $birthdate['day'];
        } elseif ($birthdate) {
            $this->birthdate = $birthdate;
        }
    }

    /**
     *
     * @param type $attribute
     * @param type $params
     */
    public function validatebirthdateArray($attribute, $params) {
        if (isset($this->birthdateArray['month'], $this->birthdateArray['day'], $this->birthdateArray['year'])) {
            if (!checkdate($this->birthdateArray['month'], $this->birthdateArray['day'], $this->birthdateArray['year']))
                $this->addError('birthdateArray_field', 'Select a valid birthdate');
        }
    }

    /**
     * @param bool $thumbnail
     * @param bool $showUnapproved
     * @return string The manager's profile photo if approved, default image otherwise
     */
    public function profilePhoto($thumbnail = false, $showUnapproved = false) {
        if ($this->photo != null) {
            if ($this->photo_approved || $showUnapproved)
                return $thumbnail ? Utils::managerThumbnailUrl($this->photo) : Utils::managerPhotoUrl($this->photo);
        }
        return $thumbnail ? Utils::imageUrl('profile/empty_profile_thumbnail.png') : Utils::imageUrl('profile/empty_profile.png');
    }

    /**
     * @return string The manager's gender in full text.
     */
    public function genderText() {
        return $this->genderToText($this->gender);
    }

    /**
     * @param string $gender
     * @return string
     */
    public function genderToText($gender) {
        if ($gender == self::GENDER_MALE)
            return _t('Male');

        return _t('Female');
    }

    /**
     * @return array
     */
    public function genderArray() {
        return array(
            self::GENDER_MALE => $this->genderToText(self::GENDER_MALE),
            self::GENDER_FEMALE => $this->genderToText(self::GENDER_FEMALE),
        );
    }

    /**
     * Creates thumbnails for all managers with photos.
     */
    public function createThumbnails() {
        $errorFree = true;

        $profiles = Profile::model()->findAll('photo is not null');
        foreach ($profiles as $profile) {

            // Thumbnail exists?
            $thumbnail = Utils::managerThumbnailsUploadDirectory() . $profile->photo;
            if (@file_exists($thumbnail)) {
                Debug::logToWindow("Thumbnail for " . strip_tags($profile->manager->username) . " exists.");
                continue;
            }

            // Get source image
            $photo = Utils::managerPhotosUploadDirectory() . $profile->photo;

            if (!@file_exists($photo)) {
                Debug::logToWindow("Photo for " . strip_tags($profile->manager->username) . " missing.");
                continue;
            }

            // Resize photo to thumbnail location
            $defaultDims = _param('profileThumbnailDefaultDimensions');
            if (!Utils::ImageResize($photo, $defaultDims['width'], $defaultDims['height'], false, $thumbnail, false)) {
                Debug::logToWindow("Thumbnail for " . strip_tags($profile->manager->username) . " failed.");
                $errorFree = false;
            }
        }

        return $errorFree;
    }

}