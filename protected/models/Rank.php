<?php

/**
 * This is the model class for table "rank".
 *
 * The followings are the available columns in table 'rank':
 * @property integer $rank
 * @property integer $manager_id
 *
 * The followings are the available model relations:
 * @property Manager $manager
 */
class Rank extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return Rank the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'rank';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('rank, manager_id', 'required'),
            array('rank, manager_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('rank, manager_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'manager' => array(self::BELONGS_TO, 'Manager', 'manager_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'rank' => _t('Rank'),
            'manager_id' => _t('Manager'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('rank', $this->rank);
        $criteria->compare('manager_id', $this->manager_id);

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 50
                ),
                'sort' => array(
                    'defaultOrder' => 'rank asc'
                )
            ));
    }

    /**
     * @return int The rank for the last manager in the league.
     */
    public static function getLowestRank() {
        $obj = Rank::model()->find(array(
            'select' => 'max(rank) as rank',
            'join' => 'join manager on id = manager_id',
            'condition' => 'total_points >= 0',
            ));

        if ($obj)
            return $obj->rank;

        // Rank table is empty
        return 1;
    }

    /**
     * @param int $manager_id Manager id
     * @return Rank A newly created rank enty
     */
    public static function giveNewTeamRank($manager_id) {

        $newrank = Rank::getLowestRank() + 1;

        // Update all ranks below the new one
        Rank::model()->updateCounters(array('rank' => 1), 'rank >= :newrank', array(':newrank' => $newrank));

        // Create new rank
        $rank = new Rank;
        $rank->manager_id = $manager_id;
        $rank->rank = $newrank;

        if (!$rank->save())
            throw new CValidateException($rank);
    }

    /**
     *
     * @param type $manager_id
     */
    public static function removeTeamFromRanks($manager) {
        // Update all ranks below the one we are removing
        Rank::model()->updateCounters(array('rank' => -1), 'rank > :newrank', array(':newrank' => $manager->rank->rank));

        // Remove enytry
        $manager->rank->delete();
    }
    
    public static function updateRanks() {
        $criteria = new CDbCriteria;
        $criteria->select = 't.id';
        $criteria->join = 'left join manager_team mt on mt.id = t.manager_team_id';
        $criteria->addInCondition('role', array(Role::MANAGER, Role::RESET));
        $criteria->order = 't.total_points desc, mt.created_on asc';
        $managerRanks = Manager::model()->findAll($criteria);
        $i = 1;
        foreach ($managerRanks as $mr) {
            $id = $mr->id;
            $rank = $mr->rank;
            if ($rank == null) {
                $rank = new Rank;
                $rank->manager_id = $mr->id;
            }
            $rank->rank = $i;
            $rank->save(false);

            ++$i;
        }
        return true;
    }
}