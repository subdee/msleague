<?php

/**
 * This is the model class for table "report".
 *
 * The followings are the available columns in table 'report':
 * @property integer $id
 * @property integer $manager_id
 * @property integer $reporter_id
 * @property integer $report_reason_id
 * @property string $date_created
 * @property string $comment
 *
 * The followings are the available model relations:
 * @property ReportReason $reportReason
 * @property Manager $manager
 * @property Message $message
 * @property Manager $reporter
 */
class Report extends CActiveRecord {
    const OK = 0;
    const ERR_UPLOADING_PICTURE = -1;
    const ERR_NO_PICTURE = -2;

    public $count;
    public $reported;

    /**
     * Returns the static model of the specified AR class.
     * @return Report the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'report';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('manager_id, reporter_id, date_created', 'required'),
            array('report_reason_id', 'required', 'message' => _t('Select a reason.')),
            array('manager_id, reporter_id, report_reason_id', 'numerical', 'integerOnly' => true),
            array('comment', 'safe'),
            array('comment', 'length', 'max' => 200),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, manager_id, reporter_id, report_reason_id, date_created, comment, report_action_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'reportReason' => array(self::BELONGS_TO, 'ReportReason', 'report_reason_id'),
            'manager' => array(self::BELONGS_TO, 'Manager', 'manager_id'),
            'reporter' => array(self::BELONGS_TO, 'Manager', 'reporter_id'),
            'reportAction' => array(self::BELONGS_TO, 'ReportAction', 'report_action_id'),
            'message' => array(self::BELONGS_TO, 'Message', 'message_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'manager_id' => 'Manager',
            'reporter_id' => 'Reporter',
            'report_reason_id' => _t('Reason'),
            'date_created' => 'Date Created',
            'comment' => _t('Comment'),
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->date_created =
                Yii::app()->localtime->fromLocalDateTime(
                $this->date_created, 'Y-m-d H:i:s');
            return true;
        }
        else
            return false;
    }

    protected function afterFind() {
        $this->date_created =
            Yii::app()->localtime->toLocalDateTime(
            $this->date_created, 'Y-m-d H:i:s');

        return (parent::afterFind());
    }

    public function afterValidate() {
        parent::afterValidate();
        if (!$this->hasErrors()) {
            if (empty($this->comment) && ($this->reportReason->reason_type == ReportReason::OTHER))
                $this->addError('comment', _t('You must describe the reason you are reporting this user.'));

            if ($this->reportReason->reason_type == ReportReason::PHOTO) {
                if (Manager::model()->findByPk($this->manager_id)->profile->photo == null)
                    $this->addError('report_reason_id', _t('This user does not have a profile photo.'));
            }

            if ($this->reportReason->reason_type == ReportReason::STATUS) {
                if (Manager::model()->findByPk($this->manager_id)->profile->line == null)
                    $this->addError('report_reason_id', _t('This user has not posted a personal message.'));
            }
        }
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('manager_id', $this->manager_id);
        $criteria->compare('reporter_id', $this->reporter_id);
        $criteria->compare('report_reason_id', $this->reportReason, true);
        $criteria->compare('date_created', $this->date_created, true);
        $criteria->compare('m.username', $this->reported, true);

        $criteria->select = 'm.id as id, m.username as reported, count(t.id) as count';
        $criteria->join = 'left join manager m on m.id = t.manager_id';
        $criteria->group = 'm.username';
        $criteria->order = 'count(t.id) desc';

        if (isset($_GET['Report']['reported']))
            $criteria->addSearchCondition('m.username', $_GET['Report']['reported']);

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 20
                ),
                'sort' => array(
                    'defaultOrder' => 'date_created desc'
                )
            ));
    }

    /**
     *
     * @param type $label
     * @param type $manager_id
     * @param type $reasons
     * @param type $params
     * @param type $htmlOptions
     * @return type
     */
    public function reportLink($label, $manager_id, $reasons, $type, $params = array(), $htmlOptions = array()) {
        return CHtml::link($label, _url('report/index', array(
                    'id' => $manager_id,
                    'returnUrl' => _app()->request->url,
                    'reasons' => $reasons,
                    'params' => $params,
                    'type' => $type,
                )), $htmlOptions
        );
    }

    /**
     * Return an array with all the reports of a manager
     * @param integer $id
     * @return array
     */
    public function getCountsForManager($id) {
        $counts = array();
        $counts['Username'] = Report::model()->with('reportReason')->count('manager_id = :id AND reportReason.reason_type = :type', array(':id' => $id, ':type' => ReportReason::USERNAME));
        $counts['Team Name'] = Report::model()->with('reportReason')->count('manager_id = :id AND reportReason.reason_type = :type', array(':id' => $id, ':type' => ReportReason::TEAM_NAME));
        $counts['Photo'] = Report::model()->with('reportReason')->count('manager_id = :id AND reportReason.reason_type = :type', array(':id' => $id, ':type' => ReportReason::PHOTO));
        $counts['Status'] = Report::model()->with('reportReason')->count('manager_id = :id AND reportReason.reason_type = :type', array(':id' => $id, ':type' => ReportReason::STATUS));
        $counts['Message'] = Report::model()->with('reportReason')->count('manager_id = :id AND reportReason.reason_type = :type', array(':id' => $id, ':type' => ReportReason::MESSAGE));
        $counts['Cheating'] = Report::model()->with('reportReason')->count('manager_id = :id AND reportReason.reason_type = :type', array(':id' => $id, ':type' => ReportReason::CHEATING));
        $counts['Other'] = Report::model()->with('reportReason')->count('manager_id = :id AND reportReason.reason_type = :type', array(':id' => $id, ':type' => ReportReason::OTHER));

        return $counts;
    }

    public function deleteReport($type, $manager_id, $sid=false) {
        if ($sid) {
            $reports = Report::model()->findAll(array(
                'join' => 'left join report_reason rr on rr.id = t.report_reason_id left join mod_forum_comments_reported comm on comm.report_id = t.id',
                'condition' => 'manager_id = :id AND rr.reason_type = :type AND comm.comment_id = :sid',
                'params' => array(':id' => $manager_id, ':type' => $type, ':sid' => $sid)
                ));
        } else {
            $reports = Report::model()->findAll(array(
                'join' => 'left join report_reason rr on rr.id = t.report_reason_id',
                'condition' => 'manager_id = :id AND rr.reason_type = :type',
                'params' => array(':id' => $manager_id, ':type' => $type)
                ));
        }

        foreach ($reports as $report)
            $report->delete();

        return 1;
    }

}
