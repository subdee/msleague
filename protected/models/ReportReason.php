<?php

/**
 * This is the model class for table "report_reason".
 *
 * The followings are the available columns in table 'report_reason':
 * @property integer $id
 * @property string $reason
 * @property integer $language_id
 * @property integer $reason_type
 *
 * The followings are the available model relations:
 * @property Ban[] $bans
 * @property Report[] $reports
 * @property Language $language
 * @property ReportAction[] $reportActions
 */
class ReportReason extends CActiveRecord {

    const USERNAME = 1;
    const TEAM_NAME = 2;
    const PHOTO = 3;
    const STATUS = 4;
    const MESSAGE = 5;
    const LEAGUE_NAME = 6;
    const DELETE = 7;
    const CHEATING = 8;
    const OTHER = 9;
    
    public $reportAction;

    /**
     * Returns the static model of the specified AR class.
     * @return ReportReason the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'report_reason';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('reason, language_id,reason_type,reportAction', 'required'),
            array('language_id,reason_type', 'numerical', 'integerOnly' => true),
            array('reason', 'length', 'max' => 100),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('reportAction', 'safe'),
            array('id, reason, language_id,reason_type', 'safe', 'on' => 'search'),
        );
    }

    protected function afterSave() {
        if (isset($this->reportAction)) {
        foreach ($this->reportAction as $action) {
            $reportAction = new ReportReasonAction;
            $reportAction->report_action_id = $action;
            $reportAction->report_reason_id = $this->id;
            if (!$reportAction->save())
                return false;
        }
        }

        if ($this->reason_type != self::OTHER)
            ReportReason::model()->updateCounters(array('display_order' => 1), 'reason_type = :type', array(':type' => self::OTHER));

        return parent::afterSave();
    }

    protected function beforeSave() {
        parent::beforeSave();
        $order = ReportReason::model()->find(array(
            'condition' => 'reason_type = :type',
            'params' => array(':type' => self::OTHER),
            'order' => 'display_order desc'
            ));
        if ($order) {
            if ($this->reason_type == self::OTHER)
                $this->display_order = $order->display_order + 1;
            else
                $this->display_order = $order->display_order;
        }else {
            $order = ReportReason::model()->find(array(
                'order' => 'display_order desc'
                ));
            if ($order)
                $this->display_order = $order->display_order + 1;
            else
                $this->display_order = 1;
        }
        
        return true;
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'bans' => array(self::HAS_MANY, 'Ban', 'report_reason_id'),
            'reports' => array(self::HAS_MANY, 'Report', 'report_reason_id'),
            'language' => array(self::BELONGS_TO, 'Language', 'language_id'),
            'reportActions' => array(self::MANY_MANY, 'ReportAction', 'report_reason_action(report_reason_id, report_action_id)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'reason' => 'Reason',
            'language_id' => 'Language',
            'reason_type' => 'Reason category',
            'reportActions' => 'Reasons appear in',
            'reportAction' => 'Reasons appear in'
        );
    }

    public function scopes() {
        return array(
            'reportTypes' => array(
                'condition' => 'reason_type != :type',
                'params' => array(':type' => self::DELETE)
            )
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

//        $criteria->compare('id', $this->id);
//        $criteria->compare('reason', $this->reason, true);
//        $criteria->compare('language_id', $this->language_id);
//        $criteria->compare('reason_type', $this->reason_type);
        $criteria->order = 'display_order asc';

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
    }
    
    public function getReportActions() {
        if ($this) {
            $actions = '';
            foreach ($this->reportActions as $action) {
                $actions .= $action->action . ', ';
        }
            return $actions;
    }
        return false;
    }

    public static function getTypes() {
        return array(
            self::USERNAME => 'Username',
            self::TEAM_NAME => 'Team Name',
            self::PHOTO => 'Photo',
            self::STATUS => 'Status',
            self::MESSAGE => 'Message',
            self::LEAGUE_NAME => 'League Name',
            self::CHEATING => 'Cheating',
            self::DELETE => 'Delete',
            self::OTHER => 'Other',
        );
}
    
    public static function getType($type) {
        $types = self::getTypes();
        return $types[$type];
    }
    
    public static function getReasons($action) {
        $reasons = array();
        $reasonsDb = ReportReason::model()->with('reportActions')->findAll(array(
            'condition' => 'reportActions.action = :action',
            'params' => array(':action' => $action),
            'order' => 'display_order asc'
        ));
        foreach ($reasonsDb as $reason) {
            $reasons[$reason->id] = $reason->reason;
        }
        return $reasons;
    }

}