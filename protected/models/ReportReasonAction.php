<?php

/**
 * This is the model class for table "report_reason_action".
 *
 * The followings are the available columns in table 'report_reason_action':
 * @property integer $report_reason_id
 * @property integer $report_action_id
 */
class ReportReasonAction extends CActiveRecord {

    public function primaryKey() {
        return array('report_reason_id','report_action_id');
    }

    /**
     * Returns the static model of the specified AR class.
     * @return ReportReasonAction the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'report_reason_action';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('report_reason_id, report_action_id', 'required'),
            array('report_reason_id, report_action_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('report_reason_id, report_action_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'report_reason_id' => 'Report Reason',
            'report_action_id' => 'Report Action',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('report_reason_id', $this->report_reason_id);
        $criteria->compare('report_action_id', $this->report_action_id);

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
    }

}