<?php

/**
 *
 */
class Role {
    /**
     * Inactive user has never logged in.
     */
    const INACTIVE = 1;

    /**
     * Temporary user has not created a team yet.
     */
    const TEMPORARY = 2;

    /**
     * A manager has a team and plays the game.
     */
    const MANAGER = 4;

    /**
     * Resetted manager after a round has finished.
     */
    const RESET = 8;

    /**
     * @param Manager $manager
     * @param int $role
     * @return bool
     */
    public static function is($manager, $role) {

        if ($manager) {
            if (($role & $manager->role) == $manager->role)
                return true;
        }
        return false;
    }

    /**
     * @param int $role
     * @return string The manager role in text form.
     */
    public static function toText($role) {
        switch ($role) {
            case self::INACTIVE: return _t('Inactive');
            case self::TEMPORARY: return _t('Temporary');
            case self::MANAGER: return _t('Manager');
            case self::RESET: return _t('Reset');
        }
        return '';
    }
    
    public static function toArray() {
        return array(
            self::INACTIVE => self::toText(self::INACTIVE),
            self::TEMPORARY => self::toText(self::TEMPORARY),
            self::MANAGER => self::toText(self::MANAGER),
            self::RESET => self::toText(self::RESET),
        );
    }

}