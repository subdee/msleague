<?php

/**
 * This is the model class for table "script_configuration".
 *
 * The followings are the available columns in table 'script_configuration':
 * @property integer $main_script_interval
 * @property string $daily_script_time
 * @property float formula_coefficient
 */
class ScriptConfiguration extends CActiveRecord {

    private $config = null;

    /**
     * Returns the static model of the specified AR class.
     * @return ScriptConfiguration the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'script_configuration';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('main_script_interval', 'numerical', 'integerOnly' => true),
            array('formula_coefficient', 'type', 'type' => 'float'),
            array('daily_script_time, newsletter_time', 'length', 'max' => 5),
            array('main_script_interval', 'divBy24'),
            array('daily_script_time, newsletter_time', 'validHour'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('main_script_interval, daily_script_time,formula_coefficient', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'main_script_interval' => 'Main Script Interval (in hours)',
            'daily_script_time' => 'Daily Script Time',
            'newsletter_time' => 'Reminder Time'
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->last_daily_run = Utils::fromLocalDatetime($this->last_daily_run);
            return true;
        }
        return false;
    }

    protected function afterFind() {
        $this->last_daily_run = Utils::toLocalDatetime($this->last_daily_run);
        return (parent::afterFind());
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('main_script_interval', $this->main_script_interval);
        $criteria->compare('daily_script_time', $this->daily_script_time, true);

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
            ));
    }

    public function divBy24($attribute, $params) {
        if (24 % $this->main_script_interval > 0) {
            $this->addError('main_script_interval', 'The interval must be divisible by 24');
            return false;
        }
        return true;
    }

    public function validHour($attribute, $params) {
        if (preg_match('/\d|1[0-9]|2[0-3]:\d{2}/', $this->daily_script_time))
            return true;
        $this->addError('daily_script_time', 'The time must be in the format of hh:mm with no leading zeros');
        return false;
    }

    public function get($key) {
        if (null == $this->config) {
            $this->config = $this->find();
        }
        return $this->config->$key;
    }

}