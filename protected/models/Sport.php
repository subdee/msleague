<?php


/**
 * @author subdee
 * @param string $formation
 * @return bool
 */
interface Sport
{
    public function changeFormation($formation);
    public function calculatePoints($players,$gameweek);
}
?>
