<?php

/**
 * This is the model class for table "static_content".
 *
 * The followings are the available columns in table 'static_content':
 * @property integer $id
 * @property string $section
 * @property string $content
 * @property integer $lang
 *
 * The followings are the available model relations:
 * @property Language $lang0
 */
class StaticContent extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return StaticContent the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'static_content';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('section, content', 'required'),
            array('lang', 'numerical', 'integerOnly' => true),
            array('section', 'length', 'max' => 50),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, section, content, lang', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'lang0' => array(self::BELONGS_TO, 'Language', 'lang'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'section' => 'Section',
            'content' => 'Content',
            'lang' => 'Lang',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('section', $this->section, true);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('lang', $this->lang);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getText($section) {
        $dependency = new CDbCacheDependency('SELECT MAX(last_updated) FROM static_content');
        $content = $this->cache(2592000,$dependency)->find(array(
            'select' => 'content',
            'condition' => 'lang = :lang AND section = :section',
            'params' => array(':lang' => Language::model()->id(_app()->language), ':section' => $section)
        ))->content;
        
        return Utils::replaceVars($content);
    }

}