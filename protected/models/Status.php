<?php

/**
 * This is the model class for table "status".
 *
 * The followings are the available columns in table 'status':
 * @property integer $id
 * @property string $status
 * @property string $coefficient
 *
 * The followings are the available model relations:
 * @property ManagerTeamPlayer[] $managerTeamPlayers
 * @property ManagerTeamPlayerHistory[] $managerTeamPlayerHistories
 */
class Status extends CActiveRecord {
    const STARTER = 1;
    const SUBSTITUTE = 2;
    const LEADER = 4;

    private $_flags;

    /**
     * Returns the static model of the specified AR class.
     * @return Status the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'status';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('status, coefficient', 'required'),
            array('status', 'length', 'max' => 45),
            array('coefficient', 'length', 'max' => 4),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, status, coefficient', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'managerTeamPlayers' => array(self::HAS_MANY, 'ManagerTeamPlayer', 'status_id'),
            'managerTeamPlayerHistories' => array(self::HAS_MANY, 'ManagerTeamPlayerHistory', 'status_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'status' => 'Status',
            'coefficient' => 'Coefficient',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('coefficient', $this->coefficient, true);

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
            ));
    }

    protected function afterFind() {
        parent::afterFind();

        switch ($this->status) {
            case 'Leader':
                $this->_flags = self::STARTER | self::LEADER;
                break;
            case 'Starter':
                $this->_flags = self::STARTER;
                break;
            case 'Substitute':
                $this->_flags = self::SUBSTITUTE;
                break;
        }
    }

    /**
     * Returns the id of a status when asked by status name.
     * @param string $name
     * @return int
     */
    public static function id($name) {
        return Status::model()->findByAttributes(array('status' => $name))->id;
    }

    /**
     * Returns an array with which we can access all statii by status name.
     * @return array
     */
    public static function all() {
        $ret = array();
        $statii = Status::model()->findAll();
        foreach ($statii as $s) {
            $ret[$s->status] = array('id' => $s->id, 'coefficient' => $s->coefficient);
        }
        return $ret;
    }

    /**
     * @return bool Whether the player is at starting team.
     */
    public function isStarter() {
        return $this->_flags & self::STARTER;
    }

    /**
     * @return bool Whether the player is benched.
     */
    public function isSubstitute() {
        return $this->_flags & self::SUBSTITUTE;
    }

    /**
     * @return bool Whether the player is the team leader.
     */
    public function isLeader() {

        if (($this->_flags & self::LEADER) && ($this->_flags & self::SUBSTITUTE)) {
            Debug::logToWindow("A substitute player should not be leader (flags: {$this->_flags})");
        }

        return $this->_flags & self::LEADER;
    }

}