<?php

/**
 * This is the model class for table "team".
 *
 * The followings are the available columns in table 'team':
 * @property integer $id
 * @property string $name
 * @property string $shortname15
 * @property string $shortname5
 * @property integer $country_id
 * @property string $jersey_small
 * @property string $jersey_large
 *
 * The followings are the available model relations:
 * @property Game[] $games
 * @property Manager[] $managers
 * @property Player[] $players
 * @property Country $country
 */
class Team extends CActiveRecord {

    public $file_small;
    public $file_large;
    public $fans;

    /**
     * Returns the static model of the specified AR class.
     * @return Team the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'team';
    }

    public function beforeSave() {
        if ($this->file_small != null)
            $this->jersey_small = 'small/' . $this->file_small;
        if ($this->file_large != null)
            $this->jersey_large = 'large/' . $this->file_large;
        return true;
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, shortname15, shortname5', 'required'),
            array('name,shortname15,shortname5,xml_id','unique'),
            array('country_id, xml_id', 'numerical', 'integerOnly' => true),
            array('name, jersey_small, jersey_large', 'length', 'max' => 45),
            array('shortname15', 'length', 'max' => 15),
            array('shortname5', 'length', 'max' => 5),
            array('file_small,file_large', 'file',
                'types' => 'jpg,gif,png',
                'maxSize' => 10240,
                'tooLarge' => _t('File was too large'),
                'allowEmpty' => true
            ),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, shortname15, shortname5, country_id, jersey_small, jersey_large, xml_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'gamesHome' => array(self::HAS_MANY, 'Game', 'team_home'),
            'gamesAway' => array(self::HAS_MANY, 'Game', 'team_away'),
            'games' => array(self::HAS_MANY, 'Game', 'team_home'),
            'managers' => array(self::HAS_MANY, 'Manager', 'team_id'),
            'players' => array(self::HAS_MANY, 'Player', 'team_id'),
            'country' => array(self::BELONGS_TO, 'Country', 'country_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => _t('Team'),
            'shortname15' => _t('Team'),
            'shortname5' => _t('Team'),
            'country_id' => 'Country',
            'jersey_small' => 'Jersey Small',
            'jersey_large' => 'Jersey Large',
            'fans' => _t('Fans'),
            'file_small' => 'Jersey Small',
            'file_large' => 'Jersey Large',
            'xml_id' => 'XML ID'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;


        $criteria->select = "jersey_small, name, (
            SELECT count(p.team_id)
              FROM profile p
         LEFT JOIN manager m ON m.id = p.manager_id
             WHERE t.id = p.team_id AND role not in (:role1, :role2)) as fans";
        $criteria->params = array(
            ':role1' => Role::INACTIVE,
            ':role2' => Role::TEMPORARY,
        );
        $criteria->order = 'fans DESC';

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 100
                )
            ));
    }

    public function small_jersey_url() {
        return Utils::imageUrl('jerseys/' . $this->jersey_small);
    }

    public function large_jersey_url() {
        return Utils::imageUrl('jerseys/' . $this->jersey_large);
    }

    public function small_jersey_html() {
        return CHtml::image($this->small_jersey_url(), '', array('title' => $this->name));
    }

    public function large_jersey_html() {
        return CHtml::image($this->large_jersey_url(), '', array('title' => $this->name));
    }

    public static function noteam_small_jersey_url() {
        return Utils::imageUrl('jerseys/small/noteam.png');
    }

    public static function noteam_large_jersey_url() {
        return Utils::imageUrl('jerseys/large/noteam.png');
    }

    public static function noteam_small_jersey_html() {
        return CHtml::image(Team::noteam_small_jersey_url(), '', array('title' => _t('No team')));
    }

    public static function noteam_large_jersey_html() {
        return CHtml::image(Team::noteam_large_jersey_url(), '', array('title' => _t('No team')));
    }

}