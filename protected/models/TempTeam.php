<?php

/**
 * TempTeam class.
 * TempTeam is the data structure for creating a new team
 */
class TempTeam extends CFormModel {

    public $teamName;
    public $favTeamId;

    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            array('favTeamId', 'numerical', 'integerOnly' => true),
            array('teamName', 'required', 'message' => _t('You must enter a name for your team.')),
            array('favTeamId', 'required', 'message' => _t('You must select a favorite team.')),
            array('teamName', 'isAllowed'),
//            array('teamName', 'type', 'type'=>'string', 'message'=>_t('Team name cannot contain special characters')),
            array('teamName', 'match', 'pattern' => '/^[a-zA-Z0-9_\s]{4,30}$/', 'message' => 'The team name must be bewteen 4 and 30 characters (letters, numbers, spaces and underscores).'),
            array('teamName', 'unique', 'className' => 'ManagerTeam', 'attributeName' => 'name', 'message' => _t('That name is already being used')),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'teamName' => _t('Enter the name of your team'),
            'favTeamId' => _t('Select your favorite team'),
        );
    }

    public function isAllowed($attribute, $params) {
        if (!Utils::isPhraseAllowed($this->teamName)) {
            $this->addError($attribute, _t('This team name is not allowed.'));
        }
    }

    public function create() {
        if (!$this->validate()) {
            Debug::logToWindow($this->getErrors(), 'create team validation');
            return false;
        }

        $transaction = _app()->db->beginTransaction();
        try {
            // update manager team name
            _team()->name = $this->teamName;
            _team()->saveAttributes(array('name'));

            // update manager's favorite team
            $profile = _manager()->profile;
            $profile->team_id = $this->favTeamId;
            $profile->saveAttributes(array('team_id'));

            Utils::triggerEvent('onAfterCreateTeamName', $this);

            $transaction->commit();
            _manager()->refresh();

            return true;
        } catch (CDBException $e) {
            $transaction->rollback();
            Debug::logToWindow($e->getMessage(), 'create team db');
        }

        return false;
    }

}
