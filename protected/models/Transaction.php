<?php

/**
 * This is the model class for table "transaction".
 *
 * The followings are the available columns in table 'transaction':
 * @property integer $id
 * @property integer $manager_id
 * @property string $date_completed
 * @property integer $player_id
 * @property integer $shares
 * @property string $price_share
 * @property string $type_of_transaction
 * @property string $coefficient
 *
 * The followings are the available model relations:
 * @property Player $player
 * @property Manager $manager
 */
class Transaction extends CActiveRecord {

    public $total_price;
    public $username;
    public $playername;
    public $from;
    public $to;

    /**
     * Returns the static model of the specified AR class.
     * @return Transaction the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'transaction';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('manager_id, date_completed,  player_id, shares, price_share, type_of_transaction', 'required'),
            array('manager_id,  player_id, shares', 'numerical', 'integerOnly' => true),
            array('price_share', 'length', 'max' => 9),
            array('type_of_transaction', 'length', 'max' => 4),
            array('coefficient', 'length', 'max' => 3),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, manager_id, date_completed,  player_id, shares, price_share, type_of_transaction, coefficient, username, playername, total_price', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'player' => array(self::BELONGS_TO, 'Player', 'player_id'),
            'manager' => array(self::BELONGS_TO, 'Manager', 'manager_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'manager_id' => 'Manager',
            'date_completed' => 'Date Completed',
            'player_id' => 'Player',
            'shares' => 'Shares',
            'price_share' => 'Price Share',
            'type_of_transaction' => 'Type of Transaction',
            'coefficient' => 'Coefficient',
            'username' => 'Manager',
            'playername' => 'Player'
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->date_completed =
                Yii::app()->localtime->fromLocalDateTime(
                    $this->date_completed, 'Y-m-d H:i:s');
            return true;
        }
        else
            return false;
    }

    protected function afterFind() {
        $this->date_completed =
            Yii::app()->localtime->toLocalDateTime(
                $this->date_completed, 'Y-m-d H:i:s');

        return (parent::afterFind());
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('manager_id', $this->manager_id);
        $criteria->compare('date_completed', $this->date_completed, true);
        $criteria->compare('player_id', $this->player_id);
        $criteria->compare('shares', $this->shares);
        $criteria->compare('price_share', $this->price_share, true);
        $criteria->compare('type_of_transaction', $this->type_of_transaction, true);
        $criteria->compare('coefficient', $this->coefficient, true);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }
    
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchAdmin() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('manager_id', $this->manager_id);
        $criteria->compare('player_id', $this->player_id);
        $criteria->compare('m.username', $this->username,true);
        $criteria->compare('player_id', $this->playername);

        $criteria->select = '*, shares*price_share as total_price';
        $criteria->join = 'LEFT JOIN manager m ON m.id = t.manager_id LEFT JOIN player p on p.id = t.player_id';

        $sort = new CSort;
        $sort->attributes = array(
            'username' => array(
                'asc' => 'm.username asc',
                'desc' => 'm.username desc',
            ),
            'playername' => array(
                'asc' => 'p.shortname asc',
                'desc' => 'p.shortname desc',
            ),
            'date_completed',
            'type_of_transaction',
            'shares',
            'price_share',
            'is_transfer',
            'total_price' => array(
                'asc' => 'shares*price_share asc',
                'desc' => 'shares*price_share desc'
            )
        );
        $sort->defaultOrder = 't.id desc';

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 50
                ),
                'sort' => $sort
            ));
    }

    public function behaviors() {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                'defaults' => array(), /* optional line */
                'defaultStickOnClear' => false /* optional line */
            ),
        );
    }

    public function getTransactionVolumePerPlayer($id) {

        $connection = _app()->db;
        $command = $connection->createCommand('
            select sum(shares * price_share) as volume from transaction where player_id = :id and date(date_completed) = date(now())
            ');

        $command->bindValue(':id', $id);

        $data = $command->queryRow();
        return $data;
    }

    /**
     *
     * @param int $id
     * @param int $limit
     * @return array of transactions 
     */
    public function getManagerTransactions($id, $limit=0) {
        $maxLimit = Gameconfiguration::model()->get('max_transactions_per_day');
        if ($limit == 0 || $limit > $maxLimit)
            $limit = $maxLimit;

        $criteria = new CDbCriteria;
        $criteria->condition = 't.manager_id = :id';
        $criteria->params = array(':id' => $id);
        $criteria->order = 't.date_completed desc';
        $criteria->limit = $limit;
        return $this->findAll($criteria);
    }

}