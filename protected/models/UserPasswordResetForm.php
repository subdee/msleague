<?php

/**
 * UserPasswordResetForm class.
 */
class UserPasswordResetForm extends CFormModel {

    // forgot password scenario
    public $email;
    public $verifyCode;
    // reset password scenario
    public $password;
    public $password2;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            // required fields
            array('email', 'required', 'on' => 'forgot'),
            array('password, password2', 'required', 'on' => 'reset'),
            // password regex check
//            array('password', 'match', 'pattern' => '/^(?=^.{6,}$)((?=.*[A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z]))^.*$/', 'on' => 'reset'),
            // emails and passwords need to be compared with each other
            array('password2', 'compare', 'compareAttribute' => 'password', 'on' => 'reset'),
            // emails need to be checked
            array('email', 'email', 'on' => 'forgot', 'message' => _t('This email is not valid.')),
            // captcha needs to be checked
            array('verifyCode', 'captcha', 'on' => 'forgot'),
            array('manager_id', 'safe'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'password' => _t('New password'),
            'password2' => _t('Confirm new password'),
            'email' => _t('Email'),
            'verifyCode' => _t('Verification Code'),
        );
    }

    private function cancel() {
        $this->password = null;
        $this->password2 = null;
        $this->verifyCode = null;
    }

    /**
     * Check if the email exists and send a message there with instructions of reseting the password.
     */
    public function sendResetEmail() {
        if (!$this->validate()) {
            $this->cancel();
            return false;
        }

        $manager = Manager::model()->findByAttributes(
            array('email' => $this->email), 'role != :role', array(':role' => Role::INACTIVE)
        );
        
        if (!$manager) {
            Utils::notice('passreset', _t('User does not exist.'), 'error');
            $this->cancel();
            return false;
        }

        // create activation code
        $activation_code = $manager->createActivationCode(array('resetPassword'));
        
        // create activation link
        $activation_link = _app()->createAbsoluteUrl('passwordReset/reset', array('email' => $manager->email, 'code' => $activation_code));

        $trans = _app()->db->beginTransaction();
        try {
            $manager->email_code = $activation_code;
            if (!$manager->save(true, array('email_code')))
                throw new CValidateException($manager);

            if (!$manager->passwordReset) {
                $manager->passwordReset = new ManagerPasswordReset;
                $manager->passwordReset->manager_id = $manager->id;
            }
            $manager->passwordReset->timeout = _app()->localtime->getLocalNow();
            if (!$manager->passwordReset->save())
                throw new CValidateException($manager->passwordReset);

            $trans->commit();
        } catch (CDbException $e) {
            Debug::logToWindow($e->getMessage());
            Utils::notice('passreset', _t('Failed to save activation code.'), 'error');
            $trans->rollback();
            $this->cancel();
            return false;
        } catch (CValidateException $e) {
            Debug::logToWindow($e->getMessage());
            Utils::notice('passreset', _t('Failed to save activation code.'), 'error');
            $trans->rollback();
            $this->cancel();
            return false;
        }

        // create email body
        $body = _controller()->renderPartial('_resetEmail', array(
            'username' => $manager->username,
            'activation_link' => $activation_link
            ), true);

        // send reset email
        if (!Utils::sendEmail($this->email, _t('Reset password'), $body)) {
            Utils::notice('passreset', _t('Failed to send email.'), 'error');
            Debug::notice('passdebug', $body, 'error');
        }

        return true;
    }

    /**
     * 
     */
    public function resetPassword($manager) {
        if (!$this->validate()) {
            $this->cancel();
            return false;
        }

        $trans = _app()->db->beginTransaction();
        try {
            $manager->setPassword($this->password);
            $manager->email_code = null;
            $manager->save(true, array('password', 'email_code'));
            
            $manager->passwordReset->delete();
                
            $trans->commit();
        } catch(CDbException $e) {
            Utils::notice('passreset', _t('Failed to reset password'), 'warning');
            Debug::logToWindow($e->getMessage());
            $this->cancel();
            $trans->rollback();
            return false;
        }
        return true;
    }

}
