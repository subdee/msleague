<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class UserRegistrationForm extends CFormModel {

    public $manager;
    public $verifyCode;
    public $tac; // Terms and conditions
    // sms module specific. 
    // TODO: get them outta here.
    public $countryCode;
    public $newsletter;
    public $mobile;
    public $fullMobile;

    public function beforeValidate() {
        $this->fullMobile = $this->countryCode . $this->mobile;
        return parent::beforeValidate();
    }

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        $rules = array(
            array('tac', 'required'),
            array('tac', 'boolean'),
            array('tac', 'compare', 'compareValue' => 1, 'message' => _t('You must read and agree with the Game Rules and Terms of Use')),
            array('verifyCode', 'CaptchaExtendedValidator','allowEmpty'=>!CCaptcha::checkRequirements(), 'message' => _t('The result you have entered is not correct')),
        );

        if (_isModuleOn('sms')) {
            array_push($rules, array('countryCode', 'match', 'pattern' => '/^[0-9]{2,4}/', 'message' => _t('The country code number must be numeric.')));
            array_push($rules, array('fullMobile', 'unique', 'className' => 'Mobile', 'attributeName' => 'mobile', 'message' => _t('This number has already been used for confirmation purposes')));
            array_push($rules, array('mobile, countryCode', 'required'));
            array_push($rules, array('mobile', 'match', 'pattern' => '/^[0-9]{10,16}/', 'message' => _t('Your mobile number must be at least 10 digits long.')));
        }
        return $rules;
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'verifyCode' => _t('Formula Result'),
            'tac' => _t('Terms of Use'),
            'mobile' => _t('Mobile number'),
        );
    }

    /**
     * 
     */
    public function init() {
        parent::init();
        $this->manager = new Manager('register');
        $this->manager->profile = new Profile('register');
    }

    /**
     *
     */
    public function register($invitation_id) {

        // read game configuration
        $config = Gameconfiguration::model()->find();

        // open database transaction
        $transaction = _app()->db->beginTransaction();

        try {
            // create manager team
            $managerTeam = ManagerTeam::createNew();
            if (!$managerTeam->save())
                throw new CValidateException($managerTeam);

            // create manager
            $this->manager->role = Role::INACTIVE;
            $this->manager->setPassword($this->manager->password);
            $this->manager->registration_date = _app()->localtime->getLocalNow();
            $this->manager->portfolio_cash = $config->manager_initial_budget;
            $this->manager->last_known_ip = Yii::app()->request->userHostAddress;
            $this->manager->total_points = 0;
            $this->manager->manager_team_id = $managerTeam->id;
            $this->manager->email_code = $this->manager->createActivationCode();
            $this->manager->accepted_terms = true;
            
            // for validation purposes
            $this->manager->password_repeat = $this->manager->password;

            if (!$this->manager->save())
                throw new CValidateException($this->manager);

            // create manager profile
            $this->manager->profile->manager_id = $this->manager->id;
            if (!$this->manager->profile->save())
                throw new CValidateException($this->manager->profile);

            // TODO: this should be replaced with a callback event.
            if (_isModuleOn('sms')) {
                $mobile = new Mobile;
                $mobile->manager_id = $this->manager->id;
                $mobile->mobile = $this->fullMobile;
                $mobile->activation_code = null;
                if (!$mobile->save())
                    throw new CValidateException($mobile);
            }

            $transaction->commit();
        } catch (CDbException $e) {
            $transaction->rollback();
            Utils::notice('notice', $e->getMessage(), 'error');
            Debug::logToWindow(Debug::exceptionDetailString($e), 'register database exception');
            return false;
        } catch (CException $e) {
            $transaction->rollback();
            Utils::notice('notice', $e->getMessage(), 'error');
            Debug::logToWindow($e->getMessage(), 'register exception');
            return false;
        } catch (CValidateException $e) {
            $transaction->rollback();
            return false;
        }

        // create activation link
        $activation_link = _app()->createAbsoluteUrl('user/verifyEmail', array('uid' => $this->manager->id, 'code' => $this->manager->email_code));

        // render activation email template
        $tpl = _controller()->renderPartial('_activationEmail', array(
            'username' => $this->manager->username,
            'activation_link' => $activation_link,
            ), true);

        // send activation email
        if (Utils::sendEmail($this->manager->email, _t('Registration'), $tpl)) {
            Utils::notice('notice', _t('You have successfully registered.  Check your email.'), 'success');
        } else {
            Utils::notice('notice', _t('Failed to send activation email. Please contact us at {support}', array('{support}' => _support())), 'error');
        }
        Debug::notice('failedEmail', 'DEBUG: Email content:<br /><br />' . strip_tags($tpl, '<a>'), 'warning');

        if ($invitation_id) {
            Invitation::model()->findByPk($invitation_id)->activate();
        }

        return true;
    }

}
