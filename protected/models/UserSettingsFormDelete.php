<?php

class UserSettingsFormDelete extends CFormModel {

    public $reason;
    public $password;
    public $details;
    private $_identity;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            // required fields
            array('reason', 'required', 'message' => _t('Select a reason for leaving')),
            array('reason, password, details', 'safe'),
            // password needs to be authenticated
            array('password', 'authenticate'),
            // validate reason
            array('details', 'validateReason'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'reason' => _t('Reason for leaving'),
            'details' => _t('Explain in detail:'),
            'password' => _t('Please enter your password to continue:'),
        );
    }

    /**
     *
     * @param type $attribute
     * @param type $params
     */
    public function authenticate($attribute, $params) {
        if ($this->hasErrors())
            return;

        $this->_identity = new UserIdentity(_manager()->username, $this->password, false);
        if (!$this->_identity->authenticate()) {
            $this->addError('password', _t('Incorrect password'));
        }
    }

    /**
     * If option 'other' is chosen, details must not be empty.
     */
    public function validateReason($attribute, $params) {
        if (($this->reason == ReportReason::OTHER) && empty($this->details)) {
            $this->addError('details', _t('Write in detail the reason you are leaving us.'));
        }
    }

    /**
     *
     */
    public function update() {
        $this->attributes = $_POST['UserSettingsFormDelete'];

        if ($this->validate()) {
            $dbTransaction = _app()->db->beginTransaction();
            try {
                // Save manager data to deleted managers table
                $deleted = new DeletedManager;
                $deleted->report_reason_id = $this->reason;
                $deleted->details = $this->details;
                $deleted->date = _app()->localtime->getLocalNow();
                $deleted->team_name = _team()->name;

                $mgrattrs = array('username', 'registration_date', 'portfolio_cash', 'portfolio_share_value', 'total_points', 'email', 'fullname');
                foreach ($mgrattrs as $ma) {
                    $deleted->$ma = _manager()->$ma;
                }
                $profattrbs = array('gender', 'birthdate', 'location', 'country_id');
                foreach ($profattrbs as $prof) {
                    $deleted->$prof = _manager()->profile->$prof;
                }

                if (!$deleted->save())
                    throw new CValidateException($deleted);

                // Remove manager from ranks
                if (_manager()->hasRole(Role::MANAGER | Role::RESET))
                    Rank::removeTeamFromRanks(_manager());

                // Remove manager team players.
                foreach (_team()->managerTeamPlayers as $mtp) {
                    $mtp->delete();
                }

                Utils::triggerEvent('onManagerDelete', $this, array('manager_id' => _manager()->id));

                // Remove manager.
                _manager()->delete();

                // Remove manager team.
                _team()->delete();

                $dbTransaction->commit();
                $_SESSION['goodbye'] = true;
                return true;
            } catch (CDBException $e) {
                $dbTransaction->rollback();
                Debug::logToWindow($e);
                Utils::notice('error', _t('An error occured while deleting your account'), 'error');
            } catch (CValidateException $e) {
                $dbTransaction->rollback();
                Utils::notice('notice', $e->getMessage(), 'error');
                Debug::logToWindow(Debug::exceptionDetailString($e));
            }
        }
        $this->password = null;
        return false;
    }

}
