<?php

class UserSettingsFormEmail extends CFormModel {

    public $username;
    public $password;
    public $email;
    public $email2;
    private $_identity;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            // required fields
            array('password, username, email, email2', 'required'),
            array('email2', 'compare', 'compareAttribute' => 'email'),
            // emails need to be checked
            array('email', 'email', 'checkMX' => true, 'message' => _t('This email is not valid.')),
            // check for username and email uniqness
            array('email', 'unique', 'className' => 'Manager', 'message' => _t('The email address is being used')),
            // password needs to be authenticated
            array('password', 'authenticate'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'password' => _t('Password'),
            'email' => _t('New Email'),
            'email2' => _t('Confirm New Email'),
        );
    }

    /**
     *
     * @param <type> $attribute
     * @param <type> $params
     */
    public function authenticate($attribute, $params) {
        if (!$this->hasErrors()) {
            $this->_identity = new UserIdentity($this->username, $this->password);
            if (!$this->_identity->authenticate()) {
                $this->addError('password', _t('Incorrect password.'));
            }
        }
    }

    /**
     *
     */
    public function update() {
        $this->attributes = $_POST['UserSettingsFormEmail'];
        if ($this->validate()) {

            // Create reactivation code
            $activation_code = _manager()->createActivationCode(array($this->email));

            // and save it to the manager
            _manager()->email_code = $activation_code;
            if (_manager()->saveAttributes(array('email_code'))) {

                // Create activation url and email template
                $activation_url = _app()->createAbsoluteUrl('settings/activateEmail', array(
                    'uid' => _manager()->id,
                    'newEmail' => $this->email,
                    'code' => $activation_code,
                    ));
                $body = _controller()->renderPartial('_changeEmailConfirmationEmail', array('activation_url' => $activation_url), true);

                // Send email
                if (Utils::sendEmail($this->email, _t('Email address confirmation'), $body)) {
                    Utils::notice('result', _t('You have received a confirmation email at your new address. To complete the process follow the instructions.'), 'info');
                } else {
                    Utils::notice('result', _t('Failed to send confirmation email. Please try again.'), 'error');
                }
                Debug::notice('failedEmail', 'Debug: <br/><br/>' . strip_tags($body, '<a>'), 'warning');
            } else {
                Utils::notice('result', Debug::arObjectErrors(_manager()), 'error');
            }
            _manager()->refreshAttributes(array('email_code'));

            $this->email = null;
        }
        $this->email2 = null;
        $this->password = null;
    }

    /**
     *
     * @param type $uid
     * @param type $newEmail
     * @param type $code
     * @return type 
     */
    public function activate($uid, $newEmail, $code) {

//        if (_is_logged_in())
//            $manager = _manager();
//        else
            $manager = Manager::model()->findByPk($uid);

        if ($manager->email_code == $code) {
            $manager->email_code = null;
            $manager->email = $newEmail;
            if ($manager->validate(array('email_code', 'email')) && $manager->saveAttributes(array('email_code', 'email'))) {
                Utils::notice('result', _t('Email was successfully updated.'), 'success');
                _manager()->refreshAttributes(array('email'));
                return true;
            } else {
                Utils::notice('result', _t('Updating email failed!'), 'error');
                Debug::notice('failedEmail', 'Debug: <br />'. Debug::arObjectErrors(_manager()), 'error');
            }
        } else {
            Utils::notice('result', _t('Invalid activation code.'), 'error');
        }
        return false;
    }

}
