<?php

class UserSettingsFormPass extends CFormModel {

    public $username;
    public $password;
    public $newpassword;
    public $newpassword2;
    private $_identity;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            // required fields
            array('password, username, newpassword, newpassword2', 'required'),
            // password regex check
            array('newpassword', 'length', 'min' => 6, 'max' => 100, 'tooShort' => _t('Your password must be at least {min} characters long. Make your password strong by adding numbers, symbols and capital letters.')),
//            array('newpassword', 'match', 'pattern' => '/^(?=^.{6,}$)((?=.*[A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z]))^.*$/'),
            // emails and passwords need to be compared with each other
            array('newpassword2', 'compare', 'compareAttribute' => 'newpassword'),
            // password needs to be authenticated
            array('password', 'authenticate'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'password' => _t('Password'),
            'newpassword' => _t('New Password'),
            'newpassword2' => _t('Confirm New Password'),
        );
    }

    /**
     *
     * @param <type> $attribute
     * @param <type> $params
     */
    public function authenticate($attribute, $params) {
        if (!$this->hasErrors()) {
            $this->_identity = new UserIdentity($this->username, $this->password);
            if (!$this->_identity->authenticate()) {
                $this->addError('password', _t('Incorrect password.'));
            }
        }
    }

    /**
     *
     */
    public function update() {
        $this->attributes = $_POST['UserSettingsFormPass'];
        if ($this->validate()) {
            _manager()->setPassword($this->newpassword);
            if (_manager()->save(null,array('password'))) {
                Utils::notice('result', _t('Password was successfully updated.'), 'success');
            } else {
                Utils::notice('result', _t('Updating password failed!'), 'failed');
                error_log("Updating password for "._manager()->username." failed. " . Debug::arObjectErrors(_manager()));
            }
        }
        $this->newpassword = null;
        $this->newpassword2 = null;
        $this->password = null;
    }

}
