<?php

/**
 * This is the model class for table "watchlist".
 *
 * The followings are the available columns in table 'watchlist':
 * @property integer $manager_id
 * @property integer $player_id
 */
class Watchlist extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return Watchlist the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }
    
    public function primaryKey() {
        return array('manager_id','player_id');
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'watchlist';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('manager_id, player_id', 'required'),
            array('manager_id, player_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('manager_id, player_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'manager' => array(self::BELONGS_TO, 'Manager', 'manager_id'),
            'player' => array(self::BELONGS_TO, 'Player', 'player_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'manager_id' => 'Manager',
            'player_id' => 'Player',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('manager_id', $this->manager_id);
        $criteria->compare('player_id', $this->player_id);

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
    }

}