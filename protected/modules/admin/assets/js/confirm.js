$(document).ready(function() {

    $(".confirm").click(function() {
        //        e.preventDefault();
        var targetUrl = $(this).attr("href");
        var info = $(this).attr("inf");
        var elem = $(this).attr("elem");

        $("#dialog").html("Are you sure you want to delete "+elem+" "+info+"?");

        $("#dialog").dialog({
            autoOpen: true,
            modal: true,
            buttons : {
                "Delete" : function() {
                    window.location.href = targetUrl;
                },
                "Cancel" : function() {
                    $(this).dialog("close");
                }
            }
        }).dialog("open");
        
        return false;
    });
    
    $(".confirmedit").click(function() {
        //        e.preventDefault();
        var targetUrl = $(this).attr("href");

        $("#dialog").html("Editing this game will affect managers' and players' points. Are you sure?");

        $("#dialog").dialog({
            autoOpen: true,
            modal: true,
            buttons : {
                "Edit" : function() {
                    window.location.href = targetUrl;
                },
                "Cancel" : function() {
                    $(this).dialog("close");
                }
            }
        }).dialog("open");
        
        return false;
    });
    
    $(".confirmcancel").click(function() {
        //        e.preventDefault();
        var targetUrl = $(this).attr("href");

        $("#dialog").html("Are you sure this match has been cancelled?");

        $("#dialog").dialog({
            autoOpen: true,
            modal: true,
            buttons : {
                "Yes" : function() {
                    window.location.href = targetUrl;
                },
                "No" : function() {
                    $(this).dialog("close");
                }
            }
        }).dialog("open");
        
        return false;
    });
});