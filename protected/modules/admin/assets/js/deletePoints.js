$(document).ready(function() {

    $(".confirm").click(function() {
        //        e.preventDefault();
        var targetUrl = $(this).attr("href");
        var info = $(this).attr("inf");
        var elem = $(this).attr("elem");

        $("#dialog").html("Are you sure you want to delete "+elem+" "+info+"?");

        $("#dialog").dialog({
            autoOpen: true,
            modal: true,
            buttons : {
                "Delete" : function() {
                    $("#dialog").dialog("close");
                    $("#dialogDeleting").dialog({
                        autoOpen: true,
                        modal: true,
                        closeOnEscape: false,
                        open: function(event, ui) {
                            $(".ui-dialog-titlebar-close").hide();
                        }
                    }).dialog("open");
                    $.ajax({
                        url: targetUrl,
                        type: "GET",
                        success: function(data)
                        {
                            window.location.replace(MSL.adminUrl + 'game/index');
                        }
                    })
                },
                "Cancel" : function() {
                    $(this).dialog("close");
                }
            }
        }).dialog("open");
        
        return false;
    });
})