$(document).ready(function(){
    $("#submitBtn").click(function(){
        $("#dialog").dialog({
            autoOpen: true,
            modal: true,
            buttons : {
                "Delist" : function() {
                    $("#delist-form").submit();
                },
                "Cancel" : function() {
                    $(this).dialog("close");
                }
            }
        }).dialog("open");

        return false;
    })
})