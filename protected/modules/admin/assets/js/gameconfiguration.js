$(document).ready(function(){
    $("#submitBtn").click(function(){
        $("#dialog").dialog({
            autoOpen: true,
            modal: true,
            buttons : {
                "Save" : function() {
                    $("#gameconfiguration-form").submit();
                    $("#scriptconfiguration-form").submit();
                    $("#gameparameters-form").submit();
                },
                "Cancel" : function() {
                    $(this).dialog("close");
                }
            }
        }).dialog("open");

        return false;
    });
    
    $("#reset-managers").click(function(){
        var url = $(this).attr("href");
        $("#reset-dialog").dialog({
            autoOpen: true,
            modal: true,
            title:'Oh no!!!',
            buttons : {
                "Yes, please" : function() {
                    window.location.href = url;
                },
                "Noooo!" : function() {
                    $(this).dialog("close");
                }
            }
        }).dialog("open");

        return false;
    })
})
