$(document).ready(function(){
    $("#changeTeamName").click(function(){
        $(".changeTeamNameForm").dialog({
            autoOpen: false,
            modal: true,
            title: 'Change team name'
        }).dialog("open");
    });

    $(".dialog").dialog({
        autoOpen: false,
        modal: true,
        height: 500,
        title: 'All users with IP'
    });

    //    $(".ipLink").click(function(){
    //        $(".dialog").dialog({
    //            autoOpen: false,
    //            modal: true,
    //            height: 500,
    //            title: 'All users with IP'
    //        }).dialog("open");
    //
    //        return false;
    //    });

    var teamChangeUrl = MSL.adminUrl+'manager/changeTeamName';
    $('.teamName span').editable(teamChangeUrl, {
        loadUrl: teamChangeUrl,
        id: $(this).attr("id"),
        cssclass: 'inline-edit',
        placeholder: '',
        submit  : 'OK',
        width: 110,
        autowidth: false,
        maxlength: 45,
        tooltip : 'Click to change team name',
        callback : function(value, settings) {
            var errors = JSON.parse(value);
            if (errors.error) {
                $("#failure").html(errors.message);
                $(this).html($(this).attr("oldName"));
            } else {
                console.log(errors.message);
                $("#failure").html("");
                $(this).attr("oldName",errors.message);
                $(this).html(errors.message);
            }
        }
    });

    $('#newsLink').live('click',function(){
        var url = $(this).attr("href");
        var id = $(this).attr("id");
        var flag = $(this).attr("flag");
        $.ajax({
            url: url,
            data: {
                id: id,
                value: flag
            },
            success: function(data) {
                $('#newsletter').html(data)
            }
        });

        return false;
    });

    $('#fullname span').editable('changeFullName', {
        loadUrl: MSL.adminUrl+'manager/changeFullName',
        id: $(this).attr("id"),
        cssclass: 'inline-edit',
        placeholder: '',
        submit  : 'OK',
        width: 110,
        autowidth: false,
        maxlength: 45,
        tooltip : 'Click to change team name'
    });

    $('#email span').editable('changeEmail', {
        loadUrl: MSL.adminUrl+'manager/changeEmail',
        id: $(this).attr("id"),
        cssclass: 'inline-edit',
        placeholder: '',
        submit  : 'OK',
        width: 110,
        autowidth: false,
        maxlength: 45,
        tooltip : 'Click to change team name'
    });

    $('#msgLink').click(function(){
        $(".msg-dialog").dialog({
            autoOpen: false,
            modal: true,
            width: 'auto',
            title: 'Say hello!'
        }).dialog("open");

        return false;
    });
});


