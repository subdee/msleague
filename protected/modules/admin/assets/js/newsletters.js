$(document).ready(function(){
    $(".newGroup").live('click', function(){
        var srcUrl = $(this).attr("href");
        $(".formDialog").dialog({
            autoOpen: true,
            modal: true,
            buttons : {
                "Save" : function() {
                    $.ajax({
                        url: srcUrl,
                        type: "POST",
                        data: $("#managerGroup-form").serialize(),
                        success: function(data) {
                            if (data == 'ok'){
                                $.ajax({
                                    url: MSL.adminUrl+'newsletter/groups',
                                    type: 'POST',
                                    success: function(data) {
                                        $(".manager_group").html(data);
                                    }
                                })
                                $(".errorDiv").html("");
                                $(".errorDiv").hide();
                                $(".formDialog").dialog("close");
                            } else {
                                $(".errorDiv").html(data);
                                $(".errorDiv").show();
                                $(".formDialog").dialog("close");
                            }
                        }
                    });
                },
                "Cancel" : function() {
                    $(this).dialog("close");
                }
            }
        }).dialog("open");
        
        return false;
    });
    
    $(".sendNews").click(function(){
        var srcUrl = $(this).attr("href");
        $(".confirmDialog").dialog({
            autoOpen: true,
            modal: true,
            buttons : {
                "Send" : function() {
                    $(this).dialog("close");
                    $.ajax({
                        url: srcUrl,
                        type: "GET",
                        success: function(data) {
                            $(".loading").dialog("close");
                            if (data == 'error'){
                                $(".notSent").dialog({
                                    autoOpen: false, 
                                    modal: true
                                }).dialog("open");
                                setTimeout(function(){
                                    $(".notSent").dialog("close")
                                    }, 5000);
                            } else {
                                $(".sent").append("The newsletter was sent succesfully to "+data+" managers.");
                                $(".sent").dialog({
                                    autoOpen: false, 
                                    modal: true
                                }).dialog("open");
                                setTimeout(function(){
                                    $(".sent").dialog("close")
                                    }, 5000);
                            }
                        },
                        beforeSend: function(){
                            $(".loading").dialog({
                                autoOpen: false, 
                                modal: true
                            }).dialog("open");
                        }
                    });
                },
                "Cancel" : function() {
                    $(this).dialog("close");
                }
            }
        }).dialog("open");
        return false;
    })
    
    $("#group_id").change(function(){
        $.ajax({
            url: MSL.adminUrl+'newsletter/countManagers',
            type: 'POST',
            data: 'id='+$(this).val(),
            success: function(data) {
                var str = 'This newsletter will be sent to '+data+' managers.';
                $(".count").html(str).show();
            }
        })
    })
//    
//    $("#radio_1").click(function(){
//        $(":checkbox, #temp_value, #inactive_value, #nonactive_value").attr("disabled",false);
//    })
//    
//    $("#radio_0").click(function(){
//        $(":checkbox, #temp_value, #inactive_value, #nonactive_value").attr("disabled",true);
//    })

//    $("#nonactive, #inactive, #temp").click(function(){
//        $($(this).attr("id")+"_value").attr("disabled",false);
//    })
})


