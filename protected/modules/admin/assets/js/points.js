$(document).ready(function(){
    $(".pointsForm form").submit(function(){
        $(".points").attr("disabled", true);
        
        $("#dialogPlayers").dialog({
            autoOpen: true,
            modal: true,
            closeOnEscape: false,
            open: function(event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            }
        }).dialog("open");
        var gid = $("#id").attr("value");

        $.ajax({
            url: MSL.adminUrl+"game/points",
            data: {
                id: gid
            },
            success: function(response)
            {
                if(response.status == 'ok') {
                    window.location.replace(MSL.adminUrl+'game/index');
                } else {
                    $(".points").attr("disabled", false);
                    $("#dialogPlayers").dialog("close");
                    $("#dialogLocked").dialog({
                        autoOpen: true,
                        modal: true,
                        closeOnEscape: false
                    }).dialog("open");
                    setTimeout(function(){
                        $("#dialogLocked").dialog("close")
                    }, 5000);
                }
            }
        })
        return false;
    })
})