$(document).ready(function(){
    $(".details").click(function(){
        var srcUrl = $(this).attr("href");
        var title = $(this).attr("title");
        $.ajax({
            url: srcUrl,
            type: "GET",
            success: function(data) {
                if (data){
                    $(".messageDetails").html(data);
                    $(".messageDetails").dialog({
                        autoOpen: false,
                        modal: true,
                        width: '800',
                        height: '700',
                        title: title
                    }).dialog("open");
                }
            }
        })
        return false;
    })
})