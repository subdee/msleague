$('#form-reset-button').click(function()
    {
        var id=$(this).attr("page");
        var inputSelector='#'+id+' .filters input, '+'#'+id+' .filters select';
        $(inputSelector).each( function(i,o) {
            $(o).val('');
        });
        var data=$.param($(inputSelector));
        $.fn.yiiGridView.update(id, {
            data: data
        });
        return false;
    });