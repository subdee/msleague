$(document).ready(function(){
    $(".upload").click(function(){
        var id = $(this).attr("id");
        var matchid = $(this).attr("matchid");
        if (typeof(matchid) != 'undefined'){
            var title = 'Select XML to upload (Match ID: '+matchid+')';
        } else {
            var title = 'Select XML to upload';
        }
        if (id){
            $.ajax({
                url: MSL.adminUrl+'game/upload',
                type: "GET",
                data: {
                    'id' : id
                },
                success: function(data)
                {
                    $("#uploadPop").html(data);
                    $("#uploadPop").dialog({
                        autoOpen: true,
                        modal: true,
                        width: '500px',
                        title: title
                    }).dialog("open");
                }
            });
        }else{
            $("#uploadPop").dialog({
                autoOpen: true,
                modal: true,
                width: '500px',
                title: title
            }).dialog("open");
        }    
        return false;
    })
})