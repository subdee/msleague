<?php

class AdminController extends Controller {

    public $layout = 'column1';

    public function init() {
        parent::init();

        _cs()->registerScriptFile(_bu('js/main.js'));

        if (isset($_GET['lang'])) {
            _app()->language = $_GET['lang'];
        }
        else
            _app()->language = 'en';

//        if ($this->module->id != 'admin')
//            $this->redirect(_url('user/logout'));

        // Override error handler
        _app()->errorHandler->errorAction = 'admin/main/error';
    }

}

?>
