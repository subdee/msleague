<?php

class AnnouncementController extends AdminController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
//	public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'add', 'edit', 'delete'),
                'expression' => 'User::hasRole(AdminRole::ADMIN | AdminRole::OPERATOR)',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $anns = new Announcement;

        $this->render('index', array('anns' => $anns));
    }

    /**
     * Saves the edited announcement or renders the edit page
     * @param integer $id The id of the announcement
     *
     */
    public function actionEdit($id) {
        $ann = $this->loadModel($id);

        if (isset($_POST['Announcement'])) {
            $t = _app()->db->beginTransaction();
            try {
                $ann->attributes = $_POST['Announcement'];
                $ann->user_id = _user()->id;
                if (!$ann->save()) {
                    $t->rollback();
                    $this->render('add', array('ann' => $ann));
                    _app()->end();
                }

                $t->commit();

                $this->redirect(AdminUtils::aUrl('announcement/index'));
            } catch (CDbException $e) {
                error_log($e);
                $t->rollback();
            }
        }

        $this->render('edit', array('ann' => $ann));
    }

    /**
     * Add a new announcement
     */
    public function actionAdd() {
        $ann = new Announcement;

        if (isset($_POST['Announcement'])) {
            $t = _app()->db->beginTransaction();
            try {
                $ann->attributes = $_POST['Announcement'];
                $ann->date_entered = _app()->localtime->getLocalNow('Y-m-d H:i:s');
                $ann->user_id = _user()->id;
                if (!$ann->save()) {
                    $t->rollback();
                    $this->render('add', array('ann' => $ann));
                    _app()->end();
                }

                $t->commit();

                $this->redirect(AdminUtils::aUrl('announcement/index'));
            } catch (CDbException $e) {
                error_log($e);
                $t->rollback();
            }
        }

        $this->render('add', array('ann' => $ann));
    }

    /**
     * Delete an announcement
     * @param type $id 
     */
    public function actionDelete($id) {
        $ann = $this->loadModel($id);

        $t = _app()->db->beginTransaction();
        try {
            $ann->delete();

            $t->commit();
        } catch (CDbException $e) {
            $t->rollback();
            error_log($e);
        }

        $this->actionIndex();
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Announcement::model()->findByPk((int) $id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

}
