<?php

class CmsController extends AdminController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
//	public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'save', 'tips', 'addTip', 'delete', 'reportReasons', 'addReason', 'deleteReason'),
                'expression' => 'User::hasRole(AdminRole::ADMIN | AdminRole::OPERATOR)',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        _cs()->registerCoreScript('jquery.ui');
        AdminUtils::registerAdminCssFile('jui/base/jquery-ui');

        $lang = isset($_POST['lang']) ? $_POST['lang'] : Language::model()->id(_app()->language);
        $sc = StaticContent::model()->findAll('lang = :lang', array(':lang' => $lang));
        $this->render('index', array('sc' => $sc, 'lang' => $lang));
    }

    /**
     * Saves all static content and redirects to static content editors
     */
    public function actionSave() {
        $vars = StaticContent::model()->findAll(array(
            'select' => 'section',
            'distinct' => true
            ));
        foreach ($vars as $var) {
            if (isset($_POST[$var->section]) && !is_null($_POST[$var->section])) {
                $sc = StaticContent::model()->find('lang = :lang AND section = :section', array(':lang' => $_POST['lang'], ':section' => $var->section));
                if ($var->section == 'TermsOfUse' && $sc->content != $_POST[$var->section]){
                    $managers = Manager::model()->findAll();
                    foreach ($managers as $manager) {
                        $manager->accepted_terms = false;
                        $manager->update();
                    }
                }
                $sc->content = $_POST[$var->section];
                $sc->save(true, array('content'));
            }
        }

        $this->redirect(AdminUtils::aUrl('cms/index'));
    }

    /**
     * Tips page
     */
    public function actionTips() {
        $tips = new Tips;

        $this->render('tips', array('tips' => $tips));
    }

    /**
     * Add a tip
     */
    public function actionAddTip() {
        $tip = new Tips;

        if (isset($_POST['Tips'])) {
            $tip->attributes = $_POST['Tips'];
            if ($tip->save())
                $this->redirect(AdminUtils::aUrl('cms/tips'));
            else
                error_log(Debug::arObjectErrors($tip));
        }

        $this->render('addTip', array('tip' => $tip));
    }

    public function actionDelete($id) {
        $tip = Tips::model()->findByPk($id);
        $tip->delete();

        $this->redirect(AdminUtils::aUrl('cms/index'));
    }

    public function actionReportReasons() {
        $reasons = new ReportReason;

        $this->render('reportReasons', array('reasons' => $reasons));
    }

    public function actionAddReason() {
        $reason = new ReportReason;
        $reasonTypes = ReportReason::getTypes();

        if (isset($_POST['ReportReason'])) {
            $reason->attributes = $_POST['ReportReason'];
            if ($reason->save()) {
                $this->redirect(AdminUtils::aUrl('cms/reportReasons'));
            }else
                error_log(Debug::arObjectErrors($reason));
        }

        $this->render('addReason', array('reason' => $reason, 'reasonTypes' => $reasonTypes));
    }

    public function actionDeleteReason($id) {
        ReportReason::model()->deleteByPk($id);

        $this->redirect(AdminUtils::aUrl('cms/reportReasons'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Announcement::model()->findByPk((int) $id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

}
