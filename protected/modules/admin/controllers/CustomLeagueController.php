<?php

class CustomLeagueController extends AdminController {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'expression' => 'User::hasRole(AdminRole::ADMIN | AdminRole::OPERATOR)',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * User leagues page.
     */
    public function actionIndex() {
        $leagues = new CustomLeague('search');
        $leagues->type = CustomLeagueType::USER;

        if (isset($_GET['CustomLeague'])) {
            $leagues->attributes = $_GET['CustomLeague'];
        }

        $this->render('index', array(
            'model' => $leagues,
        ));
    }

    /**
     * Fan leagues page.
     */
    public function actionFan() {
        $leagues = new CustomLeague('search');
        $leagues->type = CustomLeagueType::TEAM;

        if (isset($_GET['CustomLeague'])) {
            $leagues->attributes = $_GET['CustomLeague'];
        }

        $this->render('fan', array(
            'model' => $leagues,
        ));
    }

    /**
     * League page.
     * @param int $id League id
     */
    public function actionLeague($id) {
        $league = CustomLeague::model()->findByPk($id);
        if (!$league)
            throw new CHttpException('404');

        $managers = new CustomLeagueManager('searchAdmin');
        $managers->league_id = $id;

        if (isset($_GET['CustomLeagueManager'])) {
            $managers->attributes = $_GET['CustomLeagueManager'];
        }

        $this->render('league', array(
            'league' => $league,
            'model' => $managers,
        ));
    }

    /**
     * Manager's leagues
     * @param int $id Manager id
     */
    public function actionManager($id) {
        $manager = Manager::model()->findByPk($id);
        if (!$manager)
            throw new CHttpException('404');

        $leagues = new CustomLeagueManager('searchAdmin');
        $leagues->manager_id = $id;

        if (isset($_GET['CustomLeagueManager'])) {
            $leagues->attributes = $_GET['CustomLeagueManager'];
        }

        $this->render('manager', array(
            'manager' => $manager,
            'model' => $leagues,
        ));
    }

    /**
     * Adds a new league.
     */
    public function actionAdd() {

        // Create models
        $league = new CustomLeague;
        $league->type = CustomLeagueType::SYSTEM;
        $league->capacity = CustomLeague::CAPACITY_UNLIMITED;

        $team = new CustomLeagueFan;
        $discussion = new CustomLeagueDiscussion;
        if (isset($_POST['CustomLeague'])) {

            try {
                // Set attributes
                $league->attributes = $_POST['CustomLeague'];
                $league->date_created = _app()->localtime->getLocalNow();

                if ($league->type == CustomLeagueType::TEAM) {
                    $team->attributes = $_POST['CustomLeagueFan'];
                }

                // Open transaction
                $trans = _app()->db->beginTransaction();

                // First validate
                if (!$league->validate())
                    throw new CValidateException($league);

                // Replace available variables
                if ($league->type == CustomLeagueType::TEAM) {
                    $league->name = str_replace('{team}', $team->team->name, $league->name);
                }

                // Set creator id
                if ($league->type == CustomLeagueType::USER) {
                    $league->creator_id = Manager::model()->findByAttributes(array('username' => $league->creatorUsername))->id;
                }

                // Then save the league
                $league->writeMetadata();
                $league->save(false);

                // Add the creator to the league
                if ($league->type == CustomLeagueType::USER) {
                    $league->addManager($league->creator_id);
                }

                // Save necessary connections
                if ($league->type == CustomLeagueType::TEAM) {
                    $team->league_id = $league->id;
                    if (!$team->save())
                        throw new CValidateException($team);
                }

                Utils::triggerEvent('onCustomLeagueCreate', $this, array('newLeague' => $league));

                $trans->commit();
                Utils::notice('league', 'League created successfully!', 'success');
                $this->redirect('add');
            } catch (CDbException $e) {
                Utils::notice('league', $e->getMessage(), 'error');
                $trans->rollback();
            } catch (CValidateException $e) {
                $trans->rollback();
            }
        }

        $this->render('add', array(
            'league' => $league,
            'team' => $team,
            'discussion' => $discussion,
        ));
    }

    /**
     *
     */
    public function actionConfig() {

        $model = CustomLeagueConfig::model();

        if(isset($_POST['CustomLeagueConfig'])) {
            $model->attributes = $_POST['CustomLeagueConfig'];
            if(!$model->save()) {
                Utils::notice('league', Debug::arObjectErrors($model->getErrors()), 'error');
            } else {
                Utils::notice('league', _t('Updated!!'), 'success');
            }
        }

        $this->render('config', array(
            'model' => $model,
        ));
    }

}
