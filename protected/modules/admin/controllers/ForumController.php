<?php

class ForumController extends AdminController {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'expression' => 'User::hasRole(AdminRole::ADMIN | AdminRole::OPERATOR)',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists all categories
     */
    public function actionIndex() {
        $categories = new Category('search');
        $this->render('index', array(
            'categories' => $categories,
            'maxRank' => Category::model()->getMaxRank(),
            'languages' => Language::model()->findAll(),
        ));
    }

    /**
     *
     * @param type $category
     */
    public function actionDiscussions($category) {
        $categoryModel = Category::model()->findByPk($category);
        if (!$categoryModel->text) {
            throw new CHttpException('404');
        }

        $discussions = new CategoryDiscussion('search');
        $discussions->category_id = $category;

        if (isset($_GET['CategoryDiscussion'])) {
            $discussions->attributes = $_GET['CategoryDiscussion'];
        }


        $this->render('discussions', array(
            'discussions' => $discussions,
            'category' => $categoryModel,
        ));
    }

    /**
     *
     * @param type $discussion
     */
    public function actionComments($discussion) {
        $category = CategoryDiscussion::model()->findByAttributes(array('discussion_id' => $discussion));
        if (!$category->category->text) {
            throw new CHttpException('404');
        }

        $comments = new Comment('searchAdmin');
        $comments->discussion_id = $discussion;

        if (isset($_GET['Comment'])) {
            $comments->attributes = $_GET['Comment'];
        }

        $this->render('comments', array(
            'category' => $category,
            'comments' => $comments
        ));
    }

    public function actionShow($discussion) {
        $comments = new Comment('searchAdmin');
        $comments->discussion_id = $discussion;

        if (isset($_GET['Comment'])) {
            $comments->attributes = $_GET['Comment'];
        }

        $this->render('show', array(
            'category' => CategoryDiscussion::model()->findByAttributes(array('discussion_id' => $discussion)),
            'comments' => $comments
        ));
    }

    /**
     *
     * @param type $id
     */
    public function actionUpdate($id) {
        $category = Category::model()->findByPk($id);
        if ($category->edit()) {
            $this->redirect(AdminUtils::aUrl('forum/index'));
        }
        $this->render('category', array(
            'action' => 'update',
            'category' => $category,
            'languages' => Language::model()->findAll()
        ));
    }

    /**
     *
     * @param type $id
     */
    public function actionDelete($id) {
        Category::model()->findByPk($id)->delete();
        $this->redirect(AdminUtils::aUrl('forum/index'));
    }

    /**
     *
     */
    public function actionAddCategory() {
        $category = new Category;
        if ($category->edit()) {
            $this->redirect(AdminUtils::aUrl('forum/index'));
        }
        $this->render('category', array(
            'action' => 'add',
            'category' => $category,
            'languages' => Language::model()->findAll(),
        ));
    }

    /**
     *
     * @param type $id
     */
    public function actionCategoryRankUp($id) {
        $category = Category::model()->findByPk($id);
        $category->updateRank(-1);
        $this->redirect(AdminUtils::aUrl('forum/index'));
    }

    /**
     *
     * @param type $id
     */
    public function actionCategoryRankDown($id) {
        $category = Category::model()->findByPk($id);
        $category->updateRank(1);
        $this->redirect(AdminUtils::aUrl('forum/index'));
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function actionOpenDiscussion($id) {
        if (!Utils::isAjax())
            return;

        $discussion = Discussion::model()->findByPk($id);
        $discussion->comments_open = true;
        if ($discussion->save(false, array('comments_open'))) {
            Utils::jsonReturn(array('success' => true));
        }

        Utils::jsonReturn(array('success' => false));
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function actionCloseDiscussion($id) {
        if (!Utils::isAjax())
            return;

        $discussion = Discussion::model()->findByPk($id);
        $discussion->comments_open = false;
        if ($discussion->save(false, array('comments_open'))) {
            Utils::jsonReturn(array('success' => true));
        }

        Utils::jsonReturn(array('success' => false));
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function actionDeleteDiscussion($id) {
        if (!Utils::isAjax())
            return;

        $discussion = Discussion::model()->findByPk($id);
        foreach ($discussion->comments as $comment)
            Report::model()->deleteReport(ReportReason::MESSAGE, $comment->commentor_id, $comment->id);
        if ($discussion->delete()) {
            Utils::jsonReturn(array('success' => true));
        }

        Utils::jsonReturn(array('success' => false));
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function actionHideComment($id) {
        if (!Utils::isAjax())
            return;

        $comment = Comment::model()->findByPk($id);
        $comment->visible = false;
        if ($comment->save(false, array('visible')))
            Utils::jsonReturn(array('success' => true));

        Utils::jsonReturn(array('success' => false));
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function actionShowComment($id) {
        if (!Utils::isAjax())
            return;

        $comment = Comment::model()->findByPk($id);
        $comment->visible = true;
        if ($comment->save(false, array('visible')))
            Utils::jsonReturn(array('success' => true));

        Utils::jsonReturn(array('success' => false));
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function actionHideDiscussion($id) {
        if (!Utils::isAjax())
            return;

        $discussion = Discussion::model()->findByPk($id);
        $discussion->visible = false;
        if ($discussion->save(false, array('visible')))
            Utils::jsonReturn(array('success' => true));

        Utils::jsonReturn(array('success' => false));
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function actionShowDiscussion($id) {
        if (!Utils::isAjax())
            return;

        $discussion = Discussion::model()->findByPk($id);
        $discussion->visible = true;
        if ($discussion->save(false, array('visible')))
            Utils::jsonReturn(array('success' => true));

        Utils::jsonReturn(array('success' => false));
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function actionDeleteComment($id) {
        if (!Utils::isAjax())
            return;

        $comment = Comment::model()->findByPk($id);
        if ($comment->delete())
            Utils::jsonReturn(array('success' => true));

        Utils::jsonReturn(array('success' => false));
    }

    /**
     *
     */
    public function actionMatches() {
        // Get all match discussions
        $discussions = new MatchDiscussion('search');

        if (isset($_GET['MatchDiscussion'])) {
            $discussions->attributes = $_GET['MatchDiscussion'];
        }

        $this->render('matches', array('discussions' => $discussions));
    }

    /**
     *
     * @param type $discussion
     */
    public function actionMatch($discussion) {
        $comments = new Comment('searchAdmin');
        $comments->discussion_id = $discussion;

        if (isset($_GET['Comment'])) {
            $comments->attributes = $_GET['Comment'];
        }

        $this->render('match', array(
            'game' => MatchDiscussion::model()->findByAttributes(array('discussion_id' => $discussion))->game,
            'comments' => $comments
        ));
    }

    /**
     *
     */
    public function actionLeagues() {
        $discussions = new CustomLeagueDiscussion('searchExceptFanLeagues');

        if (isset($_GET['CustomLeagueDiscussion'])) {
            $discussions->attributes = $_GET['CustomLeagueDiscussion'];
        }

        $this->render('leagues', array('discussions' => $discussions));
    }

    /**
     *
     */
    public function actionFanLeagues() {
        $discussions = new CustomLeagueDiscussion('searchFanLeagues');

        if (isset($_GET['CustomLeagueDiscussion'])) {
            $discussions->attributes = $_GET['CustomLeagueDiscussion'];
        }

        $this->render('fanLeagues', array('discussions' => $discussions));
    }

    /**
     *
     * @param type $discussion
     */
    public function actionLeague($discussion) {
        $comments = new Comment('searchAdmin');
        $comments->discussion_id = $discussion;

        if (isset($_GET['Comment'])) {
            $comments->attributes = $_GET['Comment'];
        }

        $this->render('league', array(
            'league' => CustomLeagueDiscussion::model()->findByAttributes(array('discussion_id' => $discussion))->league,
            'comments' => $comments
        ));
    }

}
