<?php

class GameController extends AdminController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
//	public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'editGame', 'delete', 'deletePlayer', 'preview', 'stats', 'upload', 'toggleComments', 'points', 'error', 'managerPoints', 'admin', 'cancel'),
                'expression' => 'User::hasRole(AdminRole::ADMIN | AdminRole::OPERATOR)',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        _cs()->registerCoreScript('jquery');
        _cs()->registerCoreScript('jquery.ui');
        AdminUtils::registerAdminCssFile('jui/base/jquery-ui');
        AdminUtils::registerAdminJsFile('confirm');
        AdminUtils::registerAdminJsFile('upload');
        $search = array('team_home' => '', 'team_away' => '', 'date' => '');

        if (isset($_POST['search'])) {
            $cr = new CDbCriteria;
            $cr->condition = '1';
            $cr->params = array();
            if ($_POST['search']['date'] != '') {
                $cr->addCondition('date(convert_tz(date_played,"UTC",:tz)) = :date');
                $cr->params = array_merge($cr->params, array(':date' => $_POST['search']['date'], ':tz' => _timezone()));
            }
            if ($_POST['search']['team_home'] != '') {
                $cr->addCondition('team_home = :th');
                $cr->params = array_merge($cr->params, array(':th' => $_POST['search']['team_home']));
            }
            if ($_POST['search']['team_away'] != '') {
                $cr->addCondition('team_away = :ta');
                $cr->params = array_merge($cr->params, array(':ta' => $_POST['search']['team_away']));
            }
            $search = $_POST['search'];
            $m = Game::model()->findAll($cr);
        } else {
            $m = Game::model()->findAll(array('order' => 'date_played asc'));
        }
        $this->render('index', array('m' => $m, 'search' => $search));
    }

    public function actionCreate() {
        $game = new Game;

        if (isset($_POST['Game'])) {
            $t = _app()->db->beginTransaction();
            try {
                $game->attributes = $_POST['Game'];
                if (!$game->save()) {
                    throw new Exception(Debug::arObjectErrors($game));
                }

                $game->refresh();
                if (_isModuleOn('forum')) {

                    // Create the discussion
                    $disc = new Discussion;
                    $disc->comments_open = true;
                    if (!$disc->save()) {
                        throw new Exception(Debug::arObjectErrors($disc));
                    }

                    // Create the match discussion connection
                    $matchDiscussion = new MatchDiscussion;
                    $matchDiscussion->discussion_id = $disc->id;
                    $matchDiscussion->game_id = $game->id;
                    if (!$matchDiscussion->save()) {
                        throw new Exception(Debug::arObjectErrors($matchDiscussion));
                    }
                }

                $t->commit();
                $this->redirect(AdminUtils::aUrl('game/index'));
            } catch (CDbException $e) {
                error_log($e);
                _user()->setFlash($e->getMessage());
                $t->rollback();
            } catch (Exception $e) {
                error_log($e);
                _user()->setFlash('failure', $e->getMessage());
                $t->rollback();
            }
        }
        $this->render('create', array('game' => $game));
    }

    public function actionDelete($id, $deleteMatch=false, $updateStats=false) {
        $game = $this->loadModel($id);

        $t = _app()->db->beginTransaction();
        try {
            if ($game->is_cancelled) {
                $game->is_cancelled = $game->points_calculated = false;
                if ($game->update()) {
                    $t->commit();
                    echo 1;
                } else {
                    $t->rollback();
                    echo 0;
                }
                return;
            }

            if ($updateStats && !$game->is_cancelled) {
                $pointsSubPerPlayer = $this->removePlayerPoints($game);
                $this->removeManagerPoints($game, $pointsSubPerPlayer);

                //Update ranks
                Rank::updateRanks();
            }

            //Remove stats
            $game->score_home = null;
            $game->score_away = null;
            $game->points_calculated = false;
            foreach ($game->gamePlayers as $event)
                $event->delete();
            $game->save(false, array('score_home', 'score_away', 'points_calculated'));

            if ($deleteMatch) {
                $game->delete();
            }

            $t->commit();
        } catch (CDbException $e) {
            Debug::logToWindow($e->getMessage());
            $t->rollback();
            _user()->setFlash('failure', $e->getMessage());
            if ($updateStats) {
                echo 0;
                return;
            }
        } catch (CValidateException $e) {
            $t->rollback();
            _user()->setFlash('failure', $e->getMessage());
        } catch (CException $e) {
            Debug::logToWindow($e->getMessage());
            $t->commit();
        }
        if ($updateStats) {
            echo 1;
            return;
        } else
            $this->redirect(AdminUtils::aUrl('game/index'));
    }

    public function actionEditGame($id, $edit) {
        $game = $this->loadModel($id);

        if (isset($_POST['Game'])) {
            $t = _app()->db->beginTransaction();
            try {
                $game = Game::model()->findByPk($_POST['Game']['id']);
                $game->attributes = $_POST['Game'];
                if (!$game->save())
                    throw new Exception(Debug::arObjectErrors($game));

                $t->commit();
                $this->redirect(AdminUtils::aUrl('game/index'));
            } catch (CDbException $e) {
                $t->rollback();
                error_log($e);
            } catch (Exception $e) {
                $t->rollback();
                error_log($e);
            }
        }

        $this->render('editGame', array('game' => $game, 'edit' => $edit));
    }

    public function actionStats($id, $hEvents = null, $aEvents = null, $hScore = null, $aScore = null) {

        _cs()->registerCoreScript('jquery');
        _cs()->registerCoreScript('jquery.ui');
        AdminUtils::registerAdminCssFile('jui/base/jquery-ui');
        AdminUtils::registerAdminJsFile('confirm');

        if (isset(_app()->session['gameId'])) {
            if (_app()->session['gameId'] != $id)
                $this->unsetPosts();
        }

        $hInputs = $aInputs = '';
        $game = $this->loadModel($id);

        $homeTeam = Team::model()->with('players')->find(array('order' => 'players.shortname asc', 'condition' => 't.id = :id', 'params' => array(':id' => $game->teamHome0->id)));
        $awayTeam = Team::model()->with('players')->find(array('order' => 'players.shortname asc', 'condition' => 't.id = :id', 'params' => array(':id' => $game->teamAway0->id)));

        $events = Event::model()->findAll(array('select' => 'distinct(name) as name', 'condition' => 'is_team_event = 0'));

        foreach ($events as $event) {
            $hInputs .= '<td><input type=\"text\" size=\"2\" value=\"\" name=\"HomePlayer["+id+"][event][' . $this->sanitizeText($event->name) . ']\"></td>';
            $aInputs .= '<td><input type=\"text\" size=\"2\" value=\"\" name=\"AwayPlayer["+jd+"][event][' . $this->sanitizeText($event->name) . ']\"></td>';
        }

        $homePlayers = $this->createDropdownOptions($homeTeam->players);
        $awayPlayers = $this->createDropdownOptions($awayTeam->players);

        $home = $this->createPreview(GamePlayer::model()->with('player')->findAll(
                'player.team_id = :id and game_id = :game', array(':id' => $game->teamHome0->id, ':game' => $game->id)));
        if ($home == null)
            $home = unserialize(_app()->session['hEvents']);
        $away = $this->createPreview(GamePlayer::model()->with('player')->findAll(
                'player.team_id = :id and game_id = :game', array(':id' => $game->teamAway0->id, ':game' => $game->id)));
        if ($away == null)
            $away = unserialize(_app()->session['aEvents']);

        if ($hScore != null) {
            $game->score_home = $hScore;
        }
        if ($aScore != null) {
            $game->score_away = $aScore;
        }

        $this->render('stats', array(
            'game' => $game,
            'homeTeam' => $homeTeam,
            'awayTeam' => $awayTeam,
            'events' => $events,
            'homePlayers' => $homePlayers,
            'awayPlayers' => $awayPlayers,
            'hInputs' => $hInputs,
            'aInputs' => $aInputs,
            'home' => $home,
            'away' => $away,
        ));
    }

    public function actionPreview($data=false) {

        _cs()->registerCoreScript('jquery.ui');
        AdminUtils::registerAdminCssFile('jui/base/jquery-ui');
        AdminUtils::registerAdminJsFile('points');

        if ($data) {
            $_POST['HomePlayer'] = $data['HomePlayer'];
            $_POST['AwayPlayer'] = $data['AwayPlayer'];
            $_POST['homeTeam'] = Team::model()->findByAttributes(array('xml_id' => $data['homeTeam']))->id;
            $_POST['awayTeam'] = Team::model()->findByAttributes(array('xml_id' => $data['awayTeam']))->id;
            $_POST['homeScore'] = $data['homeScore'];
            $_POST['awayScore'] = $data['awayScore'];
            $game = Game::model()->findByPk($data['game']);
        } else {
            if (isset($_POST['game']))
                $game = Game::model()->findByPk($_POST['game']);
            else
                throw new Exception('Error getting game info');
        }

        if (isset($_POST['HomePlayer']) && isset($_POST['AwayPlayer']) && isset($_POST['homeScore']) && isset($_POST['awayScore'])) {
            $t = _app()->db->beginTransaction();

            try {

                $homeGoals = $awayGoals = 0;
                if (!is_numeric($_POST['homeScore']) || !is_numeric($_POST['awayScore']))
                    throw new Exception("You must enter a score for the game");
                $game->score_home = $_POST['homeScore'];
                $game->score_away = $_POST['awayScore'];
                if (!$game->save(null, array('score_home', 'score_away')))
                    throw new Exception("Error saving game");

                foreach ($_POST['HomePlayer'] as $homePlayer) {
                    $this->savePlayer($homePlayer, true, $game);
                    if ($homePlayer['event']['GoalsScored'])
                        $homeGoals += $homePlayer['event']['GoalsScored'];
                    if ($homePlayer['event']['OwnGoals'])
                        $awayGoals += $homePlayer['event']['OwnGoals'];
                }
                foreach ($_POST['AwayPlayer'] as $awayPlayer) {
                    $this->savePlayer($awayPlayer, false, $game);
                    if ($awayPlayer['event']['GoalsScored'])
                        $awayGoals += $awayPlayer['event']['GoalsScored'];
                    if ($awayPlayer['event']['OwnGoals'])
                        $homeGoals += $awayPlayer['event']['OwnGoals'];
                }

                if ($homeGoals != $game->score_home || $awayGoals != $game->score_away)
                    throw new Exception("Goals entered do not match score");

                $t->commit();
            } catch (CDbException $e) {
                $t->rollback();
//                error_log($e);
                _user()->setFlash('failure', $e->getMessage());
                if ($data)
                    $this->redirect(AdminUtils::aUrl('game/index'));
                else {
                    _app()->session['hEvents'] = serialize($_POST['HomePlayer']);
                    _app()->session['aEvents'] = serialize($_POST['AwayPlayer']);
                    _app()->session['gameId'] = $game->id;
                    $this->redirect(AdminUtils::aUrl('game/stats', array('id' => $game->id, 'hScore' => $game->score_home, 'aScore' => $game->score_away)));
                }
            } catch (NotExistException $e) {
                $t->rollback();
//                error_log($e);
                _user()->setFlash('failure', $e->getMessage());
                $this->redirect(AdminUtils::aUrl('game/index'));
            } catch (Exception $e) {
                $t->rollback();
//                error_log($e);
                _user()->setFlash('failure', $e->getMessage());
                if ($data)
                    $this->redirect(AdminUtils::aUrl('game/index'));
                else {
                    _app()->session['hEvents'] = serialize($_POST['HomePlayer']);
                    _app()->session['aEvents'] = serialize($_POST['AwayPlayer']);
                    _app()->session['gameId'] = $game->id;
                    $this->redirect(AdminUtils::aUrl('game/stats', array('id' => $game->id, 'hScore' => $game->score_home, 'aScore' => $game->score_away)));
                }
            }

            $home = $this->createPreview(GamePlayer::model()->with('player')->findAll(
                    'player.team_id = :id and game_id = :game', array(':id' => $game->teamHome0->id, ':game' => $game->id)));
            $away = $this->createPreview(GamePlayer::model()->with('player')->findAll(
                    'player.team_id = :id and game_id = :game', array(':id' => $game->teamAway0->id, ':game' => $game->id)));
            $events = Event::model()->findAll(array('select' => 'distinct(name) as name', 'condition' => 'is_team_event = 0'));

            Yii::log(_user()->username . " entered data for game with id: {$game->id}", 'info', 'actions');
        } else {
            _user()->setFlash('failure', 'You must enter a score and add players to the team');
            $this->redirect(AdminUtils::aUrl('game/stats', array('id' => $game->id, 'hScore' => $game->score_home, 'aScore' => $game->score_away)));
        }

        foreach ($events as $e)
            $homeTotals[$e->name] = $awayTotals[$e->name] = 0;

        $this->unsetPosts();

        $this->render('preview', array(
            'game' => $game,
            'home' => $home,
            'away' => $away,
            'events' => $events,
            'homeTotals' => $homeTotals,
            'awayTotals' => $awayTotals,));
    }

    public function actionUpload() {

        if (isset($_REQUEST['id']))
            $id = $_REQUEST['id'];
        else
            $id = null;

        if (isset($_POST['id'])) {
            $file = CUploadedFile::getInstanceByName('xml');
            if ($file->saveAs('xml/' . $file->name)) {
                try {
                    $data = $this->processFile($file->name, $id);
                } catch (Exception $e) {
                    _user()->setFlash('failure', $e->getMessage());
                    $this->redirect(AdminUtils::aUrl('game/index'));
                }
                if ($data)
                    $this->actionPreview($data);
            }
        }

        if (_app()->request->isAjaxRequest) {
            $this->renderPartial('_upload', array('id' => $id));
        }
    }

    public function actionToggleComments($id) {
        // Get match discussion connection
        $matchDiscussion = MatchDiscussion::model()->findByAttributes(array('game_id' => $id));

        // If the match has no discussion create one.
        if ($matchDiscussion == null) {
            $game = $this->loadModel($id);

            $disc = new Discussion;
            $disc->comments_open = false; // This is set to false because the default is true, so toggling it would set it to false.
            if (!$disc->save()) {
                throw new Exception(Debug::arObjectErrors($disc));
            }
            $matchDiscussion = new MatchDiscussion;
            $matchDiscussion->discussion_id = $disc->id;
            $matchDiscussion->game_id = $game->id;
            if (!$matchDiscussion->save()) {
                throw new Exception(Debug::arObjectErrors($matchDiscussion));
            }
        } else {
            // Toggle comments
            $matchDiscussion->discussion->comments_open = !$matchDiscussion->discussion->comments_open;
            if (!$matchDiscussion->discussion->save(true, array('comments_open'))) {
                _user()->setFlash('failure', Debug::arObjectErrors($matchDiscussion->discussion));
            }
        }

        $this->redirect(AdminUtils::aUrl('game/index'));
    }

    private function processFile($filename, $id) {
        $game = Game::model()->findByPk($id);
        $xml = @simplexml_load_file('xml/' . $filename);
        if (!$xml)
            throw new Exception('The XML file you uploaded is invalid');

        if (!$xml->game)
            throw new Exception('The XML file you uploaded is either invalid or contains no data');

        $data['homeTeam'] = (int) $xml->game->hometeam->general->team_id;
        $data['awayTeam'] = (int) $xml->game->awayteam->general->team_id;
        $data['homeScore'] = (int) $xml->game->hometeam->teamstats->total_stats->goals;
        $data['awayScore'] = (int) $xml->game->awayteam->teamstats->total_stats->goals;
        $data['game'] = $game->id;

        if ($data['homeTeam'] != $game->teamHome0->xml_id || $data['awayTeam'] != $game->teamAway0->xml_id)
            throw new Exception('XML teams don\'t match the teams of the selected match.');

        $i = 1;
        $j = 1;

        foreach ($xml->game->hometeam->playerstats->player as $homePlayer) {
            if ((int) $homePlayer->total_stats->time_of_play > 0) {
                $data['HomePlayer'][$i]['xml_id'] = (int) $homePlayer->general->playerid;
                $data['HomePlayer'][$i]['name'] = (string) $homePlayer->general->playerlastgre . ' ' . (string) $homePlayer->general->playerfirstgre;
                $data['HomePlayer'][$i]['event']['MinutesPlayed'] = (int) $homePlayer->total_stats->time_of_play;
                $data['HomePlayer'][$i]['event']['GoalsScored'] = (int) $homePlayer->total_stats->goals;
                $data['HomePlayer'][$i]['event']['RedCards'] = (int) $homePlayer->total_stats->cards->red;
                $data['HomePlayer'][$i]['event']['MissedPenalties'] = (int) $homePlayer->total_stats->missed->pen;
                $data['HomePlayer'][$i]['event']['Assists'] = (int) $homePlayer->total_stats->assists->goal;
                $data['HomePlayer'][$i]['event']['PenaltiesSaved'] = (int) $homePlayer->total_stats->goalkeeper_stats->penalties_saved;
                $data['HomePlayer'][$i]['event']['OwnGoals'] = (int) $homePlayer->total_stats->own_goals;
                ++$i;
            }
        }

        foreach ($xml->game->awayteam->playerstats->player as $awayPlayer) {
            if ((int) $awayPlayer->total_stats->time_of_play > 0) {
                $data['AwayPlayer'][$j]['xml_id'] = (int) $awayPlayer->general->playerid;
                $data['AwayPlayer'][$j]['name'] = (string) $awayPlayer->general->playerlastgre . ' ' . (string) $awayPlayer->general->playerfirstgre;
                $data['AwayPlayer'][$j]['event']['MinutesPlayed'] = (int) $awayPlayer->total_stats->time_of_play;
                $data['AwayPlayer'][$j]['event']['GoalsScored'] = (int) $awayPlayer->total_stats->goals;
                $data['AwayPlayer'][$j]['event']['RedCards'] = (int) $awayPlayer->total_stats->cards->red;
                $data['AwayPlayer'][$j]['event']['MissedPenalties'] = (int) $awayPlayer->total_stats->missed->pen;
                $data['AwayPlayer'][$j]['event']['Assists'] = (int) $awayPlayer->total_stats->assists->goal;
                $data['AwayPlayer'][$j]['event']['PenaltiesSaved'] = (int) $awayPlayer->total_stats->goalkeeper_stats->penalties_saved;
                $data['AwayPlayer'][$j]['event']['OwnGoals'] = (int) $awayPlayer->total_stats->own_goals;
                ++$j;
            }
        }

        return $data;
    }

    /**
     * Creates a string of html dropdown options from an array of items
     * @param array An array of items to iterate and create dropdown string
     * @return string The html with the dropdown options
     */
    public function createDropdownOptions($array) {
        $options = '';
        foreach ($array as $item) {
            $options .= '<option value=\"' . $item->id . '\">' . $item->shortname . '</option>';
        }
        return $options;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Game::model()->findByPk((int) $id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Removes all spaces from a string
     * @param string $text
     * @return string
     */
    public function sanitizeText($text) {
        return str_replace(' ', '', $text);
    }

    /**
     * Adds a space before capital letters in string
     * @param string $text
     * @return string
     */
    public function unsanitizeText($text) {
        return trim(preg_replace('/([A-Z])/', ' \\1', $text));
    }

    /**
     * Saves the team events per player
     * @param integer $playerId
     * @param integer $posId
     * @param integer $gameId
     * @param integer $homeScore
     * @param integer $awayScore
     * @param boolean $isHome
     */
    private function saveTeamEvents($playerId, $posId, $gameId, $homeScore, $awayScore, $isHome) {
        $ng = false;
        $cs = false;
        $ga = 0;

        if ($isHome) {
            if ($homeScore == 0)
                $ng = true;
            if ($awayScore == 0)
                $cs = true;
            $ga = $awayScore;
        }else {
            if ($awayScore == 0)
                $ng = true;
            if ($homeScore == 0)
                $cs = true;
            $ga = $homeScore;
        }

        if ($ga > 0) {
            $eid = Event::model()->getByName('Goals Against', $posId);
            $event = GamePlayer::model()->findByAttributes(array('game_id' => $gameId, 'event_id' => $eid, 'player_id' => $playerId));
            if ($event == null)
                $event = new GamePlayer;

            $event->event_id = $eid;
            $event->game_id = $gameId;
            $event->player_id = $playerId;
            $event->event_value = $ga;
            $event->entered = _app()->localtime->getLocalNow('Y-m-d H:i:s');
            $event->is_home_team = $isHome;

            if (!$event->save())
                throw new Exception(Debug::arObjectErrors($event));
        }

        if ($ng) {
            $eid = Event::model()->getByName('No Goals', $posId);
            $event = GamePlayer::model()->findByAttributes(array('game_id' => $gameId, 'event_id' => $eid, 'player_id' => $playerId));
            if ($event == null)
                $event = new GamePlayer;
            $event->event_id = $eid;
            $event->game_id = $gameId;
            $event->player_id = $playerId;
            $event->event_value = 1;
            $event->entered = _app()->localtime->getLocalNow('Y-m-d H:i:s');
            $event->is_home_team = $isHome;

            if (!$event->save())
                throw new Exception(Debug::arObjectErrors($event));
        }

        if ($cs) {
            $eid = Event::model()->getByName('Clean Sheet', $posId);
            $event = GamePlayer::model()->findByAttributes(array('game_id' => $gameId, 'event_id' => $eid, 'player_id' => $playerId));
            if ($event == null)
                $event = new GamePlayer;
            $event->event_id = $eid;
            $event->game_id = $gameId;
            $event->player_id = $playerId;
            $event->event_value = 1;
            $event->entered = _app()->localtime->getLocalNow('Y-m-d H:i:s');
            $event->is_home_team = $isHome;

            if (!$event->save())
                throw new Exception(Debug::arObjectErrors($event));
        }
    }

    /**
     * Creates the preview array for the player events
     * @param array An array of GamePlayer objects
     * @return array The array of the events formatted or false
     */
    public function createPreview($model) {
        $events = array();
        foreach ($model as $player) {
            $events[$player->player->id]['name'] = $player->player->shortname;
            $events[$player->player->id]['id'] = $player->player->id;
            switch ($player->event->name) {
                case 'Minutes Played':
                    $events[$player->player->id]['event'][$this->sanitizeText($player->event->name)] = $player->event_value;
                    break;
                case 'Goals Scored':
                    $events[$player->player->id]['event'][$this->sanitizeText($player->event->name)] = $player->event_value;
                    break;
                case 'Assists':
                    $events[$player->player->id]['event'][$this->sanitizeText($player->event->name)] = $player->event_value;
                    break;
                case 'Penalties Saved':
                    $events[$player->player->id]['event'][$this->sanitizeText($player->event->name)] = $player->event_value;
                    break;
                case 'Red Cards':
                    $events[$player->player->id]['event'][$this->sanitizeText($player->event->name)] = $player->event_value;
                    break;
                case 'Own Goals':
                    $events[$player->player->id]['event'][$this->sanitizeText($player->event->name)] = $player->event_value;
                    break;
                case 'Missed Penalties':
                    $events[$player->player->id]['event'][$this->sanitizeText($player->event->name)] = $player->event_value;
                    break;
            }
        }

        return $events != null ? $events : false;
        ;
    }

    /**
     * Saves a player's events in the database
     * @param array $p
     * @param boolean $isHome
     * @param Game $game
     */
    private function savePlayer($p, $isHome, Game $game) {
        if (isset($p['xml_id']))
            $player = Player::model()->findByAttributes(array('xml_id' => $p['xml_id']));
        else
            $player = Player::model()->findByPk($p['id']);

        if ($player == null)
            throw new NotExistException('Player <b>' . $p['name'] . '</b> with xml id <b>' . $p['xml_id'] . '</b> does not exist. Please enter the XML id for that player.');

        if ($game->team_home != $player->team_id && $game->team_away != $player->team_id)
            throw new NotExistException('Player <b>' . $player->name . ' (XML ID: ' . $player->xml_id . ')</b> is currently playing for ' . $player->team->name . '. ');

        foreach ($p['event'] as $ev => $value) {
            if ($ev == 'MinutesPlayed' && $value == 0)
                throw new Exception('You must enter minutes played for all players');
            $eid = Event::model()->getByName($this->unsanitizeText($ev), $player->basicPosition->id);
            $event = GamePlayer::model()->find('player_id = :player and game_id = :game and event_id = :event', array(
                ':player' => $player->id,
                ':game' => $game->id,
                ':event' => $eid
                ));
            if ($value != null) {
                if ($event == null)
                    $event = new GamePlayer;

                $event->event_id = $eid;
                $event->game_id = $game->id;
                $event->player_id = $player->id;
                $event->event_value = $value;
                $event->entered = _app()->localtime->getLocalNow('Y-m-d H:i:s');
                $event->is_home_team = $isHome;

                if (!$event->save())
                    throw new Exception(Debug::arObjectErrors($event));
            } else {
                if ($event != null) {
                    if (!$event->delete())
                        throw new Exception(Debug::arObjectErrors($event));
                }
            }
        }
//        if (!$edit)
        $this->saveTeamEvents($player->id, $player->basicPosition->id, $game->id, $game->score_home, $game->score_away, $isHome);
    }

    public function actionDeletePlayer() {
        if (_app()->request->isAjaxRequest) {
            $t = _app()->db->beginTransaction();

            try {
                $event = GamePlayer::model()->findAll('player_id = :player and game_id = :game', array(':player' => $_POST['id'], ':game' => $_POST['game']));
                foreach ($event as $e) {
                    if (!$e->delete())
                        throw new Exception(Debug::arObjectErrors($e));
                }
                $t->commit();
                echo true;
            } catch (CDbException $e) {
                $t->rollback();
                echo false;
            } catch (Exception $e) {
                $t->rollback();
                echo false;
                ;
            }
        }
    }

    public function actionPoints($id) {
        if (self::isScriptLocked()) {
            Utils::jsonReturn(array('status' => 'locked'));
            return;
        }
        
        self::lockScript();
        
        $game = Game::model()->findByPk($id);

        if ($game->points_calculated) {
            self::unlockScript();
            Utils::jsonReturn(array('status' => 'locked'));
            return;
        }

        $game->points_calculated = true;
        $game->save(false, array('points_calculated'));

        if ($this->playerPoints($game)) {
            if ($this->managerPoints($game)) {
                self::unlockScript();
                Utils::jsonReturn(array('status' => 'ok'));
                return;
            }
        }
        self::unlockScript();
        Utils::jsonReturn(array('status' => 'locked'));
        return;
    }

    private function playerPoints($game) {

        $players = Player::model()->findAll(array(
            'select' => 't.id, t.total_points, t.basic_position_id, t.delist_date, t.current_value, t.vmi_season',
            'condition' => 'list_date is not null and list_date < current_timestamp()',
            'order' => 't.id asc'
            ));

        $t = _app()->db->beginTransaction();

        try {
            foreach ($players as $player) {
                $points = 0;
                if ($player->delist_date != null && $player->delist_date < $game->date_played)
                    $points = null;
                else {
                    $gamePlayer = GamePlayer::model()->findAll(array(
                        'join' => 'left join event ev on ev.id = t.event_id',
                        'condition' => 'player_id = :player and ev.basic_position_id = :pos and game_id = :game ',
                        'params' => array(':player' => $player->id, ':game' => $game->id, ':pos' => $player->basic_position_id)
                        ));
                    $playerHistory = PlayerPointHistory::model()->find(array(
                        'select' => 'points',
                        'condition' => 'date < current_timestamp() and player_id = :player',
                        'params' => array(':player' => $player->id),
                        'order' => 'date desc',
                        'limit' => 1,
                        'offset' => 1
                        ));

                    if ($gamePlayer) {
                        foreach ($gamePlayer as $gp) {
                            if ($gp->event_value != null) {
                                if ($gp->event->name == 'Minutes Played') {
                                    if ($gp->event_value < 60)
                                        $points += $gp->event->points_awarded / 2;
                                    else
                                        $points += $gp->event->points_awarded;
                                } else {
                                    $points += $gp->event_value * $gp->event->points_awarded;
                                }
                            }
                        }
                    }
                }

                $pph = PlayerPointHistory::model()->find(array(
                    'condition' => 'date(convert_tz(date,"UTC",:tz)) = date(:now) and player_id = :player',
                    'params' => array(':tz' => _timezone(), ':player' => $player->id, ':now' => $game->date_played)
                    ));

                if ($pph != null) {
                    $tempPlayerPoints = $pph->points;
                }

                if ($points == 0 && $pph != null) {
                    
                } else {
                    if ($pph == null)
                        $pph = new PlayerPointHistory;
                    $pph->date = $game->date_played;
                    $pph->player_id = $player->id;
                    $pph->points += $points;
                }

                if (!$pph->save())
                    throw new Exception(Debug::arObjectErrors($pph));

                $player->total_points += $points;
                $player->vmi_season = round(($player->total_points / $player->current_value) * 100, 2);

                if (!$player->save(null, array('total_points', 'vmi_season')))
                    throw new Exception(Debug::arObjectErrors($player));

                unset($gamePlayer);
                unset($playerHistory);
            }

            $t->commit();

            unset($players);

            Yii::log(_user()->username . " calculated player points for game with id: {$game->id}", 'info', 'actions');

            return true;
        } catch (CDbException $e) {
            $t->rollback();
            _user()->setFlash('failure', 'There was a critical error in updating players\' points. Error was: ' . $e->getMessage());
        } catch (Exception $e) {
            $t->rollback();
            _user()->setFlash('failure', 'There was a critical error in updating players\' points. Error was: ' . $e->getMessage());
        }
        return false;
    }

    private function managerPoints($game) {
        $t = _app()->db->beginTransaction();

        $tz = _timezone();

        try {

            $criteria = new CDbCriteria;
            $criteria->join = 'left join manager_team mt on mt.id = t.manager_team_id';
            $criteria->condition = 'mt.created_on <
                        (select date_played from game where date_played=
                                (select min(date_played) from game where date(convert_tz(date_played,"UTC",:tz)) = date(:now)) limit 1)
                        - interval 15 minute
                    and manager_team_id is not null';
            $criteria->order = 't.id asc';
            $criteria->params = array(':tz' => $tz, ':now' => $game->date_played);
            $criteria->addInCondition('role', array(Role::MANAGER));
            $managers = Manager::model()->findAll($criteria);
            foreach ($managers as $manager) {
                $p = ManagerTeamPlayerHistory::model()->find(array(
                    'select' => 'sum(pph.points * t.shares * st.coefficient) as points',
                    'join' => 'left join (player pl left join player_point_history pph on pl.id = pph.player_id) on pl.id = t.player_id left join status st on st.id = t.status_id',
                    'condition' => 't.manager_id = :manager and date(convert_tz(pph.date,"UTC",:tz)) = date(:now) and date(convert_tz(t.date,"UTC",:tz)) = date(:now)',
                    'params' => array(':tz' => $tz, ':now' => $game->date_played, ':manager' => $manager->id)
                    ));

                $mph = ManagerPointHistory::model()->find(array(
                    'condition' => 'date(convert_tz(date,"UTC",:tz)) = date(:now) and manager_id = :manager',
                    'params' => array(':tz' => $tz, ':manager' => $manager->id, ':now' => $game->date_played)
                    ));

                if ($p != null && $mph != null)
                    $manager->total_points += $p->points - $mph->total_points;
                else
                    $manager->total_points += $p->points;

                if ($p->points == 0 && $mph != null) {
                    
                } else {
                    if ($mph == null)
                        $mph = new ManagerPointHistory;
                    $mph->date = $game->date_played;
                    $mph->manager_id = $manager->id;
                    $mph->total_points = $p->points;
                }
                $mph->save(false);
                $manager->save(false, array('total_points'));

                $mph = null;
            }

            Rank::updateRanks();

            Yii::log(_user()->username . " calculated manager points for game with id: {$game->id}", 'info', 'actions');

            $t->commit();
            return true;
        } catch (CDbException $e) {
            $t->rollback();
            _user()->setFlash('failure', 'There was a critical error in updating managers\' points. Error was: ' . $e->getMessage());
            return false;
        }
    }

    public function actionError() {
        $this->render('error');
    }

    public function actionAdmin() {
        _cs()->registerCoreScript('jquery');
        _cs()->registerCoreScript('jquery.ui');
        AdminUtils::registerAdminCssFile('jui/base/jquery-ui');
        AdminUtils::registerAdminJsFile('deletePoints');

        $search = array('team_home' => '', 'team_away' => '', 'date' => '');

        if (isset($_POST['search'])) {
            $cr = new CDbCriteria;
            $cr->condition = 'points_calculated = 1';
            $cr->params = array();
            if ($_POST['search']['date'] != '') {
                $cr->addCondition('date(convert_tz(date_played,"UTC",:tz)) = :date');
                $cr->params = array_merge($cr->params, array(':date' => $_POST['search']['date'], ':tz' => _timezone()));
            }
            if ($_POST['search']['team_home'] != '') {
                $cr->addCondition('team_home = :th');
                $cr->params = array_merge($cr->params, array(':th' => $_POST['search']['team_home']));
            }
            if ($_POST['search']['team_away'] != '') {
                $cr->addCondition('team_away = :ta');
                $cr->params = array_merge($cr->params, array(':ta' => $_POST['search']['team_away']));
            }
            $search = $_POST['search'];
            $m = Game::model()->findAll($cr);
        } else {
            $m = Game::model()->findAll(array('order' => 'date_played asc', 'condition' => 'points_calculated = 1'));
        }

        $this->render('admin', array('m' => $m, 'search' => $search));
    }

    private function unsetPosts() {
        _app()->session->remove('aEvents');
        _app()->session->remove('hEvents');
        _app()->session->remove('gameId');
    }

    private function removePlayerPoints($game) {
        $pointsSubPerPlayer = array();

        //Remove points from players in this match
        $players = GamePlayer::model()->findAll(array(
            'condition' => 'game_id = :game AND p.list_date IS NOT NULL AND p.list_date < now()',
            'params' => array(':game' => $game->id),
            'group' => 'player_id',
            'join' => 'LEFT JOIN player p ON p.id = t.player_id'
            ));
        foreach ($players as $gamePlayer) {
            $ph = PlayerPointHistory::model()->find(array(
                'condition' => 'date(convert_tz(date,"UTC",:tz)) = date(:date) and player_id = :pl',
                'params' => array(':tz' => _timezone(), ':date' => $game->date_played, ':pl' => $gamePlayer->player->id)
                ));
            if ($ph != null) {
                $pointsToSubtract = $ph->points;
                $ph->points = 0;
                $ph->save(false, array('points'));
            }
            $pl = $gamePlayer->player;

//            $sum = PlayerPointHistory::model()->find(array(
//                'select' => 'sum(points) as sum, count(id) as count',
//                'condition' => 'player_id = :player AND points IS NOT NULL',
//                'params' => array(':player' => $pl->id)
//                    ));
//
//            if ($sum->count == 0)
//                $avg = $pl->total_points;
//            else
//                $avg = ($sum->sum) / ($sum->count);
            $pl->total_points -= $pointsToSubtract;
            $pl->vmi_season = round(($pl->total_points / $pl->current_value) * 100, 2);

            $pl->save(false, array('total_points', 'vmi_season'));

            $pointsSubPerPlayer[$pl->id] = $pointsToSubtract;
        }

        return $pointsSubPerPlayer;
    }

    function removeManagerPoints($game, $pointsSubPerPlayer) {
        //Remove points from managers
        $criteria = new CDbCriteria;
        $criteria->addInCondition('role', array(Role::MANAGER));
        $managers = Manager::model()->findAll($criteria);
        foreach ($managers as $manager) {
            $managerPointsToSubtract = 0;

            $managerTeamPlayers = ManagerTeamPlayerHistory::model()->findAll(array(
                'condition' => 'player_id in (select player_id from game_player gp where game_id = :id group by player_id) and manager_id = :mid and date(convert_tz(date,"UTC",:tz)) = date(:date)',
                'params' => array(':id' => $game->id, ':mid' => $manager->id, ':date' => $game->date_played, ':tz' => _timezone())
                ));
            foreach ($managerTeamPlayers as $mtp) {
                if (isset($pointsSubPerPlayer[$mtp->player_id])) {
                    $managerPointsToSubtract += $pointsSubPerPlayer[$mtp->player_id] * $mtp->shares * $mtp->status->coefficient;
                }
            }
            $mh = ManagerPointHistory::model()->find(array(
                'condition' => 'manager_id = :mid and date(convert_tz(date,"UTC",:tz)) = date(:date)',
                'params' => array(':tz' => _timezone(), ':date' => $game->date_played, ':mid' => $manager->id)
                ));
            if ($mh != null) {
                $mh->total_points -= $managerPointsToSubtract;
                $mh->save(false, array('total_points'));
            }
            $manager->total_points -= $managerPointsToSubtract;
            $manager->save(false, array('total_points'));
        }

        return true;
    }

    public function actionCancel($id) {
        $game = Game::model()->findByPk($id);
        $game->is_cancelled = $game->points_calculated = true;
        $game->update();

        $this->redirect(AdminUtils::aUrl('game/index'));
    }

    public static function getTeams() {
        return CHtml::listData(Team::model()->findAll(), 'id', 'name');
    }
    
    private static function lockScript() {
        return file_put_contents('script_flag', '1');
    }
    
    private static function unlockScript() {
        return file_put_contents('script_flag', '0');
    }
    
    public static function isScriptLocked() {
        return file_get_contents('script_flag');
    }

}
