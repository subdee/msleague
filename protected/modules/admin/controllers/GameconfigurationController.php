<?php

class GameconfigurationController extends AdminController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
//	public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'expression' => 'User::hasRole(AdminRole::ADMIN)',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        _cs()->registerCoreScript('jquery.ui');
        AdminUtils::registerAdminJsFile('gameconfiguration');
        AdminUtils::registerAdminCssFile('jui/base/jquery-ui');

        $gc = Gameconfiguration::model()->find();

        if (isset($_POST['Gameconfiguration'])) {
            $gc->attributes = $_POST['Gameconfiguration'];
            if (!$gc->save())
                error_log('Couldn\'t save game configuration');
            _app()->localtime->TimeZone = _timezone();
        }

        $files = array();
        $handle = opendir('.');
        while (false !== ($file = readdir($handle))) {
            if (strpos($file, 'ranks') !== false)
                $files[_bu() . '/' . $file] = $file;
        }

        $this->render('index', array(
            'gc' => $gc,
            'timezones' => Timezone::model()->findAll(),
            'files' => $files
        ));
    }

    public function actionParameters() {
        _cs()->registerCoreScript('jquery.ui');
        AdminUtils::registerAdminJsFile('gameconfiguration');
        AdminUtils::registerAdminCssFile('jui/base/jquery-ui');

        $gc = Gameconfiguration::model()->find();
        $noDisplay = array(
            'is_enabled',
            'id',
            'is_season_open',
            'timezone_id',
            'game_name',
            'support_email',
            'siteAdminUtils::aUrl',
            'noreply_email'
        );

        if (isset($_POST['Gameconfiguration'])) {
            $gc->attributes = $_POST['Gameconfiguration'];
            if (!$gc->save())
                error_log('Couldn\'t save game configuration');
        }

        $this->render('parameters', array(
            'gc' => $gc,
            'noDisplay' => $noDisplay
        ));
    }

    public function actionPointSystem() {

        if (isset($_POST['pe'])) {
            $t = _app()->db->beginTransaction();
            try {
                $pe = $_POST['pe'];
                foreach ($pe as $position => $details) {
                    foreach ($details as $event => $points) {
                        $eModel = Event::model()->with('basicPosition')->find('basicPosition.name = :pos and t.name = :event', array(':pos' => $position, 'event' => $event));
                        $eModel->points_awarded = $points;
                        if (!$eModel->save()) {
                            throw new Exception('Event could not be saved');
                        }
                    }
                }
                $t->commit();
            } catch (Exception $e) {
                $t->rollback();
                error_log($e->getMessage());
            }
        }

        $ev = Event::model()->findAll(array('order' => 'name asc', 'group' => 'name'));
        $all = Event::model()->findAll();
        $pos = BasicPosition::model()->findAll();

        foreach ($all as $a) {
            $i[$a->name][$a->basicPosition->name] = $a->points_awarded;
        }

        $this->render('pointSystem', array('ev' => $ev, 'pos' => $pos, 'i' => $i));
    }

    public function actionFeatures() {
        $gc = Gameconfiguration::model()->find();

        $this->render('features', array(
            'pitchClass' => $gc->is_enabled,
            'seasonClass' => $gc->is_season_open,
            'registrationClass' => $gc->is_registration_open,
            'gc' => $gc
        ));
    }

    public function actionScripts() {
        _cs()->registerCoreScript('jquery.ui');
        AdminUtils::registerAdminJsFile('gameconfiguration');
        AdminUtils::registerAdminCssFile('jui/base/jquery-ui');

        $sc = ScriptConfiguration::model()->find();

        if (isset($_POST['ScriptConfiguration'])) {
            $sc->attributes = $_POST['ScriptConfiguration'];
            $sc->active_options = $_POST['ScriptConfiguration']['active1'] . ',' . $_POST['ScriptConfiguration']['active2'];
            $sc->burned_options = $_POST['ScriptConfiguration']['burned1'] . ',' . $_POST['ScriptConfiguration']['burned2'];
            if (!$sc->save())
                error_log('Couldn\'t save script configuration');
        }

        $log = array();
        $fileError = false;
        $file = @fopen(_bp('runtime/script.log'), 'r');
        if (!$file)
            $fileError = 'The logfile can not be found!';
        else {
            while ($line = fgets($file)) {
                $log[] = trim($line);
            }
            fclose($file);
        }

        $scriptLog = '';
        for ($i = count($log) - 1; $i >= 0; --$i) {
            if (strpos($log[$i], 'Error') === false && strpos($log[$i], 'error') === false) {
                $scriptLog .= trim($log[$i]) . '<br/>';
            } else {
                Yii::log("An error has appeared in the scripts: " . trim($log[$i]), 'info', 'actions');
                $scriptLog .= '<span class="error">' . trim($log[$i]) . '<br/></span>';
            }
        }

        $active = split(',', $sc->active_options);
        $burned = split(',', $sc->burned_options);

        $this->render('scripts', array(
            'sc' => $sc,
            'scriptLog' => $scriptLog,
            'fileError' => $fileError,
            'active' => $active,
            'burned' => $burned
        ));
    }

    public function actionResetManagers() {
        Gameconfiguration::model()->updateAll(array(
            'is_season_open' => false
        ));
        $t = _app()->db->beginTransaction();

        Yii::log(_user()->username . " started resetting managers...", 'info', 'actions');
        
        try {
            $contents = "Rank,Username,Points,Email,Full Name,Mobile\n";
            $ranks = Rank::model()->with('manager')->findAll(array('order' => 'rank asc'));
            foreach ($ranks as $rank) {
                $contents .= "{$rank->rank},{$rank->manager->cleanUsername},{$rank->manager->total_points},{$rank->manager->email},{$rank->manager->fullname},";
                if (_isModuleOn('sms') && $rank->manager->mobile)
                    $contents .= "{$rank->manager->mobile->mobile}";
                $contents .= "\n";
            }

            file_put_contents('ranks_' . date('Y_m_d') . '.csv', $contents);

            Yii::log("...saved ranks", 'info', 'actions');
            
            Manager::model()->updateAll(array(
                'total_points' => 0,
                'portfolio_share_value' => 0,
                'portfolio_cash' => 1000,
                'role' => Role::RESET
                ), 'role = :role', array(':role' => Role::MANAGER));
            Manager::model()->updateAll(array(
                'total_points' => 0,
                'portfolio_share_value' => 0,
                'portfolio_cash' => 1000,
                ), 'role = :role', array(':role' => Role::TEMPORARY));
            Manager::model()->updateAll(array(
                'total_points' => 0,
                'portfolio_share_value' => 0,
                'portfolio_cash' => 1000,
                ), 'role = :role', array(':role' => Role::RESET));
            Yii::log("...updated managers", 'info', 'actions');
            ManagerTeamPlayer::model()->deleteAll();
            ManagerTeamPlayerHistory::model()->deleteAll();
            Yii::log("...deleted teams", 'info', 'actions');
            ManagerDailyHistory::model()->deleteAll();
            ManagerPointHistory::model()->deleteAll();
            Yii::log("...deleted histories", 'info', 'actions');
            $totalShares = Gameconfiguration::model()->get('total_shares_per_player');
            Player::model()->updateAll(array(
                'bank_shares' => $totalShares
            ));
            $players = Player::model()->findAll('list_date is not null and list_date < now()');
            foreach ($players as $player) {
                $pdh = new PlayerDailyHistory;
                $pdh->player_id = $player->id;
                $pdh->value = $player->current_value;
                $pdh->date = Utils::getUserTime('Y-m-d H:i');
                $pdh->bought = 0;
                $pdh->save();
            }
            Yii::log("...updated players", 'info', 'actions');
            Transaction::model()->deleteAll();
            Yii::log("...removed transactions", 'info', 'actions');
            Rank::updateRanks();
            Yii::log("...updated ranks", 'info', 'actions');
            
            $t->commit();
            
            _user()->setFlash('success', 'All managers have been reset!');
        } catch (CDbException $e) {
            $t->rollback();
            _user()->setFlash('failure', $e->getMessage());
        }

        Gameconfiguration::model()->updateAll(array(
            'is_season_open' => true
        ));
        $this->redirect($this->createUrl('index'));
    }

    public function actionAddFanLeagues() {
        if (CustomLeague::model()->updateFanLeagues() && CustomLeague::model()->updateFanLeagueManagers())
            _user()->setFlash('success', 'Fan leagues have been updated and managers placed');
        else
            _user()->setFlash('failure', 'It failed');

        $this->redirect(AdminUtils::aUrl('gameconfiguration/features'));
    }

    public function actionAddLeagueDiscussions() {
        if (CustomLeagueDiscussion::model()->updateLeagueDiscussions())
            _user()->setFlash('success', 'League discussions have been attached.');
        else
            _user()->setFlash('failure', 'It failed');

        $this->redirect(AdminUtils::aUrl('gameconfiguration/features'));
    }

    public function actionAddMatchDiscussions() {
        if (MatchDiscussion::model()->attachMatchDiscussions())
            _user()->setFlash('success', 'Match discussions have been attached.');
        else
            _user()->setFlash('failure', 'It failed');

        $this->redirect(AdminUtils::aUrl('gameconfiguration/features'));
    }

    public function actionCreatePhotoThumbnails() {
        if (Profile::model()->createThumbnails())
            _user()->setFlash('success', 'Creating thumbnails finished.');
        else
            _user()->setFlash('failure', 'Creating thumbnails finished with errors.');
        $this->redirect(AdminUtils::aUrl('gameconfiguration/features'));
    }

    public function actionSwitchFeature($action) {
        $gc = Gameconfiguration::model()->find();
        $gc->$action = !$gc->$action;
        $gc->update();

        $img =  $gc->$action ? 'light_on.png' : 'light_off.png';
        echo CHtml::image(AdminUtils::adminImageUrl($img),null,array('title' => $gc->$action ? 'Turn off' : 'Turn on'));
        
        return;
    }

}
