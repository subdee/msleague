<?php

class MainController extends AdminController {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index'),
                'users' => '@',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex($interval = 'DAY') {
        if (_user()->id == null)
            $this->actionLogin();
        else {
            $sc = ScriptConfiguration::model()->find(array('select' => 'active_options,burned_options'));
            if ($sc != null) {
                $actOpts = split(',', $sc->active_options);
                $burnOpts = split(',', $sc->burned_options);
            }
            //Get data with one SQL query
            $conn = _app()->db;
            $sql = '
                    select
                        (select count(m.id) from manager m) as registered,
                        (select count(m.id) from manager m where role = "'.Role::MANAGER.'") as managers,
                        (select count(m.id) from manager m where role = "'.Role::TEMPORARY.'") as temporary,
                        (select count(m.id) from manager m where role = "'.Role::INACTIVE.'") as inactive,
                        (select count(b.id) from ban b where DATE_ADD(date_banned, INTERVAL duration DAY) > DATE(NOW())) as banned,
                        (select count(re.id) from report re) as reports,
                        (select count(p.id) from profile p where photo_approved = 0 and photo is not null) as photos,
                        (select count(m.id) from manager m where convert_tz(last_activity,"UTC",:tz) BETWEEN DATE_SUB(:now,INTERVAL 10 MINUTE) AND :now) as logged_in,
                        (select count(m.id) from manager m where convert_tz(last_login,"UTC",:tz) BETWEEN DATE_SUB(:now,INTERVAL 1 ' . $interval . ') AND :now) as logged,
                        (select count(tr.id) from transaction tr where convert_tz(date_completed,"UTC",:tz) BETWEEN DATE_SUB(:now,INTERVAL 1 ' . $interval . ') AND :now) as transactions,
                        (select count(m.id) from manager m where convert_tz(registration_date,"UTC",:tz) BETWEEN DATE_SUB(:now,INTERVAL 1 ' . $interval . ') AND :now) as new_users,
                        (select count(m.id) from manager m left join manager_team mt on m.manager_team_id = mt.id where convert_tz(mt.created_on,"UTC",:tz) BETWEEN DATE_SUB(:now,INTERVAL 1 ' . $interval . ') AND :now) as new_teams,
                        (select count(del.id) from deleted_manager del where convert_tz(date,"UTC",:tz) BETWEEN DATE_SUB(:now,INTERVAL 1 ' . $interval . ') AND :now) as deleted_users,
                        (select count(p.id) from profile p where gender = "M") as males,
                        (select count(p.id) from profile p where gender = "F") as females,
                        (select count(p.id) from profile p where (datediff(now(),birthdate)/365) between 18 and 25) as 18_25,
                        (select count(p.id) from profile p where (datediff(now(),birthdate)/365) between 25 and 35) as 25_35,
                        (select count(p.id) from profile p where (datediff(now(),birthdate)/365) between 35 and 45) as 35_45,
                        (select count(p.id) from profile p where (datediff(now(),birthdate)/365) between 45 and 60) as 45_60,
                        (select count(p.id) from profile p where (datediff(now(),birthdate)/365) > 60) as over_60,
                        (select count(m.id) from manager m where 
                                    convert_tz(m.last_login,"UTC",:tz) BETWEEN DATE_SUB(:now,INTERVAL ' . $actOpts[0] . ' DAY) AND :now
                                    AND ((select count(t.id) from transaction t where m.id = t.manager_id and convert_tz(t.date_completed,"UTC",:tz) BETWEEN DATE_SUB(:now,INTERVAL 5 DAY) AND :now) >= ' . $actOpts[1] . ')) as active,
                         (select count(m.id) from manager m left join manager_team mt on mt.id = m.manager_team_id where 
                                    (select (count(t.id)/(datediff(now(),mt.created_on)+1)) from transaction t left join (manager m2 left join manager_team mt on mt.id = m2.manager_team_id) on m2.id = t.manager_id where m.id = m2.id) >= ' . $burnOpts[0] . '
                ';

            if (_isModuleOn('forum'))
                $sql .= ' AND (select (count(comm.id)/datediff(now(),mt.created_on)+1) from mod_forum_comments comm left join (manager m2 left join manager_team mt on mt.id = m2.manager_team_id) on m2.id = comm.commentor_id where m.id = m2.id) >= ' . $burnOpts[1];
            $sql .= ') as top,
                    (select count(com.id) from mod_forum_comments com where convert_tz(date_posted,"UTC",:tz) BETWEEN DATE_SUB(:now,INTERVAL 1 ' . $interval . ') AND :now) as comments';

            if (_isModuleOn('message'))
                $sql .= ',(select count(mes.id) from mod_message_messages mes where convert_tz(date_sent,"UTC",:tz) BETWEEN DATE_SUB(:now,INTERVAL 1 ' . $interval . ') AND :now AND sender_id IS NOT NULL) as messages';

            $dependency = new CExpressionDependency('return true');
            $c = $conn->cache(300, $dependency)->createCommand($sql);

            $c->bindParam(':now', _app()->localtime->getLocalNow('Y-m-d H:i:s'));
            $c->bindParam(':tz', _timezone());
            
            $data = $c->queryAll();

            //Create charts for demographics
            $gender['count'][0] = (int) $data[0]['males'];
            $gender['gender'][0] = 'Male';
            $gender['count'][1] = (int) $data[0]['females'];
            $gender['gender'][1] = 'Female';

            $age['group'][0] = '18 - 25';
            $age['count'][0] = (int) $data[0]['18_25'];
            $age['group'][1] = '25 - 35';
            $age['count'][1] = (int) $data[0]['25_35'];
            $age['group'][2] = '35 - 45';
            $age['count'][2] = (int) $data[0]['35_45'];
            $age['group'][3] = '45 - 60';
            $age['count'][3] = (int) $data[0]['45_60'];
            $age['group'][4] = 'over 60';
            $age['count'][4] = (int) $data[0]['over_60'];

            $c2 = $conn->createCommand('select c.country as country, count(p.id) as count from profile p left join country c on c.id = p.country_id group by p.country_id order by count(p.id) desc');
            $countries = $c2->queryAll();
            $country = array();
            $i = 0;
            foreach ($countries as $dato) {
                $country['country'][$i] = $dato['country'];
                $country['count'][$i] = (int) $dato['count'];
                ++$i;
            }

            $c3 = $conn->createCommand('select date,active,top from statistics order by date asc');
            $actives = $c3->queryAll();
            $active = array();
            $i = 0;
            foreach ($actives as $act) {
                $active['month'][$i] = date('F', strtotime($act['date']));
                $active['count'][$i] = (int) $act['active'];
                ++$i;
            }

            $top = array();
            $i = 0;
            foreach ($actives as $act) {
                $top['month'][$i] = date('F', strtotime($act['date']));
                $top['count'][$i] = (int) $act['top'];
                ++$i;
            }

            $c4 = $conn->createCommand('select count(m.id) as count, date(convert_tz(m.registration_date,"UTC",:tz)) as date from manager m group by date(convert_tz(m.registration_date,"UTC",:tz)) order by m.registration_date desc limit 30');
            $c4->bindParam(':tz', _timezone());
            $regPerDays = $c4->queryAll();
            $regPerDays = array_reverse($regPerDays);
            $regPerDay = array();
            $i = 0;
            foreach ($regPerDays as $rpd) {
                $regPerDay['date'][$i] = date('M j', strtotime($rpd['date']));
                $regPerDay['count'][$i] = (int) $rpd['count'];
                ++$i;
            }

            $this->render('index', array(
                'data' => $data[0],
                'interval' => $interval,
                'gender' => $gender,
                'age' => $age,
                'country' => $country,
                'active' => $active,
                'top' => $top,
                'regPerDay' => $regPerDay,
                'credits' => array(
                    'text' => 'MSLeague',
                    'href' => '#'
                ),
            ));
        }
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $headers = "From: {$model->email}\r\nReply-To: {$model->email}";
                mail(Yii::app()->params['adminEmail'], $model->subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login(true))
//                $this->redirect(Yii::app()->user->returnUrl);
                $this->redirect(_url('admin/main/index'));
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(_url('admin/main/index'));
    }

}