<?php

class ManagerController extends AdminController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
//	public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists all managers.
     */
    public function actionIndex() {

        $m = new Manager('searchManagers');
        $m->unsetAttributes();

        if (isset($_GET['Manager']))
            $m->attributes = $_GET['Manager'];

        $this->render('index', array(
            'm' => $m,
        ));
    }

    /**
     * Lists all banned managers.
     */
    public function actionBanned() {

//        $m = new Manager('searchBanned');
        $m = new Ban('search');
        $m->unsetAttributes();

//        if (isset($_GET['Manager']))
//            $m->attributes = $_GET['Manager'];

        if (isset($_GET['Ban']))
            $m->attributes = $_GET['Ban'];

        $this->render('banned', array(
            'm' => $m,
        ));
    }

    /**
     * Lists all deleted managers.
     */
    public function actionDeleted() {

        $m = new DeletedManager('search');
        $m->unsetAttributes();

        if (isset($_GET['DeletedManager']))
            $m->attributes = $_GET['DeletedManager'];

        $this->render('deleted', array(
            'm' => $m,
        ));
    }

    /**
     * Lists all temporary managers.
     */
    public function actionTemps() {

        $m = new Manager('searchTemp');
        $m->unsetAttributes();

        if (isset($_GET['Manager']))
            $m->attributes = $_GET['Manager'];

        $this->render('temps', array(
            'm' => $m,
        ));
    }

    /**
     * Lists all managers.
     */
    public function actionReset() {

        $m = new Manager('searchReset');
        $m->unsetAttributes();

        if (isset($_GET['Manager']))
            $m->attributes = $_GET['Manager'];

        $this->render('reset', array(
            'm' => $m,
        ));
    }

    /**
     * Delete a photo
     * @param integer $id Manager id
     * @param boolean $report If you want report to be deleted also
     */
    public function actionDeletePhoto($id) {
        $m = Manager::model()->findByPk($id);
        $photo = 'images/manager_photos/' . $m->profile->photo;
        if (Manager::changeAttribute($id, 'photo', null, 'delete', 'profile')) {
            if ($m->profile->photo != null) {
                unlink($photo);
                Report::model()->deleteReport(ReportReason::PHOTO, $id);
            }

            echo CHtml::image(Utils::imageUrl('profile/empty_profile.png'));
            return;
//            $this->redirect(AdminUtils::aUrl('manager/view', array('id' => $id)));
        }
    }

    /**
     * Delete profile status
     * @param integer $id Manager id
     */
    public function actionDeleteLine($id) {
        if (Manager::changeAttribute($id, 'line', null, 'delete', 'profile')) {
            Report::model()->deleteReport(ReportReason::STATUS, $id);

            echo '<span>no personal message</span>';
            return;
//            $this->redirect(AdminUtils::aUrl('manager/view', array('id' => $id)));
        }
    }

    public function actionChangeNewsletter($id, $value) {
        if (Manager::changeAttribute($id, 'newsletter', $value, 'update')) {
            Report::model()->deleteReport(ReportReason::STATUS, $id);

            echo CHtml::link($value ? 'Yes' : 'No', AdminUtils::aUrl('manager/changeNewsletter', array('id' => $id, 'value' => (int) !$value)), array('id' => 'newsLink'));
            return;
//            $this->redirect(AdminUtils::aUrl('manager/view', array('id' => $id)));
        }
    }

    public function actionChangeTeamName() {
        if ($_POST['value']) {
            if (!Manager::changeAttribute($_POST['id'], 'name', $_POST['value'], 'update', 'managerTeam')) {
                echo Utils::jsonReturn(array(
                    'error' => true,
                    'message' => _user()->getFlash('failure')
                ));
                return;
            }
        }
        Report::model()->deleteReport(ReportReason::TEAM_NAME, $_POST['id']);

        echo Utils::jsonReturn(array(
            'error' => false,
            'message' => Manager::model()->findByPk($_POST['id'])->managerTeam->name
        ));

        return;
    }

    public function actionChangeFullName() {
        if ($_POST['value'])
            Manager::changeAttribute($_POST['id'], 'fullname', $_POST['value'], 'update');

        echo Manager::model()->findByPk($_POST['id'])->fullname;

        return;
    }

    public function actionChangeEmail() {
        if ($_POST['value'])
            Manager::changeAttribute($_POST['id'], 'email', $_POST['value'], 'update');

        echo Manager::model()->findByPk($_POST['id'])->email;

        return;
    }

    /**
     * Delete a comment
     * @param integer $id Comment id
     * @param integer $mid Manager id
     */
    public function actionDeleteComment($id, $mid, $src = 'reports') {
        $comment = Comment::model()->findByPk($id);
        if ($comment->delete()) {
            $this->redirect(AdminUtils::aUrl('manager/' . $src, array('username' => Manager::model()->findByPk($mid)->username)));
        }
    }

    /**
     * Lists all inactive managers
     */
    public function actionInactive() {

        $m = new Manager('searchInactive');
        $m->unsetAttributes();

        if (isset($_GET['Manager']))
            $m->attributes = $_GET['Manager'];

        $this->render('inactive', array(
            'm' => $m,
        ));
    }

    /**
     * Activate a manager
     * @param integer $id manager id
     */
    public function actionActivate($id) {
        $m = $this->loadModel($id);

        $m->email_code = null;
        if (_isModuleOn('sms')) {
            $mo = $m->mobile;
            $mo->activation_code = null;
            $mo->save();
        }
        $m->activation_date = _app()->localtime->getLocalNow('Y-m-d H:i:s');
        $m->role = Role::TEMPORARY;
        if (!$m->update()) {
            error_log(Debug::arObjectErrors($m));
            _user()->setFlash('failure', 'Could not activate player: <br />' . Debug::arObjectErrors($m));
            $this->redirect(AdminUtils::aUrl('manager/view', array('id' => $m->id)));
        }
        $content = 'Γειά σου ' . $m->username . '.<br />O λογαριασμός σου στο παιχνίδι ' . Gameconfiguration::model()->get('game_name') . ' έχει ενεργοποιηθεί.<br />Μπες στο παιχνίδι κάνοντας κλίκ στον παρακάτω σύνδεσμο:<br /><br />' . Gameconfiguration::model()->get('site_url');
        if (!Utils::sendEmail($m->email, 'Ενεργοποίηση λογαριασμού', $content))
            _user()->setFlash('failure', 'Account activated but an email could not be sent to the manager. Please notify him manually.');

        Yii::log(_user()->username . " activated {$m->username}", 'info', 'actions');
        
        $this->redirect(AdminUtils::aUrl('manager/view', array('id' => $m->id)));
    }

    /**
     * @deprecated Deprecated since rev. 137
     */
    public function actionViewMessage($id) {
        $message = Message::model()->findByPk($id);

        $this->render('viewMessage', array('message' => $message));
    }

    /**
     * Ban a manager
     * @param integer $id
     */
    public function actionBan() {
        $manager = Manager::model()->findByPk($_POST['id']);
        $banned = Ban::model()->find('manager_id = :id AND (date(now()) BETWEEN date(date_banned) AND date(date_banned + INTERVAL duration DAY))', array(':id' => $manager->id));
        if ($banned == null) {
            $t = _app()->db->beginTransaction();

            $ban = new Ban;
            $ban->manager_id = $manager->id;
            $ban->duration = $_POST['duration'];
            $ban->report_reason_id = $_POST['reason'];
            $ban->date_banned = _app()->localtime->getLocalNow('Y-m-d H:i:s');

            $manager->notes = $_POST['notes'];

            if ($manager->save(null, array('notes')) && $ban->save()) {
                $t->commit();
                Yii::log(_user()->username . " banned {$manager->username}", 'info', 'actions');
            } else {
                $t->rollback();
                _user()->setFlash('failure', Debug::arObjectErrors($ban));
            }
        }
        $this->redirect(AdminUtils::aUrl('manager/view', array('id' => $manager->id)));
    }

    /**
     * Unban a manager
     * @param integer $id
     */
    public function actionUnban($id) {
        $manager = $this->loadModel($id);
        $manager->ban->delete();

        $this->redirect(AdminUtils::aUrl('manager/view', array('id' => $id)));
    }

    /**
     * Delete a manager
     * @param integer $id
     */
    public function actionDelete($id) {
        DeletedManager::model()->delete($id);
        $this->redirect(AdminUtils::aUrl('manager/temps'));
    }

    /**
     * Lists all unapproved photos
     */
    public function actionPhotos() {
        $criteria = new CDbCriteria;
        $criteria->with = 'profile';
        $criteria->condition = 'profile.photo_approved = 0 and profile.photo is not null';
        $criteria->addInCondition('role', array(Role::MANAGER, Role::RESET));
        $criteria->limit = 50;
        $photos = Manager::model()->findAll($criteria);

        $this->render('photos', array('photos' => $photos));
    }

    /**
     * Approve or disapprove photos
     */
    public function actionApprove() {
        if (isset($_POST['r'])) {
            foreach ($_POST['r'] as $id => $approve) {
                $profile = $this->loadModel($id)->profile;
                if ($approve) {
                    $profile->photo_approved = 1;
                    if (!$profile->save())
                        _user()->setFlash('failure', Debug::arObjectErrors($profile));
                    Report::model()->deleteReport(ReportReason::PHOTO, $id);
                }else {
                    unlink('images/manager_photos/' . $profile->photo);
                    $profile->photo = null;
                    if (!$profile->save())
                        _user()->setFlash('failure', Debug::arObjectErrors($manager));
                }
            }
        }
        
        Yii::log(_user()->username . " approved photos", 'info', 'actions');

        $this->actionPhotos();
    }

    /**
     * View a manager's profile details
     * @param integer $id
     * @param string $action
     * @param string $username
     */
    public function actionView($id, $deleted = false) {
        _cs()->registerCoreScript('jquery.ui');
        AdminUtils::registerAdminJsFile('manager');
        AdminUtils::registerAdminJsFile('ban');
        AdminUtils::registerAdminJsFile('jquery.jeditable.mini');
        AdminUtils::registerAdminCssFile('jui/base/jquery-ui');

        if ($deleted) {
            $manager = DeletedManager::model()->findByPk($id);
            $this->render('viewDeleted', array('manager' => $manager));
        } else {
            $manager = $this->loadModel($id);

            if ($manager->hasRole(Role::INACTIVE)) {
                $this->render('viewInactive', array('manager' => $manager));
            } elseif ($manager->hasRole(Role::TEMPORARY)) {
                $this->render('viewTemporary', array('manager' => $manager));
            } else {
                $counts = array(
                    'messages' => PMThread::model()->count(array(
                        'condition' => 'sender_id = :mid or recipient_id = :mid',
                        'params' => array(
                            ':mid' => $manager->id,
                            ':mid' => $manager->id
                        )
                    )),
                    'comments' => Comment::model()->countByAttributes(array('commentor_id' => $manager->id)),
                    'reports' => Report::model()->countByAttributes(array('manager_id' => $manager->id)),
                    'leagues' => CustomLeagueManager::model()->countByAttributes(array('manager_id' => $manager->id)),
                    'transactions' => Transaction::model()->countByAttributes(array('manager_id' => $manager->id))
                );

                $ban = Ban::model()->findByAttributes(array('manager_id' => $manager->id));

                $this->render('view', array(
                    'manager' => $manager,
                    'counts' => $counts,
                    'ban' => $ban,
                ));
            }
        }
    }

    /**
     * @param type $id
     * @return type
     */
    public function actionSimilarIps($id) {
        if (!Utils::isAjax())
            return;

        $manager = $this->loadModel($id);
        $similarIPs = Manager::model()->findAll('last_known_ip = :ip', array(':ip' => $manager->last_known_ip));

        $this->renderPartial('_similarIpManagers', array(
            'similarIPs' => $similarIPs,
            'manager' => $manager,
        ));
    }

    /**
     * Send a message to a manager
     * @param integer $id
     */
    public function actionSendMessage($id) {

        if (isset($_GET['content'])) {
            if (PMThread::model()->sendSystemMessage($id, $_GET['content'])) {
                _user()->setFlash('failure', 'Unable to send system message');
            } else {
                _user()->setFlash('success', 'Message was sent succesfully');
            }
            $this->redirect(AdminUtils::aUrl('manager/view', array('id' => $id)));
        }

        $this->render('sendMessage', array('username' => Manager::model()->findByPk($id)->username, 'id' => $id));
    }

    public function actionOnline() {
        $m = new Manager();
        $m->unsetAttributes();

        $this->render('online', array('m' => $m));
    }

    /**
     *
     * @param type $manager
     */
    public function actionComments($manager = null) {
        $comments = new Comment('search');
        if ($manager)
            $comments->commentor_id = Manager::model()->findByPk($manager)->cleanUsername;

        if (isset($_GET['Comment'])) {
            $comments->attributes = $_GET['Comment'];
        }

        $this->render('comments', array('comments' => $comments));
    }

    /**
     * @param int $id Manager id
     */
    public function actionThreads($id = null) {
        $threads = new PMThread('search');

        if ($id) {
            $manager = Manager::model()->findByPk($id);
        }
        if ($id && $manager) {
            $threads->sender_id = $manager->cleanUsername;
        }

        if (isset($_GET['PMThread'])) {
            $threads->attributes = $_GET['PMThread'];
        }

        $this->render('threads', array(
            'threads' => $threads
        ));
    }

    /**
     * @param int $id Thread id
     */
    public function actionThread($id) {

        $thread = PMThread::model()->findByPk($id);
        if (!$thread) {
            throw new CHttpException('404');
        }

        $comments = new PMThreadComment('search');
        $comments->discussion_id = $id;

        if (isset($_GET['PMThreadComment'])) {
            $comments->attributes = $_GET['PMThreadComment'];
        }

        $this->render('thread', array(
            'thread' => $thread,
            'comments' => $comments,
            'nullCommentor' => $thread->system ? "CHtml::tag('span', array('class' => 'system'), 'system')" : "CHtml::tag('span', array('class' => 'deleted'), 'deleted')",
        ));
    }

    /**
     *
     */
    public function actionUserList() {
        if (Yii::app()->request->isAjaxRequest && isset($_GET['q'])) {
            /* q is the default GET variable name that is used by the autocomplete widget to pass in user input  */

            $name = $_GET['q'];

            // this was set with the "max" attribute of the CAutoComplete widget
            $limit = min($_GET['limit'], 50);
            $criteria = new CDbCriteria;
            $criteria->select = 'username';
            $criteria->condition = "username LIKE :sterm";
            $criteria->addInCondition('role', array(Role::MANAGER));
            $criteria->params = array(":sterm" => "%$name%");
            $criteria->limit = $limit;
            $userArray = Manager::model()->findAll($criteria);
            $returnVal = '';
            foreach ($userArray as $userAccount) {
                $returnVal .= $userAccount->getAttribute('username') . "\n";
            }
            echo $returnVal;
        }
    }

    public function actionHideComment($id, $src=false) {
        $comment = Comment::model()->findByPk($id);
        $comment->visible = false;
        $comment->save(null, array('visible'));
        if (!$src)
            $this->redirect(AdminUtils::aUrl('manager/comments'));
        elseif ($src == 'editReport')
            $this->redirect(AdminUtils::aUrl('manager/' . $src, array('id' => $comment->commentor_id, 'type' => 'Comments')));
    }

    public function actionShowComment($id, $src=false) {
        $comment = Comment::model()->findByPk($id);
        $comment->visible = true;
        $comment->save(null, array('visible'));
        if (!$src)
            $this->redirect(AdminUtils::aUrl('manager/comments'));
        elseif ($src == 'editReport')
            $this->redirect(AdminUtils::aUrl('manager/' . $src, array('id' => $comment->commentor_id, 'type' => 'Comments')));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Manager::model()->findByPk((int) $id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

}
