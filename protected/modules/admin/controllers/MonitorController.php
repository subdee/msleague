<?php

class MonitorController extends AdminController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
//	public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'portfolios', 'transactions', 'portfolioHistory'),
                'expression' => 'User::hasRole(AdminRole::ADMIN | AdminRole::OPERATOR)',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists players stock info.
     */
    public function actionIndex() {
        $players = new Player('search');

        if (isset($_GET['Player']))
            $players->attributes = $_GET['Player'];

        $this->render('index', array('players' => $players));
    }

    /**
     * Lists manager's portfolios
     */
    public function actionPortfolios() {
        $m = new Manager('searchPortfolios');
        $m->unsetAttributes();

        if (isset($_GET['Manager'])) {
            $m->attributes = $_GET['Manager'];
        };

        $this->render('portfolios', array('m' => $m));
    }

    /**
     * Lists transactions with a search form
     */
    public function actionTransactions($manager = null) {
        AdminUtils::registerAdminJsFile('/js/transactions.js');

        $trans = new Transaction('searchAdmin');
        if ($manager)
            $trans->manager_id = $manager;
        if (intval(Yii::app()->request->getParam('clearFilters')) == 1)
            EButtonColumnWithClearFilters::clearFilters($this, $trans);

        $this->render('transactions', array(
            'trans' => $trans,
        ));
    }

    /**
     * View a manager's portfolio history
     * @param integer $id 
     */
    public function actionPortfolioHistory($id) {
        $username = Manager::model()->findByPk($id)->username;

        $history = new ManagerDailyHistory('search');
        $history->manager_id = $id;

        $this->render('portfolioHistory', array('history' => $history, 'username' => $username));
    }

}

