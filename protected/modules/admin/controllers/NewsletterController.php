<?php

class NewsletterController extends AdminController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
//	public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'reminders', 'create', 'createReminder', 'groups', 'createGroup', 'countManagers', 'send', 'delete', 'startReminder', 'pauseReminder','editReminder','edit'),
                'expression' => 'User::hasRole(AdminRole::ADMIN | AdminRole::OPERATOR)',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        _cs()->registerCoreScript('jquery.ui');
        AdminUtils::registerAdminCssFile('jui/base/jquery-ui');
        AdminUtils::registerAdminJsFile('newsletters');

        $newsletters = new Newsletter;
        
        if (isset($_GET['Newsletter'])) {
            $newsletters->attributes = $_GET['Newsletter'];
            $newsletters->id = $_GET['Newsletter']['id'];
        }

        $this->render('index', array(
            'newsletters' => $newsletters
        ));
    }

    public function actionReminders() {
        $newsletters = new Newsletter;
        
        if (isset($_GET['Newsletter'])) {
            $newsletters->attributes = $_GET['Newsletter'];
            $newsletters->id = $_GET['Newsletter']['id'];
        }

        $this->render('reminders', array(
            'newsletters' => $newsletters,
            'reminderTime' => ScriptConfiguration::model()->get('newsletter_time')
        ));
    }

    public function actionCreate() {
        $newsletter = new Newsletter;

        if (isset($_POST['Newsletter'])) {
            $newsletter->attributes = $_POST['Newsletter'];
            if (!$newsletter->save())
                _user()->setFlash('failure', _get_errors_string($newsletter));
            else
                $this->redirect('index');
        }

        $this->render('create', array(
            'newsletter' => $newsletter
        ));
    }
    
    public function actionEdit($id) {
        $newsletter = Newsletter::model()->findByPk($id);

        if (isset($_POST['Newsletter'])) {
            $newsletter->attributes = $_POST['Newsletter'];
            if (!$newsletter->save())
                _user()->setFlash('failure', _get_errors_string($newsletter));
            else
                $this->redirect(AdminUtils::aUrl('newsletter/index'));
        }

        $this->render('create', array(
            'newsletter' => $newsletter
        ));
    }

    public function actionCreateReminder() {
        _cs()->registerCoreScript('jquery.ui');
        AdminUtils::registerAdminCssFile('jui/base/jquery-ui');
        AdminUtils::registerAdminJsFile('newsletters');

        $newsletter = new Newsletter;

        if (isset($_POST['Newsletter'])) {
            $newsletter->attributes = $_POST['Newsletter'];
            $newsletter->is_auto = true;
            if (!$newsletter->save())
                _user()->setFlash('failure', _get_errors_string($newsletter));
            else
                $this->redirect(array('reminders'));
        }
        
        $this->render('createReminder', array(
            'newsletter' => $newsletter
        ));
    }
    
    public function actionEditReminder($id) {
        _cs()->registerCoreScript('jquery.ui');
        AdminUtils::registerAdminCssFile('jui/base/jquery-ui');
        AdminUtils::registerAdminJsFile('newsletters');

        $newsletter = Newsletter::model()->findByPk($id);

        if (isset($_POST['Newsletter'])) {
            $newsletter->attributes = $_POST['Newsletter'];
            $newsletter->is_auto = true;
            if (!$newsletter->save())
                _user()->setFlash('failure', _get_errors_string($newsletter));
            else
                $this->redirect(AdminUtils::aUrl('newsletter/reminders'));
        }

        $this->render('createReminder', array(
            'newsletter' => $newsletter
        ));
    }

    public function actionCreateGroup() {
        $mg = new ManagerGroup;

        if (isset($_POST['ManagerGroup'])) {
            
            if (!isset($_POST['nonactive']) && !isset($_POST['inactive']) && !isset($_POST['temp'])){
                echo '<span>You must select at least one subgroup</span>';
            }else{
                $mg->attributes = $_POST['ManagerGroup'];
                $query = 'SELECT * FROM manager m WHERE ';

                if (isset($_POST['nonactive'])) {
                    $sc = ScriptConfiguration::model()->find(array('select'=>'active_options'));
                    $active = split(',',$sc->active_options);
                    $query .= '(role = "'.Role::MANAGER.'" AND DATEDIFF(now(),date(convert_tz(m.last_login,"UTC","'._timezone().'"))) = '.$_POST['nonactive_value'].')';
                    if (isset($_POST['inactive']) || isset($_POST['temp']))
                        $query .= ' OR ';
                    else
                        $query .= ' AND ';
                }
                if (isset($_POST['inactive'])){
                    $query .= '(role = "'.Role::INACTIVE.'" AND DATEDIFF(now(),date(convert_tz(m.registration_date,"UTC","'._timezone().'"))) = '.$_POST['inactive_value'].')';
                    if (isset($_POST['temp']))
                        $query .= ' OR ';
                    else
                        $query .= ' AND ';
                }
                if (isset($_POST['temp']))
                    $query .= '(role = "'.Role::TEMPORARY.'" AND DATEDIFF(now(),date(convert_tz(m.activation_date,"UTC","'._timezone().'"))) = '.$_POST['temp_value'].') AND ';

                $query .= 'newsletter = 1';

                $mg->query = $query;

                if ($mg->save())
                    echo 'ok';
                else {
                    error_log(_get_errors_string($mg));
                    echo '<span>'._get_errors_string($mg).'</span>';
                }
            }
        }

        return;
    }
    
    public function actionGroups() {
        $this->renderPartial('_groups');
    }

    public function actionSend($id) {

        $nl = Newsletter::model()->findByPk($id);

        $managers = Manager::model()->findAllByAttributes(array('role' => array(Role::MANAGER,Role::TEMPORARY,Role::RESET), 'newsletter' => true));

        $count = 0;
        $errors = 0;
        foreach ($managers as $manager) {
            $content = $this->replaceSmartFields($nl->content, $manager).Gameconfiguration::model()->get('newsletter_footer');
            if (!Utils::sendEmail($manager->email, $nl->subject, $content))
                ++$errors;
            ++$count;
        }
        
        $nl->last_sent = _app()->localtime->getLocalNow('Y-m-d H:i:s');
        $nl->save(false,array('last_sent'));
        
        $log = @file_put_contents('/var/log/msleague/core.log_'._app()->localtime->getLocalNow('Y-m-d'), _app()->localtime->getLocalNow('H:i:s')." (admin) Sent newsletter with id {$id} to {$count} managers.\n",FILE_APPEND);
        
        if (!$log)
            error_log('Unable to write to log');

        if ($errors == 0)
            echo $count;
        else
            echo 'error';

        return;
    }

    public function actionCountManagers() {
        if (isset($_POST['id'])) {
            $mg = ManagerGroup::model()->findByPk($_POST['id']);
            $m = Manager::model()->findAllBySql($mg->query);

            $count = count($m);
            echo $count;
            return;
        }
    }

    public function actionDelete($id) {
        $n = Newsletter::model()->findByPk($id);
        $url = AdminUtils::aUrl('newsletter/index');
        if ($n->is_auto)
            $url = AdminUtils::aUrl('newsletter/reminders');
        if ($n->delete())
            $this->redirect($url);
    }

    public function actionStartReminder($id) {
        $n = Newsletter::model()->findByPk($id);
        if ($n != null) {
            $n->is_active = true;
            if (!$n->save())
                _user()->setFlash('failure', 'Could not start reminder!');
        }

        $this->redirect(AdminUtils::aUrl('newsletter/reminders'));
    }

    public function actionPauseReminder($id) {
        $n = Newsletter::model()->findByPk($id);
        if ($n != null) {
            $n->is_active = false;
            if (!$n->save())
                _user()->setFlash('failure', 'Could not pause reminder!');
        }

        $this->redirect(AdminUtils::aUrl('newsletter/reminders'));
    }

    private static function replaceSmartFields($text, $info) {
        $text = str_replace('_USERNAME_', $info->username, $text);
        $text = str_replace('_POINTS_', $info->total_points, $text);

        return $text;
    }

}
