<?php

class PlayerController extends AdminController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
//	public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'add', 'addInjured', 'addSuspended', 'cancelDelist', 'cancelList',
                    'delete', 'delist', 'delisted', 'edit', 'injured', 'list', 'listed', 'removeInjured', 'removeSuspended', 'suspended', 'upload', 'uploadIS'),
                'expression' => 'User::hasRole(AdminRole::ADMIN | AdminRole::OPERATOR)',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists all unlisted players
     */
    public function actionIndex() {
        _cs()->registerCoreScript('jquery.ui');
        AdminUtils::registerAdminJsFile('upload');
        AdminUtils::registerAdminCssFile('jui/base/jquery-ui');

        $players = new Player('searchIndex');

        if (isset($_GET['Player']))
            $players->attributes = $_GET['Player'];

        $this->render('index', array(
            'players' => $players,
        ));
    }

    /**
     * Lists all listed players
     */
    public function actionListed() {


        $players = new Player('searchListed');

        if (isset($_GET['Player']))
            $players->attributes = $_GET['Player'];

        $this->render('listed', array(
            'players' => $players,
        ));
    }

    /**
     * Lists all delisted players
     */
    public function actionDelisted() {


        $players = new Player('searchDelisted');

        if (isset($_GET['Player']))
            $players->attributes = $_GET['Player'];

        $this->render('delisted', array(
            'players' => $players,
        ));
    }

    /**
     * Lists all injured players
     */
    public function actionInjured() {
        _cs()->registerCoreScript('jquery.ui');
        AdminUtils::registerAdminJsFile('upload');
        AdminUtils::registerAdminCssFile('jui/base/jquery-ui');

        $players = new Player('search');
        $players->is_injured = true;

        if (isset($_GET['Player']))
            $players->attributes = $_GET['Player'];

        $this->render('injured', array(
            'players' => $players,
        ));
    }

    /**
     * Lists all suspended players
     */
    public function actionSuspended() {
        _cs()->registerCoreScript('jquery.ui');
        AdminUtils::registerAdminJsFile('upload');
        AdminUtils::registerAdminCssFile('jui/base/jquery-ui');

        $players = new Player('searchSuspended');
        $players->is_suspended = true;

        if (isset($_GET['Player']))
            $players->attributes = $_GET['Player'];

        $this->render('suspended', array(
            'players' => $players,
        ));
    }

    /**
     * Adds a new player
     */
    public function actionAdd() {
        $player = new Player;

        if (isset($_POST['Player'])) {
            $t = _app()->db->beginTransaction();
            try {

                $player->attributes = $_POST['Player'];
                if (!$player->save())
                    throw new Exception('Could not save player');

                $t->commit();
                $this->redirect(AdminUtils::aUrl('player/index'));
                _app()->end();
            } catch (Exception $e) {
                error_log($e);
                $t->rollback();
            }
        }

        $this->render('add', array(
            'player' => $player, 'src' => 'index'
        ));
    }

    /**
     * Edit a player's attributes
     * @param integer $id
     * @param string $src The page that called the action
     */
    public function actionEdit($id, $src='index') {
        $player = $this->loadModel($id);

        if (isset($_POST['Player'])) {
            $t = _app()->db->beginTransaction();
            try {

                $player->attributes = $_POST['Player'];
                if (!$player->save())
                    throw new Exception('Could not save player');

                $t->commit();
                $this->redirect(array($src));
            } catch (Exception $e) {
                error_log($e);
                $t->rollback();
            }
        }
        $this->render('edit', array(
            'player' => $player, 'src' => $src
        ));
    }

    /**
     * List a player by giving values
     */
    public function actionList() {
        AdminUtils::registerAdminJsFile('jquery.numeric');

        $player = '';
        $gc = Gameconfiguration::model()->find();
        if (isset($_POST['player'])) {
            $player = $this->loadModel($_POST['player']);
            $t = _app()->db->beginTransaction();
            try {
                if (time($_POST['list_date']) < time() || $_POST['list_date'] == null)
                    throw new Exception('List date must be in the future');
                if ($_POST['initial_value'] < $gc->overall_min_price_per_share || $_POST['initial_value'] > $gc->overall_max_price_per_share)
                    throw new Exception("Stock value for player must be between {$gc->overall_min_price_per_share} and {$gc->overall_max_price_per_share}");
                $player->list_date = $_POST['list_date'];
                $player->initial_value = $_POST['initial_value'];
                $player->current_value = $_POST['initial_value'];
                $player->bank_shares = $gc->total_shares_per_player;
                $player->total_points = 0;
                $player->vmi_season = 0;
                if (!$player->save())
                    throw new Exception('Could not save player');

                $t->commit();
                $this->redirect(AdminUtils::aUrl('player/index'));
                _app()->end();
            } catch (Exception $e) {
                error_log($e);
                _user()->setFlash('failure', $e->getMessage());
                $t->rollback();
            }
        }
        if (isset($_GET['id']))
            $player = $this->loadModel($_GET['id'])->id;
        $this->render('list', array('player' => $player, 'shares' => $gc->total_shares_per_player));
    }

    /**
     * Delist player
     * @param integer $id 
     */
    public function actionDelist($id) {
        _cs()->registerCoreScript('jquery.ui');
        AdminUtils::registerAdminJsFile('delist');
        AdminUtils::registerAdminCssFile('jui/base/jquery-ui');

        $player = $this->loadModel($id);

        if (isset($_POST['Player'])) {
            $t = _app()->db->beginTransaction();
            try {
                $gc = Gameconfiguration::model()->find();
                $player->attributes = $_POST['Player'];
                $player->delist_date = _app()->localtime->getLocalNow('Y-m-d H:i:s');
                if (isset($_POST['Player']['deleteTeam']))
                    $player->team_id = null;
                if (!$player->save())
                    throw new Exception('Could not delist player');

                $t->commit();
                $this->redirect(AdminUtils::aUrl('player/listed'));
                _app()->end();
            } catch (Exception $e) {
                error_log($e);
                $t->rollback();
            }
        }

        $this->render('delist', array('player' => $player));
    }

    /**
     * Cancel a scheduled delisted player
     * @param integer $id 
     */
    public function actionCancelDelist($id) {
        $player = $this->loadModel($id);

        $t = _app()->db->beginTransaction();
        try {
            $player->delist_date = null;
            if (!$player->save())
                throw new Exception('Could not delist player');

            $t->commit();
            $this->redirect(AdminUtils::aUrl('player/listed'));
            _app()->end();
        } catch (Exception $e) {
            error_log($e);
            $t->rollback();
        }
        $this->actionListed();
    }

    /**
     * Cancel a scheduled listed player
     * @param integer $id 
     */
    public function actionCancelList($id) {
        $player = $this->loadModel($id);

        $t = _app()->db->beginTransaction();
        try {
            $player->list_date = null;
            if (!$player->save())
                throw new Exception('Could not delist player');

            $t->commit();
            $this->redirect(AdminUtils::aUrl('player/index'));
            _app()->end();
        } catch (Exception $e) {
            error_log($e);
            $t->rollback();
        }
        $this->actionIndex();
    }

    /**
     * Add an injured player to the list
     */
    public function actionAddInjured() {
        $player = $this->loadModel($_POST['player']);

        $t = _app()->db->beginTransaction();
        try {
            $player->is_injured = true;
            if (!$player->save())
                throw new Exception('Could not add injured player');

            $t->commit();
            $this->redirect(AdminUtils::aUrl('player/injured'));
            _app()->end();
        } catch (Exception $e) {
            error_log($e);
            $t->rollback();
        }
        $this->actionInjured();
    }

    /**
     * Remove a player from the injured list
     * @param integer $id 
     */
    public function actionRemoveInjured($id) {
        $player = $this->loadModel($id);

        $t = _app()->db->beginTransaction();
        try {
            $player->is_injured = false;
            if (!$player->save())
                throw new Exception('Could not remove injured player');

            $t->commit();
            $this->redirect(AdminUtils::aUrl('player/injured'));
            _app()->end();
        } catch (Exception $e) {
            error_log($e);
            $t->rollback();
        }
        $this->actionInjured();
    }

    /**
     * Add a suspended player to the list
     */
    public function actionAddSuspended() {
        $player = $this->loadModel($_POST['player']);

        $t = _app()->db->beginTransaction();
        try {
            $player->is_suspended = true;
            if (!$player->save())
                throw new Exception('Could not add injured player');

            $t->commit();
            $this->redirect(AdminUtils::aUrl('player/suspended'));
            _app()->end();
        } catch (Exception $e) {
            error_log($e);
            $t->rollback();
        }
        $this->actionSuspended();
    }

    /**
     * Remove a player from the suspended list
     * @param integer $id 
     */
    public function actionRemoveSuspended($id) {
        $player = $this->loadModel($id);

        $t = _app()->db->beginTransaction();
        try {
            $player->is_suspended = false;
            if (!$player->save())
                throw new Exception('Could not remove injured player');

            $t->commit();
            $this->redirect(AdminUtils::aUrl('player/suspended'));
            _app()->end();
        } catch (Exception $e) {
            error_log($e);
            $t->rollback();
        }
        $this->actionSuspended();
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $gp = GamePlayer::model()->count(array(
            'join' => 'LEFT JOIN player p ON p.id = t.player_id',
            'condition' => 'p.id = :id',
            'params' => array(':id' => $id)
            ));

        if ($gp)
            _user()->setFlash('failure', $this->loadModel($id)->name . ' cannot be deleted because he has played in at least one match');
        else
            $this->loadModel($id)->delete();

        $this->redirect(AdminUtils::aUrl('player/index'));
    }

    public function actionUpload($is = false) {
        $file = CUploadedFile::getInstanceByName('xml');
        if ($file->saveAs('xml/' . $file->name)) {
            try {
                $data = $this->processFile($file->name, $is);
            } catch (Exception $e) {
                _user()->setFlash('failure', $e->getMessage());
                if ($is)
                    $this->redirect(AdminUtils::aUrl('player/injured'));
                else
                    $this->redirect(AdminUtils::aUrl('player/index'));
            }
            if ($data) {
                if ($is)
                    $this->actionUpdateIS($data);
                else
                    $this->actionInsert($data);
            }
        }
    }

    public function actionUploadIS() {
        $this->actionUpload(true);
        return;
    }

    public function actionUpdateIS($data) {
        if ($data != null) {
            $t = _app()->db->beginTransaction();
            try {
                Player::model()->updateAll(array('is_injured' => false, 'is_suspended' => false));
                foreach ($data as $player) {
                    $pl = Player::model()->findByAttributes(array('xml_id' => $player['xml_id']));
                    if ($pl == null)
                        throw new Exception('Unable to find player with xml_id: ' . $player['xml_id']);
                    $pl->is_injured = $player['is_injured'];
                    $pl->is_suspended = $player['is_suspended'];

                    if (!$pl->save(null, array('is_injured', 'is_suspended')))
                        throw new Exception('Error saving player: ' . $pl->shortname . '<br />' . _get_errors_string($pl));
                }

                $t->commit();

                _user()->setFlash('success', 'Succesfully updated injured and suspended lists');
            } catch (CDbException $e) {
                $t->rollback();
                _user()->setFlash('failure', $e->getMessage());
            } catch (Exception $e) {
                $t->rollback();
                _user()->setFlash('failure', $e->getMessage());
            }
        }

        $this->redirect(AdminUtils::aUrl('player/injured'));
    }

    public function actionInsert($data) {
        if ($data != null) {
            $t = _app()->db->beginTransaction();
            try {
                foreach ($data as $player) {
                    $pl = new Player;
                    $pl->name = $player['name'];
                    $pl->shortname = $player['shortname'];
                    $pl->basic_position_id = $player['basic_position_id'];
                    $pl->team_id = $player['team_id'];
                    $pl->list_date = $player['list_date'];
                    if (strtotime($pl->list_date) < time())
                        throw new Exception('List date must be in the future for player: ' . $pl->shortname);
                    $pl->bank_shares = Gameconfiguration::model()->get('total_shares_per_player');
                    $pl->current_value = $pl->initial_value = $player['total_value'] / $pl->bank_shares;
                    if ($pl->current_value < Gameconfiguration::model()->get('overall_min_price_per_share'))
                        throw new Exception('Player value cannot be less than ' . Gameconfiguration::model()->get('overall_min_price_per_share') . ' for player: ' . $pl->shortname);
                    if ($pl->current_value > Gameconfiguration::model()->get('overall_max_price_per_share'))
                        throw new Exception('Player value cannot be more than ' . Gameconfiguration::model()->get('overall_max_price_per_share') . ' for player: ' . $pl->shortname);
                    $pl->vmi_season = 0;
                    $pl->total_points = 0;
                    $pl->xml_id = $player['xml_id'];

                    if (!$pl->save())
                        throw new Exception('Error saving player: ' . $pl->shortname . '<br />' . _get_errors_string($pl));
                }

                $t->commit();
                _user()->setFlash('success', 'Players inserted succesfully');
            } catch (Exception $e) {
                $t->rollback();
                _user()->setFlash('failure', $e->getMessage());
            }
        }

        $this->redirect(AdminUtils::aUrl('player/index'));
    }

    private function processFile($filename, $is) {
        $xml = @simplexml_load_file('xml/' . $filename);
        if (!$xml)
            throw new Exception('The XML file you uploaded is invalid');

        $i = 1;

        if ($is) {
            foreach ($xml->player as $player) {
                if ($player->id == null)
                    throw new Exception('All properties must be filled for player ' . $player->name);
                if (isset($player->name))
                    throw new Exception('The XML file you uploaded is invalid');
                $data[$i]['xml_id'] = (int) $player->id;
                $data[$i]['is_injured'] = (int) $player->is_injured;
                $data[$i]['is_suspended'] = (int) $player->is_suspended;
                ++$i;
            }
            if (empty($data))
                throw new Exception('The XML file you uploaded contains no data');
        } else {
            foreach ($xml->player as $player) {

                if ($player->id == null || $player->name == null || $player->shortname == null || $player->position == null || $player->team_id == null || $player->list_date == null || $player->total_value == null)
                    throw new Exception('All properties must be filled for player ' . $player->name);
                $data[$i]['xml_id'] = (int) $player->id;
                $data[$i]['name'] = (string) $player->name;
                $data[$i]['shortname'] = (string) $player->shortname;
                $bp = BasicPosition::model()->findByAttributes(array('abbreviation' => (string) $player->position));
                if ($bp != null)
                    $data[$i]['basic_position_id'] = $bp->id;
                else
                    throw new Exception('Invalid basic position format for player ' . $player->name);
                $t = Team::model()->findByAttributes(array('xml_id' => (int) $player->team_id));
                if ($t != null)
                    $data[$i]['team_id'] = $t->id;
                else
                    throw new Exception('Invalid team id for player ' . $player->name);
                $data[$i]['list_date'] = (string) $player->list_date;
                $data[$i]['total_value'] = (int) $player->total_value;
                ++$i;
            }
        }

        if (!isset($data))
            throw new Exception('The XML file you uploaded is either invalid or contains no data');

        return $data;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Player::model()->findByPk((int) $id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public static function getNewPlayers() {
        return CHtml::listData(Player::model()->getNewPlayers(), 'id', 'name');
    }

    public static function getUninjuredPlayers() {
        return CHtml::listData(Player::model()->getUninjuredPlayers(), 'id', 'concatNameTeam');
    }

    public static function getUnsuspendedPlayers() {
        return CHtml::listData(Player::model()->getUnsuspendedPlayers(), 'id', 'concatNameTeam');
    }
    
    public static function getBasicPositions() {
        return CHtml::listData(BasicPosition::model()->findAll(), 'id', 'name');
    }
    
    public function getTeams() {
        return CHtml::listData(Team::model()->findAll(), 'id', 'name');
    }

}
