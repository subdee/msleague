<?php

class ReportController extends AdminController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
//	public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists reports.
     */
    public function actionIndex() {

        $report = new Report;

        if (isset($_GET['Report']))
            $report->attributes = $_GET['Report'];

        $this->render('index', array('report' => $report));
    }

    public function actionManager($id) {
        _cs()->registerCssFile(_bu('css/gridview.css'));
        $typeCounts = array();

        $typeCounts = Report::getCountsForManager($id);

        $this->render('manager', array(
            'typeCounts' => $typeCounts,
            'manager' => Manager::Model()->findByPk($id)
        ));
    }

    public function actionView($id, $type) {
        _cs()->registerCoreScript('jquery.ui');
        AdminUtils::registerAdminJsFile('report');
        AdminUtils::registerAdminJsFile('ban');
        AdminUtils::registerAdminCssFile('jui/base/jquery-ui');
        AdminUtils::registerAdminCssFile('gridview');

        $criteria = new CDbCriteria;
        $criteria->with = array('reportReason');
        $criteria->condition = 'manager_id = :id';
        $criteria->params = array(':id' => $id);
        switch ($type) {
            case 'Username':
                $criteria->addInCondition('reportReason.reason_type', array(ReportReason::USERNAME));
                break;
            case 'Team Name':
                $criteria->addInCondition('reportReason.reason_type', array(ReportReason::TEAM_NAME));
                break;
            case 'Photo':
                $criteria->addInCondition('reportReason.reason_type', array(ReportReason::PHOTO));
                break;
            case 'Status':
                $criteria->addInCondition('reportReason.reason_type', array(ReportReason::STATUS));
                break;
            case 'Message':
                $criteria->addInCondition('reportReason.reason_type', array(ReportReason::MESSAGE));
                break;
            case 'Cheating':
                $criteria->addInCondition('reportReason.reason_type', array(ReportReason::CHEATING));
                break;
            case 'Other':
                $criteria->addInCondition('reportReason.reason_type', array(ReportReason::OTHER));
                break;
            default:
                break;
        }
        $criteria->order = 'date_created desc';
        $reports = Report::model()->findAll($criteria);

        if ($reports == null)
            $this->redirect(AdminUtils::aUrl('report/manager', array('id' => $id)));

        $this->render('view', array('reports' => $reports));
    }

    public function actionChangeTeamName() {
        if (isset($_POST['teamName'])) {
            if (Manager::changeAttribute($_POST['id'], 'name', $_POST['teamName'], 'update', 'managerTeam')) {
                Report::model()->deleteReport(ReportReason::TEAM_NAME, $_POST['id']);

                _user()->setFlash('success', 'User team name has been updated to <strong>' . $_POST['teamName'] . '</strong> and their reports have been deleted.');
            }
        }

        $this->redirect(AdminUtils::aUrl('report/manager', array('id' => $_POST['id'])));
    }

    public function actionDeleteStatus($id) {
        if (Manager::changeAttribute($id, 'line', null, 'delete', 'profile')) {
            Report::model()->deleteReport(ReportReason::STATUS, $id);

            _user()->setFlash('success', 'User\'s status and reports have been deleted.');
        }

        $this->redirect(AdminUtils::aUrl('report/manager', array('id' => $id)));
    }

    public function actionDeletePhoto($id) {
        $photo = Manager::model()->findByPk($id)->profile->photo;
        if ($m = Manager::changeAttribute($id, 'photo', null, 'delete', 'profile')) {
            if ($photo)
                unlink('images/manager_photos/' . $photo);
            Report::model()->deleteReport(ReportReason::PHOTO, $id);

            _user()->setFlash('success', 'User\'s photo and reports have been deleted.');
        }

        $this->redirect(AdminUtils::aUrl('report/manager', array('id' => $id)));
    }

    public function actionDeleteMessage($id, $rid) {
        $commentReport = CommentReported::model()->find('report_id = :id', array(':id' => $rid));
        if ($commentReport) {
            $comment = $commentReport->comment;
            $sid = $comment->id;
            $discussion = CategoryDiscussion::model()->getCommentDiscussion($comment->id);
            if ($discussion) {
                $pm = PMThread::model()->find('discussion_id = :did', array(':did' => $discussion->id));
                if ($pm)
                    $pm->delete();
                $discussion->visible = false;
                $discussion->save(false, array('visible'));
            } else {
                $comment->visible = false;
                $comment->save(false, array('visible'));
            }
        }
        Report::model()->deleteReport(ReportReason::MESSAGE, $id, $sid);
        _user()->setFlash('success', 'All reports have been removed and the message has become invisible.');
        $this->redirect(AdminUtils::aUrl('report/manager', array('id' => $id)));
    }

    public function actionMessageDetails($id, $mid) {
        $reports = Report::model()->findAll('id IN (select report_id from mod_forum_comments_reported where comment_id = :id)', array(':id' => $id));
        $message = Comment::model()->findByPk($id);
        if ($message) {
            $title = 'View discussion >>';
            $content = $message->content;
            $discussion = $message->discussion_id;
        }
        $this->renderPartial('_messageDetails', array('reports' => $reports, 'manager_id' => $mid, 'mid' => $id, 'minfo' => array('title' => $title, 'content' => $content, 'discussion' => $discussion)));
        return;
    }

    public function actionDelete($type, $id, $sid=false) {
        Report::model()->deleteReport($type, $id, $sid);
        $reason = ReportReason::getType($type);

        if (!_user()->hasFlash('success'))
            _user()->setFlash('success', "Reports for {$reason} have been deleted.");

        $this->redirect(AdminUtils::aUrl('report/manager', array('id' => $id)));
    }

}
