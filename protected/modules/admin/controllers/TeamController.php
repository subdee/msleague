<?php

class TeamController extends AdminController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
//	public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'add', 'edit', 'delete'),
                'expression' => 'User::hasRole(AdminRole::ADMIN | AdminRole::OPERATOR)',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists all teams
     */
    public function actionIndex() {
        $t = Team::model()->findAll(array('order' => 'name asc'));

        $this->render('index', array(
            't' => $t,
        ));
    }

    /**
     * Adds a new team
     */
    public function actionAdd() {
        $team = new Team;

        $team->country_id = Country::model()->findByAttributes(array('country' => 'Greece'))->id;

        if (isset($_POST['Team'])) {
            $t = _app()->db->beginTransaction();
            try {
                $smallExists = false;
                $largeExists = false;
                $team->attributes = $_POST['Team'];
                if (CUploadedFile::getInstance($team, 'file_small') != null) {
                    $team->file_small = CUploadedFile::getInstance($team, 'file_small');
                    if (CUploadedFile::getInstance($team, 'file_large') != null) {
                        $team->file_large = CUploadedFile::getInstance($team, 'file_large');
                        $largeExists = true;
                    }
                    $smallExists = true;
                } else {
                    $team->file_small = $team->jersey_small;
                    $team->file_large = $team->jersey_large;
                }
                if ($team->validate()) {
                    if ($largeExists && $smallExists) {
                        if (!$team->file_small->saveAs(Utils::jerseyPhotoUrl('small/' . $team->file_small)))
                            throw new Exception('Small file was not saved');
                        if (!$team->file_large->saveAs(Utils::jerseyPhotoUrl('small/' . $team->file_large)))
                            throw new Exception('Large file was not saved');
                    }
                    $team->save();
                } else {
                    $this->render('add', array('team' => $team));
                    break;
                }

                $t->commit();
                $this->redirect(AdminUtils::aUrl('team/index'));
            } catch (Exception $e) {
                error_log($e);
                $t->rollback();
            } catch (CDbException $dbe) {
                error_log($dbe);
                $t->rollback();
            }
        }
        $this->render('add', array(
            'team' => $team,
        ));
    }

    /**
     * Edits a team's details
     * @param integer $id 
     */
    public function actionEdit($id) {
        $team = $this->loadModel($id);

        if (isset($_POST['Team'])) {
            $t = _app()->db->beginTransaction();
            try {
                $smallExists = false;
                $largeExists = false;
                $team->attributes = $_POST['Team'];
                if (CUploadedFile::getInstance($team, 'file_small') != null) {
                    $team->file_small = CUploadedFile::getInstance($team, 'file_small');
                    if (CUploadedFile::getInstance($team, 'file_large') != null) {
                        $team->file_large = CUploadedFile::getInstance($team, 'file_large');
                        $largeExists = true;
                    }
                    $smallExists = true;
                }
                if ($team->validate()) {
                    if ($largeExists && $smallExists) {
                        Debug::logToWindow(getcwd());
                        if (!$team->file_small->saveAs('images/jerseys/small/' . $team->file_small))
                            throw new Exception('Small file was not saved');
                        if (!$team->file_large->saveAs('images/jerseys/large/' . $team->file_large))
                            throw new Exception('Large file was not saved');
                    }
                    if (!$team->save()) {
                        throw new Exception('Team was not saved');
                    }
                } else {
                    $this->render('add', array('team' => $team));
                    break;
                }

                $t->commit();
                $this->redirect(AdminUtils::aUrl('team/index'));
            } catch (Exception $e) {
                error_log($e);
                $t->rollback();
            } catch (CDbException $dbe) {
                error_log($dbe);
                $t->rollback();
            }
        }
        $this->render('add', array(
            'team' => $team,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $t = $this->loadModel($id);
        $message = '';

        if (count($t->players) == 0 && count($t->gamesHome) + count($t->gamesAway) == 0)
            $t->delete();
        else {
            if (count($t->gamesHome) > 0 || count($t->gamesAway) > 0)
                $message .= '<br>' . $t->name . ' has played matches and cannot be deleted.';
            if (count($t->players) > 0)
                $message .= '<br>' . $t->name . ' contains players and cannot be deleted.';

            _user()->setFlash('failure', $message);
        }

        $this->redirect(AdminUtils::aUrl('team/index'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Team::model()->findByPk((int) $id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    
    public static function getCountries() {
        return CHtml::listData(Country::model()->findAll(), 'id', 'country');
    }

}
