<?php

class ThemeController extends AdminController {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'update'),
                'expression' => 'User::hasRole(AdminRole::ADMIN | AdminRole::OPERATOR)',
            ),
            array('allow',
                'actions' => array('delete', 'newRule'),
                'expression' => 'User::hasRole(AdminRole::ADMIN)',
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $rules = new CmsTheme('search');
        $this->render('index', array(
            'rules' => $rules,
            'prettyPhotoOptions' => $this->loadPrettyPhoto(),
        ));
    }

    public function actionNewRule() {
        $rule = new CmsTheme;
        if ($rule->edit()) {
            _app()->user->setFlash('success', 'Rule added.');
            $this->redirect(AdminUtils::aUrl('theme/index'));
        }

        $this->render('rule', array(
            'rule' => $rule,
            'submit' => 'Save',
            'ruleTypes' => CmsTheme::model()->ruleTypes(),
            'prettyPhotoOptions' => $this->loadPrettyPhoto(),
        ));
    }

    public function actionUpdate($id) {
        $rule = CmsTheme::model()->findByPk($id);
        if ($rule->edit()) {
            _app()->user->setFlash('success', 'Rule updated.');
            $this->redirect(AdminUtils::aUrl('theme/index'));
        }

        $this->render('rule', array(
            'rule' => $rule,
            'submit' => 'Update',
            'ruleTypes' => CmsTheme::model()->ruleTypes(),
            'prettyPhotoOptions' => $this->loadPrettyPhoto(),
        ));
    }

    public function actionDelete($id) {
        CmsTheme::model()->deleteByPk($id);
    }

    private function loadPrettyPhoto() {
        Yii::import('application.extensions.jqPrettyPhoto');
        return array(
            'slideshow' => 5000,
            'autoplay_slideshow' => false,
            'show_title' => false
        );
    }

}

?>
