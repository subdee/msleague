<?php

class UserController extends AdminController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
//	public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'add', 'update', 'delete'),
                'expression' => 'User::hasRole(AdminRole::ADMIN)',
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists all users
     */
    public function actionIndex() {

        $users = new User('search');

        $this->render('index', array('users' => $users));
    }

    /**
     * Adds a new user
     */
    public function actionAdd() {
        $user = new User;

        if (isset($_POST['User'])) {
            $user->attributes = $_POST['User'];
            $user->password = User::model()->encrypt($_POST['User']['password']);
            if ($user->save())
                $this->redirect(AdminUtils::aUrl('user/index'));
        }

        $this->render('add', array('user' => $user));
    }

    /**
     * Update a user
     * @param integer $id 
     */
    public function actionUpdate($id) {

        $user = $this->loadModel($id);

        if (isset($_POST['User'])) {
            $user->attributes = $_POST['User'];
            $user->password = User::model()->encrypt($_POST['User']['password']);
            $user->save();

            $this->redirect(AdminUtils::aUrl('user/index'));
        }

        $this->render('update', array('user' => $user));
    }

    /**
     * Delete a user
     * @param integer $id 
     */
    public function actionDelete($id) {
        $user = $this->loadModel($id);

        $user->delete();

        $this->redirect(AdminUtils::aUrl('user/index'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = User::model()->findByPk((int) $id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

}
