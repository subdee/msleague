<?php

class AdminRole {
    
    const MODERATOR = 1;
    const OPERATOR = 2;
    const ADMIN = 4;
    
    /**
     * Role comparison for admin users
     * @param int $role
     * @param int $userRole
     * @return bool If role matches 
     */
    public static function is($role,$userRole){
        if (($role & $userRole) == $userRole) 
            return true;
        return false;
    }
    
    /**
     * Get the string representation of a role
     * @param int $idx
     * @return string The role
     */
    public static function getRoles($idx = null){
        $roles = array(
            self::MODERATOR => 'Moderator',
            self::OPERATOR => 'Operator',
        );
        if ($idx != null)
            return $roles[$idx];
        else
            return $roles;
    }
    
}

?>