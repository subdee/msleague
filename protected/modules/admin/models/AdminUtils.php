<?php

class AdminUtils extends Utils {

    /**
     *
     * @param type $file
     */
    public static function registerAdminCssFile($file, $media = '') {
        _cs()->registerCSSFile(self::_adminAssetUrl("css/{$file}.css"), $media);
    }

    /**
     *
     * @param type $file
     */
    public static function registerAdminJsFile($file) {
        _cs()->registerScriptFile(self::_adminAssetUrl("js/{$file}.js"));
    }

    /**
     *
     * @param type $asset
     * @return string
     */
    private static function _adminAssetUrl($asset='') {
        $alias = "admin.assets";
        $assetUrl = _app()->assetManager->publish(Yii::getPathOfAlias($alias), false, -1, Debug::isDebug()) . '/' . $asset;
        return $assetUrl;
    }

    /**
     *
     * @param type $image
     * @return type
     */
    public static function adminImageUrl($image) {
        return self::_adminAssetUrl("images/$image");
    }

    /**
     * Create an admin URL
     * @param type $route
     * @param type $params
     * @param type $ampersand
     * @return type
     */
    public static function aUrl($route, $params=array(), $ampersand='&') {
        return Yii::app()->createUrl('admin/'.$route, $params + array('lang' => _app()->language), $ampersand);
    }

}

?>
