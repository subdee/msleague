<?php

/**
 * This is the model class for table "newsletter".
 *
 * The followings are the available columns in table 'newsletter':
 * @property integer $id
 * @property string $subject
 * @property string $content
 * @property integer $is_auto
 */
class Newsletter extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return Newsletter the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'newsletter';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('subject, content', 'required'),
            array('is_auto, manager_group_id', 'numerical', 'integerOnly' => true),
            array('subject', 'length', 'max' => 50),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, subject', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'managerGroup' => array(self::BELONGS_TO, 'ManagerGroup', 'manager_group_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'subject' => 'Subject',
            'content' => 'Content',
            'is_auto' => 'Is Auto',
            'manager_group_id' => 'Group'
        );
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->last_sent =
                Yii::app()->localtime->fromLocalDateTime(
                $this->last_sent, 'Y-m-d H:i:s');
            return true;
        }
        else
            return false;
    }

    protected function afterFind() {
        $this->last_sent =
            Yii::app()->localtime->toLocalDateTime(
            $this->last_sent, 'Y-m-d H:i:s');

        return (parent::afterFind());
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('subject', $this->subject, true);

        $criteria->addCondition('is_auto = 0');

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
            ));
    }

    public function searchAuto() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('subject', $this->subject, true);

        $criteria->addCondition('is_auto = 1');

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
            ));
    }

}