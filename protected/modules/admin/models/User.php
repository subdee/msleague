<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property integer $role
 *
 * The followings are the available model relations:
 * @property Announcement[] $announcements
 * @property Role $role
 */
class User extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return User the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('username, password, role', 'required'),
            array('role', 'numerical', 'integerOnly' => true),
            array('username, password', 'length', 'max' => 45),
            array('username', 'unique'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, username, password, role', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'announcements' => array(self::HAS_MANY, 'Announcement', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'role' => 'Role',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {

        $criteria = new CDbCriteria;
        
        $criteria->params = array(':role' => AdminRole::ADMIN);

        $criteria->addCondition('role != :role');

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Encrypt a password with MD5 hash
     * @param string $password
     * @return string
     */
    public function encrypt($password) {
        return md5($password);
    }

    /**
     * Returns true if user has the given role(s).
     * @return bool
     */
    public static function hasRole($role) {
        if (_user()->isGuest)
            return false;
        return AdminRole::is($role, _user()->role);
    }

}