<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'announcement-form',
	'enableAjaxValidation'=>false,'enableClientValidation'=>true,
)); ?>

    <div class="row">
        <?php echo $form->labelEx($ann,'title'); ?>
        <?php echo $form->textField($ann,'title'); ?>
        <?php echo $form->error($ann,'title'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($ann,'content'); ?>
        <?php echo $form->textArea($ann,'content',array('cols'=>'70','rows'=>'15')); ?>
        <?php echo $form->error($ann,'content'); ?>
    </div>
    
    <div class="row buttons">
        <?php echo CHtml::submitButton('Save'); ?>
    </div>

<?php $this->endWidget(); ?>
<?php echo CHtml::link('Cancel',AdminUtils::aUrl('announcement/index')); ?>
</div>