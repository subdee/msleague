<div class="stables">
    <a href="<?php echo AdminUtils::aUrl('announcement/add'); ?>"><div class="addButton">Add new announcement</div></a>
    <br /><br />
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $anns->search(),    'columns' => array(
        array(
            'name' => 'title',
            'htmlOptions'=>array('class'=>'tcenter')
        ),
        array(
            'name' => 'date_entered',
            'value' => 'Utils::date($data->date_entered)',
            'htmlOptions'=>array('class'=>'tcenter')
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}{delete}',
            'updateButtonImageUrl' => AdminUtils::adminImageUrl('edit.png'),
            'deleteButtonImageUrl' => AdminUtils::adminImageUrl('delete.png'),
            'updateButtonUrl' => 'AdminUtils::aUrl("announcement/edit", array("id" => $data->id))',
            'deleteButtonUrl' => 'AdminUtils::aUrl("announcement/delete", array("id" => $data->id))',
            'deleteConfirmation' => "Are you sure you want to delete this announcement?",
        ),
    ),
));
?>
</div>