<div class="stables">
    <span><?php echo _user()->hasFlash('failure') ? _user()->getFlash('failure') : ''; ?>
</div>
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
            'id' => 'reason-form',
            'enableAjaxValidation' => false, 'enableClientValidation' => true,
        ));
    ?>

    <div class="row">
        <?php echo $form->labelEx($reason, 'reason'); ?>
        <?php echo $form->textArea($reason, 'reason',array('cols'=>40,'rows'=>6)); ?>
        <?php echo $form->error($reason, 'reason'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($reason, 'reason_type'); ?>
        <?php echo $form->dropDownList($reason, 'reason_type', $reasonTypes); ?>
        <?php echo $form->error($reason, 'reason_type'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($reason, 'reportAction'); ?>
        <?php echo $form->listBox($reason, 'reportAction', CHtml::listData(ReportAction::model()->findAll(), 'id', 'action'),array('multiple' => 'multiple','size' => 6)); ?>
        <?php echo $form->error($reason, 'reportAction'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($reason, 'language_id'); ?>
        <?php echo $form->dropDownList($reason, 'language_id', CHtml::listData(Language::model()->findAll(), 'id', 'code')); ?>
        <?php echo $form->error($reason, 'language_id'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Save'); ?>
        <?php echo CHtml::link('Cancel',AdminUtils::aUrl('cms/reportReasons')); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>
