<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
            'id' => 'tip-form',
            'enableAjaxValidation' => false, 'enableClientValidation' => true,
        ));
    ?>

    <div class="row">
        <?php echo $form->labelEx($tip, 'tip'); ?>
        <?php echo $form->textArea($tip, 'tip',array('cols'=>40,'rows'=>6)); ?>
        <?php echo $form->error($tip, 'tip'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($tip, 'tip_group'); ?>
        <?php echo $form->dropDownList($tip, 'tip_group', array(1=>1,2=>2,3=>3)); ?>
        <?php echo $form->error($tip, 'tip_group'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($tip, 'lang'); ?>
        <?php echo $form->dropDownList($tip, 'lang', CHtml::listData(Language::model()->findAll(), 'id', 'code')); ?>
        <?php echo $form->error($tip, 'lang'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Save'); ?>
        <?php echo CHtml::link('Cancel',AdminUtils::aUrl('cms/tips')); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>
