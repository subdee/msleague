<?php
echo CHtml::form(AdminUtils::aUrl('cms/index'));
echo CHtml::dropDownList('lang', $lang, CHtml::listData(Language::model()->findAll(), 'id', 'code'), array('submit' => ''));
echo CHtml::endForm();
?>
<div class="tabs">
    <ul>
        <?php foreach ($sc as $texts) : ?>
            <li><a href="#<?php echo $texts->id . $texts->lang; ?>"><?php echo $texts->section; ?></a></li>
        <?php endforeach; ?>
    </ul>
    <?php foreach ($sc as $text) : ?>
        <div id="<?php echo $text->id . $text->lang; ?>">
            <?php echo CHtml::form(AdminUtils::aUrl('cms/save')); ?>
            <div class="row">
                <?php if ($text->section == 'VideoUrl') : ?>
                    <em>http://www.youtube.com/watch?v=<?php echo CHtml::textField($text->section, $text->content); ?></em>
                <?php else : ?>
                    <?php
                    $this->widget('ext.editMe.ExtEditMe', array(
                        'name' => $text->section,
                        'value' => $text->content,
                        'htmlOptions' => array('option' => 'value'),
                        'toolbar' => array(
                            array('Undo', '-', 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'),
                            array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'BidiLtr', 'BidiRtl'),
                            array('Link', 'Unlink', 'Anchor'),
                            array('Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'),
                            '/',
                            array('Styles', 'Format', 'Font', 'FontSize'),
                            array('TextColor', 'BGColor'),
                        )
                    ));
                    ?>
                <?php endif; ?>
            </div>
            <?php echo CHtml::hiddenField('lang', $text->lang); ?>
            <div class="row buttons">
                <?php echo CHtml::submitButton('Save', array('id' => 'submitBtn')); ?>
            </div>
            <?php CHtml::endForm(); ?>
        </div>
    <?php endforeach; ?>
</div>

<script>
    $(function() {
        $( ".tabs" ).tabs();
    });
</script>