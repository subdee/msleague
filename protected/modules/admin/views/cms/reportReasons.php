<a href="<?php echo AdminUtils::aUrl('cms/addReason'); ?>"><div class="addButton">Add new reason</div></a>
<br/>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $reasons->search(),    'columns' => array(
        array(
            'name' => 'reportActions',
            'value' =>'$data->getReportActions()',
            'htmlOptions'=>array('width'=>'15%','class'=>'tcenter')
        ),
        array(
            'name' => 'reason_type',
            'value' => 'ReportReason::getType($data->reason_type)',
            'htmlOptions'=>array('width'=>'15%')
        ),
        array(
            'name' => 'reason',
            'htmlOptions'=>array('width'=>'60%')
        ),
        array(
            'name' => 'language.code',
            'htmlOptions'=>array('width'=>'10%','class'=>'tcenter')
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{delete}',
            'deleteButtonImageUrl' => AdminUtils::adminImageUrl('delete.png'),
            'deleteButtonUrl' =>' AdminUtils::aUrl("cms/deleteReason",array("id" => $data->id))',
            'deleteConfirmation' => "All reports for this reason will be deleted? Are you sure you want to delete this reason?",
        ),
    ),
));
?>