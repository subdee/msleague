<a href="<?php echo AdminUtils::aUrl('cms/addTip'); ?>"><div class="addButton">Add new tip</div></a>
<br/>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $tips->search(),    'columns' => array(
        array(
            'name' => 'tip_group',
            'htmlOptions'=>array('width'=>'10%','class'=>'tcenter')
        ),
        array(
            'name' => 'tip',
            'htmlOptions'=>array('width'=>'70%')
        ),
        array(
            'name' => 'lang0.code',
            'htmlOptions'=>array('width'=>'10%','class'=>'tcenter')
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{delete}',
            'deleteButtonImageUrl' => AdminUtils::adminImageUrl('delete.png'),
        ),
    ),
));
?>