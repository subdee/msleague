<?php

echo CHtml::label(_t('Closing date'), $feature->getVarName());
$this->widget('application.extensions.timepicker.EJuiDateTimePicker', array(
    'name' => $feature->getVarNameFor('closingDate'),
    'value' => $feature->closingDate,
    'options' => array(
        'hourGrid' => 4,
        'timeFormat' => 'hh:mm:ss',
        'changeMonth' => true,
        'changeYear' => true,
        'minDate' => date('Y-m-d'),

        'showButtonPanel' => true,
        'showOn' => "button",
        'buttonImage' => Utils::imageUrl('iconset/calendar.png'),
        'buttonImageOnly' => true,
        'dateFormat' => 'yy-mm-dd',
    ),
));
?>
