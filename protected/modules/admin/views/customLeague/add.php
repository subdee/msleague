<?php $this->widget('Notice', array('session' => 'league')); ?>
<?php echo CHtml::openTag('div', array('class' => 'form')); ?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'htmlOptions' => array(
        'onSubmit' => '$(this).find(":submit").attr("disabled", true);',
        'class' => 'add-league',
    ),
    ));
?>

<div class="row">
    <?php
    echo $form->labelEx($league, 'type');
    echo $form->dropDownList($league, 'type', CustomLeagueType::toArray(), array(
        'onchange' => 'if($(this).val() == ' . CustomLeagueType::USER . ') { $(".row.user").show(); $(".row.team,.hint.team").hide(); }
                else if($(this).val() == ' . CustomLeagueType::TEAM . ') { $(".row.team,.hint.team").show(); $(".row.user").hide(); }
                else { $(".row.team,.hint.team,.row.user").hide(); }'
    ));
    ?>
</div>

<fieldset>
    <legend><?php echo _t('League attributes'); ?></legend>

    <?php
    echo CHtml::openTag('div', array('class' => 'row user ' . (($league->type != CustomLeagueType::USER) ? 'hidden' : '')));
    echo $form->labelEx($league, 'creatorUsername');
    echo $form->textField($league, 'creatorUsername');
    echo $form->error($league, 'creatorUsername');
    echo CHtml::closeTag('div');
    ?>

    <?php
    echo CHtml::openTag('div', array('class' => 'row team ' . (($league->type != CustomLeagueType::TEAM) ? 'hidden' : '')));
    echo $form->labelEx($team, 'team_id');
    echo $form->dropDownList($team, 'team_id', CHtml::listData(Team::model()->findAll(), 'id', 'name'));
    echo CHtml::closeTag('div');
    ?>

    <div class="row">
        <?php echo $form->labelEx($league, 'name'); ?>
        <?php echo $form->textField($league, 'name'); ?>
        <?php echo CHtml::tag('span', array('class' => 'hint team ' . (($league->type != CustomLeagueType::TEAM) ? 'hidden' : '')), _t('You can use the variable {team}')); ?>
        <?php echo $form->error($league, 'name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($league, 'capacity'); ?>
        <?php echo $form->textField($league, 'capacity'); ?>
        <?php echo CHtml::tag('span', array('class' => 'hint'), _t('{unlimited} for unlimited capacity (max {max})', array('{unlimited}' => CustomLeague::CAPACITY_UNLIMITED, '{max}' => CustomLeagueConfig::model()->maxManagersPerLeague))); ?>
        <?php echo $form->error($league, 'capacity'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($league, 'privacy'); ?>
        <?php echo $form->dropDownList($league, 'privacy', CustomLeaguePrivacy::toArray()); ?>
    </div>
</fieldset>

<fieldset>
    <legend><?php echo _t('Features'); ?></legend>

    <?php
    foreach (CustomLeagueFeature::toArray() as $key => $feature) {
        $class = CustomLeagueFeature::className($key);
        $model = new $class;
        $model->adminHtml();
    }
    ?>

</fieldset>

<div class="row buttons">
    <?php echo CHtml::submitButton(_t('Add')); ?>
</div>


<?php
$this->endWidget();
echo CHtml::closeTag('div');
?>
