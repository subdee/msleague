<?php $this->widget('Notice', array('session' => 'league')); ?>
<?php echo CHtml::openTag('div', array('class' => 'form')); ?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'htmlOptions' => array(
        'onSubmit' => '$(this).find(":submit").attr("disabled", true);',
        'class' => 'league-config',
    ),
    ));
?>
<fieldset>
    <legend><?php echo _t('Leagues configuration'); ?></legend>
    <?php foreach ($model->attributes as $key => $attr) : ?>
        <?php
        if ($key == 'id')
            continue;
        ?>
        <div class="row">
            <?php echo $form->labelEx($model, $key); ?>
            <?php echo $form->textField($model, $key); ?>
            <?php echo $form->error($model, $key); ?>
        </div>

    <?php endforeach; ?>
</fieldset>

<div class="row buttons">
    <?php echo CHtml::submitButton(_t('Update')); ?>
</div>

<?php $this->endWidget(); ?>
<?php echo CHtml::closeTag('div'); ?>