<?php

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'leagues-grid',
    'dataProvider' => $model->searchAdmin(),
    'filter' => $model,
    'cssFile' => _bu('/css/gridview.css'),
    'columns' => array(
        array(
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + $row + 1',
            'htmlOptions' => array('class' => 'first-column'),
        ),
        array(
            'type' => 'raw',
            'name' => 'name',
            'value' => 'CHtml::link($data->name, _url("customLeague/admin/league", array("id" => $data->id)))',
        ),
        array(
            'header' => 'Members',
            'name' => 'numOfMembers',
            'value' => '$data->numOfMembers',
            'filter' => false,
            'htmlOptions' => array('class' => 'tcenter'),
        ),
        array(
            'name' => 'date_created',
            'value' => 'Utils::date($data->date_created, array("showYearIfDifferent" => true))',
            'filter' => false,
            'htmlOptions' => array('class' => 'datetime'),
        ),
    ),
));
?>
