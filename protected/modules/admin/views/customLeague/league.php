<?php

echo 'Back to ';
echo CHtml::link('Leagues &raquo;&nbsp;', _url('customLeague/index'));
echo CHtml::tag('span', array(), $league->name);

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'leagues-grid',
    'dataProvider' => $model->searchAdmin(),
    'filter' => $model,
    'cssFile' => _bu('/css/gridview.css'),
    'columns' => array(
        array(
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + $row + 1',
            'htmlOptions' => array('class' => 'first-column'),
        ),
        array(
            'name' => 'manager_id',
            'value' => '$data->manager->username',
            'type' => 'raw',
        ),
        array(
            'value' => '($data->league->isCreator($data->manager_id) ? "(C)" : "")',
            'htmlOptions' => array('class' => 'first-column'),
        ),
        array(
            'name' => 'manager.total_points',
            'htmlOptions' => array('class' => 'tcenter'),
        ),
        array(
            'name' => 'managerRole',
            'value' => '$data->manager->roleToText()',
            'htmlOptions' => array('class' => 'tcenter'),
            'filter' => Role::toArray(),
        ),
        array(
            'name' => 'join_date',
            'value' => 'Utils::date($data->join_date, array("showYearIfDifferent" => true))',
            'filter' => false,
            'htmlOptions' => array('class' => 'datetime'),
        ),
        array(
            'type' => 'raw',
            'value' => 'CHtml::link("other leagues", _url("customLeague/admin/manager", array("id" => $data->manager_id)))'
        )
    ),
));
?>
