<?php

echo 'Leagues for ' . $manager->username;

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'leagues-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'cssFile' => _bu('/css/gridview.css'),
    'columns' => array(
        array(
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + $row + 1',
            'htmlOptions' => array('class' => 'first-column'),
        ),
        array(
            'type' => 'raw',
            'name' => 'league.name',
            'value' => 'CHtml::link($data->league->name, _url("customLeague/admin/league", array("id" => $data->league_id)))',
        ),
        array(
            'name' => 'league.type',
            'value' => '$data->league->typeToText()',
            'htmlOptions' => array('class' => 'tcenter'),
        ),
        array(
            'name' => 'league.privacy',
            'value' => '$data->league->privacyToText()',
            'htmlOptions' => array('class' => 'tcenter'),
        )
    ),
));
?>
