<?php

$cols = array(
    array(
        'header' => '',
        'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + $row + 1',
        'filter' => false,
        'htmlOptions' => array('class' => 'first-column index')
    ),
    array(
        'type' => 'raw',
        'header' => 'Manager',
        'name' => 'commentor_id',
        'value' => '$data->commentor ? $data->commentor->username : ' . $nullCommentor,
        'htmlOptions' => array('class' => 'commentor')
    ),
    array(
        'type' => 'raw',
        'name' => 'content',
        'htmlOptions' => array('class' => 'content-text comment'),
    ),
    array(
        'name' => 'date_posted',
        'value' => 'Utils::date($data->date_posted, array("yearIfDifferent" => true))',
        'htmlOptions' => array('class' => 'datetime'),
        'filter' => false,
    )
);

$defaultButtons = array(
    'hideme' => array(
        'label' => 'Hide comment',
        'url' => 'AdminUtils::aUrl("forum/hideComment", array("id" => $data->id))',
        'imageUrl' => AdminUtils::adminImageUrl('eye.png'),
        'click' => 'js:function(){
                        $("#comments-grid").addClass("grid-view-loading");
                        $.get($(this).attr("href"), function(response) {
                                $("#comments-grid").removeClass("grid-view-loading");
                                if(response.success)
                                    $.fn.yiiGridView.update("comments-grid");
                        });
                        return false;
                    }',
        'visible' => '$data->visible',
    ),
    'showme' => array(
        'label' => 'Show comment',
        'url' => 'AdminUtils::aUrl("forum/showComment", array("id" => $data->id))',
        'imageUrl' => AdminUtils::adminImageUrl('eye.png'),
        'options' => array('style' => 'opacity: 0.5'),
        'click' => 'js:function(){
                        $("#comments-grid").addClass("grid-view-loading");
                        $.get($(this).attr("href"), function(response) {
                                $("#discussions-grid").removeClass("grid-view-loading");
                                if(response.success)
                                    $.fn.yiiGridView.update("comments-grid");
                        });
                        return false;
                    }',
        'visible' => '!$data->visible',
    ),
);

if ((isset($buttons) && $buttons !== FALSE) || !isset($buttons)) {

    if (isset($buttons['showme']))
        $defaultButtons['showme'] = $buttons['showme'] + $defaultButtons['showme'];
    if (isset($buttons['hideme']))
        $defaultButtons['hideme'] = $buttons['hideme'] + $defaultButtons['hideme'];

    $cols[] = array(
        'class' => 'CButtonColumn',
        'template' => '{hideme}{showme}',
        'buttons' => $defaultButtons
    );
}

if (isset($columns) && $columns !== FALSE) {
    $cols = $columns + $cols;
}

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'comments-grid',
    'dataProvider' => $comments->search(),
    'filter' => $comments,
    'htmlOptions' => array('class' => "grid-view $gridviewCssClass"),
    'rowCssClassExpression' => isset($rowCssClassExpression) ? $rowCssClassExpression : null,
    'columns' => $cols,
));
?>
