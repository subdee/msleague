<?php if (Yii::app()->user->hasFlash('success')): ?>
    <div class="info">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>
<?php if (Yii::app()->user->hasFlash('error')): ?>
    <div class="info">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
<?php endif; ?>

<div class="form">
    <?php echo CHtml::form(AdminUtils::aUrl('forum/update', array('id' => $category->id)), 'get'); ?>
    <div class="row links">
        <?php echo CHtml::label(_t('Language'), 'lang'); ?>
        <?php
        //echo CHtml::dropDownList('lang', _app()->language, CHtml::listData($languages, 'code', 'code'), array('submit' => ''));
        foreach ($languages as $lang) {
            $exists = CategoryText::model()->exists('lang = :lang and category_id = :cid', array(':lang' => $lang->code, ':cid' => $category->id));
            $active = _app()->language == $lang->code ? 'active' : '';
            echo CHtml::link($lang->code . ($exists ? ',' : '(+),'), AdminUtils::aUrl('forum/' . _app()->controller->action->id, array('id' => $category->id, 'lang' => $lang->code)), array('class' => $active));
        }
        ?>
    </div>
    <?php echo CHtml::endForm(); ?>
</div>
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
    ?>

    <div class="row">
        <?php echo $form->labelEx($category->text, 'title'); ?>
        <?php echo $form->textField($category->text, 'title'); ?>
        <?php echo $form->error($category->text, 'title'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($category->text, 'description'); ?>
        <?php echo $form->textArea($category->text, 'description'); ?>
        <?php echo $form->error($category->text, 'description'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($category, 'uploadImage'); ?>
        <?php echo $form->FileField($category, 'uploadImage'); ?>
        <?php echo $form->error($category, 'uploadImage'); ?>
        <?php echo CHtml::tag('div', array('class' => 'hint'), '24px and up to 100k'); ?>
    </div>
    <?php if ($action == 'update') : ?>
        <?php if ($category->image) : ?>
            <div class="row uploaded-image">
                <?php echo CHtml::image($category->uploadImageUrl(), 'Missing uploaded image'); ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <div class="row">
        <?php echo $form->labelEx($category, 'visible'); ?>
        <?php echo $form->checkBox($category, 'visible', array('checked' => $category->visible)); ?>
        <?php echo $form->error($category, 'visible'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($action); ?>
        <?php echo CHtml::link('Cancel', AdminUtils::aUrl('forum/index')); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>