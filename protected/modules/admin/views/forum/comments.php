<?php

echo 'Back to ';
echo CHtml::link('Categories &raquo;&nbsp;', AdminUtils::aUrl('forum/index'));
echo CHtml::link("{$category->category->text->title} &raquo;&nbsp;", AdminUtils::aUrl('forum/discussions', array('category' => $category->category_id)));
echo CHtml::tag('span', array(), $category->title);

$this->renderPartial('_comments', array(
    'comments' => $comments,
    'gridviewCssClass' => 'comments',
    'rowCssClassExpression' => '$data->id == ' . $category->metadata_id() . ' ? "metadata" : ""',
    'nullCommentor' => "CHtml::tag('span', array('class' => 'deleted'), 'deleted')",
    'buttons' => array(
        'showme' => array(
            // The next line, disables show option for the first comment which is the discussion metadata.
            'visible' => '!$data->visible && $data->id != ' . $category->metadata_id(),
        )
    )
));
?>