<?php

echo 'Back to ';
echo CHtml::link('Categories &raquo;&nbsp;', AdminUtils::aUrl('forum/index'));
echo CHtml::tag('span', array(), $category->text->title);

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'discussions-grid',
    'dataProvider' => $discussions->search(),
    'filter' => $discussions, 'columns' => array(
        array(
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + $row + 1',
            'htmlOptions' => array('class' => 'first-column'),
        ),
        array(
            'type' => 'raw',
            'name' => 'title',
            'value' => '
                CHtml::link($data->title, AdminUtils::aUrl("forum/comments", array("discussion" => $data->discussion_id))) .
                CHtml::tag("div", array("class" => "description"), $data->description())
                ',
            'htmlOptions' => array('class' => 'content-text discussion'),
        ),
        array(
            'type' => 'raw',
            'header' => CHtml::image(AdminUtils::adminImageUrl('commentCount.png')),
            'name' => 'commentCount',
            'filter' => false,
            'value' => '$data->realCommentCount(true)',
            'htmlOptions' => array('class' => 'tcenter'),
        ),
        array(
            'header' => 'Date updated',
            'name' => 'dateUpdated',
            'filter' => false,
            'value' => 'Utils::date($data->date_created(), array("yearIfDifferent" => true))',
            'htmlOptions' => array('class' => 'datetime'),
        ),
        array(
            'header' => 'Date created',
            'name' => 'dateCreated',
            'filter' => false,
            'value' => 'Utils::date($data->date_created(), array("yearIfDifferent" => true))',
            'htmlOptions' => array('class' => 'datetime'),
        ),
        array(
            'type' => 'raw',
            'header' => 'Creator',
            'name' => 'creatorUsername',
            'value' => '$data->owner() ? $data->owner()->username : "<em>(deleted user)</em>"',
//            'filter' => CHtml::textField('Manager[username]', $username, array('maxlength' => 17)),
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{close}{open}{hideme}{showme}{delete}',
            'deleteButtonImageUrl' => AdminUtils::adminImageUrl('delete.png'),
            'deleteButtonUrl' => 'AdminUtils::aUrl("forum/deleteDiscussion", array("id" => $data->discussion_id))',
            'deleteConfirmation' => 'Are you sure you want to delete this discussion and its comments?',
            'buttons' => array(
                'close' => array(
                    'label' => 'Turn comments off',
                    'url' => 'AdminUtils::aUrl("forum/closeDiscussion", array("id" => $data->discussion_id))',
                    'imageUrl' => AdminUtils::adminImageUrl('on_comment.png'),
                    'visible' => '$data->discussion->comments_open',
                    'click' => 'js:function(){
                        $("#discussions-grid").addClass("grid-view-loading");
                        $.get($(this).attr("href"), function(response) {
                                $("#discussions-grid").removeClass("grid-view-loading");
                                if(response.success)
                                    $.fn.yiiGridView.update("discussions-grid");
                        });
                        return false;
                    }',
                ),
                'open' => array(
                    'label' => 'Turn comments on',
                    'url' => 'AdminUtils::aUrl("forum/openDiscussion", array("id" => $data->discussion_id))',
                    'imageUrl' => AdminUtils::adminImageUrl('off_comment.png'),
                    'visible' => '!$data->discussion->comments_open',
                    'click' => 'js:function(){
                        $("#discussions-grid").addClass("grid-view-loading");
                        $.get($(this).attr("href"), function(response) {
                                $("#discussions-grid").removeClass("grid-view-loading");
                                if(response.success)
                                    $.fn.yiiGridView.update("discussions-grid");
                        });
                        return false;
                    }',
                ),
                'hideme' => array(
                    'label' => 'Hide discussion',
                    'url' => 'AdminUtils::aUrl("forum/hideDiscussion", array("id" => $data->discussion_id))',
                    'imageUrl' => AdminUtils::adminImageUrl('eye.png'),
                    'click' => 'js:function(){
                        $("#discussions-grid").addClass("grid-view-loading");
                        $.get($(this).attr("href"), function(response) {
                                $("#discussions-grid").removeClass("grid-view-loading");
                                if(response.success)
                                    $.fn.yiiGridView.update("discussions-grid");
                        });
                        return false;
                    }',
                    'visible' => '$data->discussion->visible',
                ),
                'showme' => array(
                    'label' => 'Show discussion',
                    'url' => 'AdminUtils::aUrl("forum/showDiscussion", array("id" => $data->discussion_id))',
                    'imageUrl' => AdminUtils::adminImageUrl('eye.png'),
                    'options' => array('style' => 'opacity: 0.5'),
                    'click' => 'js:function(){
                        $("#discussions-grid").addClass("grid-view-loading");
                        $.get($(this).attr("href"), function(response) {
                                $("#discussions-grid").removeClass("grid-view-loading");
                                if(response.success)
                                    $.fn.yiiGridView.update("discussions-grid");
                        });
                        return false;
                    }',
                    'visible' => '!$data->discussion->visible',
                ),
            ),
        ),
    ),
));
?>