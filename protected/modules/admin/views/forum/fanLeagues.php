<?php

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'discussions-grid',
    'dataProvider' => $discussions->search(),
    'filter' => $discussions,    'columns' => array(
        array(
            'header' => '',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + $row + 1',
            'htmlOptions' => array('class' => 'first-column'),
        ),
        array(
            'type' => 'raw',
            'name' => 'league_id',
            'value' => 'CHtml::link($data->league->name, AdminUtils::aUrl("forum/league", array("discussion" => $data->discussion_id)))',
            'htmlOptions' => array('class' => 'content-text'),
        ),
        array(
            'header' => CHtml::image(AdminUtils::adminImageUrl('commentCount.png')),
            'name' => 'commentCount',
            'filter' => false,
            'value' => '$data->commentCount',
            'htmlOptions' => array('class' => 'tcenter'),
        ),
        array(
            'header' => 'Date updated',
            'name' => 'dateUpdated',
            'filter' => false,
            'value' => 'Utils::date($data->discussion->date_updated, array("yearIfDifferent" => true))',
            'htmlOptions' => array('class' => 'datetime'),
        ),
        array(
            'header' => 'Date created',
            'name' => 'dateCreated',
            'filter' => false,
            'value' => 'Utils::date($data->league->date_created, array("yearIfDifferent" => true))',
            'htmlOptions' => array('class' => 'datetime'),
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{close}{open}',
            'buttons' => array(
                'close' => array(
                    'label' => 'Turn comments off',
                    'url' => 'AdminUtils::aUrl("forum/closeDiscussion", array("id" => $data->discussion_id))',
                    'imageUrl' => AdminUtils::adminImageUrl('on_comment.png'),
                    'visible' => '$data->discussion->comments_open',
                    'click' => 'js:function(){
                        $("#discussions-grid").addClass("grid-view-loading");
                        $.get($(this).attr("href"), function(response) {
                                $("#discussions-grid").removeClass("grid-view-loading");
                                if(response.success)
                                    $.fn.yiiGridView.update("discussions-grid");
                        });
                        return false;
                    }'
                ),
                'open' => array(
                    'label' => 'Turn comments on',
                    'url' => 'AdminUtils::aUrl("forum/openDiscussion", array("id" => $data->discussion_id))',
                    'imageUrl' => AdminUtils::adminImageUrl('off_comment.png'),
                    'visible' => '!$data->discussion->comments_open',
                    'click' => 'js:function(){
                        $("#discussions-grid").addClass("grid-view-loading");
                        $.get($(this).attr("href"), function(response) {
                                $("#discussions-grid").removeClass("grid-view-loading");
                                if(response.success)
                                    $.fn.yiiGridView.update("discussions-grid");
                        });
                        return false;
                    }'
                ),
            ),
        ),
    ),
));
?>