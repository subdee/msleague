<div class="controls">
    <?php
    echo CHtml::link(_t('+ Add category'), AdminUtils::aUrl('forum/addCategory'), array('class' => 'addButton'));
    foreach ($languages as $lang) {
        $count = CategoryText::model()->countByAttributes(array('lang' => $lang->code));
        $active = _app()->language == $lang->code ? 'active' : '';
        echo CHtml::link($lang->code . '(' . $count . ')', AdminUtils::aUrl('forum/index', array('lang' => $lang->code)), array('class' => "addButton $active"));
    }
    ?>
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $categories->search(),
    'columns' => array(
        array(
            'type' => 'raw',
            'name' => 'rank',
            'value' => '($data->rank == 1 ? "<span></span>" : CHtml::link("", AdminUtils::aUrl("forum/categoryRankUp", array("id" => $data->id)), array("class" => "asc"))) . ($data->rank == ' . $maxRank . ' ? "" : CHtml::link("", AdminUtils::aUrl("forum/categoryRankDown", array("id" => $data->id)), array("class" => "desc")))',
            'htmlOptions' => array('class' => 'rank')
        ),
        array(
            'type' => 'raw',
            'name' => 'title',
            'value' => 'CHtml::link($data->text->title, AdminUtils::aUrl("forum/discussions", array("category" => $data->id))).$data->text->description',
            'htmlOptions' => array('class' => 'content-text category'),
        ),
        array(
            'type' => 'raw',
            'name' => 'image',
            'value' => '$data->image ? CHtml::image($data->uploadImageUrl()) : "<em>no image</em>"',
            'htmlOptions' => array('class' => 'tcenter forum-category-image')
        ),
        array(
            'name' => 'visible',
            'value' => '$data->visible ? "yes" : "no"',
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'type' => 'raw',
            'header' => 'Discussions',
            'value' => 'CHtml::link(count($data->discussions), AdminUtils::aUrl("forum/discussions", array("category" => $data->id)))',
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}{delete}',
            'updateButtonImageUrl' => AdminUtils::adminImageUrl('edit.png'),
            'deleteButtonImageUrl' => AdminUtils::adminImageUrl('delete.png'),
            'updateButtonUrl' => 'AdminUtils::aUrl("forum/update", array("id" => $data->id))',
            'deleteConfirmation' => "All discussions of this category and their comments will be deleted.\nDo you want to delete this category?",
            'buttons' => array(
                'update' => array(
                    'visible' => 'User::hasRole(AdminRole::ADMIN)',
                ),
                'delete' => array(
                    'visible' => 'User::hasRole(AdminRole::ADMIN)',
                ),
            )
        ),
    ),
));
?>