<?php
echo 'Back to ';
echo CHtml::link('Leagues &raquo;&nbsp;', AdminUtils::aUrl('forum/leagues'));
echo CHtml::tag('span', array(), CHtml::tag('strong', array(), $league->name) . ($league->creator ? CHtml::tag('em', array(), ' by ' . $league->creator->username) : ''));

$this->renderPartial('_comments', array(
    'comments' => $comments,
    'gridviewCssClass' => 'league-comments',
    'nullCommentor' => "CHtml::tag('span', array('class' => 'deleted'), 'deleted')",
));
?>
