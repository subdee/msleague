<?php
echo 'Back to ';
echo CHtml::link('Matches &raquo;&nbsp;', AdminUtils::aUrl('forum/matches'));
echo CHtml::tag('span', array(), $game->teamHome0->name . ' - ' . $game->teamAway0->name);

$this->renderPartial('_comments', array(
    'comments' => $comments,
    'gridviewCssClass' => 'match-comments',
    'nullCommentor' => "CHtml::tag('span', array('class' => 'deleted'), 'deleted')",
));
?>
