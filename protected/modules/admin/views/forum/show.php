<?php

$this->renderPartial('_comments', array(
    'comments' => $comments,
    'gridviewCssClass' => 'comments',
    'rowCssClassExpression' => $category ? '$data->id == ' . $category->metadata_id() . ' ? "metadata" : ""' : "",
    'nullCommentor' => "CHtml::tag('span', array('class' => 'deleted'), 'deleted')",
    'buttons' => array(
        'showme' => array(
            // The next line, disables show option for the first comment which is the discussion metadata.
            'visible' => $category ? '!$data->visible && $data->id != ' . $category->metadata_id() : "",
        )
    )
));
?>