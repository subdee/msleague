<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'game-form',
	'enableAjaxValidation'=>false,'enableClientValidation'=>true,
)); ?>

    <div class="row">
        <?php echo $form->labelEx($game,'team_home'); ?>
        <?php echo $form->dropDownList($game,'team_home',$this->getTeams(),array('disabled'=>!$edit)); ?>
        <?php echo $form->error($game,'team_home'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($game,'team_away'); ?>
        <?php echo $form->dropDownList($game,'team_away',$this->getTeams(),array('disabled'=>!$edit)); ?>
        <?php echo $form->error($game,'team_away'); ?>
    </div>

<div class="row">
        <?php echo $form->labelEx($game,'date_played'); ?>
        <?php
        $this->widget('application.extensions.timepicker.EJuiDateTimePicker',array(
            'model'=>$game,
            'attribute'=>'date_played',
            'options'=>array(
                'hourGrid' => 4,
                'dateFormat'=>'yy-mm-dd',
                'timeFormat' => 'hh:mm:ss',
                'changeMonth' => true,
                'changeYear' => true,
                'minDate' => date('Y-m-d'),
                ),
            'htmlOptions'=>array('disabled'=>!$edit)
            ));
        ?>
        <?php echo $form->error($game,'date_played'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($game,'xml_id'); ?>
        <?php echo $form->textField($game,'xml_id'); ?>
        <?php echo $form->error($game,'xml_id'); ?>
    </div>

    <?php echo CHtml::activeHiddenField($game,'id'); ?>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Save'); ?>
        <?php echo CHtml::link('Cancel',AdminUtils::aUrl('game/index')); ?>
    </div>

<?php $this->endWidget(); ?>
</div>