<div class="stables">
    <?php if (User::hasRole(AdminRole::ADMIN)) : ?>
    
    <br/><br/>
    <?php endif; ?>
    <span><?php echo _user()->hasFlash('failure') ? _user()->getFlash('failure') : ''; ?></span>

    <?php
    echo CHtml::form(AdminUtils::aUrl('game/admin'), 'post');
    ?>
    <br /><br />
    <table>
        <thead>
            <tr>
                <th>Date/Time</th>
                <th>Home</th>
                <th>Score</th>
                <th>Away</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php
                            $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                'name'=>'search[date]',
                                'value'=>$search['date'],
                                'options'=>array(
                                    'dateFormat'=>'yy-mm-dd',
                                    'defaultDate'=>_app()->localtime->fromLocalDateTime(_app()->localtime->localNow,"Y-m-d"),
                                    ),
                                'htmlOptions'=>array('readonly'=>true,'submit'=>''),
                                ));
                            ?>
                </td>
                <td>
                    <?php echo CHtml::dropDownList('search[team_home]',$search['team_home'],$this->getTeams(),array('prompt'=>'-','submit'=>'')); ?>
                </td>
                <td></td>
                <td>
                    <?php echo CHtml::dropDownList('search[team_away]',$search['team_away'],$this->getTeams(),array('prompt'=>'-','submit'=>'')); ?>
                </td>
                <?php echo CHtml::endForm(); ?>
            </tr>
            <?php
            if ($m != null) :
                foreach ($m as $match) :
                    ?>
                    <tr>
                        <td><?php echo $match->date_played; ?></td>
                        <td><?php echo $match->teamHome0->name; ?></td>
                        <td><?php echo $match->score_home;
                    echo '-';
                    echo $match->score_away; ?></td>
                        <td><?php echo $match->teamAway0->name; ?></td>
                            <td><a class="confirm" href="<?php echo AdminUtils::aUrl('game/delete', array('id' => $match->id,'updateStats'=>true)); ?>" inf="<?php echo $match->teamHome0->name . '-' . $match->teamAway0->name; ?>" elem="the events for match"><img src="<?php echo AdminUtils::adminImageUrl('clear.png'); ?>" title="Clear stats"></a></td>
                    </tr>
    <?php endforeach;
    endif; ?>
        </tbody>
    </table>
</div>

<div id="dialog"></div>

<div id="dialogDeleting"><p>Now deleting game's events and restoring points to players and managers. Please do not interrupt.</p><img src="<?php echo AdminUtils::adminImageUrl('loading.gif'); ?>"></div>

<div id="uploadPop"></div>