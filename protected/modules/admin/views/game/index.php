<div class="stables">
    <?php if (User::hasRole(AdminRole::ADMIN)) : ?>

        <br/><br/>
    <?php endif; ?>
    <a href="<?php echo AdminUtils::aUrl('game/create'); ?>"><div class="addButton">Add new match</div></a>
    <br /><br />
    <span><?php echo _user()->hasFlash('failure') ? _user()->getFlash('failure') : ''; ?></span>

    <?php
    echo CHtml::form(AdminUtils::aUrl('game/index'), 'post');
    ?>
    <br /><br />
    <table>
        <thead>
            <tr>
                <th>Date/Time</th>
                <th>Home</th>
                <th>Score</th>
                <th>Away</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php
    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'name' => 'search[date]',
        'value' => $search['date'],
        'options' => array(
            'dateFormat' => 'yy-mm-dd',
            'defaultDate' => _app()->localtime->fromLocalDateTime(_app()->localtime->localNow, "Y-m-d"),
        ),
        'htmlOptions' => array('readonly' => true, 'submit' => ''),
    ));
    ?>
                </td>
                <td>
                    <?php echo CHtml::dropDownList('search[team_home]', $search['team_home'], $this->getTeams(), array('prompt' => '-', 'submit' => '')); ?>
                </td>
                <td></td>
                <td>
                    <?php echo CHtml::dropDownList('search[team_away]', $search['team_away'], $this->getTeams(), array('prompt' => '-', 'submit' => '')); ?>
                </td>
                <?php echo CHtml::endForm(); ?>
            </tr>
            <?php
            if ($m != null) :
                foreach ($m as $match) :
                    ?>
                    <tr>
                        <td><?php echo $match->date_played; ?></td>
                        <td><?php echo $match->teamHome0->name; ?></td>
                        <td><?php
            echo $match->score_home;
            echo '-';
            echo $match->score_away;
                    ?></td>
                        <td><?php echo $match->teamAway0->name; ?></td>
                        <?php if (!$match->points_calculated) : ?>
                            <td><a href="#" class="upload" id="<?php echo $match->id; ?>" matchid="<?php echo $match->xml_id; ?>"><img src="<?php echo AdminUtils::adminImageUrl('upload.png'); ?>" title="Upload XML"></a></td>
                            <td><a href="<?php echo AdminUtils::aUrl('game/stats', array('id' => $match->id)); ?>"><img src="<?php echo AdminUtils::adminImageUrl('match.png'); ?>" title="Enter match statistics"></a></td>
                            <td><a href="<?php echo AdminUtils::aUrl('game/editGame', array('id' => $match->id, 'edit' => true)); ?>"><img src="<?php echo AdminUtils::adminImageUrl('info.png'); ?>" title="Edit match details"></a></td>
                            <td><a class="confirm" href="<?php echo AdminUtils::aUrl('game/delete', array('id' => $match->id)); ?>" inf="<?php echo $match->teamHome0->name . '-' . $match->teamAway0->name; ?>" elem="the events for match"><img src="<?php echo AdminUtils::adminImageUrl('clear.png'); ?>" title="Clear stats"></a></td>
                            <td><a class="confirm" href="<?php echo AdminUtils::aUrl('game/delete', array('id' => $match->id, 'deleteMatch' => true)); ?>" inf="<?php echo $match->teamHome0->name . '-' . $match->teamAway0->name; ?>" elem="the match"><img src="<?php echo AdminUtils::adminImageUrl('delete.png'); ?>" title="Delete match"></a></td>
                        <?php else : ?>
                            <td><img src="<?php echo AdminUtils::adminImageUrl('upload_inactive.png'); ?>" title="This action can only be performed by the administrator"></td>
                            <td><img src="<?php echo AdminUtils::adminImageUrl('match_inactive.png'); ?>" title="This action can only be performed by the administrator"></td>
                            <td><img src="<?php echo AdminUtils::adminImageUrl('info_inactive.png'); ?>" title="This action can only be performed by the administrator"></td>
                            <td><img src="<?php echo AdminUtils::adminImageUrl('clear_inactive.png'); ?>" title="This action can only be performed by the administrator"></td>
                            <td><img src="<?php echo AdminUtils::adminImageUrl('delete_inactive.png'); ?>" title="This action can only be performed by the administrator"></td>
                        <?php endif; ?>
                        <?php Utils::triggerEvent('onAdminCommentIcon', $this, array('match_id' => $match->id)); ?>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
</div>

<div id="dialog"></div>

<div id="dialog"></div>

<div id="uploadPop"></div>