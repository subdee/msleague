<?php
echo $game->teamHome0->name;
echo ': ';
echo $game->score_home;?>
<table>
    <thead>
        <th>Player</th>
        <?php foreach ($events as $event) : ?>
            <th><?php echo $event->name; ?></th>
        <?php endforeach; ?>
    </thead>
    <tbody>
        <? foreach ($home as $hPlayer) : ?>
            <tr>
                <td><?php echo $hPlayer['name']; ?></td>
                <? foreach ($events as $event) : ?>
                    <?php if (isset($hPlayer['event'][$this->sanitizeText($event->name)])) : ?>
                    <td><?php echo $hPlayer['event'][$this->sanitizeText($event->name)]; ?></td>
                    <?php $homeTotals[$event->name] += $hPlayer['event'][$this->sanitizeText($event->name)]; ?>
                    <?php else : ?>
                    <td>0</td>
                    <?php endif; ?>
                <? endforeach; ?>
            </tr>
        <? endforeach; ?>
        <tr class="totals">
            <td>Totals</td>
            <? foreach ($events as $event) : ?>
                <td><?php echo $event->name == 'Minutes Played'  ? '' : $homeTotals[$event->name]; ?></td>
            <? endforeach; ?>
        </tr>
    </tbody>
</table>
<?php
echo $game->teamAway0->name;
echo ': ';
echo $game->score_away;?>
<table>
    <thead>
        <th>Player</th>
        <?php foreach ($events as $event) : ?>
            <th><?php echo $event->name; ?></th>
        <?php endforeach; ?>
    </thead>
    <tbody>
        <? foreach ($away as $aPlayer) : ?>
            <tr>
                <td><?php echo $aPlayer['name']; ?></td>
                <? foreach ($events as $event) : ?>
                    <?php if (isset($aPlayer['event'][$this->sanitizeText($event->name)])) : ?>
                    <td><?php echo $aPlayer['event'][$this->sanitizeText($event->name)]; ?></td>
                    <?php $awayTotals[$event->name] += $aPlayer['event'][$this->sanitizeText($event->name)]; ?>
                    <?php else : ?>
                    <td>0</td>
                    <?php endif; ?>
                <? endforeach; ?>
            </tr>
        <? endforeach; ?>
        <tr class="totals">
            <td>Totals</td>
            <? foreach ($events as $event) : ?>
                <td><?php echo $event->name == 'Minutes Played'  ? '' : $awayTotals[$event->name]; ?></td>
            <? endforeach; ?>
        </tr>
    </tbody>
</table>
<?php 
echo CHtml::form(AdminUtils::aUrl('game/stats'),'get');
echo CHtml::hiddenField('id',$game->id);
echo CHtml::button('Matches',array('onclick'=>'javascript: window.location = "'.AdminUtils::aUrl('game/index').'"'));
echo CHtml::submitButton('Edit');
echo CHtml::endForm();?>
<div class="pointsForm">
<?php echo CHtml::form('#');
echo CHtml::hiddenField('id',$game->id);
if ($this->isScriptLocked()) :
    echo CHtml::submitButton('Calculate Points',array('class'=>'points', 'disabled' => 'disabled', 'title' => 'You cannot calculate points because another user is calculating at the same time.  Please try again later.'));
else :
    echo CHtml::submitButton('Calculate Points',array('class'=>'points'));
endif;
echo CHtml::endForm();
?>
</div>
<div id="dialogPlayers"><p>Now calculating and saving players' points and VMI, and managers' points and ranks. Please don't interrupt this process.</p><img src="<?php echo AdminUtils::adminImageUrl('loading.gif'); ?>"></div>
<div id="dialogLocked"><p>There is currently another user calculating points or the points for this game have already been calculated. Please try again later.</p></div>