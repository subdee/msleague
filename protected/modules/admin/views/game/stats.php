<?php
echo CHtml::form(AdminUtils::aUrl('game/preview'),'post');
$i = 1;
$j = 1;
?>
<input type="hidden" value="<?php echo $game->id; ?>" name="game">
<div class="stables"><span><?php echo _user()->hasFlash('failure') ? _user()->getFlash('failure') : ''; ?></span></div>
<div class="home stats">
    <b>Home Team: <?php echo $homeTeam->name; ?>&nbsp;</b>
<input type="hidden" value="<?php echo $homeTeam->id; ?>" name="homeTeam">
<input type="text" size="2" value="<?php echo isset($game->score_home) ? $game->score_home : ''; ?>" name="homeScore"><br>
<br>
<table>
    <thead>
    <tr>
        <th class="largerow">Player</th>
        <?php foreach ($events as $event) : ?>
        <th><?php echo $event->name; ?></th>
        <?php endforeach; ?>
        <th>Remove</th>
    </tr>
    </thead>
    <tbody>
    <?php if ($home != null) : ?>
        <? foreach ($home as $hPlayer) : ?>
                <tr id="hrow<?php echo $i; ?>">
                <td><select name="HomePlayer[<?php echo $i; ?>][id]" readonly>
                        <option selected value="<?php echo $hPlayer['id']; ?>"><?php echo Player::model()->findByPk($hPlayer['id'])->shortname; ?></option>
                </select></td>
                    <? foreach ($events as $event) : ?>
                        <?php if (isset($hPlayer['event'][$this->sanitizeText($event->name)])) : 
                                $value = $hPlayer['event'][$this->sanitizeText($event->name)];
                            else :
                                $value = '';?>
                        <?php endif; ?>
                    <td><input type="text" size="2" name="HomePlayer[<?php echo $i; ?>][event][<?php echo $this->sanitizeText($event->name); ?>]" value="<?php echo $value; ?>"></td>
                    <? endforeach; ?>
                    <td><a href="#" class="delete" pl="<?php echo $hPlayer['id'] ?>" row="hrow<?php echo $i; ?>" plname="<?php echo Player::model()->findByPk($hPlayer['id'])->shortname; ?>"><img src="<?php echo AdminUtils::adminImageUrl('remove.png'); ?>" /></a></td>
                </tr>
        <? ++$i; endforeach; ?>
    <?php endif; ?>
    </tbody>
</table>
<a href="#" onclick="addNewHomeEvent(); return false;"><img src="<?php echo AdminUtils::adminImageUrl('add.png'); ?>"></a>
</div>
<div class="away stats">
    <b>Away Team: <?php echo $awayTeam->name; ?>&nbsp;</b>
<input type="hidden" value="<?php echo $awayTeam->id; ?>" name="awayTeam">
<input type="text" size="2" value="<?php echo isset($game->score_away) ? $game->score_away : ''; ?>" name="awayScore"><br>
<br>
<table>
    <thead>
    <tr>
        <th class="largerow">Player</th>
        <?php foreach ($events as $event) : ?>
        <th><?php echo $event->name; ?></th>
        <?php endforeach; ?>
        <th>Remove</th>
    </tr>
    </thead>
    <tbody>
    <?php if ($away != null) : ?>
        <? foreach ($away as $aPlayer) : ?>
                <tr id="arow<?php echo $j; ?>">
                <td><select name="AwayPlayer[<?php echo $j; ?>][id]" readonly>
                    <option selected value="<?php echo $aPlayer['id']; ?>"><?php echo Player::model()->findByPk($aPlayer['id'])->shortname; ?></option>
                </select></td>
                    <? foreach ($events as $event) : ?>
                        <?php if (isset($aPlayer['event'][$this->sanitizeText($event->name)])) : 
                                $value = $aPlayer['event'][$this->sanitizeText($event->name)];
                            else :
                                $value = '';?>
                        <?php endif; ?>
                    <td><input type="text" size="2" name="AwayPlayer[<?php echo $j; ?>][event][<?php echo $this->sanitizeText($event->name); ?>]" value="<?php echo $value; ?>"></td>
                    <? endforeach; ?>
                    <td><a href="#" class="delete" pl="<?php echo $aPlayer['id'] ?>" row="arow<?php echo $j; ?>" plname="<?php Player::model()->findByPk($aPlayer['id'])->shortname; ?>"><img src="<?php echo AdminUtils::adminImageUrl('remove.png'); ?>"/></a></td>
                </tr>
        <? ++$j; endforeach; ?>
    <?php endif; ?>
    </tbody>
</table>
<a href="#" onclick="addNewAwayEvent(); return false;"><img src="<?php echo AdminUtils::adminImageUrl('add.png'); ?>"></a>
</div>
<input type="hidden" id="id" value="<?php echo $i; ?>">
<input type="hidden" id="jd" value="<?php echo $j; ?>">
<?php echo CHtml::submitButton('Save'); ?>
<?php echo CHtml::link('Cancel',AdminUtils::aUrl('game/index')); ?>
<?php echo CHtml::endForm(); ?>

<span class="right"><?php echo CHtml::link('Match has been cancelled',AdminUtils::aUrl('game/cancel',array('id'=>$game->id)),array('class' => 'confirmcancel')); ?></span>

<div id="dialog"></div>
<script type="text/javascript">
function addNewHomeEvent()
{
    var id = parseInt($("#id").val());

	$(".home table").append("<tr id=\"hrow"+id+"\"><td><select name='HomePlayer["+id+"][id]'><?php echo $homePlayers; ?></select></td><?php echo $hInputs; ?><td><a href=\"#\" onclick=\"javascript:removeEvent('hrow"+id+"');\"><img src=\"<?php echo AdminUtils::adminImageUrl('remove.png'); ?>\"</a></td></tr>");

    $("#id").val(id+1);
}
function addNewAwayEvent()
{
    var jd = parseInt($("#jd").val());

	$(".away table").append("<tr id=\"arow"+jd+"\"><td><select name='AwayPlayer["+jd+"][id]'><?php echo $awayPlayers; ?></select></td><?php echo $aInputs; ?><td><a href=\"#\" onclick=\"javascript:removeEvent('arow"+jd+"');\"><img src=\"<?php echo AdminUtils::adminImageUrl('remove.png'); ?>\"</a></td></tr>");

	$("#jd").val(jd+1);
}
function removeEvent(obj)
{
    $("#"+obj).remove();
}
$(document).ready(function(){
    $(".delete").click(function(){
        var id = $(this).attr("pl");
        var row = $(this).attr("row");
        var game = "<?php echo $game->id; ?>";
        var name = $(this).attr("plname");
        $("#dialog").html("Are you sure you want to delete "+name+" from the game?");
        $("#dialog").dialog({
            autoOpen: true,
            modal: true,
            title: "Delete this player?",
            buttons : {
                "Delete" : function() {
                    $.ajax({
                        url: "<?php echo AdminUtils::aUrl('game/deletePlayer'); ?>",
                        type: "POST",
                        data: { 'id' : id, 'game' : game },
                        success: function(data)
                        {
                            if (data)
                                $("#"+row).remove();
                        }
                    });
                    $(this).dialog("close");
                },
                "Cancel" : function() {
                    $(this).dialog("close");
                }
            }
        }).dialog("open");
        return false;
    });
});
</script>