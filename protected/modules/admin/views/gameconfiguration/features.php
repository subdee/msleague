<div class="stables">
    <span class="success"><?php echo _user()->hasFlash('success') ? _user()->getFlash('success') : ''; ?></span>
    <span><?php echo _user()->hasFlash('failure') ? _user()->getFlash('failure') : ''; ?></span>
</div>
<div class="features">
    <div>
        <div><?php echo 'Season'; ?></div>
        <div>
        <?php
        $img = $seasonClass ? 'light_on.png' : 'light_off.png';
        echo CHtml::ajaxLink(
                CHtml::tag('span',array('id' => 'is_season_open'), CHtml::image(AdminUtils::adminImageUrl($img),null,array('title' => $seasonClass ? 'Turn off' : 'Turn on'))),
                AdminUtils::aUrl('gameconfiguration/switchFeature', array('action' => 'is_season_open')),
                array('update' => '#is_season_open')
            ); 
        ?>
        </div>
    </div>
    <div>
        <div><?php echo 'Pitch/Stockmarket'; ?></div>
        <div>
        <?php
        $img = $pitchClass ? 'light_on.png' : 'light_off.png';
        echo CHtml::ajaxLink(
                CHtml::tag('span',array('id' => 'is_enabled'), CHtml::image(AdminUtils::adminImageUrl($img),null,array('title' => $pitchClass ? 'Turn off' : 'Turn on'))),
                AdminUtils::aUrl('gameconfiguration/switchFeature', array('action' => 'is_enabled')),
                array('update' => '#is_enabled')
            ); 
        ?>
        </div>
    </div>
    <div>
        <div><?php echo 'Registration'; ?></div>
        <div>
        <?php
        $img = $registrationClass ? 'light_on.png' : 'light_off.png';
        echo CHtml::ajaxLink(
                CHtml::tag('span',array('id' => 'is_registration_open'), CHtml::image(AdminUtils::adminImageUrl($img),null,array('title' => $registrationClass ? 'Turn off' : 'Turn on'))),
                AdminUtils::aUrl('gameconfiguration/switchFeature', array('action' => 'is_registration_open')),
                array('update' => '#is_registration_open')
            ); 
        ?>
        </div>
    </div>
</div>
<div class="featuresActions">
    <p><?php echo CHtml::link('Add fan leagues', AdminUtils::aUrl('gameconfiguration/addFanLeagues')); ?></p>
    <p><?php echo CHtml::link('Attach league discussions', AdminUtils::aUrl('gameconfiguration/addLeagueDiscussions')); ?></p>
    <p><?php echo CHtml::link('Attach match discussions', AdminUtils::aUrl('gameconfiguration/addMatchDiscussions')); ?></p>
    <p><?php echo CHtml::link('Create photo thumbnails', AdminUtils::aUrl('gameconfiguration/createPhotoThumbnails')); ?></p>
</div>
