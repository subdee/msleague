<div class="stables">
    <span class="success"><?php echo _user()->hasFlash('success') ? _user()->getFlash('success') : ''; ?></span>
    <span><?php echo _user()->hasFlash('failure') ? _user()->getFlash('failure') : ''; ?></span>
</div>
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'gameconfiguration-form',
        'enableAjaxValidation' => false, 'enableClientValidation' => true,
        ));
    ?>

    <div class="row">
        <?php echo $form->labelEx($gc, 'game_name'); ?>
        <?php echo $form->textField($gc, 'game_name'); ?>
        <?php echo $form->error($gc, 'game_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($gc, 'site_url'); ?>
        <?php echo $form->textField($gc, 'site_url'); ?>
        <?php echo $form->error($gc, 'site_url'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($gc, 'support_email'); ?>
        <?php echo $form->textField($gc, 'support_email'); ?>
        <?php echo $form->error($gc, 'support_email'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($gc, 'noreply_email'); ?>
        <?php echo $form->textField($gc, 'noreply_email'); ?>
        <?php echo $form->error($gc, 'noreply_email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($gc, 'timezone_id'); ?>
        <?php echo $form->dropDownList($gc, 'timezone_id', CHtml::listData($timezones, 'id', 'name')); ?>
        <?php echo $form->error($gc, 'timezone_id'); ?>
    </div>

    <div class="row buttons">
    <?php echo CHtml::submitButton('Save', array('id' => 'submitBtn')); ?>
    </div>

<?php $this->endWidget(); ?>
</div>
<div class="reset-section">
    <?php echo CHtml::link('Reset managers', AdminUtils::aUrl('gameconfiguration/resetManagers'), array('id' => 'reset-managers')) ?>
    <br /><br />
        Select a  file to download:
        <?php echo CHtml::dropDownList('file', null, $files, array('prompt' => 'Select...', 'onchange' => 'js:window.location.href = $(this).val();')); ?>
</div>
<div id="dialog">These changes could affect the functionality of the game.<br />Are you sure you want to save the changes?</div>
<div id="reset-dialog">By pressing this button you will reset every manager's points and money.<br /> Are you absolutely sure you want to do this?</div>