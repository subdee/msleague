
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'gameparameters-form',
        'enableAjaxValidation' => false, 'enableClientValidation' => true,
        ));
    ?>

    <?php foreach ($gc as $att => $value) : ?>
        <?php if (!in_array($att, $noDisplay)) : ?>
            <? if ($att == 'newsletter_footer') : ?>
                <div class="row richedit">
                    <?php echo $form->labelEx($gc, $att); ?>
                    <?php
                    $this->widget('ext.editMe.ExtEditMe', array(
                        'model' => $gc,
                        'attribute' => 'newsletter_footer',
                        'htmlOptions' => array('option' => 'value'),
                        'toolbar' => array(
                            array('Undo', '-', 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'),
                            array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'BidiLtr', 'BidiRtl'),
                            array('Link', 'Unlink', 'Anchor'),
                            array('Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'),
                            '/',
                            array('Styles', 'Format', 'Font', 'FontSize'),
                            array('TextColor', 'BGColor'),
                        )
                    ));
                    ?>
                    <?php echo $form->error($gc, $att); ?>
                </div>
            <?php elseif ($att == 'disallowed_usernames') : ?>
                <div class="row">
                    <?php echo $form->labelEx($gc, $att); ?>
                    <?php echo $form->textArea($gc, $att, array('cols' => 70, 'rows' => 10)); ?>
                    <?php echo $form->error($gc, $att); ?>
                </div>
            <?php else : ?>
                <div class="row">
                    <?php echo $form->labelEx($gc, $att); ?>
                    <?php echo $form->textField($gc, $att); ?>
                    <?php echo $form->error($gc, $att); ?>
                </div>
            <? endif; ?>
        <? endif; ?>
    <?php endforeach; ?>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Save', array('id' => 'submitBtn')); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>
<div id="dialog">These changes could affect the functionality of the game.<br />Are you sure you want to save the changes?</div>