<div class="stables">
<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gameconfiguration-form',
	'enableAjaxValidation'=>false,'enableClientValidation'=>true,
    )); ?>

    <table>
        <thead>
            <tr>
                <th></th>
                <?php foreach ($pos as $p) { ?>
                <th><?php echo $p->name; ?></th>
                <?php } ?>
            </tr>
        </thead>
        <?php foreach ($ev as $e) { ?>
        <tr>
            <td><?php echo $e->name; ?></td>
            <?php foreach ($i[$e->name] as $pos=>$value) { ?>
            <td><input type="text" name="pe[<?php echo $pos; ?>][<?php echo $e->name; ?>]" value="<?php echo $value; ?>" size="1"></td>
            <?php } ?>
        </tr>
        <?php  } ?>
    </table>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Save',array('clientChange'=>'submit','confirm'=>"This change could affect the functionality of the game.\nAre you sure you want to save the changes?")); ?>
    </div>

<?php $this->endWidget(); ?>
</div>
</div>