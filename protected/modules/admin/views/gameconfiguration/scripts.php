<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
            'id' => 'scriptconfiguration-form',
            'enableAjaxValidation' => false, 'enableClientValidation' => true,
        ));
    ?>

    <div class="row">
        <?php echo $form->labelEx($sc, 'main_script_interval'); ?>
        <?php echo $form->textField($sc, 'main_script_interval'); ?>
        <?php echo $form->error($sc, 'main_script_interval'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($sc, 'daily_script_time'); ?>
        <?php
        $this->widget('application.extensions.jui_timepicker.JTimePicker', array(
            'model' => $sc,
            'attribute' => 'daily_script_time',
//            'themeUrl' => _bu('/css/jui/'),
            'options' => array(
                'showLeadingZero' => false,
                'showPeriodLabels' => false,
                'minutes' => array(
                    'interval' => 15
                )
            ),
            'htmlOptions' => array('readonly' => true)
        ));
        ?>
        <?php echo $form->error($sc, 'daily_script_time'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($sc, 'newsletter_time'); ?>
        <?php
        $this->widget('application.extensions.jui_timepicker.JTimePicker', array(
            'model' => $sc,
            'attribute' => 'newsletter_time',
//            'themeUrl' => _bu('/css/jui/'),
            'options' => array(
                'showLeadingZero' => false,
                'showPeriodLabels' => false,
                'minutes' => array(
                    'interval' => 15
                )
            ),
            'htmlOptions' => array('readonly' => true)
        ));
        ?>
        <?php echo $form->error($sc, 'newsletter_time'); ?>
    </div>

    <div class="row_separator"></div>

    <div class="row">
        <?php echo $form->labelEx($sc, 'formula_coefficient'); ?>
        <?php echo $form->textField($sc, 'formula_coefficient'); ?>
        <?php echo $form->error($sc, 'formula_coefficient'); ?>
    </div>

    <div class="row_separator"></div>

    <div class="row nofloat">
        Active managers are considered the managers that have logged in the past
        <?php echo CHtml::textField('ScriptConfiguration[active1]', $active[0], array('size' => '1', 'maxlength' => '2')); ?>
        days and have made at least
        <?php echo CHtml::textField('ScriptConfiguration[active2]', $active[1], array('size' => '1', 'maxlength' => '2')); ?>
        transactions.
    </div>

    <div class="row_separator"></div>

    <div class="row nofloat">
        Burned managers are considered the managers that have a daily average of:<br/>
        <ul>
            <li>at least
                <?php echo CHtml::textField('ScriptConfiguration[burned1]', $burned[0], array('size' => '1', 'maxlength' => '2')); ?>
                transactions</li>
            <li>at least
                <?php echo CHtml::textField('ScriptConfiguration[burned2]', $burned[1], array('size' => '1', 'maxlength' => '2')); ?>
                comments</li>
        </ul>
    </div>

    <div class="row_separator"></div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Save', array('id' => 'submitBtn')); ?>
        <?php echo CHtml::link('Cancel',AdminUtils::aUrl('gameconfiguration/index')); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>
<br />
SCRIPT LOG (today/yesterday):
<br /><br/>
<div class="scriptLog">
    <?php echo $scriptLog; ?>
    <?php if ($fileError) : ?>
        <span class="error"><?php echo $fileError; ?><br/>Make sure that the script has run for that day, the location of the file is setup correctly or that the correct permissions have been set!</span>
    <?php endif; ?>
</div>
<div id="dialog">These changes could affect the functionality of the game.<br />Are you sure you want to save the changes?</div>