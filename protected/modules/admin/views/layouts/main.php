<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <script type="text/javascript">
            /*<![CDATA[*/
            var MSL = {
                baseUrl : '<?php echo _bu(); ?>',
                themeUrl : '<?php echo _tbu(); ?>',
                controllerUrl : '<?php echo _url(_controller_id()); ?>',
                actionUrl : '<?php echo _url(_app()->controller->route); ?>',
                adminUrl : '<?php echo _bu('/admin/'); ?>',
                controllerId : '<?php echo _controller_id(); ?>',
                actionId : '<?php echo _action_id(); ?>',
                modules : {},
                config : {
                    commissionPerc : <?php echo Gameconfiguration::model()->getCommissionFactor(); ?>
                }
            }
            /*]]>*/
        </script>

        <?php
            AdminUtils::registerAdminCssFile('screen');
            AdminUtils::registerAdminCssFile('admin-main');
            AdminUtils::registerAdminCssFile('admin-form');
            AdminUtils::registerAdminCssFile('admin-gridview', 'screen');
        ?>

        <link rel="shortcut icon" href="<?php echo _bu('favicon.ico'); ?>" />

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    </head>

    <body>

        <div class="container" id="page">

            <?php if (!Yii::app()->user->isGuest) : ?>
                <div class="help-icon"><a href="<?php echo Yii::app()->request->baseUrl; ?>/guide.pdf"><img src="<?php echo AdminUtils::adminImageUrl('help.png'); ?>" /></a></div>
            <?php endif; ?>

            <div id="header">
                <div id="logo"><img src="<?php echo AdminUtils::adminImageUrl('logo.png'); ?>" /></div>
                <div id="time"><?php echo '<strong>' . Utils::date(_app()->localtime->getLocalNow('Y-m-d H:i:s')) . '</strong> ' . _app()->localtime->getTimezone(); ?></div>
            </div><!-- header -->

            <div id="mainmenu">
                <?php
                $this->widget('application.extensions.mbmenu.MbMenu', array(
                    'items' => array(
                        array('label' => 'Home', 'url' => array('/admin/main/index'), 'visible' => !_user()->isGuest),
                        array('label' => 'Configuration', 'url' => array('/admin/gameconfiguration/index'), 'visible' => User::hasRole(AdminRole::ADMIN), 'active' => _app()->controller->id == 'gameconfiguration' || _app()->controller->id == 'user',
                            'items' => array(
                                array('label' => 'Game Configuration', 'url' => false,
                                    'items' => array(
                                        array('label' => 'Game Settings', 'url' => array('/admin/gameconfiguration/index')),
                                        array('label' => 'Game Parameters', 'url' => array('/admin/gameconfiguration/parameters')),
                                        array('label' => 'Point System', 'url' => array('/admin/gameconfiguration/pointSystem')),
                                        array('label' => 'Features', 'url' => array('/admin/gameconfiguration/features')),
                                        array('label' => 'Script Configuration', 'url' => array('/admin/gameconfiguration/scripts')),
                                )),
                                array('label' => 'Admin Users', 'url' => array('/admin/user/index')),
                        )),
                        array('label' => 'Game Management', 'url' => array('/admin/game/index'), 'visible' => User::hasRole(AdminRole::ADMIN | AdminRole::OPERATOR), 'active' => _app()->controller->id == 'team' || _app()->controller->id == 'player',
                            'items' => array(
                                array('label' => 'Teams', 'url' => array('/admin/team/index')),
                                array('label' => 'Players', 'url' => array('/admin/player/index'),
                                    'items' => array(
                                        array('label' => 'Unlisted', 'url' => array('/admin/player/index')),
                                        array('label' => 'Listed', 'url' => array('/admin/player/listed')),
                                        array('label' => 'Delisted', 'url' => array('/admin/player/delisted')),
                                        array('label' => 'Injured', 'url' => array('/admin/player/injured')),
                                        array('label' => 'Suspended', 'url' => array('/admin/player/suspended')),
                                )),
                                array('label' => 'Matches', 'url' => array('/admin/game/index')),
                                array('label' => 'Matches Admin', 'url' => array('/admin/game/admin'), 'visible' => User::hasRole(AdminRole::ADMIN)),
                                array('label' => 'Leagues', 'url' => array('/admin/customLeague/index'), 'items' => array(
                                        array('label' => 'User leagues', 'url' => array('/admin/customLeague/index')),
                                        array('label' => 'Fan leagues', 'url' => array('/admin/customLeague/fan')),
                                        array('label' => 'Add league', 'url' => array('/admin/customLeague/add')),
                                        array('label' => 'Config', 'url' => array('/admin/customLeague/config')),
                                )),
                        )),
                        array('label' => 'Site Content', 'url' => array('/admin/announcement/index'), 'visible' => User::hasRole(AdminRole::ADMIN | AdminRole::OPERATOR), 'active' => _app()->controller->id == 'announcement' || _app()->controller->id == 'cms' || _app()->controller->id == 'theme',
                            'items' => array(
                                array('label' => 'Announcements', 'url' => array('/admin/announcement/index')),
                                array('label' => 'Static Content', 'url' => array('/admin/cms/index')),
                                array('label' => 'Tips', 'url' => array('/admin/cms/tips')),
                                array('label' => 'Theme', 'url' => array('/admin/theme/index')),
                                array('label' => 'Report Reasons', 'url' => array('/admin/cms/reportReasons')),
//                                array('label' => 'Slideshow', 'url' => array('/admin/cms/slideshow')),
                        )),
                        array('label' => 'Managers', 'url' => false, 'visible' => _user()->id != null, 'active' => _app()->controller->id == 'manager' || _app()->controller->id == 'report',
                            'items' => array(
                                array('label' => 'Managers', 'url' => array('/admin/manager/index')),
                                array('label' => 'Banned', 'url' => array('/admin/manager/banned')),
                                array('label' => 'Deleted', 'url' => array('/admin/manager/deleted')),
                                array('label' => 'Temporary', 'url' => array('/admin/manager/temps')),
                                array('label' => 'Inactive', 'url' => array('/admin/manager/inactive')),
                                array('label' => 'Reset', 'url' => array('/admin/manager/reset')),
                                array('label' => 'Reports', 'url' => array('/admin/report/index')),
                                array('label' => 'Photo Approvals', 'url' => array('/admin/manager/photos')),
                                array('label' => 'Comments', 'url' => array('/admin/manager/comments')),
                                array('label' => 'Messages', 'url' => array('/admin/manager/threads')),
                        )),
                        array('label' => 'Monitoring', 'url' => false, 'visible' => User::hasRole(AdminRole::ADMIN | AdminRole::OPERATOR), 'active' => _app()->controller->id == 'monitor',
                            'items' => array(
                                array('label' => 'Stock Info', 'url' => array('/admin/monitor/index')),
                                array('label' => 'Portfolios', 'url' => array('/admin/monitor/portfolios')),
                                array('label' => 'Transactions', 'url' => array('/admin/monitor/transactions')),
                        )),
                        array('label' => 'Newsletters', 'url' => false, 'visible' => User::hasRole(AdminRole::ADMIN | AdminRole::OPERATOR), 'active' => _app()->controller->id == 'newsletter',
                            'items' => array(
                                array('label' => 'Newsletters (custom)', 'url' => array('/admin/newsletter/index')),
                                array('label' => 'Reminders (automatic)', 'url' => array('/admin/newsletter/reminders')),
                        )),
                        array('label' => 'Forum', 'url' => array('/admin/forum/index'), 'visible' => !_user()->isGuest && _isModuleOn('forum'), 'active' => _app()->controller->id == 'forum',
                            'items' => array(
                                array('label' => 'Forum', 'url' => array('/admin/forum/index')),
                                array('label' => 'Matches', 'url' => array('/admin/forum/matches')),
                                array('label' => 'Leagues', 'url' => array('/admin/forum/leagues')),
                                array('label' => 'Fan Leagues', 'url' => array('/admin/forum/fanLeagues')),
                        )),
                        array('label' => 'Login', 'url' => array('/admin/main/login'), 'visible' => Yii::app()->user->isGuest),
                        array('label' => 'Logout (' . Yii::app()->user->name . ')', 'url' => array('/admin/main/logout'), 'visible' => !_user()->isGuest)
                    )
                ));
                ?>
            </div><!-- mainmenu -->

            <?php echo $content; ?>

            <div id="footer">
                Copyright &copy; <?php echo date('Y'); ?> by MSLeague.<br/>
                All Rights Reserved.<br/>
            </div><!-- footer -->

        </div><!-- page -->

    </body>
</html>
