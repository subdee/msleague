<?php $this->pageTitle = Yii::app()->name; ?>
<div class="indexDiv">
    <div class="users">
        <div>
            <a href="<?php echo AdminUtils::aUrl('manager/index'); ?>">Registered:</a>
            <span><?php echo $data['registered']; ?></span>
        </div>
        <div>
            <a href="<?php echo AdminUtils::aUrl('manager/online'); ?>">Online managers:</a>
            <span><?php echo $data['logged_in']; ?></span>
        </div>
        <div>
            <a href="<?php echo AdminUtils::aUrl('manager/index'); ?>">Managers:</a>
            <span><?php echo $data['managers']; ?></span>
        </div>
        <div>
            <a href="<?php echo AdminUtils::aUrl('manager/temps'); ?>">Temporary managers:</a>
            <span><?php echo $data['temporary']; ?></span>
        </div>
        <div>
            <a href="<?php echo AdminUtils::aUrl('manager/inactive'); ?>">Inactive managers:</a>
            <span><?php echo $data['inactive']; ?></span>
        </div>
        <div>
            <a href="<?php echo AdminUtils::aUrl('manager/banned'); ?>">Banned managers:</a>
            <span><?php echo $data['banned']; ?></span>
        </div>
        <div>
            <a href="<?php echo AdminUtils::aUrl('report/index'); ?>">Open reports:</a>
            <span <?php if ($data['reports'] > 0) : ?>class="attention"<?php endif; ?>><?php echo $data['reports']; ?></span>
        </div>
        <div>
            <a href="<?php echo AdminUtils::aUrl('manager/photos'); ?>">Photos pending approval:</a>
            <span <?php if ($data['photos'] > 0) : ?>class="attention"<?php endif; ?>><?php echo $data['photos']; ?></span>
        </div>
    </div>
    <div class="game">
        <div class="period">
            <a href="<?php echo AdminUtils::aUrl('main/index', array('interval' => 'DAY')); ?>" <?php if ($interval == 'DAY') : ?>class="bold"<?php endif; ?>>24 H</a>
            |
            <a href="<?php echo AdminUtils::aUrl('main/index', array('interval' => 'WEEK')); ?>"<?php if ($interval == 'WEEK') : ?>class="bold"<?php endif; ?>>7 D</a>
            |
            <a href="<?php echo AdminUtils::aUrl('main/index', array('interval' => 'MONTH')); ?>"<?php if ($interval == 'MONTH') : ?>class="bold"<?php endif; ?>>30 D</a>
            |
            <a href="<?php echo AdminUtils::aUrl('main/index', array('interval' => 'YEAR')); ?>"<?php if ($interval == 'YEAR') : ?>class="bold"<?php endif; ?>>Season</a>
        </div>
        <div>
            Logged in:
            <span><?php echo $data['logged']; ?></span>
        </div>
        <div>
            <a href="<?php echo User::hasRole(AdminRole::ADMIN | AdminRole::OPERATOR) ? AdminUtils::aUrl('monitor/transactions') : '#'; ?>">Transactions:</a>
            <span><?php echo $data['transactions']; ?></span>
        </div>
        <?php if (_isModuleOn('forum')) : ?>
            <div>
                Comments:
                <span><?php echo $data['comments']; ?></span>
            </div>
        <?php endif; ?>
        <?php if (_isModuleOn('message')) : ?>
            <div>
                Messages:
                <span><?php echo $data['messages']; ?></span>
            </div>
        <?php endif; ?>
        <div>
            Registrations:
            <span><?php echo $data['new_users']; ?></span>
        </div>
        <div>
            Team creations:
            <span><?php echo $data['new_teams']; ?></span>
        </div>
        <div>
            <a href="<?php echo AdminUtils::aUrl('manager/deleted'); ?>">Deleted managers:</a>
            <span><?php echo $data['deleted_users']; ?></span>
        </div>
    </div>
</div>

<div>
    <div class="active_users">
        <div>
            Active managers:
            <span><?php echo $data['active']; ?></span>
            <?php if (!empty($active)) : ?>
                <div class="charts" id="active-chart">
                    <?php
                    $this->Widget('ext.highcharts.HighchartsWidget', array(
                        'options' => array(
                            'credits' => $credits,
                            'chart' => array(
                                'renderTo' => "active-chart",
                                'defaultSeriesType' => 'column'
                            ),
                            'title' => array('text' => 'Active users per month'),
                            'xAxis' => array(
                                'categories' => $active['month']
                            ),
                            'yAxis' => array(
                                'title' => array('text' => 'Users')
                            ),
                            'legend' => array(
                                'enabled' => false,
                            ),
                            'series' => array(
                                array('name' => 'Users', 'data' => $active['count'])
                            ),
                            'theme' => 'grid',
                            'plotOptions' => array(
                                'bar' => array(
                                    'dataLabels' => array(
                                        'enabled' => true
                                    )
                                )
                            )
                        )
                    ));
                    ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <div class="top_users">
        <div>
            Burned-out managers:
            <span><?php echo $data['top']; ?></span>
            <?php if (!empty($top)) : ?>
                <div class="charts" id="top-chart">
                    <?php
                    $this->Widget('ext.highcharts.HighchartsWidget', array(
                        'options' => array(
                            'credits' => $credits,
                            'chart' => array(
                                'renderTo' => "top-chart",
                                'defaultSeriesType' => 'column'
                            ),
                            'title' => array('text' => 'Top users per month'),
                            'xAxis' => array(
                                'categories' => $top['month']
                            ),
                            'yAxis' => array(
                                'title' => array('text' => 'Users')
                            ),
                            'legend' => array(
                                'enabled' => false,
                            ),
                            'series' => array(
                                array('name' => 'Users', 'data' => $top['count'])
                            ),
                            'theme' => 'grid',
                            'plotOptions' => array(
                                'bar' => array(
                                    'dataLabels' => array(
                                        'enabled' => true
                                    )
                                )
                            )
                        )
                    ));
                    ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="regChart">
    <?php
    $this->Widget('ext.highcharts.HighchartsWidget', array(
        'options' => array(
            'credits' => $credits,
            'title' => array('text' => 'Registrations Per Day'),
            'xAxis' => array(
                'categories' => $regPerDay['date']
            ),
            'yAxis' => array(
                'title' => array('text' => 'Registrations')
            ),
            'legend' => array(
                'enabled' => false,
            ),
            'series' => array(
                array('name' => 'Registrations', 'data' => $regPerDay['count'])
            ),
            'theme' => 'grid',
        )
    ));
    ?>
</div>

<div class="otherCharts">
    <div>
        <div>
            <?php
            $this->Widget('ext.highcharts.HighchartsWidget', array(
                'options' => array(
                    'credits' => $credits,
                    'chart' => array(
                        'defaultSeriesType' => 'bar'
                    ),
                    'title' => array('text' => 'Users per age group'),
                    'xAxis' => array(
                        'categories' => $age['group']
                    ),
                    'yAxis' => array(
                        'title' => array('text' => 'Users')
                    ),
                    'legend' => array(
                        'enabled' => false,
                    ),
                    'series' => array(
                        array('name' => 'Users', 'data' => $age['count'])
                    ),
                    'theme' => 'grid',
                    'plotOptions' => array(
                        'bar' => array(
                            'dataLabels' => array(
                                'enabled' => true
                            )
                        )
                    )
                )
            ));
            ?>
            <?php
            $this->Widget('ext.highcharts.HighchartsWidget', array(
                'options' => array(
                    'credits' => $credits,
                    'chart' => array(
                        'defaultSeriesType' => 'bar'
                    ),
                    'title' => array('text' => 'Users per gender'),
                    'xAxis' => array(
                        'categories' => $gender['gender']
                    ),
                    'yAxis' => array(
                        'title' => array('text' => 'Users')
                    ),
                    'legend' => array(
                        'enabled' => false,
                    ),
                    'series' => array(
                        array('name' => 'Users', 'data' => $gender['count'])
                    ),
                    'theme' => 'grid',
                    'plotOptions' => array(
                        'bar' => array(
                            'dataLabels' => array(
                                'enabled' => true
                            )
                        )
                    )
                )
            ));
            ?>
            <?php
            $this->Widget('ext.highcharts.HighchartsWidget', array(
                'options' => array(
                    'credits' => $credits,
                    'chart' => array(
                        'defaultSeriesType' => 'bar'
                    ),
                    'title' => array('text' => 'Users per country'),
                    'xAxis' => array(
                        'categories' => $country['country']
                    ),
                    'yAxis' => array(
                        'title' => array('text' => 'Users')
                    ),
                    'legend' => array(
                        'enabled' => false,
                    ),
                    'series' => array(
                        array('name' => 'Users', 'data' => $country['count'])
                    ),
                    'theme' => 'grid',
                    'plotOptions' => array(
                        'bar' => array(
                            'dataLabels' => array(
                                'enabled' => true
                            )
                        )
                    )
                )
            ));
            ?>
        </div>
    </div>
</div>
