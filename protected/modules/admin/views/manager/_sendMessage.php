<?php  echo CHtml::form(AdminUtils::aUrl('manager/sendMessage'),'get'); ?>

    <?php echo CHtml::hiddenField('id',$manager->id); ?>

    <div class="row">
        <?php echo CHtml::label('Content','content'); ?>
        <?php echo CHtml::textArea('content','',array('cols'=>'70','rows'=>'15')); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Send'); ?>
    </div>

<?php 
echo CHtml::endForm(); ?>
</div>