<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'banned',
    'dataProvider' => $m->search(),
    'filter' => $m,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),    'columns' => array(
        array(
            'header' => '',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
        ),
        array(
            'type' => 'raw',
            'name' => 'manager.username',
        ),
        array(
            'name' => 'date_banned',
            'value' => 'Utils::date($data->date_banned)',
            'filter' => false,
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'header' => 'Banned until',
            'value' => 'Utils::daysAdd($data->date_banned,$data->duration)',
            'filter' => false,
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'type'=>'raw',
            'header'=>'Ban Reason',
            'name' => 'statusChangeReason.reason',
            'filter' => false,
            'value' => 'CHtml::link($data->reportReason->reason,"#",array("title"=>$data->manager->notes))',
        ),
        array(
            'header' => '',
            'type' => 'raw',
            'value' => 'CHtml::link("Unban",AdminUtils::aUrl("manager/unban",array("id"=>$data->manager->id)))',
            'filter' => false,
        ),
    ),
));
?>
