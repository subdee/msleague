<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $comments->search(),
    'filter' => $comments,    'columns' => array(
        array(
            'header' => '',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
            'filter' => false,
        ),
        array(
            'name' => 'content',
        ),
        array(
            'name' => 'date_posted',
            'htmlOptions' => array('class' => 'tcenter'),
            'filter' => false,
        ),
        array(
            'type' => 'raw',
            'name' => 'commentor_id',
            'value' => '$data->commentor_id != null ? $data->commentor->username : "<em>(deleted user)</em>"',
            'htmlOptions' => array('class' => 'tcenter'),
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{hide}{show}',
            'buttons' => array(
                'hide' => array(
                    'label' => 'Hide comment',
                    'url' => 'AdminUtils::aUrl("manager/hideComment", array("id" => $data->id, "src" => false))',
                    'imageUrl' => AdminUtils::adminImageUrl('eye.png'),
                    'visible' => '$data->visible',
                ),
                'show' => array(
                    'label' => 'Show comment',
                    'url' => 'AdminUtils::aUrl("manager/showComment", array("id" => $data->id, "src" => false))',
                    'imageUrl' => AdminUtils::adminImageUrl('eye.png'),
                    'options' => array('style' => 'opacity: 0.5'),
                    'visible' => '!$data->visible',
                ),
            )
        ),
    ),
));
?>