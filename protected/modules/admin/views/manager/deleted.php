<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'deleted',
    'dataProvider' => $m->search(),
    'filter' => $m,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),    'columns' => array(
        array(
            'header' => '',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
        ),
        array(
            'type' => 'raw',
            'name' => 'username'
        ),
        array(
            'type' => 'raw',
            'header' => 'Delete reason',
            'name' => 'reportReason.reason',
            'filter' => false,
            'value' => '$data->reportReason != null ? CHtml::link($data->reportReason->reason,"#",array("title"=>$data->details)) : "(no report reason given)"',
        ),
        array(
            'name' => 'date',
            'value' => 'Utils::date($data->date)',
            'filter' => false
        ),
    ),
));
?>
