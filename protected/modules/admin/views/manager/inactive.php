<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'banned',
    'dataProvider' => $m->searchInactive(),
    'filter' => $m,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),    'columns' => array(
        array(
            'header' => '',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
        ),
        array(
            'type' => 'raw',
            'name' => 'username',
        ),
        array(
            'name' => 'registration_date',
            'value' => 'Utils::date($data->registration_date)',
            'filter' => false,
            'htmlOptions' => array('class' => 'tcenter')
        )
    ),
));
?>
