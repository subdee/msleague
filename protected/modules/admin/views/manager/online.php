<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'manager',
    'dataProvider' => $m->searchOnline(),
    'pager' => array('class' => 'CLinkPager', 'header' => ''),    'columns' => array(
        array(
            'header' => '',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
        ),
        array(
            'type' => 'raw',
            'name' => 'username',
            'value' => 'CHtml::link($data->username,"#",array("title"=>$data->notes))',
        ),
    ),
));
?>
