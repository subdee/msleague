
<br />
<p>All photos will be approved by default when you press Approve. Please uncheck any photos which you do not approve.</p>
<?php echo _user()->hasFlash('failure') ? _user()->getFlash('failure') : ''; ?>
<?php echo CHtml::form(AdminUtils::aUrl('manager/approve'),'post'); ?>
<table class="photo">
    <?php $i = 1; ?>
    <?php foreach ($photos as $photo) : ?>
    <?php if ($i % 4 == 0) : ?>
    <tr>
    <?php endif; ?>
        <td>
            <b><?php echo $photo->username; ?></b>
            <br />
            <img src="<?php echo Utils::managerPhotoUrl($photo->profile->photo); ?>">
            <br />
            <?php echo CHtml::radioButtonList('r['.$photo->id.']','1',array('1'=>'Approve','0'=>'Disapprove')); ?>
        </td>
    <?php if ($i % 4 == 3) : ?>
    </tr>
    <?php endif; ?>
    <?php ++$i; ?>
    <?php endforeach; ?>
</table>
<?php echo $i == 1 ? 'No photos pending approval' : CHtml::submitButton('Save'); ?>