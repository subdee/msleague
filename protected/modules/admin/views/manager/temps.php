<div class="stables"><span><?php echo _user()->hasFlash('failure') ? _user()->getFlash('failure') : ''; ?></span></div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'temp',
    'dataProvider' => $m->searchTemp(),
    'filter' => $m,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),    'columns' => array(
//        array(
//            'name'=>'username'
//        ),
        array(
            'type' => 'raw',
            'name' => 'username',
        ),
        array(
            'name' => 'registration_date',
            'value' => 'Utils::date($data->registration_date)',
            'filter' => false,
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'name' => 'activation_date',
            'value' => 'Utils::date($data->activation_date)',
            'filter' => false,
            'htmlOptions' => array('class' => 'tcenter')
        ),
    ),
));
?>
