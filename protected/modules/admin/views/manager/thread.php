<div>
    &laquo; Back to <?php echo _l('messages', AdminUtils::aUrl('manager/threads')); ?>
</div>
<?php
if ($thread->system) {
    echo CHtml::tag('span', array('class' => 'conversation-info'), _t('System message for {m}', array(
            '{m}' => $thread->recipient->username
        )));
} else {
    echo CHtml::tag('span', array('class' => 'conversation-info'), _t('Conversation of {m1} with {m2}', array(
            '{m1}' => $thread->sender->username,
            '{m2}' => $thread->recipient->username
        )));
}

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'comments-grid',
    'dataProvider' => $comments->search(),
    'htmlOptions' => array('class' => "grid-view comments"),
    'filter' => $comments,
    'columns' => array(
        array(
            'header' => '',
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + $row + 1',
            'filter' => false,
            'htmlOptions' => array('class' => 'first-column index')
        ),
        array(
            'type' => 'raw',
            'header' => 'Manager',
            'name' => 'comment.commentor_id',
            'value' => '$data->comment->commentor ? $data->comment->commentor->username : ' . $nullCommentor,
            'htmlOptions' => array('class' => 'commentor')
        ),
        array(
            'type' => 'raw',
            'name' => 'commentContent',
            'value' => '$data->comment->content',
            'htmlOptions' => array('class' => 'content-text comment'),
        ),
        array(
            'name' => 'date_posted',
            'value' => 'Utils::date($data->comment->date_posted, array("yearIfDifferent" => true))',
            'htmlOptions' => array('class' => 'datetime'),
            'filter' => false,
        ),
        array(
            'type' => 'raw',
            'header' => $thread->system ? _gameName() : "Status ({$thread->sender->cleanUsername})",
            'value' => '$data->deleted_by_sender ? CHtml::tag("span", array("class" => "deleted"), "deleted") : ""',
            'htmlOptions' => array('class' => 'datetime'),
            'visible' => !$thread->system,
        ),
        array(
            'type' => 'raw',
            'header' => "Status ({$thread->recipient->cleanUsername})",
            'value' => '$data->deleted_by_recipient ? CHtml::tag("span", array("class" => "deleted"), "deleted") : ""',
            'htmlOptions' => array('class' => 'datetime'),
        ),
    ),
));
?>