<?php

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $threads->search(),
    'filter' => $threads,
    'filterPosition' => 'header',
    'columns' => array(
        array(
            'header' => '',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
            'filter' => false,
        ),
        array(
            'type' => 'raw',
            'name' => 'sender_id',
            'value' => '$data->system ? CHtml::tag("span", array("class" => "system"), "system") : ($data->sender ? $data->sender->username : CHtml::tag("span", array("class" => "deleted"), "deleted"))',
            'cssClassExpression' => '$data->deleted_by_sender ? "deleted" : ""',
        ),
        array(
            'type' => 'raw',
            'header' => 'Recipient',
            'name' => 'recipient_id',
            'value' => '$data->recipient ? $data->recipient->username : CHtml::tag("span", array("class" => "deleted"), "deleted")',
            'cssClassExpression' => '$data->deleted_by_recipient ? "deleted" : ""',
        ),
//        array(
//            'type' => 'raw',
//            'header' => 'Latest comment',
//            'value' => 'Utils::substrFromStart($data->discussion->lastComment(true)->content, 30, "...")',
//            'filter' => false,
//        ),
        array(
            'header' => CHtml::image(AdminUtils::adminImageUrl('commentCount.png')),
            'name' => 'commentCount',
            'filter' => false,
            'value' => '$data->commentCount',
            'htmlOptions' => array('class' => 'tcenter'),
        ),
//        array(
//            'name' => 'dateUpdated',
//            'value' => '$data->discussion->date_updated',
//            'htmlOptions' => array('class' => 'datetime'),
//            'filter' => false,
//        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}',
            'buttons' => array(
                'view' => array(
                    'label' => 'View',
                    'url' => 'AdminUtils::aUrl("manager/thread", array("id" => $data->discussion_id))',
                ),
            )
        ),
    ),
));
?>