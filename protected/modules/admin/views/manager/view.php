<div class="stables">
    <span class="success"><?php echo _user()->hasFlash('success') ? _user()->getFlash('success') : ''; ?></span>
    <span id="failure"><?php echo _user()->hasFlash('failure') ? _user()->getFlash('failure') : ''; ?></span>
</div>
<?php $none = '<span class="gray">[none]</span>'; ?>
<div class="manager-top">
    <div class="username"><?php echo $manager->cleanUsername; ?></div>
    <div class="status"><?php echo $manager->roleToText(); ?><div><?php echo $ban ? ' Banned until ' . Utils::daysAdd($ban->date_banned, $ban->duration) : ''; ?></div></div>
    <div class="photo">
        <?php
        if ($manager->profile->photo) :
            echo CHtml::ajaxLink(CHtml::image(AdminUtils::adminImageUrl('close.png')), AdminUtils::aUrl('manager/deletePhoto', array('id' => $manager->id)), array(
                'update' => '.photo',
                ), array(
                'class' => 'remove',
                'title' => _t('Remove photo')
            ));
        endif;
        ?>
        <?php echo $manager->profile->photo ? CHtml::image(Utils::managerPhotoUrl($manager->profile->photo), null, array('id' => 'pic')) : CHtml::image(Utils::imageUrl('profile/empty_profile.png')); ?>
    </div>
    <div class="personal">
        <?php
        if ($manager->profile->line) :
            echo CHtml::ajaxLink(CHtml::image(AdminUtils::adminImageUrl('close.png')), AdminUtils::aUrl('manager/deleteLine', array('id' => $manager->id)), array(
                'update' => '#line',
                ), array(
                'class' => 'removeLine',
                'title' => _t('Remove personal message')
            ));
        endif;
        ?>
        <div id="<?php echo $manager->profile->line ? 'line' : 'no-line'; ?>"><?php echo $manager->profile->line ? $manager->profile->line : '<span>no personal message</span>'; ?></div>
    </div>
    <div class="teamName">Team name: <span id="<?php echo $manager->id; ?>" oldName="<?php echo $manager->managerTeam->name; ?>"><?php echo $manager->managerTeam->name; ?></span> (created on <?php echo Utils::date($manager->managerTeam->created_on); ?>)</div>
</div>
<div class="manager-main">
    <div class="left-column">
        <div class="header">Personal Info</div>
        <p>Full Name: <span><?php echo $manager->fullname; ?></span></p>
        <p>Email: <span><?php echo $manager->email; ?></span></p>
        <p>Mobile: <span><?php echo _isModuleOn('sms') ? $manager->mobile->mobile : $none; ?></span></p>
        <p>Gender: <span><?php echo $manager->profile->gender; ?></span></p>
        <p>Birthdate: <span><?php echo $manager->profile->birthdate != null ? Utils::date($manager->profile->birthdate) : $none; ?></span></p>
        <p>Location: <span><?php echo $manager->profile->location != null ? $manager->profile->location : $none; ?></span></p>
        <p>Favorite Team: <span><?php echo $manager->profile->team->name; ?></span></p>
        <p>
            IP Address:
            <span>
                <?php
                echo CHtml::ajaxlink($manager->last_known_ip, AdminUtils::aUrl('manager/similarIps', array('id' => $manager->id)), array(
                    'success' => 'function(html){jQuery(".dialog").html(html); jQuery(".dialog").dialog("open");}'
                    ), array(
                    'class' => 'ipLink',
                    'title' => 'Click to view other users with same IP address'
                    )
                );
                ?>
            </span>
            <span><?php echo CHtml::image(AdminUtils::adminImageUrl('flags/' . strtolower(@geoip_country_name_by_name($manager->last_known_ip)) . '.png')); ?></span></p>
        <p>Newsletter: <span id="newsletter"><?php echo CHtml::link($manager->newsletter ? 'Yes' : 'No', AdminUtils::aUrl('manager/changeNewsletter', array('id' => $manager->id, 'value' => (int) !$manager->newsletter)), array('id' => 'newsLink')); ?></span></p>
    </div>
    <div class="right-column">
        <div class="header">Game Info</div>
        <p>Rank: <span><?php echo $manager->rank->rank; ?></span></p>
        <p>Points: <span><?php echo $manager->total_points; ?></span></p>
        <p>Portfolio Value: <span><?php echo Utils::currency($manager->portfolio_share_value + $manager->portfolio_cash); ?></span></p>
        <p>Transactions: <span><?php echo CHtml::link($counts['transactions'], AdminUtils::aUrl('monitor/transactions', array('manager' => $manager->id)), array('title' => 'Click to view transactions', 'target' => '_blank')); ?></span></p>
        <p>Reports: <span><?php echo CHtml::link($counts['reports'], AdminUtils::aUrl('report/manager', array('id' => $manager->id)), array('title' => 'Click to view reports', 'target' => '_blank')); ?></span></p>
        <p>Comments: <span><?php echo CHtml::link($counts['comments'], AdminUtils::aUrl('manager/comments', array('manager' => $manager->id)), array('title' => 'Click to view comments', 'target' => '_blank')); ?></span></p>
        <p>Messages: <span><?php echo CHtml::link($counts['messages'], AdminUtils::aUrl('manager/threads', array('id' => $manager->id))); ?></span></p>
        <p>Leagues: <span><?php echo $counts['leagues']; ?></span></p>
    </div>
</div>
<div class="separator"></div>
<div class="dates">
    <div>Registered: <?php echo CHtml::tag('div', array('class' => 'date'), Utils::date($manager->registration_date)); ?></span></div>
    <div>Activated: <?php echo CHtml::tag('div', array('class' => 'date'), Utils::date($manager->activation_date)); ?></span></div>
    <div>Last Login: <?php echo CHtml::tag('div', array('class' => 'date'), Utils::date($manager->last_login)); ?></span></div>
    <div>Last Activity: <?php echo CHtml::tag('div', array('class' => 'date'), Utils::date($manager->last_activity)); ?></span></div>
</div>
<div class="separator"></div>
<div class="actions">
    <span class="ban">
        <?php if ($ban) : ?>
            <?php echo CHtml::link('Unban user', AdminUtils::aUrl('manager/unban', array('id' => $manager->id))); ?>
        <?php else : ?>
            <?php echo CHtml::link('Ban user', AdminUtils::aUrl('manager/ban', array('id' => $manager->id)), array('id' => 'banLink')); ?>
        <?php endif; ?>
    </span>
    <span class="message"><?php echo CHtml::link('Send message', AdminUtils::aUrl('manager/sendMessage', array('id' => $manager->id)), array('id' => 'msgLink')); ?></span>
</div>

<div class="dialog">
</div>

<div class="ban-dialog">
    <?php $this->renderPartial('_banForm', array('manager' => $manager)); ?>
</div>

<div class="msg-dialog">
    <?php $this->renderPartial('_sendMessage', array('manager' => $manager)); ?>
</div>