<div class="stables">
    <span class="success"><?php echo _user()->hasFlash('success') ? _user()->getFlash('success') : ''; ?></span>
    <span><?php echo _user()->hasFlash('failure') ? _user()->getFlash('failure') : ''; ?></span>
</div>
<?php $none = '<span class="gray">[none]</span>'; ?>
<div class="manager-top">
    <div class="username"><?php echo $manager->cleanUsername; ?></div>
    <div class="status">Deleted (on <?php echo Utils::date($manager->date); ?>)</div>
    <div class="personal"><?php echo $manager->reportReason->reason; ?></div>
    <div class="personal"><?php echo $manager->details; ?></div>
    <div class="teamName">Team name: <?php echo $manager->team_name; ?></div>
</div>
<div class="manager-main">
    <div class="left-column">
        <div class="header">Personal Info</div>
        <p>Full Name: <span><?php echo $manager->fullname; ?></span></p>
        <p>Email: <span><?php echo $manager->email; ?></span></p>
        <p>Gender: <span><?php echo $manager->gender; ?></span></p>
        <p>Birthdate: <span><?php echo $manager->birthdate != null ? Utils::date($manager->birthdate) : $none; ?></span></p>
        <p>Location: <span><?php echo $manager->location != null ? $manager->location : $none; ?></span></p>
    </div>
    <div class="right-column">
        <div class="header">Game Info</div>
        <p>Points: <span><?php echo $manager->total_points; ?></span></p>
        <p>Portfolio Value: <span><?php echo Utils::currency($manager->portfolio_share_value + $manager->portfolio_cash); ?></span></p>
    </div>
</div>
<div class="separator"></div>
<div class="dates">
    <div>Registered: <?php echo CHtml::tag('div', array('class' => 'date'), Utils::date($manager->registration_date)); ?></span></div>
</div>
<div class="separator"></div>