<?php
/**
 * @deprecated Deprecated since rev. 137
 */
?>
<table>
    <tr>
        <td>Date/Time</td>
        <td><?php echo $message->date_sent; ?></td>
    </tr>
    <tr>
        <td>From</td>
        <td><?php echo $message->sender->username; ?></td>
    </tr>
    <tr>
        <td>To</td>
        <td><?php echo $message->recipient->username; ?></td>
    </tr>
    <tr>
        <td>Subject</td>
        <td><?php echo $message->subject; ?></td>
    </tr>
    <tr>
        <td>Message</td>
        <td><?php echo $message->message; ?></td>
    </tr>
</table>
<?php echo CHtml::link('Cancel', AdminUtils::aUrl('manager/messages')); ?>