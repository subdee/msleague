<div class="stables">
    <span class="success"><?php echo _user()->hasFlash('success') ? _user()->getFlash('success') : ''; ?></span>
    <span><?php echo _user()->hasFlash('failure') ? _user()->getFlash('failure') : ''; ?></span>
</div>
<?php $none = '<span class="gray">[none]</span>'; ?>
<div class="manager-top">
    <div class="username"><?php echo $manager->cleanUsername; ?></div>
    <div class="status"><?php echo $manager->roleToText(); ?></div>
</div>
<div class="manager-main">
    <div>
        <div class="header">Personal Info</div>
        <p>Full Name: <span><?php echo $manager->fullname; ?></span></p>
        <p>Email: <span><?php echo $manager->email; ?></span></p>
        <p>Mobile: <span><?php echo _isModuleOn('sms') ? $manager->mobile->mobile : $none; ?></span></p>
        <p>Gender: <span><?php echo $manager->profile->gender; ?></span></p>
        <p>Favorite Team: <span><?php echo $manager->profile->team ? $manager->profile->team->name : $none; ?></span></p>
        <p>IP Address: <span>
                <?php
                echo CHtml::ajaxlink($manager->last_known_ip, AdminUtils::aUrl('manager/similarIps', array('id' => $manager->id)), array(
                    'success' => 'function(html){jQuery(".dialog").html(html); jQuery(".dialog").dialog("open");}'
                    ), array(
                    'class' => 'ipLink',
                    'title' => 'Click to view other users with same IP address'
                    )
                );
                ?>
            </span><span><?php echo CHtml::image(AdminUtils::adminImageUrl('flags/' . strtolower(@geoip_country_name_by_name($manager->last_known_ip)) . '.png')); ?></span></p>
        <p>Newsletter: <span id="newsletter"><?php echo CHtml::link($manager->newsletter ? 'Yes' : 'No',AdminUtils::aUrl('manager/changeNewsletter',array('id' => $manager->id, 'value' => (int)!$manager->newsletter)),array('id' => 'newsLink')); ?></span></p>
    </div>
</div>
<div class="separator"></div>
<div class="dates">
    <div>Registered: <?php echo CHtml::tag('div', array('class' => 'date'), Utils::date($manager->registration_date)); ?></span></div>
    <div>Activated: <?php echo CHtml::tag('div', array('class' => 'date'), Utils::date($manager->activation_date)); ?></span></div>
    <div>Last Login: <?php echo CHtml::tag('div', array('class' => 'date'), Utils::date($manager->last_login ? $manager->last_login : '[never]')); ?></span></div>
    <div>Last Activity: <?php echo CHtml::tag('div', array('class' => 'date'), Utils::date($manager->last_activity ? $manager->last_activity : '[never]')); ?></span></div>
</div>
<div class="separator"></div>

<div class="dialog">
</div>