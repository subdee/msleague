<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'stockInfo',
    'dataProvider' => $players->search(),
    'filter' => $players,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),    'columns' => array(
        array(
            'header' => '',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
        ),
        array(
            'name' => 'shortname',
            'htmlOptions'=>array('width'=>'17%')
        ),
        array(
            'header'=>'Price',
            'name' => 'current_value',
            'value' => '_currency($data->current_value)',
            'filter' => false,
            'htmlOptions' => array('class' => 'tright')
        ),
        array(
            'header'=>'Points',
            'name' => 'total_points',
            'filter' => false,
            'htmlOptions' => array('class' => 'tright')
        ),
        array(
            'name' => 'bank_shares',
            'filter' => false,
            'htmlOptions' => array('class' => 'tright')
        ),
        array(
            'name' => 'list_date',
            'filter' => false,
            'value' => 'Utils::date($data->list_date)',
            'htmlOptions' => array('class' => 'tright')
        ),
        array(
            'name' => 'delist_date',
            'filter' => false,
            'value' => '$data->delist_date != null ? Utils::date($data->delist_date) : ""',
            'htmlOptions' => array('class' => 'tright')
        ),
        array(
            'header'=>'Position',
            'name' => 'basicPosition.abbreviation',
            'filter' => false,
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'header'=>'Team',
            'name' => 'team.shortname5',
            'filter' => false,
            'htmlOptions' => array('class' => 'tcenter')
        ),
    ),
));
?>