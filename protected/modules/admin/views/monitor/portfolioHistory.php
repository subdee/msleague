Portfolio history for: <b><?php echo $username; ?></b>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'portfolio',
    'dataProvider' => $history->search(),
    'filter' => $history,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),    'columns' => array(
        array(
            'name' => 'date',
            'value'=>'Utils::date($data->date)',
            'filter' => false,
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'name' => 'portfolio_value',
            'value' => '_currency($data->portfolio_value)',
            'filter' => false,
            'htmlOptions' => array('class' => 'tcenter')
        ),
    ),
));
?>