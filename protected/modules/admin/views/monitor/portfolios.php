<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'portfolio',
    'dataProvider' => $m->searchPortfolios(),
    'filter' => $m,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),    'columns' => array(
        array(
            'header' => '',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
        ),
        array(
            'type' => 'raw',
            'name' => 'username'
        ),
        array(
            'name' => 'total_points',
            'filter' => false,
            'htmlOptions' => array('class' => 'tright')
        ),
        array(
            'name' => 'value',
            'value' => '_currency($data->value)',
            'filter' => false,
            'htmlOptions' => array('class' => 'tright')
        ),
        array(
            'header' => '',
            'type' => 'raw',
            'value' => 'CHtml::link(\'<img src="\'.AdminUtils::adminImageUrl(\'info.png\').\'" title="Value history" />\',AdminUtils::aUrl(\'monitor/portfolioHistory\',array(\'id\'=>$data->id)))',
            'filter' => false,
        ),
    ),
));
?>