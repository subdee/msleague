<div class="stables"><span><?php echo _user()->hasFlash('failure') ? _user()->getFlash('failure') : ''; ?></span></div>

<?php echo CHtml::link('Clear filters',AdminUtils::aUrl('monitor/transactions',array('clearFilters'=>true))); ?>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'trans',
    'dataProvider' => $trans->searchAdmin(),
    'filter' => $trans,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),    'columns' => array(
        array(
            'header' => '',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
        ),
        array(
            'type' => 'raw',
            'name' => 'username',
            'value' => '$data->manager->username'
        ),
        array(
            'name' => 'playername',
            'value' => '$data->player->shortname',
            'filter' => CHtml::listData(Player::model()->findAll(array('order' => 'shortname asc')), 'id', 'concatNameTeam'),
        ),
        array(
            'name' => 'date_completed',
            'filter' => false,
        ),
        array(
            'name' => 'type_of_transaction',
            'filter' => false,
        ),
        array(
            'name' => 'shares',
            'filter' => false,
            'htmlOptions' => array('class' => 'tright')
        ),
        array(
            'name' => 'price_share',
            'value' => '_currency($data->price_share)',
            'filter' => false,
            'htmlOptions' => array('class' => 'tright')
        ),
        array(
            'name' => 'total_price',
            'value' => '_currency($data->total_price)',
            'filter' => false,
            'htmlOptions' => array('class' => 'tright')
        ),
    ),
));
?>
