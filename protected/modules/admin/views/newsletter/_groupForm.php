<div class="formDialog">
    <div class="form">
        <?php
        $model = new ManagerGroup;

        $form = $this->beginWidget('CActiveForm', array(
                'id' => 'managerGroup-form',
                'enableAjaxValidation' => false, 'enableClientValidation' => true,
            ));
        ?>

        <div class="row">
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField($model, 'name'); ?>
            <?php echo $form->error($model, 'name'); ?>
        </div>

        <div class="row">
            <?php //echo CHtml::radioButtonList('radio', 'all', array('all' => 'All Managers', 'select' => 'Select group')); ?>
            Select group(s)
        </div>

        <div class="bordered">
            <div class="row">
                <?php echo CHtml::label('Not logged in', 'nonactive'); ?>
                <?php echo CHtml::checkBox('nonactive'); ?>
                for the last 
                <?php echo CHtml::textField('nonactive_value','5',array('size'=>1,'maxlength'=>2)); ?>
                days
            </div>
            
            <div class="row">
                <?php echo CHtml::label('Not created team', 'temp'); ?>
                <?php echo CHtml::checkBox('temp'); ?>
                for the last 
                <?php echo CHtml::textField('temp_value','5',array('size'=>1,'maxlength'=>2)); ?>
                days
            </div>

            <div class="row">
                <?php echo CHtml::label('Not activated account', 'inactive'); ?>
                <?php echo CHtml::checkBox('inactive'); ?>
                for the last
                <?php echo CHtml::textField('inactive_value','5',array('size'=>1,'maxlength'=>2)); ?>
                days
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div>