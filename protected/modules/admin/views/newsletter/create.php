<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'newsletter-form',
        'enableAjaxValidation' => false, 'enableClientValidation' => true,
        ));
    ?>

    <div class="row">
        <?php echo $form->labelEx($newsletter, 'subject'); ?>
<?php echo $form->textField($newsletter, 'subject'); ?>
<?php echo $form->error($newsletter, 'subject'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($newsletter, 'content'); ?>
        <?php
        $this->widget('ext.editMe.ExtEditMe', array(
            'model' => $newsletter,
            'attribute' => 'content',
            'htmlOptions' => array('option' => 'value'),
            'toolbar' => array(
                array('Undo', '-', 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'),
                array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'BidiLtr', 'BidiRtl'),
                array('Link', 'Unlink', 'Anchor'),
            array('Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'),
                '/',
                array('Styles', 'Format', 'Font', 'FontSize'),
                array('TextColor', 'BGColor'),
            )
        ));
        ?>
<?php echo $form->error($newsletter, 'content'); ?>
    </div>

    <div class="row buttons">
<?php echo CHtml::submitButton('Save'); ?>
    <?php echo CHtml::link('Cancel', AdminUtils::aUrl('newsletter/index')); ?>
    </div>

<?php $this->endWidget(); ?>
</div>