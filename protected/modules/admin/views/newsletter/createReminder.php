<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'reminder-form',
        'enableAjaxValidation' => false, 'enableClientValidation' => true,
        ));
    ?>

    <div class="row">
        <?php echo $form->labelEx($newsletter, 'subject'); ?>
        <?php echo $form->textField($newsletter, 'subject'); ?>
        <?php echo $form->error($newsletter, 'subject'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($newsletter, 'content'); ?>
        <?php
        $this->widget('ext.editMe.ExtEditMe', array(
            'model' => $newsletter,
            'attribute' => 'content',
            'htmlOptions' => array('option' => 'value'),
            'toolbar' => array(
                array('Undo', '-', 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'),
                array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'BidiLtr', 'BidiRtl'),
                array('Link', 'Unlink', 'Anchor'),
            array('Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'),
                '/',
                array('Styles', 'Format', 'Font', 'FontSize'),
                array('TextColor', 'BGColor'),
            )
        ));
        ?>
        <?php echo $form->error($newsletter, 'content'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($newsletter, 'manager_group_id'); ?>
        <div class="manager_group"><?php echo $form->dropDownList($newsletter, 'manager_group_id', CHtml::listData(ManagerGroup::model()->findAll(array('order' => 'id desc')), 'id', 'name')); ?>
            <a href="<?php echo AdminUtils::aUrl('newsletter/createGroup'); ?>" class="newGroup">Create new group</a></div>
        <?php echo $form->error($newsletter, 'manager_group_id'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Save'); ?>
        <?php echo CHtml::link('Cancel', AdminUtils::aUrl('newsletter/reminders')); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>

<?php $this->renderPartial('_groupForm'); ?>

<div class="errorDiv stables">
</div>