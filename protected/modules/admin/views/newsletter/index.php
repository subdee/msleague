<a href="<?php echo AdminUtils::aUrl('newsletter/create'); ?>"><div class="addButton">Add new newsletter</div></a>
<br/><br/>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'unlisted',
    'dataProvider' => $newsletters->search(),
    'filter' => $newsletters,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),    'columns' => array(
        array(
            'name' => 'id',
            'htmlOptions' => array('width' => '5%')
        ),
        array(
            'name' => 'subject',
            'htmlOptions' => array('width' => '55%')
        ),
        array(
            'name' => 'last_sent',
            'value'=>'$data->last_sent != null ? Utils::date($data->last_sent) : "(not yet sent)"',
            'filter' => false,
            'htmlOptions' => array('width' => '15%')
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{send}{update}{delete}',
            'deleteButtonImageUrl' => AdminUtils::adminImageUrl('delete.png'),
            'updateButtonImageUrl' => AdminUtils::adminImageUrl('edit.png'),
            'deleteButtonUrl' => 'AdminUtils::aUrl("newsletter/delete", array("id" => $data->id))',
            'updateButtonUrl' => 'AdminUtils::aUrl("newsletter/edit", array("id" => $data->id))',
            'buttons' => array(
                'send' => array(
                    'label' => 'Send newsletter',
                    'url' => 'AdminUtils::aUrl("newsletter/send", array("id" => $data->id))',
                    'imageUrl' => AdminUtils::adminImageUrl('mail.png'),
                    'options' => array('class'=>'sendNews')
                ),
            )
        ),
    ),
));
?>

<div class="loading">
    Sending newsletters...
    <img src="<?php echo AdminUtils::adminImageUrl('loading.gif'); ?>" />
</div>

<div class="sent">
</div>

<div class="notSent">
    There was an error sending the newsletters. Please contact the administrator.
</div>

<div class="confirmDialog">
    Are you sure you want to send this newsletter?
</div>