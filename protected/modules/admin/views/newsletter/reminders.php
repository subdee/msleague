<?php echo _user()->hasFlash('failure') ? _user()->getFlash('failure') : ''; ?>
Reminders will run daily at <?php echo $reminderTime; ?>
<br /><br />
<a href="<?php echo AdminUtils::aUrl('newsletter/createReminder'); ?>"><div class="addButton">Add new reminder</div></a>
<br/><br/>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'unlisted',
    'dataProvider' => $newsletters->searchAuto(),
    'filter' => $newsletters,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),    'columns' => array(
        array(
            'name' => 'id',
            'htmlOptions' => array('width' => '5%')
        ),
        array(
            'name' => 'subject',
            'htmlOptions' => array('width' => '55%')
        ),
        array(
            'name' => 'managerGroup.name',
            'htmlOptions' => array('width' => '15%'),
        ),
        array(
            'name' => 'last_sent',
            'value'=>'$data->last_sent != null ? Utils::date($data->last_sent) : "(not yet sent)"',
            'filter' => false,
            'htmlOptions' => array('width' => '15%')
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{start}{pause}{update}{delete}',
            'deleteButtonImageUrl' => AdminUtils::adminImageUrl('delete.png'),
            'updateButtonImageUrl' => AdminUtils::adminImageUrl('edit.png'),
            'deleteButtonUrl' => 'AdminUtils::aUrl("newsletter/delete", array("id" => $data->id))',
            'updateButtonUrl' => 'AdminUtils::aUrl("newsletter/editReminder", array("id" => $data->id))',
            'buttons' => array(
                'start' => array(
                    'label' => 'Start',
                    'url' => 'AdminUtils::aUrl("newsletter/startReminder", array("id" => $data->id))',
                    'imageUrl' => AdminUtils::adminImageUrl('play.png'),
                    'visible' => '!$data->is_active',
                ),
                'pause' => array(
                    'label' => 'Pause',
                    'url' => 'AdminUtils::aUrl("newsletter/pauseReminder", array("id" => $data->id))',
                    'imageUrl' => AdminUtils::adminImageUrl('pause.png'),
                    'visible' => '$data->is_active',
                ),
            )
        ),
    ),
));
?>
