<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
            'id' => 'delist-form',
            'enableAjaxValidation' => false, 
            'enableClientValidation' => true,
        ));
    ?>

    <div class="row">
        <?php echo $player->shortname; ?>
    </div>
    
    <div class="row">
        <?php echo CHtml::hiddenField('Player[id]',$player->id); ?>
    </div>

    <div class="row">
        <?php echo CHtml::checkBox('Player[deleteTeam]', true); ?>
        <span style="font-weight: bold; font-size: 0.9em"><?php echo 'Remove his team (' . $player->team->name . ')'; ?></span>
    </div>

    <div class="row buttons">
    <?php echo CHtml::button('Delist now',array('id'=>'submitBtn')); ?>
        <?php echo CHtml::link('Cancel',AdminUtils::aUrl('player/listed')); ?>
    </div>

<?php $this->endWidget(); ?>
</div>
<div id="dialog"><?php echo $player->shortname; ?> will be delisted from the stockmarket from now on.Are you sure?</div>