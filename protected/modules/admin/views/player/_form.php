<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'player-form',
	'enableAjaxValidation'=>false,'enableClientValidation'=>true,
)); ?>

    <div class="row">
        <?php echo $form->labelEx($player,'name'); ?>
        <?php echo $form->textField($player,'name'); ?>
        <?php echo $form->error($player,'name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($player,'shortname'); ?>
        <?php echo $form->textField($player,'shortname'); ?>
        <?php echo $form->error($player,'shortname'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($player,'basicPosition'); ?>
        <?php echo $form->dropDownList($player,'basic_position_id',$this->getBasicPositions()); ?>
        <?php echo $form->error($player,'basicPosition'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($player,'team_id'); ?>
        <?php echo $form->dropDownList($player,'team_id',$this->getTeams(),array('prompt'=>'-')); ?>
        <?php echo $form->error($player,'team_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($player,'xml_id'); ?>
        <?php echo $form->textField($player,'xml_id'); ?>
        <?php echo $form->error($player,'xml_id'); ?>
    </div>

    <?php echo isset($src) ? CHtml::hiddenField('src',$src) : ''; ?>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Save'); ?>
        <?php echo CHtml::link('Cancel',AdminUtils::aUrl("player/$src")); ?>
    </div>

<?php $this->endWidget(); ?>
</div>