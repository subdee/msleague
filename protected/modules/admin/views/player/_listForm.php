<div class="stables"><span><?php echo _user()->hasFlash('failure') ? _user()->getFlash('failure') : ''; ?></span></div>
<div class="form">
<?php echo CHtml::form(AdminUtils::aUrl('player/list'),'post'); ?>

    <div class="row">
        <?php echo CHtml::label('Player to be listed','player'); ?>
        <?php echo CHtml::dropDownList('player',$player,$this->getNewPlayers()); ?>
    </div>

    <div class="row">
        <?php echo CHtml::label('List Date/Time','list_date'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
            'name'=>'list_date',
            'options'=>array(
                'dateFormat'=>'yy-mm-dd',
                'changeMonth' => true,
                'changeYear' => true,
                'minDate' => date('Y-m-d',strtotime(date("Y-m-d", strtotime(date('Y-m-d'))) . " +1 day")),
                ),
            'htmlOptions'=>array('readonly'=>true),
            ));
        ?>
    </div>

    <div class="row">
        <?php echo CHtml::label('Initial Total Value','total_value'); ?>
        <?php echo CHtml::textField('total_value','',array('id'=>'total_value')); ?>
    </div>

    <div class="row">
        <?php echo CHtml::label('Number of Shares','bank_shares'); ?>
        <?php echo CHtml::textField('bank_shares',$shares,array('readonly'=>true,'id'=>'bank_shares')); ?>
    </div>

    <div class="row">
        <?php echo CHtml::label('Initial Price / Share','initial_value'); ?>
        <?php echo CHtml::textField('initial_value','',array('readonly'=>true,'id'=>'initial_value')); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Save'); ?>
        <?php echo CHtml::link('Cancel',AdminUtils::aUrl('player/index')); ?>
    </div>

<?php echo CHtml::endForm(); ?>
</div>

<script type="text/javascript">
    $(document).ready(function(){
    $("#total_value").focusout(function(){
        var bs = $("#bank_shares").val();
        var tv = $(this).val();
        var init = tv/bs;
        $("#initial_value").val(init.toFixed(2)).change();
    });

    $("#total_value, #bank_shares").numeric();
});
</script>