<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'delisted',
    'dataProvider' => $players->searchDelisted(),
    'filter' => $players,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),    'columns' => array(
        array(
            'header' => '',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
        ),
        array(
            'name' => 'name'
        ),
        array(
            'name' => 'delist_date',
            'value' => 'Utils::date($data->delist_date)',
            'filter' => false,
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'name' => 'basic_position_id',
            'value' => '$data->basicPosition->abbreviation',
            'filter' => CHtml::listData(BasicPosition::model()->findAll(), 'id', 'abbreviation'),
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'name' => 'team_id',
            'value' => '$data->team != null ? $data->team->shortname5 : ""',
            'filter' => CHtml::listData(Team::model()->findAll(), 'id', 'shortname5'),
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}',
            'updateButtonImageUrl' => AdminUtils::adminImageUrl('edit.png'),
            'updateButtonUrl' => 'AdminUtils::aUrl("player/edit", array("id" => $data->id, "src" => "delisted"))',
        ),
    ),
));
?>