<a href="<?php echo AdminUtils::aUrl('player/add'); ?>"><div class="addButton">Add new player</div></a>
<span style="float: right"><a href="#" class="upload"><div class="addButton">Mass insert players (Upload XML)</div></a></span>
<br><br>
<div class="stables">
    <span class="success"><?php echo _user()->hasFlash('success') ? _user()->getFlash('success') : ''; ?></span>
    <span><?php echo _user()->hasFlash('failure') ? _user()->getFlash('failure') : ''; ?></span>
</div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'unlisted',
    'dataProvider' => $players->searchIndex(),
    'filter' => $players,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),    'columns' => array(
        array(
            'header' => '',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
        ),
        array(
            'name' => 'name'
        ),
        array(
            'name' => 'basic_position_id',
            'value' => '$data->basicPosition->abbreviation',
            'filter' => CHtml::listData(BasicPosition::model()->findAll(), 'id', 'abbreviation'),
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'name' => 'team_id',
            'value' => '$data->team != null ? $data->team->shortname5 : ""',
            'filter' => CHtml::listData(Team::model()->findAll(), 'id', 'shortname5'),
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'header' => '',
            'type' => 'raw',
            'value' => '$data->list_date == null ? CHtml::link("List",AdminUtils::aUrl("player/list",array("id"=>$data->id))) : "To be listed on: ".Utils::date($data->list_date)." at ".Utils::currency($data->initial_value)."/shr<br />".CHtml::link("Cancel",AdminUtils::aUrl("player/cancelList",array("id"=>$data->id)))',
            'filter' => false,
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}{delete}',
            'updateButtonImageUrl' => AdminUtils::adminImageUrl('edit.png'),
            'deleteButtonImageUrl' => AdminUtils::adminImageUrl('delete.png'),
            'updateButtonUrl' => 'AdminUtils::aUrl("player/edit", array("id" => $data->id))',
            'deleteConfirmation'=>"Are you sure you wish to delete this player from the system?"
        ),
    ),
));
?>

<div id="uploadPop" style="display: none;">
    <?php $this->renderPartial('_upload'); ?>
</div>