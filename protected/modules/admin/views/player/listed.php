<div class="stables">
    <span class="success"><?php echo _user()->hasFlash('success') ? _user()->getFlash('success') : ''; ?></span>
    <span><?php echo _user()->hasFlash('failure') ? _user()->getFlash('failure') : ''; ?></span>
</div>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'listed',
    'dataProvider' => $players->searchListed(),
    'filter' => $players,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),    'columns' => array(
        array(
            'header' => '',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
        ),
        array(
            'name' => 'name'
        ),
        array(
            'name' => 'list_date',
            'value' => 'Utils::date($data->list_date)',
            'filter' => false,
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'name' => 'initial_value',
            'value' => '_currency($data->initial_value)',
            'filter' => false,
            'htmlOptions' => array('class' => 'tright')
        ),
        array(
            'name' => 'basic_position_id',
            'value' => '$data->basicPosition->abbreviation',
            'filter' => CHtml::listData(BasicPosition::model()->findAll(), 'id', 'abbreviation'),
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'name' => 'team_id',
            'value' => '$data->team != null ? $data->team->shortname5 : ""',
            'filter' => CHtml::listData(Team::model()->findAll(), 'id', 'shortname5'),
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'header' => '',
            'type' => 'raw',
            'value' => '$data->delist_date == null ? CHtml::link("Delist",AdminUtils::aUrl("player/delist",array("id"=>$data->id))) : "To be delisted on: ".Utils::date($data->delist_date)."<br />".CHtml::link("Cancel",AdminUtils::aUrl("player/cancelDelist",array("id"=>$data->id)))',
            'filter' => false,
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}',
            'updateButtonImageUrl' => AdminUtils::adminImageUrl('edit.png'),
            'updateButtonUrl' => 'AdminUtils::aUrl("player/edit", array("id" => $data->id, "src" => "listed"))',
        ),
    ),
));
?>