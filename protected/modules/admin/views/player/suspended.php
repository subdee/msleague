
<br />
<span style="float: right"><a href="#" class="upload"><div class="addButton">Update injured & suspended lists (Upload XML)</div></a></span>
<div class="form">
    <div class="row">
        <?php
        echo CHtml::form(AdminUtils::aUrl('player/addSuspended'), 'post');
        echo CHtml::label('Add suspended player', 'player');
        echo CHtml::dropDownList('player', '', $this->getUnsuspendedPlayers());
        echo CHtml::submitButton('Add');
        echo CHtml::endForm();
        ?>
    </div>
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'suspended',
    'dataProvider' => $players->searchAdmin(),
    'filter' => $players,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),    'columns' => array(
        array(
            'header' => '',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
        ),
        array(
            'name' => 'name'
        ),
        array(
            'name' => 'pos',
            'value' => '$data->basicPosition->abbreviation',
            'filter' => false,
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'name' => 'myTeam',
            'value' => '$data->team != null ? $data->team->shortname5 : "-"',
            'filter' => false,
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{delete}',
            'deleteButtonImageUrl' => AdminUtils::adminImageUrl('remove.png'),
            'deleteButtonUrl' => 'AdminUtils::aUrl("player/removeSuspended", array("id" => $data->id))',
            'deleteConfirmation' => 'Are you sure you want to delete this player?',
            'deleteButtonLabel' => 'Remove',
        ),
    ),
));
?>

<div id="uploadPop" style="display: none;">
    <?php $this->renderPartial('_uploadIS'); ?>
</div>