<div class="form stables">
<?php echo CHtml::form(AdminUtils::aUrl('manager/ban'),'post'); ?>

    You are banning user: <b><?php echo $manager->username; ?></b>
    
    <?php echo CHtml::hiddenField('id',$manager->id); ?>
    
    <div class="row">
        <?php echo CHtml::label('Ban Reason','reason'); ?>
        <?php echo CHtml::dropDownList('reason','',CHtml::listData(ReportReason::model()->reportTypes()->findAll(),'id','reason')); ?>
    </div>

    <div class="row">
        <?php echo CHtml::label('Ban Duration in days','duration'); ?>
        <?php echo CHtml::textField('duration'); ?>
    </div>

    <div class="row">
        <?php echo CHtml::label('Notes','notes'); ?>
        <?php echo CHtml::textArea('notes',$manager->notes,array('cols'=>'70','rows'=>'15')); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Ban'); ?>
    </div>

<?php echo CHtml::endForm(); ?>
</div>