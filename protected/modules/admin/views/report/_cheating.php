<div class="reports">
    <div class="user">
        <?php echo $manager->username; ?>
    </div>
    <div class="options">
        <span class="delete"><?php echo CHtml::link('Delete reports', AdminUtils::aUrl('report/delete', array('type' => ReportReason::CHEATING,'id' => $manager->id))); ?></span>
        <span class="ban"><?php echo CHtml::link('Ban user', AdminUtils::aUrl('manager/ban', array('id' => $manager->id)),array('id' => 'banLink')); ?></span>
    </div>
    <div class="grid-view">
        <table class="items">
            <thead>
                <tr>
                    <th width="15%">Action</th>
                    <th width="15%">Reporter</th>
                    <th width="35%">Reason</th>
                    <th width="20%">Details</th>
                    <th width="15%">Date reported</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($reports as $report) : ?>
                    <tr>
                        <td><?php echo $report->reportReason->reportActions[0]->action; ?></td>
                        <td><?php echo $report->reporter->username; ?></td>
                        <td><?php echo $report->reportReason->reason; ?></td>
                        <td><?php echo $report->comment; ?></td>
                        <td><?php echo Utils::date($report->date_created); ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="ban-dialog">
    <?php $this->renderPartial('_banForm',array('manager'=>$manager)); ?>
</div>