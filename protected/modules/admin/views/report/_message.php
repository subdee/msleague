<div class="reports">
    <div class="user">
        <?php echo $manager->username; ?>
    </div>
    <div class="options">
        <span class="ban"><?php echo CHtml::link('Ban user', AdminUtils::aUrl('manager/ban', array('id' => $manager->id)),array('id' => 'banLink')); ?></span>
    </div>
    <div class="grid-view">
        <table class="items">
            <thead>
                <tr>
                    <th width="5%">Type</th>
                    <th width="10%">Reporter</th>
                    <th width="33%">Reason</th>
                    <th width="37%">Message</th>
                    <th width="15%">Date reported</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($reports as $report) : ?>
                    <tr>
                        <td><?php echo $report->reportAction->action == 'forum' ? 'Comment' : ucfirst($report->reportAction->action); ?></td>
                        <td><?php echo $report->reporter->username; ?></td>
                        <td><?php echo $report->reportReason->reason; ?></td>
                        <td><?php
                if ($report->reportAction->action == 'forum') :
                    $message = CommentReported::model()->find('report_id = :id', array(':id' => $report->id));
                    if ($message != null) :
                        $mid = $message->comment->id;
                        echo CHtml::link($message->comment->content, AdminUtils::aUrl('report/messageDetails', array('id' => $mid, 'mid' => $manager->id)), array('class' => 'details','title' => 'Reports for comment posted by '.$manager->cleanUsername));
                    endif;
                endif;
                    ?></td>
                        <td><?php echo Utils::date($report->date_created); ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="messageDetails"></div>

<div class="ban-dialog">
    <?php $this->renderPartial('_banForm',array('manager'=>$manager)); ?>
</div>