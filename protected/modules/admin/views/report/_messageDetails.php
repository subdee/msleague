<div class="title">
    <?php if ($minfo['discussion']) : ?>
        <span>
    <?php echo PMThread::model()->exists('discussion_id = :did', array(':did' => $minfo['discussion'])) ? 
        CHtml::link($minfo['title'], AdminUtils::aUrl('manager/thread', array('id' => $minfo['discussion'])), array('target' => '_blank')) :
        CHtml::link($minfo['title'], AdminUtils::aUrl('forum/show', array('discussion' => $minfo['discussion'])), array('target' => '_blank')); ?></span>
    <?php else : ?>
        <span><?php echo $minfo['title']; ?></span>
    <?php endif; ?>
</div>
<div class="info">
    <?php echo $minfo['content']; ?>
</div>
<div class="options">
    <?php if (!PMThread::model()->exists('discussion_id = :did', array(':did' => $minfo['discussion']))) : ?>
        <span class="remove"><?php echo CHtml::link('Hide this comment', AdminUtils::aUrl('report/deleteMessage', array('id' => $manager_id, 'rid' => $reports[0]->id))); ?></span>
    <?php endif; ?>
    <span class="delete"><?php
    echo CHtml::link('Delete reports', AdminUtils::aUrl('report/delete', array(
            'type' => ReportReason::MESSAGE,
            'id' => $manager_id,
            'sid' => $mid,
        )));
    ?></span>
</div>
<div class="grid-view">
    <?php echo count($reports); ?> reports
    <table class="items">
        <thead>
            <tr>
                <th>Reporter</th>
                <th>Report reason</th>
                <th>Details</th>
        </thead>
        <tbody>
            <?php foreach ($reports as $report) : ?>
                <tr>
                    <td><?php echo $report->reporter->username; ?></td>
                    <td><?php echo $report->reportReason->reason; ?></td>
                    <td><?php echo $report->comment; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
