<div class="reports">
    <div class="user">
        <?php echo $manager->username; ?>
    </div>
    <div class="info">
        <?php echo $manager->managerTeam->name; ?>
    </div>
    <div class="options">
        <div><?php echo CHtml::form(AdminUtils::aUrl('report/changeTeamName'),'post',array('id' => 'teamNameForm')); ?>
            <?php echo CHtml::textField('teamName', $manager->managerTeam->name); ?>
            <?php echo CHtml::hiddenField('id',$manager->id); ?>
            <?php echo CHtml::submitButton('Save'); ?>
        <?php echo CHtml::endForm(); ?>
        </div>
        <span class="delete"><?php echo CHtml::link('Delete reports', AdminUtils::aUrl('report/delete', array('type' => ReportReason::TEAM_NAME,'id' => $manager->id))); ?></span>
        <span class="ban"><?php echo CHtml::link('Ban user', AdminUtils::aUrl('manager/ban', array('id' => $manager->id)),array('id' => 'banLink')); ?></span>
    </div>
    <div class="grid-view">
        <table class="items">
            <thead>
                <tr>
                    <th width="30%">Reporter</th>
                    <th width="40%">Details</th>
                    <th width="30%">Date reported</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($reports as $report) : ?>
                    <tr>
                        <td><?php echo $report->reporter->username; ?></td>
                        <td><?php echo $report->comment; ?></td>
                        <td><?php echo Utils::date($report->date_created); ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="ban-dialog">
    <?php $this->renderPartial('_banForm',array('manager'=>$manager)); ?>
</div>