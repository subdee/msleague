<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'reports',
    'dataProvider' => $report->search(),
    'filter' => $report,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),    'columns' => array(
        array(
            'header' => '',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
        ),
        array(
            'type'=>'raw',
            'header' => 'Manager',
            'name' => 'reported',
            'value'=>'CHtml::link($data->reported,AdminUtils::aUrl(\'report/manager\', array(\'id\'=>$data->id)))',
        ),
        array(
            'header' => 'Reports',
            'name' => 'count',
            'filter'=>false,
            'htmlOptions'=>array('class'=>'tcenter')
        ),
    ),
));
?>