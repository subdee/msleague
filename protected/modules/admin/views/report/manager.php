<div class="stables">
    <span class="success"><?php echo _user()->hasFlash('success') ? _user()->getFlash('success') : ''; ?></span>
    <span><?php echo _user()->hasFlash('failure') ? _user()->getFlash('failure') : ''; ?></span>
</div>
<div class="grid-view">
    Reports for <b><?php echo $manager->username; ?></b><br /><br />
    <table class="items" style="width: 50%;">
        <thead>
            <tr>
                <th>Type of report</th>
                <th>No of reports</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($typeCounts as $type => $count) : ?>
                <tr>
                    <td><a href="<?php echo $count ? AdminUtils::aUrl('report/view',array('id'=>$manager->id,'type'=>$type)) : '#'; ?>"><?php echo $type; ?></td>
                    <td class="center-align <?php echo ($count) ? 'red' : ''; ?>"><?php echo $count; ?></td>
                </tr>
            <?php endforeach; ?>
    </table>
</div>

<?php echo CHtml::link('&lt&ltBack',AdminUtils::aUrl('report/index')); ?>