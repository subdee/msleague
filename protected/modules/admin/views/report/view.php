<?php
$manager = Manager::model()->findByPk($reports[0]->manager_id);

switch ($reports[0]->reportReason->reason_type) :
    case ReportReason::USERNAME:
        $this->renderPartial('_username', array('reports' => $reports,'manager'=>$manager));
        break;
    case ReportReason::TEAM_NAME:
        $this->renderPartial('_teamName', array('reports' => $reports,'manager'=>$manager));
        break;
    case ReportReason::PHOTO:
        $this->renderPartial('_photo', array('reports' => $reports,'manager'=>$manager));
        break;
    case ReportReason::STATUS:
        $this->renderPartial('_status', array('reports' => $reports,'manager'=>$manager));
        break;
    case ReportReason::MESSAGE:
        $this->renderPartial('_message', array('reports' => $reports,'manager'=>$manager));
        break;
    case ReportReason::CHEATING:
        $this->renderPartial('_cheating', array('reports' => $reports,'manager'=>$manager));
        break;
    case ReportReason::OTHER:
        $this->renderPartial('_other', array('reports' => $reports,'manager'=>$manager));
        break;
    default:
        ?>There was an error!!!!<?
        break;
endswitch;
?>
<?php echo CHtml::link('&lt&ltBack to '.strip_tags($manager->username).'\'s reports',AdminUtils::aUrl('report/manager',array('id'=>$manager->id))); ?>