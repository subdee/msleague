<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'team-form',
	'enableAjaxValidation'=>false,'enableClientValidation'=>true,
        'htmlOptions'=>array('enctype' => 'multipart/form-data')
)); ?>

    <div class="row">
        <?php echo $form->labelEx($team,'name'); ?>
        <?php echo $form->textField($team,'name'); ?>
        <?php echo $form->error($team,'name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($team,'shortname15'); ?>
        <?php echo $form->textField($team,'shortname15'); ?>
        <?php echo $form->error($team,'shortname15'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($team,'shortname5'); ?>
        <?php echo $form->textField($team,'shortname5'); ?>
        <?php echo $form->error($team,'shortname5'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($team,'country_id'); ?>
        <?php echo $form->dropDownList($team,'country_id',$this->getCountries()); ?>
        <?php echo $form->error($team,'country_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($team,'xml_id'); ?>
        <?php echo $form->textField($team,'xml_id'); ?>
        <?php echo $form->error($team,'xml_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($team,'file_small'); ?>
        <?php echo CHtml::activeFileField($team, 'file_small'); ?>
        <?php echo $form->error($team, 'file_small'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($team,'file_large'); ?>
        <?php echo CHtml::activeFileField($team, 'file_large'); ?>
        <?php echo $form->error($team, 'file_large'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Save'); ?>
        <?php echo CHtml::link('Cancel',AdminUtils::aUrl('team/index')); ?>
    </div>

<?php $this->endWidget(); ?>
</div>