<div class="stables">
    <a href="<?php echo AdminUtils::aUrl('team/add'); ?>"><div class="addButton">Add new team</div></a>
    <span><?php echo _user()->hasFlash('failure') ? _user()->getFlash('failure') : ''; ?></span>
    </div>
    <br /><br />
<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Shortname 15</th>
            <th>Shortname 5</th>
            <th>Jersey</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <?php foreach ($t as $team) { ?>
    <tr>
        <td><?php echo $team->name; ?></td>
        <td><?php echo $team->shortname15; ?></td>
        <td><?php echo $team->shortname5; ?></td>
        <td><img src="<?php echo $team->small_jersey_url(); ?>"></td>
        <td><a href="<?php echo AdminUtils::aUrl('team/edit',array('id'=>$team->id)); ?>">Edit</a></td>
        <td><a href="<?php echo AdminUtils::aUrl('team/delete',array('id'=>$team->id)); ?>">Delete</a></td>
    </tr>
    <?php } ?>
</table>
</div>