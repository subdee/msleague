<div class="row">
    <?php echo $form->labelEx($rule, 'color'); ?>
    <div id="colorSelector"><div></div></div>
    <?php
    $this->widget('application.extensions.colorpicker.JColorPicker', array(
        'model' => $rule,
        'attribute' => 'color',
        'options' => array(
            'color' => '#' . $rule->color,
            'onShow' => 'js:function (colpkr) {$(colpkr).fadeIn(500);return false;}',
            'onHide' => 'js:function (colpkr) {$(colpkr).fadeOut(500);return false;}',
            'onChange' => "js:function (hsb, hex, rgb) {\$('#colorSelector div').css('backgroundColor', '#' + hex);}",
        ),
    ));
    ?>
    <?php echo $form->error($rule, 'color'); ?>
</div>
<script type="text/javascript">
    $('#colorSelector div').css('backgroundColor', '#' + '<?php echo $rule->color; ?>');
    $('#colorSelector').click(function(){
        $('#CmsTheme_color').click();
    })
</script>


