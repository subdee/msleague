
<?php if (file_exists($rule->imageRealPath)) : ?>
    <div class="row image">
        <?php echo CHtml::link(CHtml::image(AdminUtils::adminImageUrl("cms/{$rule->value}")), AdminUtils::adminImageUrl("cms/{$rule->value}"), array('rel' => 'prettyPhoto')); ?>
    </div>
<?php endif; ?>
<div class="row">
    <?php echo $form->labelEx($rule, 'image'); ?>
    <?php echo $form->FileField($rule, 'image'); ?>
    <?php echo $form->error($rule, 'image'); ?>
</div>
<?php jqPrettyPhoto::addPretty('.row.image a', jqPrettyPhoto::PRETTY_SINGLE, jqPrettyPhoto::THEME_DARK_ROUNDED, $prettyPhotoOptions); ?>