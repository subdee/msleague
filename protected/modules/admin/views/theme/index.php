<?php if (Yii::app()->user->hasFlash('success')): ?>
    <div class="notice success">
        <pre><?php echo Yii::app()->user->getFlash('success'); ?></pre>
    </div>
<?php endif; ?>
<?php
if (User::hasRole(AdminRole::ADMIN))
    echo CHtml::link(CHtml::tag('div', array('class' => 'addButton'), 'Add new rule'), AdminUtils::aUrl('theme/newRule'));

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $rules->search(),    'columns' => array(
        array(
            'name' => 'name',
        ),
        array(
            'name' => 'rule',
            'value' => '$data->display()',
            'visible' => User::hasRole(AdminRole::ADMIN)
        ),
        array(
            'type' => 'raw',
            'value' => '$data->type == "image" ? CHtml::link(CHtml::image(AdminUtils::adminImageUrl("cms/{$data->value}")), AdminUtils::adminImageUrl("cms/{$data->value}"), array("rel" => "prettyPhoto")) : CHtml::tag("div", array("class" => "color", "style" => "background:#{$data->value}"))',
            'htmlOptions' => array('class' => 'theme-rule'),
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}{delete}',
            'updateButtonImageUrl' => AdminUtils::adminImageUrl('edit.png'),
            'deleteButtonImageUrl' => AdminUtils::adminImageUrl('delete.png'),
            'deleteConfirmation' => "Are you sure you wish to delete this rule?",
            'buttons' => array(
                'delete' => array(
                    'visible' => 'User::hasRole(AdminRole::ADMIN)',
                )
            )
        ),
    ),
));

jqPrettyPhoto::addPretty('.theme-rule a', jqPrettyPhoto::PRETTY_SINGLE, jqPrettyPhoto::THEME_DARK_ROUNDED, $prettyPhotoOptions); 
?>