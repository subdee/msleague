<?php if (Yii::app()->user->hasFlash('success')): ?>
    <div class="notice success">
        <pre><?php echo Yii::app()->user->getFlash('success'); ?></pre>
    </div>
<?php endif; ?>
<?php if (Yii::app()->user->hasFlash('error')): ?>
    <div class="notice error">
        <pre><?php echo Yii::app()->user->getFlash('error'); ?></pre>
    </div>
<?php endif; ?>

<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
    ?>

    <?php if (User::hasRole(AdminRole::ADMIN)) : ?>
        <div class="row">
            <?php echo $form->labelEx($rule, 'type'); ?>
            <?php echo $form->dropDownList($rule, 'type', $ruleTypes); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($rule, 'name'); ?>
            <?php echo $form->TextField($rule, 'name'); ?>
            <?php echo $form->error($rule, 'name'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($rule, 'rule'); ?>
            <?php echo CHtml::tag('div', array(), 'Eg. div { color: {{value}} }'); ?>
            <?php echo $form->Textarea($rule, 'rule'); ?>
            <?php echo $form->error($rule, 'rule'); ?>
        </div>
    <?php endif; ?>
    
    <?php foreach ($ruleTypes as $type) : ?>
        <?php
        $class = '';
        if ($rule->type != $type)
            $class = 'hidden';
        ?>
        <div id="customTypeForm_<?php echo $type; ?>" class="customTypeForms <?php echo $class; ?>">
            <?php $this->renderPartial('_rule' . ucfirst($type), array('form' => $form, 'rule' => $rule, 'prettyPhotoOptions' => $prettyPhotoOptions)); ?>
        </div>
    <?php endforeach; ?>

    <div class="row buttons">
        <?php echo CHtml::submitButton($submit); ?>
        <?php echo CHtml::link('Cancel', AdminUtils::aUrl('theme/index')); ?>
    </div>

    <?php echo $form->HiddenField($rule, 'value'); ?>

    <?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#CmsTheme_type').change(function(){
            $('.customTypeForms:visible').hide();
            $('#customTypeForm_' + $(this).val()).show();
        })
    })
</script>