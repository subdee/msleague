<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,'enableClientValidation'=>true,
    'enableClientValidation'=>true,
    'focus'=>array($user,'username'),
)); ?>

    <div class="row">
        <?php echo $form->labelEx($user,'username'); ?>
        <?php echo $form->textField($user,'username'); ?>
        <?php echo $form->error($user,'username'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($user,'password'); ?>
        <?php echo $form->passwordField($user,'password'); ?>
        <?php echo $form->error($user,'password'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($user,'role'); ?>
        <?php echo $form->dropDownList($user,'role',AdminRole::getRoles()); ?>
        <?php echo $form->error($user,'role'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Save'); ?>
        <?php echo CHtml::link('Cancel',AdminUtils::aUrl('user/index')); ?>
    </div>

<?php $this->endWidget(); ?>

</div>