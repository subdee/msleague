<a href="<?php echo AdminUtils::aUrl('user/add'); ?>"><div class="addButton">Add new user</div></a>
<br /><br />
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $users->search(),
    'pager' => array('class' => 'CLinkPager', 'header' => ''),    'columns' => array(
//        array(
//            'name'=>'username'
//        ),
        array(
            'name' => 'username',
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'name' => 'role',
            'value' => 'AdminRole::getRoles($data->role)',
            'htmlOptions' => array('class' => 'tcenter')
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}{delete}',
            'updateButtonImageUrl' => AdminUtils::adminImageUrl('edit.png'),
            'deleteButtonImageUrl' => AdminUtils::adminImageUrl('delete.png'),
        ),
    ),
));
?>