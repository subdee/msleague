MSL.Advisor = {
    flashing : true,
    visible : false,
    object : null,
    Toggle : function() {
        if(MSL.Advisor.visible) {
            MSL.Advisor.Hide();
        }
        else {
            MSL.Advisor.Show();
        }
    },
    Hide : function() {
        var balloon = $('.advisor-balloon');
        balloon.hide();
        MSL.Advisor.visible = false;
        $(document).trigger('onAdvisorHide');
    },
    Show : function() {
        var balloon = $('.advisor-balloon');
        var position = $('.training-advisor .body').offset();

        balloon
        .show()
        .css({
            'left' : position.left - balloon.width() + 29,
            'top' : position.top - ( balloon.height() - 70 )
        });
        $('.tail', balloon)
        .offset({
            'left' : position.left + 28,
            'top' : position.top
        })
        ;

        MSL.Advisor.visible = true;
        if(MSL.Advisor.flashing)
            MSL.Advisor.object.stop(true, true).css({
                opacity : 1
            });
        MSL.Advisor.flashing = false;
        $(document).trigger('onAdvisorShow', [MSL.Advisor.object]);
    }
};

$(document).ready(function(){
    // Bind event
    MSL.Advisor.object = $('.training-advisor .image img');
    MSL.Advisor.object.click(function(){
        MSL.Advisor.Toggle();
        $(document).trigger('onAdvisorClick', [MSL.Advisor.object]);
    });

    // Initial effect
    if(MSL.Advisor.flashing)
        MSL.Advisor.object.effect("pulsate", {}, 2000);
});

$(window).load(function(){
    if(MSL.Advisor.visible) {
        MSL.Advisor.Show();
        $('.advisor-balloon .notice').show();
    } else {
        MSL.Advisor.Hide();
    }
});

$(window).resize(function(){
    if(MSL.Advisor.visible) {
        MSL.Advisor.Show();
    } else {
        MSL.Advisor.Hide();
    }
});