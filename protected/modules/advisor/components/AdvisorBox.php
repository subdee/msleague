<?php

class AdvisorBox extends WidgetBox {

    /**
     * @var array Type of message and content.
     */
    public $message;

    /**
     * @var bool Whether it should be showing on page load or not.
     */
    public $show = true;

    /**
     * @var bool Whether it should be flashing on page load or not.
     */
    public $flashing = true;

    /**
     * @var bool Is advisor active (clickable)
     */
    public $active = true;

    public function init() {
        $this->view = 'advisorBox';
        $this->name = 'training-advisor';
        $this->title = _t('Advisor');
        $this->place = 'sidebar';
        $this->widgetView = 'application.components.views.widgetBox';

        parent::init();

        Utils::registerModuleCssFile('advisor', 'advisor');
        Utils::registerModuleJsFile('advisor', 'advisor');
    }

}

?>