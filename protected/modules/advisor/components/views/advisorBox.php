<span class="image">
    <?php echo CHtml::image(Utils::moduleImageUrl('advisor', 'advisor.png')); ?>
</span>
<div class="advisor-balloon hidden">
    <div class="frame">
        <?php
        if (isset($this->message['view']))
            $this->render($this->message['view']);
        elseif (isset($this->message['text']))
            echo $this->message['text'];
        ?>
    </div>
    <div class="tail"></div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        MSL.Advisor.flashing = <?php echo CJavaScript::encode($this->flashing); ?>;
        MSL.Advisor.visible = <?php echo CJavaScript::encode($this->show); ?>;
    });
</script>