<?php

class CreditsModule extends WebModule {

    /**
     *
     */
    public function init() {
        parent::init();

        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'credits.models.*',
            'credits.components.*',
        ));

        // Register events
        Utils::registerCallback('onManagerBoxMenuTop', $this, 'renderMenuItem');
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        }
        else
            return false;
    }

    public function renderMenuItem(CEvent $e) {
        $e->sender->render('credits.views._managerBoxMenuItem', array(
            'credits' => CreditsManager::model()->credits(_manager()->id),
        ));
    }

}
