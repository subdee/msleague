<?php

class DefaultController extends Controller {
    
    public function filters() {
        return array('accessControl',);
    }

    public function accessRules() {
        return array(
            array('allow',
                'expression' => '_manager()->hasRole(Role::MANAGER)',
            ),
            array('deny',
                'users' => array('*')
            )
        );
    }
    
    public function actionPackages() {
        $packages = CreditsPackage::model()->available()->findAll();
        $this->render('packages',array(
            'packages' => $packages
        ));
    }
}