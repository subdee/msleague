<?php

/**
 * This is the model class for table "mod_credits_manager".
 *
 * The followings are the available columns in table 'mod_credits_manager':
 * @property integer $manager_id
 * @property integer $credits
 *
 * The followings are the available model relations:
 * @property Manager $manager
 */
class CreditsManager extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return CreditsManager the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'mod_credits_manager';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('manager_id', 'required'),
            array('manager_id, credits', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('manager_id, credits', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'manager' => array(self::BELONGS_TO, 'Manager', 'manager_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'manager_id' => 'Manager',
            'credits' => 'Credits',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('manager_id', $this->manager_id);
        $criteria->compare('credits', $this->credits);

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
    }

    /**
     * @param int $manager_id
     * @return int The credits of a manager.
     */
    public function credits($manager_id) {
        $record = $this->findByAttributes(array('manager_id' => $manager_id));
        if ($record)
            return $record->credits;

        return 0;
    }

}