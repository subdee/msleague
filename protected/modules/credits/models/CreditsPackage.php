<?php

/**
 * This is the model class for table "mod_credits_package".
 *
 * The followings are the available columns in table 'mod_credits_package':
 * @property integer $id
 * @property string $name
 * @property integer $credits
 * @property integer $available
 */
class CreditsPackage extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return CreditsPackage the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'mod_credits_package';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('credits, available', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 30),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, credits, available', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'credits' => 'Credits',
            'available' => 'Available',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('credits', $this->credits);
        $criteria->compare('available', $this->available);

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
    }

    public function scopes() {
        return array(
            'available' => array(
                'condition' => 'available'
            )
        );
    }
}