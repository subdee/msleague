<?php

class CustomLeagueModule extends WebModule {

    public function init() {
        parent::init();

        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'customLeague.models.*',
            'customLeague.models.features.*',
            'customLeague.components.*',
        ));

        // Load module configuration
        CustomLeagueConfig::model()->attributes = CustomLeagueConfig::model()->find()->attributes;

        // Attach module events to system
        _app()->eventSystem->attachBehavior('CustomLeagueModuleEvents', new CustomLeagueModuleEvents);

        // Register events
        Utils::registerCallback('onTopMenuEnd', $this, 'renderMenuItem');
        Utils::registerCallback('onAfterProcessTeam', $this, 'addNewManagerToFanLeague');
        Utils::registerCallback('onManagerDelete', $this, 'deleteManagerLeagues');

        // Load child modules
        $this->preloadModules();
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        }
        else
            return false;
    }

    /**
     * Renders the module's menu item.
     * @param CEvent $e
     */
    public function renderMenuItem(CEvent $e) {
        $e->sender->pushItem(array(
            'url' => 'customLeague/index',
            'img' => array('customLeague', 'cup.png'),
            'title' => _t('Leagues')
        ));
    }

    /**
     * @param CEvent $e
     */
    public function addNewManagerToFanLeague(CEvent $e) {
        $league = CustomLeagueFan::model()->findByAttributes(array('team_id' => _manager()->profile->team_id));
        if ($league)
            $league->league->addManager(_manager()->id);
        else
            throw new CException('There is no fan league for this team: ' . _manager()->profile->team->name);
    }

    /**
     * @param CEvent $e
     */
    public function deleteManagerLeagues(CEvent $e) {
        $leagues = CustomLeague::model()->findAllByAttributes(array('creator_id' => $e->params['manager_id']));
        foreach ($leagues as $league) {
            $league->delete();
        }
    }

}
