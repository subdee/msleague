<?php

class LeagueTable extends CWidget {

    /**
     * @var string Table id.
     */
    public $id;

    /**
     * @var string Table header.
     */
    public $header;

    /**
     * @var CustomLeague The league search model.
     */
    public $leagues;

    /**
     * @var int Maximum number of leagues to show. Zero for all.
     */
    public $maxShow = 0;

    /**
     * @var string Message for empty table.
     */
    public $emptyTableMessage;

    /**
     * @var boolean Whether to use search.
     */
    public $search = false;

    /**
     * @var string Default message to appear in filter box.
     */
    public $emptySearchMessage = null;

    /**
     * @var boolean Whether to show the new league button.
     */
    public $showNewLeagueButton = false;

    /**
     * @var string
     */
    public $newLeagueButtonText = null;

    /**
     * @var boolean
     */
    public $showLastJoined = false;

    /**
     * @var boolean
     */
    private $_canCreateNewLeague = CustomLeague::ERROR;

    /**
     * @var CActiveDataProvider
     */
    private $_dataProvider;

    /**
     * @var int The league count.
     */
    private $_leagueCount;

    /**
     *
     */
    public function init() {
        if (!$this->emptySearchMessage) {
            $this->emptySearchMessage = _t('Search leagues...');
        }
        if (!$this->newLeagueButtonText) {
            $this->newLeagueButtonText = _t('+ New League');
        }

        $this->_dataProvider = $this->leagues->search();
        $this->_leagueCount = count($this->_dataProvider->data);
    }

    /**
     *
     */
    public function run() {
        $this->render('leagueTable', array(
            'filterValue' => isset($_GET['CustomLeague']) ? $this->leagues->name : $this->emptySearchMessage,
        ));
    }

    /**
     * @return string Html for new league button.
     */
    public function newLeagueButtonHtml() {
        if (!$this->showNewLeagueButton)
            return;

        $this->_canCreateNewLeague = CustomLeague::model()->canCreate(_manager());
        if ($this->canCreateNewLeague()) {
            return _l($this->newLeagueButtonText, '#', array(
                    'class' => 'new-league-button',
                    'onclick' => CJavaScript::encode("js:$('#new-league-dialog').dialog('open'); return false;"),
                ));
        }
        return CHtml::tag('span', array(
                'class' => 'new-league-button',
                'title' => CustomLeague::model()->cannotCreateLeagueReasons($this->_canCreateNewLeague)
                ), $this->newLeagueButtonText
        );
    }

    /**
     * @return boolean
     */
    public function canCreateNewLeague() {
        return $this->_canCreateNewLeague == CustomLeague::SUCCESS;
    }

    /**
     * @return CActiveDataProvider
     */
    public function dataProvider() {
        return $this->_dataProvider;
    }

    /**
     * @return int The league count
     */
    public function leagueCount() {
        return $this->_leagueCount;
    }

    /**
     * @param CustomLeagueManager $lastManager
     * @return string
     */
    public function lastManagerJoinedHtml($lastManager) {
        if ($lastManager)
            return CHtml::tag('div', array('class' => 'last-joined'), _t('Last joined: {manager}', array('{manager}' => $lastManager->manager->profileLink())));

        return '';
    }

    /**
     *
     * @param type $data
     * @param type $row
     */
    protected function renderLeagueDetails($data, $row) {
        echo CHtml::tag("div", array("class" => "name"), $data->link());
        echo CHtml::tag("div", array("class" => "members"), _t("{n} member|{n} members", array($data->numOfMembers())));
        echo $this->showLastJoined ? $this->lastManagerJoinedHtml($data->lastManagerJoined()) : "";
    }

}

?>