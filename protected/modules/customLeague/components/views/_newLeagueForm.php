<?php
$this->widget('Notice', array('id' => 'newLeagueNotice', 'ajax' => true));

$newLeague = new CustomLeague;
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'new-league-form',
    'action' => _url('customLeague/create'),
    'htmlOptions' => array(
        'onSubmit' => 'addLeague(); return false;',
        'class' => 'popup new-league',
    ),
    'focus' => array($newLeague, 'name')
    ));
?>
<div class="row">
    <?php echo $form->labelEx($newLeague, 'name'); ?>
    <?php echo $form->textField($newLeague, 'name'); ?>
    <?php echo $form->error($newLeague, 'name'); ?>
</div>
<div class="row">
    <?php
    echo $form->labelEx($newLeague, 'privacy');
    echo $form->dropDownList($newLeague, 'privacy', CustomLeaguePrivacy::toArray());
    echo $form->error($newLeague, 'privacy');
    ?>
</div>
<?php echo $form->hiddenField($newLeague, 'type'); ?>
<?php $this->endWidget(); ?>

<script type="text/javascript" >
    function addLeague() {
<?php
echo CHtml::ajax(array(
    'url' => array('create'),
    'data' => "js:$('#new-league-form').serialize()",
    'type' => 'post',
    'dataType' => 'json',
    'success' => "function(response) {
        if(response.status == 'success') {
            MSL.Redirect(response.redirectUrl);
        } else {
            $('#newLeagueNotice').notice(response);
        }
    }",
));
?>
    }
</script>
