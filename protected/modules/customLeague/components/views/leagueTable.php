<?php

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => $this->id,
    'dataProvider' => $this->dataProvider(),
    'filter' => $this->search ? $this->leagues : null,
    'template' => '{items}{pager}',
    'emptyText' => $this->emptyTableMessage,
    'htmlOptions' => array('class' => 'grid-view league-table'),
    'filterPosition' => 'header',
    'rowCssClassExpression' => '(($this->owner->maxShow > 0) ? (($row >= $this->owner->maxShow) ? (($row&1 ? "even" : "odd") . " hidden") : ($row&1 ? "even" : "odd")) : ($row&1 ? "even" : "odd"))',
    'pager' => array(
        'header' => '',
    ),
    'columns' => array(
        array(
            'header' => $this->header . $this->newLeagueButtonHtml(),
            'type' => 'raw',
            'value' => array($this, 'renderLeagueDetails'),
            'filter' => CHtml::textField('CustomLeague[name]', $filterValue, array('maxlength' => 50)),
            'cssClassExpression' => 'Utils::isLoggedIn() ? ($data->isCreator(_manager()->id)  ? "creator" : "") : ""',
        ),
    )
));

//WTF is this shit. PoEdit must find this until we change the way GridView evals
_t("{n} member|{n} members");

if ($this->maxShow > 0 && $this->leagueCount() > $this->maxShow) {
    echo CHtml::tag('div', array('class' => 'infolink more'), _l(_t('Show all...'), '#'));
    _cs()->registerScript('fanLeagueMore', "
        $('.infolink.more a').click(function(){
            $('#fan-leagues tr:hidden').show('blind');
            $(this).parent().hide();
            return false;
            });
            ", CClientScript::POS_READY
    );
}

if ($this->canCreateNewLeague()) {
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id' => 'new-league-dialog',
        'options' => array(
            'title' => _t('Create a new league'),
            'autoOpen' => false,
            'width' => 500,
            'modal' => true,
            'draggable' => false,
            'resizable' => false,
            'buttons' => array(
                _t('Add league') => 'js:addLeague',
                _t('Cancel') => 'js:function(){$(this).dialog("close");}',
            ),
        ),
    ));
    $this->render('_newLeagueForm');
    $this->endWidget('zii.widgets.jui.CJuiDialog');
}
?>