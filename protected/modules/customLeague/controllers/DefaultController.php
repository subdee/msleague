<?php

class DefaultController extends Controller {

    public function filters() {
        return array('accessControl',);
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index'),
                'users' => array('*'),
            ),
            array('deny',
                'users' => array('?'),
            ),
            array('allow',
                'actions' => array('league'),
                'users' => array('@'),
            ),
            array('deny',
                'expression' => '_manager()->hasRole(Role::TEMPORARY)',
            ),
            array('allow',
                'expression' => '_manager()->hasRole(Role::MANAGER | Role::RESET)',
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function init() {
        parent::init();
        Utils::registerModuleCssFile('customLeague', 'styles');
    }

    /**
     *
     */
    public function actionIndex() {
        _setPageTitle(_t('Leagues'));

        if (CustomLeague::model()->canSeeUserLeagues()) {
            $all = new CustomLeague('searchLeaguesFrontend');
            $myLeagues = new CustomLeague('searchLeaguesParticipating');
        } else {
            $all = new CustomLeague('search');
            $myLeagues = null;
        }

        $all->privacy = array(CustomLeaguePrivacy::TYPE_PUBLIC, CustomLeaguePrivacy::TYPE_PRIVATE);

        if (isset($_GET['CustomLeague'])) {
            $all->attributes = $_GET['CustomLeague'];
        }

        $this->render('index', array(
            'allLeagues' => $all,
            'myLeagues' => $myLeagues,
        ));
    }

    /**
     *
     */
    public function actionCreate() {
        if (!Utils::isAjax())
            return;

        $newLeague = new CustomLeague;
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'new-league') {
            echo CActiveForm::validate($newLeague, array('name', 'type'));
            _app()->end();
        }
        if (isset($_POST['CustomLeague'])) {

            $ret = $newLeague->canCreate(_manager());
            if (CustomLeague::SUCCESS == $ret) {
                if ($newLeague->create($_POST['CustomLeague'])) {
                    echo CJSON::encode(array(
                        'status' => 'success',
                        'redirectUrl' => _url('customLeague/league', array('id' => $newLeague->id)),
                    ));
                } else {
                    echo CJSON::encode(array(
                        'message' => nl2br(Debug::getSessionError()),
                        'type' => 'error'
                    ));
                }
                _app()->end();
            }

            echo CJSON::encode(array(
                'message' => CustomLeague::model()->cannotCreateLeagueReasons($ret),
                'type' => 'error'
            ));
            _app()->end();
        }
    }

    /**
     *
     */
    public function actionLeague($id) {
        Utils::registerJsFile('lib/jquery.jeditable.mini');

        $league = CustomLeague::model()->findByPk($id);
        if (!$league || !$league->isAccessibleBy(_manager()->id)) {
            throw new CHttpException('404');
        }

        $managers = new CustomLeagueManager('search');
        $managers->league_id = $id;

        _setPageTitle(_t('League - {league}', array('{league}' => $league->name)));

        $league->readMetadata();

        $this->render('league', array(
            'league' => $league,
            'managers' => $managers,
        ));
    }

    /**
     *
     */
    public function actionKick($league, $manager) {
        $league_id = $league;
        $league = CustomLeague::model()->findByPk($league_id);
        if ($league) {
            if ($league->kick($manager)) {
                $username = Manager::model()->findByPk($manager)->profileLink();
                Utils::notice('league', _t('You have kicked {manager} from the league.', array(
                        '{manager}' => $username
                    )), 'success', 'small'
                );
            } else {
                Utils::notice('league', _t('Failed to kick manager from league.'), 'error');
            }
        }

        _redirect('customLeague/league', array('id' => $league_id));
    }

    /**
     *
     */
    public function actionDelete($id) {
        $league = CustomLeague::model()->findByPk($id);
        if ($league) {
            $league->deleteLeague();
            Utils::notice('leagues', _t('You have deleted the league "{league}".', array(
                    '{league}' => $league->name,
                )), 'success', 'small'
            );
        }

        $this->redirect('index');
    }

    /**
     *
     */
    public function actionJoin($id) {
        $league = CustomLeague::model()->findByPk($id);
        if ($league) {
            if ($league->canJoin(_manager()) > 0) {
                if ($league->join(_manager()->id)) {
                    Utils::notice('league', _t('You have joined the league.'), 'success', 'small');
                }
            }
        }

        _redirect('customLeague/league', array('id' => $id));
    }

    /**
     *
     */
    public function actionLeave($id) {
        $league = CustomLeague::model()->findByPk($id);
        if ($league) {
            if ($league->canLeave(_manager())) {
                if ($league->leave(_manager()->id)) {
                    Utils::notice('leagues', _t('You have left the league {league}.', array('{league}' => $league->link())), 'success', 'small');
                    _redirect('customLeague/index');
                }
            }
        }

        Utils::notice('league', _t('An error occured. Please try again.'), 'error');
        _redirect('customLeague/league', array('id' => $id));
    }

    /**
     *
     */
    public function actionEdit() {
        if (!Utils::isAjax())
            return;

        if (!isset($_POST['id']))
            return;

        $league = CustomLeague::model()->findByPk($_POST['league']);
        if (!$league)
            return;

        $attribute = $_POST['id'];
        $oldValue = $league->$attribute;

        if ($league->$attribute != $_POST['value']) {

            if ($attribute == 'name') {
                if (!Utils::isPhraseAllowed($_POST['value'])) {
                    echo $oldValue;
                    _app()->end();
                }
            }

            $league->$attribute = $_POST['value'];
            if (!$league->save(true, array($attribute))) {
                Debug::logToWindow($league->getErrors());
            }

            $league->refresh();
        }


        switch ($attribute) {
            case 'privacy':
                echo _t('{type} league', array('{type}' => $league->privacyToText()));
                break;

            default:
                echo $league->$attribute;
                break;
        }

        _app()->end();
    }

    /**
     *
     */
    public function actionLeaguePrivacy() {
        if (!Utils::isAjax())
            return;

        if (!isset($_GET['id']))
            return;

        $league = CustomLeague::model()->findByPk($_GET['league']);
        if (!$league)
            return;

        Utils::jsonReturn(CustomLeaguePrivacy::toArray() + array('selected' => $league->privacy));
    }

}