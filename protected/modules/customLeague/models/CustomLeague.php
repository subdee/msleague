<?php

/**
 * This is the model class for table "mod_custom_leagues".
 *
 * The followings are the available columns in table 'mod_custom_leagues':
 * @property integer $id
 * @property string $date_created
 * @property integer $creator_id
 * @property string $name
 * @property integer $capacity
 * @property integer $type
 * @property boolean $open
 * @property integer $privacy
 * @property string $metadata
 *
 * The followings are the available model relations:
 * @property Manager $creator
 * @property CustomLeagueManager[] $members
 */
class CustomLeague extends CActiveRecord {
    /**
     * Custom league default capacity.
     */
    const CAPACITY_UNLIMITED = 0;

    /**
     * Custom league default creator.
     */
    const CREATOR_SYSTEM = null;

    /**
     * Return code for success.
     */
    const SUCCESS = 1;

    /**
     * Return code for error.
     */
    const ERROR = -1;

    /**
     * User cannot create, join, edit, leave or delete a league as he is banned.
     */
    const ERR_BANNED = -2;

    /**
     * User cannot create or join a new league as he has reached the maximum number of leagues he can be a part of.
     */
    const ERR_MAX_LEAGUES_REACHED = -3;

    /**
     * User cannot join a full league.
     */
    const ERR_NO_FREE_SPACES = -4;

    /**
     * User cannot create, join a league as he is banned.
     */
    const ERR_RESET = -5;

    /**
     * Manager has already joined the league.
     */
    const ERR_ALREADY_JOINED = -6;

    /**
     * Manager tried to join a fan league of another team.
     */
    const ERR_INVALID_FAN_LEAGUE = -7;

    /**
     * @var int The number of league members.
     */
    public $numOfMembers;

    /**
     * @var string Used for validation.
     */
    public $creatorUsername;

    /**
     * @var array The feature attributes is populated from metadata.
     */
    public $features = array();

    /**
     * Returns the static model of the specified AR class.
     * @return CustomLeague the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'mod_custom_leagues';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('date_created, name, capacity, type, open, privacy', 'required'),
            array('creator_id, capacity, type', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 50, 'min' => 3, 'tooShort' => _t('The league name must be at least {min} characters.')),
            array('name', 'unique', 'className' => 'CustomLeague', 'message' => _t('There is already a league with that name.')),
            array('open', 'boolean'),
            array('creatorUsername, capacity', 'validateLeague'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, date_created, creator_id, name, capacity, type, open, privacy', 'safe', 'on' => 'search'),
            array('creatorUsername, features', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'creator' => array(self::BELONGS_TO, 'Manager', 'creator_id'),
            // We brind the members with correct ordering (by total_points and join_date)
            'members' => array(
                self::HAS_MANY,
                'CustomLeagueManager',
                'league_id',
                'condition' => 'manager.role in (:manager, :reset)',
                'params' => array(':manager' => Role::MANAGER, ':reset' => Role::RESET),
                'with' => array(
                    'manager' => array('select' => 'username, total_points'),
                    'manager.managerTeam' => array('select' => 'created_on')
                ),
                'order' => 'total_points desc, managerTeam.created_on asc'
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'date_created' => _t('Date Created'),
            'creator_id' => _t('Creator'),
            'creatorUsername' => _t('Creator'),
            'name' => _t('Name'),
            'capacity' => _t('Capacity'),
            'type' => _t('Type'),
            'privacy' => _t('Privacy'),
            'open' => _t('Open'),
        );
    }

    /**
     *
     */
    public function validateLeague($attribute, $params) {
        if ($this->hasErrors())
            return;

        if ($this->type == CustomLeagueType::USER) {

            if ($attribute == 'creatorUsername' && $this->creator_id === NULL) {
                // Check if manager exists
                $manager = Manager::model()->find('username = :mun', array(':mun' => $this->creatorUsername));
                if (!$manager) {
                    $this->addError('creatorUsername', _t('Manager {username} does not exist.', array('{username}' => CHtml::tag('em', array(), $this->creatorUsername))));
                    return;
                } elseif (!$this->canJoinMoreLeagues($manager->id)) {
                    $this->addError('creatorUsername', _t('Manager {username} cannot join more leagues.', array('{username}' => CHtml::tag('em', array(), $this->creatorUsername))));
                    return;
                }
            } elseif ($this->creator_id !== NULL) {
                // Check if manager exists
                $manager = Manager::model()->findByPk($this->creator_id);
                if (!$manager) {
                    $this->addError('creator_id', _t('Manager {id} does not exist.', array('{id}' => CHtml::tag('em', array(), $this->creator_id))));
                    return;
                } elseif (!$this->canJoinMoreLeagues($manager->id)) {
                    $this->addError('creator_id', _t('Manager {username} cannot join more leagues.', array('{username}' => CHtml::tag('em', array(), $manager->username))));
                    return;
                }
            }
        }

        if ($attribute == 'capacity' && $this->capacity != self::CAPACITY_UNLIMITED && CustomLeagueConfig::model()->maxManagersPerLeague != 0) {
            if ($this->capacity > CustomLeagueConfig::model()->maxManagersPerLeague) {
                $this->addError('capacity', _t('Max league capacity is {max}.', array('{max}' => CustomLeagueConfig::model()->maxManagersPerLeague)));
                return;
            }
        }
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        // Order by date created
        $criteria->order = 'date_created desc';

        switch ($this->getScenario()) {

            case 'searchLeaguesParticipating':
                // filter out leagues created by manager and leagues that he does not participate in
                $criteria->select = 't.id, t.name, (SELECT COUNT(manager_id) FROM mod_custom_leagues_managers mclm LEFT JOIN manager m ON m.id = mclm.manager_id WHERE league_id = t.id AND m.role in (:manager, :reset)) AS numOfMembers';
                $criteria->join = 'LEFT JOIN mod_custom_leagues_managers lm ON lm.league_id = t.id';
                $criteria->addCondition('manager_id = :mid');
                $criteria->params[':mid'] = _manager()->id;
                $criteria->params[':manager'] = Role::MANAGER;
                $criteria->params[':reset'] = Role::RESET;
                $criteria->order = 'join_date DESC';
                $criteria->group = 't.id';
                break;

            case 'search':

                $numOfMembersSql = <<<SQL
                SELECT COUNT(manager_id)
                  FROM mod_custom_leagues_managers lm2
             LEFT JOIN manager m ON m.id = lm2.manager_id
                 WHERE league_id = t.id
                   AND m.role IN (:manager, :reset)
SQL;
                $criteria->params[':manager'] = Role::MANAGER;
                $criteria->params[':reset'] = Role::RESET;

                $criteria->select = "t.id, t.name, ($numOfMembersSql) AS numOfMembers";
                $criteria->join = 'LEFT JOIN mod_custom_leagues_managers lm ON lm.league_id = t.id ';
                $criteria->join.= 'LEFT JOIN manager m ON m.id = lm.manager_id';
                $criteria->group = 't.name';
                $criteria->order = 'numOfMembers DESC';

                $criteria->addInCondition('privacy', $this->privacy);

                break;

            case 'searchLeaguesFrontend':
                // filter out leagues that are private and manager is neither creator nor member

                $numOfMembersSql = <<<SQL
                SELECT COUNT(manager_id)
                  FROM mod_custom_leagues_managers lm2
             LEFT JOIN manager m ON m.id = lm2.manager_id
                 WHERE league_id = t.id
                   AND m.role IN (:manager, :reset)
SQL;
                $criteria->params[':manager'] = Role::MANAGER;
                $criteria->params[':reset'] = Role::RESET;

                $criteria->select = "t.id, t.name, ($numOfMembersSql) AS numOfMembers";
                $criteria->addInCondition('privacy', array(CustomLeaguePrivacy::TYPE_PUBLIC));
                $criteria->group = 't.name';
                $criteria->order = 'numOfMembers DESC';
                break;

            default:
                $criteria->compare('creator_id', $this->creator_id);
        }

        $criteria->compare('id', $this->id);
        $criteria->compare('date_created', $this->date_created, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('capacity', $this->capacity);
        $criteria->compare('type', $this->type);

        $pagination = array(
            'pageSize' => 20
        );
        if ($this->type == CustomLeagueType::TEAM) {
            $pagination = false;
        }

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => $pagination,
                'sort' => false,
            ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchAdmin() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('date_created', $this->date_created, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('capacity', $this->capacity);
        $criteria->compare('type', $this->type);
//        $criteria->compare('open', $this->open);
        $criteria->compare('privacy', $this->privacy);
        $criteria->compare('creator.username', $this->creator_id, true);

        $criteria->with = array(
            'creator' => array('select' => 'username'),
        );

        $numOfMembersSql = <<<SQL
                SELECT COUNT(lm2.manager_id)
                  FROM mod_custom_leagues_managers lm2
             LEFT JOIN manager m ON m.id = lm2.manager_id
                 WHERE league_id = t.id
SQL;
        $criteria->select = "*, ($numOfMembersSql) as numOfMembers";

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 20
                ),
                'sort' => array(
                    'attributes' => array(
                        'date_created' => array(
                            'asc' => 'date_created',
                            'desc' => 'date_created desc'
                        ),
                        'capacity' => array(
                            'asc' => 'capacity',
                            'desc' => 'capacity desc'
                        ),
                        'numOfMembers' => array(
                            'asc' => 'numOfMembers',
                            'desc' => 'numOfMembers desc'
                        ),
                    ),
                    'defaultOrder' => array(
                        'numOfMembers' => true,
                    )
                ),
            ));
    }

    /**
     * @return boolean
     */
    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->date_created = Utils::fromLocalDatetime($this->date_created);
            return true;
        }
        return false;
    }

    /**
     *
     */
    protected function afterFind() {
        parent::afterFind();
        $this->date_created = Utils::toLocalDatetime($this->date_created);
    }

    /**
     * Override league deletion.
     */
    public function delete() {
        Utils::triggerEvent('onCustomLeagueDelete', $this, array('league_id' => $this->id));
        parent::delete();
    }

    /**
     * Deletes the league.
     */
    public function deleteLeague() {
        try {
            $trans = _app()->db->beginTransaction();
            $this->delete();
            $trans->commit();
        } catch (CDbException $e) {
            Debug::logToWindow($e->getMessage(), 'delete league');
            $trans->rollback();
        } catch (CValidateException $e) {
            $trans->rollback();
        }
    }

    /**
     * @return int The number of members for this league.
     */
    public function numOfMembers() {
        if ($this->numOfMembers === NULL) {

//            $this->numOfMembers = count($this->members);
            // 100 times faster!
            $this->numOfMembers = CustomLeagueManager::model()->count(array(
                'condition' => 'league_id = :lid',
                'params' => array(':lid' => $this->id),
                ));
        }
        return $this->numOfMembers;
    }

    /**
     * @param int $manager_id
     * @return boolean Whether the manager already participates.
     */
    public function isMember($manager_id) {

//        foreach ($this->members as $p) {
//            if ($p->manager_id == $manager_id) {
//                return true;
//            }
//        }
//        return false;
        // 100 times faster!
        return CustomLeagueManager::model()->exists(array(
                'condition' => 'league_id = :lid AND manager_id = :mid',
                'params' => array(
                    ':lid' => $this->id,
                    ':mid' => $manager_id,
                )
            ));
    }

    /**
     * @param Manager $manager
     * @return boolean Whether the manager meets the conditions.
     */
    public function canJoin(Manager $manager) {
        if ($this->type == CustomLeagueType::TEAM && !$this->_belongsToFanLeague($manager))
            return self::ERR_INVALID_FAN_LEAGUE;

        if ($manager->isBanned())
            return self::ERR_BANNED;

        if (!$this->hasFreeSpaces())
            return self::ERR_NO_FREE_SPACES;

        if ($this->isMember($manager->id))
            return self::ERR_ALREADY_JOINED;

        if (!$this->canJoinMoreLeagues($manager->id))
            return self::ERR_MAX_LEAGUES_REACHED;

        foreach ($this->features as $f) {
            if (!$f->canJoin($manager)) {
                return self::ERROR;
            }
        }

        return self::SUCCESS;
    }

    /**
     * @param int $manager_id
     * @return boolean
     */
    public function canJoinMoreLeagues($manager_id) {
        return ($this->numOfLeagesForManager($manager_id) < CustomLeagueConfig::model()->maxLeaguesPerManager);
    }

    /**
     * @param int $manager_id
     * @return boolean Whether the manager meets the conditions.
     */
    public function canViewJoin($manager_id) {
        if ($this->isMember($manager_id))
            return false;

        return true;
    }

    /**
     * @param Manager $manager
     * @return boolean Whether the manager meets the conditions.
     */
    public function canLeave(Manager $manager) {
        if ($manager->isBanned())
            return false;

        if (!$this->isMember($manager->id))
            return false;

        if ($this->creator_id == $manager->id)
            return false;

        return true;
    }

    /**
     * @param int $manager_id
     * @return boolean Whether the manager meets the conditions.
     */
    public function canViewLeave($manager_id) {
        if (!$this->isMember($manager_id))
            return false;

        if ($this->creator_id == $manager_id)
            return false;

        return true;
    }

    /**
     * @param Manager $manager
     * @return boolean Whether the manager meets the conditions.
     */
    public function canDelete(Manager $manager) {
        if ($manager->isBanned())
            return false;

        if ($this->creator_id != $manager->id)
            return false;

        return true;
    }

    /**
     * @param int $manager_id
     * @return boolean
     */
    public function canViewDelete($manager_id) {
        if ($this->creator_id != $manager_id)
            return false;

        return true;
    }

    /**
     * @param Manager $manager
     * @return boolean Whether the manager meets the conditions.
     */
    public function canEdit(Manager $manager) {
        if ($manager->isBanned())
            return false;

        if ($this->creator_id != $manager->id)
            return false;

        return true;
    }

    /**
     * @param int $manager_id
     * @return boolean Whether the manager meets the conditions.
     */
    public function canKick($manager_id) {
        return ($this->creator_id == $manager_id) && !$this->creator->isBanned();
    }

    /**
     * @param int $manager_id
     * @return boolean Whether the manager meets the conditions.
     */
    public function canBeKicked($manager_id) {
        return ($this->creator_id != $manager_id);
    }

    /**
     * @param Manager $manager
     * @return boolean Whether the manager meets the conditions.
     */
    public function canCreate($manager) {
        if ($manager->isBanned())
            return self::ERR_BANNED;

        if (!$this->canJoinMoreLeagues($manager->id))
            return self::ERR_MAX_LEAGUES_REACHED;

        return self::SUCCESS;
    }

    /**
     * @param int $manager_id
     * @return boolean Whether the manager can and enter the league.
     */
    public function isAccessibleBy($manager_id) {
        // Public are visible to all.
        if ($this->privacy == CustomLeaguePrivacy::TYPE_PUBLIC)
            return true;

        // Fan leagues are visible to all.
        if ($this->type == CustomLeagueType::TEAM)
            return true;

        // Private can be seen by owners ...
        if ($this->isCreator($manager_id))
            return true;

        // ... and members.
        if ($this->isMember($manager_id))
            return true;

        return false;
    }

    /**
     * @param int $manager_id
     * @return int Number of leages the manager participates in.
     */
    public function numOfLeagesForManager($manager_id) {
        return CustomLeagueManager::model()->countByAttributes(array('manager_id' => $manager_id));
    }

    /**
     * @param int $manager_id
     * @return int Manager's rank for this league.
     */
    public function rankForManager($manager_id) {
//        $found = false;
//        $rank = 1;
//        foreach ($this->members as $p) {
//            if ($p->manager_id == $manager_id) {
//                $found = true;
//                break;
//            }
//            $rank++;
//        }
//        return $found ? $rank : 0;

        // A very scary query, but its only for one manager @ a time (at least i hope so).
        // Nevertheless, much faster than the above.
        // 1. Selects all managers of given league
        // 2. Orders the set giving a rank for every manager
        // 3. Selects a single manager
        
        $sql = <<<SQL
        SELECT rank
        FROM (
            SELECT @rownum := @rownum + 1 AS rank, manager_id
            FROM (
                SELECT total_points, created_on, manager_id
                FROM mod_custom_leagues_managers
                LEFT OUTER JOIN manager m on m.id = manager_id
                LEFT OUTER JOIN manager_team mt on m.manager_team_id = mt.id
                WHERE league_id = :lid
            ) foo,
            (SELECT @rownum := 0) fee
            ORDER BY total_points DESC, created_on ASC
        ) faa
        WHERE manager_id = :mid
SQL;

        $command = _app()->db->createCommand($sql);
        $command->bindValue(':mid', $manager_id);
        $command->bindValue(':lid', $this->id);
        return $command->queryScalar();
    }

    /**
     * Creates a new league
     * @param array $attributes
     * @return boolean Whether the league was successfully added.
     */
    public function create($attributes) {
        try {
            $transaction = _app()->db->beginTransaction();

            // Create league
            $this->attributes = $attributes;
            $this->date_created = _app()->localtime->getLocalNow();
            $this->capacity = CustomLeagueConfig::model()->maxManagersPerLeague;
            $this->creator_id = _manager()->id;
            $this->type = CustomLeagueType::USER;
            if (!$this->save())
                throw new CValidateException($this);

            // Add league creator as first manager in the league
            $this->addManager($this->creator_id);

            Utils::triggerEvent('onCustomLeagueCreate', $this, array('newLeague' => $this));

            $transaction->commit();
            return true;
        } catch (CDbException $e) {
            Debug::logToWindow($e->getMessage(), 'create league');
            Debug::setSessionError($e->getMessage());
            $transaction->rollback();
        } catch (CValidateException $e) {
            Debug::setSessionError($e->getMessage());
            $transaction->rollback();
        } catch (CException $e) {
            Debug::logToWindow($e->getMessage(), 'create league');
            Debug::setSessionError($e->getMessage());
            $transaction->rollback();
        }

        return false;
    }

    /**
     * @param int $manager_id The manager's id to join.
     * @return boolean
     */
    public function join($manager_id) {
        try {
            $this->addManager($manager_id);
            return true;
        } catch (CDbException $e) {
            Debug::logToWindow($e->getMessage(), 'join league');
        } catch (CValidateException $e) {

        }
        return false;
    }

    /**
     * @param int $manager_id The manager's id to leave.
     * @return boolean
     */
    public function leave($manager_id) {
        try {
            $this->_removeManager($manager_id);
            return true;
        } catch (CDbException $e) {
            Debug::logToWindow($e->getMessage(), 'leave league');
        } catch (CValidateException $e) {

        }
        return false;
    }

    /**
     * Kicks a manager from the league.
     * @param int $manager_id
     * @return boolean Whether the kick was successfull
     */
    public function kick($manager_id) {
        if (!$this->canBeKicked($manager_id))
            return false;

        try {
            if ($this->_removeManager($manager_id)) {
                $this->refresh();
                return true;
            }
        } catch (CDbException $e) {
            Debug::logToWindow($e->getMessage(), 'league kick');
        } catch (CException $e) {
            Debug::logToWindow($e->getMessage(), 'league kick');
        }

        return false;
    }

    /**
     * @param int $manager_id
     * @return boolean Whether the manager is the owner of this league.
     */
    public function isCreator($manager_id) {
        return ($this->creator_id == $manager_id);
    }

    /**
     * @return boolean True if the league has free spaces.
     */
    public function hasFreeSpaces() {
        return ($this->capacity == self::CAPACITY_UNLIMITED) || ($this->capacity > $this->numOfMembers());
    }

    /**
     * Adds a manager in a league.
     * @param int $manager_id Manager to add ID
     * @return boolean Whether the addition was successfull.
     */
    public function addManager($manager_id) {
        $participant = new CustomLeagueManager;
        $participant->join_date = _app()->localtime->getLocalNow();
        $participant->league_id = $this->id;
        $participant->manager_id = $manager_id;
        if (!$participant->save()) {
            throw new CValidateException($participant);
            return false;
        }

        Utils::triggerEvent('onManagerJoinCustomLeague', $this, array(
            'manager_id' => $manager_id,
            'league_id' => $this->id
        ));
        return true;
    }

    /**
     * Removes a manager from a league.
     * @param int $manager_id Manager to delete ID
     * @return boolean Whether the manager was successfully removed.
     */
    protected function _removeManager($manager_id) {

//        foreach ($this->members as $p) {
//            if ($p->manager_id == $manager_id) {
//                return $p->delete();
//            }
//        }

        // Faster
        $manager = CustomLeagueManager::model()->findByAttributes(array('league_id' => $this->id, 'manager_id' => $manager_id));
        if ($manager) {
            return $manager->delete();
        }
        else
            throw new CException(_t('Cannot remove non-existent manager({mid}) from league({lid})', array('{mid}' => $manager_id, '{lid}' => $this->id)));

        return false;
    }

    /**
     * @param int $type The constant for the type of message.
     * @return string
     */
    public function cannotCreateLeagueReasons($type) {
        switch ($type) {
            case self::ERR_BANNED:
                return _t('You cannot create a new league because you are banned.');
                break;

            case self::ERR_MAX_LEAGUES_REACHED:
                return _t('You cannot create a new league as you have reached the maximum number of leagues you can join.');
                break;

            case self::ERR_RESET:
                return _t('In order to create a league you need to create your team.');
                break;
        }
        return '';
    }

    /**
     * @param int $type The constant for the type of message.
     * @return string
     */
    public function cannotJoinReason($reason) {
        switch ($reason) {
            case self::ERR_BANNED:
                return _t('You cannot join the league because you are banned.');
                break;

            case self::ERR_NO_FREE_SPACES:
                return _t('You cannot join the league because it is full.');
                break;

            case self::ERR_MAX_LEAGUES_REACHED:
                return _t('You cannot join the league as you have reached the maximum number of leagues you can join.');
                break;

            case self::ERR_RESET:
                return _t('In order to join this league you need to create your team.');
                break;

            case self::ERR_ALREADY_JOINED:
                return _t('You have already joined this league.');
                break;

            case self::ERR_INVALID_FAN_LEAGUE:
                return _t("You cannot join a fan league other than your favorite team's.");
                break;
        }
        return '';
    }

    /**
     * @return boolean Whether a manager can see the user leagues table.
     */
    public function canSeeUserLeagues() {

        if (!Utils::isLoggedIn())
            return false;

        if (_manager()->hasRole(Role::TEMPORARY))
            return false;

        return true;
    }

    /**
     * Adds fan leagues for all teams.
     * @param string $leagueName League name template
     * @return boolean True if successfull
     */
    public function updateFanLeagues($leagueName = '{team}') {
        $teams = Team::model()->findAll();
        $fanLeagues = CustomLeagueFan::model()->with('league')->findAll();

        try {
            $trans = _app()->db->beginTransaction();

            foreach ($teams as $team) {

                $found = false;
                foreach ($fanLeagues as $fanLeague) {
                    if ($fanLeague->team_id == $team->id) {
                        $found = true;
                        break;
                    }
                }

                if ($found)
                    continue;

                $this->_createFanLeague($team, $leagueName);
            }

            $trans->commit();
            return true;
        } catch (CDbException $e) {
            Debug::logToWindow($e->getMessage());
            $trans->rollback();
        } catch (CValidateException $e) {
            $trans->rollback();
        }

        return false;
    }

    /**
     * Every user joins their favorite team's league.
     * @return boolean True if successfull
     */
    public function updateFanLeagueManagers() {

        $managers = Manager::model()->with('profile')->findAll('role != :role', array(':role' => Role::INACTIVE));
        $fanLeagues = CustomLeagueFan::model()->with('league')->findAll();

        try {
            $trans = _app()->db->beginTransaction();

            // For each manager
            foreach ($managers as $manager) {
                // If he has a favorite team yet
                if ($manager->profile->team_id === null)
                    continue;

                // Get the team's league
                $found = false;
                foreach ($fanLeagues as $fanLeague) {
                    if ($fanLeague->team_id == $manager->profile->team_id) {
                        $found = true;
                        break;
                    }
                }

                // If no fan league is found for this team, throw exception
                if (!$found) {
                    throw new CException("Fan league for {$manager->profile->team->name}({$manager->profile->team_id}) not found! Create it and come back.");
                }

                // If manager has not already joined
                if (!$fanLeague->league->isMember($manager->id)) {
                    // Join manager
                    $fanLeague->league->addManager($manager->id);
                }
            }

            $trans->commit();
            return true;
        } catch (CDbException $e) {
            Debug::logToWindow($e->getMessage());
            $trans->rollback();
        } catch (CValidateException $e) {
            $trans->rollback();
        } catch (CException $e) {
            Debug::logToWindow(Debug::exceptionDetailString($e));
            $trans->rollback();
        }

        return false;
    }

    /**
     * @param Team $team
     * @param string $leagueName League name template
     * @return boolean Whether the fan league was successfully created
     */
    private function _createFanLeague(Team $team, $leagueName) {
        // Create league and fan league
        $newLeague = new CustomLeague;
        $newLeague->date_created = _app()->localtime->getLocalNow();
        $newLeague->capacity = self::CAPACITY_UNLIMITED;
        $newLeague->creator_id = self::CREATOR_SYSTEM;
        $newLeague->type = CustomLeagueType::TEAM;
        $newLeague->privacy = CustomLeaguePrivacy::TYPE_PUBLIC;
        $newLeague->name = str_replace('{team}', $team->name, $leagueName);
        if ($newLeague->save()) {
            Utils::triggerEvent('onCustomLeagueCreate', $this, array('newLeague' => $newLeague));
            return $newLeague->addFanLeague($team->id);
        }
        else
            throw new CValidateException($newLeague);

        return false;
    }

    /**
     * @param int $team The team id
     * @return boolean Whether the fan league was successfully added
     */
    public function addFanLeague($team_id) {
        $newFanLeague = new CustomLeagueFan;
        $newFanLeague->league_id = $this->id;
        $newFanLeague->team_id = $team_id;
        if ($newFanLeague->save())
            return true;
        else
            throw new CValidateException($newFanLeague);

        return false;
    }

    /**
     * @param string $leagueName League name template
     */
    public function updateFanLeagueNames($leagueName) {
        $fanLeagues = CustomLeagueFan::model()->findAll();

        try {
            $trans = _app()->db->beginTransaction();
            foreach ($fanLeagues as $fanLeague) {
                $fanLeague->league->name = str_replace('{team}', $fanLeague->team->name, $leagueName);
                if (!$fanLeague->league->save())
                    throw new CValidateException($fanLeague->league);
            }

            $trans->commit();
        } catch (CDbException $e) {
            Debug::logToWindow($e->getMessage());
            $trans->rollback();
        } catch (CValidateException $e) {
            $trans->rollback();
        }
    }

    /**
     * @return boolean True if the league was created from the system.
     */
    public function isSystemLeague() {
        return $this->type == CustomLeagueType::SYSTEM;
    }

    /**
     * @param Manager $manager
     * @return boolean True if manager belongs to this fan league
     */
    private function _belongsToFanLeague(Manager $manager) {
        $fan = CustomLeagueFan::model()->findByAttributes(array('league_id' => $this->id));
        return $fan->team_id == $manager->profile->team_id;
    }

    /**
     * @param string $attribute
     * @return string
     */
    public function link($attribute = 'name') {
        return _l($this->$attribute, _url('customLeague/league', array('id' => $this->id)));
    }

    /**
     * @return CustomLeagueManager The manager that joined last, creator included.
     */
    public function lastManagerJoined() {
        return CustomLeagueManager::model()->find(array(
                'condition' => 'league_id = :lid',
                'params' => array(':lid' => $this->id),
                'order' => 'join_date desc',
            ));
    }

    /**
     * @return string Type of league
     */
    public function typeToText() {
        return CustomLeagueType::toText($this->type);
    }

    /**
     * @return string Type of league
     */
    public function privacyToText() {
        return CustomLeaguePrivacy::toText($this->privacy);
    }

    /**
     * Check if this league has a specific feature
     * @param int $feature
     * @return boolean
     */
    public function hasFeature($feature) {
        if ($this->metadata) {
            return (($feature & $this->metadata['flags']) == $this->metadata['flags']);
        }
        return false;
    }

    /**
     * @return boolean Whether this league is private.
     */
    public function isPrivate() {
        return in_array($this->privacy, array(CustomLeaguePrivacy::TYPE_PRIVATE, CustomLeaguePrivacy::TYPE_PRIVATE_HIDDEN));
    }

    /**
     * @param type $features
     */
    public function writeMetadata() {

        $metadata = array();

        foreach ($this->features as $class => $attributes) {
            if (key_exists(CustomLeagueFeature::getCbKey(), $attributes)) {
                unset($attributes[CustomLeagueFeature::getCbKey()]);
                $feature = new $class($attributes);
                $metadata[$class] = $feature->save();
            }
        }

        $this->metadata = CJSON::encode($metadata);
    }

    /**
     * @param type $metadata
     */
    public function readMetadata() {
        if (!$this->metadata)
            return;

        $features = CJSON::decode($this->metadata);
        foreach ($features as $class => $attributes) {
            $f = new $class($attributes);
            $this->features[] = $f->load();
        }
    }

    /**
     *
     */
    public function joinAllManagersToAllLeagues() {
        $managers = Manager::model()->findAll();
        $leagues = CustomLeague::model()->findAll();
        foreach ($managers as $m) {
            if (!$m->hasRole(Role::MANAGER))
                continue;
            foreach ($leagues as $l) {
                if ($l->canJoin($m) == CustomLeague::SUCCESS) {
                    $l->addManager($m->id);
                }
            }
        }
    }

}

?>
