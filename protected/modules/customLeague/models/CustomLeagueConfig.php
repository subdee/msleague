<?php

/**
 * This is the model class for table "mod_custom_leagues_config".
 *
 * The followings are the available columns in table 'mod_custom_leagues_config':
 * @property integer $maxLeaguesPerManager
 * @property integer $maxManagersPerLeague
 * @property integer $id
 */
class CustomLeagueConfig extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return CustomLeagueConfig the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'mod_custom_leagues_config';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('maxLeaguesPerManager, maxManagersPerLeague, id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('maxLeaguesPerManager, maxManagersPerLeague, id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'maxLeaguesPerManager' => 'Max Leagues Per Manager',
            'maxManagersPerLeague' => 'Max Managers Per League',
            'id' => 'ID',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('maxLeaguesPerManager', $this->maxLeaguesPerManager);
        $criteria->compare('maxManagersPerLeague', $this->maxManagersPerLeague);
        $criteria->compare('id', $this->id);

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
    }

}

?>