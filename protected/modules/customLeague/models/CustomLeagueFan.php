<?php

/**
 * This is the model class for table "mod_custom_leagues_fan".
 *
 * The followings are the available columns in table 'mod_custom_leagues_fan':
 * @property integer $league_id
 * @property integer $team_id
 *
 * The followings are the available model relations:
 * @property CustomLeague $league
 * @property Team $team
 */
class CustomLeagueFan extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return CustomLeagueFan the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'mod_custom_leagues_fan';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('league_id, team_id', 'required'),
            array('league_id, team_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('league_id, team_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'league' => array(self::BELONGS_TO, 'CustomLeague', 'league_id'),
            'team' => array(self::BELONGS_TO, 'Team', 'team_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'league_id' => 'League',
            'team_id' => 'Team',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('league_id', $this->league_id);
        $criteria->compare('team_id', $this->team_id);

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
    }

}

?>
