<?php

/**
 * Combinational league features.
 */
abstract class CustomLeagueFeature {
    /**
     * Leagues that have a pay fee.
     */
    const PAY = 1;

    /**
     * Leagues that have a custom theme (e.g. sponsor)
     */
    const THEME = 2;

    /**
     * Leagues that have a closing join date.
     */
    const LIMITED_JOIN_PERIOD = 4;

    /**
     * Leageus that have an end date.
     */
    const LIMITED_PLAY_PERIOD = 8;

    /**
     * @var int
     */
    protected $_type;

    /**
     * @var boolean
     */
    protected $_isOn = false;

    /**
     *
     */
    protected $attributes = array(
    );

    /**
     * @param int $type
     * @return string
     */
    public static function toText($type) {
        switch ($type) {
            case self::PAY: return _t('Pay');
            case self::THEME: return _t('Theme');
            case self::LIMITED_JOIN_PERIOD: return _t('Limited join period');
            case self::LIMITED_PLAY_PERIOD: return _t('Limited play period');
        }
        throw new CException('Invalid <em>CustomLeagueFeature</em>: ' . $type);
    }

    /**
     * @return array
     */
    public static function toArray() {
        return array(
//            self::PAY => self::toText(self::PAY),
//            self::THEME => self::toText(self::THEME),
            self::LIMITED_JOIN_PERIOD => self::toText(self::LIMITED_JOIN_PERIOD),
//            self::LIMITED_PLAY_PERIOD => self::toText(self::LIMITED_PLAY_PERIOD),
        );
    }

    /**
     *
     * @param int $type
     * @return string
     */
    public static function className($type) {
        switch ($type) {
            case self::PAY: return 'CustomLeagueFeaturePay';
            case self::THEME: return 'CustomLeagueFeatureTheme';
            case self::LIMITED_JOIN_PERIOD: return 'CustomLeagueFeatureLimitedJoinPeriod';
            case self::LIMITED_PLAY_PERIOD: return 'CustomLeagueFeatureLimitedPlayPeriod';
        }
        throw new CException('Invalid <em>CustomLeagueFeature</em>: ' . $type);
    }

    /**
     * @param array $attributes
     */
    public function __construct($attributes) {

        if (isset($_POST['CustomLeague']) && isset($_POST['CustomLeague']['features'][$this->getClassName()])) {
            foreach ($_POST['CustomLeague']['features'][$this->getClassName()] as $attr => $value) {
                $this->$attr = $value;
            }

            $this->_isOn = isset($_POST['CustomLeague']['features'][$this->getClassName()][self::getCbKey()]);
        } elseif ($attributes !== null) {
            foreach ($attributes as $attr => $value) {
                $this->$attr = $value;
            }
            $this->_isOn = true;
        }

        $this->init();
    }

    /**
     *
     */
    public function save() {
        $return = array();
        if ($this->beforeSave()) {
            foreach ($this->attributes as $attr => $value) {
                $return[$attr] = $value;
            }
        }
        return $return;
    }

    /**
     *
     */
    public function load() {
        $this->afterFind();
        return $this;
    }

    /**
     * @return string
     */
    public function getVarName() {
        return 'CustomLeague[features][' . $this->getClassName() . ']';
    }

    /**
     *
     */
    public function getVarNameFor($attribute) {
        return $this->getVarName() . '[' . $attribute . ']';
    }

    /**
     * @return type
     */
    public static function getCbKey() {
        return '_isOn';
    }

    /**
     * @return type
     */
    public function getCbName() {
        return $this->getVarName() . '[' . self::getCbKey() . ']';
    }

    /**
     * @return type
     */
    public function getCbId() {
        return 'CustomLeague_features_' . $this->getClassName() . '_' . self::getCbKey();
    }

    /**
     * @return type
     */
    public function isOn() {
        return $this->_isOn;
    }

    /**
     * @param type $attribute
     * @return type
     */
    public function __get($attribute) {
        if ($this->_hasAttribute($attribute)) {
            return $this->attributes[$attribute];
        }
        else
            throw new CException(_t('{class} does have an attribute named ${attr}', array('{class}' => $this->getClassName(), '{attr}' => $attribute)));

        return null;
    }

    /**
     * @param type $attribute
     * @param type $value
     */
    public function __set($attribute, $value) {
        if ($this->_hasAttribute($attribute)) {
            $this->attributes[$attribute] = $value;
        }
        else
            $this->attribute = $value;
    }

    /**
     *
     */
    public function typeToText() {
        return self::toText($this->_type);
    }

    /**
     *
     * @param type $attribute
     * @return type
     */
    protected function _hasAttribute($attribute) {
        return key_exists($attribute, $this->attributes);
    }

    /**
     *
     */
    public function getClassName() {
        return self::className($this->_type);
    }

    /**
     *
     */
    public function adminHtml() {
        echo CHtml::openTag('div', array('id' => "feature-$this->_type", 'class' => 'row feature'));

        // label
        echo CHtml::tag('label', array('class' => 'feature-title'), $this->typeToText());

        // add link
        echo CHtml::link(_t('Add'), '#', array(
            'class' => 'action add ' . ($this->isOn() ? 'hidden' : ''),
            'onclick' => '
                js:$("#feature-' . $this->_type . ' .feature-form").toggle();
                $("#feature-' . $this->_type . ' .action").toggle();
                $("#' . $this->getCbId() . '").attr("checked", true);
                return false;
            '
        ));

        // remove link
        echo CHtml::link(_t('remove'), '#', array(
            'class' => 'action remove ' . ($this->isOn() ? '' : 'hidden'),
            'onclick' => '
                js:$("#feature-' . $this->_type . ' .feature-form").toggle();
                $("#feature-' . $this->_type . ' .action").toggle();
                $("#' . $this->getCbId() . '").removeAttr("checked");
                return false;
            '
        ));

        // Render feature attributes
        echo CHtml::openTag('div', array('class' => 'feature-form ' . ($this->isOn() ? '' : 'hidden')));
        $this->_html();
        echo CHtml::closeTag('div');

        echo CHtml::closeTag('div');
        echo CHtml::checkBox($this->getCbName(), $this->isOn(), array('class' => 'hidden'));
    }

    /**
     * Initialises the feature.
     */
    protected function init() {

    }

    /**
     * @return type
     */
    protected function beforeSave() {
        return true;
    }

    /**
     *
     */
    protected function afterFind() {

    }

    /**
     *
     */
    abstract protected function _html();

    /**
     * Feature action check: can manager join this league?
     * @param Manager $manager
     * @return boolean
     */
    public function canJoin(Manager $manager) {
        return true;
    }
}

?>
