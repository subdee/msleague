<?php

/**
 * This is the model class for table "mod_custom_leagues_managers".
 *
 * The followings are the available columns in table 'mod_custom_leagues_managers':
 * @property integer $id
 * @property string $join_date
 * @property integer $league_id
 * @property integer $manager_id
 *
 * The followings are the available model relations:
 * @property CustomLeague $league
 * @property Manager $manager
 */
class CustomLeagueManager extends CActiveRecord {

    /**
     * @var boolean Whether this manager is the creator of the parent league.
     */
    public $_isCreator = false;

    /**
     * @var int Used only by search.
     */
    public $managerRole;

    /**
     * Returns the static model of the specified AR class.
     * @return CustomLeagueManager the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'mod_custom_leagues_managers';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('join_date', 'required'),
            array('league_id, manager_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('managerRole', 'safe'),
            array('id, join_date, league_id, manager_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'league' => array(self::BELONGS_TO, 'CustomLeague', 'league_id'),
            'manager' => array(self::BELONGS_TO, 'Manager', 'manager_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'join_date' => _t('Join Date'),
            'league_id' => _t('League'),
            'manager_id' => _t('Username'),
            'name' => _t('Name'),
            'capacity' => _t('Capacity'),
            'type' => _t('Type'),
            'rank' => _t('Rank'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        // Join the manager
        $criteria->with = array(
            'manager' => array('select' => 'username, role, total_points', 'with' => 'managerTeam'),
            'manager.profile' => array('select' => 'line'),
            'manager.managerTeam' => array('select' => 'name, created_on'),
        );

        // Order managers by points and date joined from older to newer.
        $criteria->order = 'total_points desc, managerTeam.created_on asc';

        switch ($this->getScenario()) {
            case 'search':
                $criteria->addInCondition('manager.role', array(Role::MANAGER, Role::RESET));
                break;
        }

        $criteria->compare('id', $this->id);
        $criteria->compare('join_date', $this->join_date, true);
        $criteria->compare('league_id', $this->league_id);
        $criteria->compare('manager_id', $this->manager_id);

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 10
                ),
                'sort' => false
            ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchAdmin() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('join_date', $this->join_date, true);
        $criteria->compare('league_id', $this->league_id);
        $criteria->compare('manager.username', $this->manager_id, true);
        $criteria->compare('manager.role', $this->managerRole);

        // Join the manager
        $criteria->with = array(
            'manager' => array('select' => 'username, total_points, role'),
        );

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 30
                ),
                'sort' => array(
                    'attributes' => array(
                        'join_date' => array(
                            'asc' => 'join_date',
                            'desc' => 'join_date desc',
                        ),
                        'manager.total_points' => array(
                            'asc' => 'manager.total_points',
                            'desc' => 'manager.total_points desc',
                        )
                    ),
                    'defaultOrder' => array(
                        'manager.total_points' => true,
                    )
                ),
            ));
    }

    /**
     * @return boolean
     */
    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->join_date = Utils::fromLocalDatetime($this->join_date);
            return true;
        }
        return false;
    }

    /**
     * Updates the manager's attributes after loading from the database.
     */
    protected function afterFind() {
        parent::afterFind();

        if ($this->manager_id == $this->league->creator_id) {
            $this->_isCreator = true;
        }

        $this->join_date = Utils::toLocalDatetime($this->join_date);
    }

    /**
     * @return boolean The manager's creator flag.
     */
    public function isCreator() {
        return $this->_isCreator;
    }

    /**
     *
     * @return string
     */
    public function creatorImage() {
        return CHtml::image(Utils::moduleImageUrl('customLeague', 'star.png'), _t('League creator'), array('title' => _t('League creator')));
    }
}

?>
