<?php

class CustomLeagueModuleEvents extends CBehavior {

    public function onCustomLeagueViewActions($event) {
        $this->raiseEvent('onCustomLeagueViewActions', $event);
    }

    public function onCustomLeaguesPageTop($event) {
        $this->raiseEvent('onCustomLeaguesPageTop', $event);
    }

    public function onCustomLeaguePageBottom($event) {
        $this->raiseEvent('onCustomLeaguePageBottom', $event);
    }

    public function onManagerJoinCustomLeague($event) {
        $this->raiseEvent('onManagerJoinCustomLeague', $event);
    }

    public function onCustomLeagueDelete($event) {
        $this->raiseEvent('onCustomLeagueDelete', $event);
    }

    public function onCustomLeagueCreate($event) {
        $this->raiseEvent('onCustomLeagueCreate', $event);
    }

    public function onLeaguePageSidebarBottom($event) {
        $this->raiseEvent('onLeaguePageSidebarBottom', $event);
    }
}

?>
