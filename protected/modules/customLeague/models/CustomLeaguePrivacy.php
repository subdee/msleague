<?php

/**
 * League privacy.
 */
class CustomLeaguePrivacy {
    /**
     * Public league.
     */
    const TYPE_PUBLIC = 1;

    /**
     * Private leagues. Need invitation to join.
     */
    const TYPE_PRIVATE = 2;

    /**
     * Hidden leagues. Same as private.
     */
    const TYPE_PRIVATE_HIDDEN = 3;

    /**
     * @param int $type
     * @return string
     */
    public static function toText($type) {
        switch ($type) {
            case self::TYPE_PUBLIC: return _t('Public');
            case self::TYPE_PRIVATE: return _t('Private & Visible');
            case self::TYPE_PRIVATE_HIDDEN: return _t('Private');
        }
        throw new CException('Invalid <em>CustomLeaguePrivacy</em>: ' . $type);
    }

    /**
     * @return array
     */
    public static function toArray() {
        return array(
            self::TYPE_PUBLIC => self::toText(self::TYPE_PUBLIC),
//            self::TYPE_PRIVATE => self::toText(self::TYPE_PRIVATE),
            self::TYPE_PRIVATE_HIDDEN => self::toText(self::TYPE_PRIVATE_HIDDEN),
        );
    }

}

?>
