<?php

/**
 * League types.
 */
class CustomLeagueType {
    /**
     * Other leagues.
     */
    const SYSTEM = 1;

    /**
     * Leagues created by users.
     */
    const USER = 2;

    /**
     * League created for fans of a team.
     */
    const TEAM = 3;

    /**
     *
     * @param type $type
     * @return type
     */
    public static function toText($type) {
        switch ($type) {
            case self::SYSTEM: return _t('{gameName}', array('{gameName}' => _gameName()), 'CustomLeagueType');
            case self::USER: return _t('User', array(), 'CustomLeagueType');
            case self::TEAM: return _t('Fan', array(), 'CustomLeagueType');
        }
        throw new CException('Invalid <em>CustomLeagueType</em>: ' . $type);
    }

    /**
     *
     * @return type
     */
    public static function toArray() {
        return array(
            self::SYSTEM => self::toText(self::SYSTEM),
            self::USER => self::toText(self::USER),
            self::TEAM => self::toText(self::TEAM),
        );
    }

}

?>
