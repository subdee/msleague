<?php

/**
 *
 */
class CustomLeagueFeatureLimitedJoinPeriod extends CustomLeagueFeature {

    protected $attributes = array(
        /**
         * @var string When the league will close.
         */
        'closingDate' => null
    );

    /**
     *
     */
    public function __construct($attributes=null) {

        // Type must be set before calling parent constructor
        $this->_type = self::LIMITED_JOIN_PERIOD;

        // Initialise attributes
        $this->closingDate = date('Y-m-d H:i:s');

        // Call parent costructor
        parent::__construct($attributes);
    }

    /**
     * Initialises the feature.
     */
    protected function init() {
        // Register to league's page sidedar event.
        Utils::registerNamedCallback('CustomLeagueModuleEvents', 'onLeaguePageSidebarBottom', $this, 'renderClosingDate');
    }

    /**
     *
     */
    protected function _html() {
        _app()->controller->renderPartial('_featureLimitedJoinPeriod', array(
            'feature' => $this,
        ));
    }

    /**
     * @return boolean True if closing date has passed.
     */
    public function isOpen() {
        return strtotime($this->closingDate) > _app()->localtime->getLocalNow('U');
    }

    /**
     * @param CEvent $e
     */
    public function renderClosingDate(CEvent $e) {
        $league = $e->params['league'];
        if ($league->isMember(_manager()->id))
            return;

        if ($this->isOpen()) {
            echo CHtml::tag('div', array('class' => 'league-feature limited-join-period open'), _t('You can join this league until {date}', array(
                    '{date}' => Utils::date($this->closingDate, array(
//                        'time' => false,
                        'dayAbbreviated' => false,
                        'monthAbbreviated' => false
                    ))
                )));
        }
        else
            echo CHtml::tag('div', array('class' => 'league-feature limited-join-period closed'), _t('Join period has passed.'));
    }

    /**
     * @return boolean
     */
    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->closingDate = Utils::fromLocalDatetime($this->closingDate);
            return true;
        }
        return false;
    }

    /**
     *
     */
    protected function afterFind() {
        parent::afterFind();
        $this->closingDate = Utils::toLocalDatetime($this->closingDate);
    }

    /**
     * Feature action check: can manager join this league?
     * @param Manager $manager
     * @return boolean
     */
    public function canJoin(Manager $manager) {
        if (parent::canJoin($manager)) {
            return $this->isOpen();
        }
        return false;
    }

}

?>
