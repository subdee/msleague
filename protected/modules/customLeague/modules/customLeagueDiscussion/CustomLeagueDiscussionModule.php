<?php

class CustomLeagueDiscussionModule extends WebModule {

    public function init() {
        parent::init();

        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'customLeagueDiscussion.models.*',
            'customLeagueDiscussion.components.*',
        ));

        // Register events
        Utils::registerNamedCallback('CustomLeagueModuleEvents', 'onCustomLeaguePageBottom', $this, 'renderDiscussion');
        Utils::registerNamedCallback('CustomLeagueModuleEvents', 'onCustomLeagueCreate', $this, 'addDiscussionToLeague');
        Utils::registerNamedCallback('CustomLeagueModuleEvents', 'onCustomLeagueDelete', $this, 'deleteLeagueDiscussion');
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        }
        else
            return false;
    }

    /**
     * @param CEvent $e
     */
    public function renderDiscussion(CEvent $e) {
        $league = $e->params['league'];
        $discussion = CustomLeagueDiscussion::model()->findByAttributes(array('league_id' => $league->id));
        if ($discussion) {
            $e->sender->widget('Comments', array(
                'discussionId' => $discussion->discussion_id,
                'returnUrl' => _url('customLeague/league', array('id' => $league->id)),
                'openCondition' => CustomLeagueDiscussion::model()->canComment($league, _manager()),
                'commentsClosedCustomMessage' => _t("You can only comment in your favorite team's league discussion."),
                'cssClass' => 'narrow',
                'showHeader' => true,
            ));
        }
        else
            throw new CException("Discussion missing for league({$league->id})");
    }

    /**
     * @param CEvent $e
     */
    public function addDiscussionToLeague(CEvent $e) {
        $league = $e->params['newLeague'];
        CustomLeagueDiscussion::model()->attachDiscussion($league);
    }

    /**
     * @param CEvent $e
     */
    public function deleteLeagueDiscussion(CEvent $e) {
        $connection = CustomLeagueDiscussion::model()->findByAttributes(array('league_id' => $e->params['league_id']));
        if ($connection && $connection->discussion)
            $connection->discussion->delete();
//        else
//            throw new CException("DIscussion missing for league({$e->params['league_id']})");
    }

}
