<?php

/**
 * This is the model class for table "mod_custom_leagues_discussions".
 *
 * The followings are the available columns in table 'mod_custom_leagues_discussions':
 * @property integer $league_id
 * @property integer $discussion_id
 *
 * The followings are the available model relations:
 * @property CustomLeague $league
 * @property Discussion $discussion
 */
class CustomLeagueDiscussion extends CActiveRecord {

    /**
     * @var string Used only by search.
     */
    public $creatorUsername;

    /**
     * @var string Used only by search.
     */
    public $dateCreated;

    /**
     * @var string Used only by search.
     */
    public $dateUpdated;

    /**
     * @var int Used only by search.
     */
    public $commentCount;

    /**
     * @var int Used only by search.
     */
    public $leaguePrivacy;

    /**
     * Returns the static model of the specified AR class.
     * @return CustomLeagueDiscussion the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'mod_custom_leagues_discussions';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('league_id, discussion_id', 'required'),
            array('league_id, discussion_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('creatorUsername, leaguePrivacy', 'safe'),
            array('league_id, discussion_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'league' => array(self::BELONGS_TO, 'CustomLeague', 'league_id'),
            'discussion' => array(self::BELONGS_TO, 'Discussion', 'discussion_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'league_id' => 'League',
            'discussion_id' => 'Discussion',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('league.name', $this->league_id, true);
        $criteria->compare('discussion_id', $this->discussion_id);
        $criteria->compare('creator.username', $this->creatorUsername, true);
        $criteria->compare('league.privacy', $this->leaguePrivacy);

        if ($this->getScenario() == 'searchFanLeagues') {
            $criteria->compare('league.type', CustomLeagueType::TEAM);
        } elseif ($this->getScenario() == 'searchExceptFanLeagues') {
            $criteria->compare('league.type', CustomLeagueType::USER);
        }

        $criteria->with = array(
            'league',
            'league.creator' => array('select' => 'username'),
            'discussion',
        );

        // Add select comment count
        $criteria->select = '*, (SELECT COUNT(id) FROM mod_forum_comments WHERE discussion_id = t.discussion_id) as commentCount';

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'sort' => array(
                    'attributes' => array(
                        'commentCount' => array(
                            'asc' => 'commentCount',
                            'desc' => 'commentCount desc',
                        ),
                        'dateCreated' => array(
                            'asc' => 'league.date_created',
                            'desc' => 'league.date_created desc',
                        ),
                        'dateUpdated' => array(
                            'asc' => 'discussion.date_updated',
                            'desc' => 'discussion.date_updated desc',
                        ),
                    ),
                    'defaultOrder' => array(
                        'dateUpdated' => true
                    ),
                ),
                'pagination' => array(
                    'pageSize' => 20,
                ),
            ));
    }

    /**
     *
     * @param CustomLeague $league
     * @param Manager $manager
     * @return boolean Whether the manager can comment on that league.
     */
    public function canComment(CustomLeague $league, Manager $manager) {

        if ($league->privacy == CustomLeaguePrivacy::TYPE_PUBLIC && $league->type != CustomLeagueType::TEAM)
            return true;

        if ($league->type == CustomLeagueType::TEAM) {
            $team_id = CustomLeagueFan::model()->findByAttributes(array('league_id' => $league->id))->team_id;
            if ($manager->profile->team_id == $team_id)
                return true;
        }

        if ($league->isPrivate()) {
            if ($league->isMember($manager->id))
                return true;
        }

        return false;
    }

    /**
     * Adds discussion to league
     * @param CustomLeague $league
     * @return boolean
     */
    public function attachDiscussion(CustomLeague $league) {

        // Create the discussion
        $discussion = new Discussion;
        if (!$discussion->save()) {
            throw new CValidateException($discussion);
            return false;
        }

        // Create the connection
        $connection = new CustomLeagueDiscussion;
        $connection->league_id = $league->id;
        $connection->discussion_id = $discussion->id;
        if (!$connection->save()) {
            throw new CValidateException($connection);
            return false;
        }

        return true;
    }

    /**
     * Attaches a discussion thread to each league.
     * @return boolean True for success
     */
    public function updateLeagueDiscussions() {

        // Only get leagues that do not have a discussion thread attached to them yet.
        $leagues = CustomLeague::model()->findAll('t.id not in (SELECT league_id FROM mod_custom_leagues_discussions)');

        try {
            $trans = _app()->db->beginTransaction();

            foreach ($leagues as $league) {
                $this->attachDiscussion($league);
            }

            $trans->commit();
            return true;
        } catch (CDbException $e) {
            Debug::logToWindow($e->getMessage(), 'update league discussions');
            $trans->rollback();
        } catch (CValidateException $e) {
            $trans->rollback();
        } catch (CException $e) {
            Debug::logToWindow($e->getMessage(), 'update league discussions');
            $trans->rollback();
        }

        return false;
    }

}

?>
