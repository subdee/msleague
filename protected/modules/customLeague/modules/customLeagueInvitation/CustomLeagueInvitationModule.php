<?php

class CustomLeagueInvitationModule extends WebModule {

    public function init() {
        parent::init();

        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'customLeagueInvitation.models.*',
            'customLeagueInvitation.components.*',
        ));

        // Register events
        Utils::registerNamedCallback('CustomLeagueModuleEvents', 'onCustomLeagueViewActions', $this, 'renderInviteButton');
        Utils::registerNamedCallback('CustomLeagueModuleEvents', 'onCustomLeaguesPageTop', $this, 'renderMyInvitations');
        Utils::registerNamedCallback('CustomLeagueModuleEvents', 'onManagerJoinCustomLeague', $this, 'clearManagerInvitations');
        Utils::registerNamedCallback('CustomLeagueModuleEvents', 'onCustomLeagueDelete', $this, 'clearLeagueInvitations');
        Utils::registerNamedCallback('NotificationModuleEvents', 'onNotificationAccept', $this, 'acceptInvitation');

        // TODO: Events to be implemented
//        Utils::registerCallback('onManagerDelete', $this, 'clearManagerInvitations');
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        }
        else
            return false;
    }

    /**
     * @param CEvent $e
     */
    public function renderInviteButton(CEvent $e) {
        Utils::registerModuleChildCssFile('customLeague', 'customLeagueInvitation', 'styles');

        $league = $e->params['league'];
        if ($this->canInvite($league, _manager())) {
            $e->sender->renderPartial('customLeagueInvitation.views._inviteButton', array('league' => $league));
        } else if ($this->canViewInvite($league, _manager()->id)) {
            echo CHtml::tag('span', array('class' => 'invite', 'title' => _t('You cannot invite anyone to the league as you are banned.')), _t('Invite'));
        }
    }

    /**
     * @param CEvent $e
     */
    public function renderMyInvitations(CEvent $e) {
        Utils::registerModuleChildCssFile('customLeague', 'customLeagueInvitation', 'styles');

        if (Utils::isLoggedIn()) {
            $invitations = new CustomLeagueInvitation('searchMyInvitations');
            $e->sender->renderPartial('customLeagueInvitation.views._myInvitations', array(
                'invitations' => $invitations->search()->data,
            ));
        }
    }

    /**
     * @param CEvent $e
     */
    public function acceptInvitation(CEvent $e) {

        // Get notification
        $notification = $e->params['notification'];

        // Get invitation
        $invitation = CustomLeagueInvitation::model()->findByAttributes(array('notification_id' => $notification->id));

        // Let recipient join the league
        $reason = $invitation->league->canJoin($notification->recipient);
        if ($reason > 0) {
            $invitation->league->addManager($notification->recipient_id);
        } else
            throw new CException(CustomLeague::model()->cannotJoinReason($reason));

//        Notification::model()->setShowOnReturn(
//            $e->sender->renderPartial('customLeagueInvitation.views._onAcceptInvitation', array(
//                'invitation' => $invitation,
//                ), true)
//        );

        Notification::model()->setRedirectOnReturn(_url('customLeague/league', array('id' => $invitation->league_id)));
    }

    /**
     * @param CEvent $e
     */
    public function clearManagerInvitations(CEvent $e) {
        CustomLeagueInvitation::model()->clearManagerInvitations($e->params['league_id'], $e->params['manager_id']);
    }

    /**
     * @param CEvent $e
     */
    public function clearLeagueInvitations(CEvent $e) {
        CustomLeagueInvitation::model()->clearLeagueInvitations($e->params['league_id']);
    }

    /**
     * @param CustomLeague $league
     * @param Manager $manager
     * @return boolean Whether the manager meets the conditions.
     */
    public function canInvite(CustomLeague $league, Manager $manager) {
        if ($manager->isBanned())
            return false;

        if ($league->creator_id != $manager->id)
            return false;

        return true;
    }

    /**
     * @param CustomLeague $league
     * @param int $manager_id
     * @return boolean
     */
    public function canViewInvite(CustomLeague $league, $manager_id) {
        if ($league->creator_id != $manager_id)
            return false;

        return true;
    }

}
