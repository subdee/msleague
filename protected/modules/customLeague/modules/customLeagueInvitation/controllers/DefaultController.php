<?php

class DefaultController extends Controller {

    public function filters() {
        return array('accessControl',);
    }

    public function accessRules() {
        return array(
            array('allow',
                'expression' => '_manager()->hasRole(Role::MANAGER | Role::RESET)',
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

    /**
     *
     */
    public function actionInvite() {

        if (!Utils::isAjax() || !isset($_POST['username']) || !isset($_POST['league_id']))
            return;

        // Get manager
        $manager = Manager::model()->find(array(
            'condition' => 'username = :uname AND role IN (:role1, :role2)',
            'params' => array(
                ':uname' => $_POST['username'],
                ':role1' => Role::MANAGER,
                ':role2' => Role::RESET,
            ),
            ));
        if (!$manager) {
            Utils::jsonReturn(array(
                'message' => _t('Manager does not exist.'),
                'type' => 'error',
                'class' => 'small',
            ));
        }

        // Check if blocked
        if ($manager->hasBlockedManager(_manager()->id)) {
            Utils::jsonReturn(array(
                'message' => _t("You cannot send invitation to a manager who has blocked you."),
                'type' => 'error',
                'class' => 'small',
            ));
        }

        // Get league
        $league = CustomLeague::model()->findByPk($_POST['league_id']);
        if (!$league) {
            Utils::jsonReturn(array(
                'message' => _t('League does not exist.'),
                'type' => 'error',
                'class' => 'small',
            ));
        }

        // See if manager can be invited to this league
        $reason = CustomLeagueInvitation::model()->canBeInvited($league, $manager->id);
        if ($reason < 0) {
            Utils::jsonReturn(array(
                'message' => CustomLeagueInvitation::model()->cannotInviteReason($reason),
                'type' => 'error',
                'class' => 'small',
            ));
        }

        // Invite manager
        try {
            $trans = _app()->db->beginTransaction();

            $notification = new Notification;
            $notification->creation_date = _app()->localtime->getLocalNow();
            $notification->type = Notification::TYPE_YESNO;
            $notification->recipient_id = $manager->id;
            $notification->message = $this->renderPartial('customLeagueInvitation.views._invitationMessage', array(
                'league' => $league,
                'manager' => _manager(),
                ), true);
            if (!$notification->save())
                throw new CValidateException($notification);

            $invitation = new CustomLeagueInvitation;
            $invitation->notification_id = $notification->id;
            $invitation->league_id = $league->id;
            if (!$invitation->save())
                throw new CValidateException($invitation);

            $trans->commit();
            Utils::jsonReturn(array(
                'status' => 'success',
                'notice' => array(
                    'message' => _t('Invitation sent to {manager}.', array('{manager}' => $manager->profileLink())),
                    'type' => 'success',
                    'class' => 'small',
                )
            ));
        } catch (CDbException $e) {
            $trans->rollback();
            Debug::logToWindow($e->getMessage());
            Utils::jsonReturn(array(
                'message' => _t('Database exception occurred.'),
                'type' => 'error'
            ));
        } catch (CValidateException $e) {
            $trans->rollback();
            Utils::jsonReturn(array(
                'message' => nl2br($e->getMessage()),
                'type' => 'warning',
                'class' => 'small',
            ));
        }
    }

    /**
     *
     */
    public function actionRecipientsList() {
        if (Yii::app()->request->isAjaxRequest && isset($_GET['q'])) {
            /* q is the default GET variable name that is used by the autocomplete widget to pass in user input  */

            $name = $_GET['q'];

            // this was set with the "max" attribute of the CAutoComplete widget
            $limit = min($_GET['limit'], 50);
            $criteria = new CDbCriteria;
            $criteria->select = 'username';
            $criteria->condition = "username LIKE :sterm AND role in (:role1, :role2)";
            $criteria->params = array(
                ':sterm' => "%$name%",
                ':role1' => Role::MANAGER,
                ':role2' => Role::RESET,
            );
            $criteria->limit = $limit;
            $userArray = Manager::model()->findAll($criteria);
            $returnVal = '';
            foreach ($userArray as $userAccount) {
                $returnVal .= $userAccount->getAttribute('username') . "\n";
            }
            echo $returnVal;
        }
    }

}