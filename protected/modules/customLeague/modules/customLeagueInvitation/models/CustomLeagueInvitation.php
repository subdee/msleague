<?php

/**
 * This is the model class for table "mod_custom_leagues_notifications".
 *
 * The followings are the available columns in table 'mod_custom_leagues_notifications':
 * @property integer $league_id
 * @property integer $notification_id
 *
 * The followings are the available model relations:
 * @property CustomLeague $league
 * @property Notification $notification
 */
class CustomLeagueInvitation extends CActiveRecord {
    const SUCCESS = 1;

    /**
     * Manager has already joined the league.
     */
    const ERR_ALREADY_JOINED = -1;

    /**
     * Manager has already been invited to the league.
     */
    const ERR_ALREADY_INVITED = -2;

    /**
     * User cannot create or join a new league as he has reached the maximum number of leagues he can be a part of.
     */
    const ERR_MAX_LEAGUES_REACHED = -3;

    /**
     * User cannot join a full league.
     */
    const ERR_NO_FREE_SPACES = -4;

    /**
     * User cannot create, join a league as he is banned.
     */
    const ERR_RESET = -5;

    /**
     * User cannot create, join, edit, leave or delete a league as he is banned.
     */
    const ERR_BANNED = -6;

    /**
     * Returns the static model of the specified AR class.
     * @return CustomLeagueInvitation the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'mod_custom_leagues_invitations';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('league_id, notification_id', 'required'),
            array('league_id, notification_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('league_id, notification_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'league' => array(self::BELONGS_TO, 'CustomLeague', 'league_id'),
            'notification' => array(self::BELONGS_TO, 'Notification', 'notification_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'league_id' => _t('League'),
            'notification_id' => _t('notification'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        if ($this->getScenario() == 'searchMyInvitations') {
            $criteria->join = 'join mod_notifications n on n.id = t.notification_id';
            $criteria->condition = 'n.recipient_id = :mid';
            $criteria->params[':mid'] = _manager()->id;
        }

        $criteria->compare('league_id', $this->league_id);
        $criteria->compare('notification_id', $this->notification_id);

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
    }

    /**
     *
     */
    protected function beforeDelete() {
        if (parent::beforeDelete()) {
            // Remove relevant notification.
            $this->notification->delete();
            return true;
        }
        return false;
    }

    /**
     * @param CustomLeague $league
     * @param int $manager_id
     * @return int Whether the manager meets the conditions.
     */
    public function canBeInvited(CustomLeague $league, $manager_id) {

        if ($league->isMember($manager_id))
            return self::ERR_ALREADY_JOINED;

        if ($this->hasBeenInvited($league->id, $manager_id))
            return self::ERR_ALREADY_INVITED;

        return self::SUCCESS;
    }

    /**
     * @param CustomLeague $league
     * @param Manager $manager
     * @return int
     */
    public function canAccept(CustomLeague $league, Manager $manager) {
        if ($manager->isBanned())
            return self::ERR_BANNED;

        if ($league->numOfLeagesForManager($manager->id) >= CustomLeagueConfig::model()->maxLeaguesPerManager)
            return self::ERR_MAX_LEAGUES_REACHED;

        if (!$league->hasFreeSpaces())
            return self::ERR_NO_FREE_SPACES;

        if ($league->isMember($manager->id))
            return self::ERR_ALREADY_JOINED;

        return self::SUCCESS;
    }

    /**
     * @param int $league_id
     * @param int $manager_id
     * @return boolean Whether manager has already joined this league.
     */
    protected function hasBeenInvited($league_id, $manager_id) {
        $query = <<<SQL
            select count(manager_id) as mc
            from (
                select manager_id
                from (
                    select creator_id, lm.manager_id as manager_id
                    from mod_custom_leagues_managers lm
                    join mod_custom_leagues l on l.id = lm.league_id
                    where lm.league_id = :league and lm.manager_id = :manager
                ) foo
                where creator_id != manager_id
                union
                select n.recipient_id as manager_id
                from mod_custom_leagues_invitations li
                join mod_notifications n on n.id = li.notification_id
                where li.league_id = :league and n.recipient_id = :manager
            ) foo2
SQL;

        $command = _app()->db->createCommand($query);
        $command->bindValue(':league', $league_id);
        $command->bindValue(':manager', $manager_id);

        $dataReader = $command->queryRow();
        return $dataReader['mc'] > 0;
    }

    /**
     * @param int $reason The reason constant.
     * @return string The reason text.
     */
    public function cannotInviteReason($reason) {
        switch ($reason) {
            case self::ERR_ALREADY_JOINED:
                return _t('The manager has already joined the league.');

            case self::ERR_ALREADY_INVITED:
                return _t('The manager has already been invited to the league.');
        }
        return '';
    }

    /**
     * @param int $type The constant for the type of message.
     * @return string
     */
    public function cannotAcceptReasons($type) {
        switch ($type) {
            case self::ERR_BANNED:
                return _t('You cannot join this league because you are banned.');
                break;

            case self::ERR_NO_FREE_SPACES:
                return _t('You cannot join this league because it if full.');
                break;

            case self::ERR_MAX_LEAGUES_REACHED:
                return _t('You cannot join this league because you have reached the maximum number of leagues you can join.');
                break;
        }
        return '';
    }

    /**
     * @param int $type The constant for the type of message.
     * @return string
     */
    public function cannotRejectReasons($type) {
        switch ($type) {
            case self::ERR_BANNED:
                return _t('You cannot reject this invitation because you are banned.');
                break;
        }
        return '';
    }

    /**
     * Deletes the invitation sent to a manager for a league.
     * @param int $league_id
     * @param int $manager_id
     * @return boolean whether the deletion is successful.
     */
    public function clearManagerInvitations($league_id, $manager_id) {

        // Use find cause it should only bring one invitation back anyway.
        // Note: We do not delete the notification (which would remove the invitation as foreign key) on purpose.
        // The relevant notification will be deleted onBeforeDelete callback.

        $invitation = CustomLeagueInvitation::model()->find(array(
            'join' => 'join mod_notifications n on n.id = t.notification_id',
            'condition' => 'n.recipient_id = :rid and t.league_id = :lid',
            'params' => array(
                ':rid' => $manager_id,
                ':lid' => $league_id
            )
            ));

        return $invitation ? $invitation->delete() : true;
    }

    /**
     * Deletes the invitation sent for this league.
     * @param int $league_id
     * @return int Number of invitations deleted.
     */
    public function clearLeagueInvitations($league_id) {

        // Note: We do not delete the notification (which would remove the invitation as foreign key) on purpose.
        // The relevant notification will be deleted onBeforeDelete callback.

        $invitations = CustomLeagueInvitation::model()->findAll(array(
            'join' => 'join mod_notifications n on n.id = t.notification_id',
            'condition' => 't.league_id = :lid',
            'params' => array(
                ':lid' => $league_id
            )
            ));
        foreach ($invitations as $i) {
            $i->delete();
        }

        return count($invitations);
    }

}

?>
