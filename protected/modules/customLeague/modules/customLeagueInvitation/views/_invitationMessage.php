<?php

if ($league->privacy == CustomLeaguePrivacy::TYPE_PUBLIC) {
    echo _t('{user} invites you to join the league: {league}', array(
        '{user}' => _l($manager->username, _url('profile/index', array('id' => $manager->id))),
        '{league}' => _l($league->name, _url('customLeague/league', array('id' => $league->id))),
    ));
} else {
    echo _t('{user} invites you to join the private league: {league}', array(
        '{user}' => _l($manager->username, _url('profile/index', array('id' => $manager->id))),
        '{league}' => $league->name,
    ));
}
?>