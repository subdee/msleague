<?php
echo CHtml::openTag('li');

echo _l(_t('Invite'), '#', array(
    'class' => 'button invite',
    'onclick' => CJavaScript::encode("js:$('#invite-dialog').dialog('open'); return false;"),
));

$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'invite-dialog',
    'options' => array(
        'title' => _t('Invite a manager to your league'),
        'autoOpen' => false,
        'width' => 500,
        'modal' => true,
        'draggable' => false,
        'resizable' => false,
        'buttons' => array(
            _t('Send invitation') => 'js:inviteManagerToLeague',
            _t('Cancel') => 'js:function(){$(this).dialog("close");}',
        ),
    ),
));

$this->widget('Notice', array('id' => 'inviteNotice', 'ajax' => true));

echo CHtml::beginForm('', 'post', array(
    'id' => 'invite-form',
    'onSubmit' => 'inviteManagerToLeague(); return false;',
    'class' => 'popup new-league',
));

echo CHtml::openTag('div', array('class' => 'row'));
echo CHtml::label(_t('Username'), 'username');
$this->widget('CAutoComplete', array(
    'name' => 'username',
    'value' => '',
    'url' => _url('customLeague/customLeagueInvitation/default/recipientsList'),
    'max' => 10,
    'minChars' => 2,
    'delay' => 500,
    'matchCase' => false,
    'inputClass' => 'textfield',
//    'htmlOptions' => array('size' => '16'),
));
echo CHtml::closeTag('div');
echo CHtml::hiddenField('league_id', $league->id);

echo CHtml::endForm();
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>
<script type="text/javascript" >
    function inviteManagerToLeague() {
<?php
echo CHtml::ajax(array(
    'url' => _url('customLeague/customLeagueInvitation/default/invite'),
    'data' => "js:$('#invite-form').serialize()",
    'type' => 'post',
    'dataType' => 'json',
    'success' => "function(response) {
        if(response.status == 'success') {
            $('#invite-dialog').dialog('close');
            $('#leagueNotice').notice(response.notice);
        } else {
            $('#inviteNotice').show();
            $('#inviteNotice').notice(response);
        }
    }",
    'beforeSend' => "function(){
        $('#leagueSessionNotice').hide();
        $('#inviteNotice').hide();
    }",
));
?>
    }
</script>

<?php echo CHtml::closeTag('li'); ?>