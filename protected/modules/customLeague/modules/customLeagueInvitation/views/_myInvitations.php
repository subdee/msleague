<?php if (!empty($invitations)) : ?>

    <?php

//    $this->widget('Notice', array(
//        'id' => 'show-my-invitations',
//        'message' => _l(_t('You have {n} invitations.', array('{n}' => count($invitations))), '#'),
//        'type' => 'info',
////        'class' => 'small',
//    ));

    echo CHtml::openTag('div', array('id' => 'my-invitations'));
    foreach ($invitations as $invitation) {

        $reason = CustomLeagueInvitation::model()->canAccept($invitation->league, _manager());
        $this->widget('notification.components.NotificationNotice', array(
            'notification' => $invitation->notification,
            'type' => 'question',
            'buttons' => array(
                'accept' => array(
                    'enabled' => $reason > 0,
                    'htmlOptions' => array(
                        'title' => $reason <= 0 ? CustomLeagueInvitation::model()->cannotAcceptReasons($reason) : _t('Accept'),
                    ),
                ),
                'reject' => array(
                    'enabled' => $reason > 0,
                    'htmlOptions' => array(
                        'title' => $reason <= 0 ? CustomLeagueInvitation::model()->cannotRejectReasons($reason) : _t('Reject'),
                    ),
                ),
            ),
        ));
    }
    echo CHtml::closeTag('div');
    ?>
    <script type="text/javascript">

        $(document).ready(function(){
            $(document).bind('onNotificationSuccess', function(event, response){
                if(response.action != 'accept') return;

                // Refresh my and public leagues
                $.fn.yiiGridView.update('my-leagues');
                $.fn.yiiGridView.update('public-leagues');
            })

            $('#show-my-invitations').click(function(){
                $(this).hide();
                $('#my-invitations').show('blind');
                return false;
            })

            $(document).bind('onNotificationRemove', function(){
                $('#my-invitations:empty').hide();
            })
        })
    </script>
<?php endif; ?>