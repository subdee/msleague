<?php

echo _t('You have joined the league {league}.', array(
    '{league}' => _l($invitation->league->name, _url('customLeague/league', array('id' => $invitation->league->id)))
));
?>
