<?php

$this->widget('Notice', array('id' => 'leagueSessionNotice', 'session' => 'league'));
$this->widget('Notice', array('id' => 'leagueNotice', 'ajax' => true));

echo CHtml::openTag('div', array('class' => 'league-table'));

if ($league->isMember(_manager()->id)) {
    echo CHtml::openTag('div', array('class' => 'table-outer-header'));
    echo CHtml::tag('span', array('class' => 'manager-rank'), _t('Your rank: {rank}', array('{rank}' => CHtml::tag('span', array(), $league->rankForManager(_manager()->id)))));
    echo CHtml::closeTag('div');
}
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'league-managers',
    'dataProvider' => $managers->search(),
    'pager' => array(
        'class' => 'CLinkPager',
        'header' => '',
    ),
    'rowCssClassExpression' => '$data->manager_id == _manager()->id ? (($row&1 ? "even" : "odd") . " owner") : ($row&1 ? "even" : "odd")',
    'template' => '{items}{pager}',
    'columns' => array(
        array(
            'header' => _t('Rank'),
            'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
            'htmlOptions' => array('class' => 'first-column'),
        ),
        array(
            'type' => 'raw',
            'value' => 'CHtml::image($data->manager->profilePhoto())',
            'htmlOptions' => array('class' => 'profile-image'),
        ),
        array(
            'type' => 'raw',
            'name' => 'manager_id',
            'value' => '$data->manager->teamLink() . CHtml::tag("div", array(), $data->manager->profileLink() . "&nbsp;" . ($data->manager->profile->line ? "&quot;" . $data->manager->profile->line . "&quot;" : ""))',
            'htmlOptions' => array('class' => 'manager'),
        ),
        'manager.total_points',
        array(
            'value' => '$data->league->canBeKicked($data->manager->id) ? _l("' . _t('Kick') . '", _url("customLeague/kick", array("league" => $_GET["id"], "manager" => $data->manager_id)), array("confirm" => "' . _t('Are you sure you want to kick this manager from your league?') . '")) : ""',
            'type' => 'raw',
            'visible' => $league->canKick(_manager()->id),
        ),
    ),
));

echo CHtml::closeTag('div');
?>