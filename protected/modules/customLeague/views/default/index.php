<?php
$this->widget('Notice', array('session' => 'leagues'));
Utils::triggerEvent('onCustomLeaguesPageTop', $this);
?>

<div class="leagues-page">
    <div id="left-profile-column">
        <?php


        if ($myLeagues) {
            $this->widget('LeagueTable', array(
                'id' => 'my-leagues',
                'header' => _t('My leagues'),
                'leagues' => $myLeagues,
                'showNewLeagueButton' => true,
                'emptyTableMessage' => _t('You have not joined any group yet.'),
            ));
        }

        $allLeagues->type = CustomLeagueType::TEAM;
        $this->widget('LeagueTable', array(
            'id' => 'fan-leagues',
            'header' => _t('Fan leagues'),
            'leagues' => $allLeagues,
            'maxShow' => 5,
        ));
        ?>
    </div>

    <div id="right-profile-column">
        <?php
        $allLeagues->type = array(CustomLeagueType::USER, CustomLeagueType::SYSTEM);
        $this->widget('LeagueTable', array(
            'id' => 'public-leagues',
            'header' => _t('Public leagues'),
            'leagues' => $allLeagues,
            'emptyTableMessage' => _t('There are no leagues.'),
            'search' => true,
            'showLastJoined' => true,
        ));
        ?>
    </div>

</div>