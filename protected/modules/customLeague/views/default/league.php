<?php if (_manager()->hasRole(Role::TEMPORARY)) : ?>
    <?php
    $this->widget('Notice', array(
        'message' => _t('In order to have access to this league you need to {url}.', array(
            '{url}' => CHtml::link(_t('create your team'), _url('createTeam/index'))
        )),
        'type' => 'info',
    ));
    ?>
<?php else : ?>

    <div class="profile-usermenu league-page">
        <ul class="meta">
            <li>
                <?php
                if ($league->canEdit(_manager())) {
                    echo CHtml::tag('div', array(
                        'id' => 'name',
                        'class' => 'editable name',
                        ), $league->name
                    );
                }
                else
                    echo CHtml::tag('div', array('class' => 'name'), $league->name);
                ?>
            </li>
        </ul>
    </div>

    <div id="left-profile-column">
        <div class="profile">
            <div class="information">
                <?php
                // Creator
                echo CHtml::openTag('div');
                echo CHtml::tag('div', array(), _t('Creator:'));
                if ($league->type == CustomLeagueType::USER) {
                    echo $league->creator->profileLink();
                } else {
                    echo _gameName();
                }
                echo CHtml::closeTag('div');
//
//                // Type
//                echo CHtml::openTag('div');
//                echo CHtml::tag('div', array(), _t('Type:'));
//                echo _t('{type} league', array('{type}' => $league->typeToText()));
//                echo CHtml::closeTag('div');

                // Privacy
                echo CHtml::openTag('div');
                echo CHtml::tag('div', array(), _t('Privacy:'));
                echo _t('{type} league', array('{type}' => $league->privacyToText()));
                echo CHtml::closeTag('div');

                // Members
                echo CHtml::openTag('div');
                echo CHtml::tag('div', array(), _t('Members:'));
                echo $league->numOfMembers();
                echo CHtml::closeTag('div');

                // Date created
                echo CHtml::openTag('div');
                echo CHtml::tag('div', array(), _t('Created:'));
                echo Utils::date($league->date_created, array('time' => false, 'dayAbbreviated' => false, 'monthAbbreviated' => false));
                echo CHtml::closeTag('div');
                ?>
            </div>
            <div class="links bottom">
                <ul>
                    <?php
                    Utils::triggerEvent('onCustomLeagueViewActions', $this, array('league' => $league));

                    if ($league->canDelete(_manager())) {
                        echo CHtml::openTag('li');
                        echo _l(_t('Delete'), _url('customLeague/delete', array('id' => $league->id)), array(
                            'class' => 'button delete',
                            'confirm' => _t('Delete this league? All members will be removed from the league.'),
                        ));
                        echo CHtml::closeTag('li');
                    } else if ($league->canViewDelete(_manager()->id)) {
                        echo CHtml::openTag('li');
                        echo CHtml::tag('span', array('class' => 'button delete', 'title' => _t('You cannot delete the league as you are banned.')), _t('Delete'));
                        echo CHtml::closeTag('li');
                    }

                    $ret = $league->canJoin(_manager());
                    if ($ret > 0) {
                        echo CHtml::openTag('li');
                        echo _l(_t('Join'), _url('customLeague/join', array('id' => $league->id)), array('class' => 'button join'));
                        echo CHtml::closeTag('li');
                    } else if ($league->canViewJoin(_manager()->id)) {
                        echo CHtml::openTag('li');
                        echo CHtml::tag('span', array('class' => 'button join', 'title' => CustomLeague::model()->cannotJoinReason($ret)), _t('Join'));
                        echo CHtml::closeTag('li');
                    }

                    if ($league->canLeave(_manager())) {
                        echo CHtml::openTag('li');
                        echo _l(_t('Leave'), _url('customLeague/leave', array('id' => $league->id)), array(
                            'class' => 'button leave',
                            'confirm' => _t('Are you sure you want to leave this league?'),
                        ));
                        echo CHtml::closeTag('li');
                    } else if ($league->canViewLeave(_manager()->id)) {
                        echo CHtml::openTag('li');
                        echo CHtml::tag('span', array('class' => 'button leave', 'title' => _t('You cannot leave the league as you are banned.')), _t('Leave'));
                        echo CHtml::closeTag('li');
                    }
                    ?>
                </ul>
            </div>
        </div>
        <?php Utils::triggerEvent('onLeaguePageSidebarBottom', $this, array('league' => $league)); ?>
    </div>

    <div id="right-profile-column">
        <?php
        $this->renderPartial('_leagueManagersTable', array(
            'league' => $league,
            'managers' => $managers,
        ));
        Utils::triggerEvent('onCustomLeaguePageBottom', $this, array('league' => $league));
        ?>
    </div>

    <?php if ($league->canEdit(_manager())) : ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#name').editable('edit', {
                    cssclass: 'inline-edit',
                    indicator : '<?php echo CHtml::image(Utils::imageUrl('ajaxloader/small-circle.gif')); ?>',
                    submit  : 'OK',
                    //                    onblur : 'ignore',
                    width: 350,
                    autowidth: false,
                    autoheight: false,
                    submitdata: {league : <?php echo $league->id; ?>},
                    maxlength: 50,
                    tooltip : '<?php echo _t('Click to edit'); ?>'
                });
            })
        </script>
    <?php endif; ?>
<?php endif; ?>