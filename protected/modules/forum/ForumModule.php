<?php

class ForumModule extends WebModule {

    public $tableNamePrefix = 'mod_forum_';
    public $models = array('categories', 'categories_text', 'discussions', 'comments');
    public $maxShowDiscussionsPerUser;
    public $maxLoadDiscussionsPerUser;
    public $showInMenu = true;

    /**
     * @var string Forum uploaded images will end up here
     */
    public $uploadImagePath = 'images/uploads/forum';

    public function init() {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'forum.models.*',
            'forum.components.*',
        ));

        // Register events
        Utils::registerCallback('onAfterReportManager', $this, 'reportComment');
        Utils::registerCallback('onCalculateBurned', $this, 'addCriteriaCondition');

        if ($this->showInMenu) {
            Utils::registerCallback('onTopMenuEnd', $this, 'renderMenuItem');
            Utils::registerCallback('onFrontpageBoxLeft2', $this, 'renderFrontpageBox');
            Utils::registerCallback('onProfileInfoSection', $this, 'renderProfileComments');
            Utils::registerCallback('onAdminCommentIcon', $this, 'renderCommentIcon');
        }
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        }
        else
            return false;
    }

    /**
     * @param CEvent $e
     */
    public function renderFrontpageBox(CEvent $e) {
        $e->sender->widget('FrontpageBox');
    }

    /**
     * @param CEvent $e
     */
    public function reportComment(CEvent $e) {
        if (isset($e->params['comment_id'])) {
            $comment = Comment::model()->findByPk($e->params['comment_id']);
            if (!$comment->report($e->params['report_id']))
                throw new CValidateException($comment);
        }
    }

    /**
     *
     * @param CEvent $e
     */
    public function renderMenuItem(CEvent $e) {
        if (!(_user()->isGuest || _manager()->hasRole(Role::TEMPORARY))) {
            $e->sender->pushItem(array(
                'url' => 'forum/index',
                'img' => array('forum', 'mainMenu.png'),
                'title' => _t('Forum')
            ));
        }
    }

    /**
     * @param CEvent $e
     */
    public function renderProfileComments(CEvent $e) {
        $e->sender->renderPartial('forum.views._profileComments', array(
            'discussions' => CategoryDiscussion::model()->discussionsForUser($e->params['manager_id'])
        ));
    }

    public function renderCommentIcon(CEvent $e) {
        $e->sender->renderPartial('forum.views._adminMatchCommentIcon', array(
            'match_id' => $e->params['match_id']
        ));
    }

    public function addCriteriaCondition(CEvent $e) {
        $criteria = $e->params['criteria'];

        $criteria->addCondition(<<<SQL
        (
            select (count(comm.id)/datediff(now(),mt.created_on)+1) from mod_forum_comments comm left join (manager m2 left join manager_team mt on mt.id = m2.manager_team_id) on m2.id = comm.commentor_id where t.id = m2.id
        ) >= :burnedComments
SQL
        );
        $criteria->params[':burnedComments'] = $e->params['burnedComments'];
    }

}
