<?php

class CommentCountBubble extends CWidget {

    public $discussion_id = 0;

    public function run() {
        Utils::registerModuleCssFile('forum', 'commentCountBubble');
        $this->render('commentCountBubble', array(
            'count' => Comment::model()->countByAttributes(array('discussion_id' => $this->discussion_id, 'visible' => true)),
        ));
    }

}

?>