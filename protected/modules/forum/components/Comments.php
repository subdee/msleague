<?php

class Comments extends CWidget {

    /**
     * @var int The discussion to load comments from
     */
    public $discussionId;

    /**
     * @var string The url to go to after a successfull comment post.
     */
    public $returnUrl;

    /**
     * @var int Comments per page.
     */
    public $pageSize = 10;

    /**
     * @var bool Whether to show the comments or not when the discussion is closed.
     */
    public $showCommentsWhenClosed = true;

    /**
     * @var boolean Custom open comments condition.
     */
    public $openCondition = true;

    /**
     * @var string Text to warn a user that he cannot comment because he is banned.
     */
    public $bannedUserMessage = null;

    /**
     * @var string Text to inform temporary manager.
     */
    public $tempUserMessage = null;

    /**
     * @var string Text to inform that comments have been disabled.
     */
    public $commentsDisabledMessage = null;

    /**
     * @var string Error message for custom condition failure.
     */
    public $commentsClosedCustomMessage = null;

    /**
     * @var string Text to inform that there are no comments.
     */
    public $noCommentsMessage = null;

    /**
     * @var boolean Show the discussion thread header.
     */
    public $showHeader = false;

    /**
     * @var string Header text.
     */
    public $header = null;

    /**
     * @var boolean Show the number of comments in a bubble.
     */
    public $showCommentCount = true;

    /**
     * @var string Extra classes.
     */
    public $cssClass = '';

    /**
     * @var boolean True to show all comments.
     */
    public $showHidden = false;

    /**
     * @var string
     */
    public $submitButtonText = null;

    /**
     * @var string
     */
    public $textAreaPrompt = null;

    /**
     * @var string
     */
    public $endOfScrollMessage = null;

    /**
     * @var boolean
     */
    public $nullCommentorCondition = false;

    /**
     * @var string
     */
    public $nullCommentorTemplate = '';

    /**
     * @var boolean Whether to show the reason the comments are closed.
     */
    public $showClosedReason = true;

    /**
     * @var string The event to call after message has been posted.
     */
    public $onSuccessEvent = null;

    /**
     * @var string Empty comment validation message.
     */
    public $emptyCommentValidationMessage = null;

    /**
     * @var array Extra criteria for comment loading.
     */
    public $criteria = array();

    /**
     *
     */
    public function init() {
        $defaultMessages = array(
            'bannedUserMessage' => _t('You cannot comment because you are banned.'),
            'tempUserMessage' => _t('In order to comment on you need to {url} first.', array('{url}' => _l(_t('create your team'), _url('createTeam/index')))),
            'commentsDisabledMessage' => _t('Comments are closed.'),
            'commentsClosedCustomMessage' => _t('Comments are closed.'),
            'noCommentsMessage' => _t('No comments yet.'),
            'header' => _t('Comments'),
            'submitButtonText' => _t('Post'),
            'textAreaPrompt' => _t('Write a comment...'),
            'endOfScrollMessage' => _t('There are no more comments.'),
            'emptyCommentValidationMessage' => _t('You must type a comment.')
        );
        foreach ($defaultMessages as $key => $value) {
            if ($this->$key === null) {
                $this->$key = $value;
            }
        }

        _app()->user->setReturnUrl($this->returnUrl);
    }

    /**
     *
     */
    public function run() {

        Utils::registerModuleCssFile('forum', 'comments');

        $discussion = Discussion::model()->findByPk($this->discussionId);

        if (!$discussion)
            throw new CException("Invalid discussion ({$this->discussionId}) for comment component");

        $newComment = new Comment;
        $newComment->contentValidationMessage = $this->emptyCommentValidationMessage;
        $newComment->content = $this->textAreaPrompt;

        $canComment = $this->_commentsEnabled($discussion);

        if ($canComment['status'] && isset($_POST['Comment'])) {

            try {
                $trans = _app()->db->beginTransaction();
                $discussion->createComment($_POST['Comment'], $newComment);

                // Trigger event
                if ($this->onSuccessEvent !== null) {
                    Utils::triggerEvent($this->onSuccessEvent, $this, array('comment' => $newComment));
                }

                $trans->commit();
                _return();
            } catch (CDbException $e) {
                Debug::logToWindow($e->getMessage(), 'forum comment');
                $trans->rollback();
            } catch (CValidateException $e) {
                $trans->rollback();
            }
        }

        // Build selection criteria
        $criteria = new CDbCriteria;
        $criteria->addCondition('discussion_id = :did');
        $criteria->params[':did'] = $discussion->id;

        if (!$this->showHidden) {
            $criteria->addCondition('visible');
        }

        $criteria->order = 'date_posted DESC';

        $criteria->mergeWith($this->criteria);

        // results per page
        $pages = new CPagination(Comment::model()->count($criteria));
        $pages->pageSize = $this->pageSize;
        $pages->applyLimit($criteria);

        // Get comments

        $this->render('comments', array(
            'newComment' => $newComment,
            'comments' => Comment::model()->findAll($criteria),
            'pages' => $pages,
            'commentsEnabled' => $canComment,
        ));
    }

    /**
     * @param Discussion $discussion
     * @return array Whether the comments are closed and a relative message
     */
    private function _commentsEnabled(Discussion $discussion) {
        if (!$this->openCondition) {
            return array(
                'status' => false,
                'notice' => array(
                    'message' => $this->commentsClosedCustomMessage,
                    'type' => 'info',
                    'class' => 'small',
                )
            );
        }

        if (_manager()->hasRole(Role::TEMPORARY)) {
            return array(
                'status' => false,
                'notice' => array(
                    'message' => $this->tempUserMessage,
                    'type' => 'info',
                )
            );
        }

        if (_manager()->isBanned()) {
            return array(
                'status' => false,
                'notice' => array(
                    'message' => $this->bannedUserMessage,
                    'type' => 'warning',
                    'class' => 'small',
                )
            );
        }

        if (!$discussion->comments_open) {
            return array(
                'status' => false,
                'notice' => array(
                    'message' => $this->commentsDisabledMessage,
                    'type' => 'info',
                    'class' => 'small',
                )
            );
        }

        return array('status' => true);
    }

}

?>
