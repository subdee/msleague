<?php

class ForumProfileSection extends CWidget {

    public $discussions;

    public function run() {
        Utils::registerModuleCssFile('forum', 'forumProfileSection');
        $this->render('forumProfileSection');
    }

}

?>