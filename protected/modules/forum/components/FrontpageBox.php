<?php

class FrontpageBox extends WidgetBox {

    public $discussionsToLoad = 5;

    public function init() {
        $this->view = 'frontpageBox';
        $this->title = _t('Forum Discussions');
        $this->name = 'forum';
        $this->params = array(
            'discussions' => CategoryDiscussion::model()->excludeInvisible()->limit($this->discussionsToLoad)->dateUpdated()->findAll()
        );
        $this->more = array(
            'label' => _t('Forum'),
            'url' => _url('forum/index'),
        );

        Utils::registerModuleCssFile('forum', 'frontpageBox');
        parent::init();
    }

}

?>