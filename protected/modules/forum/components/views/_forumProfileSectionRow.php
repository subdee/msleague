<?php
$cutSize = 40;
?>
<tr class="<?php echo $class; ?>">
    <td class="first">
        <div class="title">
            <?php
            if (isset($discussion->title[$cutSize])) {
                $discussion->title = mb_substr($discussion->title, 0, $cutSize) . '...';
            }
            ?>
            <?php echo CHtml::link($discussion->title, _url('forum/comments', array('id' => $discussion->discussion_id))); ?>
        </div>
        <div class="description">
            <?php echo $discussion->shortDescription($cutSize); ?>
        </div>
        <?php $this->render('forum.views._discussionDetails', array('discussion' => $discussion)); ?>
    </td>
</tr>