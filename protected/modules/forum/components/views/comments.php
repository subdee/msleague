<?php
if ($this->showHeader) {
    echo CHtml::openTag('div', array('class' => 'comments-title'));
    echo CHtml::tag('span', array(), $this->header);
    if ($this->showCommentCount) {
        $this->widget('CommentCountBubble', array('discussion_id' => $this->discussionId));
    }
    echo CHtml::closeTag('div');
}

echo CHtml::openTag('div', array(
    'id' => "widget-comments-{$this->discussionId}",
    'class' => "widget-comments {$this->cssClass}",
));
if ($commentsEnabled['status']) {
    $form = $this->beginWidget('CActiveForm', array(
        'htmlOptions' => array(
            'onSubmit' => '$(this).find(":submit").attr("disabled", true);',
            'class' => 'new-comment'
        )
        ));

    echo CHtml::openTag('div', array('class' => 'row'));
    echo $form->textArea($newComment, 'content');
    echo $form->error($newComment, 'content');
    echo CHtml::closeTag('div');
    echo CHtml::submitButton($this->submitButtonText);
    $this->endWidget();
} elseif ($this->showClosedReason) {
    $this->widget('Notice', $commentsEnabled['notice']);
}
?>

<?php $this->widget('Notice', array('session' => 'report')); ?>
<?php $this->widget('Notice', array('session' => 'reportDebug')); ?>

<?php if ($this->showCommentsWhenClosed) : ?>
    <?php if (count($comments) > 0) : ?>
        <div class="comments">
            <?php $counter = 0; ?>
            <?php foreach ($comments as $comment) : ?>
                <?php
                $class = $counter & 1 ? 'even' : 'odd';
                ?>
                <div class="comment <?php echo $class; ?>">
                    <div class="avatar">
                        <?php echo $comment->commentor ? CHtml::image($comment->commentor->profilePhoto()) : CHtml::image(Utils::imageUrl('profile/empty_profile.png')); ?>
                    </div>
                    <div>
                        <div class="details">
                            <?php
                            if ($comment->commentor) {
                                echo $comment->commentor->profileLink();
                            } elseif ($this->nullCommentorCondition) {
                                echo $this->nullCommentorTemplate;
                            } else {
                                echo _t('(deleted user)');
                            }
                            echo ' ';
                            echo Utils::relativeDatetime($comment->date_posted);
                            ?>
                        </div>
                        <div class="content">
                            <?php echo $comment->content; ?>
                        </div>
                        <?php if ($comment->canBeReported(_manager())) : ?>
                            <div class="options">
                                <?php echo Report::reportLink(_t('Report'), $comment->commentor_id, 'report', 'forum', array('comment_id' => $comment->id)); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <?php $counter++; ?>
            <?php endforeach; ?>
        </div>
        <?php
        $this->widget('ext.yiinfinite-scroll.YiinfiniteScroller', array(
            'contentSelector' => "#widget-comments-{$this->discussionId} .comments",
            'itemSelector' => "#widget-comments-{$this->discussionId} .comment",
            'pages' => $pages,
            'loading' => array(
                'img' => Utils::ajaxLoaderUrl(),
                'msgText' => _t('Loading...'),
                'finishedMsg' => $this->endOfScrollMessage,
            ),
        ));
        ?>
    <?php else : ?>
        <p><em><?php echo $this->noCommentsMessage; ?></em></p>
    <?php endif; ?>
<?php endif; ?>
<?php echo CHtml::closeTag('div'); ?>
<script type="text/javascript">
    $(document).ready(function(){
<?php if (isset($_POST['Comment'])) : ?>
            $('.new-comment textarea').css({
                'color' : 'black',
                'height' : '50px'
            });
<?php else : ?>
            var focused = false;
            $('.new-comment textarea').focus(function(){
                if(focused) return;
                $(this).css({
                    'color' : 'black',
                    'height' : '50px'
                })
                .val('');
                focused = true;
            });
            $('.new-comment :submit').click(function(){
                if(!focused) {
                    $('.new-comment textarea').focus();
                    return false;
                }
                return true;
            });
<?php endif; ?>
    });
</script>