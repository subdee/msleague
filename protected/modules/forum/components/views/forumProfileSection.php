<?php
$discussionCount = count($this->discussions);
$discussionsToShow =
    $discussionCount < _app()->getModule('forum')->maxShowDiscussionsPerUser ?
    $discussionCount :
    _app()->getModule('forum')->maxShowDiscussionsPerUser;
?>

<?php if ($discussionCount > 0) : ?>
    <table class="discussions">
        <?php
        for ($key = 0; $key < $discussionsToShow; $key++) {
            $class = 'odd';
            if ($key & 1)
                $class = 'even';
            $this->render('_forumProfileSectionRow', array('discussion' => $this->discussions[$key], 'class' => $class));
        }
        ?>
    </table>
        <?php if ($discussionCount > $discussionsToShow) : ?>
        <table class="discussions hidden">
            <?php
            for ($key = $discussionsToShow; $key < $discussionCount; $key++) {
                $class = 'odd';
                if ($key & 1)
                    $class = 'even';
                $this->render('_forumProfileSectionRow', array('discussion' => $this->discussions[$key], 'class' => $class));
            }
            ?>
        </table>
        <div class="infolink more">
        <?php echo CHtml::link(_t('More discussions'), '#'); ?>
        </div>
    <?php endif; ?>
<?php else : ?>
    <table
        <tr>
            <td coldspan="2">
    <?php echo _t('No comments posted yet'); ?>
            </td>
        </tr>
    </table>
<?php endif; ?>