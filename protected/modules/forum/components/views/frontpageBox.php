<?php $count = count($discussions); ?>
<?php if ($count > 0) : ?>
    <ul>
        <?php $counter = 0; ?>
        <?php foreach ($discussions as $discussion) : ?>
            <?php
            $class = $counter & 1 ? 'even' : 'odd';
            if ($counter == 0)
                $class .= ' first';
            elseif ($counter == $count - 1)
                $class .= ' last';
            ?>
            <li class="<?php echo $class; ?>">
                <div class="title">
                    <?php echo CHtml::link($discussion->title, _url('forum/comments', array('id' => $discussion->discussion_id))); ?>
                    <?php if (!$discussion->discussion->comments_open) : ?>
                        <span class="closed"><?php echo _t('(closed)'); ?></span>
                    <?php endif; ?>
                </div>
                <div class="description">
                    <?php echo $discussion->shortDescription(40); ?>
                </div>
                <?php $this->render('forum.views._discussionDetails', array('discussion' => $discussion)); ?>
            </li>
            <?php $counter++; ?>
        <?php endforeach; ?>
    </ul>
<?php else : ?>
    <p><em><?php echo _t('No discussions yet'); ?></em></p>
<?php endif; ?>