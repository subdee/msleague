<?php

class DefaultController extends Controller {

    public function filters() {
        return array('accessControl');
    }

    public function accessRules() {
        return array(
            array(
                'deny',
                'expression' => '!_module()->showInMenu',
            ),
            array(
                'allow',
                'expression' => 'Utils::isLoggedIn() && _manager()->hasRole(Role::MANAGER | Role::RESET)',
            ),
            array(
                'deny', // deny all users
                'users' => array('*')),
        );
    }

    public function init() {
        parent::init();
        Utils::registerModuleCssFile('forum', 'forum');
        _setPageTitle(_t('Forum'));
    }

    public function actionIndex() {
        $categories = Category::model()->with('text')->findAll('visible AND text.lang = "' . _app()->language . '"');
        $this->render('index', array(
            'categories' => $categories,
        ));
    }

    public function actionDiscussions($id) {

        $category = Category::model()->findByPk($id);
        if (!$category || !$category->visible) {
            throw new CHttpException('404');
        }

        $validated = false;
        $categoryDiscussion = new CategoryDiscussion;
        if (!_manager()->isBanned() && isset($_POST['CategoryDiscussion'])) {

            try {
                $trans = _app()->db->beginTransaction();

                $category->createDiscussion($_POST['CategoryDiscussion'], $_POST['Comment'], $categoryDiscussion);

                $trans->commit();
                _redirect('forum/discussions', array('id' => $id));
            } catch (CDbException $e) {
                Debug::logToWindow($e->getMessage());
                $trans->rollback();
            } catch (CValidateException $e) {
                $trans->rollback();
            }
            $validated = true;
        }

        $criteria = new CDbCriteria;
        $criteria->addCondition('category_id = :cid AND visible');
        $criteria->params[':cid'] = $id;
        $criteria->order = 'date_updated desc';
        $criteria->with = array('discussion');

        $count = CategoryDiscussion::model()->count($criteria);
        $discussions = CategoryDiscussion::model()->findAll($criteria);

        // results per page
        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);

        $pageTitle = _t('Forum - {category}', array('{category}' => $category->text->title));
        _setPageTitle($pageTitle);
        $this->render('discussions', array(
            'discussions' => $discussions,
            'newDiscussion' => $categoryDiscussion,
            'validated' => $validated,
            'pages' => $pages,
        ));
    }

    public function actionComments($id) {
        $discussion = CategoryDiscussion::model()->findByPk($id);
        if (!$discussion || !$discussion->category->visible || !$discussion->discussion->visible) {
            throw new CHttpException('404');
        }

        $pageTitle = _t('Forum - {category}', array('{category}' => $discussion->category->text->title));
        _setPageTitle($pageTitle . ' - ' . $discussion->title);
        $this->render('comments', array(
            'pageTitle' => $pageTitle,
            'discussion' => $discussion,
        ));
    }

}
