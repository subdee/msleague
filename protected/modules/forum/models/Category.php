<?php

/**
 * This is the model class for table "mod_forum_categories".
 *
 * The followings are the available columns in table 'mod_forum_categories':
 * @property integer $id
 * @property integer $rank
 * @property string $image
 * @property boolean $visible
 *
 * The followings are the available model relations:
 * @property CategoryText $text
 * @property CategoryDiscussion[] $discussions
 */
class Category extends CActiveRecord {

    public $uploadImage;

    /**
     * Returns the static model of the specified AR class.
     * @return Category the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'mod_forum_categories';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('rank', 'required'),
            array('rank', 'numerical', 'integerOnly' => true),
            array('image', 'length', 'max' => 64),
            array('uploadImage', 'file',
                'types' => 'jpg,gif,png',
                'maxSize' => 102400,
                'tooLarge' => _t('File was too large'),
                'allowEmpty' => true,
            ),
            array('visible', 'boolean'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, rank, image, visible', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'text' => array(self::HAS_ONE, 'CategoryText', 'category_id', 'condition' => 'lang = :lang', 'params' => array(':lang' => _app()->language)),
            'discussions' => array(self::HAS_MANY, 'CategoryDiscussion', 'category_id', 'together' => false),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'rank' => 'Rank',
            'image' => 'Image',
            'visible' => 'Visible',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->select = 'category.id, image, visible, title, description, rank';
        $criteria->alias = 'category';
        $criteria->join = 'LEFT JOIN mod_forum_categories_text category_text ON category.id = category_text.category_id';

        $criteria->compare('id', $this->id);
        $criteria->compare('rank', $this->rank);
        $criteria->compare('image', $this->image, true);
//        $criteria->compare('visible', $this->visible);

        $criteria->addCondition('category_text.lang = :lng');
        $criteria->params[':lng'] = _app()->language;

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'sort' => false,
                'pagination' => false,
            ));
    }

    /**
     *
     */
    public function init() {
        parent::init();
        $this->attachEventHandler('onBeforeDelete', array($this, 'deleteDiscussions'));
    }

    /**
     * Removes all discussions from this category.
     */
    protected function deleteDiscussions() {
        foreach ($this->discussions as $discussion) {
            $discussion->delete();
        }
    }

    /**
     * Default find and order by rank.
     */
    public function defaultScope() {
        return array(
            'order' => 'rank asc',
        );
    }

    /**
     * Returns the number of discussions for this category.
     * @return int
     */
    public function discussionCount() {
        return CategoryDiscussion::model()->count(array(
                'with' => array('discussion'),
                'condition' => 't.category_id = :cid and discussion.visible',
                'params' => array(':cid' => $this->id),
            ));
    }

    /**
     * Returns the number of visible comments for all discussions.
     * @return int
     */
    public function commentCount() {
        return Comment::model()->count(array(
                'join' => '
                    left join mod_forum_discussions d on t.discussion_id = d.id
                    left join mod_forum_category_discussions fcd on fcd.discussion_id = d.id
                    left join mod_forum_categories c on fcd.category_id = c.id',
                'condition' => 'fcd.category_id = :cid and d.visible and t.visible',
                'params' => array(':cid' => $this->id),
            ));
    }

    /**
     * Returns the last posted comment within the category.
     * @return Comment
     */
    public function latestComment() {
        return Comment::model()->find(array(
                'join' => '
                    left join mod_forum_discussions d on t.discussion_id = d.id
                    left join mod_forum_category_discussions fcd on fcd.discussion_id = d.id
                    left join mod_forum_categories c on fcd.category_id = c.id',
                'condition' => 'fcd.category_id = :cid and d.visible and t.visible = true',
                'params' => array(':cid' => $this->id),
                'limit' => 1,
                'order' => 'date_posted desc',
            ));
    }

    /**
     * Updates the remaining category ranks.
     * @return boolean
     */
    public function beforeDelete() {
        if (parent::beforeDelete()) {

            $criteria = new CDbCriteria;
            $criteria->addCondition('rank > :rank');
            $criteria->params[':rank'] = $this->rank;
            $this->updateCounters(array('rank' => -1), $criteria);

            return true;
        }
        return false;
    }

    /**
     * Deletes category image.
     */
    public function afterDelete() {
        parent::afterDelete();
        if ($this->image && file_exists($this->uploadImage()))
            @unlink($this->uploadImage());
    }

    /**
     * @return boolean True for successfull edit.
     */
    public function edit() {
        if ($this->getIsNewRecord() || $this->text === null) {
            $this->text = new CategoryText;
        }

        if (isset($_POST['CategoryText']))
            $this->text->attributes = $_POST['CategoryText'];

        if (isset($_POST['Category'])) {

            $trans = _app()->db->beginTransaction();
            try {

                $this->attributes = $_POST['Category'];

                if ($this->text->getIsNewRecord()) {
                    $this->rank = $this->getMaxRank() + 1;
                }
                if (!$this->save()) {
                    throw new CValidateException($this);
                }

                $this->text->attributes = $_POST['CategoryText'];
                $this->text->description = nl2br($this->text->description);
                if ($this->text->getIsNewRecord()) {
                    $this->text->category_id = $this->id;
                    $this->text->lang = _app()->language;
                }
                if (!$this->text->save()) {
                    throw new CValidateException($this->text);
                }

                $this->uploadImage = CUploadedFile::getInstance($this, 'uploadImage');
                if ($this->uploadImage) {
                    $this->image = "forum_category_{$this->id}." . $this->uploadImage->getExtensionName();
                    $this->saveAttributes(array('image'));

                    if (!$this->uploadImage->SaveAs($this->uploadImage())) {
                        throw new CException('Image upload failed');
                    }
                }

                $trans->commit();
                return true;
            } catch (CDbException $e) {
                _app()->user->setFlash('error', $e->getMessage());
            } catch (CValidateException $e) {

            } catch (CException $e) {
                _app()->user->setFlash('error', $e->getMessage());
            }
            $trans->rollback();
        }

        return false;
    }

    /**
     * @return string the category image real path.
     */
    public function uploadImage() {
        $path = rtrim(_app()->getModule('forum')->uploadImagePath, '/');
        return _bp('../' . $path . '/categories/' . $this->image);
    }

    /**
     * @return string the category image url.
     */
    public function uploadImageUrl() {
        $alias = 'images/uploads/forum/categories/' . $this->image;
        return _bu($alias);
    }

    /**
     * Update all catetory ranks.
     * @param int $offset
     */
    public function updateRank($offset) {
        $newRank = $this->rank + $offset;
        $this->updateAll(array('rank' => $this->rank), 'rank = :rank', array(':rank' => $newRank));
        $this->rank = $newRank;
        $this->saveAttributes(array('rank'));
    }

    /**
     * @return int Max category rank.
     */
    public function getMaxRank() {
        return $this->findBySql('select max(rank) as rank from mod_forum_categories')->rank;
    }

    /**
     * Creates a forum discussion thread.
     *
     * @param array $discussion
     * @param array $metadata
     * @param CategodyDiscusion $discussion
     *
     * @return boolean True for success
     */
    public function createDiscussion($discussion, $metadata, $model = null) {

        // Create the main model
        if (!$model)
            $model = new CategoryDiscussion;

        // Create discussion
        if ($model->discussion->save()) {

            // Create the first comment
            $model->metadata->attributes = $metadata;
            $model->metadata->visible = false;
            $model->metadata->discussion_id = $model->discussion->id;
            $model->metadata->commentor_id = _manager()->id;
            $model->metadata->date_posted = _app()->localtime->getLocalNow('Y-m-d H:i:s');
            if ($model->metadata->save()) {

                // Attach discussion to category
                $model->attributes = $discussion;
                $model->category_id = $this->id;
                $model->discussion_id = $model->discussion->id;
                $model->metadata_id = $model->metadata->id;
                if ($model->save()) {
                    return true;
                }
                else
                    throw new CValidateException($model);
            }
            else
                throw new CValidateException($model->metadata);
        }
        else
            throw new CValidateException($model->discussion);

        return false;
    }

}