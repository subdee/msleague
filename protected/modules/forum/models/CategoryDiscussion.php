<?php

/**
 * This is the model class for table "mod_forum_category_discussions".
 *
 * The followings are the available columns in table 'mod_forum_category_discussions':
 * @property integer $category_id
 * @property integer $discussion_id
 * @property string $title
 * @property integer $metadata_id
 *
 * The followings are the available model relations:
 * @property Category $category
 * @property Discussion $discussion
 */
class CategoryDiscussion extends CActiveRecord {

    /**
     * @var string Used only by search.
     */
    public $creatorUsername;

    /**
     * @var string Used only by search.
     */
    public $dateCreated;

    /**
     * @var string Used only by search.
     */
    public $dateUpdated;

    /**
     * @var int Used only by search.
     */
    public $commentCount;

    /**
     * Returns the static model of the specified AR class.
     * @return CategoryDiscussion the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'mod_forum_category_discussions';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('category_id, discussion_id, title, metadata_id', 'required', 'message' => _t('The field cannot be empty.')),
            array('category_id, discussion_id, metadata_id', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 64),
            array('title', 'isAllowed'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('creatorUsername', 'safe'),
            array('category_id, discussion_id, title, metadata_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
            'discussion' => array(self::BELONGS_TO, 'Discussion', 'discussion_id'),
            'metadata' => array(self::BELONGS_TO, 'Comment', 'metadata_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'category_id' => 'Category',
            'discussion_id' => 'Discussion',
            'title' => _t('Discussion'),
            'metadata_id' => 'Metadata',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;
        $criteria->compare('category_id', $this->category_id);
        $criteria->compare('discussion_id', $this->discussion_id);
        $criteria->compare('commentor.username', $this->creatorUsername, true);

        $criteria->compare('title', $this->title, true);
        $criteria->compare('metadata.content', $this->title, true, 'OR');

        $criteria->with = array(
            'metadata',
            'metadata.commentor' => array('select' => 'username'),
            'discussion',
        );

        // Add select comment count
        $criteria->select = '*, (SELECT COUNT(id) FROM mod_forum_comments WHERE discussion_id = t.discussion_id) as commentCount';

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'sort' => array(
                    'attributes' => array(
                        'commentCount' => array(
                            'asc' => 'commentCount',
                            'desc' => 'commentCount desc',
                        ),
                        'dateCreated' => array(
                            'asc' => 'metadata.date_posted',
                            'desc' => 'metadata.date_posted desc',
                        ),
                        'dateUpdated' => array(
                            'asc' => 'discussion.date_updated',
                            'desc' => 'discussion.date_updated desc',
                        ),
                    ),
                    'defaultOrder' => array(
                        'dateUpdated' => true
                    ),
                ),
                'pagination' => array(
                    'pageSize' => 20,
                ),
            ));
    }

    /**
     * Set the metadata after construction.
     */
    protected function afterConstruct() {
        parent::afterConstruct();

        if ($this->getIsNewRecord()) {
            $this->discussion = new Discussion;
            $this->metadata = new Comment;
        }
    }

    /**
     * @return bool
     */
    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->title = trim($this->title);
            $this->title = strip_tags($this->title);
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public function scopes() {
        return array(
            'dateUpdated' => array(
                'join' => 'left join mod_forum_discussions d on t.discussion_id = d.id',
                'condition' => 'd.visible',
                'order' => 'date_updated DESC',
            ),
            'dateCreated' => array(
                'join' => 'left join mod_forum_comments c on c.discussion_id = t.discussion_id',
                'condition' => 'c.visible',
                'order' => 'date_posted DESC',
            )
        );
    }

    /**
     * @param type $limit
     * @return Discussion
     */
    public function limit($limit=null) {
        if (is_null($limit))
            $limit = _app()->getModule('forum')->maxLoadDiscussionsPerUser;
        $this->getDbCriteria()->mergeWith(array(
            'limit' => $limit,
        ));
        return $this;
    }

    /**
     * @return Discussion
     */
    public function excludeInvisible() {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => 'category_id IN (select id from mod_forum_categories where visible)'
        ));
        return $this;
    }

    /**
     *
     * @param type $attribute
     * @param type $params
     */
    public function isAllowed($attribute, $params) {
        if (!Utils::isPhraseAllowed($this->$attribute)) {
            $this->addError($attribute, _t('Inappropriate words used.'));
        }
    }

    /**
     * Returns all the discussions that the user has either initiated or participates in.
     * @param int $id
     * @return CActiveRecord
     */
    public function discussionsForUser($id) {
        return $this->findAll(array(
                'select' => '
                    *,
                    max(co.date_posted) as date_posted,
                    max(ifnull(date_posted, d.date_updated)) as datemax',
                'join' => '
                    left join mod_forum_comments co on t.discussion_id = co.discussion_id
                    left join mod_forum_discussions d on t.discussion_id = d.id
                    left join mod_forum_categories ca on t.category_id = ca.id',
                'condition' => '(ca.visible is true and d.visible is true and co.commentor_id = :mid)',
                'params' => array(':mid' => $id),
                'group' => 't.discussion_id',
                'order' => 'datemax desc',
                'limit' => _app()->getModule('forum')->maxLoadDiscussionsPerUser,
            ));
    }

    /**
     * @return int
     */
    public function metadata_id() {
        return $this->metadata->id;
    }

    /**
     * @return string
     */
    public function description() {
        return $this->metadata->content;
    }

    /**
     * @return Manager
     */
    public function owner() {
        return $this->metadata->commentor;
    }

    /**
     * @return int
     */
    public function owner_id() {
        return $this->metadata->commentor_id;
    }

    /**
     * @return string
     */
    public function date_created() {
        return $this->metadata->date_posted;
    }

    /**
     * @param Manager $manager
     * @return boolean Can manager report this discussion.
     */
    public function canBeReported(Manager $manager) {
        return ($this->owner_id() && $manager->id != $this->owner_id() && !$manager->isBanned());
    }

    /**
     * @param int $cutSize
     * @return string
     */
    public function shortDescription($cutSize) {
        $len = mb_strlen($this->description());
        if ($len > $cutSize)
            return mb_substr($this->description(), 0, $cutSize) . '...';
        return $this->description();
    }

    /**
     * @param bool $includeHidden True to include the hidden comments as well
     * @return int The number of comments without the metadata
     */
    public function realCommentCount($includeHidden = false) {
        return $this->discussion->commentCount($includeHidden) - ($includeHidden ? 1 : 0);
}

    /**
     *
     * @param type $comment_id
     * @return Discussion
     */
    public function getCommentDiscussion($comment_id) {
        $commentDiscussion = $this->model()->find('metadata_id = :id',array(':id' => $comment_id));
        if ($commentDiscussion)
            return $commentDiscussion->discussion;
        return false;
    }

}

?>