<?php

/**
 * This is the model class for table "mod_forum_comments".
 *
 * The followings are the available columns in table 'mod_forum_comments':
 * @property integer $id
 * @property string $content
 * @property integer $discussion_id
 * @property integer $commentor_id
 * @property string $date_posted
 * @property boolean $visible
 *
 * The followings are the available model relations:
 * @property Discussion $discussion
 * @property Manager $commentor
 */
class Comment extends CActiveRecord {

    public $contentValidationMessage;

    /**
     * Returns the static model of the specified AR class.
     * @return Comment the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'mod_forum_comments';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('content', 'required', 'message' => $this->contentValidationMessage),
            array('discussion_id', 'required'),
            array('discussion_id, commentor_id', 'numerical', 'integerOnly' => true),
            array('visible', 'boolean'),
            array('content', 'length', 'max' => 600, 'tooLong' => _t('Your comment cannot exceed {max} characters.')),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, content, discussion_id, commentor_id, date_posted, visible', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'discussion' => array(self::BELONGS_TO, 'Discussion', 'discussion_id'),
            'commentor' => array(self::BELONGS_TO, 'Manager', 'commentor_id'),
        );
    }

    /**
     * @return array
     */
    public function scopes() {
        return array(
            'recent' => array(
                'order' => 'date_posted DESC',
//                'limit' => 5,
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'content' => _t('Description'),
            'discussion_id' => 'Discussion',
            'commentor_id' => 'Commentor',
            'date_posted' => 'Date Posted',
            'visible' => 'Visible',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('discussion_id', $this->discussion_id);
        $criteria->compare('commentor.username', $this->commentor_id, true);
        $criteria->compare('date(date_posted)', $this->date_posted, true);
//        $criteria->compare('visible', $this->visible);

        $criteria->with = array(
            'commentor' => array('select' => 'commentor.username')
        );

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
                'sort' => array(
                    'attributes' => array(
                        'date_posted' => array(
                            'asc' => 't.date_posted asc',
                            'desc' => 't.date_posted desc',
                        ),
                    ),
                    'defaultOrder' => array(
                        'date_posted' => false
                    ),
                ),
                'pagination' => array(
                    'pageSize' => 20,
                ),
            ));
    }

    /**
     *
     */
    public function init() {
        $this->contentValidationMessage = _t('You must type a comment.');
    }

    /**
     * @return bool
     */
    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->date_posted = Utils::fromLocalDatetime($this->date_posted);
            $this->content = trim($this->content);
            $this->content = strip_tags($this->content);
            return true;
        }
        return false;
    }

    /**
     *
     */
    protected function afterFind() {
        parent::afterFind();
        $this->date_posted = Utils::toLocalDatetime($this->date_posted);
        $this->content = nl2br($this->content);
        $this->content = Utils::replaceSmileys($this->content);
    }

    /**
     * If the comment is reported it makes sure the report is deleted as well.
     */
    protected function beforeDelete() {
        if (parent::beforeDelete()) {
            $reportedComment = CommentReported::model()->findByPk($this->id);
            if ($reportedComment) {
                return $reportedComment->report->delete();
            }
            return true;
        }
        return false;
    }

    /**
     * @param Manager $manager
     * @return boolean Can manager report this comment.
     */
    public function canBeReported(Manager $manager) {
        return ($this->commentor_id && $manager->id != $this->commentor_id && !$manager->isBanned());
    }

    /**
     * Checks if this comment is reported or not.
     * @return bool
     */
    public function isReported() {
        return CommentReported::model()->exists('comment_id = :mid', array(':mid' => $this->id));
    }

    /**
     * Creates a reported message.
     * @param int $report_id
     * @return bool
     */
    public function report($report_id) {
        $report = new CommentReported;
        $report->report_id = $report_id;
        $report->comment_id = $this->id;
        return $report->save();
    }

}