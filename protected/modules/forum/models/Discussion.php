<?php

/**
 * This is the model class for table "mod_forum_discussions".
 *
 * The followings are the available columns in table 'mod_forum_discussions':
 * @property integer $id
 * @property string $date_updated
 * @property integer $comments_open
 *
 * The followings are the available model relations:
 * @property Comment[] $comments
 */
class Discussion extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @return Discussion the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'mod_forum_discussions';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('comments_open', 'boolean'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, date_updated, comments_open', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'comments' => array(self::HAS_MANY, 'Comment', 'discussion_id', 'order' => 'date_posted DESC', 'together' => false),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'date_updated' => 'Date Updated',
            'comments_open' => 'Comment Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
//        $criteria->compare('date_updated', $this->date_updated, true);
//        $criteria->compare('comments_open', $this->comments_open);

        return new CActiveDataProvider(get_class($this), array(
                'criteria' => $criteria,
            ));
    }

    /**
     * @return boolean
     */
    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->date_updated = Utils::fromLocalDatetime($this->date_updated);
            return true;
        }
        return false;
    }

    /**
     *
     * @return type
     */
    protected function afterFind() {
        parent::afterFind();
        $this->date_updated = Utils::toLocalDatetime($this->date_updated);
    }

    /**
     * @param boolean $includeHidden
     * @param array $criteria
     *
     * @return Comment The latest comment of the thread.
     */
    public function lastComment($includeHidden = false, $criteria = array()) {
        $internal = new CDbCriteria;

        // Filter discussion
        $internal->addCondition('discussion_id = :did');
        $internal->params[':did'] = $this->id;

        // Filter hidden
        if (!$includeHidden) {
            $internal->addCondition('visible');
        }

        // Order
        $internal->order = 'date_posted desc';

        $internal->mergeWith($criteria);

        return Comment::model()->find($internal);
    }

    /**
     * @param boolean $includeHidden
     * @param array $criteria
     *
     * @return Comment The first comment of the thread.
     */
    public function firstComment($includeHidden = false, $criteria = array()) {
        $internal = new CDbCriteria;

        // Filter discussion
        $internal->addCondition('discussion_id = :did');
        $internal->params[':did'] = $this->id;

        // Filter hidden
        if (!$includeHidden) {
            $internal->addCondition('visible');
        }

        // Order
        $internal->order = 'date_posted asc';

        $internal->mergeWith($criteria);

        return Comment::model()->find($internal);
    }

    /**
     * @param bool $includeHidden True to include the hidden comments as well
     * @param array $criteria
     *
     * @return int The number of comments
     */
    public function commentCount($includeHidden = false, $criteria = array()) {
        $internal = new CDbCriteria;
        $internal->addCondition('discussion_id = :id');
        $internal->params[':id'] = $this->id;

        if (!$includeHidden)
            $internal->addCondition('visible');

        $internal->mergeWith($criteria);

        return Comment::model()->count($internal);
    }

    /**
     * Adds a comment to the discussion.
     *
     * @param array $data
     * @param Comment $data
     *
     * @return boolean True for success
     */
    public function createComment($data, $comment = null) {

        if (!$comment)
            $comment = new Comment;

        $comment->attributes = $data;
        $comment->commentor_id = _manager()->id;
        $comment->discussion_id = $this->id;

        if ($comment->save()) {
            $this->date_updated = _app()->localtime->getLocalNow('Y-m-d H:i:s');
            $this->save(false, array('date_updated'));
            return true;
        }
        else
            throw new CValidateException($comment);

        return false;
    }

}
