<td>
    <?php
    $disc = MatchDiscussion::model()->findByAttributes(array('game_id' => $match_id));
    if ($disc) :
        $image = $disc->discussion->comments_open ? AdminUtils::adminImageUrl('on_comment.png') : AdminUtils::adminImageUrl('off_comment.png');
        $title = $disc->discussion->comments_open ? 'Turn off' : 'Turn on';
        echo CHtml::link(CHtml::image($image, '', array('title' => "Turn comments {$title}")), AdminUtils::aUrl('game/toggleComments', array('id' => $match_id)));
    endif;
    ?>
</td>