<div class="details">
    <?php
    $commentCount = $discussion->discussion->commentCount();
    if ($commentCount > 0) {
        $lastComment = $discussion->discussion->lastComment();
        echo _t('{posts} comments, last comment {date} by {author}', array(
            '{posts}' => $commentCount,
            '{date}' => Utils::relativeDatetime($lastComment->date_posted),
            '{author}' => $lastComment->commentor ? $lastComment->commentor->profileLink() : _t('(deleted user)'),
        ));
    } else {
        echo _t('{posts} comments, created {date} by {author}', array(
            '{posts}' => $commentCount,
            '{date}' => Utils::relativeDatetime($discussion->date_created()),
            '{author}' => $discussion->owner() ? $discussion->owner()->profileLink() : _t('(deleted user)'),
        ));
    }
    ?>
</div>