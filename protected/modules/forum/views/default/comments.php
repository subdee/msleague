<?php echo CHtml::link(_t('&laquo; Back to discussions'), _url('forum/discussions', array('id' => $discussion->category->id)), array('class' => 'back')); ?>
<div class="forum">
    <div class="comments-discussion">
        <div class="avatar">
            <?php echo $discussion->owner() ? CHtml::image($discussion->owner()->profilePhoto()) : CHtml::image(Utils::imageUrl('profile/empty_profile.png')); ?>
        </div>
        <div>
            <h3><?php echo $discussion->title; ?></h3>
            <?php if ($discussion->canBeReported(_manager())) : ?>
                <div class="options">
                    <?php echo Report::reportLink(_t('Report'), $discussion->owner_id(), 'report', 'forum', array('comment_id' => $discussion->metadata_id())); ?>
                </div>
            <?php endif; ?>
            <div class="description">
                <?php echo $discussion->description(); ?>
            </div>
            <div class="details">
                <?php
                echo _t('{posts} comments, created {date} by {author}', array(
                    '{posts}' => $discussion->discussion->commentCount(),
                    '{date}' => Utils::relativeDatetime($discussion->date_created()),
                    '{author}' => $discussion->owner() ? $discussion->owner()->profileLink() : _t('(deleted user)'),
                ));
                ?>
            </div>
        </div>
    </div>
    <?php
    $this->widget('Comments', array(
        'discussionId' => $discussion->discussion_id,
        'returnUrl' => _url('forum/comments', array('id' => $discussion->discussion_id)),
    ));
    ?>
</div>