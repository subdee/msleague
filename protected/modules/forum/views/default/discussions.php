<?php echo CHtml::link(_t('&laquo; Back to forum categories'), _url('forum/index'), array('class' => 'back')); ?>
<div class="forum">
    <?php if (!_manager()->isBanned()) : ?>
        <?php if (!$validated) : ?>
            <div class="infolink more">
                <?php
                echo CHtml::link(_t('Start a new discussion'), '#', array(
                    'onclick' => '$(".forum form").show(); $(this).parent().hide(); return false;'
                ));
                ?>
            </div>
        <?php endif; ?>
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'htmlOptions' => array(
                'onSubmit' => '$(this).find(":submit").attr("disabled", true);',
                'class' => ($validated ? '' : 'hidden'),
            )
            ));
        ?>
        <div class="row">
            <?php echo $form->labelEx($newDiscussion, 'title'); ?>
            <?php echo $form->textField($newDiscussion, 'title'); ?>
            <?php echo $form->error($newDiscussion, 'title'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($newDiscussion->metadata, 'content'); ?>
            <?php echo $form->textArea($newDiscussion->metadata, 'content'); ?>
            <?php echo $form->error($newDiscussion->metadata, 'content'); ?>
        </div>
        <div class="row buttons">
            <?php echo CHtml::submitButton(_t('Open discussion')); ?>
        </div>
        <?php $this->endWidget(); ?>

    <?php endif; ?>
    <?php if (count($discussions) > 0) : ?>
        <?php
        $this->beginWidget('CHtmlPurifier');
        echo CHtml::openTag('ul', array('class' => 'discussions'));
        $counter = 0;
        foreach ($discussions as $discussion) {

            echo CHtml::openTag('li', array('class' => $counter & 1 ? 'even' : 'odd'));
            {
                // Title
                echo CHtml::openTag('div', array('class' => 'title'));
                {
                    echo CHtml::link($discussion->title, _url('forum/comments', array('id' => $discussion->discussion_id)));
                    if (!$discussion->discussion->comments_open)
                        echo CHtml::tag('span', array('class' => 'closed'), _t('(closed)'));
                }
                echo CHtml::closeTag('div');

                // Description
                echo CHtml::tag('div', array('class' => 'description'), $discussion->shortDescription(90));

                // Details
                $this->renderPartial('forum.views._discussionDetails', array('discussion' => $discussion));
            }
            echo CHtml::closeTag('li');
            $counter++;
        }
        echo CHtml::closeTag('ul');
        $this->endWidget();
        $this->widget('CLinkPager', array(
            'pages' => $pages,
            'header' => '',
        ));
        ?>
<?php else : ?>
        <p><em><?php echo _t('No discussions yet'); ?></em></p>
<?php endif; ?>
</div>
