<div class="forum">
    <table class="categories">
        <?php $counter = 0; ?>
        <?php foreach ($categories as $category) : ?>
            <?php $class = $counter & 1 ? 'even' : 'odd'; ?>
            <tr class="<?php echo $class; ?>">
                <td class="colImages"><?php echo CHtml::image($category->uploadImageUrl()); ?></td>
                <td class="colTitle">
                    <div class="title">
                        <?php if ($category->text) : ?>
                            <?php echo CHtml::link($category->text->title, _url('forum/discussions', array('id' => $category->id))); ?>
                        <?php endif; ?>
                    </div>
                    <div class="description">
                        <?php if ($category->text) : ?>
                            <?php echo $category->text->description; ?>
                        <?php endif; ?>
                    </div>
                </td>
                <td class="colDetails">
                    <ul>
                        <li>
                            <?php
                            echo _t('{n} discussions', array(
                                '{n}' => CHtml::tag('strong', array(), $category->discussionCount()),
                            ));
                            ?>
                        </li>
                        <li>
                            <?php
                            echo _t('{n} comments', array(
                                '{n}' => CHtml::tag('strong', array(), $category->commentCount()),
                            ));
                            ?>
                        </li>
                    </ul>
                </td>
            </tr>
            <?php $counter++; ?>
        <?php endforeach; ?>
    </table>
</div>