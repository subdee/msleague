<?php

class MatchDiscussionModule extends WebModule {

    public function init() {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'matchDiscussion.models.*',
            'matchDiscussion.components.*',
        ));

        // Register events
        Utils::registerCallback('onPageLeagueMatchBottom', $this, 'renderDiscussion');
        Utils::registerCallback('onBeforeMatchDelete', $this, 'onMatchDelete');
        Utils::registerCallback('onMatchListViewColumn3', $this, 'renderMatchListViewCommentCount');
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        }
        else
            return false;
    }

    /**
     * @param CEvent $e
     */
    public function renderDiscussion(CEvent $e) {
        $game = $e->params['game'];
        $discussion = MatchDiscussion::model()->findByAttributes(array('game_id' => $game->id));
        if ($discussion) {
            $e->sender->widget('Comments', array(
                'discussionId' => $discussion->discussion_id,
                'returnUrl' => _url('match/match', array('id' => $game->id)),
                'bannedUserMessage' => _t('You cannot comment on the match because you are banned.'),
                'commentsDisabledMessage' => _t('Comments have been disabled for this match.'),
                'showHeader' => true,
                'header' => _t('Match discussion'),
            ));
        }
        else
            throw new CException("Discussion missing for match({$game->id})");
    }

    /**
     * @param CEvent $e
     */
    public function onMatchDelete(CEvent $e) {
        $discussion = MatchDiscussion::model()->findByAttributes(array('game_id' => $e->sender->id));
        if ($discussion) {
            if ($discussion->discussion)
                $discussion->discussion->delete();
            else
                throw new CException("MatchDiscussion found with invalid discussion({$discussion->discussion_id})");
        }
//        else
//            throw new CException("MatchFound without discussion({$e->sender->id})");
    }

    /**
     * @param CEvent $e
     */
    public function renderMatchListViewCommentCount(CEvent $e) {
        $discussion = MatchDiscussion::model()->findByAttributes(array('game_id' => $e->params['data']->id));
        if ($discussion) {
            _controller()->widget('CommentCountBubble', array(
                'discussion_id' => $discussion->discussion_id
            ));
        }
        else
            throw new CException("Missing discussion for match({$e->params['data']->id})");
    }

}
