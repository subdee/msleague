<?php

/**
 * This is the model class for table "mod_match_discussions".
 *
 * The followings are the available columns in table 'mod_match_discussions':
 * @property integer $game_id
 * @property integer $discussion_id
 *
 * Relations
 * @property Game $game
 * @property Discussion $discussion
 */
class MatchDiscussion extends CActiveRecord {

    /**
     * @var int The team id. Used by admin for search.
     */
    public $team_id;

    /**
     * @var int Used only by search.
     */
    public $commentCount;

    /**
     * @var string Used only by search.
     */
    public $dateUpdated;

    /**
     * Returns the static model of the specified AR class.
     * @return MatchDiscussion the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'mod_match_discussions';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('game_id, discussion_id', 'required'),
            array('game_id, discussion_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('game_id, discussion_id', 'safe', 'on' => 'search'),
            array('team_id, commentCount', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'game' => array(self::BELONGS_TO, 'Game', 'game_id'),
            'discussion' => array(self::BELONGS_TO, 'Discussion', 'discussion_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'game_id' => 'Game',
            'discussion_id' => 'Discussion',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('game_id', $this->game_id);
        $criteria->compare('discussion_id', $this->discussion_id);
        $criteria->compare('game.team_home', $this->team_id);
        $criteria->compare('game.team_away', $this->team_id, false, 'OR');

        // For some reason, 'with' does not work
//        $critera->with = array(
//            'game',
//            'discussion'
//        );
        $criteria->join = 'LEFT OUTER JOIN mod_forum_discussions discussion ON discussion.id = t.discussion_id';
        $criteria->join .= ' LEFT OUTER JOIN game ON game.id = t.game_id';

        $criteria->select = '*, (SELECT COUNT(id) FROM mod_forum_comments c WHERE c.discussion_id = t.discussion_id) as commentCount';

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'sort' => array(
                    'attributes' => array(
                        'commentCount' => array(
                            'asc' => 'commentCount',
                            'desc' => 'commentCount desc',
                        ),
                        'dateUpdated' => array(
                            'asc' => 'discussion.date_updated',
                            'desc' => 'discussion.date_updated desc',
                        ),
                    ),
                    'defaultOrder' => array(
                        'dateUpdated' => true
                    ),
                ),
            ));
    }

    /**
     * Attaches a discussion thead to each match.
     * @return boolean True for success
     */
    public function attachMatchDiscussions() {
        // Begin transaction
        $trans = _app()->db->beginTransaction();

        try {

            // Only get matches that do not have a discussion thread attached to them yet.
            $matches = Game::model()->with('teamAway0', 'teamHome0')->findAll('t.id not in (SELECT game_id FROM mod_match_discussions)');

            foreach ($matches as $match) {

                // Create the discussion
                $discussion = new Discussion;
                if (!$discussion->save()) {
                    throw new CValidateException($discussion);
                    return false;
                }

                // Create the connection
                $connection = new MatchDiscussion;
                $connection->game_id = $match->id;
                $connection->discussion_id = $discussion->id;
                if (!$connection->save()) {
                    throw new CValidateException($connection);
                    return false;
                }
            }

            // Commit transaction
            $trans->commit();

            return true;
        } catch (CDbException $e) {
            Debug::logToWindow($e->getMessage(), 'update match discussions');
            $trans->rollback();
        } catch (CValidateException $e) {
            $trans->rollback();
        } catch (CException $e) {
            Debug::logToWindow($e->getMessage(), 'update match discussions');
            $trans->rollback();
        }
        return false;
    }

}

?>
