<?php

class MessageModule extends WebModule {

    public $tableNamePrefix = 'mod_message_';
    public $models = array('messages', 'reported');

    /**
     * Default in game messages.
     */
    const TYPE_MESSAGE = 0;

    /**
     * Emails.
     */
    const TYPE_EMAIL = 1;

    /**
     * @var string The type of message sent can either be 'messgae' or 'email'.
     */
    public $messageType = self::TYPE_MESSAGE;

    /**
     * @var int Used in afterProcessTeam to filter out reseted managers.
     */
    private $_managerRole;

    /**
     * @var array Behaviors for manager from message module.
     */
    public $managerRelations = array(
        'messageInbox' => array(CActiveRecord::HAS_MANY, 'Message', 'sender_id', 'together' => false),
        'messageOutbox' => array(CActiveRecord::HAS_MANY, 'Message', 'recipient_id', 'together' => false),
    );

    public function init() {
        parent::init();

        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'message.models.*',
            'message.components.*',
        ));

        // Attach module events to system
        _app()->eventSystem->attachBehavior('MessageModuleEvents', new MessageModuleEvents);

        // Insert menu item to manager box
        Utils::registerCallback('onManagerBoxMenu1', $this, 'renderManagerBoxMenuItem');
        Utils::registerCallback('onAfterReportManager', $this, 'reportMessage');
        Utils::registerCallback('onProfileLeftColumnLinksTop', $this, 'renderProfileSendMessageLink');
        Utils::registerCallback('onProfileLeftColumnLinksBottom', $this, 'renderProfileBanAndReportLinks');
        Utils::registerCallback('onBeforeProcessTeam', $this, 'saveManagerRole');
        Utils::registerCallback('onAfterProcessTeam', $this, 'sendWelcomeMessage');
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        }
        else
            return false;
    }

    /**
     * Renders the module's menu item.
     * @param CEvent $e
     */
    public function renderManagerBoxMenuItem(CEvent $e) {
        if (_app()->theme) {
            $view = Utils::themeViewsDirectory('message._managerBoxMenuItem');
        } else {
            $view = 'message.views._managerBoxMenuItem';
        }
        $e->sender->render($view, array(
            'messageCount' => Message::model()->getNumNewMessages(),
        ));
    }

    /**
     * @param CEvent $e
     */
    public function reportMessage(CEvent $e) {
        if (isset($e->params['message_id'])) {
            $message = Message::model()->findByPk($e->params['message_id']);
            if (!$message->report($e->params['report_id']))
                throw new CValidateException($message);
        }
    }

    /**
     * @param CEvent $e
     */
    public function renderProfileSendMessageLink(CEvent $e) {
        if (_app()->theme) {
            $view = Utils::themeViewsDirectory('message._profileLeftColumnTopLinks');
        } else {
            $view = 'message.views._profileLeftColumnTopLinks';
        }
        $e->sender->renderPartial($view, $e->params);
    }

    /**
     * @param CEvent $e
     */
    public function renderProfileBanAndReportLinks(CEvent $e) {
        if (_app()->theme) {
            $view = Utils::themeViewsDirectory('message._profileLeftColumnBottomLinks');
        } else {
            $view = 'message.views._profileLeftColumnBottomLinks';
        }
        $e->sender->renderPartial($view, $e->params);
    }

    /**
     * @param CEvent $e
     */
    public function sendWelcomeMessage(CEvent $e) {
        if ($this->_managerRole == Role::RESET)
            return;

        $welcomeMessage = StaticContent::model()->getText('WelcomeMessage');
        $subject = _t('Welcome');

        switch ($this->messageType) {
            case self::TYPE_MESSAGE:
                if (($ret = Message::model()->sendSystemMessage(_manager(), $subject, $welcomeMessage)) != Message::ERR_SEND_OK) {
                    error_log("Failed to send welcome message. (Return code: $ret)Db error: " . Debug::getSessionError());
                }
                break;

            case self::TYPE_EMAIL:
                if (!Utils::sendEmail(_manager()->email, $subject, $welcomeMessage)) {
                    Debug::notice('failedEmail', _t('Failed to send welcome message.') . "<br/><br/>$welcomeMessage", 'warning');
                    error_log('Failed to send welcome email to ' . _manager()->username);
                }
                break;
        }
    }

    /**
     * @param CEvent $e
     */
    public function saveManagerRole(CEvent $e) {
        $this->_managerRole = _manager()->role;
    }

}
