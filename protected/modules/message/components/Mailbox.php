<?php

class Mailbox extends CWidget {

    // [inbox], outbox
    public $type = 'inbox';
    public $page = 1;
    // either is inbox or outbox
    private $inbox = true;
    // array of messages
    private $messages;
    // total mailbox size
    private $size;
    // messages per page
    private $mpp = 10;
    private $next = 0;
    private $prev = 0;

    public function init() {
        // get mailbox size
        if ($this->type === 'outbox') {
            $this->size = Message::model()->getOutboxSize();
            $this->inbox = false;
        } else {
            $this->size = Message::model()->getInboxSize();
        }

        // get number of pages to show
        $numPages = ceil($this->size / $this->mpp);

        // sanitize current page
        if ($this->page > $numPages)
            $this->page = 1;
        if ($this->page < 1)
            $this->page = 1;

        // get mailbox messages
        $offset = ($this->page - 1) * $this->mpp;
        $params = array('limit' => $this->mpp, 'offset' => $offset);

        if ($this->inbox) {
            $this->messages = Message::model()->getInbox($params);
        } else {
            $this->messages = Message::model()->getOutbox($params);
        }

        // configure previous and next page markers
        if ($numPages > 1) {
            if ($this->page == 1) {
                $this->next = $this->page + 1;
            } elseif ($this->page == $numPages) {
                $this->prev = $this->page - 1;
            } else {
                $this->next = $this->page + 1;
                $this->prev = $this->page - 1;
            }
        }
    }

    public function run() {
        // configure min and max message number to show
        $min = ($this->page - 1) * $this->mpp + 1;
        $max = $min + $this->mpp - 1;
        if ($max > $this->size)
            $max = $this->size;
        if ($min > $this->size)
            $min = 0;

        $this->render('mailbox', array(
            'inbox' => $this->inbox,
            'messages' => $this->messages,
            'size' => $this->size,
            'min' => $min,
            'max' => $max,
            'page' => array(
                'prev' => $this->prev,
                'next' => $this->next,
            ),
            'src' => $this->inbox ? 'inbox' : 'outbox',
        ));
    }

}

?>
