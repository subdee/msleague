<div class="mailbox">
    <?php echo CHtml::form('delete'); ?>
    <table>
        <thead>
            <tr>
                <th class="check-delete">
                    <?php echo CHtml::checkBox('cb', false, array('id' => 'checkall')); ?>
                </th>
                <th class="subject"><?php echo _t('Subject'); ?></th>
                <th class="manager"><?php echo $inbox ? _t('From') : _t('To'); ?></th>
                <th class="date"><?php echo _t('Date'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($messages as $m) : ?>
                <?php $isNew = ($inbox) && !$m->has_been_opened; ?>
                <?php
                $classNew = '';
                if ($isNew) {
                    $classNew = 'unread';
                }
                ?>
                <tr class="<?php echo $classNew; ?>">
                    <td class="check-delete">
                        <?php echo CHtml::checkbox("cb[{$m->id}]", false, array('id' => $m->id, 'value' => $m->id)); ?>
                    </td>
                    <td class="subject">
                        <?php echo CHtml::link($m->subject, _url('message/view', array('id' => $m->id, 'src' => $src))); ?>
                        <?php if ($isNew) : ?>
                            - <span class="newtrans"><?php echo _t('new!'); ?></span>
                        <?php endif; ?>
                    </td>
                    <td class="manager">
                        <?php
                        if ($inbox && $m->sender != null)
                            echo CHtml::link($m->sender->username, _url('profile/index', array('id' => $m->sender_id)));
                        elseif (!$inbox && $m->recipient != null)
                            echo CHtml::link($m->recipient->username, _url('profile/index', array('id' => $m->recipient_id)));
                        elseif ($inbox && !$m->sender && $m->is_system_message)
                            echo CHtml::tag('span', array('class' => 'sendersystem'), _gameName());
                        else
                            echo _t('(deleted user)');
                        ?>
                    </td>
                    <td class="date"><?php echo Utils::date($m->date_sent); ?></td>
                </tr>
            <?php endforeach; ?>
            <?php if ($size == 0) : ?>
                <tr class="emptymailbox">
                    <td colspan="4"><?php echo _t('There are no messages.'); ?></td>
                </tr>
            <?php endif; ?>
            <tr class="footer">
                <td class="check-delete">
                    <input
                        type="image"
                        src="<?php echo Utils::moduleImageUrl('message', 'bin.png'); ?>"
                        disabled
                        />
                </td>
                <td colspan="2">
                    <?php
                    if ($size > 0)
                        echo _t('Showing messages {min}-{max} out of {size}', array('{min}' => $min, '{max}' => $max, '{size}' => $size));
                    ?>
                </td>
                <td class="pagenav">
                    <?php if ($size > 0) : ?>
                        <?php
                        if ($page['prev'])
                            echo CHtml::link('&laquo;', _url(_controller()->route, array('page' => $page['prev'])));
                        else
                            echo CHtml::tag('span', array(), '&laquo');

                        if ($page['next'])
                            echo CHtml::link('&raquo;', _url(_controller()->route, array('page' => $page['next'])));
                        else
                            echo CHtml::tag('span', array(), '&raquo');
                        ?>
                    <?php endif; ?>
                </td>
            </tr>
        </tbody>
    </table>
    <?php echo CHtml::hiddenField('src', _action_id()); ?>
    <?php echo CHtml::endForm(); ?>
</div>
<script>
    $(function () {
        $('#checkall').click(function () {
            $(".mailbox :checkbox").attr('checked', this.checked);
            if( $(".mailbox :checkbox:checked").size() > 0 ) {
                $(".mailbox .footer input").removeAttr('disabled').attr('src', MSL.ModuleImage('message', 'bin-full.png'));
            }
            else {
                $(".mailbox .footer input").attr('disabled', true).attr('src', MSL.ModuleImage('message', 'bin.png'));
            }
        });

        $(".mailbox :checkbox:not(#checkall)").click(function(){
            if( $(".mailbox :checkbox:checked").size() > 0 ) {
                $(".mailbox .footer input").removeAttr('disabled').attr('src', MSL.ModuleImage('message', 'bin-full.png'));
            }
            else {
                $(".mailbox .footer input").attr('disabled', true).attr('src', MSL.ModuleImage('message', 'bin.png'));
            }
        });
    });
</script>