<?php

class DefaultController extends Controller {

    public function filters() {
        return array('accessControl',);
    }

    public function accessRules() {
        return array(
            array('deny',
                'users' => array('?'),
            ),
            array('deny',
                'actions' => array('compose', 'recipientsList'),
                'expression' => array('Manager', 'isBanned')
            ),
            array('allow',
                'expression' => '_manager()->hasRole(Role::MANAGER | Role::RESET)',
            ),
            array('deny',
                'users' => array('*')
            )
        );
    }

    /**
     *
     * @return <type>
     */
    public function init() {
        parent::init();
        Utils::registerModuleCSSFile('message', 'styles');
        Utils::publishModuleAssetUrlToJS('message');
        _setPageTitle(_t('Messages'));
    }

    public function subMenuItems() {
        return array(
            array('index', _t('Inbox')),
            array('outbox', _t('Outbox')),
            array('compose', _t('New Message'), 'expression' => '!_manager()->isBanned()', 'onfail' => 'disable'),
        );
    }

    /**
     * Creates all the neccessary tables and models for the module to work.
     */
    public function actionInstall() {
        $tableNamePrefix = $this->module->tableNamePrefix;
        $installed = false;
        $error = '';

        $sql = array();

        // Messages table
        $sql['messages'] = <<<SQL
        CREATE TABLE IF NOT EXISTS {$tableNamePrefix}messages (
            id int NOT NULL AUTO_INCREMENT,
            sender_id int,
            recipient_id int,
            date_sent datetime NOT NULL,
            has_been_opened tinyint(1) DEFAULT '0' NOT NULL,
            subject varchar(45) NOT NULL,
            message text NOT NULL,
            is_visible_sender tinyint(1) DEFAULT '1' NOT NULL,
            is_visible_recipient tinyint(1) DEFAULT '1' NOT NULL,
            is_system_message tinyint(1) DEFAULT '0' NOT NULL, PRIMARY KEY (id),
            CONSTRAINT fk_message_recipient FOREIGN KEY (recipient_id) REFERENCES manager (id) ON DELETE SET NULL ON UPDATE CASCADE,
            CONSTRAINT fk_message_sender FOREIGN KEY (sender_id) REFERENCES manager (id) ON DELETE SET NULL ON UPDATE CASCADE,
            INDEX fk_message_sender (sender_id),
            INDEX fk_message_recipient (recipient_id)
            )
            ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL;

        // Reported messages table
        $sql['reported'] = <<<SQL
        CREATE TABLE IF NOT EXISTS {$tableNamePrefix}reported (
            id INT NOT NULL AUTO_INCREMENT,
            message_id INT NOT NULL,
            report_id INT NOT NULL,
            PRIMARY KEY (id),
            CONSTRAINT fk_reported_message FOREIGN KEY (message_id) REFERENCES {$tableNamePrefix}messages (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
            CONSTRAINT fk_reported_report FOREIGN KEY (report_id) REFERENCES report (id) ON DELETE CASCADE ON UPDATE CASCADE
            )
            ENGINE=InnoDB DEFAULT CHARSET=utf8
SQL;

        if (isset($_POST['install'])) {
            // Execute installation queries
            $connection = _app()->db;
            $transaction = $connection->beginTransaction();
            try {
                foreach ($sql as $query) {
                    $connection->createCommand($query)->execute();
                }
                $transaction->commit();
                $installed = true;
            } catch (CDbException $e) {
                $transaction->rollback();
                $error = Debug::exceptionDetailString($e);
            }
        }

        $this->render('install', array(
            'sql' => $sql,
            'installed' => $installed,
            'error' => $error,
            'prefix' => $tableNamePrefix,
            'models' => $this->module->models,
        ));
    }

    /**
     * Inbox page
     */
    public function actionIndex($page=1) {

        _setPageTitle(_t('Message inbox'));
        $this->render('messages', array(
            'mailboxType' => 'inbox',
            'pageTitle' => _t('Inbox'),
            'mailboxPage' => $page,
        ));
    }

    /**
     * Outbox page
     */
    public function actionOutbox($page=1) {

        _setPageTitle(_t('Message outbox'));
        $this->render('messages', array(
            'mailboxType' => 'outbox',
            'pageTitle' => _t('Outbox'),
            'mailboxPage' => $page,
        ));
    }

    /**
     *
     * @param <type> $id
     * @return <type>
     */
    public function actionView($id) {
        $msg = Message::model()->with('sender', 'recipient')->findByPk($id);
        if (!$msg) {
            throw new CHttpException('404');
        }


        if ($msg->sender_id == $msg->recipient_id || $msg->sender_id != _manager()->id)
            $msg->markAsRead();

        if ($msg->recipient_id && $msg->recipient_id != _manager()->id) {
            _setPageTitle(_t('Message "{title}" to {user}', array('{title}' => $msg->subject, '{user}' => $msg->recipient->username)));
        } elseif ($msg->sender_id && $msg->sender_id != _manager()->id) {
            _setPageTitle(_t('Message "{title}" from {user}', array('{title}' => $msg->subject, '{user}' => $msg->sender->username)));
        } else {
            _setPageTitle(_t('Message "{title}"', array('{title}' => $msg->subject)));
        }

        $this->render('view', array(
            'msg' => $msg,
            'canReply' => $msg->recipient_id == _manager()->id,
            'disabled' => _manager()->isBanned() ? 'disabled' : '',
            'src' => isset($_GET['src']) && $_GET['src'] == 'outbox' ? 'outbox' : 'inbox',
        ));
    }

    /**
     *
     * @param type $recipient_id
     */
    public function actionCompose($recipient_id=null) {

        $model = new Message('compose');
        $model->sender_id = _manager()->id;

        // Check if we have a default recipient
        if ($recipient_id !== null) {
            $model->recipient_id = $recipient_id;
            if ($model->recipient) {
                $model->recipientUsername = $model->recipient->username;
            }
        } elseif (isset($_POST['isReply'])) {

            $model->recipientUsername = $_POST['sender'];

            if ($_POST['isReply'] == 1) {
                $reply = Message::model()->findByPk($_POST['message_id']);
                if ($reply) {
                    if (mb_strstr($reply->subject, 'Re: ') === false) {
                        $model->subject = 'Re: ' . $reply->subject;
                    } else {
                        $model->subject = $reply->subject;
                    }

                    $model->message = str_replace('<br />', '', $reply->message);

                    $model->message = "\n\n\n________________________________\n";
                    $model->message.= _t('{manager} wrote', array('{manager}' => $reply->sender->username));
                    $model->message.= ":\n\n{$reply->message}";
                }
            }
        }

        if (isset($_POST['Message'])) {
            $model->attributes = $_POST['Message'];
            if ($model->send()) {
                Utils::notice('mail', _t('Your message has been sent succesfully.'), 'success');
                $this->redirect('outbox');
            }
        }

        _setPageTitle(_t('New message'));
        $this->render('compose', array(
            'model' => $model,
        ));
    }

    /**
     *
     */
    public function actionDelete() {

        $redirect = 'index';
        if (isset($_GET['id'])) {
            $m = Message::model()->findByPk($_GET['id']);
            if ($m) {
                if ($_GET['src'] == 'outbox') {
                    $redirect = 'outbox';
                    $key = 'is_visible_sender';
                } else {
                    $key = 'is_visible_recipient';
                }
                $m->$key = false;
                if (!$m->save(false, array($key)))
                    Utils::notice('mail', print_r($m->getErrors(), true), 'error', 'small');
                else
                    Utils::notice('mail', _t('Messages successfully deleted.'), 'success');
            }
        } elseif (isset($_POST['src'])) {
            $out = '';
            if ($_POST['src'] == 'outbox') {
                $redirect = 'outbox';
                $key = 'is_visible_sender';
            } else {
                $key = 'is_visible_recipient';
            }
            if (isset($_POST['cb']) && is_array($_POST['cb'])) {
                foreach ($_POST['cb'] as $msg) {
                    $m = Message::model()->findByPk($msg);
                    $m->$key = false;
                    if (!$m->save(false, array($key))) {
                        $out .= print_r($m->getErrors(), true);
                    }
                }
                if ($out !== '')
                    Utils::notice('mail', $out, 'error', 'small');
                else
                    Utils::notice('mail', _t('Messages successfully deleted.'), 'success');
            }
        }
        $this->redirect($redirect);
    }

    /**
     *
     */
    public function actionRecipientsList() {
        if (Yii::app()->request->isAjaxRequest && isset($_GET['q'])) {
            /* q is the default GET variable name that is used by the autocomplete widget to pass in user input  */

            $name = $_GET['q'];

            // this was set with the "max" attribute of the CAutoComplete widget
            $limit = min($_GET['limit'], 50);
            $criteria = new CDbCriteria;
            $criteria->select = 'username';
            $criteria->condition = "username LIKE :sterm AND role in (:role1, :role2) AND id != :mid";
            $criteria->params = array(
                ':sterm' => "%$name%",
                ':role1' => Role::MANAGER,
                ':role2' => Role::RESET,
                ':mid' => _manager()->id,
            );
            $criteria->limit = $limit;
            $userArray = Manager::model()->findAll($criteria);
            $returnVal = '';
            foreach ($userArray as $userAccount) {
                $returnVal .= $userAccount->getAttribute('username') . "\n";
            }
            echo $returnVal;
        }
    }

}