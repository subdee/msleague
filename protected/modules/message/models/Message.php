<?php

/**
 * This is the model class for table "mod_message_messages".
 *
 * The followings are the available columns in table 'mod_message_messages':
 * @property integer $id
 * @property integer $sender_id
 * @property integer $recipient_id
 * @property string $date_sent
 * @property integer $has_been_opened
 * @property string $subject
 * @property string $message
 * @property integer $is_visible_sender
 * @property integer $is_visible_recipient
 * @property integer $is_system_message
 *
 * The followings are the available model relations:
 * @property Manager $recipient
 * @property Manager $sender
 * @property ModMessageReported[] $modMessageReporteds
 */
class Message extends CActiveRecord {
    /**
     * Returns codes for send() method.
     */
    const ERR_SEND_OK = 0;
    const ERR_SEND_INVALID_SENDER = 1;
    const ERR_SEND_INVALID_RECIPIENT = 2;
    const ERR_SEND_UNAUTHORISED_RECIPIENT = 3;
    const ERR_SEND_BLOCKED_SENDER = 4;
    const ERR_SEND_MESSAGE_FAILED = 5;

    /**
     *
     * @var string The recipient's username.
     */
    public $recipientUsername = null;

    /**
     * Returns the static model of the specified AR class.
     * @return Message the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'mod_message_messages';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('recipientUsername', 'required', 'on' => 'compose'),
            array('recipientUsername', 'exist', 'className' => 'Manager', 'attributeName' => 'username', 'safe' => true, 'message' => _t('Manager does not exist.')),
            array('recipientUsername', 'validateRecipientUsername'),
            array('date_sent, subject, message', 'required'),
            array('recipient_id', 'required', 'on' => 'send'),
            array('sender_id, recipient_id', 'numerical', 'integerOnly' => true),
            array('subject', 'length', 'max' => 45),
            array('has_been_opened, is_visible_sender, is_visible_recipient, is_system_message', 'boolean'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, sender_id, recipient_id, date_sent, has_been_opened, subject, message, is_visible_sender, is_visible_recipient, is_system_message', 'safe', 'on' => 'search'),
            array('message', 'length', 'max' => 5000),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'recipient' => array(self::BELONGS_TO, 'Manager', 'recipient_id'),
            'sender' => array(self::BELONGS_TO, 'Manager', 'sender_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'sender_id' => 'Sender',
            'recipient_id' => _t('Recipient'),
            'date_sent' => 'Date Sent',
            'has_been_opened' => 'Has Been Opened',
            'subject' => _t('Subject'),
            'message' => _t('Message'),
            'is_visible_sender' => 'Is Visible Sender',
            'is_visible_recipient' => 'Is Visible Recipient',
            'is_system_message' => 'Is System Message',
            'recipientUsername' => _t('Recipient'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('sender_id', $this->sender_id);
        $criteria->compare('recipient_id', $this->recipient_id);
        $criteria->compare('date_sent', $this->date_sent, true);
        $criteria->compare('has_been_opened', $this->has_been_opened);
        $criteria->compare('subject', $this->subject, true);
        $criteria->compare('message', $this->message, true);
        $criteria->compare('is_visible_sender', $this->is_visible_sender);
        $criteria->compare('is_visible_recipient', $this->is_visible_recipient);
        $criteria->compare('is_system_message', $this->is_system_message);

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
    }

    /**
     *
     * @param type $attributes
     * @param type $params
     */
    public function validateRecipientUsername($attributes, $params) {
        if ($this->hasErrors())
            return;

        if ($this->recipientUsername !== null) {

            // Check if manager is messaging himself
            if ($this->recipientUsername == _manager()->username) {
                $this->addError('recipientUsername', _t('You cannot send a message to yourself.'));
            }

            // Check if manager is blocked by recipient.
            $this->recipient_id = Manager::model()->findByAttributes(array('username' => $this->recipientUsername))->id;
            if ($this->sender->isBlockedBy($this->recipient_id)) {
                $this->addError('recipientUsername', _t('You can\'t send a message to a user that has blocked you.'));
            }
        }
    }

    /**
     * @return boolean
     */
    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->date_sent = Utils::fromLocalDatetime($this->date_sent);
            $this->subject = strip_tags(trim($this->subject));

            if (!$this->is_system_message)
                $this->message = nl2br(strip_tags(trim($this->message)));

            return true;
        }
        return false;
    }

    /**
     *
     */
    protected function afterFind() {
        parent::afterFind();
        $this->date_sent = Utils::toLocalDatetime($this->date_sent);
    }

    /**
     * Marks a message as read by 'reading' itself and the parent of the thread.
     * @return bool
     */
    public function markAsRead() {
        $ok = false;
        if (!$this->has_been_opened) {
            $this->has_been_opened = true;
            $ok = $this->save(false);
            Utils::triggerEvent('onMessageViewFirstTime', $this, array('message' => $this));
        }
        Utils::triggerEvent('onMessageView', $this, array('message' => $this));

        return $ok;
    }

    /**
     * Checks if this message is reported or not.
     * Note: a message can only be logically reported by its recipient.
     * @return bool
     */
    public function isReported() {
        return ReportedMessage::model()->exists('message_id = :mid', array(':mid' => $this->id));
    }

    /**
     * Sends a message.
     * @param Manager $sender
     * @param Manager $recipient
     * @param string $subject
     * @param string $content
     * @return boolean
     */
    public function sendMessage(Manager $sender, Manager $recipient, $subject, $content) {
        if (!$sender) {
            return self::ERR_SEND_INVALID_SENDER;
        }

        if (!$recipient) {
            return self::ERR_SEND_INVALID_RECIPIENT;
        }

        if (!self::canReceiveMessage($recipient)) {
            return self::ERR_SEND_UNAUTHORISED_RECIPIENT;
        }

        if ($sender->isBlockedBy($recipient->id)) {
            return self::ERR_SEND_BLOCKED_SENDER;
        }

        Utils::triggerEvent('onMessageSend', $this, array(
            'sender' => $sender,
            'recipient' => $recipient,
        ));

        return $this->_send($sender->id, $recipient->id, $subject, $content, false, false);
    }

    /**
     * System messages are by default html.
     * @param Manager $recipient
     * @param string $subject
     * @param string $content
     * @return bool
     */
    public function sendSystemMessage(Manager $recipient, $subject, $content) {
        if (!$recipient) {
            return self::ERR_SEND_INVALID_RECIPIENT;
        }
        return $this->_send(null, $recipient->id, $subject, $content, true, true);
    }

    /**
     *
     * @param int $sender_id
     * @param int $recipient_id
     * @param string $subject
     * @param string $content
     * @param bool $system
     * @return bool
     */
    private function _send($sender_id, $recipient_id, $subject, $content, $isSystem, $isHtml) {
        $message = new Message('send');
        $message->date_sent = _app()->localtime->getLocalNow();
        $message->sender_id = $sender_id;
        $message->recipient_id = $recipient_id;
        $message->subject = $subject;
        $message->is_system_message = $isSystem;

        $message->message = trim($content);
        if (!$isHtml) {
            $message->message = nl2br(strip_tags($message->message));
        }

        if (!$message->save()) {
            Debug::setSessionObjectError($message);
            Debug::logToWindow($message->getErrors());
            return self::ERR_SEND_MESSAGE_FAILED;
        }

        return self::ERR_SEND_OK;
    }

    /**
     * Creates a reported message.
     * @param int $report_id
     * @return bool
     */
    public function report($report_id) {
        $report = new ReportedMessage;
        $report->report_id = $report_id;
        $report->message_id = $this->id;
        return $report->save();
    }

    /**
     * Used to send a message through the form.
     */
    public function send() {

        $this->date_sent = _app()->localtime->getLocalNow();
        if (!$this->save()) {
            Debug::logToWindow($this->getErrors(), 'message');
            return false;
        }

        return true;
    }

    /**
     * A user can receive a message if he is manager.
     * @param Manager $manager
     * @return bool
     */
    public static function canReceiveMessage(Manager $manager) {
        switch ($manager->role) {
            case Role::MANAGER:
            case Role::RESET:
                return true;
        }
        return false;
    }

    /**
     *
     * @param <type> $params
     * @return <type>
     */
    public static function getInbox($params=array()) {
        $criteria = array(
            'condition' => 'recipient_id = :id and is_visible_recipient = 1',
            'params' => array('id' => _user()->id),
            'order' => 'date_sent desc'
        );

        foreach ($params as $k => $v)
            $criteria[$k] = $v;

        return Message::model()->findAll($criteria);
    }

    /**
     *
     * @param type $params
     * @return type
     */
    public static function getOutbox($params=array()) {
        $criteria = array(
            'condition' => 'sender_id = :id and is_visible_sender = 1',
            'params' => array('id' => _user()->id),
            'order' => 'date_sent desc'
        );

        foreach ($params as $k => $v)
            $criteria[$k] = $v;

        return Message::model()->findAll($criteria);
    }

    /**
     *
     * @return type
     */
    public static function getInboxSize() {
        return Message::model()->count(array(
                'condition' => 'recipient_id = :id and is_visible_recipient = 1',
                'params' => array('id' => _user()->id),
            ));
    }

    /**
     *
     * @return type
     */
    public static function getOutboxSize() {
        return Message::model()->count(array(
                'condition' => 'sender_id = :id and is_visible_sender = 1',
                'params' => array('id' => _user()->id),
            ));
    }

    /**
     *
     * @return int
     */
    public static function getNumNewMessages() {

        return Message::model()->count('recipient_id = :id and has_been_opened = 0 and is_visible_recipient = 1', array(':id' => _manager()->id));
    }

}

?>