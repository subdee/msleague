<?php

if (_manager()->hasRole(Role::MANAGER | Role::RESET)) {
    echo CHtml::openTag('li');
    echo CHtml::image(Utils::moduleImageUrl('message', 'messages.png'));
    echo CHtml::openTag('a', array('href' => _url('message/index')));
    echo _t('Messages');
    if ($messageCount > 0)
        echo CHtml::tag('strong', array(), " ($messageCount)");
    echo CHtml::closeTag('a');
    echo CHtml::closeTag('li');
}
?>