<?php

if (_manager()->id != $manager->id) {
    if (_manager()->isBanned()) {
        if (_manager()->hasBlockedManager($manager->id)) {
            echo CHtml::tag('li', array(), CHtml::tag('span', array('title' => _t('You can\'t unblock a user because you are banned')), _t('Unblock user')));
        } else {
            echo CHtml::tag('li', array(), CHtml::tag('span', array('title' => _t('You can\'t block a user because you are banned')), _t('Block user')));
        }
    } else {
        if (_manager()->hasBlockedManager($manager->id)) {
            echo CHtml::tag('li', array(), _l(_t('Unblock user'), _url('managers/unblock', array('id' => $manager->id))));
        } else {
            echo CHtml::tag('li', array(), _l(_t('Block user'), _url('managers/block', array('id' => $manager->id))));
        }
    }
}
?>