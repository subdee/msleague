<?php $this->widget('Notice', array('session' => 'mail')); ?>
<div class="compose-message">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'htmlOptions' => array(
            'onSubmit' => '$(this).find(":submit").attr("disabled", true);',
        ),
        'focus' => array($model, 'recipientUsername')
        ));
    ?>

    <div class="row">
        <?php
        echo $form->labelEx($model, 'recipient_id');
        $this->widget('CAutoComplete', array(
            'name' => CHtml::activeName($model, 'recipientUsername'),
            'value' => $model->recipientUsername,
            'url' => array('recipientsList'),
            'max' => 10,
            'minChars' => 2,
            'delay' => 500,
            'matchCase' => false,
            'inputClass' => 'textfield',
            'htmlOptions' => array('size' => '16'),
        ));
        echo $form->error($model, 'recipientUsername');
        ?>
    </div>
    <div class="row">
        <?php
        echo $form->labelEx($model, 'subject');
        echo $form->textField($model, 'subject', array('maxlength' => 45));
        echo $form->error($model, 'subject');
        ?>
    </div>
    <div class="row">
        <?php
        echo $form->labelEx($model, 'message');
        echo $form->textArea($model, 'message', array('onkeyup' => 'onType();'));
        echo $form->error($model, 'message');
        ?>
    </div>
    <div class="row buttons">
        <?php
        echo CHtml::submitButton(_t('Send'));
        echo $form->hiddenField($model, 'sender_id');
        ?>
        <span class="character-count">
            <?php echo _t('{numChars} characters', array('{numChars}' => CHtml::tag('span', array('id' => 'numChars'), 0))); ?>
        </span>
    </div>
    <?php $this->endWidget(); ?>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        onType();
    });

    function onType() {
        var charLength = $('textarea').val().length;
        $('#numChars').html(charLength);
    }
</script>