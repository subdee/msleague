<div class="box_c">
    <div class="boxheader"><span class="title_c"><?php echo 'Message module installtion'; ?></span></div>
    <div class="boxmain_c">
        <div class="boxtext_c">
            <?php if($installed) : ?>
            <?php $this->widget('Notice', array('message' => 'Module installed ok', 'type' => 'success')); ?>
            <p>No automatic model generation took place. Go to Gii and generate the following models manually.</p>
            <ul>
                <?php foreach($models as $m) : ?>
                <li><?php echo $prefix.$m; ?></li>
                <?php endforeach; ?>
            </ul>
            <?php else : ?>
            <?php if(!empty($error)) : ?>
            <?php $this->widget('Notice', array('message' => $error, 'type' => 'error')); ?>
            <?php endif; ?>
            <p>The following sql will execute.</p>
            <?php foreach($sql as $query) : ?>
            <blockquote><?php echo nl2br($query); ?></blockquote>
            <?php endforeach; ?>
            <p>Continue?</p>
            <form method="post">
                <input type='submit' name="install" value="Yes" />
            </form>
            <?php endif; ?>
        </div>
    </div>
    <div class="boxfooter_c"><!-- empty--></div>
</div>