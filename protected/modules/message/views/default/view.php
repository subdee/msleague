<div class="profile-usermenu">
    <?php
    echo CHtml::tag('h3', array(), _t('View message'));
    ?>
</div>
<?php $this->widget('Notice', array('session' => 'report')); ?>
<?php $this->widget('Notice', array('session' => 'reportDebug')); ?>
<div class="view-message">
    <div class="manager">
        <label><?php echo $canReply ? _t('From') : _t('To'); ?>:</label>
        <?php
        if ($canReply && $msg->sender != null)
            echo $msg->sender->profileLink();
        elseif (!$msg->sender && $msg->is_system_message)
            echo CHtml::tag('span', array('class' => 'sendersystem'), _gameName());
        elseif (!$msg->sender)
            echo _t('(deleted user)');
        else
            echo $msg->recipient->username;
        ?>
    </div>
    <div class="subject">
        <label><?php echo _t('Subject'); ?>:</label>
        <?php echo $msg->subject; ?>
    </div>
    <div class="date">
        <label><?php echo _t('Date'); ?>:</label>
        <?php
        echo Utils::date($msg->date_sent, array(
            'dayAbbreviated' => false,
            'monthAbbreviated' => false
        ));
        ?>
    </div>
    <div class="actions">
        <?php
        echo _l(_t('Delete'), _url('message/delete', array('id' => $msg->id, 'src' => $src)), array(
            'class' => 'delete',
            'confirm' => _t('Are you sure you want to delete this message?'),
        ));
        if ($canReply && _manager()->id != $msg->sender_id && $msg->sender != null)
            echo Report::reportLink(_t('Report'), $msg->sender_id, 'report', 'message', array('message_id' => $msg->id), array('class' => 'report'));
        ?>
    </div>
    <div class="message">
        <?php
        $this->beginWidget('CHtmlPurifier');
        echo Utils::replaceSmileys($msg->message);
        $this->endWidget();
        ?>
    </div>
    <div class="buttons">
        <?php
        if ($canReply && $msg->sender) {
            echo CHtml::form(_url('message/compose'));
            echo CHtml::submitButton(_t('Reply'), array('disabled' => $disabled));
            echo CHtml::hiddenField('isReply', 1);
            echo CHtml::hiddenField('message_id', $msg->id);
            echo CHtml::hiddenField('sender', $msg->sender != null ? $msg->sender->username : _t('(deleted user)'));
            echo CHtml::endForm();
        } elseif ($msg->sender) {
            echo CHtml::form(_url('message/compose'));
            echo CHtml::submitButton(_t('Send again'), array('disabled' => $disabled));
            echo CHtml::hiddenField('isReply', 0);
            echo CHtml::hiddenField('sender', $msg->recipient->username);
            echo CHtml::endForm();
        }
        ?>
    </div>
</div>
