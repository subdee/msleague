
/**
 * Notification singleton
 */
Notification = {

    lastClickedId : 0,

    success : function(response){
        var notif = this.get(response.id);
        if(response.show) {
            notif.find('.error,.buttons,.loader').hide();
            notif.find('.message').html(response.show);
            notif.switchClass('question', 'info');
        }else if(response.redirect) {
            notif.find('.error,.buttons,.loader').hide();
            MSL.Redirect(response.redirect);
        } else {
            notif.hide('fade', function(){
                $(this).remove();
                $(document).trigger('onNotificationRemove');
            });
        }
        $(document).trigger('onNotificationSuccess', [response]);
    },

    failure : function(response) {
        this.bindHoverById(response.id);

        var notif = this.get(response.id);
        var errorDiv = notif.find('.error');
        if(!errorDiv.length) {
            errorDiv = $('<div />').appendTo(notif).addClass('error');
        }
        errorDiv.show().html(response.errorMessage);
        $(document).trigger('onNotificationFailure', [response]);
    },

    call : function(url, notification_id) {
        this.lastClickedId = notification_id.split('-')[1];
        $.ajax({
            url: url,
            data: {
                id : this.lastClickedId
            },
            success: this.ajax.success,
            beforeSend: this.ajax.beforeSend,
        });
    },

    ajax : {
        success : function(response) {
            Notification.disableAjaxLoader(response.id);
            if(response.status == 'success') {
                Notification.success(response);
            } else {
                Notification.failure(response);
            }
        },
        beforeSend : function() {
            Notification.unbindHoverById(Notification.lastClickedId);
            Notification.enableAjaxLoader(Notification.lastClickedId);
        }
    },

    get : function(id) {
        return $('#notification-' + id);
    },

    getLast : function() {
        return this.get(this.lastClickedId);
    },

    bindHoverToAll : function() {
        $('.notification.hoverable').hover(this.onMouseOver, this.onMouseOut);
    },

    bindHoverById : function(id) {
        this.get(id).hover(this.onMouseOver, this.onMouseOut);
    },

    onMouseOver : function() {
        $('.error', this).show();

    },

    onMouseOut : function() {
        $('.error', this).hide();

    },

    unbindHoverById : function(id) {
        this.get(id).unbind('mouseenter mouseleave');
    },

    enableAjaxLoader : function(id) {
        var me = this.get(id);
        me.find('.buttons').hide();
        me.find('.loader').show();
    },

    disableAjaxLoader : function(id) {
        var me = this.get(id);
        me.find('.buttons').show();
        me.find('.loader').hide();
    }
}

$(document).ready(function(){
    Notification.bindHoverToAll();

    $('.notification .buttons a').click(function(){
        Notification.call($(this).attr('href'), $(this).parent().parent().attr('id'));
        return false;
    });
});

