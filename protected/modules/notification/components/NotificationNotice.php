<?php

class NotificationNotice extends CWidget {

    /**
     * @var Notification The main object.
     */
    public $notification;

    /**
     * @var string Type tied to css class.
     */
    public $type = 'info';

    /**
     * @var array The buttons to show, if any.
     */
    public $buttons = array();

    /**
     * @var string Text to display for errors.
     */
    public $error = '';

    /**
     * @var boolean Condition in which to display the error.
     */
    public $errorCondition = false;

    /**
     * @var string Css class for disabled item.
     */
    public $cssClassDisabled = 'disabled';

    /**
     *
     */
    public function init() {

        $buttonDefaults = array(
            'label' => _t('OK'),
            'url' => _url('notification/ok'),
            'htmlOptions' => array(),
            'imageUrl' => Utils::moduleImageUrl('notification', 'ok.gif'),
            'useImage' => true,
            'visible' => true,
            'enabled' => true,
            'title' => null,
        );

        $defaultButtons = array(
            'accept' => array(
                'label' => _t('Accept'),
                'url' => _url('notification/accept'),
                'imageUrl' => Utils::moduleImageUrl('notification', 'accept.gif'),
            ),
            'reject' => array(
                'label' => _t('Reject'),
                'url' => _url('notification/reject'),
                'imageUrl' => Utils::moduleImageUrl('notification', 'reject.gif'),
            ),
            'ok'
        );

        foreach ($defaultButtons as $name => &$overrides) {
            if (is_int($name)) {
                $defaultButtons[$overrides] = $buttonDefaults;
                unset($defaultButtons[$name]);
            }
            else
                $overrides = array_merge($buttonDefaults, $overrides);
        }

        foreach ($this->buttons as $name => &$overrides) {
            if (is_int($name)) {
                $this->buttons[$overrides] = $defaultButtons[$overrides];
                unset($this->buttons[$name]);

                if (!isset($this->buttons[$overrides]['htmlOptions']['title']))
                    $this->buttons[$overrides]['htmlOptions']['title'] = $this->buttons[$overrides]['label'];

                if (!isset($this->buttons[$overrides]['htmlOptions']['class']))
                    $this->buttons[$overrides]['htmlOptions']['class'] = '';

                if (!$this->buttons[$overrides]['enabled'])
                    $this->buttons[$overrides]['htmlOptions']['class'] .= ' ' . $this->cssClassDisabled;
            } else {
                $overrides = array_merge($defaultButtons[$name], $overrides);

                if (!isset($overrides['htmlOptions']['title']))
                    $overrides['htmlOptions']['title'] = $overrides['label'];

                if (!isset($overrides['htmlOptions']['class']))
                    $overrides['htmlOptions']['class'] = '';

                if (!$overrides['enabled'])
                    $overrides['htmlOptions']['class'] .= ' ' . $this->cssClassDisabled;
            }
        }
    }

    /**
     *
     */
    public function run() {
        Utils::registerJsFile('lib/jquery.tools.tooltip.min');
        Utils::registerModuleCssFile('notification', 'styles');
        Utils::registerModuleJsFile('notification', 'script');

        $class = $this->type;
        $class.= ' hoverable';
        $this->render('notification', array(
            'class' => $class,
        ));
    }

}

?>
