<?php
echo CHtml::openTag('div', array('id' => "notification-{$this->notification->id}", 'class' => "notification {$class}"));
echo CHtml::tag('div', array('class' => 'message'), $this->notification->message);


if (!empty($this->buttons)) {
    echo CHtml::openTag('div', array('class' => 'buttons'));
    foreach ($this->buttons as $button) {
        if (!$button['visible'])
            continue;

        if ($button['useImage']) {
            $label = CHtml::image($button['imageUrl']);
        } else {
            $label = $button['label'];
        }

        if ($button['enabled']) {
            echo CHtml::link($label, $button['url'], $button['htmlOptions']);
        } else {
            echo CHtml::tag('span', $button['htmlOptions'], $label
            );
        }
    }
    echo CHtml::closeTag('div');
    echo CHtml::image(Utils::imageUrl('ajaxloader/small-circle.gif'), _t('wait...'), array('class' => 'loader'));
}

//if ($this->errorCondition)
//    echo CHtml::tag('div', array('class' => 'error'), $this->error);

echo CHtml::closeTag('div');
?>
<script type="text/javascript">
//    $(document).ready(function(){
//        $('.notification .disabled[title]').tooltip({
//            position: "bottom center"
//        });
//    });
</script>