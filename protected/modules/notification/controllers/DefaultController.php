<?php

class DefaultController extends Controller {

    public function filters() {
        return array('accessControl',);
    }

    public function accessRules() {
        return array(
            array('allow',
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*')
            )
        );
    }

    /**
     *
     */
    public function actionOk() {
        if (!Utils::isAjax() || !isset($_GET['id']))
            return;

        $this->viewNotification($_GET['id']);
    }

    /**
     *
     */
    public function actionReject() {
        if (!Utils::isAjax() || !isset($_GET['id']))
            return;

        $this->viewNotification($_GET['id']);
    }

    /**
     *
     */
    public function actionAccept() {
        if (!Utils::isAjax() || !isset($_GET['id']))
            return;

        $this->viewNotification($_GET['id']);
    }

    /**
     * @param int $id Notification id
     */
    private function returnSuccess($id) {
        Utils::jsonReturn(array(
            'status' => 'success',
            'id' => $id,
            'action' => $this->action->id,
            'show' => Notification::model()->showOnReturn(),
            'redirect' => Notification::model()->redirectOnReturn(),
        ));
    }

    /**
     * @param int $id Notification id
     */
    private function returnError($id, $message) {
        Utils::jsonReturn(array(
            'errorMessage' => nl2br($message),
            'id' => $id,
        ));
    }

    /**
     * @param int $id Notification id
     */
    private function viewNotification($id) {
        $notification = Notification::model()->findByPk($id);

        try {
            $trans = _app()->db->beginTransaction();

            Utils::triggerEvent('onNotificationView', $this, array('notification' => $notification));

            $action = ucfirst($this->action->id);
            Utils::triggerEvent("onNotification$action", $this, array('notification' => $notification));

            $notification->view();

            $trans->commit();
            $this->returnSuccess($id);
        } catch (CDbException $e) {
            $trans->rollback();
            $this->returnError($id, $e->getMessage());
        } catch (CValidateException $e) {
            $trans->rollback();
            $this->returnError($id, $e->getMessage());
        } catch (CException $e) {
            $trans->rollback();
            $this->returnError($id, $e->getMessage());
        }
    }

}