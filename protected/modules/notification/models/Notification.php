<?php

/**
 * This is the model class for table "mod_notifications".
 *
 * The followings are the available columns in table 'mod_notifications':
 * @property integer $id
 * @property string $creation_date
 * @property integer $type
 * @property integer $recipient_id
 * @property string $message
 *
 * The followings are the available model relations:
 * @property Manager $recipient
 */
class Notification extends CActiveRecord {
    /**
     * Notification type: Information
     * Default. Shows a simple message and is destroyed when read.
     */
    const TYPE_INFORMATION = 1;

    /**
     * Notification type: Interactive
     * Shows a message and expects a response after which it triggers a callback.
     */
    const TYPE_YESNO = 2;

    /**
     * @var string The html to render after acceping/rejecting or OKing a notification.
     */
    private $_onReturnHtml = null;

    /**
     * @var string Url to redirect after accepting/rejecting.
     */
    private $_onReturnRedirect = null;

    /**
     * Returns the static model of the specified AR class.
     * @return Notification the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'mod_notifications';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('creation_date, type, recipient_id, message', 'required'),
            array('type, recipient_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, creation_date, type, recipient_id, message', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'recipient' => array(self::BELONGS_TO, 'Manager', 'recipient_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'creation_date' => 'Creation Date',
            'type' => 'Type',
            'recipient_id' => 'Recipient',
            'message' => 'Message',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('creation_date', $this->creation_date, true);
        $criteria->compare('type', $this->type);
        $criteria->compare('recipient_id', $this->recipient_id);
        $criteria->compare('message', $this->message, true);

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
    }

    /**
     * @return boolean
     */
    protected function beforeSave() {
        if (parent::beforeSave()) {
            $this->creation_date = Utils::fromLocalDatetime($this->creation_date);
            return true;
        }
        return false;
    }

    /**
     *
     */
    protected function afterFind() {
        parent::afterFind();
        $this->creation_date = Utils::toLocalDatetime($this->creation_date);
    }

    /**
     * @return boolean
     */
    public function view() {
        return $this->delete();
    }

    /**
     * @param string $html The html to render.
     */
    public function setShowOnReturn($html) {
        $this->_onReturnHtml = $html;
    }

    /**
     * Returns the html to show on the client side and nulls the current set variable.
     * @return string The html to render.
     */
    public function showOnReturn() {
        $html = $this->_onReturnHtml;
        $this->_onReturnHtml = null;
        return $html;
    }

    /**
     * @param string $url Url to redirect to.
     */
    public function setRedirectOnReturn($url) {
        $this->_onReturnRedirect = $url;
    }

    /**
     * @return string Url to redirect to.
     */
    public function redirectOnReturn() {
        $url = $this->_onReturnRedirect;
        $this->_onReturnRedirect = null;
        return $url;
    }

}

/**
 * Notification debug class
 */
class NotificationDebug {

    /**
     * @param string $notification Notification object
     * @return string
     */
    public static function typeToText($notification) {
        if ($notification) {
            switch ($notification->type) {
                case Notification::TYPE_INFORMATION: return _t('Information', array(), 'Notification');
                case Notification::TYPE_YESNO: return _t('YesNo', array(), 'Notification');
            }
        }
        return '';
    }

}

?>
