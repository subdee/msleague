<?php

class NotificationModuleEvents extends CBehavior {

    public function onNotificationView($event) {
        $this->raiseEvent('onNotificationView', $event);
    }

    public function onNotificationAccept($event) {
        $this->raiseEvent('onNotificationAccept', $event);
    }

    public function onNotificationReject($event) {
        $this->raiseEvent('onNotificationReject', $event);
    }

    public function onNotificationOk($event) {
        $this->raiseEvent('onNotificationOk', $event);
    }
}

?>
