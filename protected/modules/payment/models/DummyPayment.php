<?php

/**
 * This is a dummy class for the payment interface
 */
class DummyPayment implements PaymentProvider {

    /**
     * Pay amount to provider
     * @param float $amount
     * @param int $managerId
     * @return bool If the payment was succesful
     */
    public function pay($amount, Manager $manager) {
        if ($amount) {
            Debug::logToWindow("{$manager->username} payed {$amount} succesfully");
            return true;
        } else {
            Debug::logToWindow("{$manager->username} cannot pay {$amount}");
        }
        return false;
    }

}

?>
