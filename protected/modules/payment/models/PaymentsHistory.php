<?php

/**
 * This is the model class for table "mod_payments_history".
 *
 * The followings are the available columns in table 'mod_payments_history':
 * @property integer $manager_id
 * @property string $transaction_no
 * @property string $payment_provider
 * @property string $date_completed
 */
class PaymentsHistory extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return PaymentsHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mod_payments_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('manager_id, transaction_no', 'required'),
			array('manager_id', 'numerical', 'integerOnly'=>true),
			array('transaction_no', 'length', 'max'=>50),
			array('payment_provider', 'length', 'max'=>10),
			array('date_completed', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('manager_id, transaction_no, payment_provider, date_completed', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'manager_id' => 'Manager',
			'transaction_no' => 'Transaction No',
			'payment_provider' => 'Payment Provider',
			'date_completed' => 'Date Completed',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('manager_id',$this->manager_id);
		$criteria->compare('transaction_no',$this->transaction_no,true);
		$criteria->compare('payment_provider',$this->payment_provider,true);
		$criteria->compare('date_completed',$this->date_completed,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}