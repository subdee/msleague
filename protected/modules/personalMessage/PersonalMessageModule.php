<?php

class PersonalMessageModule extends WebModule {

    /**
     * @var int Used for after process team.
     */
    private $_managerRole;

    /**
     *
     */
    public function init() {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'personalMessage.models.*',
            'personalMessage.components.*',
        ));

        // Attach module events to system
        _app()->eventSystem->attachBehavior('PersonalMessageModuleEvents', new PersonalMessageModuleEvents);

        // Register events
        Utils::registerCallback('onManagerBoxMenu1', $this, 'renderManagerBoxMenuItem');
        Utils::registerNamedCallback('PersonalMessageModuleEvents', 'onMessageSend', $this, 'updateThread');

        Utils::registerCallback('onProfileLeftColumnLinksTop', $this, 'renderProfileSendMessageLink');

        Utils::registerCallback('onBeforeProcessTeam', $this, 'saveManagerRole');
        Utils::registerCallback('onAfterProcessTeam', $this, 'sendWelcomeMessage');
    }

    /**
     *
     * @param type $controller
     * @param type $action
     * @return type
     */
    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        }
        else
            return false;
    }

    /**
     * Renders the module's menu item.
     * @param CEvent $e
     */
    public function renderManagerBoxMenuItem(CEvent $e) {
        $e->sender->render('personalMessage.views._managerBoxMenuItem', array(
            'messageCount' => PMThread::model()->countUnreadThreadsFor(_manager()->id),
        ));
    }

    /**
     * @var CEvent $e
     */
    public function updateThread(CEvent $e) {
        // After a message has been sent we need to update the thread to set unread for the new recipient.
        $comment = $e->params['comment'];
        $thread = PMThread::model()->findByPk($comment->discussion_id);
        $thread->setUnreadForOpposite($comment->commentor_id);
        $thread->updateThread($comment);

        Utils::notice('message', _t('Your message has been sent!'), 'success', 'small');
    }

    /**
     * @param CEvent $e
     */
    public function renderProfileSendMessageLink(CEvent $e) {
        $e->sender->renderPartial('personalMessage.views._profileLeftColumnTopLinks', $e->params);
    }

    /**
     * @param CEvent $e
     */
    public function sendWelcomeMessage(CEvent $e) {

        // Skip for reset managers
        if ($this->_managerRole == Role::RESET)
            return;

        // Load welcome message
        $welcomeMessage = StaticContent::model()->getText('WelcomeMessage');

        // Send system message
        PMThread::model()->sendSystemMessage(_manager()->id, $welcomeMessage);
    }

    /**
     * @param CEvent $e
     */
    public function saveManagerRole(CEvent $e) {
        $this->_managerRole = _manager()->role;
    }

}
