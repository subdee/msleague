<?php

class DefaultController extends Controller {

    public function filters() {
        return array('accessControl',);
    }

    public function accessRules() {
        return array(
            array('deny',
                'users' => array('?'),
            ),
            array('deny',
                'actions' => array('compose', 'recipientsList'),
                'expression' => array('Manager', 'isBanned')
            ),
            array('allow',
                'expression' => '_manager()->hasRole(Role::MANAGER | Role::RESET)',
            ),
            array('deny',
                'users' => array('*')
            )
        );
    }

    /**
     *
     */
    public function init() {
        parent::init();
        Utils::registerModuleCssFile('personalMessage', 'styles');
    }

    /**
     *
     */
    public function actionIndex() {

        $threads = new PMThread;

        if (isset($_GET['PMThread'])) {
            $threads->attributes = $_GET['PMThread'];
        }

        _setPageTitle(_t('Messages'));
        $this->render('index', array(
            'threads' => $threads,
        ));
    }

    /**
     *
     */
    public function actionSend() {
        if (!Utils::isAjax())
            return;

        if (!(isset($_POST['PMThread']) && isset($_POST['Comment'])))
            return;

        $trans = _app()->db->beginTransaction();
        try {

            $validator = new PMThread('compose');
            $validator->attributes = $_POST['PMThread'];
            $validator->sender_id = _manager()->id;
            if (!$validator->validate(array('recipientUsername')))
                throw new CValidateException($validator);

            $recipient = Manager::model()->findByAttributes(array('username' => $_POST['PMThread']['recipientUsername']));

            if (!PMThread::model()->canSendMessage(_manager(), $recipient))
                throw new CException(_t('You cannot send a message to a user that has blocked you.'));

            PMThread::model()->sendMessage(_manager()->id, $recipient->id, $_POST['Comment']['content']);


            $trans->commit();
            Utils::jsonReturn(array(
                'success' => true,
                'notice' => array(
                    'message' => _t('Message sent!'),
                    'type' => 'success',
                    'class' => 'small',
                )
            ));
        } catch (CDbException $e) {
            Debug::logToWindow($e->getMessage());
        } catch (CException $e) {

        } catch (CValidateException $e) {

        }

        $trans->rollback();

        Utils::jsonReturn(array(
            'message' => $e->getMessage(),
            'type' => 'error',
            'class' => 'small',
        ));
    }

    /**
     *
     */
    public function actionView($id) {

        $thread = PMThread::model()->with('sender', 'recipient')->findByPk($id);
        if (!$thread || !$thread->isVisibleBy(_manager()->id))
            throw new CHttpException('404');

        $thread->readBy(_manager()->id);

        if ($thread->system) {
            _setPageTitle(_t('Conversation between {user1} and {user2}', array(
                    '{user1}' => _gameName(),
                    '{user2}' => $thread->recipient->username,
                )));
        } else {
            _setPageTitle(_t('Conversation between {user1} and {user2}', array(
                    '{user1}' => $thread->sender ? $thread->sender->username : _t('(deleted user)'),
                    '{user2}' => $thread->recipient ? $thread->recipient->username : _t('(deleted user)'),
                )));
        }
        $this->render('view', array(
            'thread' => $thread,
        ));
    }

    /**
     *
     */
    public function actionRecipientsList() {
        if (Yii::app()->request->isAjaxRequest && isset($_GET['q'])) {
            /* q is the default GET variable name that is used by the autocomplete widget to pass in user input  */

            $name = $_GET['q'];

            // this was set with the "max" attribute of the CAutoComplete widget
            $limit = min($_GET['limit'], 50);
            $criteria = new CDbCriteria;
            $criteria->select = 'username';
            $criteria->condition = "username LIKE :sterm AND role in (:role1, :role2) AND id != :mid";
            $criteria->params = array(
                ':sterm' => "%$name%",
                ':role1' => Role::MANAGER,
                ':role2' => Role::RESET,
                ':mid' => _manager()->id,
            );
            $criteria->limit = $limit;
            $userArray = Manager::model()->findAll($criteria);
            $returnVal = '';
            foreach ($userArray as $userAccount) {
                $returnVal .= $userAccount->getAttribute('username') . "\n";
            }
            echo $returnVal;
        }
    }

    /**
     * @param int $id The discussion thread id
     */
    public function actionDeleteAll($id) {

        $thread = PMThread::model()->findByPk($id);
        if (!$thread)
            throw new CHttpException('404');

        $trans = _app()->db->beginTransaction();
        try {
            $thread->deleteBy(_manager()->id);
            Utils::notice('message', _t('Conversation deleted.'), 'success', 'small');
            $trans->commit();
            $this->redirect('index');
        } catch (CDbException $e) {
            Debug::logToWindow($e->getMessage());
        } catch (CException $e) {

        }

        $trans->rollback();
        Utils::notice('message-view', _t('An error occured white trying to delete your messages. Please try again.'), 'error', 'small');
        $this->redirect(_url('personalMessage/view', array('id' => $id)));
    }

}