<?php

/**
 * This is the model class for table "mod_pm_threads".
 *
 * The followings are the available columns in table 'mod_pm_threads':
 * @property integer $discussion_id
 * @property integer $recipient_id
 * @property integer $sender_id
 * @property boolean $deleted_by_sender
 * @property boolean $deleted_by_recipient
 * @property boolean $read_by_sender
 * @property boolean $read_by_recipient
 * @property boolean $system
 *
 * The followings are the available model relations:
 * @property Manager $recipient
 * @property Discussion $discussion
 * @property Manager $sender
 */
class PMThread extends CActiveRecord {

    /**
     * @var string Used in admin.
     */
    public $dateUpdated;

    /**
     * @var string Used in admin.
     */
    public $message;

    /**
     * @var int Used only by search.
     */
    public $commentCount;

    /**
     * @var string Used for validations on new thread.
     */
    public $recipientUsername;

    /**
     * Returns the static model of the specified AR class.
     * @return PMThread the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'mod_pm_threads';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('recipientUsername', 'required', 'on' => 'compose'),
            array('recipientUsername', 'exist', 'className' => 'Manager', 'attributeName' => 'username', 'safe' => true, 'message' => _t('Manager does not exist.')),
            array('recipientUsername', 'validateRecipientUsername'),
            array('discussion_id', 'required'),
            array('discussion_id, recipient_id, sender_id', 'numerical', 'integerOnly' => true),
            array('deleted_by_sender, deleted_by_recipient, read_by_sender, read_by_recipient, system', 'boolean'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('discussion_id, recipient_id, sender_id, deleted_by_sender, deleted_by_recipient, read_by_sender, read_by_recipient, system', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'recipient' => array(self::BELONGS_TO, 'Manager', 'recipient_id'),
            'discussion' => array(self::BELONGS_TO, 'Discussion', 'discussion_id'),
            'sender' => array(self::BELONGS_TO, 'Manager', 'sender_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'discussion_id' => _t('Discussion'),
            'recipient_id' => _t('Manager'),
            'recipientUsername' => _t('Manager'),
            'sender_id' => _t('Sender'),
            'deleted_by_sender' => 'Visible To Sender',
            'deleted_by_recipient' => 'Visible To Recipient',
            'read_by_sender' => 'Read By Sender',
            'read_by_recipient' => 'Read By Recipient',
            'system' => 'System',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('discussion_id', $this->discussion_id);

        if ($this->sender_id == '') {
            $criteria->compare('sender.username', $this->recipient_id, true);
            $criteria->compare('recipient.username', $this->recipient_id, true, 'OR');
        } else if ($this->recipient_id == '') {
            $criteria->compare('sender.username', $this->sender_id, true);
            $criteria->compare('recipient.username', $this->sender_id, true, 'OR');
        } else {
//            $criteria->compare('sender.username', $this->sender_id, true);
//            $criteria->compare('sender.username', $this->recipient_id, true, 'OR');
//            $criteria->compare('recipient.username', $this->sender_id, true, 'AND');
//            $criteria->compare('recipient.username', $this->recipient_id, true, 'OR');
            $criteria->addCondition('
                (sender.username LIKE "%' . $this->sender_id . '%" AND recipient.username LIKE "%' . $this->recipient_id . '%")
                OR
                (recipient.username LIKE "%' . $this->sender_id . '%" AND sender.username LIKE "%' . $this->recipient_id . '%")
            ');
        }

        // Join with managers
        $criteria->with = array(
            'recipient' => array('select' => 'recipient.username'),
            'sender' => array('select' => 'sender.username'),
            'discussion' => array('select' => 'discussion.date_updated'),
        );

        // Add select comment count
        $criteria->select = '*, (SELECT COUNT(id) FROM mod_forum_comments WHERE discussion_id = t.discussion_id) as commentCount';

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'sort' => array(
                    'attributes' => array(
                        'commentCount' => array(
                            'asc' => 'commentCount',
                            'desc' => 'commentCount desc',
                        ),
                        'dateUpdated' => array(
                            'asc' => 'discussion.date_updated asc',
                            'desc' => 'discussion.date_updated desc',
                        ),
                    ),
                    'defaultOrder' => array(
                        'dateUpdated' => false
                    ),
                ),
            ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchFrontend() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('discussion_id', $this->discussion_id);
        $criteria->compare('sender_id', $this->sender_id);
        $criteria->compare('recipient.username', $this->recipient_id, true, 'OR');

        // Rule out deleted messages
        $criteria->addCondition('sender_id = :mid AND deleted_by_sender is false');
        $criteria->addCondition('recipient_id = :mid AND deleted_by_recipient is false', 'OR');

        $criteria->params[':mid'] = _manager()->id;

        // Join with managers
        $criteria->with = array(
            'recipient' => array('select' => 'recipient.username'),
            'sender' => array('select' => 'sender.username'),
            'discussion' => array('select' => 'discussion.date_updated'),
        );

        // Order by most recently updated
        $criteria->order = 'discussion.date_updated desc';

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 10,
                ),
                'sort' => false,
            ));
    }

    /**
     *
     * @param type $attributes
     * @param type $params
     */
    public function validateRecipientUsername($attributes, $params) {
        if ($this->hasErrors())
            return;

        if ($this->recipientUsername !== null) {

            // Check if manager is messaging himself
            if ($this->recipientUsername == _manager()->username) {
                $this->addError('recipientUsername', _t('You cannot send a message to yourself.'));
            }

            // Check if manager is blocked by recipient.
            $this->recipient_id = Manager::model()->findByAttributes(array('username' => $this->recipientUsername))->id;
            if ($this->sender->isBlockedBy($this->recipient_id)) {
                $this->addError('recipientUsername', _t('You can\'t send a message to a user that has blocked you.'));
            }
        }
    }

    /**
     * Creates a new personal message thread or returns the already existing one.
     * @param int $sender_id
     * @param int $recipient_id
     * @param boolean $system
     * @return PMThread Model on success, NULL on failure
     */
    public function getThread($sender_id, $recipient_id, $system = false) {
        if ($system) {
            $thread = $this->_getSystemThread($recipient_id);
        } else {
            $thread = $this->_getThread($sender_id, $recipient_id);
        }

        if (!$thread) {
            $thread = $this->_createThread($sender_id, $recipient_id, $system);
        }

        return $thread;
    }

    /**
     *
     * @param Manager $from
     * @param Manager $to
     * @return boolean
     */
    public function canSendMessage(Manager $from, Manager $to) {
        return $from->isBlockedBy($to->id) === FALSE;
    }

    /**
     *
     * @param int $from
     * @param int $to
     * @param string $message
     * @return boolean
     */
    public function sendMessage($from, $to, $message) {
        return $this->_sendMessage($from, $to, $message, false);
    }

    /**
     *
     * @param int $to
     * @param string $message
     * @return boolean
     */
    public function sendSystemMessage($to, $message) {
        return $this->_sendMessage(null, $to, $message, true);
    }

    /**
     *
     * @param int $from
     * @param int $to
     * @param string $message
     * @param boolean $system
     * @return boolean
     */
    public function _sendMessage($from, $to, $message, $system) {
        // Get thread
        $thread = $this->getThread($from, $to, $system);

        // Create comment
        if ($thread->_addComment($from, $message, $system)) {

            // Update thread
            if (!$thread->system) {
                return $thread->setUnreadFor($to);
            }
        }

        return false;
    }

    /**
     * Adds a comment to the discussion.
     *
     * @param int $commentor_id
     * @param Comment $message
     * @param boolean $system
     *
     * @return boolean True for success
     */
    private function _addComment($commentor_id, $message, $system) {

        // Sanitize message
        if (!$system) {
            $message = nl2br(strip_tags(trim($message)));
        }

        // Create comment
        $comment = new Comment;
        $comment->content = $message;
        $comment->commentor_id = $commentor_id;
        $comment->discussion_id = $this->discussion_id;

        if ($comment->save(!$system)) {
            return $this->updateThread($comment);
        }
        else
            throw new CValidateException($comment);

        return false;
    }

    /**
     * Adds a pm thread comment and updates discussion date.
     *
     * @param Comment $comment
     *
     * @return boolean
     */
    public function updateThread(Comment $comment) {

        // Bind with pm attributes
        $pm = new PMThreadComment;
        $pm->comment_id = $comment->id;
        if ($pm->save()) {

            // Undo thread deletion
            $this->undelete();

            // Update discussion
            $this->discussion->date_updated = _app()->localtime->getLocalNow('Y-m-d H:i:s');
            return $this->discussion->save(false, array('date_updated'));
        }
        else
            throw new CValidateException($pm);

        return false;
    }

    /**
     * @param int $sender_id
     * @param int $recipient_id
     * @param boolean $system
     * @return PMThread New model on success, NULL on failure
     */
    private function _createThread($sender_id, $recipient_id, $system = false) {
        // Create discussion
        $discussion = new Discussion;
        if ($discussion->save()) {

            // Create thread
            $thread = new PMThread;
            $thread->discussion_id = $discussion->id;
            $thread->sender_id = $sender_id;
            $thread->recipient_id = $recipient_id;

            $thread->deleted_by_recipient = false;
            $thread->deleted_by_sender = false;
            $thread->read_by_sender = true;
            $thread->system = $system;

            if ($thread->save()) {
                return $thread;
            }
            else
                throw new CValidateException($thread);
        }
        else
            throw new CValidateException($thread);

        return null;
    }

    /**
     * @param int $sender_id
     * @param int $recipient_id
     * @return PMThread Model if found, NULL on failure
     */
    private function _getThread($sender_id, $recipient_id) {
        return PMThread::model()->find(array(
                'condition' => 'sender_id in (:sid, :rid) and recipient_id in (:sid, :rid) and system is false',
                'params' => array(
                    ':sid' => $sender_id,
                    ':rid' => $recipient_id,
                )
            ));
    }

    /**
     * @param int $recipient_id
     * @return PMThread Model if found, NULL on failure
     */
    private function _getSystemThread($recipient_id) {
        return PMThread::model()->find(array(
                'condition' => 'sender_id is null and recipient_id = :rid and system is true',
                'params' => array(
                    ':rid' => $recipient_id,
                )
            ));
    }

    /**
     *
     * @param type $manager_id
     * @return type
     */
    public function isReadBy($manager_id) {
        if ($this->sender_id == $manager_id && $this->read_by_sender)
            return true;
        if ($this->recipient_id == $manager_id && $this->read_by_recipient)
            return true;

        return false;
    }

    /**
     *
     * @param type $manager_id
     * @return type
     */
    public function isVisibleBy($manager_id) {
        if ($this->sender_id == $manager_id && $this->deleted_by_sender)
            return false;
        if ($this->recipient_id == $manager_id && $this->deleted_by_recipient)
            return false;

        return true;
    }

    /**
     * @param int $manager_id
     * @return boolean
     */
    public function readBy($manager_id) {
        if ($this->sender_id == $manager_id)
            $attribute = 'read_by_sender';
        elseif ($this->recipient_id == $manager_id)
            $attribute = 'read_by_recipient';
        else
            throw new CException("Invalid manager($manager_id) tries to read a foreign thread");

        $this->$attribute = true;
        return $this->update(array($attribute));
    }

    /**
     * Marks the thread as deleted and all the comments.
     *
     * @param int $manager_id The manager id to delete for
     *
     * @return boolean
     */
    public function deleteBy($manager_id) {
        if ($this->sender_id == $manager_id)
            $attribute = 'deleted_by_sender';
        elseif ($this->recipient_id == $manager_id)
            $attribute = 'deleted_by_recipient';
        else
            throw new CException("Invalid manager($manager_id) tries to delete a foreign thread");

        // Update thread
        $this->$attribute = true;
        if ($this->update(array($attribute))) {

            // Update comments
            $query = <<<SQL
            UPDATE mod_pm_threads_comments
               SET `{$attribute}` = 1
             WHERE comment_id in (
                SELECT id
                  FROM mod_forum_comments
                 WHERE discussion_id = {$this->discussion_id}
             )
SQL;
            _app()->db->createCommand($query)->execute();
            return true;
        }

        return false;
    }

    /**
     * @param int $manager_id
     * @return boolean
     */
    public function setUnreadFor($manager_id) {
        if ($this->sender_id == $manager_id)
            $attribute = 'read_by_sender';
        elseif ($this->recipient_id == $manager_id)
            $attribute = 'read_by_recipient';
        else
            throw new CException("Invalid manager($manager_id) tries to unread a foreign thread");

        $this->$attribute = false;
        return $this->saveAttributes(array($attribute));
    }

    /**
     * @param int $manager_id
     * @return boolean
     */
    public function undelete() {
        $this->deleted_by_recipient = false;
        $this->deleted_by_sender = false;
        return $this->saveAttributes(array('deleted_by_recipient', 'deleted_by_sender'));
    }

    /**
     * @param int $manager_id
     * @return boolean
     */
    public function setUnreadForOpposite($manager_id) {
        if ($this->sender_id == $manager_id)
            $attribute = 'read_by_recipient';
        elseif ($this->recipient_id == $manager_id)
            $attribute = 'read_by_sender';
        else
            throw new CException("Invalid manager($manager_id) tries to unread a foreign thread");

        $this->$attribute = false;
        return $this->saveAttributes(array($attribute));
    }

    /**
     * @param int $manager_id
     * @return boolean
     */
    public function isSender($manager_id) {
        return $this->sender_id == $manager_id;
    }

    /**
     * @param int $manager_id
     * @return boolean
     */
    public function isRecipient($manager_id) {
        return $this->recipient_id == $manager_id;
    }

    /**
     * @param int $manager_id
     * @return int The number of updated (unread) threads.
     */
    public function countUnreadThreadsFor($manager_id) {
        return PMThread::model()->count(array(
                'condition' => '(sender_id = :mid and read_by_sender is false) or (recipient_id = :mid and read_by_recipient is false)',
                'params' => array(
                    ':mid' => $manager_id,
                )
            ));
    }

    /**
     * @param int $manager_id
     * @return boolean Whether the manager can reply to a thread.
     */
    public function canReply($manager_id) {
        if ($this->system)
            return false;

        if ($this->sender_id != $manager_id && $this->sender_id === null)
            return false;

        if ($this->recipient_id != $manager_id && $this->recipient_id === null)
            return false;

        return true;
    }

    /**
     * Returns the correct manager for recipient column in mailbox display.
     * @param int $manager_id
     * @return Manager
     */
    public function recipientColHtml($manager_id) {

        // System message
        if ($this->system) {
            $html = CHtml::image(Utils::imageUrl('profile/empty_profile_thumbnail.png'));
            $html .= CHtml::tag('span', array('class' => 'profile system'), _gameName());
            return $html;
        }

        // Deleted user
        if (($this->recipient_id == $manager_id && $this->sender_id === null) || ($this->sender_id == $manager_id && $this->recipient_id === null)) {
            $html = CHtml::image(Utils::imageUrl('profile/empty_profile_thumbnail.png'));
            $html .= CHtml::tag('span', array('class' => 'profile deleted'), _t("(deleted user)"));
            return $html;
        }

        if ($this->sender_id == $manager_id) {
            $html = CHtml::image($this->recipient->profilePhoto(true));
            $html .= CHtml::openTag('span', array('class' => 'profile'));
            $html .= $this->recipient->profileLink();
            $html .= CHtml::closeTag('span');
            return $html;
        }

        if ($this->recipient_id == $manager_id) {
            $html = CHtml::image($this->sender->profilePhoto(true));
            $html .= CHtml::openTag('span', array('class' => 'profile'));
            $html .= $this->sender->profileLink();
            if (!$this->isReadBy($manager_id)) {
                $html .= CHtml::tag("span", array('class' => 'new'), _t('new!'));
            }
            $html .= CHtml::closeTag('span');
            return $html;
        }

        throw new CException('Invalid combination for tread');
    }

    /**
     *
     * @param int $manager_id
     * @return string
     */
    public function infoColHtml($manager_id) {

        // Get latest comment
        $lastComment = $this->discussion->lastComment(false, array(
            'join' => 'JOIN mod_pm_threads_comments pm ON pm.comment_id = t.id',
            'condition' => $this->isSender($manager_id) ? 'deleted_by_sender is false' : 'deleted_by_recipient is false',
            ));

        $content = Utils::substrFromStart($lastComment->content, 30, '...');
        if (!$this->isReadBy($manager_id)) {
            $content .= CHtml::tag("span", array('class' => 'new'), _t(' - new!'));
        }
        $html = CHtml::tag('div', array(), $content);

        if (!$this->system) {

            // Get last commentor
            $commentor = $lastComment->commentor;

            if ($commentor) {
                $username = $manager_id == $commentor->id ? _t('you', array(), 'PMThread') : $commentor->username;
            } else {
                $username = CHtml::tag('span', array('class' => 'deleted'), _t("(deleted user)"));
            }

            $html .= CHtml::tag('div', array('class' => 'info'), _t('by {user} {date}', array(
                        '{user}' => $username,
                        '{date}' => Utils::relativeDatetime($this->discussion->date_updated)
                    )));
        } else {
            $html .= CHtml::tag('div', array('class' => 'info'), Utils::relativeDatetime($this->discussion->date_updated));
        }

        return _l($html, _url("personalMessage/view", array("id" => $this->discussion_id)));
    }

}

?>
