<?php

/**
 * This is the model class for table "mod_pm_threads_comments".
 *
 * The followings are the available columns in table 'mod_pm_threads_comments':
 * @property integer $comment_id
 * @property integer $deleted_by_sender
 * @property integer $deleted_by_recipient
 *
 * The followings are the available model relations:
 * @property Comment $comment
 */
class PMThreadComment extends CActiveRecord {

    /**
     * @var int Used in amdin.
     */
    public $discussion_id;

    /**
     * @var string Used in admin.
     */
    public $commentContent;

    /**
     * Returns the static model of the specified AR class.
     * @return PMThreadComment the static model class
     */
    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'mod_pm_threads_comments';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('comment_id', 'required'),
            array('comment_id', 'numerical', 'integerOnly' => true),
            array('deleted_by_sender, deleted_by_recipient', 'boolean'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('comment_id, deleted_by_sender, deleted_by_recipient', 'safe', 'on' => 'search'),
            array('discussion_id, commentContent', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'comment' => array(self::BELONGS_TO, 'Comment', 'comment_id', 'together' => false),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'comment_id' => 'Comment',
            'deleted_by_sender' => 'Deleted By Sender',
            'deleted_by_recipient' => 'Deleted By Recipient',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

//        $criteria->compare('comment_id', $this->comment_id);
//        $criteria->compare('deleted_by_sender', $this->deleted_by_sender);
//        $criteria->compare('deleted_by_recipient', $this->deleted_by_recipient);

        $criteria->compare('comment.discussion_id', $this->discussion_id);
        $criteria->compare('comment.content', $this->commentContent, true);

        $criteria->with = array(
            'comment',
        );

        return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
    }

}

?>
