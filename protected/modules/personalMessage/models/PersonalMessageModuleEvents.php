<?php

class PersonalMessageModuleEvents extends CBehavior {

    public function onMessageSend($event) {
        $this->raiseEvent('onMessageSend', $event);
    }

    public function onMessageView($event) {
        $this->raiseEvent('onMessageView', $event);
    }

    public function onMessageViewFirstTime($event) {
        $this->raiseEvent('onMessageViewFirstTime', $event);
    }

}

?>
