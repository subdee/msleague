<?php

if (_manager()->id != $manager->id) {
    if (_manager()->isBanned()) {
        echo CHtml::tag('li', array(), CHtml::tag('span', array(
                'class' => 'mimic-tooltip',
                'title' => _t('You can\'t send a message because you are banned.')
                ), _t('Send message')
            ));
    } else {
        echo CHtml::tag('li', array(), _l(_t('Send a message'), '#', array(
                'onclick' => CJavaScript::encode("js:$('#new-message-dialog').dialog('open'); $('#PMThread_recipientUsername').val('{$manager->username}'); return false;"),
            )));
        $this->renderPartial('personalMessage.views.default._newMessageForm', array('focus' => 'content'));
    }
}
?>