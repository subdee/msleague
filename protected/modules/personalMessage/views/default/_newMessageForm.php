<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'new-message-dialog',
    'options' => array(
        'title' => _t('Write message'),
        'autoOpen' => false,
        'width' => 500,
        'modal' => true,
        'draggable' => false,
        'resizable' => false,
        'buttons' => array(
            _t('Send') => 'js:addMessage',
            _t('Cancel') => 'js:function(){$(this).dialog("close");}',
        ),
    ),
));

$this->widget('Notice', array('id' => 'newMessageNotice', 'ajax' => true));

$thread = new PMThread('compose');
$comment = new Comment;


if(isset($focus) && $focus == 'content') {
    $focusArray = array($comment, 'content');
} else {
    $focusArray = array($thread, 'recipientUsername');
}

$form = $this->beginWidget('CActiveForm', array(
    'id' => 'new-message-form',
    'action' => _url('personalMessage/send'),
    'htmlOptions' => array(
        'onSubmit' => 'addMessage(); return false;',
        'class' => 'popup new-message',
    ),
    'focus' => $focusArray,
    ));
?>
<div class="row">
    <?php
    echo $form->labelEx($thread, 'recipientUsername');
    $this->widget('CAutoComplete', array(
        'name' => CHtml::activeName($thread, 'recipientUsername'),
        'value' => $thread->recipientUsername,
        'url' => _url('personalMessage/recipientsList'),
        'max' => 10,
        'minChars' => 2,
        'delay' => 500,
        'matchCase' => false,
        'inputClass' => 'textfield',
        'htmlOptions' => array('size' => '16'),
    ));
    ?>
</div>
<div class="row">
    <?php
    echo $form->labelEx($comment, 'content');
    echo $form->textArea($comment, 'content');
    ?>
</div>
<?php $this->endWidget(); ?>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>

<script type="text/javascript">
    function addMessage() {
<?php
echo CHtml::ajax(array(
    'url' => _url('personalMessage/send'),
    'data' => "js:$('#new-message-form').serialize()",
    'type' => 'post',
    'dataType' => 'json',
    'success' => "function(response) {
        console.log(response);
        if(response.success) {
            $('#new-message-dialog').dialog('close');
            if($.fn.yiiGridView)
                $.fn.yiiGridView.update('personal-messages');
            $('#new-message-form')[0].reset();
            $('#newMessageSuccess').notice(response.notice);
        } else {
            $('#newMessageNotice').notice(response);
        }
    }",
));
?>
    }
</script>
