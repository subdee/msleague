<div class="messages">
    <div class="actions">
        <?php $this->widget('Notice', array('session' => 'message')); ?>
        <?php
        if (!_manager()->isBanned()) {
            echo _l(_t('New message'), '#', array(
                'class' => 'new-message',
                'onclick' => CJavaScript::encode("js:$('#new-message-dialog').dialog('open'); return false;"),
            ));
        } else {
            echo CHtml::tag('span', array('class' => 'new-message mimic-tooltip', 'title' => _t('You cannot send a message because you are banned.')), _t('New message'));
        }
        ?>
    </div>
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'personal-messages',
        'dataProvider' => $threads->searchFrontend(),
//        'filter' => $threads,
//        'filterPosition' => 'header',
        'pager' => array(
            'class' => 'CLinkPager',
            'header' => '',
            'prevPageLabel' => '&laquo;',
            'nextPageLabel' => '&raquo;',
        ),
        'rowCssClassExpression' => '$data->isReadBy(_manager()->id) ? ($row&1 ? "even" : "odd") : (($row&1 ? "even" : "odd") . " unread")',
        'template' => '{items}<div class="footer">{summary}{pager}</div>',
        'summaryText' => _t('Showing messages {start}-{end} out of {count}'),
        'summaryCssClass' => 'message-summary',
        'emptyText' => _t('No messages found.'),
        'htmlOptions' => array('class' => 'grid-view personal-messages'),
        'columns' => array(
            array(
                'name' => 'recipient_id',
                'type' => 'raw',
                'value' => '$data->recipientColHtml(_manager()->id)',
                'header' => _t('Conversation with'),
                'htmlOptions' => array('class' => 'recipient'),
            ),
            array(
                'type' => 'raw',
                'header' => _t('Message'),
                'value' => '$data->infoColHtml(_manager()->id)',
                'htmlOptions' => array('class' => 'content'),
            ),
        )
    ));

    $this->renderPartial('_newMessageForm');
    ?>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        $lp = new MSL.LoopedEvent(function(){
            $.fn.yiiGridView.update('personal-messages');
        }, 30000)
        $lp.Start(false);
    });
</script>