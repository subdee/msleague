<div class="view-message">
    <div class="actions">
        <?php $this->widget('Notice', array('session' => 'message-view')); ?>
        <?php
        echo _l(_t('Delete all'), _url('personalMessage/deleteAll', array('id' => $thread->discussion_id)), array(
            'class' => 'delete',
            'confirm' => _t('Are you sure you want to delete the conversation?'),
        ));
        if ($thread->system) {
            echo CHtml::tag('span', array('class' => 'conversation-info'), _t('Messages from {gn}', array('{gn}' => _gameName())));
        } else {
            echo CHtml::tag('span', array('class' => 'conversation-info'), _t('Conversation with {manager}', array(
                    '{manager}' => $thread->isSender(_manager()->id) ? $thread->recipient->profileLink() : $thread->sender->profileLink())
                ));
        }
        echo _l(_t('Back to messages'), _url('personalMessage/index'), array('class' => 'back'));
        ?>
    </div>

    <?php
    $isBlocked = _manager()->isBlockedBy($thread->isSender(_manager()->id) ? $thread->recipient_id : $thread->sender_id);
    $this->widget('Comments', array(
        'discussionId' => $thread->discussion_id,
        'returnUrl' => _url('personalMessage/index'),
        'onSuccessEvent' => 'onMessageSend',
        'openCondition' => $thread->canReply(_manager()->id) && !$isBlocked,
        'commentsClosedCustomMessage' => _t('You cannot send a message to a user that has blocked you.'),
        'showClosedReason' => $thread->canReply(_manager()->id) && (_manager()->isBanned() || $isBlocked),
        'bannedUserMessage' => _t('You cannot send a message because you are banned.'),
        'textAreaPrompt' => _t('Reply ...'),
        'submitButtonText' => _t('Send'),
        'endOfScrollMessage' => _t('No more messages.'),
        'nullCommentorCondition' => $thread->system,
        'nullCommentorTemplate' => CHtml::tag('span', array('class' => 'system'), _gameName()),
        'emptyCommentValidationMessage' => _t('You must write a message.'),
        'criteria' => array(
            'join' => 'JOIN mod_pm_threads_comments pm ON pm.comment_id = t.id',
            'condition' => $thread->isSender(_manager()->id) ? 'deleted_by_sender is false' : 'deleted_by_recipient is false',
        )
    ));
    ?>
</div>