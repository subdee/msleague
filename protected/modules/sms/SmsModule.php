<?php

class SmsModule extends WebModule {

    public $tableNamePrefix = 'mod_sms_';
    public $models = array('mobile');
    
    public $soap_api_id;
    public $soap_user;
    public $soap_password;
    public $soap_url;
    
    public function init() {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'sms.models.*',
            'sms.components.*',
        ));
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        }
        else
            return false;
    }
    
}
