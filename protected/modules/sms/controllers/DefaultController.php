<?php

class DefaultController extends Controller {

    public function filters() {
        return array('accessControl',);
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('verify', 'install'),
                'users' => array('*')
            )
        );
    }

    /**
     *
     * @return <type>
     */
    public function init() {
        parent::init();
        _cs()->registerCSSFile(_moduleUrl('sms') . 'css/sms.css');
        _setPageTitle(_t('SMS Activation'));
    }

    public function actionInstall() {
        $tableNamePrefix = $this->module->tableNamePrefix;
        $installed = false;
        $error = '';

        $sql = array();

        // Messages table
        $sql['messages'] = <<<SQL
        CREATE TABLE IF NOT EXISTS {$tableNamePrefix}mobile (
            id INT NOT NULL AUTO_INCREMENT,
            manager_id INT NOT NULL,
            mobile VARCHAR(16),
            activation_code VARCHAR(6),
            PRIMARY KEY (id),
            CONSTRAINT fk_mobile_manager FOREIGN KEY (manager_id) REFERENCES manager (id) ON DELETE CASCADE ON UPDATE CASCADE
            )
            ENGINE=InnoDB DEFAULT CHARSET=utf8;
SQL;

        if (isset($_POST['install'])) {
            // Execute installation queries
            $connection = _app()->db;
            $transaction = $connection->beginTransaction();
            try {
                foreach ($sql as $query) {
                    $connection->createCommand($query)->execute();
                }
                $transaction->commit();
                $installed = true;
            } catch (CDbException $e) {
                $transaction->rollback();
                $error = Debug::exceptionDetailString($e);
            }
        }

        $this->render('install', array(
            'sql' => $sql,
            'installed' => $installed,
            'error' => $error,
            'prefix' => $tableNamePrefix,
            'models' => $this->module->models,
        ));
    }

    public function actionVerify($manager_id=0) {
        $sms = new Sms;

        if (!isset($_POST['Sms']) && $manager_id > 0) {
            $sms->manager_id = $manager_id;
        } else if (isset($_POST['Sms'])) {
            $sms->attributes = $_POST['Sms'];
        }
        if ($manager_id == 0) {
            $manager_id = $sms->manager_id;
        }

        $mobile = Mobile::model()->with('manager')->findByAttributes(array('manager_id' => $manager_id));
        $manager = $mobile->manager;

        if (!$manager || !$manager->hasRole(Role::INACTIVE))
            _home();

        // send activation code if not already sent
        if (!Sms::isActivationCodeSent($mobile)) {

            // create and send
            $provider = new Clickatell;
            if (!$sms->sendActivationCode($provider, $mobile->mobile)) {
                Utils::notice('sms', _t('Failed to send activation sms. Provider said:<br />') . $provider->getError(), 'error');
            }

            // save the activation code
            $mobile->activation_code = $sms->createdCode;
            if (!$mobile->saveAttributes(array('activation_code'))) {
                Utils::notice('manager', _t('Failed to save sms activation code to database.'), 'error');
                error_log(Debug::arObjectErrors($manager));
            }

            Debug::notice('failedSms', "DEBUG: Created sms code is: $sms->createdCode", 'warning');
        }

        if (isset($_POST['Sms'])) {
            if ($sms->validate()) {

                if ($sms->verifyActivationCode($mobile)) {
                    _redirect('user/activate', array('uid' => $manager->id));
                }
                Utils::notice('sms', Debug::getSessionError(), 'error');
            }
            $sms->code = null;
        }
        $this->render('index', array('model' => $sms));
    }

}