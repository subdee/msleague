<?php

class Clickatell implements SMSProvider {

    const SMS_FROM = 'MSLeague';
    
    private $_api;
    private $_user;
    private $_password;
    private $_url;
    private $_client;
    private $_error;
    private $_session_id;

    function __construct() {
        $this->_api = _app()->getModule('sms')->soap_api_id;
        $this->_user = _app()->getModule('sms')->soap_user;
        $this->_password = _app()->getModule('sms')->soap_password;
        $this->_url = _app()->getModule('sms')->soap_url;
        $this->_error = '1';
        $this->_client = new SoapClient($this->_url);
    }

    private function authorize($setSession=true) {
        $auth = $this->_client->auth($this->_api, $this->_user, $this->_password);
        if (substr($auth, 0, 2) == 'OK') {
            $this->_session_id = substr($auth, 4);
            if ($setSession)
                _app()->session->add('soap_session_id', $this->_session_id);
            return true;
        }
        $this->setError($auth);
        return false;
    }

    /**
     * Send a message using the Clickatell SOAP API
     * 
     * @property array $mobile An array of mobile numbers to send message to
     * @property string $msg The SMS content
     * 
     * @return boolean Whether the message was sent to the API succesfully
     */
    public function sendMessage($mobile,$msg) {
        if (_app()->session->get('soap_session_id') == null) {
            if(!$this->authorize())
                return false;
        }
        $msg = unpack('H*hex', iconv('UTF-8', 'UCS-2BE', $msg));
        $send = $this->_client->sendmsg($this->_session_id, null, null, null, $mobile, self::SMS_FROM, strtoupper($msg['hex']), null, null, null, null, null, null, null, null, null, null, true);
        if (substr($send[0], 0, 2) == 'ID')
            return true;
        //authorize to try and get a new session id and try one more time
        if(!$this->authorize())
            return false;
        $send = $this->_client->sendmsg($this->_session_id, null, null, null, $mobile, self::SMS_FROM, strtoupper($msg['hex']), null, null, null, null, null, null, null, null, null, null, true);
        if (substr($send[0], 0, 2) == 'ID')
            return true;
        $this->setError($send[0]);
        return false;
    }
    
    public function getError() {
        return $this->_error;
    }
    
    private function setError($error) {
        $this->_error = $error;
    }
    
    private function unicode($message) {
        return unpack('H*hex', iconv('UTF-8', 'UCS-2BE', $message));
    }

}

?>
