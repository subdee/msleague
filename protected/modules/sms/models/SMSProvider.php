<?php

interface SMSProvider{
    /**
     * Send a message using the provider's SOAP API
     * 
     * @property array $mobile An array of mobile numbers to send message to
     * @property string $msg The SMS content
     * 
     * @return boolean Whether the message was sent to the API succesfully
     */
    public function sendMessage($mobile,$msg);
    /**
     * Get the last error from the provider API
     * 
     * @return string The error code
     */
    public function getError();
}

?>
