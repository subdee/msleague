<?php

/**
 * Sms class.
 * Sms class contains the SMS structure
 */
class Sms extends CFormModel {

    public $code;
    public $manager_id;
    public $createdCode;

    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
//            array('code', 'length', 'min' => 6, 'max' => 6),
//            array('code', 'required', 'message' => _t('You must enter the SMS code sent to your mobile phone.')),
            array('manager_id, code', 'safe'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'code' => _t('Verification code'),
        );
    }

    /**
     *
     * @param SMSProvider $provider
     * @param int $mobile
     * @return bool 
     */
    public function sendActivationCode(SMSProvider $provider, $mobile) {

        // create the activation code
        $this->createdCode = $this->createActivationCode();

        $msg = _gameName() . ' ' . _t('Your verification code is: {code}', array('{code}' => $this->createdCode));

        if (!$provider->sendMessage(array($mobile), $msg))
            return false;

        return true;
    }

    /**
     *
     * @param Mobile $mobile
     * @return bool 
     */
    public function verifyActivationCode(Mobile $mobile) {
        $manager = $mobile->manager;

        // if sms code is null, sms is already verified.
        if ($mobile->activation_code == null)
            return true;

        if ($manager->hasRole(Role::INACTIVE) && $this->code == $mobile->activation_code) {
            $mobile->activation_code = null;
            if ($mobile->saveAttributes(array('activation_code'))) {
                return true;
            }
            error_log(Debug::arObjectErrors($mobile));
            Debug::setSessionObjectError($mobile);
        }
        Debug::setSessionError(_t('The code you provided is not correct. Please re-enter your code'));

        return false;
    }

    /**
     * When sms code is sent, we save the code in the database.
     * @param Mobile $mobile
     * @return bool 
     */
    public static function isActivationCodeSent(Mobile $mobile) {
        return ($mobile->activation_code != null);
    }

    /**
     *
     * @param Mobile $mobile
     * @return bool 
     */
    public static function isVerified(Mobile $mobile) {
        return ($mobile->activation_code == null);
    }

    private function createActivationCode() {
        $character_set_array = array();
        $character_set_array[] = array('count' => 2, 'characters' => 'abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ');
        $character_set_array[] = array('count' => 4, 'characters' => '0123456789');
        $temp_array = array();
        foreach ($character_set_array as $character_set) {
            for ($i = 0; $i < $character_set['count']; $i++) {
                $temp_array[] = $character_set['characters'][rand(0, strlen($character_set['characters']) - 1)];
            }
        }
        shuffle($temp_array);
        return implode('', $temp_array);
    }

}
