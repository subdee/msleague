<div class="box_c">
    <div class="boxheader"><span class="title_c"><?php echo _t('SMS Verification'); ?></span></div>
    <div class="boxmain_c">
        <div class="boxtext_c smsBox">
            <?php $this->widget('Notice', array('type' => 'info', 'message' => _t('Enter below the verification code that you received on your mobile to activate your account.'))); ?>
            <?php $this->widget('Notice', array('session' => 'sms')); ?>
            <?php $this->widget('Notice', array('session' => 'manager')); ?>
            <?php $this->widget('Notice', array('session' => 'failedSms')); ?>
            <div class="form">
                <?php
                $form = $this->beginWidget('CActiveForm', array(
                        'action' => _url('sms/verify'),
                        'htmlOptions' => array(
                            'onSubmit' => '$(this).find(":submit").attr("disabled", true);',
                        )
                    ));
                ?>

                <div class="row">
                    <?php echo $form->labelEx($model, 'code'); ?>
                    <?php echo $form->textField($model, 'code', array('maxlength' => '6', 'size' => '6')); ?>
                    <?php echo $form->error($model, 'code'); ?>
                </div>
                <?php echo $form->hiddenField($model, 'manager_id'); ?>

                <div class="row buttons">
                    <?php echo CHtml::submitButton(_t('Verify')); ?>
                </div>

                <?php $this->endWidget(); ?>

                <?php echo _t("If you didn't receive an SMS, please contact us at ")._support(); ?>
            </div>
        </div>
    </div>
    <div class="boxfooter_c"><!-- empty--></div>
</div>