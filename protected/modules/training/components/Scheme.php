<?php

class Scheme extends CApplicationComponent {
    const STATE_START = 0;
    const STATE_TASK_RUNNING = 1;
    const STATE_TASK_DONE = 2;
    const STATE_END = 3;
    const STATE_CLOSED = 4;

    private $_managerRole;

    public function init() {
        if (_is_logged_in()) {
//            $this->install();
//            $this->assignToManager();
            Utils::registerCallback('onBeforeProcessTeam', $this, 'saveManagerRole');
            Utils::registerCallback('onAfterProcessTeam', $this, 'assignToManager');

            $this->session('enabled', false);
            $this->nullify('task');
            $this->setup();
        }

        parent::init();
    }

    /**
     * Adds in the database the necessary manager tasks needed for the training system.
     * Only needs to happen once, when we install the system.
     * Adding these tasks does not mean that they have not been already installed by some other
     * system.
     */
    public function install() {
        $transaction = _app()->db->beginTransaction();
        try {
            _app()->managerPointSystem->addTaskDescriptions(array(
                array(
                    'name' => 'makeOneSubstitution',
                    'points' => 1,
                    'event' => 'onSwapPlayers',
                ),
                array(
                    'name' => 'assignLeader',
                    'points' => 1,
                    'event' => 'onAssignLeader',
                ),
                array(
                    'name' => 'changeFormation',
                    'points' => 1,
                    'event' => 'onChangeFormation',
                ),
                array(
                    'name' => 'makeOneTransaction',
                    'points' => 1,
                    'event' => 'onStockmarketTransaction',
                ),
            ));

            $transaction->commit();
        } catch (CDbException $e) {
            $transaction->rollback();
            Debug::logToWindow($e->getMessage(), 'install manager tasks');
        } catch (CException $e) {
            $transaction->rollback();
            Debug::logToWindow(Debug::exceptionDetailString($e));
        } catch (CValidateException $e) {
            $transaction->rollback();
        }
    }

    /**
     * Assigns tasks to manager in the database.
     * @param CEvent $e
     */
    public function assignToManager() {
        // If manager is reset, do not assign any training
        if ($this->_managerRole == Role::RESET)
            return;

        _app()->managerPointSystem->assignTasks(_manager()->id, 'training', array(
            array('name' => 'makeOneSubstitution'),
            array('name' => 'assignLeader'),
            array('name' => 'changeFormation'),
            array('name' => 'makeOneTransaction'),
        ));
    }

    /**
     * Removes manager's training tasks.
     * @param type $manager_id
     */
    public function clearManager($manager_id) {
        try {
            ManagerTask::model()->deleteAllByAttributes(array(
                'manager_id' => $manager_id,
                'context' => 'training'
            ));
            _manager()->refresh();
        } catch (CDbException $e) {
            Debug::logToWindow(Debug::exceptionDetailString($e));
        } catch (CException $e) {
            Debug::logToWindow(Debug::exceptionDetailString($e));
        }
    }

    /**
     * Sets up all necessary data for the training system.
     * Needs to setup every time.
     * TODO: Use sessions to keep the training status?
     */
    private function setup() {

        if (Utils::isAdminUser() || _manager()->hasRole(Role::RESET))
            return;

        $isBeginingOfTraining = true;

        // Set one task for the trainer.
        if ($this->session('task') === null) {
            foreach (_manager()->tasks as $task) {
                if ($task->context != 'training')
                    continue;

                $this->session('enabled', true);

                if ($task->completed) {
                    $isBeginingOfTraining = false;
                    continue;
                }

                if ($task->desc->class) {
                    $newTask = new $task->desc->class;
                    $newTask->setIsNewRecord(false);
                    $newTask->attributes = $task->attributes;
                    $task = $newTask;
                }
                $task->setContext($this);

                $this->session('task', $task);
                break;
            }
        }

        // For the trainer to be enabled there must be at least one active task present.
        if ($this->session('enabled') == true) {

            // Check if pitch and stockmarket are open.
            if (Gameconfiguration::model()->isOpen()) {

                if ($isBeginingOfTraining && $this->session('state') === null) {
                    $state = self::STATE_START;
                } else {
                    if ($this->session('task') === null) {
                        $state = self::STATE_END;
                    } else {
                        $state = self::STATE_TASK_RUNNING;

                        if ($this->session('show') === true) {
                            // Register task with the point system
                            _app()->managerPointSystem->connect($this->session('task'));
                        }
                    }
                }

                if ($this->session('state') === null || $this->session('state') == self::STATE_CLOSED || $state == self::STATE_END)
                    $this->session('state', $state);
                if ($this->session('show') === null)
                    $this->session('show', true);

                // setup advisor
                Utils::registerCallback('onLayoutRightColumnBegin', $this, 'renderAdvisor');
            } else {
                // If not, advisor should show a standard message.
                if (Gameconfiguration::model()->getCloseTime()) {
                    $this->session('flash', false);
                    $this->session('show', false);
                    $this->session('state', self::STATE_CLOSED);
                    Utils::registerCallback('onLayoutRightColumnBegin', $this, 'renderAdvisor');
                }
            }
        }
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return mixed
     */
    public function session($name, $value=null) {
        if ($value === null) {
            if (isset($_SESSION['trainingModule'][$name]))
                return $_SESSION['trainingModule'][$name];
        } else {
            $_SESSION['trainingModule'][$name] = $value;
        }
        return null;
    }

    /**
     * @param string $name
     */
    public function nullify($name) {
        unset($_SESSION['trainingModule'][$name]);
    }

    /**
     * @param CEvent $event
     */
    public function renderAdvisor(CEvent $event) {
        $nextTask = $this->session('task');
        if ($this->session('state') == self::STATE_START) {
            $currentTaskView = 'training.views._welcome';
        } else if ($this->session('state') == self::STATE_TASK_RUNNING && $nextTask) {
            $currentTaskView = "training.views._{$nextTask->desc->name}";
        } else if ($this->session('state') == self::STATE_TASK_DONE) {
            $currentTaskView = "training.views._{$nextTask->desc->name}Completed";
        } else if ($this->session('state') == self::STATE_END) {
            $currentTaskView = 'training.views._end';
        } else {
            $currentTaskView = 'training.views._closed';
        }

        Utils::publishModuleAssetUrlToJS('training');
        $event->sender->widget('advisor.components.AdvisorBox', array(
            'flashing' => $this->session('flash'),
            'show' => $this->session('show'),
            'message' => array(
                'view' => $currentTaskView
            )
        ));
    }

    /**
     * @param array $params
     * @param ManagerTask $task
     */
    public function onManagerTask($params, ManagerTask $task) {
        if ($task->completed)
            $this->nextStep();
    }

    /**
     *
     */
    public function nextStep() {
        if ($this->session('state') == self::STATE_START) {
            $this->session('state', self::STATE_TASK_RUNNING);
        } else if ($this->session('state') == self::STATE_TASK_RUNNING) {
            $this->session('state', self::STATE_TASK_DONE);
        } else if ($this->session('state') == self::STATE_TASK_DONE) {
            $this->session('state', self::STATE_TASK_RUNNING);
            $this->nullify('task');
        }
    }

    /**
     *
     */
    public function finish() {
        $this->session('enabled', false);
        $this->clearManager(_manager()->id);
    }

    public function saveManagerRole(CEvent $e) {
        $this->_managerRole = _manager()->role;
    }

}

?>
