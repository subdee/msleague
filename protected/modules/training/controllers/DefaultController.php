<?php

class DefaultController extends Controller {

    public function filters() {
        return array('accessControl',);
    }

    public function accessRules() {
        return array(
            array(
                'allow',
                'expression' => '_manager()->hasRole(Role::MANAGER)',
            ),
            array(
                'deny',
                'users' => array('*')
            ),
        );
    }

    public function actionNextStep() {
        _app()->getModule('training')->scheme->nextStep();
        _return();
    }

    public function actionFinish() {
        _app()->getModule('training')->scheme->finish();
        $this->redirect(_url('match/index'));
    }

    public function actionToggleSkip() {
        if (Utils::isAjax()) {
            $show = _app()->getModule('training')->scheme->session('show');
            $show = $show === null || $show ? false : true;
            _app()->getModule('training')->scheme->session('show', $show);
        }
    }

}