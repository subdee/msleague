<?php
$this->render('training.views._trainingAdvisorTemplate', array(
    'action' => _t('Change the captain of your team'),
    'description' => array(_t("The captain of your team has been set automatically. Change him.")),
    'prompt' => array(
        'text' => _t('Change captain'),
        'url' => _url('pitch/index'),
    )
));
?>
<script type="text/javascript">
    var Arrow = {
        arrow : null,
        elem : $('div.leader'),

        Setup : function() {
            this.arrow = $('<img />')
            .attr('src', MSL.ModuleImage('training', 'hint-right.gif'))
            .addClass('training-hint-arrow hidden')
            .appendTo('#container')
            ;
        },
        Show : function() {
            Arrow.arrow.show().css({
                'top' : Arrow.elem.offset().top + 5,
                'left' : Arrow.elem.offset().left - 50
            });
            Arrow.elem.addClass('training-hint-frame').animateBorderColor('#FF8A00', 1000);
        },
        Hide : function() {
            Arrow.arrow.hide();
            Arrow.elem.removeClass('training-hint-frame');
        }
    }
    $(document).ready(function(){
        if(Arrow.elem.length == 0) return;

        Arrow.Setup();

        $(document).bind('onAdvisorShow', Arrow.Show);
        $(document).bind('onAdvisorHide', Arrow.Hide);

        $(document).bind('onWidgetNoticeClose', function(){
            if(MSL.Advisor.visible)
                Arrow.Show();
        });
    });
</script>