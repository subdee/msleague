<?php

$this->render('training.views._trainingAdvisorTemplate', array(
    'action' => _t('Congratulations! You changed the captain in your team'),
    'isSuccess' => true,
    'description' => array(_t("TIP: Captain receives double points. For example, a starting midfielder who gives an assist gets {assistPoints} pts, while the captain gets {assistPointsLeader} pts for the same reason.",array(
        '{assistPoints}' => Event::model()->getPoints('Assists', 'Midfielder'),
        '{assistPointsLeader}' => Event::model()->getPoints('Assists', 'Midfielder','Leader'),
    ))),
    'prompt' => array(
        'text' => _t("Next training course"),
        'url' => _url('training/nextStep'),
    )
));
_user()->setReturnUrl(_url('pitch/index'));
?>