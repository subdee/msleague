<?php

$this->render('training.views._trainingAdvisorTemplate', array(
    'action' => _t("Congratulations! You changed your team's formation."),
    'isSuccess' => true,
    'description' => array(_t("TIP: 4-3-3 is an attacking formation with 3 starting strikers, while 5-3-2 is a defensive formation with 5 starting defenders.")),
    'prompt' => array(
        'text' => _t("Next training course"),
        'url' => _url('training/nextStep'),
    )
));
_user()->setReturnUrl(_url('pitch/index'));
?>