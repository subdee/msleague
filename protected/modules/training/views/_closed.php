<?php
$this->render('training.views._trainingAdvisorTemplate', array(
    'description' => array(
        _t('Training is temporarily disabled as there are matches in progress.')
    ),
));
?>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).bind('onAdvisorShow', function(ev, obj){
            $('.advisor-balloon .notice').show();
        });
    });
</script>