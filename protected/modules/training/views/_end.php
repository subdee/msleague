<?php

$this->render('training.views._trainingAdvisorTemplate', array(
    'action' => _t('Congratulations {name}, you completed the training courses.', array('{name}' => _manager()->username)),
    'isSuccess' => true,
    'description' => array(
        _t('Use your remaining cash to buy shares of players that will play in the following match day.'),
    ),
    'prompt' => array(
        'text' => _t('View next matches'),
        'url' => _url('training/finish'),
    )
));
?>