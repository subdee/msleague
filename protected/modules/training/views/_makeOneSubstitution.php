<?php
$this->render('training.views._trainingAdvisorTemplate', array(
    'action' => _t('Make a substitution'),
    'description' => array(_t("Let's start adjusting your team. In your team's pitch, click on a starting and then on a bench player (or the opposite) in order to make a substitution.")),
    'prompt' => array(
        'text' => _t('Make a substitution'),
        'url' => _url('pitch/index'),
    )
));
?>
<script type="text/javascript">
    var Arrow = {
        arrow : null,
        arrow2 : null,
        elem : $('.pitch-layout .pitch'),
        elem2 : $('.pitch-layout .bench'),

        Setup : function() {
            this.arrow = $('<img />')
            .attr('src', MSL.ModuleImage('training', 'hint-right.gif'))
            .addClass('training-hint-arrow hidden')
            .appendTo('#container')
            ;
            this.arrow2 = $('<img />')
            .attr('src', MSL.ModuleImage('training', 'hint-right.gif'))
            .addClass('training-hint-arrow hidden')
            .appendTo('#container')
            ;
        },
        Show : function() {
            Arrow.arrow.show().css({
                'top' : Arrow.elem.offset().top,
                'left' : Arrow.elem.offset().left
            });
            Arrow.elem.addClass('training-hint-frame').animateBorderColor('#FF8A00', 1000);
            Arrow.arrow2.show().css({
                'top' : Arrow.elem2.offset().top + 50,
                'left' : Arrow.elem2.offset().left
            });
            Arrow.elem2.addClass('training-hint-frame').animateBorderColor('#FF8A00', 1000);
        },
        Hide : function() {
            Arrow.arrow.hide();
            Arrow.elem.removeClass('training-hint-frame');
            Arrow.arrow2.hide();
            Arrow.elem2.removeClass('training-hint-frame');
        }
    }
    $(document).ready(function(){
        if(Arrow.elem.length == 0) return;

        Arrow.Setup();

        $(document).bind('onAdvisorShow', Arrow.Show);
        $(document).bind('onAdvisorHide', Arrow.Hide);

        $(document).bind('onWidgetNoticeClose', function(){
            if(MSL.Advisor.visible)
                Arrow.Show();
        });
    });
</script>