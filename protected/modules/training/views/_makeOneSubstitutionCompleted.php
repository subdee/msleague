<?php

$this->render('training.views._trainingAdvisorTemplate', array(
    'action' => _t('Congratulations! You made a substitution.'),
    'isSuccess' => true,
    'description' => array(
        _t("TIP: You earn points based on the real performance of your players (e.g. {goalPoints} pts if a striker scores a goal).",array(
            '{goalPoints}' => Event::model()->getPoints('Goals Scored','Striker')
        )),
        _t("Starting players receive full points while your bench players receive half points."),
        ),
    'prompt' => array(
        'text' => _t("Next training course"),
        'url' => _url('training/nextStep'),
    )
));
_user()->setReturnUrl(_url('pitch/index'));
?>