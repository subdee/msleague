<?php
$this->render('training.views._trainingAdvisorTemplate', array(
    'action' => _t('Make a transaction in the stockmarket'),
    'description' => array(_t("You have {cash} remaining. In the stockmarket, click on player and choose how many shares you want to buy.", array('{cash}' => _currency(_manager()->portfolio_cash)))),
    'prompt' => array(
        'text' => _t('Make a transaction'),
        'url' => _url('stockmarket/buy'),
    )
));
?>
<script type="text/javascript">
    var Arrow = {
        arrow : null,
        elem : $('.dataTables_scroll'),
        elem2 : null,

        Setup : function() {
            this.arrow = $('<img />')
            .attr('src', MSL.ModuleImage('training', 'hint-right.gif'))
            .addClass('training-hint-arrow hidden over-everything')
            .appendTo('#container')
            ;
        },
        ShowInPlayerList : function() {
            Arrow.arrow.show().css({
                'top' : Arrow.elem.offset().top + 0,
                'left' : Arrow.elem.offset().left + -30
            });
            Arrow.elem.addClass('training-hint-frame').animateBorderColor('#FF8A00', 1000);
            if(Arrow.elem2) Arrow.elem2.removeClass('training-hint-frame');
        },
        ShowInDialog : function() {
            Arrow.arrow.show().css({
                'top' : Arrow.elem2.offset().top + 20,
                'left' : Arrow.elem2.offset().left + -30
            });
            Arrow.elem.removeClass('training-hint-frame');
            Arrow.elem2.addClass('training-hint-frame').animateBorderColor('#FF8A00', 1000);
        },
        Hide : function() {
            Arrow.arrow.hide();
            Arrow.elem.removeClass('training-hint-frame');
            if(Arrow.elem2) Arrow.elem2.removeClass('training-hint-frame');
        }
    }

    $(document).bind('onStockmarketDatatableReady', function(){
        Arrow.elem = $('.dataTables_scroll');
        if(Arrow.elem.length == 0) return;

        Arrow.Setup();

        $(document).bind('onAdvisorShow', Arrow.ShowInPlayerList);
        $(document).bind('onAdvisorHide', Arrow.Hide);

        var isTransactionCompleted = false;

        $(document).bind('onStockmarketDialogOpen', function(event, mydialog){
            if(!MSL.Advisor.visible) return;
            var dialog = $(mydialog.selector);
            Arrow.elem2 = $('.info-box.slider-info', dialog);
            if(!Arrow.elem2) {
                console.error('.info-box.slider-info NOT FOUND');
                return;
            }
            Arrow.ShowInDialog();
        });
        $(document).bind('onStockmarketDialogClose', function(){
            if(!MSL.Advisor.visible) return;
            if(isTransactionCompleted) return;
            Arrow.ShowInPlayerList();
        });
        $(document).bind('onStockmarketBeforeTransaction', function(){
            Arrow.Hide();
            isTransactionCompleted = true;
        });
    });
</script>