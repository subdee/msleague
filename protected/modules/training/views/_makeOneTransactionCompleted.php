<?php

$this->render('training.views._trainingAdvisorTemplate', array(
    'action' => _t('Congratulations! You completed a transaction'),
    'isSuccess' => true,
    'description' => array(_t("TIP: You get points for each share that you own. For example, 5 shares of a player who receives 10 pts gives you 50 pts.")),
    'prompt' => array(
        'text' => _t("Next training course"),
        'url' => _url('training/nextStep'),
    )
));
_user()->setReturnUrl(_url('stockmarket/buy'));
?>