<div class="title">
    <?php echo _t('TRAINING COURSE'); ?>
    <img src="<?php echo Utils::imageUrl('iconset/close.png'); ?>" />
</div>
<?php if (isset($action)) : ?>
    <div class="action">
        <?php if (isset($isSuccess)) : ?>
            <?php $this->widget('Notice', array('type' => 'success', 'message' => $action)); ?>
        <?php elseif (isset($isWarning)) : ?>
            <?php $this->widget('Notice', array('type' => 'warning', 'message' => $action)); ?>
        <?php else : ?>
            <span><?php echo $action; ?></span>
        <?php endif; ?>
    </div>
<?php endif; ?>
<div class="description">
    <?php foreach ($description as $d) : ?>
        <p>
            <?php echo $d; ?>
        </p>
    <?php endforeach; ?>
</div>
<div class="prompt">
    <div class="skip left">
        <?php
        echo CHtml::ajaxLink(_t('&laquo; Not now'), _url('training/toggleSkip'), array(
            'beforeSend' => 'MSL.Advisor.Hide',
        ));
        ?>
    </div>
    <?php if (isset($prompt) && $prompt['url'] != _app()->getRequest()->getUrl()) : ?>
        <div class="right">
            <?php echo CHtml::link($prompt['text'] . '&nbsp;&raquo;', $prompt['url']); ?>
        </div>
    <?php endif; ?>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.advisor-balloon .title > img').click(MSL.Advisor.Hide);
        $(document).bind('onAdvisorClick', function(){
            $.ajax({
                url: MSL.Url('toggleSkip', 'training')
            })
        });
    })
</script>