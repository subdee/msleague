<?php

$this->render('training.views._trainingAdvisorTemplate', array(
    'description' => array(
        _t('Hello {name}! Welcome to {game}.', array('{name}' =>'<strong>'. _manager()->username.'</strong>', '{game}' => _gameName())),
        _t("I am your game advisor. As I can see you are a new manager and might need some training to start playing."),
    ),
    'prompt' => array(
        'text' => _t('Start training'),
        'url' => _url('training/nextStep'),
    )
));
_user()->setReturnUrl(_url('pitch/index'));
?>
