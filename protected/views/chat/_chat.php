<div class="chatbox">
    <?php
    if ($shouts != null) :
        $this->renderPartial('//chat/_newChat', array('shouts' => $shouts));
    else :
        echo _t('Noone has spoken yet');
    endif;
    ?>
</div>