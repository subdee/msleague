<div class="chatForm">
    <div class="chatFormTitle"><?php echo _t('Live discussion'); ?>
        <img src="<?php echo Utils::imageUrl('messaging/chat.png'); ?>" title="<?php echo _t('Live discussion'); ?>">
    </div>
    <div class="chat"><?php $this->renderPartial('//chat/_chat', array('error' => false, 'shouts' => $shouts)); ?></div>
    <div id="errorMsg"></div>
    <?php
    if (_isEnabled('Chat') && !$isUserBanned) :
        echo CHtml::form();
        echo CHtml::textField('shout', null, array('size' => 40));
        echo CHtml::submitButton(_t('Say'));
        ?>
        <img src="<?php echo Utils::imageUrl("ajaxloader/small-circle.gif"); ?>" />
        <?php
        echo CHtml::endForm();
    elseif (_isEnabled('Chat') && $isUserBanned) :
        echo _t('You are not allowed to chat because you are banned');
    else :
        echo _t('The chat has been temporarily disabled.');
    endif;
    ?>
    <input type="hidden" id="shoutCount" value="<?php echo count($shouts); ?>" />
</div>
<div class="chatError"></div>