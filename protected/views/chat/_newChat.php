<?php
if ($shouts != null) :
    foreach ($shouts as $shout) :
        $class = 'user';
        if ($shout->manager->username == _manager()->username)
            $class = 'self';
        ?>
        <div class="<?php echo $class; ?>">
            <?php echo Utils::relativeDate($shout->date_entered) . ' '; ?>
            <a href="<?php echo _url('profile/index', array('id' => $shout->manager->id)); ?>">
                <?php echo $shout->manager->username; ?>
            </a>:
            <?php echo ' ' . Utils::replaceSmileys($shout->shout); ?>
        </div>
        <?php
    endforeach;
endif;
?>
