<?php if ($canReport) : ?>
    <?php $this->widget('Notice', array('session' => 'report')); ?>
    <div class="reporttable">
        <form method="POST" action="">
            <table class="message" cellpadding="0" cellspacing="0">
                <tr>
                    <td><b><?php echo _t('Message'); ?>:</b></td>
                    <td><?php echo $comment->comment; ?></td>
                </tr>
            </table>
            <table cellpadding="4" cellspacing="0">
                <tr class="label">
                    <td><?php echo _t('Report for the following reason:'); ?></td>
                </tr>
                <tr class="options">
                    <td>
                        <ul>
                            <?php
                            $r = isset($_POST['reason']) ? $_POST['reason'] : '';
                            foreach ($reasons as $reason) :
                                $checked = '';
                                if ($reason->reason === $r)
                                    $checked = 'checked';
                                ?>
                                <li><label><input name="reason" value="<?php echo $reason->id; ?>" type="radio" <?php echo $checked; ?>/> <?php echo $reason->reason; ?></label></li>
                                <?php
                            endforeach;
                            ?>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td>
                        <textarea name="comment"></textarea>
                        <input type="hidden" value="<?php echo $id; ?>" name="id" />
                        <input value="<?php echo _t('Report'); ?>" type="submit" />
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <?php
else :
    $this->widget('Notice', array('message' => $noticeMsg, 'type' => 'warning'));
endif;
?>