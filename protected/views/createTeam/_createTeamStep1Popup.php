<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'create-team-first-step',
    // additional javascript options for the dialog plugin
    'options' => array(
        'title' => _t('Create your team'),
        'resizable' => false,
        'draggable' => false,
        'autoOpen' => true,
        'width' => '580px',
        'position' => array('center', 'middle'),
        'modal' => true,
        'closeOnEscape' => false,
        'open' => 'js:function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }'
    ),
));
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'htmlOptions' => array(
        'onSubmit' => '$(this).find(":submit").attr("disabled", true);',
    ),
    'focus' => array($model, 'teamName')
    ));
?>
<div class="row">
    <?php echo $form->labelEx($model, 'teamName'); ?>
    <?php echo $form->textField($model, 'teamName', array('maxlength' => '30')); ?>
    <?php echo $form->error($model, 'teamName'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'favTeamId'); ?>
    <?php echo $form->dropDownList($model, 'favTeamId', CHtml::listData(Team::model()->findAll(), 'id', 'name'), array('prompt' => _t('Select team'))); ?>
    <?php echo $form->error($model, 'favTeamId'); ?>
</div>

<div class="row buttons">
    <?php echo CHtml::submitButton(_t('Continue')); ?>
    <?php echo CHtml::link(_t('Not now'), _url('site/index')); ?>
</div>
<?php $this->endWidget('CActiveForm'); ?>

<?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>
