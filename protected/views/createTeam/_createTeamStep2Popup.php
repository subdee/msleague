<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'create-team-second-step',
    // additional javascript options for the dialog plugin
    'options' => array(
        'title' => _t('Create your team'),
        'resizable' => false,
        'draggable' => false,
        'autoOpen' => true,
        'width' => '580px',
        'position' => array('center', 'middle'),
        'modal' => true,
        'closeOnEscape' => false,
        'open' => 'js:function(event, ui) { $(".ui-dialog-titlebar-close").hide(); }'
    ),
));
?>
<p>
    <?php echo _t('Create your team by using your {budget} budget to buy initially 1 share of each player that you may pick.', array('{budget}' => _app()->locale->getCurrencySymbol('EUR') . Gameconfiguration::model()->get('manager_initial_budget'))); ?>
</p>
<p>
    <?php echo _t('Your team must consist of 11 starting players (4-4-2 formation) and 4 bench players (1 of each position).'); ?>
</p>
<p>
    <?php echo _t('Note: You cannot buy shares of more than {maxPerTeam} players of the same team.', array('{maxPerTeam}' => Gameconfiguration::model()->get('max_players_same_team'))); ?>
</p>

<?php $form = $this->beginWidget('CActiveForm'); ?>
<div class="row buttons">
<?php echo CHtml::hiddenField('createTeam', 'done'); ?>
    <?php echo CHtml::submitButton(_t('Create your team!')); ?>
</div>
    <?php $this->endWidget('CActiveForm'); ?>

<?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>
