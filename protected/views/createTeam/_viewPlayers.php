<?php
$this->widget('Notice', array(
    'message' => _t('Buy one share by picking {player} from the list below:', array(
        '{player}' => BasicPosition::translated($position, array('accusative' => true))))
));
$this->widget('Notice', array(
    'id' => 'waitforredirect', 
    'message' => _t('Wait...'),
    'type' => 'loader',
    'ajax' => true,
));
?>
<table class="player-list create-team">
    <thead>
        <tr>
            <th class="first"><?php echo _t('Name'); ?></th>
            <th><?php echo _t('Team'); ?></th>
            <th>
                <?php
                echo _t('Share price ({symbol})', array(
                    '{symbol}' => _app()->locale->getCurrencySymbol('EUR')
                ));
                ?>
            </th>
        </tr>
        <tr class="filters">
            <th class="name"><input type="text" value="<?php echo _t('Search player...'); ?>" maxlength="17" /></th>
            <th class="team">
                <select>
                    <option value=""></option>
                    <?php foreach ($teams as $team) : ?>
                        <option value="<?php echo $team->name; ?>"><?php echo $team->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($displayList as $dl)
            _controller()->renderPartial('_viewPlayersRow', array('display' => $dl));
        ?>
    </tbody>
</table>

<div class="stats-link"><?php echo CHtml::link(_t('Player Statistics'), _url('players/stats'), array('target' => '_blank')); ?></div>