<?php

$url = _url('createTeam/addTempPlayer', array(
    'player_id' => $display->player->id,
    ));

echo CHtml::openTag('tr', array(
    'class' => $display->cssClass,  
    'onclick' => $display->isActive ? "$('#playerList .dataTables_wrapper').hide();$('#waitforredirect').show();MSL.Goto('$url');" : '',
    'title' => $display->inactivityReason,
));

echo CHtml::openTag('td', array('class' => 'first playername'));
echo CHtml::tag('span', array(), $display->player->shortname);
echo $display->injuredHtml();
echo $display->suspendedHtml();
echo CHtml::closeTag('td');
echo CHtml::tag('td', array(), $display->player->team->name);
echo CHtml::tag('td', array('class' => 'stock-value'), $display->player->current_value);

echo CHtml::closeTag('tr');
?>