<div class="pitch-page">

    <?php
    $this->widget('Notice', array(
        'message' => _t('Choose 15 players (11 starters, 4 substitutes) and buy 1 share of each one. You may buy more shares after you create your team.'),
        'type' => 'info',
    ));
    $this->widget('Notice', array('session' => 'notice'));
    ?>

    <div class="profile-usermenu create-team">
        <div class="meta">
            <div class="name"><?php echo _team()->name; ?></div>
            <div class="details">
                <div class="left"><span><?php echo $numOfPlayers; ?>/15</span> <?php echo _t('players'); ?></div>
                <div class="right"><?php echo _t('Remaining cash'); ?>: <span><?php echo _currency(_manager()->portfolio_cash); ?></span></div>
            </div>
        </div>
    </div>

    <?php _controller()->renderPartial('/pitch/_pitch', array('players' => $players, 'formation' => '442')); ?>
    <?php
    $disabled = $numOfPlayers < 15 ? 'disabled' : '';
    echo CHtml::form(_url('createTeam/processTempTeam'), 'post', array('class' => 'create-team'));
    if (Debug::isDebug())
        echo _l(_t('Random selection'), _url('createTeam/createRandomTeam'));
    echo CHtml::submitButton(_t('Clear team'), array('name' => 'reset'));
    echo CHtml::submitButton(_t('Start game'), array('name' => 'save', 'disabled' => $disabled));
    echo CHtml::endForm();
    ?>

</div>

<?php
if (!Yii::app()->session['step1']) {
    _controller()->renderPartial('_createTeamStep1Popup', array('model' => $model));
} elseif (!Yii::app()->session['step2']) {
    _controller()->renderPartial('_createTeamStep2Popup');
}


$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'popupList',
    'options' => array(
        'title' => _t('Choose player'),
        'resizable' => false,
        'draggable' => false,
        'autoOpen' => false,
        'width' => '600',
        'height' => '480',
        'position' => array('center', 'middle'),
        'modal' => true,
        'closeOnEscape' => true,
    ),
));
?>
<div id="playerList">
</div>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>