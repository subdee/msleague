<div class="grid-view">
    <table class="items" width="100%">
        <thead>
            <tr>
                <th></th>
                <?php foreach ($pos as $p) : ?>
                    <th><?php echo BasicPosition::model()->translated($p->name); ?></th>
                <?php endforeach; ?>
            </tr>
        </thead>
        <tbody>
            <?php $ctr = 0; ?>
            <?php foreach ($events as $e) : ?>
                <?php $class = $ctr & 1 ? 'odd' : 'even'; ?>
                <tr class="<?php echo $class; ?>">
                    <td class="firstcol"><?php echo Event::model()->translated($e[0]); ?></td>
                    <?php if ($e[0] == 'Minutes Played Less') : ?>
                        <?php for ($cnt = 1; $cnt < 5; $cnt++) : ?>
                            <td><?php echo $e[$cnt]; ?></td>
                        <?php endfor; ?>
                    <?php else : ?>
                        <?php foreach ($i[$e[0]] as $pos => $value) : ?>
                            <td><?php echo $value; ?></td>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tr>
                <?php ++$ctr; ?>
            <?php endforeach; ?>
        </tbody>
    </table>

    <p style="font-size:10px;"><?php echo _t('* the player receives zero points if he didn\'t participate in the match.'); ?></p>
</div>