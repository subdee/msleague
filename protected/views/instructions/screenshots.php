<div class="screenshot-row">
    <div class="profile-usermenu">
        <h3><?php echo _t('Create your team'); ?></h3>
        <?php echo _t('Use your {budget} virtual budget, pick 15 players and buy 1 share of each.',array(
            '{budget}'=> _app()->getLocale()->getCurrencySymbol('EUR').Gameconfiguration::model()->get('manager_initial_budget')
        )); ?>
    </div>
    <a href="<?php echo Utils::imageUrl('screenshots/large/screenshot1.png'); ?>" rel="prettyPhoto[pp_gal]"><img rel="prettyPhoto" src="<?php echo Utils::imageUrl('screenshots/screenshot1.png'); ?>" /></a>
    <a href="<?php echo Utils::imageUrl('screenshots/large/screenshot2.png'); ?>" rel="prettyPhoto[pp_gal]"><img class="right" src="<?php echo Utils::imageUrl('screenshots/screenshot2.png'); ?>" /></a>
</div>
<div class="screenshot-row">
    <div class="profile-usermenu">
        <h3><?php echo _t('Trade stocks of players in the stock market'); ?></h3>
        <?php echo _t('Buy and sell stocks of players and increase your budget.'); ?>
    </div>
    <a href="<?php echo Utils::imageUrl('screenshots/large/screenshot3.png'); ?>" rel="prettyPhoto[pp_gal]"><img src="<?php echo Utils::imageUrl('screenshots/screenshot3.png'); ?>" /></a>
    <a href="<?php echo Utils::imageUrl('screenshots/large/screenshot4.png'); ?>" rel="prettyPhoto[pp_gal]"><img class="right" src="<?php echo Utils::imageUrl('screenshots/screenshot4.png'); ?>" /></a>
</div>
<div class="screenshot-row">
    <div class="profile-usermenu">
        <h3><?php echo _t('Adjust your team'); ?></h3>
        <?php echo _t('Select your team\'s formation, starting players and captain.'); ?>
    </div>
    <a href="<?php echo Utils::imageUrl('screenshots/large/screenshot5.png'); ?>" rel="prettyPhoto[pp_gal]"><img src="<?php echo Utils::imageUrl('screenshots/screenshot5.png'); ?>" /></a>
    <a href="<?php echo Utils::imageUrl('screenshots/large/screenshot6.png'); ?>" rel="prettyPhoto[pp_gal]"><img class="right" src="<?php echo Utils::imageUrl('screenshots/screenshot6.png'); ?>" /></a>
</div>
<div class="screenshot-row">
    <div class="profile-usermenu">
        <h3><?php echo _t('Earn points'); ?></h3>
        <?php echo _t('Earn points for each share according to the performance of your players in real matches.  '); ?>
    </div>
    <a href="<?php echo Utils::imageUrl('screenshots/large/screenshot7.png'); ?>" rel="prettyPhoto[pp_gal]"><img src="<?php echo Utils::imageUrl('screenshots/screenshot7.png'); ?>" /></a>
    <a href="<?php echo Utils::imageUrl('screenshots/large/screenshot8.png'); ?>" rel="prettyPhoto[pp_gal]"><img class="right" src="<?php echo Utils::imageUrl('screenshots/screenshot8.png'); ?>" /></a>
</div>
<?php jqPrettyPhoto::addPretty('.screenshot-row a',jqPrettyPhoto::PRETTY_GALLERY,jqPrettyPhoto::THEME_DARK_SQUARE, $options); ?>