<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
<!--        <meta property="og:title" content="Onsports Fantasy League" />
        <meta property="og:description" content="Παίξε στο καλύτερο ποδοσφαιρικό fantasy league που διοργανώθηκε ποτέ στην Ελλάδα και κέρδισε μοναδικά δώρα." />
        <meta property="og:image" content="http://fantasy.onsports.gr/images/slideshow/slideshow3.jpg" />
        <meta property="og:url" content="http://fantasy.onsports.gr/" />
        <meta itemprop="name" content="Onsports Fantasy League" />
        <meta itemprop="description" content="Παίξε στο καλύτερο ποδοσφαιρικό fantasy league που διοργανώθηκε ποτέ στην Ελλάδα και κέρδισε μοναδικά δώρα." />
        <meta itemprop="image" content="http://fantasy.onsports.gr/images/slideshow/slideshow3.jpg" />
        <meta itemprop="url" content="http://fantasy.onsports.gr/" />-->
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <script type="text/javascript">
            /*<![CDATA[*/
            var MSL = {
                baseUrl : '<?php echo _bu(); ?>',
                themeUrl : '<?php echo _tbu(); ?>',
                controllerUrl : '<?php echo _url(_controller_id()); ?>',
                actionUrl : '<?php echo _url(_app()->controller->route); ?>',
                adminUrl : '<?php echo _bu('/admin/'); ?>',
                controllerId : '<?php echo _controller_id(); ?>',
                actionId : '<?php echo _action_id(); ?>',
                modules : {},
                config : {
                    commissionPerc : <?php echo Gameconfiguration::model()->getCommissionFactor(); ?>
                }
            }
            /*]]>*/
        </script>
<!--        <script type="text/javascript">
//            _gaq.push(['_setCustomVar', 1, 'Subdomain', 'FantasyLeague', 3]);
//            _gaq.push(['_trackPageview','/by/subdomain/FantasyLeague']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
            /*]]>*/
        </script>
        <script type="text/javascript" src="http://partner.googleadservices.com/gampad/google_service.js "></script>
        <script type="text/javascript">
            GS_googleAddAdSenseService("ca-pub-5813820630585387");
            GS_googleEnableAllServices();
        </script>
        <script type="text/javascript">
            GA_googleAddSlot("ca-pub-5813820630585387", "MsLeague_728x90_Header");
            GA_googleAddSlot("ca-pub-5813820630585387", "MsLeague_200x150_Ros_1");
            GA_googleAddSlot("ca-pub-5813820630585387", "MsLeague_200x150_Ros_2");
            GA_googleAddSlot("ca-pub-5813820630585387", "MsLeague_200x150_Ros_3");
            GA_googleAddSlot("ca-pub-5813820630585387", "MsLeague_200x150_Ros_4");
            GA_googleAddSlot("ca-pub-5813820630585387", "MsLeague_234x60_Gipedo_1");
            GA_googleAddSlot("ca-pub-5813820630585387", "MsLeague_234x60_Gipedo_2");
        </script>
        <script type="text/javascript">
            GA_googleFetchAds();
        </script>
        <script type="text/javascript">
            window.___gcfg = {lang: 'el'};

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
            /*]]>*/
        </script>
        <script type="text/javascript" src="http://partner.googleadservices.com/gampad/google_service.js "></script>
        <script type="text/javascript">
            GS_googleAddAdSenseService("ca-pub-5813820630585387");
            GS_googleEnableAllServices();
        </script>
        <script type="text/javascript">
            GA_googleAddSlot("ca-pub-5813820630585387", "MsLeague_728x90_Header");
            GA_googleAddSlot("ca-pub-5813820630585387", "MsLeague_200x150_Ros_1");
            GA_googleAddSlot("ca-pub-5813820630585387", "MsLeague_234x60_Gipedo_1");
            GA_googleAddSlot("ca-pub-5813820630585387", "MsLeague_234x60_Gipedo_2");
        </script>
        <script type="text/javascript">
            GA_googleFetchAds();
        </script>
        <script type="text/javascript">
            window.___gcfg = {lang: 'el'};

            (function() {
                var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                po.src = 'https://apis.google.com/js/plusone.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
            })();
        </script>-->

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <link rel="shortcut icon" href="<?php echo _bu('favicon.ico'); ?>" />
        <link rel="icon" href="<?php echo _bu('favicon.gif'); ?>" type="image/gif" />

        <link rel="stylesheet" type="text/css" href="<?php echo _tbu('css/reset.css'); ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo _tbu('css/main.css'); ?>" />

        <?php
        $cssRules = CmsTheme::model()->findAll();
        $cssContent = '';
        foreach ($cssRules as $r)
            $cssContent .= $r->display() . "\n";
        ?>
        <link rel="stylesheet" type="text/css" href="<?php echo _tbu('css/manageable.css.php') . '?content=' . urlencode($cssContent); ?>" />
        <?php
        unset($cssRules);
        unset($cssContent);
        ?>
    </head>
    <body>
        <?php $this->widget('DebugWindow'); ?>
        <div id="container">
            <div id="header">
                <div id="logo">
                    <?php echo _l('', _url('site/index'), array('class' => 'left')); ?>
                    <div class="right">
                        <div class="ad">
                            <?php if (!Debug::isDebug()) : ?>
<!--                                <script type='text/javascript'>GA_googleFillSlot('MsLeague_728x90_Header');</script>-->
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div id="general_info"><?php $this->widget('TopNav'); ?></div>
                <div id="main_menu"><?php $this->widget('TopMenu'); ?></div>
            </div>
            <div id="leftcolumn">
                <?php $this->widget('SubMenu'); ?>
                <div id="content"><?php echo $content; ?></div>
            </div>
            <div id="rightcolumn">
                <?php Utils::triggerEvent('onLayoutRightColumnBegin', $this); ?>

                <?php if (_user()->isGuest) : ?>
                    <?php
                    if (_controller()->route !== 'user/login' && _controller()->route !== 'user/register')
                        $this->widget('LoginBox');
                    if (_controller()->route !== 'user/register') {
                        $this->widget('WidgetBox', array(
                            'place' => 'sidebar',
                            'class' => 'register',
                            'title' => _t('New user?'),
                            'view' => '_registerBox',
                        ));
                    }
                    ?>
                    <?php
//                    echo CHtml::link(CHtml::tag('div', array(), ''), 'http://www.onsports.gr/', array(
//                        'class' => 'back_onsports',
//                    ));
                    ?>

                <?php elseif (_manager()->hasRole(Role::TEMPORARY)) : ?>
                    <?php $this->widget('ManagerBox'); ?>
                    <?php
//                    echo CHtml::link(CHtml::tag('div', array(), ''), 'http://www.onsports.gr/', array(
//                        'class' => 'back_onsports',
//                    ));
                    ?>
                <?php else : ?>
                    <?php $this->widget('ManagerBox'); ?>
                    <?php
//                    echo CHtml::link(CHtml::tag('div', array(), ''), 'http://www.onsports.gr/', array(
//                        'class' => 'back_onsports',
//                    ));
                    ?>
                    <?php if (_manager()->isBanned()): ?>
                        <div class="invite inactive">
                            <?php echo CHtml::tag('div', array('title' => _t('You cannot invite anyone because your are banned')), _t('Invite friends')); ?>
                        </div>
                    <?php else : ?>
                        <?php
                        echo CHtml::link(CHtml::tag('div', array(), _t('Invite friends')), _url('managers/invite'), array(
                            'title' => _t('Click here to invite your friends to the game'),
                            'class' => 'invite',
                        ));
                        ?>
                    <?php endif; ?>
<!--                    <div class="sidebar-ad">
                        <script type='text/javascript'>GA_googleFillSlot('MsLeague_200x150_Ros_1');</script>
                    </div>
                    <div class="sidebar-ad">
                        <script type='text/javascript'>GA_googleFillSlot('MsLeague_200x150_Ros_2');</script>
                    </div>
                    <div class="sidebar-ad">
                        <script type='text/javascript'>GA_googleFillSlot('MsLeague_200x150_Ros_3');</script>
                    </div>
                    <div class="sidebar-ad">
                        <script type='text/javascript'>GA_googleFillSlot('MsLeague_200x150_Ros_4');</script>
                    </div>-->
                    <?php $this->widget('TipsBox'); ?>
                <?php endif; ?>

                <?php if (_user()->isGuest || _manager()->hasRole(Role::TEMPORARY)) : ?>
<!--                    <div class="sidebar-ad">
                        <script type='text/javascript'>GA_googleFillSlot('MsLeague_200x150_Ros_1');</script>
                    </div>
                    <div class="sidebar-ad">
                        <script type='text/javascript'>GA_googleFillSlot('MsLeague_200x150_Ros_2');</script>
                    </div>
                    <div class="sidebar-ad">
                        <script type='text/javascript'>GA_googleFillSlot('MsLeague_200x150_Ros_3');</script>
                    </div>
                    <div class="sidebar-ad">
                        <script type='text/javascript'>GA_googleFillSlot('MsLeague_200x150_Ros_4');</script>
                    </div>-->
                <?php endif; ?>

                <?php Utils::triggerEvent('onLayoutRightColumnEnd', $this); ?>
            </div>
            <div id="footer">
                <div class="left"><?php $this->widget('FooterLinks'); ?></div>
                <div class="right">&copy;2011 Funialand Ltd. - Developed by Funialand Ltd.</div>
            </div>
        </div>

    </body>
</html>
