<?php if (!$canInvite) : ?>
    <?php
    $this->widget('Notice', array(
        'message' => _t('You can send up to {invs} invitations today.', array(
            '{invs}' => Gameconfiguration::model()->get('max_invites_per_day')
        )),
        'type' => 'warning'
    ));
    ?>
<?php else : ?>
    <div class="profile-usermenu">
        <?php
        echo CHtml::tag('h3', array(), _t('Invite a friend'));
        echo _t('You can bring your friends in the game by sending them an email invitation.');
        ?>
    </div>

    <?php $this->widget('Notice', array('session' => 'invite')); ?>
    <?php $this->widget('Notice', array('session' => 'failedEmail')); ?>

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => '#main',
        'htmlOptions' => array(
            'onSubmit' => '$(this).find(":submit").attr("disabled", true);',
        )));
    ?>
    <p class="form-hint">
        <?php echo str_replace('*', CHtml::tag('span', array('class' => 'required'), '*'), _t('Asterisk (*) indicates required fields')); ?>
    </p>
    <div class="row noneditable paragraph">
        <label><?php echo _t('Default message'); ?>:</label>
        <div><?php echo StaticContent::model()->getText('InvitationMailDefault'); ?></div>
    </div>
    <div class="row separator"></div>

    <div class="row">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email'); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'message'); ?>
        <?php echo $form->textArea($model, 'message', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'message'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton(_t('Send invitation')); ?>
    </div>

    <?php $this->endWidget(); ?>
<?php endif; ?>