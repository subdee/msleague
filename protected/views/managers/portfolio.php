<?php $this->widget('InformationHBar'); ?>
<?php

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $portfolioPlayers->search(),
    'enablePagination' => false,
    'template' => '{items}',
    'htmlOptions' => array('class' => 'grid-view portfolio'),
    'columns' => array(
        array(
            'name' => 'player.basicPosition.abbreviation',
            'value' => '$data->player->basicPosition->getTranslatedAbbr()',
            'htmlOptions' => array('class' => 'first-column'),
        ),
        array(
            'type' => 'raw',
            'value' => '$data->player->small_jersey_html()',
        ),
        array(
            'type' => 'raw',
            'name' => 'player.shortname',
            'value' => 'CHtml::link($data->player->shortname,_url("players/view",array("plId"=>$data->player->id)),array("class"=>"tname")).PlayerListDisplay::sInjuredHtml($data->player).PlayerListDisplay::sSuspendedHtml($data->player)',
            'htmlOptions' => array('class' => 'player'),
        ),
        array(
            'name' => 'player.team.shortname5',
            'value' => '$data->player->team != null ? $data->player->team->shortname5 : "-"',
        ),
        array(
            'name' => 'shares',
            'htmlOptions' => array('class' => 'numeric'),
        ),
        array(
            'name' => 'player.current_value',
            'header' => _t('Sell Price'),
            'value' => '_currency($data->player->current_value)',
            'htmlOptions' => array('class' => 'numeric'),
        ),
        array(
            'type' => 'raw',
            'name' => 'chng',
            'value' => '$this->grid->owner->widget("ValueChangeInfo", array("change" => $data->chng), true)',
            'htmlOptions' => array('class' => 'numeric'),
        ),
        array(
            'name' => 'total_value',
            'value' => '_currency($data->total_value,true)',
            'htmlOptions' => array('class' => 'numeric'),
        ),
    ),
));
?>