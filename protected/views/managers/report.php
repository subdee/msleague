<div class="profile-usermenu">
    <?php
    echo CHtml::tag('h3', array(), $title);
    echo _t('Choose the reason why you are reporting {username}.', array(
        '{username}' => CHtml::link($model->manager->username, _url('profile/index', array(
                'id' => $_GET['id']))
        ))
    );
    ?>
</div>

<?php $this->widget('Notice', array('session' => 'report')); ?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'htmlOptions' => array(
        'onSubmit' => '$(this).find(":submit").attr("disabled", true);',
    ),
    ));
?>
<div class="row">
    <?php echo $form->labelEx($model, 'report_reason_id'); ?>
    <div class="radiolist">
        <?php echo $form->radioButtonList($model, 'report_reason_id', $reasons); ?>
    </div>
    <?php echo $form->error($model, 'report_reason_id'); ?>
</div>
<div class="row note">
    <?php echo _t('Please explain in more detail the reason for reporting this user'); ?>    
</div>
<div class="row">
    <?php
    echo $form->labelEx($model, 'comment');
    echo $form->textArea($model, 'comment', array('rows' => 6, 'cols' => 50));
    echo $form->error($model, 'comment');
    ?>
</div>
<div class="row buttons">
    <?php echo CHtml::submitButton(_t('Report')); ?>
</div>
<?php $this->endWidget(); ?>
