<div class="profile-usermenu">
    <?php
    echo CHtml::tag('h3', array(), _t('Watchlist'));
    echo _t('Add players to your watchlist in order to store them and track changes in their price or points. To add a player in your watchlist go to his page and click on "Add to watchlist".');
    ?>
    <span class="right watchlist-count"><?php echo _t('You have {players} players on your watchlist.',array('{players}' => count($players))); ?></span>
</div>
<div class="watchlist-notice"><?php $this->widget('Notice', array('session' => 'watchlistMsg')); ?> </div>
<?php
foreach ($players as $player) :
    ?>
    <div class="watchlist-player">
        <?php
        $this->widget('PlayerInfo', array(
            'player' => $player->player,
            'showExtraDetails' => true
        ));
        ?>
    </div>
<?php endforeach; ?>

<script type="text/javascript">
    $(document).ready(function(){
        $("[class^=remove-link]").click(function(){
            var url =$(this).attr("href");
            var id =$(this).attr("id");
            $.ajax({
                url: url,
                type: "GET",
                success: function(response) {
                    var item = $(".remove-link-"+id).parent().parent().parent();
                    item.fadeOut(200, function() { item.remove(); })
                    $(".notice").remove();
                    $(".watchlist-notice").html(response.notice);
                    $(".watchlist-count").html(response.count);
                }
            });
            return false;
        })
    });
</script>