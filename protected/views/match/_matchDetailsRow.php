<?php

/**
 * Inputs
 */
if (!isset($isFirstDone))
    $isFirstDone = false;
if (!isset($isSecondDone))
    $isSecondDone = false;

$this->renderPartial('_matchDetailsRowColumn', array(
    'data' => $home,
    'column' => 'first',
    'isDone' => $isFirstDone
));
$this->renderPartial('_matchDetailsRowColumn', array(
    'data' => $away,
    'column' => 'second',
    'isDone' => $isSecondDone
));
?>
