<?php

echo CHtml::tag('td', array('class' => "$column dummy"));

echo CHtml::openTag('td', array('class' => "$column minutes-played"));
if (!$isDone) :
    echo $data['data']['events']['Minutes Played'] . '\'';
endif;
echo CHtml::closeTag('td');

if (!$isDone)
    echo CHtml::tag('td', array('class' => "$column player-position"), $data['data']['details']['positionText']);
else
    echo CHtml::tag('td', array('class' => "$column player-position"), ' ');

echo CHtml::openTag('td', array('class' => "$column player-name"));
if (!$isDone) :
    echo CHtml::openTag('span');
    if ($data['data']['details']['isListed']) :
        echo CHtml::link($data['playerName'], _url('players/view', array('plId' => $data['data']['details']['id'])));
    else :
        echo CHtml::tag('a', array(), $data['playerName']);
    endif;

    echo CHtml::closeTag('span');
    unset($data['data']['events']['Minutes Played']);
    foreach ($data['data']['events'] as $event => $value) :
        for ($i = 0; $i < $value; $i++) :
            echo Event::image($event, 16);
        endfor;
    endforeach;
endif;
echo CHtml::closeTag('td');
?>