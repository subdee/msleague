<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => $id,
    'dataProvider' => $matches->search(),
    'emptyText' => _t('There are no matches for this matchday.'),
    'htmlOptions' => array('class' => 'grid-view clickable'),
    'template' => '{items}{pager}',
    'selectableRows' => 1,
    'selectionChanged' => 'function(id){ MSL.Goto("' . _url("match/match?id=") . '" + $.fn.yiiGridView.getSelection(id)); }',
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'columns' => array(
        array(
            'name' => 'date_played',
            'sortable' => false,
            'value' => 'Utils::date($data->date_played)',
            'htmlOptions' => array('class' => 'first-column'),
        ),
        array(
            'value' => '$data->teamHome0->shortname15',
            'htmlOptions' => array('class' => 'team-home'),
        ),
        array(
            'header' => _t('Match'),
            'value' => '$data->is_cancelled ? "'._t('canc.').'" : $data->score_home . " - " . $data->score_away',
        ),
        array(
            'value' => '$data->teamAway0->shortname15',
            'htmlOptions' => array('class' => 'team-away'),
        ),
        array(
            'type'=>'raw',
            'value' => 'Utils::triggerEvent("onMatchListViewColumn3", $this, array("data" => $data))',
            'visible' => _isModuleOn("forum"),
        ),
    ))
);
?>