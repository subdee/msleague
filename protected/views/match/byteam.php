
<div class="match-search-page">

    <div class="table-outer-header">
        <?php echo CHtml::form(_url('match/byteam'), 'get'); ?>
        <span><?php echo _t('Team'); ?>:</span>
        <?php
        echo CHtml::dropDownList('team', $team, CHtml::listData($teams, 'id', 'name'), array(
            'prompt' => _t('Choose a team'),
        ));
        echo CHtml::endForm();
        ?>
    </div>

    <div class="match-list">
        <?php $this->renderPartial('_viewGames', array('matches' => $matchesPerTeam, 'id' => 'matchlist-team')); ?>
    </div>

    <div class="timezone-info">
        <?php echo _t('All matches are displayed in {timezone} timezone.', array('{timezone}' => _getUserTime()->format('T'))); ?>
    </div>

</div>
<script type="text/javascript">
    $(document).ready(function()
    {
        $('#team').change(function(){
            $(this).parent().submit();
        });
        $('#tabs form').submit(function(){
            var form = $(this);
            $.get(
            form.attr('action'),
            form.serialize(),
            function(data){
                var contentId = '#matchlist-' + $('select', form).attr('id');
                $(contentId).html($(contentId, data).html());
            });
            return false;
        })
    });
</script>