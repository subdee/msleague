
<div class="match-search-page">
    <div class="table-outer-header">
        <?php if ($previousGame->dateplayed) : ?>
            <a href="<?php echo _url('match/index', array('date' => $previousGame->dateplayed)); ?>">
                <img src="<?php echo Utils::imageUrl('arrows/st_left.png'); ?>" title="<?php echo Utils::date($previousGame->dateplayed, array('time' => false)); ?>" />
            </a>
        <?php endif; ?>

        <div class="match-date"><?php echo Utils::date($date, array('time' => false)); ?></div>

        <?php if ($nextGame->dateplayed) : ?>
            <a href="<?php echo _url('match/index', array('date' => $nextGame->dateplayed)); ?>">
                <img src="<?php echo Utils::imageUrl('arrows/st_right.png'); ?>" title="<?php echo Utils::date($nextGame->dateplayed, array('time' => false)); ?>" />
            </a>
        <?php endif; ?>
    </div>
    <div class="match-list">
        <?php $this->renderPartial('_viewGames', array('matches' => $matchesPerMonth, 'id' => 'matchlist-month')); ?>
    </div>
    <div class="timezone-info">
        <?php echo _t('All matches are displayed in {timezone} timezone.', array('{timezone}' => _getUserTime()->format('T'))); ?>
    </div>
</div>