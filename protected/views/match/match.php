<div class="match-page">
    <div class="score-details">
        <table>
            <tr>
                <td class="home">
                    <?php echo $game->teamHome0->large_jersey_html(); ?>
                    <div><?php echo $game->teamHome0->shortname15; ?></div>
                </td>
                <td class="score">
                    <?php if ($game->score_home === null) : ?>
                        <?php echo $game->is_cancelled ? _t('canc.') : _t('vs'); ?>
                    <?php else : ?>
                        <?php echo $game->score_home . ' - ' . $game->score_away; ?>
                    <?php endif; ?>
                    <div><?php echo Utils::date($game->date_played); ?></div>
                </td>
                <td class="away">
                    <?php echo $game->teamAway0->large_jersey_html(); ?>
                    <div><?php echo $game->teamAway0->shortname15; ?></div>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="date"><?php echo CHtml::link(CHtml::image(Utils::imageUrl('onsports-live.png')), 'http://live.onsports.gr/livematch/livematch.php?nMatchID=' . $game->xml_id, array('target' => '_blank')); ?></td>
            </tr>
        </table>
    </div>
    
    <?php $this->widget('MatchVoting',array('game'=>$game)); ?>

    <div class="match-details">
        <table>
            <?php
            $isHomeMore = true;
            $arrayToIterate = &$events['home'];
            $otherArray = &$events['away'];
            if ($events['playerHomeCount'] < $events['playerAwayCount']) {
                $arrayToIterate = &$events['away'];
                $otherArray = &$events['home'];
                $isHomeMore = false;
            }

            $index = 0;
            $done = false;
            ?>
            <?php foreach ($arrayToIterate as $playerName => $data) : ?>
                <?php
                if (!$done) {
                    $otherPlayerName = key($otherArray);
                    $otherData = current($otherArray);
                }
                ?>
                <tr>
                    <?php if ($isHomeMore) : ?>
                        <?php
                        $this->renderPartial('_matchDetailsRow', array(
                            'home' => array('playerName' => $playerName, 'data' => $data),
                            'away' => array('playerName' => $otherPlayerName, 'data' => $otherData),
                            'isSecondDone' => $done,
                        ));
                        ?>
                    <?php else : ?>
                        <?php
                        $this->renderPartial('_matchDetailsRow', array(
                            'home' => array('playerName' => $otherPlayerName, 'data' => $otherData),
                            'away' => array('playerName' => $playerName, 'data' => $data),
                            'isFirstDone' => $done,
                        ));
                        ?>
                    <?php endif; ?>
                </tr>
                <?php
                $index++;
                $done = !next($otherArray);
                ?>
            <?php endforeach; ?>
        </table>
    </div>
    <?php Utils::triggerEvent('onPageLeagueMatchBottom', $this, array('game' => $game)); ?>
</div>