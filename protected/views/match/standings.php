<div class="box_c">
    <div class="boxheader"><span class="title_c"><?php echo _t('League Table'); ?></span></div>
    <div class="boxmain_c">
        <div class="boxtext_c">

            <div class="grid-view">
                <table cellpadding="3" cellspacing="1" class="items">
                    <tr class="gtitle sleague">
                        <th width="20">&nbsp;</td>
                        <th width="120"><a class="wchar" href="#"><?php echo _t('Team'); ?></a></th>
                        <th width="30"><a class="wchar" href="#"><?php echo _t('GP'); ?></a></th>
                        <th width="40"><a class="wchar" href="#"><?php echo _t('Pts'); ?></a></th>
                        <th colspan="4" width="150"><?php echo _t('Total'); ?></th>
                        <th colspan="4" width="150"><?php echo _t('Home'); ?></th>
                        <th colspan="4" width="150"><?php echo _t('Away'); ?></th>
                    </tr>
                    <tr class="gtitle sleaguew">
                        <th colspan="4">&nbsp;</th>
                        <th width="30"><?php echo _t('W'); ?></th>
                        <th width="30"><?php echo _t('D'); ?></th>
                        <th width="30"><?php echo _t('L'); ?></th>
                        <th width="50"><?php echo _t('Goals'); ?></th>

                        <th width="30"><?php echo _t('W'); ?></th>
                        <th width="30"><?php echo _t('D'); ?></th>
                        <th width="30"><?php echo _t('L'); ?></th>
                        <th width="50"><?php echo _t('Goals'); ?></th>

                        <th width="30"><?php echo _t('W'); ?></th>
                        <th width="30"><?php echo _t('D'); ?></th>
                        <th width="30"><?php echo _t('L'); ?></th>
                        <th width="50"><?php echo _t('Goals'); ?></th>
                    </tr>

                <?php
                if ($list != null) :
                    $i = 1;
                    foreach($list as $pos=>$d) :
                        $class = $i & 1 ? 'odd' : 'even';
                ?>
                    <tr class="dblue <?php echo $class; ?>">
                        <td align="center"><?php echo $d['index']; ?></td>
                        <td align="left"><?php echo $d['name']; ?></td>
                        <td align="center"><?php echo $d['props']['g']; ?></td>
                        <td align="center"><b><?php echo $d['props']['p']; ?></b></td>
                        <td align="center"><?php echo $d['props']['hw']+$d['props']['aw']; ?></td>
                        <td align="center"><?php echo $d['props']['hd']+$d['props']['ad']; ?></td>
                        <td align="center"><?php echo $d['props']['hl']+$d['props']['al']; ?></td>
                        <td align="center"><?php echo $d['props']['hgf']+$d['props']['agf']; ?> - <?php echo $d['props']['hga']+$d['props']['aga']; ?></td>

                        <td align="center"><?php echo $d['props']['hw']; ?></td>
                        <td align="center"><?php echo $d['props']['hd']; ?></td>
                        <td align="center"><?php echo $d['props']['hl']; ?></td>
                        <td align="center"><?php echo $d['props']['hgf']; ?> - <?php echo $d['props']['hga']; ?></td>

                        <td align="center"><?php echo $d['props']['aw']; ?></td>
                        <td align="center"><?php echo $d['props']['ad']; ?></td>
                        <td align="center"><?php echo $d['props']['al']; ?></td>
                        <td align="center"><?php echo $d['props']['agf']; ?> - <?php echo $d['props']['aga']; ?></td>
                    </tr>
                    <?php ++$i; ?>
                <?php endforeach; ?>
                <?php else : ?>
                    <tr><td colspan="16"><?php echo _t('Standings not available.'); ?></td></tr>
                <?php endif; ?>
                </table>
            </div>
            <p>
                *<?php echo _t('In case of tie, the teams are displayed in alphabetical order.'); ?>
            </p>
        </div>
    </div>
    <div class="boxfooter_c"><!-- empty--></div>
</div>