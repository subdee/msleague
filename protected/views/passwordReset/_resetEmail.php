<?php

echo _t('Click on the following link to set a new password.');

?>

<p><a href='<?php echo $activation_link; ?>'><?php echo $activation_link; ?></a></p>

<?php

echo _t('If you did not request to change your password, you can ignore this e-mail');

?>