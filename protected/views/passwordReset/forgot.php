<div class="profile-usermenu">
    <?php 
    echo CHtml::tag('h3', array(), _t('Forgot password?'));
    echo _t('Enter your email address and you will receive an email with instructions on how to reset your password.'); 
    ?>
</div>
<?php $this->widget('Notice', array('session' => 'passreset')); ?>
<?php $this->widget('Notice', array('session' => 'passdebug')); ?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'action' => _url('passwordReset/index'),
    'htmlOptions' => array(
        'onSubmit' => '$(this).find(":submit").attr("disabled", true);',
    ),
    'focus' => array($model, 'email')
    ));
?>

<div class="row">
    <?php echo $form->labelEx($model, 'email'); ?>
    <?php echo $form->textField($model, 'email'); ?>
    <?php echo $form->error($model, 'email'); ?>
</div>

<div class="row captcha">
    <?php $this->widget('CCaptcha'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'verifyCode'); ?>
    <?php echo $form->textField($model, 'verifyCode'); ?>
    <?php echo $form->error($model, 'verifyCode'); ?>
</div>

<div class="row buttons">
    <?php echo CHtml::submitButton(_t('Reset')); ?>
</div>
<?php $this->endWidget(); ?>