<div class="profile-usermenu">
    <?php
    echo CHtml::tag('h3', array(), _t('Forgot password?'));
    ?>
</div>
<?php $this->widget('Notice', array('session' => 'passreset')); ?>
<?php $this->widget('Notice', array('session' => 'passdebug')); ?>
<?php
$this->widget('Notice', array(
    'message' => _t('Check your email for instructions on how to reset your password!'),
    'type' => 'info',
));
?>