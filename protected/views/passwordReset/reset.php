<div class="profile-usermenu">
    <?php
    echo CHtml::tag('h3', array(), _t('Reset password'));
    echo _t('Enter your new password');
    ?>
</div>
<?php $this->widget('Notice', array('session' => 'passreset')); ?>
<?php $this->widget('Notice', array('session' => 'passdebug')); ?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'htmlOptions' => array(
        'onSubmit' => '$(this).find(":submit").attr("disabled", true);',
    ),
    'focus' => array($model, 'password')
    ));
?>

<div class="row">
    <?php echo $form->labelEx($model, 'password'); ?>
    <?php echo $form->passwordField($model, 'password'); ?>
    <div class="hidden tooltip">
        <?php
        echo _t('Your password must be at least {min} characters long. Make your password strong by adding numbers, symbols and capital letters.', array(
            '{min}' => 6,
        ));
        ?>
        <div class="neutral" id="pwdMeter"><?php echo _t('Very weak'); ?></div>
    </div>
    <?php echo $form->error($model, 'password'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model, 'password2'); ?>
    <?php echo $form->passwordField($model, 'password2'); ?>
    <div class="hidden tooltip">
        <?php
        echo _t('Repeat password exactly. {passFail}{passOk}', array(
            '{passFail}' => CHtml::tag('div', array('class' => 'pwdMatchFail hidden'), _t('Passwords do not match.')),
            '{passOk}' => CHtml::tag('div', array('class' => 'pwdMatchOk hidden'), _t('Passwords match.')),
        ));
        ?>
    </div>
    <?php echo $form->error($model, 'password2'); ?>
</div>
<div class="row buttons">
    <?php echo CHtml::submitButton(_t('Reset')); ?>
</div>
<?php $this->endWidget(); ?>
<script type="text/javascript">

    $(document).ready(function()
    {
        // select all desired input fields and attach tooltips to them
        $("form input[type='password']").tooltip({
            position: "center right",
            offset: [0, 10],
            effect: "fade"
        });

        $('#UserPasswordResetForm_password').pwdMeter({
            minLength: 6
        });
    
        $('#UserPasswordResetForm_password2').keyup(function(){
            if($('#UserPasswordResetForm_password').val() == '') {
                $('.pwdMatchFail').hide();
                $('.pwdMatchOk').hide();
            }
            else if($(this).val() != $('#UserPasswordResetForm_password').val()) {
                $('.pwdMatchFail').show();
                $('.pwdMatchOk').hide();
            } else {
                $('.pwdMatchFail').hide();
                $('.pwdMatchOk').show();
            }
        });
    });
</script>