<div class="profile-usermenu">
    <?php echo CHtml::tag('h3', array(), _t('Reset password')); ?>
</div>
<?php $this->widget('Notice', array('message' => _t('Your password was reset successfully.'), 'type' => 'success')); ?>