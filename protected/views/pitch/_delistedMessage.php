<?php

$show = false;
$delistedMsg = _t('The following players have been delisted from the stockmarket and will receive no points. Replace them by selling their shares.');
$delistedMsg .= '<br />';
foreach (_team()->managerTeamPlayers as $mtp) {
    if ($mtp->player->isDelisted()) {
        $show = true;
        $delistedMsg .= "<strong>&raquo; {$mtp->player->shortname}</strong>";
        $delistedMsg .= '<br />';
    }
}
if ($show) {
    $this->widget('Notice', array(
        'type' => 'info',
//        'closable' => true,
        'message' => $delistedMsg,
    ));
}
?>