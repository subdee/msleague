<div class="pitch-layout">
    <div class="pitch">
        <?php if ($formation == '442') : ?>
            <!-- 4-4-2 -->
            <table>
                <tr>
                    <td></td>
                    <td><?php echo $players['LD'][0]->view(); ?></td>
                    <td><?php echo $players['LM'][0]->view(); ?></td>
                    <td></td>
                </tr>
                <tr>
                    <td rowspan="2"><?php echo $players['GK'][0]->view(); ?></td>
                    <td><?php echo $players['CLD'][0]->view(); ?></td>
                    <td><?php echo $players['CLM'][0]->view(); ?></td>
                    <td><?php echo $players['LS'][0]->view(); ?></td>
                </tr>
                <tr>
                    <td><?php echo $players['CRD'][0]->view(); ?></td>
                    <td><?php echo $players['CRM'][0]->view(); ?></td>
                    <td><?php echo $players['RS'][0]->view(); ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td><?php echo $players['RD'][0]->view(); ?></td>
                    <td><?php echo $players['RM'][0]->view(); ?></td>
                    <td></td>
                </tr>
            </table>
        <?php elseif ($formation == '433') : ?>
            <!-- 4-3-3 -->
            <table>
                <tr>
                    <td></td>
                    <td><?php echo $players['LD'][0]->view() ?></td>
                    <td><?php echo $players['LM'][0]->view(); ?></td>
                    <td><?php echo $players['LS'][0]->view(); ?></td>
                </tr>
                <tr>
                    <td rowspan="2"><?php echo $players['GK'][0]->view(); ?></td>
                    <td><?php echo $players['CLD'][0]->view(); ?></td>
                    <td rowspan="2"><?php echo $players['CM'][0]->view(); ?></td>
                    <td rowspan="2"><?php echo $players['CS'][0]->view(); ?></td>
                </tr>
                <tr>
                    <td><?php echo $players['CRD'][0]->view(); ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td><?php echo $players['RD'][0]->view(); ?></td>
                    <td><?php echo $players['RM'][0]->view(); ?></td>
                    <td><?php echo $players['RS'][0]->view(); ?></td>
                </tr>
            </table>
        <?php else : ?>
            <!-- 5-3-2 -->
            <table class="f532">
                <tr>
                    <td></td>
                    <td><?php echo $players['LD'][0]->view(); ?></td>
                    <td rowspan="2"><?php echo $players['LM'][0]->view(); ?></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td><?php echo $players['CLD'][0]->view(); ?></td>
                    <td><?php echo $players['LS'][0]->view(); ?></td>
                </tr>
                <tr>
                    <td><?php echo $players['GK'][0]->view(); ?></td>
                    <td><?php echo $players['CD'][0]->view(); ?></td>
                    <td><?php echo $players['CM'][0]->view(); ?></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td><?php echo $players['CRD'][0]->view(); ?></td>
                    <td rowspan="2"><?php echo $players['RM'][0]->view(); ?></td>
                    <td><?php echo $players['RS'][0]->view(); ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td><?php echo $players['RD'][0]->view(); ?></td>
                    <td></td>
                </tr>
            </table>
        <?php endif; ?>
    </div>

    <?php if (isset($players['SG'][0]) || isset($players['SD'][0]) || isset($players['SM'][0]) || isset($players['SS'][0])) : ?>

        <div class="bench">
            <h4><?php echo _t('Bench'); ?></h4>
            <table>
                <?php for ($i = 0; $i < 5; $i++) : ?>
                    <tr>
                        <td>
                            <?php
                            if (isset($players['SG'][$i]))
                                echo $players['SG'][$i]->view();
                            ?>
                        </td>
                        <td><?php if (isset($players['SD'][$i]))
                        echo $players['SD'][$i]->view(); ?></td>
                        <td><?php if (isset($players['SM'][$i]))
                        echo $players['SM'][$i]->view(); ?></td>
                        <td><?php if (isset($players['SS'][$i]))
                        echo $players['SS'][$i]->view(); ?></td>
                    </tr>
                <?php endfor; ?>
            </table>
        </div>
    <?php endif; ?>
</div>