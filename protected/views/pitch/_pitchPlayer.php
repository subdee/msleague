<?php

echo CHtml::openTag('a', array(
    'id' => isset($id) ? $id : '',
    'class' => $class,
));

if ($isEmpty) {

    echo CHtml::image(Team::noteam_small_jersey_url(), '', array('class' => 'jersey'));
    echo CHtml::image(Utils::imageUrl('pitch/cross.png'), '', array('class' => 'leader'));
} else {

    echo CHtml::openTag('span', array('class' => 'jersey'));
    if ($player->team) {
        echo CHtml::image($player->team->small_jersey_url(), '', array('title' => $player->team->name, 'class' => 'jersey'));
    } else {
        echo CHtml::image(Team::noteam_small_jersey_url(), '', array('title' => _t('Player has left his team.'), 'class' => 'jersey'));
    }
    if (isset($isLeader) && $isLeader) {
        echo CHtml::image(Utils::imageUrl('pitch/star.png'), '', array('title' => _t('Captain receives double points.'), 'class' => 'leader'));
    }
    echo CHtml::closeTag('span');

    echo CHtml::openTag('span', array('class' => 'shares-value'));
    if ($player->is_injured || $player->is_suspended) {
        echo PlayerListDisplay::sInjuredHtml($player);
        echo PlayerListDisplay::sSuspendedHtml($player);
    }
    echo CHtml::openTag('span');
    if (isset($shares)) {
        echo $shares . ' ' . _t('sh.');
    } elseif (isset($plpoints)) {
        echo $plpoints . ' ' . _t('pts');
    } else {
        echo _currency($player->current_value);
    }
    echo CHtml::closeTag('span');
    echo CHtml::closeTag('span');

    echo CHtml::tag('span', array('class' => 'name'), $player->shortname);

    if (isset($pitchPosition)) {
        $redirectUrl = _url('createTeam/deleteTempPlayer', array('position' => $pitchPosition));
        echo CHtml::openTag('span', array('class' => 'clear-player'));
        echo CHtml::tag('span', array(
            'onclick' => '$(this).parent().parent().unbind("click"); MSL.Redirect("' . $redirectUrl . '")',
        ));
        echo CHtml::closeTag('span');
    } else {
        echo CHtml::hiddenField('player_id', $player->id);
    }
}
echo CHtml::closeTag('a');
?>
