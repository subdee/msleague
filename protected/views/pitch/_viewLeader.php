<ul>
    <?php foreach ($leaders as $leader) : ?>
        <li>
            <?php echo CHtml::link($leader['name'], _url('pitch/assignLeader', array('id' => $leader['id']))); ?>
        </li>
    <?php endforeach; ?>
</ul>