<?php $this->widget('InformationHBar'); ?>

<div class="pitch-page">

    <?php
    if ($isBanned) {
        $this->widget('Notice', array(
            'type' => 'warning',
            'message' => _t('You cannot edit your team because you are banned'),
        ));
    } else {
        $noticeMsg = null;
        $closable = true;
        if ($isOpen) {
            $noticeMsg = _t('You can adjust your team for the {link}next matches{/link} until {end}', array(
                '{end}' => CHtml::tag('strong', array(), Utils::date($nextMatch)),
                '{link}' => CHtml::openTag('a', array('href' => _url('match/index'))),
                '{/link}' => CHtml::closeTag('a')
                ));
        } elseif (!$manualClosed) {
            $noticeMsg = _t('Team adjustment is temporarily <strong>CLOSED</strong>.');
        } else {
            $noticeMsg = _t('Team adjustment period for the current matchday is <strong>CLOSED</strong>.');
            if ($nextMatch) {
                $openTime = new DateTime(date('Y-m-d H:i:s', strtotime('Tomorrow')), new DateTimeZone(_timezone()));
                $openTime = Utils::date(_app()->localtime->toLocalDateTime($openTime->format(DATE_ISO8601), 'Y-m-d H:i:s'));
                $noticeMsg .= _t('You can adjust your team again on {openTime}.', array(
                    '{openTime}' => CHtml::tag('strong', array(), $openTime),
                    ));
            }
            $closable = false;
        }
        $this->widget('Notice', array(
            'type' => 'info',
//            'closable' => $closable,
            'message' => $noticeMsg,
        ));
    }

    if ($isOpen && $manualClosed) {
        // Show which players are delisted regardless or user role and pitch state.
        $this->renderPartial('_delistedMessage');
    }
    $this->widget('Notice', array('session' => 'changeError'));
    ?>

    <div class="profile-usermenu">
        <ul class="meta">
            <li>
                <div class="name"><?php echo _team()->name; ?></div>
                <span class="right">(<?php echo _t('Created on ') . Utils::date(_team()->created_on, array('time' => false)); ?>)</span>
            </li>
        </ul>
    </div>

    <?php if ($isOpen) : ?>
        <div class="controllbar">
            <ul class="formation">
                <?php foreach ($formations as $f) : ?>
                    <?php $forma = $f->formation; ?>
                    <?php if (_team()->formation->formation == $forma) : ?>
                        <li class="selected">
                            <?php echo "{$forma[0]}-{$forma[1]}-{$forma[2]}"; ?>
                        </li>
                    <?php else : ?>
                        <li>
                            <?php echo CHtml::link("{$forma[0]}-{$forma[1]}-{$forma[2]}", _url('pitch/changeFormation', array('formation' => $forma))); ?>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
            <div class="leader">
                <?php echo CHtml::link(CHtml::tag('div', array(), _t('Captain')), '#'); ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if (!Debug::isDebug()) : ?>
        <div class="pitch-banners">
            <div class="left"><script type='text/javascript'>GA_googleFillSlot('MsLeague_234x60_Gipedo_1');</script></div>
            <div class="right"><script type='text/javascript'>GA_googleFillSlot('MsLeague_234x60_Gipedo_2');</script></div>
            <!--        <div class="left"></div>
                    <div class="right"></div>-->
        </div>
    <?php endif; ?>

    <?php _controller()->renderPartial('/pitch/_pitch', array('players' => $players, 'formation' => _team()->formation->formation)); ?>

</div>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'leaderList',
    'options' => array(
        'title' => _t('Select captain'),
        'resizable' => false,
        'draggable' => false,
        'autoOpen' => false,
        'width' => 'auto',
        'height' => 'auto',
        'minHeight' => '10',
        'modal' => false,
        'closeOnEscape' => true,
    ),
));
?>
<ul>
    <?php foreach ($players as $playerByPosition) : ?>
        <?php foreach ($playerByPosition as $player) : ?>
            <?php if (!$player->params['isLeader'] && !$player->params['isSubstitute']) : ?>
                <li><?php echo CHtml::link($player->player->shortname, _url('pitch/assignLeader', array('id' => $player->player->id))); ?></li>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endforeach; ?>
</ul>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>