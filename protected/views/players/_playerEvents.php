<?php

foreach ($events as $e) {
    echo CHtml::openTag('div'); {
        $label = $e->event_value;
        switch ($e->event->name) {
            case 'Minutes Played': $label .= '\'';
                break;
        }
        echo CHtml::tag('span', array(), $label);
        echo CHtml::tag('label', array(), Event::image($e->event->name, 16));
    }
    echo CHtml::closeTag('div');
}
?>
