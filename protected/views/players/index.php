<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $players->search(),
    'filter' => $players,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'columns' => array(
        array(
            'type' => 'raw',
            'header' => '',
           'value' => '!_user()->isGuest && Watchlist::model()->exists("manager_id = :id AND player_id = :pid", array(":id" => _manager()->id, ":pid" => $data->plId)) ? CHtml::image(Utils::imageUrl("iconset/flag.png")) : ""',
            'htmlOptions' => array('class' => 'position','title'=>_t('Watchlist')),
        ),
        array(
            'name' => 'pos',
            'value' => '$data->basicPosition->getTranslatedAbbr()',
            'filter' => CHtml::listData(BasicPosition::model()->findAll(), 'id', 'translatedAbbr'),
            'htmlOptions' => array('class' => 'position'),
        ),
        array(
            'type' => 'raw',
            'value' => '$data->small_jersey_html()',
        ),
        array(
            'type' => 'raw',
            'name' => 'shortname',
            'value' => 'CHtml::link($data->shortname,_url("players/view",array("plId"=>$data->plId))).PlayerListDisplay::sInjuredHtml($data).PlayerListDisplay::sSuspendedHtml($data)',
            'filter' => CHtml::textField('Player[shortname]', $name, array('maxlength' => 17)),
            'htmlOptions' => array('class' => 'player'),
        ),
        array(
            'name' => 'myTeam',
            'value' => '$data->team != null ? $data->team->shortname5 : "-"',
            'filter' => CHtml::listData(Team::model()->findAll(), 'id', 'shortname5'),
            'htmlOptions' => array('class' => 'team'),
        ),
        array(
            'name' => 'total_points',
            'filter' => false,
            'htmlOptions' => array('class' => 'player-points'),
        ),
        array(
            'name' => 'current_value',
            'value' => '_currency($data->current_value)',
            'filter' => false,
            'htmlOptions' => array('class' => 'player-value'),
        ),
        array(
            'type' => 'raw',
            'name' => 'chng',
            'value' => '$this->grid->owner->widget("ValueChangeInfo", array("change" => $data->chng), true)',
            'filter' => false,
            'htmlOptions' => array('class' => 'player-change'),
        ),
        array(
            'name' => 'bought',
            'header' => _t('Owned'),
            'filter' => false,
            'htmlOptions' => array('class' => 'player-baught-shares'),
        ),
        array(
            'name' => 'vmi_season',
            'value' => 'Utils::number($data->vmi_season,true,2)',
            'filter' => false,
            'htmlOptions' => array('class' => 'player-vmi'),
        ),
    ),
));
?>
<div class="legends">
    <table>
        <tr>
            <td><?php echo _t('<b>Change:</b> % share price change from last update'); ?></td>
        </tr>
        <tr>
            <td><?php echo _t('<b>Owned:</b> number of shares owned by all managers (this index is updated every 6 hours)'); ?></td>
        </tr>
        <tr>
            <td>
                <?php
                echo _t('<b>VMI (Value for Money Index):</b> total points per {symbol}100 share price', array(
                    '{symbol}' => _app()->locale->getCurrencySymbol('EUR')
                ));
                ?>
            </td>
        </tr>
    </table>
</div>