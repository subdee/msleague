<?php

$tooltipLabels = Player::model()->tooltipLabels();
$playerAttrLabels = Player::model()->attributeLabels();

function _getAttribLink($attr, $playerAttrLabels, $tooltipLabels) {
    return _l($playerAttrLabels[$attr] . CHtml::tag('span', array('class' => 'classic'), $tooltipLabels[$attr]), '#');
}

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $players->statSearch(),
    'filter' => $players,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'columns' => array(
        array(
            'type' => 'raw',
            'header' => '',
           'value' => '!_user()->isGuest && Watchlist::model()->exists("manager_id = :id AND player_id = :pid", array(":id" => _manager()->id, ":pid" => $data->plId)) ? CHtml::image(Utils::imageUrl("iconset/flag.png")) : ""',
            'htmlOptions' => array('class' => 'position','title'=>_t('Watchlist')),
        ),
        array(
            'name' => 'pos',
            'value' => '$data->basicPosition->getTranslatedAbbr()',
            'filter' => CHtml::listData(BasicPosition::model()->findAll(), 'id', 'translatedAbbr'),
            'htmlOptions' => array('class' => 'position'),
        ),
        array(
            'type' => 'raw',
            'value' => '$data->small_jersey_html()',
        ),
        array(
            'type' => 'raw',
            'name' => 'shortname',
            'value' => '$data->link("shortname", "plId") . PlayerListDisplay::sInjuredHtml($data) . PlayerListDisplay::sSuspendedHtml($data)',
            'filter' => CHtml::textField('Player[shortname]', $name, array('maxlength' => 17)),
            'htmlOptions' => array('class' => 'player'),
        ),
        array(
            'name' => 'myTeam',
            'value' => '$data->team != null ? $data->team->shortname5 : "-"',
            'filter' => CHtml::listData(Team::model()->findAll(), 'id', 'shortname5'),
        ),
        array(
            'name' => 'current_value',
            'value' => '_currency($data->current_value)',
            'filter' => false,
        ),
        array(
            'name' => 'apps',
            'header' => _getAttribLink('apps', $playerAttrLabels, $tooltipLabels),
            'value' => '$data->apps == 0 ? "-" : $data->apps',
            'filter' => false,
            'headerHtmlOptions' => array('class' => 'tooltip')
        ),
        array(
            'name' => 'low_apps',
            'header' => _getAttribLink('low_apps', $playerAttrLabels, $tooltipLabels),
            'value' => '$data->low_apps == 0 ? "-" : $data->low_apps',
            'filter' => false,
            'headerHtmlOptions' => array('class' => 'tooltip')
        ),
        array(
            'name' => 'gs',
            'header' => _getAttribLink('gs', $playerAttrLabels, $tooltipLabels),
            'value' => '$data->gs == 0 ? "-" : $data->gs',
            'filter' => false,
            'headerHtmlOptions' => array('class' => 'tooltip')
        ),
        array(
            'name' => 'ass',
            'header' => _getAttribLink('ass', $playerAttrLabels, $tooltipLabels),
            'value' => '$data->ass == 0 ? "-" : $data->ass',
            'filter' => false,
            'headerHtmlOptions' => array('class' => 'tooltip')
        ),
        array(
            'name' => 'ng',
            'header' => _getAttribLink('ng', $playerAttrLabels, $tooltipLabels),
            'value' => '$data->ng == 0 ? "-" : $data->ng',
            'filter' => false,
            'headerHtmlOptions' => array('class' => 'tooltip')
        ),
        array(
            'name' => 'ga',
            'header' => _getAttribLink('ga', $playerAttrLabels, $tooltipLabels),
            'value' => '$data->ga == 0 ? "-" : $data->ga',
            'filter' => false,
            'headerHtmlOptions' => array('class' => 'tooltip')
        ),
        array(
            'name' => 'cs',
            'header' => _getAttribLink('cs', $playerAttrLabels, $tooltipLabels),
            'value' => '$data->cs == 0 ? "-" : $data->cs',
            'filter' => false,
            'headerHtmlOptions' => array('class' => 'tooltip')
        ),
        array(
            'name' => 'ps',
            'header' => _getAttribLink('ps', $playerAttrLabels, $tooltipLabels),
            'value' => '$data->ps == 0 ? "-" : $data->ps',
            'filter' => false,
            'headerHtmlOptions' => array('class' => 'tooltip')
        ),
        array(
            'name' => 'rc',
            'header' => _getAttribLink('rc', $playerAttrLabels, $tooltipLabels),
            'value' => '$data->rc == 0 ? "-" : $data->rc',
            'filter' => false,
            'headerHtmlOptions' => array('class' => 'tooltip')
        ),
        array(
            'name' => 'og',
            'header' => _getAttribLink('og', $playerAttrLabels, $tooltipLabels),
            'value' => '$data->og == 0 ? "-" : $data->og',
            'filter' => false,
            'headerHtmlOptions' => array('class' => 'tooltip')
        ),
        array(
            'name' => 'mp',
            'header' => _getAttribLink('mp', $playerAttrLabels, $tooltipLabels),
            'value' => '$data->mp == 0 ? "-" : $data->mp',
            'filter' => false,
            'headerHtmlOptions' => array('class' => 'tooltip')
        ),
    ),
));
?>