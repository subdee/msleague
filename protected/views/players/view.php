<?php if (_manager()->hasRole(Role::TEMPORARY)) : ?>
    <?php
    $this->widget('Notice', array(
        'message' => _t('In order to have access to player profiles you need to {url}.', array(
            '{url}' => CHtml::link(_t('create your team'), _url('createTeam/index'))
        )),
        'type' => 'info',
    ));
    ?>
<?php else : ?>

    <div id="left-profile-column">
        <div id="chart" class="chart">
            <!--Enter stock value chart-->
            <?php
//            var_dump($st); die();
            $this->Widget('ext.highcharts.HighchartsWidget', array(
                'options' => array(
                    'credits' => array(
                        'enabled' => false
                    ),
                    'chart' => array(
                        'defaultSeriesType' => 'area',
                        'renderTo' => 'chart',
                    ),
                    'title' => array('text' => ''),
                    'xAxis' => array(
                        'categories' => $st['date'],
                        'tickInterval' => (int) (count($st['date']) / 3),
                    ),
                    'yAxis' => array(
                        'title' => array('text' => ''),
                        'min' => $loSt - ($loSt * 0.05),
                        'labels' => array(
                            'formatter' => 'js:function(){
                            return "€"+this.value;
                                }',
                        ),
                    ),
                    'plotOptions' => array(
                        'area' => array(
                            'marker' => array(
                                'enabled' => false,
                                'symbol' => 'circle',
                                'radius' => 2,
                                'states' => array(
                                    'hover' => array(
                                        'enabled' => true
                                    )
                                )
                            ),
                            'lineWidth' => 1.5,
                            'shadow' => false,
                            'fillOpacity' => '0.35',
                        )
                    ),
                    'tooltip' => array(
                        'formatter' => 'js:function(){
                            return "<strong>"+this.x+":</strong> "+this.y;
                           }'
                    ),
                    'legend' => array(
                        'enabled' => false,
                    ),
                    'series' => array(
                        array('name' => _t('Share price'), 'data' => $st['value'])
                    ),
                    'theme' => 'default',
                    'exporting' => array(
                        'enabled' => false
                    )
                )
            ));
            ?>
        </div>
        <div class="profile">
            <div class="information">
                <div>
                    <?php
                    echo CHtml::tag('div', array(), _t('Share Status') . ':');
                    echo $player->list_date != null && $player->delist_date == null ? _t('Listed') : _t('Delisted');
                    ?>
                </div>
                <div>
                    <?php
                    $label = $player->list_date != null && $player->delist_date == null ? _t('List Date') : _t('Delist Date');
                    echo CHtml::tag('div', array(), $label . ':');
                    echo $player->list_date != null && $player->delist_date == null ? Utils::date($player->list_date) : Utils::date($player->delist_date);
                    ?>
                </div>
                <div class="hidden"><div><?php echo _t('Total Shares'); ?>:</div><?php echo Utils::number($total_shares); ?></div>
                <div class="hidden"><div><?php echo _t('Total Price'); ?>:</div><?php echo _currency($total_shares * $player->current_value, true, false); ?></div>
                <div class="hidden"><div><?php echo _t('HIGH (highest share price)'); ?>:</div><?php echo _currency($hiSt); ?></div>
                <div class="hidden"><div><?php echo _t('LOW (lowest share price)'); ?>:</div><?php echo _currency($loSt); ?></div>
                <div class="hidden"><div><?php echo _t('Value for Money'); ?>:</div><?php echo Utils::number($player->vmi_season); ?></div>
            </div>
            <div class="infolink more"><?php echo CHtml::link(_t('More...'), '#'); ?></div>
            <div class="infolink close hidden"><?php echo CHtml::link(_t('Close...'), '#'); ?></div>
        </div>
    </div>

    <div id="right-profile-column">
        <div class="profile-usermenu player-view">
            <div class="meta">
                <ul>
                    <li class="name">
                        <?php echo $player->name; ?>
                    </li>
                </ul>
            </div>
            <div class="status">
                <?php echo PlayerListDisplay::sInjuredHtml($player); ?>
                <?php echo PlayerListDisplay::sSuspendedHtml($player); ?>
            </div>
            <div class="watchlist-button">
                <?php if (Watchlist::model()->exists('manager_id = :id and player_id = :pid', array(':id' => _manager()->id, ':pid' => $player->id))) : ?>
                    <?php if (_manager()->isBanned()) : ?>
                        <?php echo CHtml::tag('div', array('class' => 'no-watchlist', 'title' => _t('You cannot add players to your watchlist because you are banned.')), CHtml::image(Utils::imageUrl('iconset/grayadd.png')) . _t('Remove from watchlist')); ?>
                    <?php else : ?>
                        <?php echo CHtml::link(CHtml::tag('div', array('class' => 'remove-watchlist'), CHtml::image(Utils::imageUrl('iconset/remove.png')) . _t('Remove from watchlist')), _url('managers/removeWatchlist', array('id' => $player->id))); ?>
                    <?php endif; ?>
                <?php else : ?>
                    <?php if (_manager()->isBanned()) : ?>
                        <?php echo CHtml::tag('div', array('class' => 'no-watchlist', 'title' => _t('You cannot remove players to your watchlist because you are banned.')), CHtml::image(Utils::imageUrl('iconset/grayadd.png')) . _t('Add to watchlist')); ?>
                    <?php else : ?>
                        <?php if ($canAdd) : ?>
                            <?php echo CHtml::link(CHtml::tag('div', array('class' => 'add-watchlist'), CHtml::image(Utils::imageUrl('iconset/add.png')) . _t('Add to watchlist')), _url('managers/addWatchlist', array('id' => $player->id))); ?>
                        <?php else : ?>
                            <?php echo CHtml::tag('div', array('class' => 'no-watchlist', 'title' => _t('Your watchlist cannot have more than {count} players.', array('{count}' => Gameconfiguration::model()->get('max_watchlist_items')))), CHtml::image(Utils::imageUrl('iconset/grayadd.png')) . _t('Add to watchlist')); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
            <div class="rating">
                <?php
                $this->widget('CStarRating', array(
                    'name' => 'rating' . $player->id,
                    'value' => $player->average_rating,
                    'minRating' => 1,
                    'maxRating' => 5,
                    'resetValue' => 0,
                    'allowEmpty' => false,
                    'resetText' => _t('Reset vote'),
                    'callback' => 'function(){
                        $.ajax({
                            type: "GET",
                            url: "' . _url('players/rating') . '",
                            data: "id=' . $player->id . '&rating="+$(this).val(),
                                success : function(data){
                                        $("#rating' . $player->id . ' > input").rating("select",data.rating - 1,false);
                                        $(".rateCount' . $player->id . '").html(data.votes);
                                }
                        })
                    }'
                ));
                ?>
                <div class="rateCount<?php echo $player->id; ?>">(<?php echo $ratingCount . ' ' . _t('votes'); ?>)</div>
            </div>
        </div>
        <div class="player-profile-info game-info">
            <?php echo $player->large_jersey_html(); ?>
            <table>
                <tr>
                    <td class="first" ><?php echo _t('Position'); ?>:</td>
                    <td><?php echo mb_ucfirst($player->basicPosition->getTranslatedName()); ?></td>
                </tr>
                <tr>
                    <td class="first" ><?php echo _t('Team'); ?>:</td>
                    <td>
                        <?php
                        if ($player->team != null) {
                            echo $player->team->name;
                        } else {
                            echo '-';
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="first" ><?php echo _t('Sell Price'); ?>:</td>
                    <td>
                        <?php echo _currency($player->current_value); ?>&nbsp;&nbsp;
                        <?php
                        $this->widget('ValueChangeInfo', array(
                            'change' => $change,
                            'type' => 'big',
                        ));
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="first" ><?php echo _t('Points'); ?>:</td>
                    <td><?php echo $player->total_points; ?></td>
                </tr>
            </table>
        </div>
        <div class="player-profile-stats">
            <h3><?php echo _t('Statistics'); ?></h3>
            <div class="grid-view">
                <table class="items stats">
                    <tr>
                        <?php $playerAttrLabels = Player::model()->attributeLabels(); ?>
                        <?php foreach ($playerTotalStats as $key => $value) : ?>
                            <th><?php echo $playerAttrLabels[$key]; ?></th>
                        <?php endforeach; ?>
                    </tr>
                    <tr class="odd">
                        <?php foreach ($playerTotalStats as $value) : ?>
                            <td><?php echo $value; ?></td>
                        <?php endforeach; ?>
                    </tr>
                </table>
            </div>
        </div>

        <div class="player-profile-stats">
            <h3><?php echo _t('Last Matches'); ?></h3>
            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'dataProvider' => $lastMatches->searchPastMatchesByPlayerId($player->id),
                'emptyText' => _t('There are no matches.'),
                'htmlOptions' => array('class' => 'grid-view clickable'),
                'template' => ' {items}{pager}',
                'showTableOnEmpty' => false,
                'selectableRows' => 1,
                'selectionChanged' => 'function(id){ MSL.Goto("' . _url("match/match?id=") . '" + $.fn.yiiGridView.getSelection(id)); }',
                'columns' => array(
                    array(
                        'name' => 'date_played',
                        'sortable' => false,
                        'value' => 'Utils::date($data->date_played)',
                    ),
                    array(
                        'cssClassExpression' => '$data->is_home_team ? "ownteam" : ""',
                        'value' => '$data->teamHome0->shortname15',
                        'htmlOptions' => array('class' => 'tright'),
                    ),
                    array(
                        'header' => _t('Match'),
                        'value' => '$data->score_home . " - " . $data->score_away',
                    ),
                    array(
                        'cssClassExpression' => '$data->is_home_team ? "" : "ownteam"',
                        'value' => '$data->teamAway0->shortname15',
                        'htmlOptions' => array('class' => 'tleft'),
                    ),
                    array(
                        'type' => 'raw',
                        'value' =>
                           'CHtml::tag("div", array("class" => "info", "rel" => _url("players/events", array("player" => $data->player_id, "game" => $data->id))), CHtml::image(Utils::imageUrl("iconset/info.png"))) .
                            CHtml::tag("div", array("class" => "tooltip white player-events empty"), CHtml::image(Utils::imageUrl("ajaxloader/small-circle.gif")))'
                    ),
                ))
            );
            ?>
        </div>

        <div class="player-profile-stats">
            <h3><?php echo _t('Next Matches'); ?></h3>
            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'dataProvider' => $nextMatches->searchFutureMatchesByPlayerId($player->id),
                'emptyText' => _t('There are no matches.'),
                'htmlOptions' => array('class' => 'grid-view clickable'),
                'template' => ' {items}{pager}',
                'showTableOnEmpty' => false,
                'selectableRows' => 1,
                'selectionChanged' => 'function(id){ MSL.Goto( MSL.Url("match", "match", {id : $.fn.yiiGridView.getSelection(id)}) ); }',
                'columns' => array(
                    array(
                        'name' => 'date_played',
                        'value' => 'Utils::date($data->date_played)',
                    ),
                    array(
                        'cssClassExpression' => '$data->team_home == $data->player_team_id ? "ownteam" : ""',
                        'value' => '$data->teamHome0->name',
                        'htmlOptions' => array('class' => 'tright'),
                    ),
                    array(
                        'header' => _t('Match'),
                        'value' => '"-"',
                    ),
                    array(
                        'cssClassExpression' => '$data->team_away == $data->player_team_id ? "ownteam" : ""',
                        'value' => '$data->teamAway0->name',
                        'htmlOptions' => array('class' => 'tleft'),
                    ),
                ))
            );
            ?>
        </div>
    </div>
<?php endif; ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('.profile .infolink a').click(function(){
            $('.more').toggle();
            $('.close').toggle();
            $('.information .hidden').toggle('slow');
            return false;
        });

        $('.player-profile-stats .info').tooltip({
            onBeforeShow : function(){
                if(this.getTrigger().next('.empty').size()) {
                    $.ajax({
                        type : 'get',
                        context: this,
                        url : this.getTrigger().attr('rel'),
                        success : function(response) {
                            this.getTrigger().next().html(response);
                        }
                    });
                    this.getTrigger().next('.empty').removeClass('empty');
                }
            },
            position : ['center', 'left'],
            offset : [0, -5]
        });
    });
</script>