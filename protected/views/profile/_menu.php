<div class="profile-usermenu user-profile">
    <ul class="meta">
        <li>
            <div class="name"><?php echo $manager->username; ?></div>
            <?php if (Manager::model()->isBanned($manager->id)) : ?>
                <span class="banned">
                    <?php
                    if ($manager->id == _manager()->id) {
                        echo _t('banned until {date}', array('{date}' => $manager->ban->unbanTime()));
                    } else {
                        echo _t('banned');
                    }
                    ?>
                </span>
<?php endif; ?>
        </li>
        <li>
            <label><?php echo _t('Team'); ?>:</label>
            <span class="team"><?php echo $manager->managerTeam->name; ?></span>
            <span class="date">(<?php echo _t('Created on {date}', array('{date}' => Utils::date($manager->managerTeam->created_on, array('time' => false)))); ?>)</span>
        </li>
    </ul>

    <?php if ($manager->id != _manager()->id && $manager->hasRole(Role::MANAGER)) : ?>
        <?php
        $menu = array(
            array(_url('profile/index', array('id' => $manager->id)), Utils::imageUrl('managerBox/menu_profile.png'), _t('Profile'), 'index' == _action_id() ? 'first selected' : 'first'),
            array(_url('profile/points', array('id' => $manager->id)), Utils::imageUrl('managerBox/menu_points.png'), _t('Points'), 'points' == _action_id() ? 'selected' : ''),
            array(_url('profile/players', array('id' => $manager->id)), Utils::imageUrl('managerBox/menu_pitch.png'), _t('Team'), 'players' == _action_id() ? 'last selected' : 'last'),
        );
        ?>
        <div class="menu">
            <?php
            foreach ($menu as $m)
                echo CHtml::link(CHtml::image($m[1], '', array('title' => $m[2])), $m[0], array('class' => $m[3]));
            ?>
        </div>
<?php endif; ?>
</div>
