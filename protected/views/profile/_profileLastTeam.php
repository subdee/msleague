<ul class="lastteam <?php echo $starting ? 'startingteam' : 'benchteam'; ?> ">
    <?php foreach( $lastTeam as $mtp ) : ?>
    <?php if(($starting && $mtp->isSubstitute()) || (!$starting && !$mtp->isSubstitute()) ) continue; ?>

    <li>
        <?php if($mtp->player->team) : ?>
        <img src="<?php echo $mtp->player->team->small_jersey_url(); ?>" title="<?php echo $mtp->player->team->name; ?>" />
        <?php else : ?>
        <img src="<?php echo Team::noteam_small_jersey_url(); ?>" title="<?php echo _t('Player has left his team.'); ?>" />
        <?php endif; ?>
        <ul>
            <li><a href="<?php echo _url('players/view', array('plId'=>$mtp->player->id)); ?>"><?php echo $mtp->player->shortname; ?></a></li>
            <li><?php echo _t('{n} share(s)', array($mtp->shares)); ?></li>
        </ul>
    </li>
    <?php endforeach; ?>
</ul>