<div id="left-profile-column">
    <?php
    $this->renderPartial('_profilePhoto', array(
        'photo' => $profilePhoto,
        'canEditProfile' => $canEditProfile,
        'showApprovalNotice' => $manager->id == _manager()->id && _manager()->profile->photo && !_manager()->profile->photo_approved,
    ));

    $reportManager = '';
    if (_user()->id != $manager->id) {
        if (_manager()->isBanned()) {
            $reportManager = CHtml::tag('li', array(), CHtml::tag('span', array(
                        'class' => 'mimic-tooltip',
                        'title' => _t('You can\'t report a user because you are banned.')
                        ), _t('Report user')
                    ));
        } else {
            $reportManager = CHtml::tag('li', array(), Report::reportLink(_t('Report user'), $manager->id, 'profile', 'profile', array()));
        }
    }
    ?>
    <div class="profile">
        <div class="links top">
            <ul>
                <?php Utils::triggerEvent('onProfileLeftColumnLinksTop', $this, array('manager' => $manager)); ?>
            </ul>
        </div>
        <?php $this->renderPartial('_profileMeta', array('manager' => $manager, 'canEditProfile' => $canEditProfile)); ?>
        <div class="links bottom">
            <ul>
                <?php echo $reportManager; ?>
                <?php
                if (_manager()->id != $manager->id) {
                    if (_manager()->isBanned()) {
                        if (_manager()->hasBlockedManager($manager->id)) {
                            echo CHtml::tag('li', array(), CHtml::tag('span', array(
                                    'class' => 'mimic-tooltip',
                                    'title' => _t('You can\'t unblock a user because you are banned')
                                    ), _t('Unblock user')
                                ));
                        } else {
                            echo CHtml::tag('li', array(), CHtml::tag('span', array(
                                    'class' => 'mimic-tooltip',
                                    'title' => _t('You can\'t block a user because you are banned')
                                    ), _t('Block user')
                                ));
                        }
                    } else {
                        if (_manager()->hasBlockedManager($manager->id)) {
                            echo CHtml::tag('li', array(), _l(_t('Unblock user'), _url('managers/unblock', array('id' => $manager->id))));
                        } else {
                            echo CHtml::tag('li', array(), _l(_t('Block user'), _url('managers/block', array('id' => $manager->id))));
                        }
                    }
                }
                ?>
                <?php Utils::triggerEvent('onProfileLeftColumnLinksBottom', $this, array('manager' => $manager)); ?>
            </ul>
        </div>
    </div>
</div>
