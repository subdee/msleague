<div class="information">

    <?php
    if ($canEditProfile) {

        // Manager's gender attribute
        echo CHtml::openTag('div');
        echo CHtml::tag('div', array(), _t('Gender:'));
        echo CHtml::tag('div', array(
            'id' => 'gender',
            'class' => 'editable',
            ), $manager->profile->genderText()
        );
        echo CHtml::closeTag('div');

        // Manager's age attribute
        if (_manager()->profile->birthdate) {
            $age = $manager->getAge();
            $dateFormatted = _app()->dateFormatter->format('dd/MM/y', _manager()->profile->birthdate);
            $birthdate = "$age ($dateFormatted)";
        } else {
            $birthdate = CHtml::tag('span', array('class' => 'unedited'), _t('[Edit]'));
        }

        echo CHtml::openTag('div');
        echo CHtml::tag('div', array(), _t('Age:'));
        echo CHtml::tag('div', array(
            'id' => 'birthdate',
            'class' => 'editable',
            ), $birthdate
        );

        $minDate = date('Y-m-d', mktime(0, 0, 0, 1, 1, 1920));
        $maxDate = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d"), date("Y") - 18));
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'id' => 'dob_datepicker',
            'name' => 'birthdate',
            'value' => _manager()->profile->birthdate,
            'language' => 'el',
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth' => true,
                'changeYear' => true,
                'minDate' => $minDate,
                'maxDate' => $maxDate,
                'onSelect' => 'js:onDatepickerSelect',
            ),
        ));

        echo CHtml::closeTag('div');

        // Manager's location attribute.
        echo CHtml::openTag('div');
        echo CHtml::tag('div', array(), _t('Location:'));
        echo CHtml::tag('div', array(
            'id' => 'location',
            'class' => 'editable',
            ), $manager->profile->location
        );
        echo CHtml::closeTag('div');

        // Manager's country attribute
        echo CHtml::openTag('div');
        echo CHtml::tag('div', array(), _t('Country:'));
        echo CHtml::tag('div', array(
            'id' => 'country_id',
            'class' => 'editable',
            ), $manager->profile->country->country
        );
        echo CHtml::closeTag('div');
    } else {

        $cannotEditReason = '';
        if (_manager()->id == $manager->id) {
            $cannotEditReason = _t('You can\'t edit your profile because you are banned.');
        }

        // Manager's gender attribute
        echo CHtml::openTag('div');
        echo CHtml::tag('div', array(), _t('Gender:'));
        echo CHtml::tag('div', array('title' => $cannotEditReason), $manager->profile->genderText());
        echo CHtml::closeTag('div');

        // Manager's age attribute
        $age = $manager->getAge();
        if ($age) {
            echo CHtml::openTag('div');
            echo CHtml::tag('div', array(), _t('Age:'));
            echo CHtml::tag('div', array('title' => $cannotEditReason), $age);
            echo CHtml::closeTag('div');
        }

        // Manager's location attribute.
        if ($manager->profile->location) {
            echo CHtml::openTag('div');
            echo CHtml::tag('div', array(), _t('Location:'));
            echo CHtml::openTag('div', array('title' => $cannotEditReason));
            $this->beginWidget('CHtmlPurifier');
            echo $manager->profile->location;
            $this->endWidget();
            echo CHtml::closeTag('div');
            echo CHtml::closeTag('div');
        }

        // Manager's country attribute
        echo CHtml::openTag('div');
        echo CHtml::tag('div', array(), _t('Country:'));
        echo CHtml::tag('div', array('title' => $cannotEditReason), $manager->profile->country->country);
        echo CHtml::closeTag('div');
    }
    ?>

</div>
<?php if ($canEditProfile) : ?>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#gender').editable('updateProfile', {
                data   : <?php echo CJavaScript::jsonEncode(Profile::model()->genderArray()); ?>,
                cssclass: 'inline-edit',
                indicator : '<?php echo CHtml::image(Utils::imageUrl('ajaxloader/small-circle.gif')); ?>',
                type : 'select',
                submit  : 'OK',
                //onblur : 'ignore',
                autowidth: false,
                tooltip : '<?php echo _t('Click to edit'); ?>'
            });

            $('#location').editable('updateProfile', {
                cssclass: 'inline-edit',
                indicator : '<?php echo CHtml::image(Utils::imageUrl('ajaxloader/small-circle.gif')); ?>',
                placeholder: '<?php echo CHtml::tag('span', array('class' => 'unedited'), _t('[Edit]')); ?>',
                submit  : 'OK',
                //onblur : 'ignore',
                width: 110,
                autowidth: false,
                maxlength: 45,
                tooltip : '<?php echo _t('Click to edit'); ?>'
            });

            $('#country_id').editable('updateProfile', {
                loadurl : 'countryList',
                cssclass: 'inline-edit',
                indicator : '<?php echo CHtml::image(Utils::imageUrl('ajaxloader/small-circle.gif')); ?>',
                type : 'select',
                submit  : 'OK',
                //onblur : 'ignore',
                autowidth: false,
                tooltip : '<?php echo _t('Click to edit'); ?>'
            });

            $('#birthdate').click(function(){
                $('#dob_datepicker').datepicker('show');
            });
        });

        function onDatepickerSelect(dateText, inst) {
    <?php
    echo CHtml::ajax(array(
        'url' => array('profile/updateProfile'),
        'data' => "js:{id : 'birthdate', value : $('#dob_datepicker').val()}",
        'type' => 'post',
        'update' => '#birthdate',
    ));
    ?>

        }
    </script>
<?php endif; ?>