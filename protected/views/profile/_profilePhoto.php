<?php

if ($canEditProfile) {

    echo CHtml::openTag('div', array('class' => 'profilethumb' . (_manager()->profile->photo ? ' active' : '')));

    // Remove profile photo link
    echo CHtml::ajaxLink(CHtml::image(Utils::imageUrl('pitch/new_player_close.png')), _url('profile/removePhoto'), array(
        'success' => 'js:onPhotoRemove',
        ), array(
        'class' => 'remove',
        'title' => _t('Remove profile picture')
    ));

    // Upload photo widget
    $this->widget('ext.EAjaxUpload.EAjaxUpload', array(
        'id' => 'uploadFile',
        'config' => array(
            'action' => _url('profile/uploadPhoto'),
            'allowedExtensions' => array('jpg', 'jpeg', 'gif', 'png'), //array("jpg","jpeg","gif","exe","mov" and etc...
            'sizeLimit' => 102400, // maximum file size in bytes
            'minSizeLimit' => 10, // minimum file size in bytes
            'onSubmit' => 'js:onPhotoUpload',
            'onComplete' => 'js:onPhotoUploadComplete',
            'onProgress' => 'js:onPhotoUploadProgress',
            'showMessage' => 'js:onPhotoUploadError',
            'messages' => array(
                'typeError' => _t("Invalid file type. Only {extensions} are allowed."),
                'sizeError' => _t("{file} is too large, maximum file size is {sizeLimit}."),
                'minSizeError' => _t("{file} is too small, minimum file size is {minSizeLimit}."),
                'emptyError' => _t("{file} is empty, please select a valid file."),
                'onLeave' => _t("The file is being uploaded, if you leave now the upload will be cancelled.")
            ),
            // Profile photo
            'buttonLabel' => CHtml::image($photo, '', array(
                'class' => 'photo',
            ))
            ,
            'hideList' => true,
//            'debug' => true,
        )
    ));

    echo CHtml::openTag('div', array('class' => 'tooltip black upload-photo-hint'));
    echo _t('Click to change your photo.');
    $message = _t('The image cannot exceed {limit}. (Supported types: jpeg, png, gif).', array('{limit}' => CHtml::tag('strong', array(), '100kb')));
    $message .= '<br />';
    $message .= _t('By uploading your profile photo you verify that you have the right of use and that you are not violating the {terms}.', array(
        '{terms}' => CHtml::link(_t('Terms of Use'), _url('site/termsOfUse'), array('target' => '_blank'))
        ));
    echo CHtml::tag('span', array('class' => 'hint'), $message);
    echo CHtml::closeTag('div'); // tooltip

    echo CHtml::closeTag('div'); // profile thumb

    // Progress bar
    echo CHtml::openTag('div', array('class' => 'progress'));
    $this->widget('zii.widgets.jui.CJuiProgressBar', array(
        'id' => 'photoUploadProgressBar',
        'value' => 0,
    ));
    echo CHtml::closeTag('div'); // Progress bar
} else {

    // Cannot edit profile
    echo CHtml::openTag('div', array('class' => 'profilethumb'));
    echo CHtml::image($photo, '', array('class' => 'photo'));
    echo CHtml::closeTag('div');
}

$this->widget('Notice', array(
    'id' => 'photoApproval',
    'ajax' => !$showApprovalNotice,
    'message' => _t('Your photo is pending approval.'),
    'type' => 'info',
));
?>

<?php if ($canEditProfile) : ?>
    <script type="text/javascript">

        $(document).ready(function(){
            $("#uploadFile").tooltip({
                position: "bottom center",
                offset: [-20, 0],
                effect: "fade"
            });
        });

        function onPhotoUpload(id, fileName) {
            $('.progress').show();
            $('#photoUploadProgressBar').progressbar({value: 0});
            $('#uploadError').hide();
            $('#emptyPhotoNotice').hide();
            $('#uploadFile').tooltip().hide();
        }

        function onPhotoUploadComplete(id, fileName, responseJSON) {
            if(responseJSON.success) {
                var d = new Date();
                $('#uploadFile .photo').attr('src', responseJSON.imageUrl + '?' + d.getTime()).show('highlight');
                $('#uploadError').hide();
                $('.profilethumb').addClass('active');
                $('#photoApproval').show();
            } else {
                $('#uploadError').notice({'message' : responseJSON.error, 'type' : 'error'});
            }
            $('.progress').hide();
        }

        function onPhotoUploadError(response) {
            $('#uploadError').show().notice({'message' : response, 'type' : 'error'});
            $('.progress').hide();
        }

        function onPhotoUploadProgress(id, fileName, loaded, total){
            $('#photoUploadProgressBar').progressbar({
                value: (loaded / total) * 100
            });
        }

        function onPhotoRemove(response) {
            if(response.success) {

                var d = new Date();
                $('#uploadFile .photo').attr('src', response.imageUrl + '?' + d.getTime());
                $('#uploadError').hide();
                $('.profilethumb.active').removeClass('active');
                $('#photoApproval').hide();

            } else if(response.error) {
                $('#uploadError').notice({'message' : response.error, 'type' : 'error'});
                if(!response.details.empty) {
                    console.log('Profile photo removal: ' + response.details);
                }
            }
        }

    </script>
<?php endif; ?>