<li>
    <span class="date"><?php echo Utils::date($transaction->date_completed); ?></span>
    <span>
        <?php if ($transaction->type_of_transaction === 'sell') : ?>
            <span class="sell"><?php echo _t('Sold'); ?></span>
        <?php else : ?>
            <span class="buy"><?php echo _t('Purchased'); ?></span>
        <?php endif; ?>
        <?php
        echo _t('{n} share(s) of {name} at a price per share {price}', array(
            '{n}' => $transaction->shares,
            '{name}' => '<a href="' . _url('players/view', array('plId' => $transaction->player->id)) . '">' . $transaction->player->shortname . '</a>',
            '{price}' => _currency($transaction->price_share),
        ));
        ?>
    </span>
</li>