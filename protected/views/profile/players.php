<?php if (_manager()->hasRole(Role::TEMPORARY)) : ?>
    <?php
    $this->widget('Notice', array(
        'message' => _t('In order to have access to user profiles you need to {url}.', array(
            '{url}' => '<a href="' . _url('createTeam/index') . '">' . _t('create your team') . '</a>'
        )),
        'type' => 'info',
    ));
    ?>
<?php else : ?>

    <?php $this->renderPartial('_menu', array('manager' => $manager)); ?>

    <div class="pitch-banners">
        <div class="left"><script type='text/javascript'>GA_googleFillSlot('MsLeague_234x60_Gipedo_1');</script></div>
        <div class="right"><script type='text/javascript'>GA_googleFillSlot('MsLeague_234x60_Gipedo_2');</script></div>
    </div>

    <?php
    _controller()->renderPartial('/pitch/_pitch', array(
        'players' => $players,
        'formation' => $manager->managerTeam->formation->formation));
    ?>

<?php endif; ?>
