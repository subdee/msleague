<?php if (_manager()->hasRole(Role::TEMPORARY)) : ?>
    <?php

    $this->widget('Notice', array(
        'message' => _t('In order to have access to user profiles you need to {url}.', array(
            '{url}' => '<a href="' . _url('createTeam/index') . '">' . _t('create your team') . '</a>'
        )),
        'type' => 'info',
    ));
    ?>
<?php else : ?>
    <?php $this->renderPartial('_menu', array('manager' => $manager)); ?>
    <?php $this->widget('Points'); ?>
<?php endif; ?>