<?php if (_manager()->hasRole(Role::TEMPORARY)) : ?>
    <?php
    $this->widget('Notice', array(
        'message' => _t('In order to have access to user profiles you need to {url}.', array(
            '{url}' => CHtml::link(_t('create your team'), _url('createTeam/index'))
        )),
        'type' => 'info',
    ));
    ?>
<?php else : ?>

    <?php
    $this->renderPartial('_profileLeftColumn', array(
        'manager' => $manager,
        'profilePhoto' => $profilePhoto,
        'canEditProfile' => $canEditProfile
    ));
    ?>

    <div id="right-profile-column">

        <?php
        $this->widget('Notice', array('session' => 'blockaction'));
        $this->widget('Notice', array('session' => 'report'));
        $this->widget('Notice', array('session' => 'profile'));
        $this->widget('Notice', array('ajax' => true, 'id' => 'uploadError'));
        $this->widget('Notice', array('ajax' => true, 'id' => 'newMessageSuccess'));
        $this->renderPartial('_menu', array('manager' => $manager));
        ?>

        <?php if ($canEditProfile) : ?>
            <div class="profile-catchprase">
                <?php
                echo CHtml::tag('div', array(
                    'id' => 'line',
                    'class' => 'editable',
                    ), $manager->profile->line
                );
                ?>
            </div>
        <?php elseif ($manager->profile->line) : ?>
            <div class="profile-catchprase">
                <?php echo $manager->profile->line; ?>
            </div>
        <?php endif; ?>

        <?php if ($manager->hasRole(Role::MANAGER)) : ?>
            <div class="profile-info-section game-info">
                <h3><?php echo _t('Game Info'); ?></h3>
                <table>
                    <tr>
                        <td class="first"><?php echo _t('Points'); ?>:</td>
                        <td ><?php echo Utils::number($manager->total_points); ?></td>
                    </tr>
                    <tr>
                        <td class="first"><?php echo _t('Rank'); ?>:</td>
                        <td>
                            <?php echo $manager->rank->rank; ?>
                            <span class="font-small"><?php echo _t('(top {pc}%)', array('{pc}' => $manager->getRankByPercent())); ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="first"><?php echo _t('Budget'); ?>:</td>
                        <td>
                            <?php echo _currency($manager->portfolio_share_value + $manager->portfolio_cash); ?>

                        </td>
                    </tr>
                    <tr>
                        <td class="first"><?php echo _t('Change'); ?>:</td>
                        <td>
                            <?php if (!empty($portfolioChange)) : ?>
                                <?php
                                $this->widget('ValueChangeInfo', array(
                                    'change' => $portfolioChange[2]['chng'],
                                    'type' => 'big',
                                ));
                                echo ' ';
                                echo _app()->dateFormatter->format('HH:mm', $portfolioChange[2]['date']);
                                ?>
                            <?php else : ?>
                                <?php echo _t('No updates yet!'); ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            </div>
        <?php endif; ?>

        <div class="profile-info-section fav-team">
            <h3><?php echo _t('Favorite Team'); ?></h3>
            <table>
                <tr>
                    <td class="first"><?php echo $manager->profile->team->large_jersey_html(); ?></td>
                    <td><?php echo $manager->profile->team->name; ?></td>
                </tr>
            </table>
        </div>

        <div class="profile-info-section transactions">
            <h3><?php echo _t('Latest Transactions'); ?></h3>
            <?php if ($trCount > 0) : ?>
                <ul>
                    <?php
                    for ($key = 0; $key < $trToShow; $key++) :
                        $this->renderPartial('_transactionRow', array('transaction' => $transactions[$key]));
                    endfor;
                    ?>
                </ul>
                <?php if ($trCount > $trToShow) : ?>
                    <ul class="hidden">
                        <?php
                        for ($key = $trToShow; $key < $trCount; $key++) :
                            $this->renderPartial('_transactionRow', array('transaction' => $transactions[$key]));
                        endfor;
                        ?>
                    </ul>
                    <div class="infolink more"><a href="#"><?php echo _t('More transactions'); ?></a></div>
                <?php endif; ?>
            <?php else : ?>
                <div class="empty"><?php echo _t('No transactions completed yet'); ?></div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('.infolink a').click(function(){
            $('.transactions ul:hidden').show('highlight');
            $(this).parent().hide();
            return false;
        });

<?php if ($canEditProfile) : ?>
            $('#line').editable('updateProfile', {
                cssclass: 'inline-edit',
                indicator : '<?php echo CHtml::image(Utils::imageUrl('ajaxloader/small-circle.gif')); ?>',
                submit  : 'OK',
                //            onblur : 'ignore',
                width: 400,
                autowidth: false,
                placeholder: '<?php echo _t('Click here to write something!'); ?>',
                maxlength: 45,
                tooltip : '<?php echo _t('Click to update your profile message'); ?>'

            });
<?php endif; ?>
    });
</script>
