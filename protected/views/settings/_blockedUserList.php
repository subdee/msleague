<div class="blocked-users-page">
    <?php if (empty($blocked)) : ?>
        <?php $this->widget('Notice', array('message' => _t('You have not blocked anyone yet.'))); ?>
    <?php else : ?>
        <table>
            <?php foreach ($blocked as $user): ?>
                <tr>
                    <td class="profile-photo">
                        <?php echo CHtml::image($user->blocked->profilePhoto()); ?>
                    </td>
                    <td>
                        <a href="<?php echo _url('profile/index', array('id' => $user->blocked->id)); ?>">
                            <?php echo $user->blocked->username; ?>
                        </a>
                    </td>
                    <td class="unblock">
                        (<?php echo CHtml::link(_t('Unblock'), _url(_controller()->route, array('id' => $user->blocked->id))); ?>)
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
</div>