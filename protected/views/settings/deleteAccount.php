<?php $this->widget('Notice', array('session' => 'error')); ?>

<?php
$formclass = '';
if (!isset($_POST['UserSettingsFormDelete'])) {
    $formclass = 'hidden';

    $this->widget('Notice', array(
        'message' => CHtml::link(_t('Are you sure you want to delete your account?'), '#', array('id' => 'click-for-delete')),
        'type' => 'question',
    ));
}
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
        'action' => _url('settings/deleteAccount'),
        'htmlOptions' => array(
            'class' => "deleteAccount $formclass",
        )
    ));
?>


<?php
$this->widget('Notice', array(
    'message' => _t('By deleting your account, your personal information and game history will be lost and cannot be recovered.'),
    'type' => 'info',
));
?>
<p><?php //echo _t('Please choose the reason why you decided to delete your account');  ?></p>

<div class="row">
    <?php echo $form->labelEx($model, 'reason'); ?>
    <div class="radiolist">
        <?php echo $form->radioButtonList($model, 'reason', $reasons); ?>
    </div>
    <?php echo $form->error($model, 'reason'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'details'); ?>
    <?php echo $form->textArea($model, 'details', array('cols' => 50, 'rows' => 10)); ?>
    <?php echo $form->error($model, 'details'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'password'); ?>
    <?php echo $form->passwordField($model, 'password'); ?>
    <?php echo $form->error($model, 'password'); ?>
</div>

<div class="row buttons">
    <?php echo CHtml::submitButton(_t('Delete account')); ?>
</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#click-for-delete').click(function(){
            $('.notice.question').next().show('blind');
            $('.notice.question').hide();
            return false;
        });
    });
</script>