<?php $this->widget('Notice', array('session' => 'result')); ?>
<?php $this->widget('Notice', array('session' => 'failedEmail')); ?>
<?php
$form = $this->beginWidget('CActiveForm', array(
        'action' => _url('settings/email')
    ));
?>
<div class="row noneditable">
    <label><?php echo _('Email'); ?>:</label>
    <div><?php echo _manager()->email; ?></div>
</div>
<div class="row separator"></div>
<div class="row">
    <?php echo $form->labelEx($model, 'email'); ?>
    <?php echo $form->textField($model, 'email'); ?>
    <?php echo $form->error($model, 'email'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'email2'); ?>
    <?php echo $form->textField($model, 'email2'); ?>
    <?php echo $form->error($model, 'email2'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'password'); ?>
    <?php echo $form->passwordField($model, 'password'); ?>
    <?php echo $form->error($model, 'password'); ?>
</div>

<div class="row buttons">
    <?php echo CHtml::submitButton(_t('Change email')); ?>
</div>
<?php echo $form->hiddenField($model, 'username', array('value' => _manager()->username)); ?>
<?php $this->endWidget(); ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#UserSettingsFormEmail_email2').InputAutocompleteOff();
    });
</script>