<?php $this->widget('Notice', array('session' => 'result')); ?>

<?php
$form = $this->beginWidget('CActiveForm', array(
        'action' => _url('settings/index'),
        'htmlOptions' => array(
            'onSubmit' => '$(this).find(":submit").attr("disabled", true);',
            'class' => 'preferences',
        ),
    ));
?>
<div class="row checkbox">
    <?php echo $form->labelEx($model, 'newsletter'); ?>
    <?php echo $form->checkBox($model, 'newsletter', array('checked' => $model->newsletter)); ?>
    <div class="hint"><?php echo _t('I want to receive newsletters from {gamename}', array('{gamename}' => _gameName())); ?></div>
</div>

<div class="row separator"></div>

<div class="row">
    <?php echo $form->labelEx($model, 'timezone_id'); ?>
    <?php echo $form->dropDownList($model, 'timezone_id', CHtml::listData($timezones, 'id', 'name')); ?>
    <?php echo $form->error($model, 'timezone_id'); ?>
</div>

<div class="row separator"></div>

<div class="row buttons">
    <?php echo CHtml::submitButton(_t('Save')); ?>
</div>

<?php echo $form->hiddenField($model, 'username', array('value' => _manager()->username)); ?>

<?php $this->endWidget(); ?>