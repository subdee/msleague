<?php $this->widget('Notice', array('session' => 'result')); ?>
<?php
$form = $this->beginWidget('CActiveForm', array(
        'action' => _url('settings/password')
    ));
?>
<div class="row">
    <?php echo $form->labelEx($model, 'password'); ?>
    <?php echo $form->passwordField($model, 'password'); ?>
    <?php echo $form->error($model, 'password'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'newpassword'); ?>
    <?php echo $form->passwordField($model, 'newpassword'); ?>
    <div class="hidden tooltip">
        <?php
        echo _t('Your password must be at least {min} characters long. Make your password strong by adding numbers, symbols and capital letters.', array(
            '{min}' => 6,
        ));
        ?>
        <div class="neutral" id="pwdMeter"><?php echo _t('Very weak'); ?></div>
    </div>
    <?php echo $form->error($model, 'newpassword'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'newpassword2'); ?>
    <?php echo $form->passwordField($model, 'newpassword2'); ?>
    <div class="hidden tooltip">
        <?php
        echo _t('Repeat password exactly. {passFail}{passOk}', array(
            '{passFail}' => '<div class="pwdMatchFail hidden">' . _t('Passwords do not match.') . '</div>',
            '{passOk}' => '<div class="pwdMatchOk hidden">' . _t('Passwords match.') . '</div>',
        ));
        ?>
    </div>
    <?php echo $form->error($model, 'newpassword2'); ?>
</div>

<div class="row buttons">
    <?php echo CHtml::submitButton(_t('Change password')); ?>
</div>
<?php echo $form->hiddenField($model, 'username', array('value' => _manager()->username)); ?>
<?php $this->endWidget(); ?>

<script type="text/javascript">
    $(document).ready(function()
    {
        if($('#UserSettingsFormPass_newpassword').size() == 0) return;
    
        $('#UserSettingsFormPass_newpassword2').InputAutocompleteOff();
    
        $("#UserSettingsFormPass_newpassword,#UserSettingsFormPass_newpassword2")
        .tooltip({
            position: "center right",
            offset: [, 10],
            effect: "fade",
        })
        .pwdMeter({
            minLength: 6
        });
    
        $('#UserSettingsFormPass_newpassword2').keyup(function(){
            if($('#UserRegistrationForm_password').val() == '') {
                $('.pwdMatchFail').hide();
                $('.pwdMatchOk').hide();
            }
            else if($(this).val() != $('#UserSettingsFormPass_newpassword').val()) {
                $('.pwdMatchFail').show();
                $('.pwdMatchOk').hide();
            } else {
                $('.pwdMatchFail').hide();
                $('.pwdMatchOk').show();
            }
        });
    });
</script>