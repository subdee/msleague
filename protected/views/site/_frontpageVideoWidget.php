<div id="videocontainer">
    <?php if ($link) : ?>
        <iframe width="375" height="309" src="http://www.youtube.com/embed/<?php echo $link; ?>?rel=0&autoplay=0" frameborder="0" allowfullscreen></iframe>
    <?php else : ?>
        <?php echo _t('Loading the player ...'); ?>
        <script type="text/javascript">
            jwplayer("videocontainer").setup({
                flashplayer: "<?php echo _bu('/video/player.swf'); ?>",
                file: "<?php echo _bu('/video/video.mp4'); ?>",
                height: 275,
                width: 375,
                image: "<?php echo _bu('/video/image.png'); ?>",
                //                        autostart: true,
                controlbar: "bottom"
            });
        </script>
    <?php endif; ?>
</div>