<?php echo CHtml::tag('div', array(), _t('The Terms of Use have been changed. In order to continue playing, you need to read and accept the updated Terms of Use.')); ?>
<?php echo CHtml::tag('div', array('class' => 'staticWindow'),$termsOfUse); ?>
<?php echo CHtml::openTag('div', array('class' => 'buttons')); ?>
<?php echo CHtml::link(CHtml::button(_t('Accept')),_url('managers/acceptTerms')); ?>
<?php echo CHtml::link(CHtml::button(_t('Decline')),_url('user/logout')); ?>
<?php echo Chtml::closeTag('div'); ?>