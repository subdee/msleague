<ul>
    <?php $count = count($news); ?>
    <?php $counter = 0; ?>
    <?php foreach ($news as $new) : ?>
        <?php $class = $counter & 1 ? 'even' : 'odd'; ?>
        <li class="<?php echo $class; ?>">
            <h3>
                <a target="_blank" href="<?php echo $new['link']; ?>"><?php echo $new['title']; ?></a>
            </h3>
            <?php echo $new['content']; ?>
        </li>
        <?php ++$counter; ?>
    <?php endforeach; ?>
</ul>