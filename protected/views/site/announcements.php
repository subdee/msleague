<?php $counter = 0; ?>
<?php $lastDate = null; ?>
<?php foreach ($a as $ann) : ?>
<?php $class = $counter & 1 ? 'even' : 'odd'; ?>
    <div class="announcement-date <?php echo $class; ?>">
        <?php if (date('Y-m-d',strtotime($ann->date_entered)) != $lastDate) : ?>
            <h1><?php echo Utils::date($ann->date_entered,array('time'=>false,'monthAbbreviated'=>false,'dayAbbreviated'=>false)); ?></h1>
        <?php endif; ?>
        <div class="announcement">
            <div class="time">
                <?php echo _app()->dateFormatter->format('H:mm', $ann->date_entered); ?>
            </div>
            <div class="details">
                <div class="title bold"><?php echo $ann->title; ?></div>

                <div class="content">
                    <?php echo $ann->content; ?>
                </div>
            </div>
        </div>
    </div>
<?php $lastDate = date('Y-m-d',strtotime($ann->date_entered)); ?>
<?php ++$counter; ?>
<?php endforeach; ?>