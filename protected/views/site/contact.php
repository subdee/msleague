<?php $this->widget('Notice', array('session' => 'contact')); ?>
<div class="profile-usermenu">
    <?php
    echo CHtml::tag('h3', array(), _t('Contact'));
    echo _t('Fill out the form to contact us.');
    ?>
</div>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'htmlOptions' => array(
        'onSubmit' => '$(this).find(":submit").attr("disabled", true);',
        'class' => 'contact',
    ),
    'focus' => array($model, 'subject')
    ));
?>
<p class="form-hint">
    <?php echo str_replace('*', CHtml::tag('span', array('class' => 'required'), '*'), _t('Asterisk (*) indicates required fields')); ?>
</p>
<div class="row">
    <?php echo $form->labelEx($model, 'subject'); ?>
    <?php echo $form->textField($model, 'subject', array('size' => 60, 'maxlength' => 128)); ?>
    <?php echo $form->error($model, 'subject'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'body'); ?>
    <?php echo $form->textArea($model, 'body', array('rows' => 6, 'cols' => 50)); ?>
    <?php echo $form->error($model, 'body'); ?>
</div>

<?php if (_is_logged_in()) : ?>
    <?php echo $form->hiddenField($model, 'email'); ?>
<?php else : ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email'); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>
<?php endif; ?>

<div class="row captcha">
    <?php $this->widget('CCaptcha'); ?>
</div>

<?php if (CCaptcha::checkRequirements()): ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'verifyCode'); ?>
        <?php echo $form->textField($model, 'verifyCode', array('title' => _t('Enter the letters as they are shown in the image. Letters are not case-sensitive.'))); ?>
        <?php echo $form->error($model, 'verifyCode'); ?>
    </div>
<?php endif; ?>

<div class="row buttons">
    <?php echo CHtml::submitButton(_t('Send')); ?>
</div>
<?php $this->endWidget(); ?>
<script type="text/javascript">
    $("#ContactForm_verifyCode").tooltip({
        position: "center right",
        offset: [0, 10],
        effect: "fade"
    });
</script>