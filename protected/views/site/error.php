<div class="errorPage">
    <?php
    if (_app()->errorHandler->error['code'] == 404) {
        $message = CHtml::tag('div', array(), _t('The page you requested is not available.')) . CHtml::image(Utils::imageUrl('404.png'));
        echo $message;
    } else {
        $message = '(' . _app()->errorHandler->error['code'] . ') ' . CHtml::encode(_app()->errorHandler->error['message']);
        $this->widget('Notice', array(
            'message' => $message,
            'type' => 'error',
        ));
    }
    ?>
</div>