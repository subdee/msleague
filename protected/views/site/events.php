<div class="box_c">
        <div class="boxheader"><span class="title_c"><?php echo _t('Game Log'); ?></span></div>
        <div class="boxmain_c">
                <div class="boxtext_c">
                        <div class="list announcementsp">
                                <table cellspacing="0" cellpadding="0" class="history">
                                    <?php foreach($events as $event) : ?>
                                    <tr style="">
                                        <td width="18%" valign="top">
                                            <span class="transaction"><?php echo Utils::date($event->date_occurred); ?></span>
                                        </td>
                                        <td valign="top">
                                            <?php echo $event->event; ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </table>
                        </div>
                </div>
        </div>
        <div class="boxfooter_c"><!-- empty--></div>
</div>