<div class="left">
    <?php
    $this->widget('AnnouncementsBox', array(
        'cache' => array('indexWelcome', array('duration' => 60))
    ));

    Utils::triggerEvent('onFrontpageBoxLeft2', $this);

    $this->widget('WidgetBox', array(
        'title' => _t('How to play'),
        'static' => $howToPlay,
        'more' => array(
            'url' => _url('instructions'),
            'label' => _t('Game guide'),
        )
    ));

//    $this->widget('WidgetBox', array(
//        'class' => 'facebookBox',
//        'title' => false,
//        'view' => Utils::themeViewsDirectory('site._facebookBox'),
//    ));

    $this->widget('WidgetBox', array(
        'class' => 'fansstats',
        'title' => _t('Fans Stats'),
        'view' => '_fansStatsBox',
        'params' => array('teams' => $teams),
    ));
    ?>
</div>

<div class="right">
    <?php
    $this->widget('WidgetBox', array(
        'class' => 'news',
        'title' => _t('Latest news') . '&nbsp' . _t('by') . CHtml::tag('span', array(), CHtml::link(' OnSports.gr', 'http://www.onsports.gr')),
        'static' => CHtml::tag('div', array('class' => 'news-container'), CHtml::image(Utils::imageUrl('ajaxloader/small-circle.gif'), '', array('class' => 'loading-image'))),
        'more' => array(
            'url' => _url('site/news'),
            'label' => _t('More'),
        )
    ));

    $this->widget('WidgetBox', array(
        'class' => 'fixtures',
        'title' => _t('Fixtures &amp; Results'),
        'view' =>'_fixturesResultsBox',
        'params' => array('games' => $games),
        'more' => array(
            'url' => _url('match/index'),
            'label' => _t('More'),
        )
    ));

//    $this->widget('WidgetBox', array(
//        'class' => 'news',
//        'title' => _manager()->profile->team->name,
//        'static' => CHtml::tag('div', array('class' => 'team-news-container'), CHtml::image(Utils::imageUrl('loading.gif'), '', array('class' => 'loading-image'))),
//        'more' => array(
//            'url' => _url('site/news'),
//            'label' => _t('More'),
//        )
//    ));

//    $this->widget('WidgetBox', array(
//        'class' => 'facebookBox',
//        'title' => false,
//        'view' => 'webroot.themes.' . Yii::app()->theme->name . '.views.site._twitterBox',
//    ));
    ?>

</div>