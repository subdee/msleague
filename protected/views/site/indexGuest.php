<?php if ($this->beginCache('indexGuest', array('duration' => 2592000))) : ?>

    <div id="loopedslider" class="left">
        <div class="container">
            <div class="slides">
                <div><?php echo CHtml::image(Utils::imageUrl('slideshow/slideshow1.jpg')); ?></div>
                <div><?php echo CHtml::image(Utils::imageUrl('slideshow/slideshow2.jpg')); ?></div>
                <div><?php echo CHtml::image(Utils::imageUrl('slideshow/slideshow3.jpg')); ?></div>
                <div><?php echo CHtml::image(Utils::imageUrl('slideshow/slideshow4.jpg')); ?></div>
                <div><?php echo CHtml::image(Utils::imageUrl('slideshow/slideshow5.jpg')); ?></div>
            </div>
        </div>
        <div class="controls">
            <a href="#" class="previous"><?php echo CHtml::image(Utils::imageUrl('slideshow/previous.png')); ?></a>
            <a href="#" class="next"><?php echo CHtml::image(Utils::imageUrl('slideshow/next.png')); ?></a>
        </div>
    </div>

    <?php
    $this->widget('WidgetBox', array(
        'title' => Gameconfiguration::model()->get('game_name'),
        'dynamic' => 'guestText',
        'class' => 'left',
        'more' => array(
            'label' => _t('Screenshots'),
            'url' => _url('instructions/screenshots'),
        )
    ));

    $this->widget('WidgetBox', array(
        'dynamic' => 'videoUrl',
        'class' => 'right video'
    ));
    
    $this->endCache();
    ?>
<?php endif; ?>