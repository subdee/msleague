<ul class="news-page">
    <?php $count = count($news); ?>
    <?php $counter = 0; ?>
    <?php foreach ($news as $new) : ?>
        <?php $class = $counter & 1 ? 'even' : 'odd'; ?>
        <li class="<?php echo $class; ?>">
            <h3>
                <a target="_blank" href="<?php echo $new['link']; ?>"><?php echo $new['title']; ?></a>
            </h3>
            <div class="date"><?php echo Utils::date($new['date']); ?></div>
            <div class="content"><?php echo $new['content']; ?></div>
            <div class="more"><a target="_blank" href="<?php echo $new['link']; ?>"><?php echo _t('Read more'); ?> &raquo;</a></div>
        </li>
        <?php $counter++; ?>
    <?php endforeach; ?>
</ul>