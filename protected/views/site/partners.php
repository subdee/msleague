<div class="box_c">
    <div class="boxheader"><span class="title_c"><?php echo _t('Advertise'); ?></span></div>
    <div class="boxmain_c">
        <div class="boxtext_c">

            <?php $this->widget('Notice', array('session'=>'contact')); ?>

            <div class="contact-prolog">
                <p><?php echo _t('If you wish to join the advertising and sponsoring program of MSLeague, fill the form below.'); ?></p>
            </div>
            <div class="wide form form_register">
                <p><em><b>(*)</b> <?php echo _t('indicates required fields'); ?></em></p>
                <?php $form=$this->beginWidget('CActiveForm', array('action'=>'#main')); ?>
                <?php //echo $form->errorSummary($model); ?>

                <div class="row">
                    <?php echo $form->labelEx($model,'fullname'); ?>
                    <?php echo $form->textField($model,'fullname'); ?>
                    <?php echo $form->error($model,'fullname'); ?>
                </div>
                
                <div class="row">
                    <?php echo $form->labelEx($model,'email'); ?>
                    <?php echo $form->textField($model,'email'); ?>
                    <?php echo $form->error($model,'email'); ?>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model,'company'); ?>
                    <?php echo $form->textField($model,'company'); ?>
                    <?php echo $form->error($model,'company'); ?>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model,'address'); ?>
                    <?php echo $form->textField($model,'address'); ?>
                    <?php echo $form->error($model,'address'); ?>
                </div>

                <div class="row">
                    <?php echo $form->labelEx($model,'phone'); ?>
                    <?php echo $form->textField($model,'phone'); ?>
                    <?php echo $form->error($model,'phone'); ?>
                </div>
                
                <div class="row">
                    <?php echo $form->labelEx($model,'topic'); ?>
                    <?php echo $form->textArea($model,'topic',array('rows'=>6, 'cols'=>50)); ?>
                    <?php echo $form->error($model,'topic'); ?>
                </div>

                <?php if(CCaptcha::checkRequirements()): ?>
                <div class="row">
                    <?php echo $form->labelEx($model,'verifyCode'); ?>
                    <?php echo $form->textField($model,'verifyCode'); ?>
                    <?php echo $form->error($model,'verifyCode'); ?>
                </div>
                <div class="row captcha">
                    <?php $this->widget('CCaptcha'); ?>
                </div>
                <div class="hint"><?php echo _t('Please enter the letters as they are shown in the image above. Letters are not case-sensitive.'); ?></div>
                <?php endif; ?>

                <div class="row buttons">
                    <?php echo CHtml::submitButton('Send'); ?>
                </div>
            <?php $this->endWidget(); ?>
            </div><!-- form -->
        </div>
    </div>
    <div class="boxfooter_c"><!-- empty--></div>
</div>
