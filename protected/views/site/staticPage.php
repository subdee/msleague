<?php echo $staticContent; ?>
<?php if (isset($renderDialog) && $renderDialog) : ?>
    <?php
    $this->widget('NotificationDialog', array(
        'title' => _t('Terms of Use'),
        'obligatory' => true,
        'content' => $this->renderPartial('_newTermsOfUse', array('termsOfUse' => $staticContent), true)
    ));
    ?>
<?php endif; ?>
