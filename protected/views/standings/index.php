<?php
if (_is_logged_in() && _manager()->hasRole(Role::MANAGER)) {
    echo CHtml::openTag('div', array('class' => 'table-outer-header'));
    echo CHtml::tag('span', array('class' => 'manager-rank'), _t('Your rank: {rank}', array('{rank}' => CHtml::tag('span', array(), _manager()->rank->rank))));
    echo CHtml::closeTag('div');
}

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'pager' => array(
        'class' => 'CLinkPager',
        'header' => '',
    ),
    'rowCssClassExpression' => '_is_logged_in() ? ($data->id == _manager()->id ? (($row&1 ? "even" : "odd") . " owner") : ($row&1 ? "even" : "odd")) : ($row&1 ? "even" : "odd")',
    'template' => '{pager}{items}{pager}',
    'columns' => array(
        array(
            'value' => '$data->rank->rank',
            'header' => _t('Rank'),
            'filter' => false,
            'htmlOptions' => array('class' => 'first-column'),
        ),
        array(
            'type' => 'raw',
            'filter' => false,
            'value' => 'CHtml::image($data->profilePhoto())',
            'htmlOptions' => array('class' => 'profile-image'),
        ),
        array(
            'type' => 'raw',
            'name' => 'username',
//            'value' => '($data->managerTeam ? "<span>" . ($data->hasRole(Role::MANAGER) ? CHtml::link($data->managerTeam->name,_url("profile/players",array("id" => $data->id))) : "<span title=\''._t('This manager has not yet created his team').'\'>" . $data->managerTeam->name . "</span>") . "</span>" : "") . CHtml::tag("div", array(), $data->profileLink() . "&nbsp;" . ($data->profile->line ? "&quot;" . $data->profile->line . "&quot;" : ""))',
            'value' => '$data->teamLink() . CHtml::tag("div", array(), $data->profileLink() . "&nbsp;" . ($data->profile->line ? "&quot;" . $data->profile->line . "&quot;" : ""))',
            'filter' => CHtml::textField('Manager[username]', $name, array('maxlength' => 17)),
            'htmlOptions' => array('class' => 'manager'),
        ),
        array(
            'type' => 'raw',
            'name' => 'total_points',
            'filter' => false,
            'value' => 'Utils::number($data->total_points,true)',
            'htmlOptions' => array('class' => 'manager-points'),
        ),
    ),
));
?>