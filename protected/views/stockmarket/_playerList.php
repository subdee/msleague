<?php if (isset($extra['notice'])) : ?>
    <?php $this->widget('Notice', $extra['notice']); ?>
<?php endif; ?>
<table class="player-list">
    <thead>
        <tr>
            <th class="first"></th>
            <th class="playername"><?php echo _t('Name'); ?></th>
            <th><?php echo _t('Position'); ?></th>
            <th><?php echo _t('Team'); ?></th>
            <th><?php echo _t('Shares'); ?></th>
            <th>
                <?php
                if ($this->action->id == 'sell') {
                    echo _t('Sell Price');
                } else {
                    echo _t('Buy Price');
                }
                ?>
            </th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
        </tr>
        <tr class="filters">
            <th></th>
            <th class="name"><input type="text" value="<?php echo _t('Search player...'); ?>" size="13" maxlength="17" /></th>
            <th class="position">
                <?php if (isset($extra['positions'])) : ?>
                    <select>
                        <option value=""></option>
                        <?php foreach ($extra['positions'] as $pos) : ?>
                            <option value="<?php echo $pos->getTranslatedAbbr(); ?>"><?php echo $pos->getTranslatedAbbr(); ?></option>
                        <?php endforeach; ?>
                    </select>
                <?php endif; ?>
            </th>
            <th class="team">
                <select>
                    <option value=""></option>
                    <?php foreach ($teams as $team) : ?>
                        <option value="<?php echo $team->shortname5; ?>"><?php echo $team->shortname5; ?></option>
                    <?php endforeach; ?>
                </select>
            </th>
            <th></th>
            <th class="stock-value"></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($displayList as $dl) {
            _controller()->renderPartial('_playerListRow', array('display' => $dl, 'action' => $this->action->id));
        }
        ?>
    </tbody>
</table>
