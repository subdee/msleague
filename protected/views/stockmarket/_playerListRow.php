<?php
echo CHtml::openTag('tr', array(
    'class' => $display->cssClass,
    'onclick' => $display->isActive ? "showPopup({$display->player->id}, '$action')" : '',
    'title' => $display->inactivityReason,
));
?>

<td class="first"><?php echo $display->player->small_jersey_html(); ?></td>
<td class="playername">
    <?php
    echo CHtml::tag('span', array(), $display->player->shortname);
    echo $display->injuredHtml();
    echo $display->suspendedHtml();
    ?>
</td>
<td><?php echo $display->player->basicPosition->getTranslatedAbbr(); ?></td>
<td><?php echo $display->player->team != null ? $display->player->team->shortname5 : '-'; ?></td>
<td><?php echo $display->mtp ? $display->mtp->shares : '-'; ?></td>
<td class="stock-value">
    <?php
    if ($action == 'sell')
        echo $display->player->current_value;
    else
        echo $display->player->current_value * Gameconfiguration::model()->getCommissionFactor();
    ?>
</td>

<!-- We turn the color white because javascript takes some time to kick in and hide them -->
<td style="color:white"><?php echo $display->isActive; ?></td>
<td style="color:white"><?php echo $display->isOwned; ?></td>
<td style="color:white"><?php echo $display->isWatched; ?></td>

<?php echo CHtml::closeTag('tr'); ?>