<?php $dialog_id = "stocksPopup-{$player->id}"; ?>
<?php
if ($buy) {
    $dialog_icon = 'money_up32.png';
    $dialog_title = _t('Buy Shares');
} else if ($replace) {
    $dialog_icon = 'replace32.png';
    $dialog_title = _t('Replace player');
} else {
    $dialog_icon = 'money_down32.png';
    $dialog_title = _t('Sell Shares');
}
//$dialog_title = htmlentities(CHtml::image(Utils::imageUrl($dialog_icon))) . CHtml::tag('span', array(), $dialog_title);
?>

<div id="<?php echo $dialog_id; ?>"
     title="<?php echo $dialog_title; ?>"
     class="stocksPopup stocksPopupStandard">
    <form onSubmit='$(this).find(":submit").attr("disabled", true);'>

        <?php
        $this->renderPartial('_popupSharesPlayerInfo', array(
            'player' => $player,
            'curStocks' => $curStocks,
            'action' => ($buy || $replace) ? 'buy' : 'sell',
        ));
        ?>

        <div class="info-box slider-info">
            <label>
                <?php
                if ($buy || $replace)
                    echo _t('Choose how many shares you want to buy:');
                else
                    echo _t('Choose how many shares you want to sell:');
                ?>
            </label>
            <div class="slider" />
        </div>

        <div class="info-box transaction-info">
                <?php if ($buy || $replace) : ?>
                <label>
                    <?php
                    echo _t('Buy {sc} share(s) at price {pp}?', array(
                        '{sc}' => CHtml::tag('span', array('class' => 'stock-count'), ''),
                        '{pp}' => CHtml::tag('span', array('class' => 'purchase-price'), ''),
                    ));
                    ?>
                </label>
                <!--                <label>
                <?php
//                    echo _t('Extra fee: {cf}', array(
//                        '{cf}' => CHtml::tag('span', array('class' => 'commission-fee'), ''),
//                    ));
                ?>
                                </label>-->
                <?php
                //echo CHtml::image(Utils::themeImageUrl('iconset/info.png'), 'help', array('title' => _t('You are charged an extra {fee}% fee on total buying price.', array('{fee}' => Gameconfiguration::model()->get('commission_perc')))));
                ?>
                <?php else : ?>
                <label>
                    <?php
                    echo _t('Sell {sc} share(s) at price {pp}?', array(
                        '{sc}' => CHtml::tag('span', array('class' => 'stock-count'), ''),
                        '{pp}' => CHtml::tag('span', array('class' => 'purchase-price'), ''),
                    ));
                    ?>
                </label>
<?php endif; ?>
        </div>

        <div class="info-box buttons last">
            <input type="hidden" name="action" value="<?php echo $action; ?>" />
            <input type="hidden" name="shares" value="0" />
            <input type="hidden" class="share-value" value="<?php echo $player->current_value; ?>" />
<?php if ($replace) : ?>
                <input type="hidden" name="player_buy_id" value="<?php echo $player->id; ?>" />
                <input type="hidden" name="player_sell_id" value="<?php echo $_SESSION['playerToReplace']; ?>" />
                <input type="submit" value="<?php echo _t('Replace'); ?>" disabled />
<?php else : ?>
                <input type="hidden" name="player_id" value="<?php echo $player->id; ?>" />
                <input type="submit" value="<?php echo ($buy) ? _t('Buy') : _t('Sell'); ?>" disabled />
<?php endif; ?>
        </div>

    </form>
</div>
<?php
$this->renderPartial('_popupWarnTooltip', array(
    'player' => $player,
    'class' => 'warn-tooltip-' . ($replace ? 'replace-' : '') . $player->id . (($buy || $replace) || $canReplace ? '' : ' caution'),
    'maxReason' => $maxReason,
));
?>
