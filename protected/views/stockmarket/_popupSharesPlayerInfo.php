<?php
$isLesser = false;
if (isset($type) && $type == 'lesser')
    $isLesser = true;
?>
<div class="info-box player-name <?php if ($isLesser)
    echo 'with-custom-select-box'; ?>" >
         <?php echo $player->name; ?>
         <?php if ($isLesser) : ?>
         <?php endif; ?>
</div>
<div class="info-box player-info">
    <?php
    if ($player->team)
        echo $player->team->large_jersey_html();
    else
        echo Team::noteam_small_jersey_html();
    ?>

    <table>
        <tr>
            <td><?php echo _t('Position'); ?>:</td>
            <td><?php echo mb_ucfirst($player->basicPosition->getTranslatedName()); ?></td>
        </tr>
        <tr>
            <td><?php echo _t('Team'); ?>:</td>
            <td><?php echo $player->team != null ? $player->team->name : '-'; ?></td>
        </tr>
        <tr>
            <td>
                <?php
                if ($action == 'sell')
                    echo _t('Sell price');
                else
                    echo _t('Buy price');
                ?>:
            </td>
            <td class="stock-value">
                <?php
                if ($action == 'sell')
                    echo _currency($player->current_value);
                else
                    echo _currency($player->current_value * Gameconfiguration::model()->getCommissionFactor());
                ?>
            </td>
        </tr>
        <?php if (!$isLesser) : ?>
            <tr>
                <td><?php echo _t('You own'); ?>:</td>
                <td><?php echo _t('{n} shares', array('{n}' => CHtml::tag('span', array('class' => 'current-stocks'), $curStocks))); ?></td>
            </tr>
        <?php endif; ?>
    </table>
</div>

