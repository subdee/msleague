<div id="popupTransactionCompleted"
     title="<?php echo _t('Transaction completed'); ?>" 
     class="stocksPopup transaction-result">

    <div id="transactionResultMessage"></div>
    <div class="info-box"><?php echo _t('What would you like to do next?'); ?></div>
    <ul class="info-box last">
        <li class="buy">
            <?php echo CHtml::link(CHtml::tag('span', array(), _t('Buy shares')), _url('stockmarket/buy')); ?>
        </li>
        <li class="sell">
            <?php echo CHtml::link(CHtml::tag('span', array(), _t('Sell shares')), _url('stockmarket/sell')); ?>
        </li>
        <li class="hidden pitch">
            <?php echo CHtml::link(CHtml::tag('span', array(), _t('View your team')), _url('pitch/index')); ?>
        </li>
    </ul>

</div>