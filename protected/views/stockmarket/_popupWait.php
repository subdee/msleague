<div id="popupWait"
     title="<?php echo _t('Transaction in progress'); ?>" 
     class="stocksPopup transaction-wait">
    <?php $this->widget('Notice', array('id' => 'popupWaitNotice', 'message' => _t('Please wait...'), 'type' => 'loader')); ?>
</div>