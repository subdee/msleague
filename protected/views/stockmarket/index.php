<?php $this->widget('InformationHBar'); ?>

<div id="front-content">

    <?php $this->widget('Notice', array('session' => 'transaction')); ?>
    <?php $this->widget('Notice', array('session' => 'notice')); ?>

    <?php if ($isUserBanned || _manager()->hasRole(Role::RESET)) : ?>

        <div class="marketplace inactive">
            <div class="catchphrase"><?php echo _t('In the stockmarket you can buy and sell shares of players.'); ?></div>
            <div class="anchors">
                <?php echo CHtml::tag('div', array('class' => 'buy'), CHtml::tag('div', array(), _t('Buy'))); ?>
                <?php echo CHtml::tag('div', array('class' => 'sell'), CHtml::tag('div', array(), _t('Sell'))); ?>
            </div>
            <div class="status">
                <?php
                if (_manager()->hasRole(Role::RESET))
                    $message = _t('You must create your team before you can access the stockmarket.');
                else
                    $message = _t('Access to the stock market is denied because you are banned.');
                $this->widget('Notice', array(
                    'message' => $message,
                    'type' => 'warning',
                    'class' => 'no-background',
                ));
                ?>
            </div>
        </div>

    <?php elseif (!$isMarketOpen) : ?>

        <?php
        if (!$manualClosed) {
            $message = _t('The stockmarket is temporarily CLOSED.');
        } else {
            $message = _t('The stockmarket is currently CLOSED.');
            $nextMatch = Gameconfiguration::model()->getCloseTime();
            if ($nextMatch) {
                $openTime = new DateTime(_app()->localtime->getGameNow());
                $openTime = $openTime->add(new DateInterval('P1D'))->format('Y-m-d');
                $openTime = Utils::date(_app()->localtime->fromGameToLocal($openTime, 'Y-m-d H:i:s'));
                $message .= ' ' . _t('Opening time: {openTime}.', array('{openTime}' => CHtml::tag('strong', array(), $openTime)));
            }
        }
        ?>

        <div class="marketplace inactive">
            <div class="catchphrase"><?php echo _t('In the stockmarket you can buy and sell shares of players.'); ?></div>
            <div class="anchors">
                <?php echo CHtml::tag('div', array('class' => 'buy'), CHtml::tag('div', array(), _t('Buy'))); ?>
                <?php echo CHtml::tag('div', array('class' => 'sell'), CHtml::tag('div', array(), _t('Sell'))); ?>
            </div>
            <div class="status">
                <?php
                $this->widget('Notice', array(
                    'message' => $message,
                    'type' => 'warning',
                    'class' => 'no-background',
                ));
                ?>
            </div>
        </div>

    <?php else : ?>
        <div class="marketplace">
            <div class="catchphrase"><?php echo _t('In the stockmarket you can buy and sell shares of players.'); ?></div>
            <div class="anchors">
                <?php echo CHtml::link(CHtml::tag('div', array(), _t('Buy')), _url('stockmarket/buy'), array('class' => 'buy')); ?>
                <?php echo CHtml::link(CHtml::tag('div', array(), _t('Sell')), _url('stockmarket/sell'), array('class' => 'sell')); ?>
            </div>
            <div class="status">
                <?php
                $message = _t('Current trading period ends on {end}.', array(
                    '{closeTime}' => Utils::date($closeTime, array('time' => false)),
                    '{end}' => CHtml::tag('span', array('class' => 'bold'), Utils::date($closeTime)),
                    ));

                echo $message;
                ?>
            </div>
        </div>

    <?php endif; ?>

    <?php
    if (_isModuleOn('chat'))
        $this->renderPartial('//chat/_chatForm', array('shouts' => $shouts, 'isUserBanned' => $isUserBanned));
    ?>

</div>
<div id="playerList">
</div>