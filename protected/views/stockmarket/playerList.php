<?php $this->widget('InformationHBar'); ?>

<div id="left-profile-column" class="stockmarket">
    <?php $this->widget('StockmarketFilters'); ?>
</div>
<div id="right-profile-column" class="stockmarket">

    <?php $this->widget('Notice', array('session' => 'transaction')); ?>
    <?php $this->widget('Notice', array('session' => 'notice')); ?>
    
    <div id="playerList">
        <?php
        $this->renderPartial('_playerList', array(
            'extra' => $extra,
            'teams' => $teams,
            'displayList' => $displayList,
        ));
        ?>
    </div>
</div>
<div id="popupContainer"></div>
<?php $this->renderPartial('_popupSharesTransactionResult'); ?>
<?php $this->renderPartial('_popupWait'); ?>