<?php $this->widget('Notice', array('session' => 'notice')); ?>
<?php $this->widget('Notice', array('message' => _t('Account was successfully activated'), 'type' => 'success')); ?>
<div class="profile-usermenu">
    <?php echo _t('Welcome to the game {manager}! You can now login and enjoy the game.', array('{manager}' => CHtml::tag('em', array(), $manager))); ?>
</div>