<?php $this->widget('Notice', array('session' => 'notice')); ?>
<?php $this->widget('Notice', array('session' => 'failedEmail')); ?>
<div class="profile-usermenu">
    <?php echo _t('Check your email for instructions on how to activate your account. NOTE: Check your spam folder as well.'); ?>
</div>