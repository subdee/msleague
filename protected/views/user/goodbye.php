<div class="profile-usermenu">
    <?php
    echo CHtml::tag('h3', array(), _t('Thank you for playing {gameName}', array('{gameName}' => _gameName())));
    echo _t('We hope to see you again in the game.');
    ?>
</div>