<div class="profile-usermenu">
    <?php
    echo CHtml::tag('h3', array(), _t('Login'));
    echo _t('To access the whole content, you must login.');
    ?>
</div>
<?php $this->widget('Notice', array('session' => 'login')); ?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'htmlOptions' => array(
        'onSubmit' => '$(this).find(":submit").attr("disabled", true);',
        'class' => 'login',
    ),
    'focus' => array($model, 'username')
    ));
?>
<p class="form-hint">
    <?php echo str_replace('*', CHtml::tag('span', array('class' => 'required'), '*'), _t('Asterisk (*) indicates required fields')); ?>
</p>
<div class="row">
    <?php echo $form->labelEx($model, 'username'); ?>
    <?php echo $form->textField($model, 'username'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model, 'password'); ?>
    <?php echo $form->passwordField($model, 'password'); ?>
</div>

<div class="row buttons">
    <?php echo CHtml::submitButton(_t('Login')); ?>
</div>

<div class="row forgot-pass">
    <?php echo CHtml::link(_t('Forgot password?'), _url('passwordReset/index')); ?>
</div>

<?php $this->endWidget(); ?>
