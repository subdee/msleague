<?php if ($toomany) : ?>
    <?php
    $this->widget('Notice', array(
        'message' => _t("Registration is currently closed. Come back later!"),
        'type' => 'info'
    ));
    ?>
<?php else : ?>
    <div class="profile-usermenu">
        <?php
        echo CHtml::tag('h3', array(), _t('Register'));
        ?>
    </div>

    <?php
    if ($invitation_id) {
        $this->widget('Notice', array(
            'message' => _t("Register now and join {inviter} in the most exciting sports game.", array('{inviter}' => $inviter))
        ));
    }
    ?>

    <?php $this->widget('Notice', array('session' => 'notice')); ?>

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'action' => _url('user/register'),
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnChange' => false,
        ),
        'htmlOptions' => array(
            'onSubmit' => '$(this).find(":submit").attr("disabled", true);',
            'class' => 'register',
        ),
        'focus' => array($model->manager, 'username')
        ));
    ?>
    <p class="form-hint">
        <?php echo str_replace('*', CHtml::tag('span', array('class' => 'required'), '*'), _t('Asterisk (*) indicates required fields')); ?>
    </p>
    <div class="row">
        <?php
        echo $form->labelEx($model->manager, 'username');
        echo $form->textField($model->manager, 'username');
        echo CHtml::openTag('div', array('class' => 'hidden tooltip username'));
        echo _t('Your username must be bewteen 5 and 16 characters long (letters, numbers and underscores).');
        echo CHtml::closeTag('div');
        echo $form->error($model->manager, 'username');
        ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model->manager, 'fullname'); ?>
        <?php echo $form->textField($model->manager, 'fullname', array('title' => _t('Your name will not appear anywhere in the game and will be used to verify your identity in case you win a prize.'))); ?>
        <?php echo $form->error($model->manager, 'fullname'); ?>
    </div>
    <div class="row separator"></div>
    <div class="row">
        <?php echo $form->labelEx($model->manager, 'password'); ?>
        <?php echo $form->passwordField($model->manager, 'password'); ?>
        <div class="hidden tooltip">
            <?php
            echo _t('Your password must be at least {min} characters long. Make your password strong by adding numbers, symbols and capital letters.', array(
                '{min}' => 6,
            ));
            ?>
            <div class="neutral" id="pwdMeter"><?php echo _t('Very weak'); ?></div>
        </div>
        <?php echo $form->error($model->manager, 'password'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model->manager, 'password_repeat'); ?>
        <?php echo $form->passwordField($model->manager, 'password_repeat'); ?>
        <div class="hidden tooltip">
            <?php
            echo _t('Repeat password exactly. {passFail}{passOk}', array(
                '{passFail}' => '<div class="pwdMatchFail hidden">' . _t('Passwords do not match.') . '</div>',
                '{passOk}' => '<div class="pwdMatchOk hidden">' . _t('Passwords match.') . '</div>',
            ));
            ?>
        </div>
        <?php echo $form->error($model->manager, 'password_repeat'); ?>
    </div>
    <div class="row separator"></div>
    <div class="row">
        <?php echo $form->labelEx($model->manager, 'email'); ?>
        <?php echo $form->textField($model->manager, 'email', array('title' => _t('Type your email. Your email will be used to activate your account.'))); ?>
        <?php echo $form->error($model->manager, 'email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model->manager, 'email_repeat'); ?>
        <?php echo $form->textField($model->manager, 'email_repeat', array('title' => _t('Repeat your email.'))); ?>
        <?php echo $form->error($model->manager, 'email_repeat'); ?>
    </div>

    <div class="row separator"></div>
    <div class="row">
        <?php echo $form->labelEx($model->manager->profile, 'country_id'); ?>
        <?php echo $form->dropDownList($model->manager->profile, 'country_id', CHtml::listData($countries, 'id', 'country'), array('value' => $model->manager->profile->country_id)); ?>
        <?php echo $form->error($model->manager->profile, 'country_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model->manager, 'timezone_id'); ?>
        <?php echo $form->dropDownList($model->manager, 'timezone_id', CHtml::listData($timezones, 'id', 'name'), array('value' => $model->manager->timezone_id)); ?>
        <?php echo $form->error($model->manager, 'timezone_id'); ?>
    </div>
    <?php if (_isModuleOn('sms')) : ?>
        <div class="row mobile">
            <?php echo $form->labelEx($model, 'mobile'); ?>
            <?php echo $form->textField($model, 'countryCode', array('class' => 'countryCode', 'title' => _t('Enter your country code.'))); ?>
            <?php echo $form->textField($model, 'mobile', array('class' => 'phoneNumber', 'title' => _t('Enter your mobile number. This information will be used only for activation purposes.'))); ?>
            <?php echo $form->error($model, 'countryCode'); ?>
            <?php echo $form->error($model, 'mobile'); ?>
            <?php echo $form->error($model, 'fullMobile'); ?>
            <?php echo CHtml::dropDownList('country', '', CHtml::listData($countries, 'id', 'dial'), array('class' => 'hidden')); ?>
        </div>
    <?php endif; ?>

    <div class="row separator"></div>
    <div class="row">
        <?php echo $form->labelEx($model->manager->profile, 'gender'); ?>
        <?php echo $form->dropDownList($model->manager->profile, 'gender', Profile::model()->genderArray()); ?>
    </div>

    <div class="row captcha">
        <?php $this->widget('CCaptcha'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'verifyCode'); ?>
        <?php echo $form->textField($model, 'verifyCode', array('title' => _t('Enter the result of the formula shown in the picture.'))); ?>
        <?php echo $form->error($model, 'verifyCode'); ?>
    </div>

    <div class="row checkbox">
        <?php echo $form->checkBox($model->manager, 'newsletter', array('checked' => $model->manager->newsletter)); ?>
        <div class="hint"><?php echo _t('I want to receive newsletters from {gamename}', array('{gamename}' => _gameName())); ?></div>
    </div>

    <div class="row checkbox">
        <?php echo $form->checkBox($model, 'tac'); ?>
        <div class="hint">
            <?php
            echo _t('I am over 18 years old and I have read and agree to the {gamerules} and {terms}.', array(
                '{gamerules}' => _l(_t('Game rules'), _url('instructions/index')),
                '{terms}' => _l(_t('Terms of Use'), _param('termsOfUseUrl'), array('target' => '_blank')),
            ));
            ?>
        </div>
        <?php echo $form->error($model, 'tac'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton(_t('Register')); ?>
    </div>

    <?php echo CHtml::hiddenField('invitation_id', $invitation_id); ?>
    <?php echo CHtml::hiddenField('inviter', $inviter); ?>

    <?php $this->endWidget(); ?>
<?php endif; ?>
<script type="text/javascript">

    $(document).ready(function()
    {
        // TODO: Remove code SMS module specific.

        // select all desired input fields and attach tooltips to them
        $("form.register input[type='text']:not(.countryCode, #Manager_username), form.register input[type='password']").tooltip({
            position: "center right",
            offset: [0, 10],
            effect: "fade"
        });
        $(".countryCode").tooltip({
            position: "center right",
            offset: [0, 240],
            effect: "fade"
        });

        $("#Manager_email_repeat").InputAutocompleteOff();
        $("#Profile_country").change(function(){
            $('#country').val($(this).val()).change();
        });
        $('#country').change(function(){
            $('#UserRegistrationForm_countryCode').val($(this).find(':selected').text());
        });
        if($('#UserRegistrationForm_countryCode').val() == '') {
            $("#Profile_country").change();
        }

        $('#Manager_password').pwdMeter({
            minLength: 6
        });

        $('#Manager_password_repeat').keyup(function(){
            if($('#Manager_password').val() == '') {
                $('.pwdMatchFail').hide();
                $('.pwdMatchOk').hide();
            }
            else if($(this).val() != $('#Manager_password').val()) {
                $('.pwdMatchFail').show();
                $('.pwdMatchOk').hide();
            } else {
                $('.pwdMatchFail').hide();
                $('.pwdMatchOk').show();
            }
        });

        $("#Manager_username").tooltip({
            position: "center right",
            offset: [0, 10],
            effect: "fade"
        });

        $('#Manager_username').blur(function(){
            $("#Manager_username").tooltip().hide();
            checkUsernameAvailability();
        });

    });

    function checkUsernameAvailability() {
        $.ajax({
            context : $('#Manager_username'),
            url : 'checkUsernameAvailability',
            data : { username : $('#Manager_username').val() },
            dataType : 'json',
            beforeSend : function(){
                this.switchClass('unavailable available', 'loading');
                this.next().next().hide();
            },
            success : function(response){
                if(response.Manager_username) {
                    this.switchClass('loading available', 'unavailable');
                    this.next().next().show().html(response.Manager_username[0]);
                }
                else {
                    this.switchClass('loading unavailable', 'available');
                }
            }
        });
    }
</script>