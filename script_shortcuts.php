<?php

/**
 * This is the shortcut to Yii::app()
 */
function _app() {
    return Yii::app();
}

function _transaction() {
    return _app()->db->beginTransaction();
}

function _log($msg) {
    Yii::log($msg, 'info', 'script');
}

?>
